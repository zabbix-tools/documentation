[comment]: # translation:outdated

[comment]: # ({new-e62e1497})
# 5 Installation depuis les containers

[comment]: # ({/new-e62e1497})

[comment]: # ({new-a3ac452e})

### Overview

This section describes how to deploy Zabbix with [Docker](#docker) or [Docker Compose](#docker-compose).

Zabbix officially provides:

- Separate Docker images for each Zabbix
component to run as portable and self-sufficient containers.
- Compose files for defining and running
multi-container Zabbix components in Docker.

[comment]: # ({/new-a3ac452e})

[comment]: # ({new-f9acea87})
### Docker

Zabbix fourni des images [Docker](https://www.docker.com) pour chaque
composant Zabbix en tant que conteneurs portables et auto-suffisants
pour accélérer le déploiement et mettre à jour la procédure.

Les composants Zabbix sont fournis avec la prise en charge de base de
données MySQL et PostgreSQL, le support des serveurs web Apache2 et
Nginx. Ces images sont séparées en différentes images.

[comment]: # ({/new-f9acea87})

[comment]: # ({new-e498c2aa})

#### Tags

Official Zabbix component images may contain the following tags:

|Tag|Description|Example|
|--|----------|----|
|latest            | The latest stable version of a Zabbix component based on Alpine Linux image. |zabbix-agent:latest |
|<OS>-trunk | The latest nightly build of the Zabbix version that is currently being developed on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values: <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu  |zabbix agent:ubuntu-trunk |
|<OS>-latest     | The latest stable version of a Zabbix component on a specific operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu|zabbix-agent:ol-latest |
|<OS>-X.X-latest |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). |zabbix-agent:alpine-6.0-latest |
|<OS>-X.X.*      |The latest minor version of a Zabbix component of a specific major version and operating system. <br> <br> **<OS>** - the base operating system. Supported values:  <br> *alpine* - Alpine Linux; <br> *centos* - CentOS (only for Zabbix 4.0) <br> *ltsc2019* -  Windows 10 LTSC 2019 (agent only); <br> *ol* - Oracle Linux; <br> *ltsc2022* - Windows 11 LTSC 2022 (agent only); <br> *ubuntu* - Ubuntu<br><br>**X.X** - the major Zabbix version (supported: *4.0*, *5.0*, *6.0*, *6.2*). <br><br> **\*** - the Zabbix minor version  |zabbix-agent:alpine-6.4.1|

[comment]: # ({/new-e498c2aa})


[comment]: # ({new-fba82ded})
#### Sources des fichiers Docker

Tout le monde peut suivre les modifications du fichier Docker en
utilisant le [dépôt officiel](https://github.com/zabbix/zabbix-docker)
de Zabbix sur [github.com](https://github.com/). Vous pouvez faire un
fork du projet ou créer vos propres images basées sur les fichiers
Docker officiels.

[comment]: # ({/new-fba82ded})



[comment]: # ({new-0b4b68c7})
#### Utilisation

[comment]: # ({/new-0b4b68c7})

[comment]: # ({new-de9f41d4})
##### Variables d'environnement

Toutes les images des composants Zabbix fournissent des variables
d'environnement pour contrôler la configuration. Ces variables
d'environnement sont répertoriées dans chaque dépôt de composants. Ces
variables d'environnement sont des options des fichiers de configuration
Zabbix, mais avec une méthode de nommage différente. Par exemple,
`ZBX_LOGSLOWQUERIES` est égal à `LogSlowQueries` des fichiers de
configuration du serveur Zabbix et des proxys Zabbix.

::: noteimportant
 Certaines options de configuration ne sont pas
modifiables. Par exemple, `PIDFile` et `LogType`.
:::

Certains composants ont des variables d'environnement spécifiques, qui
n'existent pas dans les fichiers de configuration officiels de Zabbix :

|   |   |   |
|---|---|---|
|**Variable**|**Composants**|**Description**|
|`DB_SERVER_HOST`|Serveur<br>Proxy<br>Interface Web|Cette variable est l’adresse IP ou le nom DNS du serveur MySQL ou PostgreSQL.  \\\\Par défaut, la valeur est `mysql-server` ou `postgres-server` pour MySQL ou PostgreSQL respectivement|
|`DB_SERVER_PORT`|Serveur<br>Proxy<br>Interface Web|Cette variable est le port du serveur MySQL ou PostgreSQL.<br>Par défaut, la valeur est '3306' ou '5432' respectivement.|
|`MYSQL_USER`|Serveur<br>Proxy<br>Interface Web|Utilisateur de la base de données MySQL.<br>Par défaut, la valeur est 'zabbix'.|
|`MYSQL_PASSWORD`|Serveur<br>Proxy<br>Interface Web|Mot de passe de la base de données MySQL.<br>Par défaut, la valeur est 'zabbix'.|
|`MYSQL_DATABASE`|Serveur<br>Proxy<br>Interface Web|Nom de la base de données.<br>Par défaut, la valeur est 'zabbix' pour le serveur Zabbix et 'zabbix\_proxy' pour le proxy Zabbix.|
|`POSTGRES_USER`|Serveur<br>Interface Web|utilisateur de la base de données PostgreSQL.<br>Par défaut, la valeur est 'zabbix'.|
|`POSTGRES_PASSWORD`|Serveur<br>Interface Web|Mot de passe de la base de données PostgreSQL.<br>Par défaut, la valeur est 'zabbix'.|
|`POSTGRES_DB`|Serveur<br>Interface Web|Nom de la base de données.<br>Par défaut, la valeur est 'zabbix' pout le serveur Zabbix et 'zabbix\_proxy' pour le proxy Zabbix.|
|`TZ`|Interface Web|Fuseau horaire au format PHP. La liste complète des fuseaux horaires pris en charge est disponible sur [php.net](http://php.net/manual/en/timezones.php).<br> Par défaut, la valeur est 'Europe / Riga'.|
|`ZBX_SERVER_NAME`|Interface Web|Nom de l'installation Zabbix visible dans le coin supérieur droit de l'interface Web. <br>Par défaut, la valeur est 'Zabbix Docker'|
|`ZBX_JAVAGATEWAY_ENABLE`|Serveur<br>Proxy|Permet la communication avec la passerelle Java Zabbix pour collecter les vérifications liées à Java.\\\\ Par défaut, la valeur est "false"|
|`ZBX_ENABLE_SNMP_TRAPS`|Serveur<br>Proxy|Active la fonctionnalité de trap SNMP. Il requiert l'instance **zabbix-snmptraps** et le volume partagé */var/lib/zabbix/snmptraps* au niveau du serveur Zabbix ou au proxy Zabbix.|

[comment]: # ({/new-de9f41d4})

[comment]: # ({new-7d10f7dc})
##### Volumes

Les images permettent d'utiliser certains points de montage. Ces points
de montage sont différents et dépendent du type de composant Zabbix :

|   |   |
|---|---|
|**Volume**|**Description**|
|**Agent Zabbix**|<|
|*/etc/zabbix/zabbix\_agentd.d*|Le volume permet d'inclure des fichiers *\*.conf* et d'étendre l'agent Zabbix en utilisant la fonctionnalité `UserParameter`|
|*/var/lib/zabbix/modules*|Le volume permet de charger des modules supplémentaires et d'étendre l'agent Zabbix à l'aide de la fonctionnalité [LoadModule](/fr/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|Le volume est utilisé pour stocker les fichiers liés à TLS. Ces noms de fichiers sont spécifiés à l'aide des variables d'environnement `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` et `ZBX_TLSPSKFILE`|
|**Serveur Zabbix**|<|
|*/usr/lib/zabbix/alertscripts*|Le volume est utilisé pour les scripts d'alerte personnalisés. C'est le paramètre `AlertScriptsPath` dans [zabbix\_server.conf](/fr/manual/appendix/config/zabbix_server)|
|*/usr/lib/zabbix/externalscripts*|Le volume est utilisé par des [vérifications externes](/fr/manual/config/items/itemtypes/external). C'est le paramètre `ExternalScripts` dans [zabbix\_server.conf](/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/modules*|Le volume permet de charger des modules supplémentaires et d'étendre le serveur Zabbix à l'aide de la fonctionnalité [LoadModule](/fr/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|Le volume est utilisé pour stocker les fichiers liés à TLS. Ces noms de fichiers sont spécifiés à l'aide des variables d'environnement `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` et `ZBX_TLSPSKFILE`|
|*/var/lib/zabbix/ssl/certs*|Le volume est utilisé comme emplacement des fichiers de certificat client SSL pour l'authentification du client. C'est le paramètre `SSLCertLocation` dans [zabbix\_server.conf](/fr/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/ssl/keys*|Le volume est utilisé comme emplacement des fichiers de clé privée SSL pour l'authentification du client. C'est le paramètre `SSLKeyLocation` dans [zabbix\_server.conf](/fr/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/ssl/ssl\_ca*|Le volume est utilisé comme emplacement des fichiers d'autorité de certification (CA) pour la vérification du certificat du serveur SSL. C'est le paramètre `SSLCALocation` dans [zabbix\_server.conf](/fr/manual/appendix/config/zabbix_server)|
|*/var/lib/zabbix/snmptraps*|Le volume est utilisé comme emplacement du fichier snmptraps.log. Il peut être partagé par le conteneur zabbix-snmptraps et hérité à l'aide de l'option Docker volumes\_from lors de la création d'une nouvelle instance du serveur Zabbix. La fonctionnalité de traitement des traps SNMP peut être activée en utilisant le volume partagé et en basculant la variable d'environnement `ZBX_ENABLE_SNMP_TRAPS` sur 'true'|
|*/var/lib/zabbix/mibs*|Le volume permet d'ajouter de nouveaux fichiers MIB. Il ne supporte pas les sous-répertoires, toutes les MIBs doivent être placés dans `/var/lib/zabbix/mibs`|
|**Proxy Zabbix**|<|
|*/usr/lib/zabbix/externalscripts*|Le volume est utilisé par des [vérifications externes](/fr/manual/config/items/itemtypes/external). C'est le paramètre `ExternalScripts` dans [zabbix\_proxy.conf](/fr/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/modules*|Le volume permet de charger des modules supplémentaires et d'étendre le serveur Zabbix à l'aide de la fonctionnalité [LoadModule](/fr/manual/config/items/loadablemodules)|
|*/var/lib/zabbix/enc*|Le volume est utilisé pour stocker les fichiers liés à TLS. Ces noms de fichiers sont spécifiés à l'aide des variables d'environnement `ZBX_TLSCAFILE`, `ZBX_TLSCRLFILE`, `ZBX_TLSKEY_FILE` et `ZBX_TLSPSKFILE`|
|*/var/lib/zabbix/ssl/certs*|Le volume est utilisé comme emplacement des fichiers de certificat client SSL pour l'authentification du client. C'est le paramètre `SSLCertLocation` dans [zabbix\_proxy.conf](/fr/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/keys*|Le volume est utilisé comme emplacement des fichiers de clé privée SSL pour l'authentification du client. C'est le paramètre `SSLKeyLocation` dans [zabbix\_proxy.conf](/fr/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/ssl/ssl\_ca*|The volume is used as location of certificate authority (CA) files for SSL server certificate verification. It is the `SSLCALocation` parameter in [zabbix\_proxy.conf](/fr/manual/appendix/config/zabbix_proxy)|
|*/var/lib/zabbix/snmptraps*|Le volume est utilisé comme emplacement du fichier snmptraps.log. Il peut être partagé par le conteneur zabbix-snmptraps et hérité en utilisant l'option Docker volumes\_from lors de la création d'une nouvelle instance du serveur Zabbix. La fonctionnalité de traitement des traps SNMP peut être activée en utilisant le volume partagé et en basculant la variable d'environnement `ZBX_ENABLE_SNMP_TRAPS` sur 'true'|
|*/var/lib/zabbix/mibs*|Le volume permet d'ajouter de nouveaux fichiers MIB. Il ne supporte pas les sous-répertoires, toutes les MIBs doivent être placées dans `/var/lib/zabbix/mibs`|
|**Interface Web Zabbix basée sur un serveur Web Apache2**|<|
|*/etc/ssl/apache2*|Le volume permet d'activer HTTPS pour l'interface web Zabbix. Le volume doit contenir les deux fichiers `ssl.crt` et `ssl.key` préparés pour les connexions SSL Apache2|
|**Interface Web Zabbix basée sur un serveur Web Nginx**|<|
|*/etc/ssl/nginx*|Le volume permet d'activer HTTPS pour l'interface web Zabbix. Le volume doit contenir les deux fichiers `ssl.crt`, `ssl.key` et `dhparam.pem` préparés pour les connexions SSL Nginx|
|**snmptraps Zabbix**|<|
|*/var/lib/zabbix/snmptraps*|Le volume contient le fichier journal snmptraps.log nommé avec les traps SNMP reçues|
|*/var/lib/zabbix/mibs*|Le volume permet d'ajouter de nouveaux fichiers MIB. Il ne supporte pas les sous-répertoires, toutes les MIBs doivent être placées dans `/var/lib/zabbix/mibs`|

Pour plus d'informations, utilisez les dépôts officiels de Zabbix dans
Docker Hub.

[comment]: # ({/new-7d10f7dc})

[comment]: # ({new-492bd3ba})
##### Exemples d'utilisation

\*\* Exemple 1 \*\*

L'exemple démontre comment exécuter une appliance Zabbix construite avec
une base de données MySQL, un serveur Zabbix, une interface Web Zabbix
basée sur une serveur web Nginx et une passerelle Java Zabbix.

    # docker run --name zabbix-appliance -t \
          -p 10051:10051 \
          -p 80:80 \
          -d zabbix/zabbix-appliance:latest

::: noteclassic
L'instance de l'appliance Zabbix expose le port TCP/10051
(trapper Zabbix) et le port TCP/80 (HTTP) à la machine
hôte.
:::

\*\* Exemple 2 \*\*

L'exemple démontre comment exécuter le serveur Zabbix avec prise en
charge de base de données MySQL, l'interface Web Zabbix basée sur le
serveur Web Nginx et la passerelle Java Zabbix.

1\. Démarrez une instance de serveur MySQL vide

    # docker run --name mysql-server -t \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -d mysql:5.7 \
          --character-set-server=utf8 --collation-server=utf8_bin

2\. Démarrez l'instance de passerelle Java Zabbix

    # docker run --name zabbix-java-gateway -t \
          -d zabbix/zabbix-java-gateway:latest

3\. Démarrez l'instance de serveur Zabbix et liez la à l'instance de
serveur MySQL créée

    # docker run --name zabbix-server-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
          --link mysql-server:mysql \
          --link zabbix-java-gateway:zabbix-java-gateway \
          -p 10051:10051 \
          -d zabbix/zabbix-server-mysql:latest

::: noteclassic
L'instance de serveur Zabbix expose le port TCP/10051
(trapper Zabbix) à la machine hôte.
:::

4\. Lancez l'interface Web de Zabbix et liez l'instance avec le serveur
MySQL créé et les instances de serveur Zabbix

    # docker run --name zabbix-web-nginx-mysql -t \
          -e DB_SERVER_HOST="mysql-server" \
          -e MYSQL_DATABASE="zabbix" \
          -e MYSQL_USER="zabbix" \
          -e MYSQL_PASSWORD="zabbix_pwd" \
          -e MYSQL_ROOT_PASSWORD="root_pwd" \
          --link mysql-server:mysql \
          --link zabbix-server-mysql:zabbix-server \
          -p 80:80 \
          -d zabbix/zabbix-web-nginx-mysql:latest

::: noteclassic
L'instance d'interface Web Zabbix expose le port TCP/80
(HTTP) à la machine hôte.
:::

\*\* Exemple 3 \*\*

L'exemple démontre comment exécuter le serveur Zabbix avec la prise en
charge de base de données PostgreSQL, l'interface Web Zabbix basée sur
le serveur Web Nginx et la fonctionnalité de traps SNMP.

1\. Démarrez l'instance de serveur PostgreSQL vide

    # docker run --name postgres-server -t \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix" \
          -e POSTGRES_DB="zabbix_pwd" \
          -d postgres:latest

2\. Démarrez l'instance Zabbix snmptraps

    # docker run --name zabbix-snmptraps -t \
          -v /zbx_instance/snmptraps:/var/lib/zabbix/snmptraps:rw \
          -v /var/lib/zabbix/mibs:/usr/share/snmp/mibs:ro \
          -p 162:162/udp \
          -d zabbix/zabbix-snmptraps:latest

::: noteclassic
L'instance snmptrap de Zabbix expose le port UDP/162(traps
SNMP) à la machine hôte.
:::

3\. Démarrez l'instance de serveur Zabbix et liez la à l'instance de
serveur PostgreSQL créée.

    # docker run --name zabbix-server-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix" \
          -e POSTGRES_DB="zabbix_pwd" \
          -e ZBX_ENABLE_SNMP_TRAPS="true" \
          --link postgres-server:postgres \
          -p 10051:10051 \
          --volumes-from zabbix-snmptraps \
          -d zabbix/zabbix-server-pgsql:latest

::: noteclassic
L'instance de serveur Zabbix expose le port TCP/10051
(trapper Zabbix) à la machine hôte.
:::

4\. Démarrez l'interface Web de Zabbix et liez l'instance avec le
serveur PostgreSQL créé et les instances de serveur Zabbix.

    # docker run --name zabbix-web-nginx-pgsql -t \
          -e DB_SERVER_HOST="postgres-server" \
          -e POSTGRES_USER="zabbix" \
          -e POSTGRES_PASSWORD="zabbix" \
          -e POSTGRES_DB="zabbix_pwd" \
          --link postgres-server:postgres \
          --link zabbix-server-pgsql:zabbix-server \
          -p 443:443 \
          -v /etc/ssl/nginx:/etc/ssl/nginx:ro \
          -d zabbix/zabbix-web-nginx-pgsql:latest

::: noteclassic
L'instance de l'interface Web Zabbix expose le port TCP/443
(HTTPS) à la machine hôte.\
 Le répertoire */etc/ssl/nginx* doit contenir un certificat avec un nom
requis.
:::

[comment]: # ({/new-492bd3ba})

[comment]: # ({new-c443c22e})
### Docker Compose

Zabbix fournit également des fichiers compose pour définir et exécuter
des composants Zabbix multi-conteneurs dans Docker. Ces fichiers compose
sont disponibles dans le dépôt officiel Zabbix-docker sur github.com:
<https://github.com/zabbix/zabbix-docker>. Ces fichiers compose sont
ajoutés à titre d'exemples, ils sont surchargés. Par exemple, ils
contiennent des proxys avec la prise en charge MySQL et SQLite3.

Il existe quelques différentes versions des fichiers compose :

|   |   |
|---|---|
|**Nom de fichier**|**Description**|
|`docker-compose_v3_alpine_mysql_latest.yaml`|Le fichier compose exécute la dernière version des composants Zabbix 4.0 sur Alpine Linux avec base de données MySQL.|
|`docker-compose_v3_alpine_mysql_local.yaml`|Le fichier compose construit localement la dernière version de Zabbix 4.0 et exécute les composants Zabbix sur Alpine Linux avec base de données MySQL.|
|`docker-compose_v3_alpine_pgsql_latest.yaml`|Le fichier compose exécute la dernière version des composants Zabbix 4.0 sur Alpine Linux avec base de données PostgreSQL.|
|`docker-compose_v3_alpine_pgsql_local.yaml`|Le fichier compose construit localement la dernière version de Zabbix 4.0 et exécute les composants Zabbix sur Alpine Linux avec base de données PostgreSQL.|
|`docker-compose_v3_centos_mysql_latest.yaml`|Le fichier compose exécute la dernière version des composants Zabbix 4.0 sur CentOS 7 avec base de données MySQL.|
|`docker-compose_v3_centos_mysql_local.yaml`|Le fichier compose construit localement la dernière version de Zabbix 4.0 et exécute les composants Zabbix sur CentOS 7 avec base de données MySQL.|
|`docker-compose_v3_centos_pgsql_latest.yaml`|Le fichier compose exécute la dernière version des composants Zabbix 4.0 sur sur CentOS 7 avec base de données PostgreSQL.|
|`docker-compose_v3_centos_pgsql_local.yaml`|Le fichier compose construit localement la dernière version de Zabbix 4.0 et exécute les composants Zabbix sur CentOS 7 avec base de données PostgreSQL.|
|`docker-compose_v3_ubuntu_mysql_latest.yaml`|Le fichier compose exécute la dernière version des composants Zabbix 4.0 sur Ubuntu 14.04 avec base de données MySQL.|
|`docker-compose_v3_ubuntu_mysql_local.yaml`|Le fichier compose construit localement la dernière version de Zabbix 4.0 et exécute les composants Zabbix sur Ubuntu 14.04 avec base de données MySQL.|
|`docker-compose_v3_ubuntu_pgsql_latest.yaml`|Le fichier compose exécute la dernière version des composants Zabbix 4.0 sur Ubuntu 14.04 avec base de données PostgreSQL.|
|`docker-compose_v3_ubuntu_pgsql_local.yaml`|Le fichier compose construit localement la dernière version de Zabbix 4.0 et exécute les composants Zabbix sur Ubuntu 14.04 avec base de données PostgreSQL.|

::: noteimportant
Les fichiers compose de Docker disponibles
prennent en charge les versions 2 et 3 de Docker Compose.
:::

[comment]: # ({/new-c443c22e})

[comment]: # ({new-52e39127})
#### Stockage

Les fichiers compose sont configurés pour prendre en charge le stockage
local sur une machine hôte. Docker Compose crée un répertoire `zbx_env`
dans le dossier contenant le fichier compose lorsque vous exécutez des
composants Zabbix à l'aide du fichier compose. Le répertoire contiendra
la même structure que celle décrite ci-dessus dans la section
[Volumes](#Volumes) et le répertoire de stockage de la base de données.

Il existe également des volumes en mode lecture seule pour les fichiers
`/etc/localtime` et `/etc/timezone`.

[comment]: # ({/new-52e39127})

[comment]: # ({new-0be3c140})
#### Fichiers d’environnement

Dans le même répertoire avec les fichiers compose sur github.com vous
pouvez trouver des fichiers avec des variables d'environnement par
défaut pour chaque composant dans le fichier compose. Ces fichiers
d'environnement sont nommés comme `.env_<type de composant>`.

[comment]: # ({/new-0be3c140})

[comment]: # ({new-ad55959c})
#### Exemples

\*\* Exemple 1 \*\*

    # docker-compose -f ./docker-compose_v3_alpine_mysql_latest.yaml up -d

La commande téléchargera les dernières images Zabbix 4.0 pour chaque
composant Zabbix et les exécutera en mode détaché.

::: noteimportant
N'oubliez pas de télécharger les fichiers
`.env_<type de composant>` sur le dépôt officiel Zabbix de github.com
avec les fichiers compose.
:::

\*\* Exemple 2 \*\*

    # docker-compose -f ./docker-compose_v3_ubuntu_mysql_local.yaml up -d

La commande téléchargera l'image de base Ubuntu 14.04, puis construira
localement les composants de Zabbix 4.0 et les exécutera en mode
détaché.

[comment]: # ({/new-ad55959c})
