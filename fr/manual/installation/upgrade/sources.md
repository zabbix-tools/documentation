[comment]: # translation:outdated

[comment]: # ({new-7900383c})
# Mise à jour depuis les sources

[comment]: # ({/new-7900383c})

[comment]: # ({new-92c7a8e5})
#### Aperçu

Cette section fournit les étapes nécessaires à une [mise à
jour](/fr/manual/installation/upgrade) réussie de Zabbix **3.4** vers
Zabbix **4.0**.x en utilisant les sources officielles Zabbix.

Même si la mise à jour des agents Zabbix n'est pas obligatoire (mais
recommandée), le serveur Zabbix et les proxys doivent être de la même
[version majeure](/fr/manual/appendix/compatibility). Par conséquent,
dans une configuration serveur-proxy, le serveur Zabbix et tous les
proxys doivent être arrêtés et mis à jour.

Pour minimiser les temps d'arrêt et la perte de données lors de la mise
à jour, il est recommandé d'arrêter et de mettre à jour le serveur
Zabbix, puis d'arrêter, de mettre à jour et de démarrer les proxys
Zabbix les uns après les autres. Lorsque tous les proxys sont mis à
jour, démarrez le serveur Zabbix. Pendant l'indisponibilité du serveur
Zabbix, les proxys en cours d’exécution continueront à collecter et à
stocker des données et transmettront les données au serveur Zabbix
lorsque le serveur sera de nouveau opérationnel. Les notifications de
problèmes lors de l'arrêt du serveur Zabbix ne seront générées qu'après
le démarrage du serveur mis à jour.

::: noteimportant
Il est possible de mettre à jour le serveur
uniquement et d'avoir des proxys plus anciens, non mis à jour, qui
rapportent des données à un serveur plus récent (les proxys ne peuvent
cependant pas actualiser leur configuration). Cette approche, cependant,
n'est pas recommandée et n'est pas supportée par Zabbix et ce choix est
entièrement à vos risques et périls.
:::

Notez qu'avec la base de données SQLite sur les proxys, les données
d'historique des proxys avant la mise à jour seront perdues, car la mise
à jour de base de données SQLite n'est pas prise en charge et le fichier
de base de données SQLite doit être supprimé manuellement. Lorsque le
proxy est démarré pour la première fois et que le fichier de base de
données SQLite est manquant, le proxy le crée automatiquement.

Selon la taille de la base de données, la mise à jour de la base de
données vers la version 4.0 peut prendre beaucoup de temps.

::: notewarning
Avant la mise à jour, assurez-vous de lire les \*\*
notes de mise à jour pertinentes ! \*\*
:::

Les notes de mise à jour suivantes sont disponibles :

|Mise à jour depuis|Lire les notes de mise à jour|Notes importantes/changements entre les versions|
|-------------------|------------------------------|------------------------------------------------|
|3.4.x|[Pour 4.0](/manual/installation/upgrade_notes_400)|Les bibliothèques 'libpthread' et 'zlib' sont maintenant obligatoires;<br>Prise en charge du protocole de texte brut supprimée et l'en-tête est obligatoire.;<br>Les agents Zabbix de version antérieure à la version 1.4 ne sont plus supporté;<br>Le paramètre Serveur dans la configuration du proxy passif est maintenant obligatoire|
|3.2.x|[Pour 3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)|Support SQLite en tant que base de données abandonné pour le serveur et l’interface Web Zabbix;<br>Perl Compatible Regular Expressions (PCRE) supporté au lieu de POSIX étendu;<br>bibliothèques 'libpcre' et 'libevent' obligatoires pour le serveur Zabbix;<br>Vérifications de code de sortie ajoutées pour les paramètres utilisateur, les commandes distantes et les éléments system.run\[\] sans l'indicateur 'nowait' ainsi que les scripts exécutés par le serveur Zabbix;;<br>La passerelle Java Zabbix doit être mise à jour pour prendre en charge de nouvelles fonctionnalités|
|3.0.x|[pour 3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)|La mise à jour de la base de données peut être lente, en fonction de la taille de la table d'historique|
|2.4.x|[pour 3.0](https://www.zabbix.com/documentation/3.0/manual/installation/upgrade_notes_300)|La version minimale requise de PHP est passée de 5.3.0 à 5.4.0<br>Le paramètre de l'agent LogFile doit être spécifié|
|2.2.x|[pour 2.4](https://www.zabbix.com/documentation/2.4/manual/installation/upgrade_notes_240)|Suppression de la surveillance distribuée basée sur un nœud|
|2.0.x|[pour 2.2](https://www.zabbix.com/documentation/2.2/manual/installation/upgrade_notes_220)|La version minimale requise de PHP est passée de 5.1.6 à 5.3.0;<br>Une base de données MySQL sensible à la casse est nécessaire pour le bon fonctionnement du serveur; l'extension PHP 'mysqli' est requise au lieu de 'mysql'|

Vous pouvez également vérifier les
[pré-requis](/fr/manual/installation/requirements) pour la version 4.0.

::: notetip
Il peut être pratique d'exécuter deux sessions SSH
parallèles pendant la mise à jour, en exécutant les étapes de mise à
jour dans la première et en surveillant les journaux de serveur/proxy
dans la seconde. Par exemple, exécutez `tail -f zabbix_server.log` ou
`tail -f zabbix_proxy.log` dans la deuxième session SSH en affichant les
dernières entrées du fichier journal et les erreurs possibles en temps
réel. Cela peut être critique pour les instances de
production.
:::

[comment]: # ({/new-92c7a8e5})

[comment]: # ({new-101d6faf})
#### Procédure de mise à jour du serveur

[comment]: # ({/new-101d6faf})

[comment]: # ({new-b8308740})
##### 1 Arrêt du serveur Zabbix

Arrêter le serveur Zabbix pour être sûr qu’aucune nouvelle donnée soit
insérée en base de données.

[comment]: # ({/new-b8308740})

[comment]: # ({new-ab13a6a4})
##### 2 Sauvegarde de la base de données Zabbix existante

Il s’agit d’une étape très importante. Vérifiez que vous disposez d’une
sauvegarde de votre base de données. Cela aidera si la procédure de mise
à jour échoue (manque d’espace disque, arrêt inopiné ou tout autre
problème imprévu).

[comment]: # ({/new-ab13a6a4})

[comment]: # ({new-d2778675})
##### 3 Sauvegarde des fichiers de configuration, des fichiers PHP et des binaires Zabbix

Faites une copie de sauvegarde des binaires Zabbix, des fichiers de
configuration et du répertoire des fichiers PHP.

[comment]: # ({/new-d2778675})

[comment]: # ({new-2bb75ddd})
##### 4 Installation des nouveaux binaires du serveur

Utilisez les
[instructions](/fr/manual/installation/install#installing_zabbix_daemons)
pour compiler le serveur Zabbix depuis les sources.

[comment]: # ({/new-2bb75ddd})

[comment]: # ({new-ec6edae7})
##### 5 Examen des paramètres de configuration du serveur

Voir les notes de mise à jour pour les détails sur les [changements
obligatoires](/fr/manual/installation/upgrade_notes_400#configuration_parameters).

Pour les paramètres optionnels, voir la section
[Nouveautés](/fr/manual/introduction/whatsnew400#configuration_parameters).

[comment]: # ({/new-ec6edae7})

[comment]: # ({new-a01e967f})
##### 6 Démarrer les nouveaux binaires Zabbix

Démarrez les nouveaux binaires. Vérifiez les fichiers de logs pour voir
si les fichiers binaires ont démarré avec succès.

Le serveur Zabbix mettra automatiquement à jour la base de données. Au
démarrage, le serveur Zabbix signale les versions de base de données
actuelles (obligatoires et facultatives) et requises. Si la version
actuelle est antérieure à la version requise, le serveur Zabbix exécute
automatiquement les correctifs requis de mise à jour de base de données.
La progression (en pourcentage) de la mise à jour de la base de données
est écrite dans le fichier de log du serveur Zabbix. Lorsque la mise à
jour est terminée, un message «Mise à jour de la base de données
entièrement terminée» est écrit dans le fichier de logs. Si l'un des
correctifs de mise à jour échoue, le serveur Zabbix ne démarre pas. Le
serveur Zabbix ne démarrera pas non plus si la version de base de
données actuelle est plus récente que celle requise. Le serveur Zabbix
démarre uniquement si la version de base de données obligatoire actuelle
correspond à la version obligatoire requise.

    8673:20161117:104750.259 current database version (mandatory/optional): 03040000/03040000
    8673:20161117:104750.259 required mandatory version: 03040000

Avant de démarrer le serveur :

-   Assurez-vous que l'utilisateur de la base de données dispose des
    autorisations nécessaires (créer une table, supprimer une table,
    créer un index, supprimer un index)
-   Assurez-vous d'avoir suffisamment d'espace disque libre.

[comment]: # ({/new-a01e967f})

[comment]: # ({new-22e0e4e9})
##### 7 Installation de la nouvelle interface Web Zabbix

La version minimum requise de PHP est 5.4.0. Mettez à jour si nécessaire
et suivez les [instructions
d’installation](/fr/manual/installation/install#installing_zabbix_web_interface).

[comment]: # ({/new-22e0e4e9})

[comment]: # ({new-49e4f43e})
##### 8 Effacer les cookies et le cache du navigateur

Après la mise à jour, vous devrez peut-être effacer les cookies et le
cache du navigateur Web pour que l'interface Web de Zabbix fonctionne
correctement.

[comment]: # ({/new-49e4f43e})

[comment]: # ({new-75f5b3ed})
#### Procédure de mise à jour du Proxy

[comment]: # ({/new-75f5b3ed})

[comment]: # ({new-f33e6ddb})
##### 1 Arrêt du proxy Zabbix

Arrêtez le proxy Zabbix.

[comment]: # ({/new-f33e6ddb})

[comment]: # ({new-2e03c550})
##### 2 Sauvegarde des fichiers de configuration et des binaires du proxy Zabbix

Faites une copie de sauvegarde des binaires du proxy Zabbix et des
fichiers de configuration.

[comment]: # ({/new-2e03c550})

[comment]: # ({new-024201dc})
##### 3 Installation des nouveaux binaires

Utilisez les
[instructions](/fr/manual/installation/install#installing_zabbix_daemons)
pour compiler le proxy Zabbix depuis les sources.

[comment]: # ({/new-024201dc})

[comment]: # ({new-2fbda571})
##### 4 Examen des paramètres de configuration du proxy

Il n’y a pas de changements obligatoires dans cette version pour les
[paramètres](/fr/manual/appendix/config/zabbix_proxy) du proxy. Pour les
paramètres optionnels, voir la section
[Nouveautés](/fr/manual/introduction/whatsnew400#configuration_parameters).

[comment]: # ({/new-2fbda571})

[comment]: # ({new-520f43c6})
##### 5 Démarrage du nouveau proxy Zabbix

Démarrez le nouveau proxy Zabbix. Vérifiez les fichiers de logs pour
voir si le proxy a démarré correctement.

Zabbix mettra automatiquement à jour la base de données. La mise à jour
de la base de données a lieu de la même manière que lors du démarrage du
[serveur
Zabbix](/fr/manual/installation/upgrade#start_new_zabbix_binaries).

[comment]: # ({/new-520f43c6})

[comment]: # ({new-59736bba})
#### Procédure de mise à jour de l’agent Zabbix

::: noteimportant
La mise à jour des agents n’est pas obligatoire.
Vous n'avez besoin de mettre à jour les agents que si vous devez accéder
à de nouvelles fonctionnalités.
:::

[comment]: # ({/new-59736bba})

[comment]: # ({new-02698e69})
##### 1 Arrêt de l’agent Zabbix

Arrêter l’agent Zabbix.

[comment]: # ({/new-02698e69})

[comment]: # ({new-50263823})
##### 2 Sauvegarde des fichiers de configuration et des binaires de l’agent Zabbix

Faites une copie de sauvegarde des binaires de l’agent Zabbix et des
fichiers de configuration.

[comment]: # ({/new-50263823})

[comment]: # ({new-467f56b3})
##### 3 Installation des nouveaux binaires d’agent

Utilisez les
[instructions](/fr/manual/installation/install#installing_zabbix_daemons)
pour compiler l’agent Zabbix depuis les sources.

Sinon, vous pouvez télécharger un agent pré-compilé depuis la [page de
téléchargement Zabbix](http://www.zabbix.com/download.php).

[comment]: # ({/new-467f56b3})

[comment]: # ({new-34721aad})
##### 4 Examen des paramètres de configuration de l’agent

Il n’y a pas de changements obligatoires dans cette version pour les
[paramètres](/fr/manual/appendix/config/zabbix_agentd) d’agent.

[comment]: # ({/new-34721aad})

[comment]: # ({new-04c253a7})
##### 5 Démarrage du nouvel agent Zabbix

Démarrez le nouvel agent Zabbix. Vérifier les fichiers de logs pour
vérifier que l’agent a démarré correctement.

[comment]: # ({/new-04c253a7})

[comment]: # ({new-a06019d2})
##### Mise à jour entre les versions mineures

Lors de la mise à jour entre les versions mineures de 4.0.x (par exemple
de 4.0.1 à 4.0.3), il est nécessaire d'exécuter les mêmes actions pour
le serveur/proxy/agent, que lors de la mise à jour entre les versions
majeures. La seule différence est que lors de la mise à jour entre les
versions mineures, aucune modification de la base de données n'est
effectuée.

[comment]: # ({/new-a06019d2})
