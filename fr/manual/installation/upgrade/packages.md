[comment]: # translation:outdated

[comment]: # ({new-490a37c2})
# Mise à jour depuis les packages

[comment]: # ({/new-490a37c2})

[comment]: # ({new-0fa07e9f})
#### Aperçu

Cette section fournit les étapes nécessaires à une [mise à
jour](/fr/manual/installation/upgrade) réussie en utilisant les packages
officiels RPM et DEB fournis par Zabbix pour :

-   [Red Hat Enterprise
    Linux/CentOS](/fr/manual/installation/upgrade/packages/rhel_centos)
-   [Debian/Ubuntu](/fr/manual/installation/upgrade/packages/debian_ubuntu)

[comment]: # ({/new-0fa07e9f})

[comment]: # ({new-69a2013d})
##### Zabbix packages from OS repositories

Often, OS distributions (in particular, Debian-based distributions)
provide their own Zabbix packages.\
Note, that these packages are not supported by Zabbix, they are
typically out of date and lack the latest features and bug fixes. Only
the packages from [repo.zabbix.com](https://repo.zabbix.com/) are
officially supported.

If you are upgrading from packages provided by OS distributions (or had
them installed at some point), follow this procedure to switch to
official Zabbix packages:

1.  Always uninstall the old packages first.
2.  Check for residual files that may have been left after
    deinstallation.
3.  Install official packages following [installation
    instructions](https://www.zabbix.com/download) provided by Zabbix.

Never do a direct update, as this may result in a broken installation.

[comment]: # ({/new-69a2013d})
