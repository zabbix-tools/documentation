<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/installation/upgrade/packages/rhel.md">
    <body>
      <trans-unit id="83c2fbfb" xml:space="preserve">
        <source># 1 Red Hat Enterprise Linux</source>
      </trans-unit>
      <trans-unit id="f5a2912b" xml:space="preserve">
        <source>#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **6.4**.x to Zabbix
**7.0**.x using official Zabbix packages for Red Hat Enterprise
Linux.

::: notewarning
Before the upgrade make sure to read the relevant
[**upgrade notes**](/manual/installation/upgrade)!
:::

You may also want to check the
[requirements](/manual/installation/requirements) for 7.0.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::</source>
      </trans-unit>
      <trans-unit id="93ff8b03" xml:space="preserve">
        <source>#### Upgrade procedure</source>
      </trans-unit>
      <trans-unit id="165cddb5" xml:space="preserve">
        <source>##### 1 Stop Zabbix processes

Stop Zabbix server to make sure that no new data is inserted into
database.

    systemctl stop zabbix-server

If upgrading the proxy, stop proxy too.

    systemctl stop zabbix-proxy</source>
      </trans-unit>
      <trans-unit id="ab13a6a4" xml:space="preserve">
        <source>##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).</source>
      </trans-unit>
      <trans-unit id="bddc3d2b" xml:space="preserve">
        <source>##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

Configuration files:

    mkdir /opt/zabbix-backup/
    cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    cp /etc/httpd/conf.d/zabbix.conf  /opt/zabbix-backup/

PHP files and Zabbix binaries:

    cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    cp -R /usr/share/zabbix-* /opt/zabbix-backup/</source>
      </trans-unit>
      <trans-unit id="f9da6e1f" xml:space="preserve">
        <source>##### 4 Update repository configuration package

Before proceeding with the upgrade, update your current repository package. On RHEL 9, run:

    rpm -Uvh https://repo.zabbix.com/zabbix/6.5/rhel/9/x86_64/zabbix-release-6.5-1.el9.noarch.rpm

For older RHEL versions, replace the link above with the correct one from [Zabbix repository](https://repo.zabbix.com/zabbix/6.5/rhel/).</source>
      </trans-unit>
      <trans-unit id="9524156f" xml:space="preserve">
        <source>##### 5 Upgrade Zabbix components

To upgrade Zabbix components you may run something like:

    dnf upgrade zabbix-server-mysql zabbix-web-mysql zabbix-agent

If using PostgreSQL, substitute `mysql` with `pgsql` in the command. If
upgrading the proxy, substitute `server` with `proxy` in the command. If
upgrading the agent 2, substitute `zabbix-agent` with `zabbix-agent2` in
the command.

To upgrade the web frontend with Apache **on RHEL 8** correctly, also
run:

    dnf install zabbix-apache-conf </source>
      </trans-unit>
      <trans-unit id="f25076e1" xml:space="preserve">
        <source>##### 6 Review component configuration parameters

Make sure to review [Upgrade notes](/manual/installation/upgrade_notes_700) to check if any changes in the configuration parameters are required.

For new optional parameters, see the [What's new](/manual/introduction/whatsnew700) page.</source>
      </trans-unit>
      <trans-unit id="3190fa72" xml:space="preserve">
        <source>##### 7 Start Zabbix processes

Start the updated Zabbix components.

    systemctl start zabbix-server
    systemctl start zabbix-proxy
    systemctl start zabbix-agent
    systemctl start zabbix-agent2</source>
      </trans-unit>
      <trans-unit id="49e4f43e" xml:space="preserve">
        <source>##### 8 Clear web browser cookies and cache

You may need to clear web browser cookies and web
browser cache after the upgrade for Zabbix web interface to work properly.</source>
      </trans-unit>
      <trans-unit id="ad85b70b" xml:space="preserve">
        <source>#### Upgrade between minor versions

It is possible to upgrade between minor versions of 7.0.x (for example,
from 7.0.1 to 7.0.3). Upgrading between minor versions is easy.

To execute Zabbix minor version upgrade it is required to run:

    sudo dnf upgrade 'zabbix-*'

To execute Zabbix server minor version upgrade run:

    sudo dnf upgrade 'zabbix-server-*'

To execute Zabbix agent minor version upgrade run:

    sudo dnf upgrade 'zabbix-agent-*'

or, for Zabbix agent 2:

    sudo dnf upgrade 'zabbix-agent2-*'</source>
      </trans-unit>
    </body>
  </file>
</xliff>
