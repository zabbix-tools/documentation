[comment]: # translation:outdated

[comment]: # ({new-91aab3e5})
# 2 Debian/Ubuntu

[comment]: # ({/new-91aab3e5})

[comment]: # ({new-d02acea9})
#### Aperçu

Cette section fournit les étapes nécessaires à une [mise à
jour](/manual/installation/upgrade) réussie de Zabbix **3.4** vers
Zabbix **4.0.x** en utilisant les packages Zabbix officiels pour
Debian/Ubuntu.

Même si la mise à jour des agents Zabbix n'est pas obligatoire (mais
recommandée), le serveur Zabbix et les proxys doivent être de la [même
version majeure](/manual/appendix/compatibility). Par conséquent, dans
une configuration serveur-proxy, le serveur Zabbix et tous les proxys
doivent être arrêtés et mis à jour.

Pour minimiser les temps d'arrêt et la perte de données lors de la mise
à jour, il est recommandé d'arrêter et de mettre à jour le serveur
Zabbix, puis d'arrêter, de mettre à jour et de démarrer les proxys
Zabbix les uns après les autres. Lorsque tous les proxys sont mis à
jour, démarrez le serveur Zabbix. Pendant l'indisponibilité du serveur
Zabbix, les proxys en cours d’exécution continueront à collecter et à
stocker des données et transmettront les données au serveur Zabbix
lorsque le serveur sera de nouveau opérationnel. Les notifications de
problèmes lors de l'arrêt du serveur Zabbix ne seront générées qu'après
le démarrage du serveur à jour.

Notez qu'avec la base de données SQLite sur les proxys, les données
d'historique des proxys avant la mise à jour seront perdues, car la mise
à jour de base de données SQLite n'est pas prise en charge et le fichier
de base de données SQLite doit être supprimé manuellement. Lorsque le
proxy est démarré pour la première fois et que le fichier de base de
données SQLite est manquant, le proxy le crée automatiquement.

Selon la taille de la base de données, la mise à jour de la base de
données vers la version 4.0 peut prendre beaucoup de temps.

::: notewarning
Avant la mise à jour, assurez-vous de lire les notes
de **mise à jour pertinentes** !
:::

Les notes de mise à jour suivantes sont disponibles :

|Mise à jour depuis|Lire les notes de mise à jour|Notes importantes/changements entre les versions|
|-------------------|------------------------------|------------------------------------------------|
|3.4.x|[Pour 4.0](/manual/installation/upgrade_notes_400)|Les bibliothèques 'libpthread' et 'zlib' sont maintenant obligatoires;<br>Prise en charge du protocole de texte brut supprimée et l'en-tête est obligatoire.;<br>Les agents Zabbix de version antérieure à la version 1.4 ne sont plus supporté;<br>Le paramètre Serveur dans la configuration du proxy passif est maintenant obligatoire|
|3.2.x|[Pour 3.4](https://www.zabbix.com/documentation/3.4/manual/installation/upgrade_notes_340)|Support SQLite en tant que base de données abandonné pour le serveur et l’interface Web Zabbix;<br>Perl Compatible Regular Expressions (PCRE) supporté au lieu de POSIX étendu;<br>bibliothèques 'libpcre' et 'libevent' obligatoires pour le serveur Zabbix;<br>Vérifications de code de sortie ajoutées pour les paramètres utilisateur, les commandes distantes et les éléments system.run\[\] sans l'indicateur 'nowait' ainsi que les scripts exécutés par le serveur Zabbix ;<br>La passerelle Java Zabbix doit être mise à jour pour prendre en charge de nouvelles fonctionnalités|
|3.0.x|[for 3.2](https://www.zabbix.com/documentation/3.2/manual/installation/upgrade_notes_320)|La mise à jour de la base de données peut être lente, en fonction de la taille de la table d'historique|

Vous pouvez également vérifier les
[pré-requis](/fr/manual/installation/requirements) pour la version 4.0.

::: notetip
Il peut être pratique d'exécuter deux sessions SSH
parallèles pendant la mise à jour, en exécutant les étapes de mise à
jour dans la première et en surveillant les journaux de serveur/proxy
dans la seconde. Par exemple, exécutez `tail -f zabbix_server.log` ou
`tail -f zabbix_proxy.log` dans la deuxième session SSH en affichant les
dernières entrées du fichier journal et les erreurs possibles en temps
réel. Cela peut être critique pour les instances de
production.
:::

[comment]: # ({/new-d02acea9})

[comment]: # ({new-93ff8b03})
#### Procédure de mise à jour

[comment]: # ({/new-93ff8b03})

[comment]: # ({new-f8102233})
##### 1 Arrêt des processus Zabbix

Arrêtez le serveur Zabbix pour être sûr qu’aucune donnée ne soit insérée
en base de données.

    # service zabbix-server stop

Si vous mettez à jour un proxy Zabbix arrêtez-le également.

    # service zabbix-proxy stop

::: noteimportant
Il est possible de mettre à jour le serveur
uniquement et d'avoir des proxys plus anciens, non mis à jour, qui
rapportent des données à un serveur plus récent (les proxys ne peuvent
cependant pas actualiser leur configuration). Cette approche, cependant,
n'est pas recommandée et n'est pas supportée par Zabbix et ce choix est
entièrement à vos risques et périls
:::

[comment]: # ({/new-f8102233})

[comment]: # ({new-ab13a6a4})
##### 2 Sauvegarde de la base de données Zabbix existante

Il s’agit d’une étape très importante. Assurez-vous d’avoir une
sauvegarde de votre base de données. Cela aidera si la procédure de mise
à jour échoue (manque d'espace disque, arrêt inopiné, tout autre
problème inattendu).

[comment]: # ({/new-ab13a6a4})

[comment]: # ({new-6c141723})
##### 3 Sauvegarde des fichiers de configuration, des fichiers PHP et des binaires Zabbix

Faites une copie de sauvegarde des binaires Zabbix, des fichiers de
configuration et des répertoires contenant les fichiers PHP.

Fichiers de configuration :

    # mkdir /opt/zabbix-backup/
    # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    # cp /etc/apache2/conf-enabled/zabbix.conf /opt/zabbix-backup/

Fichiers PHP et binaires Zabbix :

    # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    # cp -R /usr/share/doc/zabbix-* /opt/zabbix-backup/

[comment]: # ({/new-6c141723})

[comment]: # ({new-31c75b55})
##### 4 Mise à jour du package de configuration du dépôt

Pour procéder à la mise à jour, votre dépôt de package actuel doit être
désinstallé.

    # rm -Rf /etc/apt/sources.list.d/zabbix.list

Ensuite, installez les nouveau package de configuration du dépôt.

Sur **Debian 9** exécutez :

    # wget http://repo.zabbix.com/zabbix/3.5/debian/pool/main/z/zabbix-release/zabbix-release_3.5-1+stretch_all.deb
    # dpkg -i zabbix-release_3.5-1+stretch_all.deb

Sur **Debian 8** exécutez :

    # wget http://repo.zabbix.com/zabbix/3.5/debian/pool/main/z/zabbix-release/zabbix-release_3.5-1+jessie_all.deb
    # dpkg -i zabbix-release_3.5-1+jessie_all.deb

Sur **Debian 7** exécutez :

    # wget http://repo.zabbix.com/zabbix/3.5/debian/pool/main/z/zabbix-release/zabbix-release_3.5-1+wheezy_all.deb
    # dpkg -i zabbix-release_3.5-1+wheezy_all.deb

Sur **Ubuntu 18.04** exécutez :

    # wget http://repo.zabbix.com/zabbix/3.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.5-1+bionic_all.deb
    # dpkg -i zabbix-release_3.5-1+bionic_all.deb

Sur **Ubuntu 16.04** exécutez :

    # wget http://repo.zabbix.com/zabbix/3.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.5-1+xenial_all.deb
    # dpkg -i zabbix-release_3.5-1+xenial_all.deb

Sur **Ubuntu 14.04** exécutez :

    # wget http://repo.zabbix.com/zabbix/3.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.5-1+trusty_all.deb
    # dpkg -i zabbix-release_3.5-1+trusty_all.deb

Mettez à jour les informations du dépôt.

    # apt-get update

[comment]: # ({/new-31c75b55})

[comment]: # ({new-08c7383c})
##### 5 Mise à jour des composants Zabbix

Pour mettre à jour les composants Zabbix, vous pouvez exécuter quelque
chose comme :

    # apt-get install --only-upgrade zabbix-server-mysql zabbix-frontend-php zabbix-agent

Si vous utilisez PostgreSQL, remplacez `mysql` par `pgsql` dans la
commande. Si vous mettez à jour le proxy,remplacez `server` par `proxy`
dans la commande.

[comment]: # ({/new-08c7383c})

[comment]: # ({new-455f4e97})
##### 6 Examiner les paramètres de configuration du composant

Voir les notes de mise à jour pour plus de détails sur les
[modifications
obligatoires](/fr/manual/installation/upgrade_notes_400#configuration_parameters).
  Pour les nouveaux paramètres optionnels, voir la section
[Nouveautés](/fr/manual/introduction/whatsnew400#configuration_parameters).

[comment]: # ({/new-455f4e97})

[comment]: # ({new-624a8fc7})
##### 7 Démarrage des processus Zabbix

Démarrez les composants Zabbix à jour :

    # service zabbix-server start
    # service zabbix-proxy start
    # service zabbix-agent start

[comment]: # ({/new-624a8fc7})

[comment]: # ({new-49e4f43e})
##### 8 Effacer les cookies et le cache du navigateur

Après la mise à jour, vous devrez peut-être effacer les cookies et le
cache du navigateur Web pour que l'interface Web de Zabbix fonctionne
correctement.

[comment]: # ({/new-49e4f43e})

[comment]: # ({new-9bab02f2})
#### Mise à jour entre versions mineures

Il est possible de mettre à jour entre des versions mineures de 4.0.x
(par exemple, de 4.0.1 vers 4.0.3). La mise à jour entre les versions
mineure est très facile.

Pour exécuter la mise à jour de version mineure de Zabbix, il faut
exécuter :

    # sudo apt-get install --only-upgrade zabbix.

Pour exécuter la mise à jour de version mineure du serveur Zabbix, il
faut exécuter :

    # sudo apt-get install --only-upgrade zabbix-server.

Pour exécuter la mise à jour de version mineure de l’agent Zabbix, il
faut exécuter :

    # sudo apt-get install --only-upgrade zabbix-agent.

[comment]: # ({/new-9bab02f2})
