[comment]: # translation:outdated

[comment]: # ({new-8d986b3b})
# 1 Obtenir Zabbix

[comment]: # ({/new-8d986b3b})

[comment]: # ({new-74ef7c01})
#### Aperçu

Il existe quatre façons d'obtenir Zabbix :

-   L'installer à partir des [packages de
    distribution](install_from_packages#From_distribution_packages)
-   Télécharger la dernière archive source et la [compiler
    soi-même](install#Installation_from_sources)
-   L'installer à partir de [conteneurs](containers)
-   Télécharger l'[appliance virtuelle](/fr/manual/appliance)

Pour télécharger les dernières sources ou l'appliance virtuelle, accédez
à la [page de téléchargement
Zabbix](http://www.zabbix.com/download.php), où des liens directs vers
les dernières versions sont fournis. Pour télécharger des versions
antérieures, consultez le lien de téléchargements précédent contenant
les versions stables.

[comment]: # ({/new-74ef7c01})

[comment]: # ({new-e88f42af})
#### Getting Zabbix source code

There are several ways of getting Zabbix source code:

-   You can [download](https://www.zabbix.com/download_sources) the
    released stable versions from the official Zabbix website
-   You can [download](https://www.zabbix.com/developers) nightly builds
    from the official Zabbix website developer page
-   You can get the latest development version from the Git source code
    repository system:
    -   The primary location of the full repository is at
        <https://git.zabbix.com/scm/zbx/zabbix.git>
    -   Master and supported releases are also mirrored to Github at
        <https://github.com/zabbix/zabbix>

A Git client must be installed to clone the repository. The official
commandline Git client package is commonly called **git** in
distributions. To install, for example, on Debian/Ubuntu, run:

    sudo apt-get update
    sudo apt-get install git

To grab all Zabbix source, change to the directory you want to place the
code in and execute:

    git clone https://git.zabbix.com/scm/zbx/zabbix.git

[comment]: # ({/new-e88f42af})
