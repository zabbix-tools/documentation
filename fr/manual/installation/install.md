[comment]: # translation:outdated

[comment]: # ({new-af2eaf2f})
# 3 Installation depuis les sources

Vous pouvez obtenir la dernière version de Zabbix en le compilant à
partir des sources.

Un didacticiel étape par étape pour l'installation de Zabbix à partir
des sources est fourni ici.

[comment]: # ({/new-af2eaf2f})

[comment]: # ({new-0040d992})
#### - Installation des démons Zabbix

[comment]: # ({/new-0040d992})

[comment]: # ({new-8cc2c1b1})
##### 1 Télécharger l'archive contenant les sources

Rendez-vous sur la [page de téléchargement de
Zabbix](http://www.zabbix.com/download_sources) et téléchargez l'archive
contenant les sources. Une fois téléchargée, extraire les sources en
exécutant :

    $ tar -zxvf zabbix-5.2.0.tar.gz

::: notetip
Renseigner la version de Zabbix correspondante dans la
commande. Cela doit correspondre au nom de l'archive
téléchargée.
:::

[comment]: # ({/new-8cc2c1b1})

[comment]: # ({new-87e1101d})
##### 2 Créez un compte utilisateur

Pour tous les processus démons Zabbix, un utilisateur sans privilège est
nécessaire. Si un démon Zabbix est démarré avec un utilisateur sans
privilège, il s'exécutera en tant que cet utilisateur.

Cependant, si un démon est démarré à partir d'un compte 'root', il
passera à un compte utilisateur 'zabbix', qui doit être présent. Pour
créer un tel compte utilisateur (dans son propre groupe, "zabbix") sur
les systèmes Linux, lancez :

    groupadd zabbix
    useradd -g zabbix zabbix

Un compte utilisateur distinct n'est pas nécessaire pour installation de
l’interface utilisateur de Zabbix.

Si le [serveur](/fr/manual/concepts/server) et
l'[agent](/fr/manual/concepts/agent) Zabbix sont exécutés sur la même
machine, il est recommandé d'utiliser un autre utilisateur pour exécuter
le serveur que pour exécuter l'agent. Sinon, si les deux sont exécutés
sous le même utilisateur, l'agent peut accéder au fichier de
configuration du serveur et tout utilisateur de niveau Admin dans Zabbix
peut récupérer très facilement, par exemple, le mot de passe de la base
de données.

::: noteimportant
L'exécution de Zabbix en tant que root, bin ou
tout autre compte avec des droits spéciaux constitue un risque de
sécurité.
:::

[comment]: # ({/new-87e1101d})

[comment]: # ({new-06256db0})
##### 3 Créez la base de données Zabbix

Pour les démons [serveur](/fr/manual/concepts/server) et
[proxy](/fr/manual/concepts/proxy), tout comme pour l'interface Web, une
base de données est nécessaire. Cela n’est pas nécessaire pour
l'[agent](/fr/manual/concepts/agent) Zabbix.

Des [scripts SQL sont fournis](/fr/manual/appendix/install/db_scripts)
pour créer le schéma de base de données et insérer l'ensemble de
données. La base de données du proxy Zabbix n'a besoin que du schéma
tandis que la base de données du serveur Zabbix requiert également
l'ensemble de données au-dessus du schéma.

Après avoir créé la base de données Zabbix, passez aux étapes suivantes
de la compilation de Zabbix.

[comment]: # ({/new-06256db0})

[comment]: # ({new-8ff1b41c})
##### 4 Configurez les sources

Lors de la configuration des sources pour un serveur ou un proxy Zabbix,
vous devez spécifier le type de base de données à utiliser. Un seul type
de base de données peut être compilé avec un serveur ou un processus
proxy à la fois.

Pour voir toutes les options de configuration prises en charge, depuis
le répertoire des sources Zabbix extrait, exécutez :

    ./configure --help

Pour configurer les sources pour l'agent et le serveur Zabbix, vous
devrez exécuter quelque chose comme :

    ./configure --enable-server --enable-agent --with-mysql --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2

::: noteclassic
L’option de configuration --with-libcurl avec cURL 7.20.0 ou
supérieur est nécessaire pour l'authentification SMTP, pris en charge
depuis la version 3.0.0 de Zabbix.\
Les options de configuration --with-libcurl et --with-libxml2 sont
nécessaires pour superviser une machine virtuelle, pris en charge depuis
la version 2.2.0 de Zabbix.
:::

::: noteimportant
Depuis la version 3.4, Zabbix sera toujours
compilé avec la bibliothèque PCRE; l'installer n'est pas optionnel.
--with-libpcre=\[DIR\] permet uniquement de pointer vers un répertoire
spécifique d'installation, au lieu de rechercher un certain nombre
d'emplacements communs pour les fichiers libpcre.
:::

Pour configurer les sources pour un serveur Zabbix (avec PostgreSQL
etc.), vous devrez exécuter :

    ./configure --enable-server --with-postgresql --with-net-snmp

Pour configurer les sources pour un proxy Zabbix (avec SQLite etc.),
vous devrez exécuter :

    ./configure --prefix=/usr --enable-proxy --with-net-snmp --with-sqlite3 --with-ssh2

Pour configurer les sources pour un agent Zabbix, vous devrez exécuter :

    ./configure --enable-agent

Vous pouvez utiliser le flag --enable-static pour lier statiquement les
bibliothèques. Si vous envisagez de distribuer des fichiers binaires
compilés entre différents serveurs, vous devez utiliser ce flag pour que
ces fichiers binaires fonctionnent sans les bibliothèques requises.
Notez que --enable-static [ne fonctionne pas sous
Solaris](http://blogs.sun.com/rie/entry/static_linking_where_did_it).

::: noteimportant
 Utiliser l'option --enable-static n'est pas
recommandé lors de la construction du serveur.\
Afin de construire le serveur de manière statique, vous devez avoir une
version statique de chaque bibliothèque externe nécessaire. Il n'y a pas
de vérification stricte pour cela dans le script de configuration.

:::

::: noteclassic
Les utilitaires de ligne de commande zabbix\_get et
zabbix\_sender sont compilés si l'option --enable-agent est
utilisée.
:::

::: noteclassic
 Ajoutez un chemin optionnel vers le fichier de
configuration MySQL
--with-mysql=/<path\_to\_the\_file>/mysql\_config pour
sélectionner la bibliothèque client MySQL souhaitée quand il est
nécessaire d'en utiliser une qui ne se trouve pas dans l'emplacement par
défaut.

C'est utile quand plusieurs versions de MySQL sont installées ou que
MariaDB est installé avec MySQL sur le même système.
:::

::: noteclassic
Utilisez l’option --with-ibm-db2 pour spécifier
l'emplacement de l'API CLI.\\\\Utilisez l'option --with-oracle pour
spécifier l'emplacement de l'API OCI.
:::

Pour la prise en charge du chiffrement, voir [Compilation Zabbix avec
prise en charge du
chiffrement](/fr/manual/encryption#compiling_zabbix_with_encryption_support).

[comment]: # ({/new-8ff1b41c})

[comment]: # ({new-08edd3dc})
##### 5 Construction et installation

::: noteclassic
Si vous installez à partir d’un git, il est nécessaire
d'exécuter d'abord :

`$ make dbschema` 
:::

    make install

Cette étape doit être exécutée en tant qu'utilisateur avec des
autorisations suffisantes (généralement 'root', ou en utilisant `sudo`).

L'exécution de `make install` installera par défaut les binaires du
démon (zabbix\_server, zabbix\_agentd, zabbix\_proxy) dans
/usr/local/sbin et les binaires clients (zabbix\_get, zabbix\_sender)
dans /usr/local/bin.

::: noteclassic
Pour spécifier un emplacement différent de /usr/local,
utilisez une clé --prefix à l'étape précédente de la configuration des
sources, par exemple --prefix=/home/zabbix. Dans ce cas, les fichiers
binaires seront installés sous <prefix>/sbin, tandis que les
utilitaires seront sous <prefix>/bin. Les pages man seront
installées sous <prefix>/share.
:::

[comment]: # ({/new-08edd3dc})

[comment]: # ({new-112b74f7})
##### 6 Examinez et modifiez les fichiers de configuration

-   éditez le fichier de configuration de l'agent Zabbix
    **/usr/local/etc/zabbix\_agentd.conf**

Vous devez configurer ce fichier pour chaque hôte où zabbix\_agentd est
installé.

Vous devez spécifier l'**adresse IP** du serveur Zabbix dans le fichier.
Les connexions des autres hôtes seront refusées.

-   éditez le fichier de configuration du serveur Zabbix
    **/usr/local/etc/zabbix\_server.conf**

Vous devez spécifier le nom de la base de données, l'utilisateur et le
mot de passe (si vous en utilisez un).

Le reste des paramètres vous conviendra avec leurs valeurs par défaut si
vous avez une petite installation (jusqu'à dix hôtes surveillés). Vous
devriez changer les paramètres par défaut si vous voulez maximiser les
performances du serveur Zabbix (ou du proxy). Voir la section de
[réglage des performances](/fr/manual/appendix/performance_tuning) pour
plus de détails.

-   Si vous avez installé un proxy Zabbix, éditez le fichier de
    configuration du proxy **/usr/local/etc/zabbix\_proxy.conf**

Vous devez spécifier l'adresse IP du serveur et le nom d'hôte du proxy
(qui doit être connu du serveur), ainsi que le nom de la base de
données, l'utilisateur et le mot de passe (si vous en utilisez un).

::: noteclassic
Avec SQLite, le chemin complet du fichier de la base de
données doit être spécifié ; l'utilisateur de la base de données et le
mot de passe ne sont pas obligatoires.
:::

[comment]: # ({/new-112b74f7})

[comment]: # ({new-bbdd82ac})
##### 7 Démarrage des démons

Exécuter zabbix\_server sur le serveur Zabbix.

    shell> zabbix_server

::: noteclassic
Assurez-vous que votre système autorise l'allocation de 36
Mo (ou un peu plus) de mémoire partagée, sinon le serveur risque de ne
pas démarrer et vous verrez "Impossible d'allouer de la mémoire partagée
pour <type de cache>" dans le fichier journal du serveur. Cela
peut arriver sur FreeBSD, Solaris 8.\
Voir la section ["Voir aussi"](#see_also) au bas de cette page pour
savoir comment configurer la mémoire partagée.
:::

Exécuter zabbix\_agentd sur les machines supervisées.

    shell> zabbix_agentd

::: noteclassic
Assurez-vous que votre système autorise l'allocation de 2 Mo
de mémoire partagée, sinon l'agent risque de ne pas démarrer et vous
verrez "Impossible d'allouer de la mémoire partagée pour le collecteur"
dans le fichier journal de l'agent. Cela peut arriver sur Solaris
8.
:::

Si vous avez installé un proxy Zabbix, lancer zabbix\_proxy.

    shell> zabbix_proxy

[comment]: # ({/new-bbdd82ac})

[comment]: # ({new-c9f154ca})
#### - Installez l'interface web Zabbix

[comment]: # ({/new-c9f154ca})

[comment]: # ({new-ed4e56ce})
##### Copiez les fichiers PHP

L'interface Zabbix est écrite en PHP, donc pour l'exécuter, un serveur
web qui supporte PHP est nécessaire. L'installation se fait simplement
en copiant les fichiers PHP depuis frontends/php vers le répertoire HTML
du serveur web.

Les emplacements communs des répertoires HTML pour les serveurs Web
Apache incluent :

-   /usr/local/apache2/htdocs (répertoire par défaut lors de
    l'installation d'Apache à partir des sources)
-   /srv/www/htdocs (OpenSUSE, SLES)
-   /var/www/html (Debian, Ubuntu, Fedora, RHEL, CentOS)

Il est suggéré d'utiliser un sous-répertoire à la place de la racine
HTML. Pour créer un sous-répertoire et y copier des fichiers frontaux
Zabbix, exécutez les commandes suivantes en remplaçant le répertoire
actuel :

    mkdir <htdocs>/zabbix
    cd frontends/php
    cp -a . <htdocs>/zabbix

Si vous installez à partir d’un git et prévoyez d'utiliser une autre
langue que l'anglais, vous devez générer des fichiers de traduction.
Pour ce faire, exécutez:

    locale/make_mo.sh

L'utilitaire `msgfmt` du package gettext est requis.

::: noteclassic
En outre, pour utiliser une autre langue que l'anglais, les
paramètres régionaux doivent être installés sur le serveur Web. Voir la
section ["Voir aussi"](/fr/manual/web_interface/user_profile#see_also)
dans la page "Profil de l'utilisateur" pour savoir comment l'installer
si nécessaire.
:::

[comment]: # ({/new-ed4e56ce})

[comment]: # ({new-63583cbb})
##### Installez l'interface Web

[comment]: # ({/new-63583cbb})

[comment]: # ({new-1a519c06})
##### Étape 1

Dans votre navigateur, ouvrez l'URL Zabbix :
http://<IP\_server\_ou\_Nom\_serveur>/zabbix

Vous devriez voir le premier écran de l'assistant d'installation de
l'interface utilisateur.

![](../../../assets/en/manual/installation/install_1.png){width="500"}

[comment]: # ({/new-1a519c06})

[comment]: # ({new-76c01064})
##### Étape 2

Assurez-vous que tous les prérequis logiciels sont remplis.

![](../../../assets/en/manual/installation/install_2.png){width="550"}

|Pré-requis|Valeur minimum|Description|
|-----------|--------------|-----------|
|*Version PHP*|5.4.0|<|
|*Option PHP memory\_limit*|128Mo|Dans php.ini:<br>memory\_limit = 128M|
|*Option PHP post\_max\_size*|16Mo|Dans php.ini:<br>post\_max\_size = 16M|
|*Option PHP upload\_max\_filesize*|2Mo|In php.ini:<br>upload\_max\_filesize = 2M|
|*Option PHP max\_execution\_time*|300 secondes (valeurs 0 et -1 sont autorisées)|Dans php.ini:<br>max\_execution\_time = 300|
|*Option PHP max\_input\_time*|300 secondes (valeurs 0 et -1 sont autorisées)|Dans php.ini:<br>max\_input\_time = 300|
|*Option PHP session.auto\_start*|doit être désactivée|Dans php.ini:<br>session.auto\_start = 0|
|*Base de données supportées*|Une parmi : MySQL, Oracle, PostgreSQL, IBM DB2|L'un des modules suivant doit être installé : :<br>mysql, oci8, pgsql, ibm\_db2|
|*bcmath*|<|php-bcmath|
|*mbstring*|<|php-mbstring|
|//Option PHP mbstring.func\_overload //|doit être désactivée|Dans php.ini:<br>mbstring.func\_overload = 0|
|//Option PHP always\_populate\_raw\_post\_data //|doit être désactivée|Nécessaire uniquement pour les versions PHP 5.6.0 ou plus récentes.<br>Dans php.ini:<br>always\_populate\_raw\_post\_data = -1|
|*sockets*|<|php-net-socket. Nécessaire pour la prise en charge des scripts utilisateur.|
|*gd*|2.0 ou supérieur|php-gd. L'extension PHP GD doit prendre en charge les images au format PNG (*--with-png-dir*), JPEG (*--with-jpeg-dir*) et FreeType 2 (*--with-freetype-dir*).|
|*libxml*|2.6.15|php-xml ou php5-dom|
|*xmlwriter*|<|php-xmlwriter|
|*xmlreader*|<|php-xmlreader|
|*ctype*|<|php-ctype|
|*session*|<|php-session|
|*gettext*|<|php-gettext<br>Depuis Zabbix 2.2.1, l’extension PHP gettext n’est plus un pré-requis obligatoire pour l’installation de Zabbix. Si gettext n’est pas installé, l’interface Web fonctionnera, bien que les traductions ne seront pas disponibles.|

Des pré-requis facultatifs peuvent également être présents dans la
liste. Un prérequis optionnel défectueux est affiché en orange et a un
statut d'*avertissement*. Avec un pré-requis optionnel manquant,
l'installation peut continuer.

::: noteimportant
S'il est nécessaire de modifier l'utilisateur
Apache ou le groupe d'utilisateurs Apache, les autorisations sur le
dossier de session doivent être vérifiées. Sinon, le programme
d'installation Zabbix risque de ne pas pouvoir continuer.
:::

[comment]: # ({/new-76c01064})

