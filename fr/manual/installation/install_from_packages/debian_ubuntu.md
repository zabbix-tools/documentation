[comment]: # translation:outdated

[comment]: # ({new-d96d3275})
# 2 Debian/Ubuntu

[comment]: # ({/new-d96d3275})

[comment]: # ({new-9df568e9})
#### Aperçu

Les packages officiels Zabbix sont disponibles pour :

-   Debian 9 (Stretch)
-   Debian 8 (Jessie)
-   Debian 7 (Wheezy)
-   Ubuntu 18.04 (Bionic Beaver) LTS
-   Ubuntu 16.04 (Xenial Xerus) LTS
-   Ubuntu 14.04 (Trusty Tahr) LTS

[comment]: # ({/new-9df568e9})

[comment]: # ({new-ae631a63})
#### Ajout du dépôt Zabbix

Installez le package de configuration du dépôt. Ce package contient les
fichiers de configuration apt (logiciel de gestion de packages).

Pour **Debian 9**, exécutez les commandes suivantes :

**Remarque !** Pour Debian 8, remplacez 'stretch' par 'jessie' dans les
commandes. Pour Debian 7, remplacez 'stretch' par 'wheezy' dans les
commandes.

    # wget http://repo.zabbix.com/zabbix/3.5/debian/pool/main/z/zabbix-release/zabbix-release_3.5-1+stretch_all.deb
    # dpkg -i zabbix-release_3.5-1+stretch_all.deb
    # apt update

Pour **Ubuntu 18.04 (bionic)**, exécutez les commandes suivantes :

    # wget http://repo.zabbix.com/zabbix/3.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.5-1+bionic_all.deb
    # dpkg -i zabbix-release_3.5-1+bionic_all.deb
    # apt update

-   Pour Ubuntu 16.04, remplacez 'bionic' par 'xenial' dans les
    commandes.
-   Pour Ubuntu 14.04, remplacez 'bionic' par 'trusty' dans les
    commandes.

[comment]: # ({/new-ae631a63})

[comment]: # ({new-99a402fa})
#### Installation du serveur/proxy/interface web

Pour installer le serveur Zabbix avec MySQL :

    # apt install zabbix-server-mysql

Pour installer le proxy Zabbix avec MySQL :

    # apt install zabbix-proxy-mysql

Pour installer l’interface web Zabbix :

    # apt install zabbix-frontend-php

Remplacez 'mysql' dans les commandes par 'pgsql' pour utiliser
PostgreSQL, pour par 'sqlite3' pour utiliser SQLite3 (proxy uniquement).

[comment]: # ({/new-99a402fa})


[comment]: # ({new-fa73411a})
#### Import des données

Maintenant importez le schéma initial et les données pour le serveur
avec MySQL :

    # zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -uzabbix -p zabbix

Vous serez invité à entrer votre nouveau mot de passe pour la base de
données.

Avec PostgreSQL :

    # zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | sudo -u <username> psql zabbix

Pour le proxy, importez le schéma initial :

    # zcat /usr/share/doc/zabbix-proxy-mysql/schema.sql.gz | mysql -uzabbix -p zabbix

Pour le proxy avec PostgreSQL (ou SQLite):

    # zcat /usr/share/doc/zabbix-proxy-pgsql/schema.sql.gz | sudo -u <username> psql zabbix
    # zcat /usr/share/doc/zabbix-proxy-sqlite3/schema.sql.gz | sqlite3 zabbix.db

[comment]: # ({/new-fa73411a})

[comment]: # ({new-a12da003})
#### Configuration de la base de données pour le serveur/proxy Zabbix

Editez zabbix\_server.conf ou zabbix\_proxy.conf pour utiliser la base
de données créée. Par exemple :

    # vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=<password>

Dans le paramètre DBPassword utilisez le mot de passe de la base de
données Zabbix pour MySQL; et le mot de passe de l’utilisateur PosgreSQL
pour PosgreSQL.

Utilisez `DBHost=` avec PostgreSQL. Vous souhaiterez peut-être conserver
le paramètre par défaut `DBHost=localhost` (ou une adresse IP), mais
cela fera en sorte que PostgreSQL utilise un socket réseau pour se
connecter à Zabbix. Voir la [section
correspondante](/fr/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
pour CentOS pour les instructions.

[comment]: # ({/new-a12da003})

[comment]: # ({new-fe6abb8e})
#### Démarrage des processus du serveur Zabbix

C’est maintenant le moment de démarrer les processus du serveur Zabbix
et de faire en sorte qu’il démarre au démarrage du système :

    # service zabbix-server start
    # update-rc.d zabbix-server enable

Remplacez 'zabbix-server' par 'zabbix-proxy' pour démarrer les processus
du proxy Zabbix.

[comment]: # ({/new-fe6abb8e})

[comment]: # ({new-2ab835d7})
#### Configuration SELinux

Reportez-vous à la [section
correspondante](/fr/manual/installation/install_from_packages/rhel_centos#selinux_configuration)
pour RHEL/CentOS.

Comme la configuration de l’interface web et de SELinux est terminée,
vous devez redémarrer le serveur Web Apache :

    # service apache2 restart

[comment]: # ({/new-2ab835d7})

[comment]: # ({new-d0a225c7})
#### Configuration de l’interface Web

Le fichier de configuration Apache pour l’interface web Zabbix se trouve
dans /etc/apache2/conf-enabled/zabbix.conf. Certains paramètres PHP sont
déjà configurés. Mais il est nécessaire de décommenter le paramètre
"date.timezone" et de [définir le fuseau
horaire](http://php.net/manual/en/timezones.php) qui vous correspond.

    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga

Vous êtes maintenant prêt à procéder aux [étapes d'installation de
l’interface web](/fr/manual/installation/install#installing_frontend)
qui vous permettront d'accéder à votre Zabbix nouvellement installé.

[comment]: # ({/new-d0a225c7})

[comment]: # ({new-27de2ced})
#### Installation de l’agent

Pour installer l’agent, exécutez :

    # apt install zabbix-agent

Pour démarrer l’agent, exécutez :

    # service zabbix-agent start

[comment]: # ({/new-27de2ced})

[comment]: # ({new-871a973b})
##### Frontend configuration

A Zabbix proxy does not have a frontend; it communicates with Zabbix
server only.

[comment]: # ({/new-871a973b})

[comment]: # ({new-cd9340bd})
### Java gateway installation

It is required to install [Java gateway](/manual/concepts/java) only if
you want to monitor JMX applications. Java gateway is lightweight and
does not require a database.

Once the required repository is added, you can install Zabbix Java
gateway by running:

    # apt install zabbix-java-gateway

Proceed to [setup](/manual/concepts/java/from_debian_ubuntu) for more
details on configuring and running Java gateway.

[comment]: # ({/new-cd9340bd})
