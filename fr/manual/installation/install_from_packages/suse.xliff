<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/installation/install_from_packages/suse.md">
    <body>
      <trans-unit id="51abb0f6" xml:space="preserve">
        <source># 3 SUSE Linux Enterprise Server</source>
      </trans-unit>
      <trans-unit id="ee73b6da" xml:space="preserve">
        <source>### Overview

Official Zabbix 6.5 PRE-RELEASE packages for SUSE Linux Enterprise Server are available on
[Zabbix website](https://www.zabbix.com/download). Packages for Zabbix 7.0 will be available upon its release.

*Zabbix agent* packages and utilities *Zabbix get* and *Zabbix sender* are available in Zabbix Official Repository for
[SLES 15 (SP4 and newer)](https://repo.zabbix.com/zabbix/6.5/sles/15/x86_64/) and
[SLES 12 (SP4 and newer)](https://repo.zabbix.com/zabbix/6.5/sles/12/x86_64/) as 6.5 pre-release versions.

Please note that on SLES 12 the following features are not available:

- *Verify CA* [encryption mode](/manual/appendix/install/db_encrypt/mysql) with MySQL does not worked due to older MySQL libraries.
- Since Zabbix 6.4, *SSH checks* are not supported for both proxy and server because of the older libssh version. </source>
      </trans-unit>
      <trans-unit id="37fbf2db" xml:space="preserve">
        <source>### Adding Zabbix repository

Install the repository configuration package. This package contains yum
(software package manager) configuration files.

SLES 15:

    rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.5/sles/15/x86_64/zabbix-release-6.5-1.sles15.noarch.rpm
    zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

SLES 12:

    rpm -Uvh --nosignature https://repo.zabbix.com/zabbix/6.5/sles/12/x86_64/zabbix-release-6.5-1.sles12.noarch.rpm
    zypper --gpg-auto-import-keys refresh 'Zabbix Official Repository' 

Please note that Zabbix web service process, which is used for
[scheduled report
generation](/manual/web_interface/frontend_sections/reports/scheduled),
requires Google Chrome browser. The browser is not included into
packages and has to be installed manually.</source>
      </trans-unit>
      <trans-unit id="ef4f3e50" xml:space="preserve">
        <source>### Server/frontend/agent installation

To install Zabbix server/frontend/agent with PHP 7, Apache and MySQL support, run:

    zypper install zabbix-server-mysql zabbix-web-mysql zabbix-apache-conf zabbix-agent


Substitute component names in this command as needed:

- **For Nginx**: use `zabbix-nginx-conf` instead of `zabbix-apache-conf`. See also: [Nginx setup for Zabbix on SLES 12/15](/manual/appendix/install/nginx).
- **For PHP 8**: use `zabbix-apache-conf-php8` instead of `zabbix-apache-conf` for Apache; use `zabbix-nginx-conf-php8` instead of `zabbix-nginx-conf` for Nginx.
- **For PostgreSQL**: use `zabbix-server-pgsql` instead of `zabbix-server-mysql`; use `zabbix-web-pgsql` instead of `zabbix-web-mysql`.
- **For Zabbix agent 2** (only SLES 15): use `zabbix-agent2` instead of or in addition to `zabbix-agent`.


To install Zabbix proxy with MySQL support:

    zypper install zabbix-proxy-mysql zabbix-sql-scripts

**For PostgreSQL**, use `zabbix-proxy-pgsql` instead of `zabbix-proxy-mysql`.

**For SQLite3**, use `zabbix-proxy-sqlite3` instead of `zabbix-proxy-mysql`.

The package 'zabbix-sql-scripts' contains database schemas for all supported database management systems for both Zabbix server and Zabbix proxy and will be used for data import.</source>
      </trans-unit>
      <trans-unit id="c573e862" xml:space="preserve">
        <source>#### Creating database

Zabbix [server](/manual/concepts/server) and [proxy](/manual/concepts/proxy) daemons require a database.
Zabbix [agent](/manual/concepts/agent) does not need a database.

To create a database, follow the instructions for [MySQL](/manual/appendix/install/db_scripts#mysql) or [PostgreSQL](/manual/appendix/install/db_scripts#postgresql).
An SQLite3 database (supported for Zabbix proxy only) will be created automatically and does not require additional installation steps.

::: notewarning
Separate databases are required for Zabbix server and Zabbix proxy; they cannot share the same database.
If a server and a proxy are installed on the same host, their databases must be created with different names!
:::</source>
      </trans-unit>
      <trans-unit id="35bc057d" xml:space="preserve">
        <source>#### Importing data

Now import initial schema and data for the **server** with MySQL:

    zcat /usr/share/packages/zabbix-sql-scripts/mysql/create.sql.gz | mysql -uzabbix -p zabbix

You will be prompted to enter your newly created database password.

With PostgreSQL:

    zcat /usr/share/packages/zabbix-sql-scripts/postgresql/create.sql.gz | sudo -u zabbix psql zabbix

With TimescaleDB, in addition to the previous command, also run:

    zcat /usr/share/packages/zabbix-sql-scripts/postgresql/timescaledb.sql.gz | sudo -u &lt;username&gt; psql zabbix

::: notewarning
TimescaleDB is supported with Zabbix server
only.
:::

For proxy, import initial schema:

    zcat /usr/share/packages/zabbix-sql-scripts/mysql/schema.sql.gz | mysql -uzabbix -p zabbix

For proxy with PostgreSQL:

    zcat /usr/share/packages/zabbix-sql-scripts/postgresql/schema.sql.gz | sudo -u zabbix psql zabbix</source>
      </trans-unit>
      <trans-unit id="0ea127cd" xml:space="preserve">
        <source>#### Configure database for Zabbix server/proxy

Edit /etc/zabbix/zabbix\_server.conf (and zabbix\_proxy.conf) to use
their respective databases. For example:

    vi /etc/zabbix/zabbix_server.conf
    DBHost=localhost
    DBName=zabbix
    DBUser=zabbix
    DBPassword=&lt;password&gt;

In DBPassword use Zabbix database password for MySQL; PostgreSQL user
password for PostgreSQL.

Use `DBHost=` with PostgreSQL. You might want to keep the default
setting `DBHost=localhost` (or an IP address), but this would make
PostgreSQL use a network socket for connecting to Zabbix.</source>
      </trans-unit>
      <trans-unit id="d162545b" xml:space="preserve">
        <source>#### Zabbix frontend configuration

Depending on the web server used (Apache/Nginx), edit the corresponding configuration file for Zabbix frontend.
While some PHP settings may already be configured, 
it's essential that you uncomment the `date.timezone` setting and specify the appropriate [timezone](http://php.net/manual/en/timezones.php) setting that suits your requirements.

-   For Apache the configuration file is located in `/etc/apache2/conf.d/zabbix.conf`.

```bash
    php_value max_execution_time 300
    php_value memory_limit 128M
    php_value post_max_size 16M
    php_value upload_max_filesize 2M
    php_value max_input_time 300
    php_value max_input_vars 10000
    php_value always_populate_raw_post_data -1
    # php_value date.timezone Europe/Riga
```

-   The zabbix-nginx-conf package installs a separate Nginx server for Zabbix frontend.
    Its configuration file is located in `/etc/nginx/conf.d/zabbix.conf`.
    For Zabbix frontend to work, it's necessary to uncomment and set `listen` and/or `server_name` directives.

```bash
    # listen 80;
    # server_name example.com;
```

-   Zabbix uses its own dedicated php-fpm connection pool with Nginx:

Its configuration file is located in `/etc/php7/fpm/php-fpm.d/zabbix.conf` (the path may vary slightly depending on the service pack).

```bash
    php_value[max_execution_time] = 300
    php_value[memory_limit] = 128M
    php_value[post_max_size] = 16M
    php_value[upload_max_filesize] = 2M
    php_value[max_input_time] = 300
    php_value[max_input_vars] = 10000
    ; php_value[date.timezone] = Europe/Riga
```

Now you are ready to proceed with [frontend installation steps](/manual/installation/install#installing_frontend) that will
allow you to access your newly installed Zabbix.

Note that a Zabbix proxy does not have a frontend; it communicates with Zabbix server only.</source>
      </trans-unit>
      <trans-unit id="71fda19d" xml:space="preserve">
        <source>#### Starting Zabbix server/agent process

Start Zabbix server and agent processes and make it start at system
boot.

With Apache web server:

    systemctl restart zabbix-server zabbix-agent apache2 php-fpm
    systemctl enable zabbix-server zabbix-agent apache2 php-fpm

**For Nginx**, substitute `apache2` with `nginx`.</source>
      </trans-unit>
      <trans-unit id="8e553867" xml:space="preserve">
        <source>### Installing debuginfo packages

To enable debuginfo repository edit */etc/zypp/repos.d/zabbix.repo*
file. Change `enabled=0` to `enabled=1` for zabbix-debuginfo repository.

```yaml
    [zabbix-debuginfo]
    name=Zabbix Official Repository debuginfo
    type=rpm-md
    baseurl=http://repo.zabbix.com/zabbix/6.5/sles/15/x86_64/debuginfo/
    gpgcheck=1
    gpgkey=http://repo.zabbix.com/zabbix/6.5/sles/15/x86_64/debuginfo/repodata/repomd.xml.key
    enabled=0
    update=1
```

This will allow you to install zabbix-***&lt;component&gt;***-debuginfo
packages.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
