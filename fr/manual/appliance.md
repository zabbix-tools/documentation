[comment]: # translation:outdated

[comment]: # ({new-8c911baa})
# 6. Appliance Zabbix

[comment]: # ({/new-8c911baa})

[comment]: # ({new-cfa1a361})
#### Aperçu

Au lieu de configurer manuellement ou de réutiliser un serveur existant
pour Zabbix, les utilisateurs peuvent
[télécharger](https://www.zabbix.com/download_appliance) une image du CD
d'installation de l'appliance Zabbix ou de l'appliance Zabbix
directement. Le CD d'installation de l'appliance Zabbix pourrait être
utilisé pour le déploiement instantané du serveur Zabbix (MySQL), du
serveur Zabbix (PostgreSQL), du proxy Zabbix (MySQL) et du proxy Zabbix
(SQLite 3).

Les machines virtuelles pour l'appliance Zabbix sont préparées avec le
support MySQL pour le serveur Zabbix. Ce dernier est construit à l'aide
du CD d'installation de l'appliance Zabbix.

|<|
|<|
|-|

|<|
|<|
|-|

Les versions de l'appliance Zabbix et du CD d'installation sont basées
sur les versions suivantes d'Ubuntu :

|Version de l'appliance Zabbix|Version Ubuntu|
|-----------------------------|--------------|
|3.4.0|14.04.3|

L'appliance Zabbix est disponible dans les formats suivants :

-   vmdk (VMware/Virtualbox)
-   OVF (Open Virtualisation Format)
-   KVM
-   HDD/flash image, USB stick
-   Live CD/DVD
-   Xen guest
-   Microsoft VHD (Azure)
-   Microsoft VHD (Hyper-V)

Pour commencer, démarrez l'appliance et pointez votre navigateur sur
l'adresse IP reçue via DHCP : http://<host\_ip>/zabbix

Elle dispose d'un serveur Zabbix configuré et s'exécutant sur MySQL,
ainsi que d'une interface utilisateur disponible.

L'appliance a été construite en utilisant la fonctionnalité standard
Ubuntu/Debian appelée fichiers 'Preseed'.

[comment]: # ({/new-cfa1a361})

[comment]: # ({new-cffa82a3})
#### - Modifications dans la configuration Ubuntu

Il y a quelques modifications appliquées à la configuration Ubuntu de
base.

[comment]: # ({/new-cffa82a3})

[comment]: # ({new-589fd5e2})
##### - Dépôts

Le [dépôt](/fr/manual/installation/install_from_packages#debianubuntu)
officiel Zabbix a été ajouté à */etc/apt/sources.list*:

    ## Zabbix repository
    deb http://repo.zabbix.com/zabbix/3.4/ubuntu trusty main
    deb-src http://repo.zabbix.com/zabbix/3.4/ubuntu trusty main

[comment]: # ({/new-589fd5e2})

[comment]: # ({new-193d3b23})
##### - Pare-feu

L'appliance utilise le pare-feu iptables avec des règles prédéfinies :

-   Ouverture du port SSH (22 TCP) ;
-   Ouverture pour des ports pour l'agent Zabbix (10050 TCP) et pour le
    trappeur Zabbix (10051 TCP) ;
-   Ouverture des ports HTTP (80 TCP) et HTTPS (443 TCP) ;
-   Ouverture du port SNMP trap (162 UDP) ;
-   Ouverture du port pour les connexions sortantes NTP (53 UDP) ;
-   Les paquets ICMP sont limités à 5 paquets par seconde ;
-   Toutes les autres connexions sont bloquées sont supprimées.

[comment]: # ({/new-193d3b23})

[comment]: # ({new-589dc798})
##### - Packages supplémentaires

Divers utilitaires basiques ont été ajoutés pour faciliter le travail
avec Zabbix et de supervision de manière générale :

-   iptables-persistent
-   mc
-   htop
-   snmptrapfmt
-   snmp-mibs-downloader

Certains de ces packages sont utilisés par Zabbix, d'autres sont
installés pour aider les utilisateurs à configurer/gérer les paramètres
de l'appliance.

[comment]: # ({/new-589dc798})

[comment]: # ({new-b2283a9a})
##### - Utilisation d'une adresse IP statique

Par défaut, l'appliance utilise DHCP pour obtenir une adresse IP. Pour
spécifier une adresse IP statique :

-   Se connecter en tant que root ;
-   Ouvrir le fichier */etc/network/interfaces* dans votre éditeur
    favori ;
-   *iface eth0 inet dhcp* → *iface eth0 inet static*
-   Ajouter les lignes suivantes après *iface eth0 inet static* :
    -   *address <adresse IP de l'appliance>*
    -   *netmask <masque de réseau>*
    -   *gateway <votre adresse de passerelle>*
-   Exécuter les commandes **sudo ifdown eth0 && sudo ifup eth0**.

::: noteclassic
Pour plus d'informations sur les autres options possible,
référez-vous à la
[documentation](https://help.ubuntu.com/lts/serverguide/network-configuration.html)
officielle Unbuntu.
:::

Pour configurer le DNS, ajoutez les noms des serveurs dans
*/etc/resolv.conf*, en spécifiant chaque nom de serveur par ligne :
**nameserver 192.168.1.2**.

[comment]: # ({/new-b2283a9a})

[comment]: # ({new-acf06935})
##### - Changement de fuseau horaire

Par défaut, l'appliance utilise l'horloge système UTC. Pour changer de
fuseau horaire, copiez le fichier approprié depuis */usr/share/zoneinfo*
vers */etc/localtime*, par exemple :

    cp /usr/share/zoneinfo/Europe/Riga /etc/localtime

[comment]: # ({/new-acf06935})

[comment]: # ({new-2c378c8d})
##### - Changement d'encodage

L'appliance contient quelques modification d'encodage :

-   Contient les langages : *en\_US.UTF-8*, *ru\_RU.UTF-8*,
    *ja\_JP.UTF-8*, *cs\_CZ.UTF-8*, *ko\_KR.UTF-8*, *it\_IT.UTF-8*,\
    *pt\_BR.UTF-8*, *sk\_SK.UTF-8*, *uk\_UA.UTF-8*, *fr\_FR.UTF-8*,
    *pl.UTF-8*;
-   L'encodage par défaut est *en\_US.UTF-8*.

Ces changements sont nécessaire pour prendre en charge une interface web
Zabbix dans plusieurs langues.

[comment]: # ({/new-2c378c8d})

[comment]: # ({new-d39b5151})
##### - Autres changements

-   Le réseau est configuré pour utiliser DHCP pour obtenir une adresse
    IP ;
-   L'utilitaire **fping** est configurer pour avoir les permissions
    4710 et appartient au groupe **zabbix** - suid et seulement autorisé
    à être utilisé par le groupe zabbix ;
-   ntpd est configuré pour se synchroniser aux serveurs publiques :
    *ntp.ubuntu.com*;
-   Le volume LVM est utilisé avec l'extension de système de fichier
    ext4 ;
-   "*UseDNS no*" est ajouté au fichier de configuration du server SSH
    */etc/ssh/sshd\_config* pour éviter les longues connexions SSH;
-   Le démon snmpd est désactivé en utilisant le fichier de
    configuration */etc/default/snmpd*.

[comment]: # ({/new-d39b5151})

[comment]: # ({new-f11ee379})
#### - Configuration Zabbix

La configuration de l’appliance Zabbix comporte les mots de passe et
d'autres modifications de configuration suivants :

[comment]: # ({/new-f11ee379})

[comment]: # ({new-e00773c5})
##### - Informations d'identification (login:password)

System :

-   appliance:zabbix

Database :

-   root:<random>
-   zabbix:<random>

::: noteclassic
Les mots de passe de base de données sont générés
aléatoirement lors de l'installation.\
Le mot de passe root est stocké dans le fichier /root/.my.cnf, il n'est
pas nécessaire d'entrer un mot de passe sous le compte "root".

:::

Zabbix frontend :

-   Admin:zabbix

Pour changer le mot de passe de l'utilisateur de la base de données,
cela doit être effectué aux endroits suivants :

-   MySQL ;
-   /etc/zabbix/zabbix\_server.conf ;
-   /etc/zabbix/web/zabbix.conf.php.

[comment]: # ({/new-e00773c5})

[comment]: # ({new-2c4e4239})
##### - Localisation des fichiers

-   Les fichiers de configuration se situent dans **/etc/zabbix**.
-   Les fichiers de log du serveur, du proxy et de l'agent Zabbix se
    situent dans **/var/log/zabbix**.
-   L'interface utilisateur Zabbix se situent dans
    **/usr/share/zabbix**.
-   Le répertoire de base de l'utilisateur (home directory) **zabbix**
    est **/var/lib/zabbix**.

[comment]: # ({/new-2c4e4239})

[comment]: # ({new-4fbc391b})
##### - Modifications dans la configuration Zabbix

-   Le nom du serveur pour l'interface utilisateur est défini sur
    "Appliance Zabbix" ;
-   Le fuseau horaire de l'interface utilisateur est défini sur
    Europe/Riga (cela peut être modifié dans
    **/etc/apache2/conf-available/zabbix.conf**) ;

[comment]: # ({/new-4fbc391b})

[comment]: # ({new-bf60554b})
##### - Préserver la configuration

Si vous exécutez une version Live CD/DVD de l'appliance ou que vous ne
pouvez pas utiliser de stockage persistant pour une autre raison, vous
pouvez créer une sauvegarde de la base de données entière, y compris
toutes les données de configuration et les données collectées.

Pour créer la sauvegarde, exécutez :

    sudo mysqldump zabbix | bzip2 -9 > dbdump.bz2

Vous pouvez maintenant transférer le fichier **dbdump.bz2** sur une
autre machine.

Pour restaurer à partir de la sauvegarde, transférez-la vers l'appliance
et exécutez :

    bzcat dbdump.bz2 | sudo mysql zabbix

::: noteimportant
 Assurez-vous que le serveur Zabbix est arrêté
lors de la restauration. 
:::

[comment]: # ({/new-bf60554b})

[comment]: # ({new-d167c766})
#### - Accès à l'interface utilisateur

Par défaut, l'accès à l'interface utilisateur est autorisée depuis
n'importe quel endroit.

L'interface utilisateur est accessible via *http://<host>/zabbix*.

Cela peut être personnalisé dans
**/etc/apache2/conf-available/zabbix.conf**. Vous devez redémarrer le
serveur web après avoir modifié ce fichier. Pour cela, connectez-vous à
la machine en SSH en tant que **root** et exécutez :

    service apache2 restart

[comment]: # ({/new-d167c766})

[comment]: # ({new-a582d1bf})
#### - Pare-feu

Par défaut, seuls les ports listés précédemment sont ouverts. Pour
ouvrir d'autres ports, modifiez simplement le fichier
"*/etc/iptables/rules.v4*" ou "*/etc/iptables/rules.v6*" et rechargez
les règles du pare-feu :

    service iptables-persistent reload

[comment]: # ({/new-a582d1bf})

[comment]: # ({new-a45df55b})
#### - Possibilités de supervision

Cette installation de Zabbix est fournie avec le support pour les
éléments suivants :

-   SNMP
-   IPMI
-   Supervision Web
-   Supervision VMware
-   Notifications Jabber
-   Notifications EZ Texting
-   ODBC
-   SSH2
-   IPv6
-   Traps SNMP
-   Passerelle Java Zabbix

[comment]: # ({/new-a45df55b})

[comment]: # ({new-7d1c1440})
#### - Traps SNMP

L'appliance Zabbix utilise *snmptrapfmt* pour gérer les traps SNMP. Elle
est configurée pour recevoir tous les traps.

L'authentication n'est pas obligatoire. Si vous voulez activer
l'authentification, vous devez changer le fichier
*/etc/snmp/snmptrapd.conf* et spécifiez les paramètres
d'authentification nécessaires.

Tous les traps sont stockés dans le fichier
*/var/log/zabbix/snmptrapfmt.log*. Un mécanisme de rotation de log est
mis en place grâce à logrotate avant que le fichier n'atteigne la taille
de 2Go.

#### - Mise à jour

Les packages Zabbix de l'appliance peuvent être mis à jour. Pour ce
faire, exécutez :

    sudo apt-get --only-upgrade install zabbix*

#### - Nommage, init et autres scripts

Des scripts d'initialisation appropriés sont fournis. Pour contrôler le
serveur Zabbix, utilisez l'un de ces éléments :

    service zabbix-server status

Remplacez **server** par **agent** pour l'agent Zabbix ou par **proxy**
pour le proxy Zabbix.

##### - Augmenter l'espace disque disponible

::: notewarning
Créez une sauvegarde de toutes les données avant
d'essayer l'une des étapes.
:::

L'espace disque disponible sur l'appliance peut ne pas être suffisant.
Dans ce cas, il est possible d'étendre le disque. Pour ce faire, étendez
d'abord le périphérique bloc dans votre environnement de virtualisation,
puis procédez comme suit.

Démarrer *fdisk* pour changer la taille de la partition. En tant que
*root*, exécutez :

    fdisk /dev/sda

Cela démarrera *fdisk* sur le disque *sda*. Ensuite, passez aux secteurs
en utilisant :

    u

::: noteimportant
Ne désactivez pas le mode de compatibilité DOS en
entrant **c**. Si vous la désactivez, cela endommagera la partition.

:::

Supprimez ensuite la partition existante et créez-en une nouvelle avec
la taille souhaitée. Dans la majorité des cas, vous accepterez le
maximum disponible, ce qui étendra le système de fichiers à la taille
disponible pour le disque virtuel. Pour ce faire, entrez la séquence
suivante dans l'invite fdisk :

    d
    n
    p
    1
    (accept default 63)
    (accept default max)

Si vous souhaitez laisser de l'espace pour des partitions additionnelles
(swap etc), vous pouvez entrer une autre valeur pour le *nouveau
secteur*. Quand cela est fait sauvegardez les modifications en tapant :

    w

Après la création de la partition (nouveau disque ou disque existant
étendu), créer le volume physique :

    pvcreate /dev/sdb1

::: notewarning
Le nom de la partition /dev/sdb1 est utilisée dans
l'exemple ; dans votre cas, le nom du disque et le numéro de la
partition peuvent être différent. Vous pouvez vérifier le numéro de la
partition en utilisant la commande *fdisk -l /dev/sdb*.
:::

Vérifiez le nouveau volume physique :

    pvdisplay /dev/sdb1

Vérifiez les volumes physiques disponibles. Il doit y avoir 2 volumes
zabbix-vg et nouvellement créés :

    pvs

Etendez votre volume group avec le nouveau volume physique

    vgextend zabbix-vg /dev/sdb1

Vérifiez le volume group "zabbix-vg" :

    vgdisplay

Maintenant étendez votre volume logique avec l'espace libre :

    lvextend -l +100%FREE /dev/mapper/zabbix--vg-root

Redimensionnez votre volume racine (peut peut fait via le live system) :

    resize2fs /dev/mapper/zabbix--vg-root

Redémarrez la machine virtuelle (comme la partition que nous avons
modifiée est actuellement utilisée). Ça y est, le système de fichiers
devrait maintenant atteindre la taille de la partition. Vérifiez le
volume "/dev/mapper/zabbix--vg-root" :

    df -h

#### - Note de format spécifique

\*\* Conversion d'image pour XenServer \*\*

Pour utiliser des images Xen avec Citrix Xenserver, vous devez convertir
l'image disque. Pour faire cela :

-   Créez un disque virtuel au moins aussi grand que l'image
-   Trouvez l'UUID pour ce disque

```{=html}
<!-- -->
```
    xe vdi-list params=all

-   S'il y a beaucoup de disques, ils peuvent être filtrés par le nom du
    paramètre *name-label*, tel qu'attribué lors de la création du
    disque virtuel
-   Importez l'image

```{=html}
<!-- -->
```
    xe vdi-import filename="image.raw" uuid="<UUID>"

// Instructions du blog de Brian Radford//.

##### - VMware

Les images au format *vmdk* sont utilisables directement dans les
produits VMware Player, Server et Workstation. Pour être utilisés dans
ESX, ESXi et vSphere, ils doivent être convertis à l'aide de [VMware
converter](http://www.vmware.com/fr/products/converter/).

##### - Image HDD/flash (brut)

    dd if=./zabbix_appliance_3.4.0_x86_64.raw of=/dev/sdc bs=4k conv=fdatasync

Remplacez */dev/sdc* par votre lecteur de disque Flash/HDD.

#### - Problèmes connus

[comment]: # ({/new-7d1c1440})
