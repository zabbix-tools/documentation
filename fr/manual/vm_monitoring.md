[comment]: # translation:outdated

[comment]: # ({new-3ca85094})
# 10. Virtual machine monitoring

[comment]: # ({/new-3ca85094})

[comment]: # ({new-621e23e6})
#### Overview

Support of monitoring VMware environments is available in Zabbix
starting with version 2.2.0.

Zabbix can use low-level discovery rules to automatically discover
VMware hypervisors and virtual machines and create hosts to monitor
them, based on pre-defined host prototypes.

The default dataset in Zabbix offers several ready-to-use templates for
monitoring VMware vCenter or ESX hypervisor.

The minimum required VMware vCenter or vSphere version is 4.1.

[comment]: # ({/new-621e23e6})

[comment]: # ({new-4c2e8939})
#### Details

The virtual machine monitoring is done in two steps. First, virtual
machine data is gathered by *vmware collector* Zabbix processes. Those
processes obtain necessary information from VMware web services over the
SOAP protocol, pre-process it and store into Zabbix server shared
memory. Then, this data is retrieved by pollers using Zabbix simple
check [VMware
keys](/fr/manual/config/items/itemtypes/simple_checks/vmware_keys).

Starting with Zabbix version 2.4.4 the collected data is divided into 2
types: VMware configuration data and VMware performance counter data.
Both types are collected independently by *vmware collectors*. Because
of this it is recommended to enable more collectors than the monitored
VMware services. Otherwise retrieval of VMware performance counter
statistics might be delayed by the retrieval of VMware configuration
data (which takes a while for large installations).

Currently only datastore, network interface and disk device statistics
and custom performance counter items are based on the VMware performance
counter information.

[comment]: # ({/new-4c2e8939})

[comment]: # ({new-68c3f7d5})
#### Configuration

For virtual machine monitoring to work, Zabbix should be
[compiled](/fr/manual/installation/install#configure_the_sources) with
the --with-libxml2 and --with-libcurl compilation options.

The following configuration file options can be used to tune the Virtual
machine monitoring:

-   **StartVMwareCollectors** - the number of pre-forked vmware
    collector instances.\
    This value depends on the number of VMware services you are going to
    monitor. For the most cases this should be:\
    *servicenum < StartVMwareCollectors < (servicenum \* 2)*\
    where *servicenum* is the number of VMware services. E. g. if you
    have 1 VMware service to monitor set StartVMwareCollectors to 2, if
    you have 3 VMware services, set it to 5. Note that in most cases
    this value should not be less than 2 and should not be 2 times
    greater than the number of VMware services that you monitor. Also
    keep in mind that this value also depends on your VMware environment
    size and *VMwareFrequency* and *VMwarePerfFrequency* configuration
    parameters (see below).
-   **VMwareCacheSize**
-   **VMwareFrequency**
-   **VMwarePerfFrequency**
-   **VMwareTimeout**

For more details, see the configuration file pages for Zabbix
[server](/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy).

[comment]: # ({/new-68c3f7d5})

[comment]: # ({new-e5966479})
#### Discovery

Zabbix can use a low-level discovery rule to automatically discover
VMware hypervisors and virtual machines.

![](../../assets/en/manual/vm_monitoring/vm_hypervisor_lld.png)

All mandatory input fields are marked with a red asteisk.

Discovery rule key in the above screenshot is
*vmware.hv.discovery\[{$URL}\]*.

[comment]: # ({/new-e5966479})


[comment]: # ({new-2f026fa1})
#### Ready-to-use templates

The default dataset in Zabbix offers several ready-to-use templates for
monitoring VMware vCenter or directly ESX hypervisor.

These templates contain pre-configured LLD rules as well as a number of
built-in checks for monitoring virtual installations.

Note that "*Template Virt VMware*" template should be used for VMware
vCenter and ESX hypervisor monitoring. The "*Template Virt VMware
Hypervisor*" and "*Template Virt VMware Guest*" templates are used by
discovery and normally should not be manually linked to a host.

![](../../assets/en/manual/vm_monitoring/vm_templates.png)

::: noteclassic
If your server has been upgraded from a pre-2.2 version and
has no such templates, you can import them manually, downloading from
the community page with [official
templates](http://www.zabbix.org/wiki/Zabbix_Templates/Official_Templates).
However, these templates have dependencies from the *VMware
VirtualMachinePowerState* and *VMware status* value maps, so it is
necessary to create these value maps first (using an [SQL
script](https://www.zabbix.org/wiki/Zabbix_Templates/SQLs_for_Official_Templates),
manually or importing from an XML) before importing the
templates.
:::

[comment]: # ({/new-2f026fa1})

[comment]: # ({new-317ec2ff})
#### Host configuration

To use VMware simple checks the host must have the following user macros
defined:

-   **{$URL}** - VMware service (vCenter or ESX hypervisor) SDK URL
    (<https://servername/sdk>).
-   **{$USERNAME}** - VMware service user name
-   **{$PASSWORD}** - VMware service {$USERNAME} user password

[comment]: # ({/new-317ec2ff})

[comment]: # ({new-96cbfb15})
#### Example

The following example demonstrates how to quickly setup VMware
monitoring on Zabbix:

-   compile zabbix server with required options (--with-libxml2 and
    --with-libcurl)
-   set the StartVMwareCollectors option in Zabbix server configuration
    file to 1 or more
-   create a new host
-   set the host macros required for VMware authentication:

```{=html}
<!-- -->
```
        {{..:..:assets:en:manual:vm_monitoring:vm_host_macros.png|}}
    * Link the host to the VMware service template: 
        {{..:..:assets:en:manual:vm_monitoring:vm_host_templates.png|}}
    * Click on the //Add// button to save the host

[comment]: # ({/new-96cbfb15})

[comment]: # ({new-44948bb1})
#### Extended logging

The data gathered by VMware collector can be logged for detailed
debugging using debug level 5. This level can be set in
[server](/fr/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option
(`-R log_level_increase="vmware collector,N"`, where N is a process
number). The following examples demonstrate how extended logging can be
started provided debug level 4 is already set:

    Increase log level of all vmware collectors:
    shell> zabbix_server -R log_level_increase="vmware collector"

    Increase log level of second vmware collector:
    shell> zabbix_server -R log_level_increase="vmware collector,2"

If extended logging of VMware collector data is not required it can be
stopped using the `-R log_level_decrease` option.

[comment]: # ({/new-44948bb1})

[comment]: # ({new-f6f9d3ef})
#### Troubleshooting

-   In case of unavailable metrics, please make sure if they are not
    made unavailable or turned off by default in recent VMware vSphere
    versions or if some limits are not placed on performance-metric
    database queries. See
    [ZBX-12094](https://support.zabbix.com/browse/ZBX-12094) for
    additional details.

[comment]: # ({/new-f6f9d3ef})
