[comment]: # translation:outdated

[comment]: # ({new-bd3d4e41})
# 2. Définitions

[comment]: # ({/new-bd3d4e41})

[comment]: # ({new-99b1c1f4})
#### Aperçu

Dans cette section vous apprendrez la signification de certains termes
couramment utilisés dans Zabbix.

[comment]: # ({/new-99b1c1f4})

[comment]: # ({new-77400725})
#### Définitions

***[hôte](/fr/manual/config/hosts)***

\- *un équipement sur le réseau que vous souhaitez surveiller, avec
adresse IP/DNS,*

***[groupe d'hôtes](/fr/manual/config/hosts)***

\- *un regroupement logique d'hôtes ; il peut contenir des hôtes et des
modèles. Les hôtes et les modèles d'un groupe d'hôtes ne sont aucunement
liés les uns aux autres.  Les groupes d'hôtes sont utilisés lors de
l'attribution de droits d'accès aux hôtes pour différents groupes
d'utilisateurs.*

***[élément](/fr/manual/config/items)***

\- *une donnée particulière que vous voulez recevoir d'un hôte, une
métrique de données.*

***[déclencheur](/fr/manual/config/triggers)***

\- *une expression logique qui définit un seuil de problème et qui est
utilisée pour "évaluer" les données reçues dans les éléments*

Lorsque les données reçues sont au-dessus du seuil, les déclencheurs
vont de 'Ok' à 'Problème'. Lorsque les données reçues sont inférieures
au seuil, les déclencheurs restent ou reviennent à un état 'Ok'.

***[événement](/fr/manual/config/events)***

\- *une occurrence unique de quelque chose qui mérite l'attention, comme
un changement d'état de déclenchement, un enregistrement automatique
d’agent ou une découverte d’agent.*

***[problème](/fr/manual/web_interface/frontend_sections/monitoring/problems)***

\- *un déclencheur en état "Problème"*

***[action](/fr/manual/config/notifications/action)***

\- *un moyen prédéfini de réagir à un événement.*

Une action consiste en des opérations (par exemple l'envoi d'une
notification) et des conditions (*lorsque* l'opération est effectuée)

***[escalade](/fr/manual/config/notifications/action/escalations)***

\- * un scénario personnalisé pour l'exécution d'opérations dans une
action ; une séquence d'envoi de notifications/exécution de commandes
distances*

***[media](/fr/manual/config/notifications/media)***

\- *un moyen de délivrer des notifications ; canal de livraison*

***[notification](/fr/manual/config/notifications/action/operation/message)***

\- *un message sur un événement envoyé à un utilisateur via le canal
choisi (media)*

***[commande
distante](/fr/manual/config/notifications/action/operation/remote_command)***

\- *une commande prédéfinie qui est automatiquement exécutée sur un hôte
surveillé à certaines conditions*

***[modèle](/fr/manual/config/templates)***

\- *un ensemble d'entités (éléments, déclencheurs, graphiques, écrans,
applications, règles de découverte de bas niveau, scénarios Web) prêtes
à être appliquées à un ou plusieurs hôtes.*

Le travail des modèles est d'accélérer le déploiement des tâches de
surveillance sur un hôte ; également pour faciliter l'application de
modifications de masse aux tâches de surveillance. Les modèles sont
directement liés à des hôtes individuels.

\- *a set of entities (items, triggers, graphs, screens, applications,
low-level discovery rules, web scenarios) ready to be applied to one or
several hosts*

The job of templates is to speed up the deployment of monitoring tasks
on a host; also to make it easier to apply mass changes to monitoring
tasks. Templates are linked directly to individual hosts.

***[application](/fr/manual/config/items/applications)***

\- *un regroupement d'éléments dans un groupe logique*

***[scénario Web](/fr/manual/web_monitoring)***

\- *une ou plusieurs requêtes HTTP pour vérifier la disponibilité d'un
site web*

***[interface web](/fr/manual/introduction/overview#architecture)***

\- *l'interface web fournie avec Zabbix*

***[API Zabbix](/fr/manual/api)***

\- *L'API Zabbix vous permet d'utiliser le protocole JSON RPC pour
créer, mettre à jour et récupérer des objets Zabbix (comme des hôtes,
des éléments, des graphiques et autres) ou effectuer d'autres tâches
personnalisées*

***[serveur Zabbix](/fr/manual/concepts/server)***

\- *un processus central du logiciel Zabbix qui effectue la
surveillance, interagit avec les proxys et les agents Zabbix, calcule
les déclencheurs, envoie des notifications ; il peut être vu comme un
référentiel central de données*

***[agent Zabbix](/fr/manual/concepts/agent)***

\- *un processus déployé sur les cibles de surveillance pour superviser
activement les ressources locales et les applications*

***[proxy Zabbix](/fr/manual/concepts/proxy)***

\- *un processus qui peut collecter des données pour le compte du
serveur Zabbix, en supprimant une partie de la charge de traitement du
serveur*

[comment]: # ({/new-77400725})
