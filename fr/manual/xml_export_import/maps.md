[comment]: # translation:outdated

[comment]: # ({new-c7c352d1})
# 4 Network maps

[comment]: # ({/new-c7c352d1})

[comment]: # ({new-f69156a2})
#### Overview

Network map [export](/manual/xml_export_import) contains:

-   all related images
-   map structure - all map settings, all contained elements with their
    settings, map links and map link status indicators

Not exported are host groups, hosts, triggers, other maps or any other
elements that may be related to the exported map. Thus, if at least one
of the elements the map refers to is missing, importing it will fail.

Network map export/import is supported since Zabbix 1.8.2.

[comment]: # ({/new-f69156a2})

[comment]: # ({new-bc82aec8})
#### Exporting

To export network maps, do the following:

-   Go to: *Monitoring* → *Maps*
-   Mark the checkboxes of the network maps to export
-   Click on *Export* below the list

![](../../../assets/en/manual/xml_export_import/export_maps.png)

Selected maps are exported to a local XML file with default name
*zabbix\_export\_maps.xml*.

[comment]: # ({/new-bc82aec8})

[comment]: # ({new-cc31311c})
#### Importing

To import network maps, do the following:

-   Go to: *Monitoring* → *Maps*
-   Click on *Import* to the right
-   Select the import file
-   Mark the required options in import rules
-   Click on *Import*

![](../../../assets/en/manual/xml_export_import/import_maps.png)

All mandatory input fields are marked with a red asterisk.

A success or failure message of the import will be displayed in the
frontend.

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing maps will be updated with data taken from the import file. Otherwise they will not be updated.|
|*Create new*|The import will add new maps using data from the import file. Otherwise it will not add them.|

If you uncheck both map options and check the respective options for
images, images only will be imported. Image importing is only available
to Zabbix Super Admin users.

::: notewarning
If replacing an existing image, it will affect all
maps that are using this image.
:::

[comment]: # ({/new-cc31311c})

[comment]: # ({new-53418cf1})
#### Export format

Exporting a small network map with three elements, their images and some
links between them. Note that images are truncated to save space.

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2016-10-05T08:16:20Z</date>
    <images>
        <image>
            <name>Server_(64)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...SuQmCC</encodedImage>
        </image>
        <image>
            <name>Workstation_(64)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...SuQmCC</encodedImage>
        </image>
        <image>
            <name>Zabbix_server_3D_(96)</name>
            <imagetype>1</imagetype>
            <encodedImage>iVBOR...ggg==</encodedImage>
        </image>
    </images>
    <maps>
        <map>
            <name>Network</name>
            <width>590</width>
            <height>400</height>
            <label_type>0</label_type>
            <label_location>0</label_location>
            <highlight>1</highlight>
            <expandproblem>0</expandproblem>
            <markelements>1</markelements>
            <show_unack>0</show_unack>
            <severity_min>2</severity_min>
            <grid_size>40</grid_size>
            <grid_show>1</grid_show>
            <grid_align>1</grid_align>
            <label_format>0</label_format>
            <label_type_host>2</label_type_host>
            <label_type_hostgroup>2</label_type_hostgroup>
            <label_type_trigger>2</label_type_trigger>
            <label_type_map>2</label_type_map>
            <label_type_image>2</label_type_image>
            <label_string_host/>
            <label_string_hostgroup/>
            <label_string_trigger/>
            <label_string_map/>
            <label_string_image/>
            <expand_macros>0</expand_macros>
            <background/>
            <iconmap/>
            <urls/>
            <selements>
                <selement>
                    <elementtype>0</elementtype>
                    <label>Host 1</label>
                    <label_location>-1</label_location>
                    <x>476</x>
                    <y>28</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>8</selementid>
                    <elements>
                        <element>
                            <host>Discovered host</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Server_(64)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
                <selement>
                    <elementtype>0</elementtype>
                    <label>Zabbix server</label>
                    <label_location>-1</label_location>
                    <x>252</x>
                    <y>50</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>6</selementid>
                    <elements>
                        <element>
                            <host>Zabbix server</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Zabbix_server_3D_(96)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
                <selement>
                    <elementtype>0</elementtype>
                    <label>New host</label>
                    <label_location>-1</label_location>
                    <x>308</x>
                    <y>230</y>
                    <elementsubtype>0</elementsubtype>
                    <areatype>0</areatype>
                    <width>200</width>
                    <height>200</height>
                    <viewtype>0</viewtype>
                    <use_iconmap>0</use_iconmap>
                    <selementid>7</selementid>
                    <elements>
                        <element>
                            <host>Zabbix host</host>
                        </element>
                    </elements>
                    <icon_off>
                        <name>Workstation_(64)</name>
                    </icon_off>
                    <icon_on/>
                    <icon_disabled/>
                    <icon_maintenance/>
                    <application/>
                    <urls/>
                </selement>
            </selements>
            <links>
                <link>
                    <drawtype>0</drawtype>
                    <color>008800</color>
                    <label/>
                    <selementid1>6</selementid1>
                    <selementid2>8</selementid2>
                    <linktriggers/>
                </link>
                <link>
                    <drawtype>2</drawtype>
                    <color>00CC00</color>
                    <label>100MBps</label>
                    <selementid1>7</selementid1>
                    <selementid2>6</selementid2>
                    <linktriggers>
                        <linktrigger>
                            <drawtype>0</drawtype>
                            <color>DD0000</color>
                            <trigger>
                                <description>Zabbix agent on {HOST.NAME} is unreachable for 5 minutes</description>
                                <expression>{Zabbix host:agent.ping.nodata(5m)}=1</expression>
                                <recovery_expression/>
                            </trigger>
                        </linktrigger>
                    </linktriggers>
                </link>
            </links>
        </map>
    </maps>
</zabbix_export>
```

[comment]: # ({/new-53418cf1})

[comment]: # ({new-efe541b5})
#### Element tags

Element tag values are explained in the table below.

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|images|<|<|<|Root element for images.|
|image|<|<|<|Individual image.|
|<|name|`string`|<|Unique image name.|
|<|imagetype|`integer`|1 - image<br>2 - background|Image type.|
|<|encodedImage|<|<|Base64 encoded image.|
|maps|<|<|<|Root element for maps.|
|map|<|<|<|Individual map.|
|<|name|`string`|<|Unique map name.|
|<|width|`integer`|<|Map width, in pixels.|
|<|height|`integer`|<|Map height, in pixels.|
|<|label\_type|`integer`|0 - label<br>1 - host IP address<br>2 - element name<br>3 - status only<br>4 - nothing|Map element label type.|
|<|label\_location|`integer`|0 - bottom<br>1 - left<br>2 - right<br>3 - top|Map element label location by default.|
|<|highlight|`integer`|0 - no<br>1 - yes|Enable icon highlighting for active triggers and host statuses.|
|<|expandproblem|`integer`|0 - no<br>1 - yes|Display problem trigger for elements with a single problem.|
|<|markelements|`integer`|0 - no<br>1 - yes|Highlight map elements that have recently changed their status.|
|<|show\_unack|`integer`|0 - count of all problems<br>1 - count of unacknowledged problems<br>2 - count of acknowledged and unacknowledged problems separately|Problem display.|
|<|severity\_min|`integer`|0 - not classified<br>1 - information<br>2 - warning<br>3 - average<br>4 - high<br>5 - disaster|Minimum trigger severity to show on the map by default.|
|<|grid\_size|`integer`|20, 40, 50, 75 or 100|Cell size of a map grid in pixels, if "grid\_show=1"|
|<|grid\_show|`integer`|0 - yes<br>1 - no|Display a grid in map configuration.|
|<|grid\_align|`integer`|0 - yes<br>1 - no|Automatically align icons in map configuration.|
|<|label\_format|`integer`|0 - no<br>1 - yes|Use advanced label configuration.|
|<|label\_type\_host|`integer`|0 - label<br>1 - host IP address<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as host label, if "label\_format=1"|
|<|label\_type\_hostgroup|`integer`|0 - label<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as host group label, if "label\_format=1"|
|<|label\_type\_trigger|`integer`|0 - label<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as trigger label, if "label\_format=1"|
|<|label\_type\_map|`integer`|0 - label<br>2 - element name<br>3 - status only<br>4 - nothing<br>5 - custom label|Display as map label, if "label\_format=1"|
|<|label\_type\_image|`integer`|0 - label<br>2 - element name<br>4 - nothing<br>5 - custom label|Display as image label, if "label\_format=1"|
|<|label\_string\_host|`string`|<|Custom label for host elements, if "label\_type\_host=5"|
|<|label\_string\_hostgroup|`string`|<|Custom label for host group elements, if "label\_type\_hostgroup=5"|
|<|label\_string\_trigger|`string`|<|Custom label for trigger elements, if "label\_type\_trigger=5"|
|<|label\_string\_map|`string`|<|Custom label for map elements, if "label\_type\_map=5"|
|<|label\_string\_image|`string`|<|Custom label for image elements, if "label\_type\_image=5"|
|<|expand\_macros|`integer`|0 - no<br>1 - yes|Expand macros in labels in map configuration.|
|<|background|`id`|<|ID of the background image (if any), if "imagetype=2"|
|<|iconmap|`id`|<|ID of the icon mapping (if any).|
|urls|<|<|<|<|
|url|<|<|<|Individual URL.|
|<|name|`string`|<|Link name.|
|<|url|`string`|<|Link URL.|
|<|elementtype|`integer`|0 - host<br>1 - map<br>2 - trigger<br>3 - host group<br>4 - image|Map item type the link belongs to.|
|selements|<|<|<|<|
|selement|<|<|<|Individual map element.|
|<|elementtype|`integer`|0 - host<br>1 - map<br>2 - trigger<br>3 - host group<br>4 - image|Map element type.|
|<|label|`string`|<|Icon label.|
|<|label\_location|`integer`|-1 - use map default<br>0 - bottom<br>1 - left<br>2 - right<br>3 - top|<|
|<|x|`integer`|<|Location on the X axis.|
|<|y|`integer`|<|Location on the Y axis.|
|<|elementsubtype|`integer`|0 - single host group<br>1 - all host groups|Element subtype, if "elementtype=3"|
|<|areatype|`integer`|0 - same as whole map<br>1 - custom size|Area size, if "elementsubtype=1"|
|<|width|`integer`|<|Width of area, if "areatype=1"|
|<|height|`integer`|<|Height of area, if "areatype=1"|
|<|viewtype|`integer`|0 - place evenly in the area|Area placement algorithm, if "elementsubtype=1"|
|<|use\_iconmap|`integer`|0 - no<br>1 - yes|Use icon mapping for this element. Relevant only if iconmapping is activated on map level.|
|<|selementid|`id`|<|Unique element record ID.|
|<|application|`string`|<|Application name filter. If an application name is given, only problems of triggers that belong to the given application will be displayed on the map.|
|elements|<|<|<|<|
|element|<|<|<|Individual Zabbix entity that is represented on the map (map, hostgroup, host, etc).|
|<|host|<|<|<|
|icon\_off|<|<|<|Image to use when element is in 'OK' status.|
|icon\_on|<|<|<|Image to use when element is in 'Problem' status.|
|icon\_disabled|<|<|<|Image to use when element is disabled.|
|icon\_maintenance|<|<|<|Image to use when element is in maintenance.|
|<|name|`string`|<|Unique image name.|
|links|<|<|<|<|
|link|<|<|<|Individual link between map elements.|
|<|drawtype|`integer`|0 - line<br>2 - bold line<br>3 - dotted line<br>4 - dashed line|Link style.|
|<|color|`string`|<|Link color (6 symbols, hex).|
|<|label|`string`|<|Link label.|
|<|selementid1|`id`|<|ID of one element to connect.|
|<|selementid2|`id`|<|ID of the other element to connect.|
|linktriggers|<|<|<|<|
|linktrigger|<|<|<|Individual link status indicator.|
|<|drawtype|`integer`|0 - line<br>2 - bold line<br>3 - dotted line<br>4 - dashed line|Link style when trigger is in the 'Problem' state.|
|<|color|`string`|<|Link color (6 symbols, hex) when trigger is in the 'Problem' state.|
|trigger|<|<|<|Trigger used for indicating link status.|
|<|description|`string`|<|Trigger name.|
|<|expression|`string`|<|Trigger expression.|
|<|recovery\_expression|`string`|<|Trigger recovery expression.|

[comment]: # ({/new-efe541b5})
