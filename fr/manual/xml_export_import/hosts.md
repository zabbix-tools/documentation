[comment]: # translation:outdated

[comment]: # ({new-ae6d2ebd})
# 3 Hosts

[comment]: # ({/new-ae6d2ebd})

[comment]: # ({new-907c3faa})
#### Overview

Hosts are [exported](/manual/xml_export_import) with many related
objects and object relations.

Host export contains:

-   linked host groups
-   host data
-   template linkage
-   host group linkage
-   host interfaces
-   directly linked applications
-   directly linked items
-   directly linked triggers
-   directly linked graphs
-   directly linked discovery rules with all prototypes
-   directly linked web scenarios
-   host macros
-   host inventory data
-   value maps

When a host is imported and updated, it can only be linked to additional
templates and never be unlinked from any.

[comment]: # ({/new-907c3faa})

[comment]: # ({new-55d6da26})
#### Exporting

To export hosts, do the following:

-   Go to: *Configuration* → *Hosts*
-   Mark the checkboxes of the hosts to export
-   Click on *Export* below the list

![](../../../assets/en/manual/xml_export_import/export_hosts.png)

Selected hosts are exported to a local XML file with default name
*zabbix\_export\_hosts.xml*.

[comment]: # ({/new-55d6da26})

[comment]: # ({new-158cdcbf})
#### Importing

To import hosts, do the following:

-   Go to: *Configuration* → *Hosts*
-   Click on *Import* to the right
-   Select the import file
-   Mark the required options in import rules
-   Click on *Import*

![](../../../assets/en/manual/xml_export_import/import_hosts.png)

All mandatory input fields are marked with a red asterisk.

A success or failure message of the import will be displayed in the
frontend.

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing elements will be updated with data taken from the import file. Otherwise they will not be updated.|
|*Create new*|The import will add new elements using data from the import file. Otherwise it will not add them.|
|*Delete missing*|The import will remove existing elements not present in the import file. Otherwise it will not remove them.|

[comment]: # ({/new-158cdcbf})

[comment]: # ({new-93059e09})
#### Export format

``` {.xml}
<?xml version="1.0" encoding="UTF-8"?>
<zabbix_export>
    <version>4.0</version>
    <date>2018-04-03T08:11:42Z</date>
    <groups>
        <group>
            <name>Zabbix servers</name>
        </group>
    </groups>
    <hosts>
        <host>
            <host>Export host</host>
            <name>Export host</name>
            <description/>
            <proxy/>
            <status>0</status>
            <ipmi_authtype>-1</ipmi_authtype>
            <ipmi_privilege>2</ipmi_privilege>
            <ipmi_username/>
            <ipmi_password/>
            <tls_connect>1</tls_connect>
            <tls_accept>1</tls_accept>
            <tls_issuer/>
            <tls_subject/>
            <tls_psk_identity/>
            <tls_psk/>
            <templates/>
            <groups>
                <group>
                    <name>Zabbix servers</name>
                </group>
            </groups>
            <interfaces>
                <interface>
                    <default>1</default>
                    <type>1</type>
                    <useip>1</useip>
                    <ip>127.0.0.1</ip>
                    <dns/>
                    <port>10050</port>
                    <bulk>1</bulk>
                    <interface_ref>if1</interface_ref>
                </interface>
            </interfaces>
            <applications>
                <application>
                    <name>Application</name>
                </application>
            </applications>
            <items>
                <item>
                    <name>Item</name>
                    <type>0</type>
                    <snmp_community/>
                    <snmp_oid/>
                    <key>item.key</key>
                    <delay>30s</delay>
                    <history>90d</history>
                    <trends>365d</trends>
                    <status>0</status>
                    <value_type>3</value_type>
                    <allowed_hosts/>
                    <units/>
                    <snmpv3_contextname/>
                    <snmpv3_securityname/>
                    <snmpv3_securitylevel>0</snmpv3_securitylevel>
                    <snmpv3_authprotocol>0</snmpv3_authprotocol>
                    <snmpv3_authpassphrase/>
                    <snmpv3_privprotocol>0</snmpv3_privprotocol>
                    <snmpv3_privpassphrase/>
                    <params/>
                    <ipmi_sensor/>
                    <authtype>0</authtype>
                    <username/>
                    <password/>
                    <publickey/>
                    <privatekey/>
                    <port/>
                    <description/>
                    <inventory_link>0</inventory_link>
                    <applications>
                        <application>
                            <name>Application</name>
                        </application>
                    </applications>
                    <valuemap>
                        <name>Host status</name>
                    </valuemap>
                    <logtimefmt/>
                    <preprocessing/>
                    <jmx_endpoint/>
                    <timeout>3s</timeout>
                    <url/>
                    <query_fields/>
                    <posts/>
                    <status_codes>200</status_codes>
                    <follow_redirects>1</follow_redirects>
                    <post_type>0</post_type>
                    <http_proxy/>
                    <headers/>
                    <retrieve_mode>0</retrieve_mode>
                    <request_method>1</request_method>
                    <output_format>0</output_format>
                    <allow_traps>0</allow_traps>
                    <ssl_cert_file/>
                    <ssl_key_file/>
                    <ssl_key_password/>
                    <verify_peer>0</verify_peer>
                    <verify_host>0</verify_host>
                    <master_item/>
                    <interface_ref>if1</interface_ref>
                </item>
            </items>
            <discovery_rules/>
            <httptests/>
            <macros/>
            <inventory/>
        </host>
    </hosts>
    <triggers>
        <trigger>
            <expression>{Export host:item.key.last()}<>0</expression>
            <recovery_mode>0</recovery_mode>
            <recovery_expression/>
            <name>Item value not 0</name>
            <correlation_mode>0</correlation_mode>
            <correlation_tag/>
            <url/>
            <status>0</status>
            <priority>2</priority>
            <description/>
            <type>0</type>
            <manual_close>0</manual_close>
            <dependencies/>
            <tags/>
        </trigger>
    </triggers>
    <graphs>
        <graph>
            <name>Example graph</name>
            <width>900</width>
            <height>200</height>
            <yaxismin>0.0000</yaxismin>
            <yaxismax>100.0000</yaxismax>
            <show_work_period>1</show_work_period>
            <show_triggers>1</show_triggers>
            <type>0</type>
            <show_legend>1</show_legend>
            <show_3d>0</show_3d>
            <percent_left>0.0000</percent_left>
            <percent_right>0.0000</percent_right>
            <ymin_type_1>0</ymin_type_1>
            <ymax_type_1>0</ymax_type_1>
            <ymin_item_1>0</ymin_item_1>
            <ymax_item_1>0</ymax_item_1>
            <graph_items>
                <graph_item>
                    <sortorder>0</sortorder>
                    <drawtype>0</drawtype>
                    <color>1A7C11</color>
                    <yaxisside>0</yaxisside>
                    <calc_fnc>2</calc_fnc>
                    <type>0</type>
                    <item>
                        <host>Export host</host>
                        <key>item.key</key>
                    </item>
                </graph_item>
            </graph_items>
        </graph>
    </graphs>
    <value_maps>
        <value_map>
            <name>Host status</name>
            <mappings>
                <mapping>
                    <value>0</value>
                    <newvalue>Up</newvalue>
                </mapping>
                <mapping>
                    <value>2</value>
                    <newvalue>Unreachable</newvalue>
                </mapping>
            </mappings>
        </value_map>
    </value_maps>
</zabbix_export>
```

[comment]: # ({/new-93059e09})

[comment]: # ({new-2c61d3f8})
#### Element tags

Element tag values are explained in the table below.

[comment]: # ({/new-2c61d3f8})

[comment]: # ({new-eedfe2b9})
##### Host tags

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|groups|<|<|<|Root element for groups.|
|group|<|<|<|Individual group.|
|<|name|`string`|<|Unique group name.|
|hosts|<|<|<|Root element for hosts.|
|host|<|<|<|Individual host.|
|<|host|`string`|<|Unique host name.|
|<|name|`string`|<|Visible host name.|
|<|description|`text`|<|Host description.|
|<|status|`integer`|0 - monitored<br>1 - not monitored|Host status.|
|<|ipmi\_authtype|`integer`|-1 - default<br>0 - none<br>1 - MD2<br>2 - MD5<br>4 - straight<br>5 - OEM<br>6 - RMCP+|IPMI session authentication type.|
|<|ipmi\_privilege|`integer`|1 - callback<br>2 - user<br>3 - operator<br>4 - admin<br>5 - OEM|IPMI session privilege level.|
|<|ipmi\_username|`string`|<|Username for IPMI checks.|
|<|ipmi\_password|`string`|<|Password for IPMI checks.|
|<|tls\_connect|`integer`|1 - unencrypted<br>2 - TLS with PSK<br>4 - TLS with certificate|Type of outgoing connection.|
|<|tls\_accept|`integer`|1 - unencrypted<br>2 - TLS with PSK<br>3 - unencrypted and TLS with PSK<br>4 - TLS with certificate<br>5 - unencrypted and TLS with certificate<br>6 - TLS with PSK or certificate<br>7 - unencrypted and TLS with PSK or certificate|Type of incoming connection.|
|<|tls\_issuer|`string`|<|Allowed agent/proxy certificate issuer.|
|<|tls\_subject|`string`|<|Allowed agent/proxy certificate subject.|
|<|tls\_psk\_indentity|`string`|<|PSK identity string.|
|<|tls\_psk|`string`|<|PSK value string.|
|proxy|<|<|<|Proxy.|
|<|name|`string`|<|Name of the proxy (if any) that monitors the host.|
|templates|<|<|<|Root element for linked templates.|
|template|<|<|<|Individual template.|
|<|name|`string`|<|Template name.|
|interfaces|<|<|<|Root element for host interfaces.|
|interface|<|<|<|Individual interface.|
|<|default|`integer`|0 - secondary<br>1 - primary (default)|Interface status.<br>Only one primary interface of one type can be on a host.|
|<|type|`integer`|0 - unknown<br>1 - Zabbix agent<br>2 - SNMP<br>3 - IPMI<br>4 - JMX|Interface type.|
|<|useip|`integer`|0 - use DNS name<br>1 - use IP address|Interface to use for connecting to the host.|
|<|ip|`string`|<|IP address, can be either IPv4 or IPv6.|
|<|dns|`string`|<|DNS name.|
|<|port|`string`|<|Port number.|
|<|bulk|`integer`|0 - disable<br>1 - enable|Use bulk requests for SNMP.|
|<|interface\_ref|`string`|<|Interface reference name to be used in items.|
|applications|<|<|<|Root element for applications.|
|application|<|<|<|Individual application.|
|<|name|<|<|Application name.|
|macros|<|<|<|Root element for macros.|
|macro|<|<|<|Individual macro.|
|<|name|<|<|User macro name.|
|<|value|<|<|User macro value.|

[comment]: # ({/new-eedfe2b9})

[comment]: # ({new-8a2e4e2f})
##### Host item tags

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|items|<|<|<|Root element for items.|
|item|<|<|<|Individual item.|
|<|name|`string`|<|Item name.|
|<|type|`integer`|0 - Zabbix agent<br>1 - SNMPv1 agent<br>2 - Zabbix trapper<br>3 - simple check<br>4 - SNMPv2 agent<br>5 - internal<br>6 - SNMPv3 agent<br>7 - Zabbix agent (active)<br>8 - aggregate<br>9 - HTTP test (web monitoring scenario step)<br>10 - external<br>11 - database monitor<br>12 - IPMI agent<br>13 - SSH agent<br>14 - Telnet agent<br>15 - calculated<br>16 - JMX agent<br>17 - SNMP trap<br>18 - Dependent item<br>19 - HTTP agent item|Item type.|
|<|snmp\_community|`string`|<|SNMP community name if 'type' is 1,4.|
|<|snmp\_oid|`string`|<|SNMP object ID.|
|<|key|`string`|<|Item key.|
|<|delay|`string`|<|Update interval of the item. Seconds, time unit with suffix, custom intervals or user macros.|
|<|history|`string`|<|A time unit of how long the history data should be stored. Time unit with suffix or user macro.|
|<|trends|`string`|<|A time unit of how long the trends data should be stored. Time unit with suffix or user macro.|
|<|status|`integer`|0 - enabled<br>1 - disabled|Item status.|
|<|value\_type|`integer`|0 - float<br>1 - character<br>2 - log<br>3 - unsigned integer<br>4 - text|Received value type.|
|<|allowed\_hosts|`string`|<|List of IP addresses (comma delimited) of hosts allowed sending data for the item if 'type' is 2 or 19.|
|<|units|`string`|<|Units of returned values (bps, B).|
|<|snmpv3\_contextname|`string`|<|SNMPv3 context name.|
|<|snmpv3\_securityname|`string`|<|SNMPv3 security name.|
|<|snmpv3\_securitylevel|`integer`|0 - noAuthNoPriv<br>1 - authNoPriv<br>2 - authPriv|SNMPv3 security level.|
|<|snmpv3\_authprotocol|`integer`|0 - MD5<br>1 - SHA|SNMPv3 authentication protocol.|
|<|snmpv3\_authpassphrase|`string`|<|SNMPv3 authentication passphrase.|
|<|snmpv3\_privprotocol|`integer`|0 - DES<br>1 - AES|SNMPv3 privacy protocol.|
|<|snmpv3\_privpassphrase|`string`|<|SNMPv3 privacy passphrase.|
|<|params|`text`|<|Name of the "Executed script" if 'type' is 13,14<br>"SQL query" field if 'type' is 11<br>"Formula" field if 'type' is 15.|
|<|ipmi\_sensor|`string`|<|IPMI sensor ID if 'type' is 12.|
|<|authtype|`integer`|Authentication type for SSH agent items:<br>0 - password<br>1 - key<br><br>Authentication type for HTTP agent items:<br>0 - none<br>1 - basic<br>2 - NTLM|Authentication type if 'type' is 13 or 19.|
|<|username|`string`|<|User name if 'type' is 11,13,14,19.|
|<|password|`string`|<|Password if 'type' is 11,13,14,19.|
|<|publickey|`string`|<|Name of the public key file if 'type' is 13.|
|<|privatekey|`string`|<|Name of the private key file if 'type' is 13.|
|<|port|`string`|<|Custom port for the item.|
|<|description|`text`|<|Item description.|
|<|inventory\_link|`integer`|0 - no link<br>*number* - number of field in the 'host\_inventory' table|Use item value to populate this inventory field.|
|<|logtimefmt|`string`|<|Format of the time in log entries. Used only by log items.|
|<|interface\_ref|`string`|<|Reference to host interface.|
|<|jmx\_endpoint|`string`|<|JMX endpoint if 'type' is 16.|
|<|url|`string`|<|URL string if 'type' is 19.|
|<|allow\_traps|`integer`|0 - Do not allow trapping.<br>1 - Allow trapping.|Property to allow to send data to item if 'type' is 19.|
|<|follow\_redirects|`integer`|0 - Do not follow redirects.<br>1 - Follow redirects.|Follow HTTP redirects if 'type' is 19.|
|<|headers|`object`|<|Object with HTTP(S) request headers if 'type' is 19.|
|<|http\_proxy|`string`|<|HTTP(S) proxy connection string if 'type' is 19.|
|<|output\_format|`integer`|0 - Store as is.<br>1 - Convert to JSON.|How to process response if 'type' is 19.|
|<|post\_type|`integer`|0 - Raw data.<br>2 - JSON data.<br>3 - XML data.|Type of request body if 'type' is 19.|
|<|posts|`text`|<|Request body if 'type' is 19.|
|<|query\_fields|`array`|<|Array of objects for request query fields if 'type' is 19.|
|<|request\_method|`integer`|0 - GET<br>1 - POST<br>2 - PUT<br>3 - HEAD|Request method if 'type' is 19.|
|<|retrieve\_mode|`integer`|0 - Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.|What part of response should be stored if 'type' is 19.|
|<|ssl\_cert\_file|`string`|<|Public SSL Key file path if 'type' is 19.|
|<|ssl\_key\_file|`string`|<|Private SSL Key file path if 'type' is 19.|
|<|ssl\_key\_password|`string`|<|Password for SSL Key file if 'type' is 19.|
|<|status\_codes|`string`|<|Ranges of required HTTP status codes separated by commas if 'type' is 19.|
|<|timeout|`string`|<|Item data polling request timeout if 'type' is 19.|
|<|verify\_host|`integer`|0 - Do not validate.<br>1 - Validate.|Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate if 'type' is 19.|
|<|verify\_peer|`integer`|0 - Do not validate.<br>1 - Validate.|Validate is host certificate authentic if 'type' is 19.|
|value map|<|<|<|Value map.|
|<|name|`string`|<|Name of the value map to use for the item.|
|applications|<|<|<|Root element for applications.|
|application|<|<|<|Individual application.|
|<|name|<|<|Application name.|
|preprocessing|<|<|<|Item value preprocessing.|
|step|<|<|<|Individual item value preprocessing step.|
|<|type|`integer`|1 - custom multiplier<br>2 - right trim<br>3 - left trim<br>4 - trim from both sides<br>5 - regular expression matching<br>6 - boolean to decimal<br>7 - octal to decimal<br>8 - hexadecimal to decimal<br>9 - simple change; calculated as (received value-previous value)<br>10 - change per second; calculated as (received value-previous value)/(time now-time of last check)|Type of the item value preprocessing step.|
|<|params|`string`|<|Parameters of the item value preprocessing step.|
|master\_item|<|<|<|Individual item master item data.|
|<|key|`string`|<|Dependent item master item key value.|

[comment]: # ({/new-8a2e4e2f})

[comment]: # ({new-19e8eae6})
##### Host low-level discovery rule tags

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|discovery\_rules|<|<|<|Root element for low-level discovery rules.|
|discovery\_rule|<|<|<|Individual low-level discovery rule.|
|<|*For most of the element tag values, see element tag values for a regular item. Only the tags that are specific to low-level discovery rules, are described below.*|<|<|<|
|<|lifetime|`string`|<|Time period after which items that are no longer discovered will be deleted. Seconds, time unit with suffix or user macro.|
|filter|<|<|<|Individual filter.|
|<|evaltype|`integer`|0 - And/or logic<br>1 - And logic<br>2 - Or logic<br>3 - custom formula|Logic to use for checking low-level discovery rule filter conditions.|
|<|formula|`string`|<|Custom calculation formula for filter conditions.|
|<|conditions|<|<|Root element for filter conditions.|
|condition|<|<|<|Individual filter condition.|
|<|macro|`string`|<|Low-level discovery macro name.|
|<|value|`string`|<|Filter value: regular expression or global regular expression.|
|<|operator|`integer`|<|<|
|<|formulaid|`character`|<|Filter condition ID. Used in the custom calculation formula.|
|item\_prototypes|<|<|<|Root element for item\_prototypes.|
|item\_prototype|<|<|<|Individual item\_prototype.|
|<|*For most of the element tag values, see element tag values for a regular item. Only the tags that are specific to item\_prototypes, are described below.*|<|<|<|
|application\_prototypes|<|<|<|Root element for application prototypes.|
|application\_prototype|<|<|<|Individual application prototype.|
|<|name|<|<|Application prototype name.|
|master\_item\_prototype|<|<|<|Individual item prototype master item prototype data.|
|<|key|`string`|<|Dependent item prototype master item prototype key value.|

[comment]: # ({/new-19e8eae6})

[comment]: # ({new-5ff5f79a})
##### Host trigger tags

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|triggers|<|<|<|Root element for triggers.|
|trigger|<|<|<|Individual trigger.|
|<|expression|`string`|<|Trigger expression.|
|<|recovery\_mode|`integer`|0 - expression<br>1 - recovery expression<br>2 - none|Basis for generating OK events.|
|<|recovery\_expression|`string`|<|Trigger recovery expression.|
|<|name|`string`|<|Trigger name.|
|<|correlation\_mode|`integer`|0 - no event correlation<br>1 - event correlation by tag|Correlation mode.|
|<|correlation\_tag|`string`|<|The tag name to be used for event correlation.|
|<|url|`string`|<|Trigger URL.|
|<|status|`integer`|0 - enabled<br>1 - disabled|Trigger status.|
|<|priority|`integer`|0 - not classified<br>1 - information<br>2 - warning<br>3 - average<br>4 - high<br>5 - disaster|Trigger severity.|
|<|description|`text`|<|Trigger description.|
|<|type|`integer`|0 - single problem event<br>1 - multiple problem events|Event generation type.|
|<|manual\_close|`integer`|0 - not allowed<br>1 - allowed|Manual closing of problem events.|
|dependencies|<|<|<|Root element for dependencies.|
|dependency|<|<|<|Individual dependency.|
|<|name|`string`|<|Dependency trigger name.|
|<|expression|`string`|<|Dependency trigger expression.|
|<|recovery\_expression|`string`|<|Dependency trigger recovery expression.|
|tags|<|<|<|Root element for event tags.|
|tag|<|<|<|Individual event tag.|
|<|tag|`string`|<|Tag name.|
|<|value|`string`|<|Tag value.|

[comment]: # ({/new-5ff5f79a})

[comment]: # ({new-0a8a430a})
##### Host graph tags

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|graphs|<|<|<|Root element for graphs.|
|graph|<|<|<|Individual graph.|
|<|name|`string`|<|Graph name.|
|<|width|`integer`|<|Graph width, in pixels. Used for preview and for pie/exploded graphs.|
|<|height|`integer`|<|Graph height, in pixels. Used for preview and for pie/exploded graphs.|
|<|yaxismin|`double`|<|Value of Y axis minimum if 'ymin\_type\_1' is 1.|
|<|yaxismax|`double`|<|Value of Y axis maximum if 'ymax\_type\_1' is 1.|
|<|show\_work\_period|`integer`|0 - no<br>1 - yes|Highlight non-working hours if 'type' is 0,1.|
|<|show\_triggers|`integer`|0 - no<br>1 - yes|Display simple trigger values as a line if 'type' is 0,1.|
|<|type|`integer`|0 - normal<br>1 - stacked<br>2 - pie<br>3 - exploded<br>4 - 3D pie<br>5 - 3D exploded|Graph type.|
|<|show\_legend|`integer`|0 - no<br>1 - yes|Display graph legend.|
|<|show\_3d|`integer`|0 - 2D<br>1 - 3D|Enable 3D style if 'type' is 2,3.|
|<|percent\_left|`double`|<|Show the percentile line for left axis if 'type' is 0.|
|<|percent\_right|`double`|<|Show the percentile line for right axis if 'type' is 0.|
|<|ymin\_type\_1|`integer`|0 - calculated<br>1 - fixed<br>2 - last value of the selected item|Minimum value of Y axis if 'type' is 0,1.|
|<|ymax\_type\_1|`integer`|0 - calculated<br>1 - fixed<br>2 - last value of the selected item|Maximum value of Y axis if 'type' is 0,1.|
|<|ymin\_item\_1|`string`|null or item details|Item details if 'ymin\_type\_1' is 2.|
|<|ymax\_item\_1|`string`|null or item details|Item details if 'ymax\_type\_1' is 2.|
|graph\_items|<|<|<|Root element for graph items.|
|graph\_item|<|<|<|Individual graph item.|
|<|sortorder|`integer`|<|Draw order. The smaller value is drawn first. Can be used to draw lines or regions behind (or in front of) another.|
|<|drawtype|`integer`|0 - single line<br>1 - filled region<br>2 - bold line<br>3 - dotted line<br>4 - dashed line|Draw style if graph 'type' is 0.|
|<|color|`string`|<|Element colour (6 symbols, hex).|
|<|yaxisside|`integer`|0 - left axis<br>1 - right axis|Y axis position (left or right) the element belongs to if graph 'type' is 0,1.|
|<|calc\_fnc|`integer`|1 - minimum<br>2 - average<br>4 - maximum<br>7 - all (minimum, average and maximum, if graph 'type' is 0)<br>9 - last (if graph 'type' is not 0,1)|Data to draw if more than one value exists for an item.|
|<|type|`integer`|1 - value of the item is represented proportionally on the pie<br>2 - value of the item represents the whole pie (graph sum)|Draw type for pie/exploded graphs.|
|item|<|<|<|Individual item.|
|<|host|`string`|<|Item host.|
|<|key|`string`|<|Item key.|

[comment]: # ({/new-0a8a430a})

[comment]: # ({new-cae5eb15})
##### Host web scenario tags

|Element|Element property|Type|Range|Description|
|-------|----------------|----|-----|-----------|
|httptests|<|<|<|Root element for web scenarios.|
|httptest|<|<|<|Individual web scenario.|
|<|name|`string`|<|Web scenario name.|
|<|delay|`string`|<|Frequency of executing the web scenario. Seconds, time unit with suffix or user macro.|
|<|attempts|`integer`|1-10|The number of attempts for executing web scenario steps.|
|<|agent|`string`|<|Client agent. Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.|
|<|http\_proxy|`string`|<|Specify an HTTP proxy to use, using the format: `http://[username[:password]@]proxy.mycompany.com[:port]`|
|<|variables|`text`|<|List of scenario-level variables (macros) that may be used in scenario steps.|
|<|headers|`text`|<|HTTP headers that will be sent when performing a request.|
|<|status|`integer`|0 - enabled<br>1 - disabled|Web scenario status.|
|<|authentication|`integer`|0 - none<br>1 - basic<br>2 - NTLM|Authentication method.|
|<|http\_user|`string`|<|Authentication user name.|
|<|http\_password|`string`|<|Authentication password for specified user name.|
|<|verify\_peer|`integer`|0 - no<br>1 - yes|Verify the SSL certificate of the web server.|
|<|verify\_host|`integer`|0 - no<br>1 - yes|Verify that the Common Name field or the Subject Alternate Name field of the web server certificate matches.|
|<|ssl\_cert\_file|`string`|<|Name of the SSL certificate file used for client authentication.|
|<|ssl\_key\_file|`string`|<|Name of the SSL private key file used for client authentication.|
|<|ssl\_key\_password|`string`|<|SSL private key file password.|
|steps|<|<|<|Root element for web scenario steps.|
|step|<|<|<|Individual web scenario step.|
|<|name|`string`|<|Web scenario step name.|
|<|url|`string`|<|URL for monitoring.|
|<|posts|`text`|<|List of 'Post' variables.|
|<|variables|`text`|<|List of step-level variables (macros) that should be applied after this step.<br><br>If the variable value has a 'regex:' prefix, then its value is extracted from the data returned by this step according to the regular expression pattern following the 'regex:' prefix|
|<|headers|`text`|<|HTTP headers that will be sent when performing a request.|
|<|follow\_redirects|`integer`|0 - no<br>1 - yes|Follow HTTP redirects.|
|<|retrieve\_mode|`integer`|0 - content<br>1 - headers only|HTTP response retrieve mode.|
|<|timeout|`string`|<|Timeout of step execution. Seconds, time unit with suffix or user macro.|
|<|required|`string`|<|Required string. Ignored if empty.|
|<|status\_codes|`string`|<|A comma delimited list of accepted status codes. Ignored if empty. For example: 200-201,210-299|

[comment]: # ({/new-cae5eb15})

[comment]: # ({new-869bd76e})
##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"ZABBIX\_ACTIVE") without the numbering used in this table. The numbers
for range values (corresponding to the API values) in this table is used
for ordering only.

[comment]: # ({/new-869bd76e})
