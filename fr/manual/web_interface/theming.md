[comment]: # translation:outdated

[comment]: # ({new-d1036ff8})
# 7 Creating your own theme

[comment]: # ({/new-d1036ff8})

[comment]: # ({new-cb54c172})
#### Overview

By default, Zabbix provides a number of predefined themes. You may
follow the step-by-step procedure provided here in order to create your
own. Feel free to share the result of your work with Zabbix community if
you created something nice.

[comment]: # ({/new-cb54c172})

[comment]: # ({new-be6afd90})
##### Step 1

To define your own theme you'll need to create a CSS file and save it in
the *styles/* folder (for example, *custom-theme.css*). You can either
copy the files from a different theme and create your theme based on it
or start from scratch.

[comment]: # ({/new-be6afd90})

[comment]: # ({new-34bae249})
##### Step 2

Add your theme to the list of themes returned by the Z::getThemes()
method. You can do this by overriding the ZBase::getThemes() method in
the Z class. This can be done by adding the following code before the
closing brace in *include/classes/core/Z.php*:

      public static function getThemes() {
          return array_merge(parent::getThemes(), array(
              'custom-theme' => _('Custom theme')
          ));
      }

::: noteimportant
Note that the name you specify within the first
pair of quotes must match the name of the theme file without
extension.
:::

To add multiple themes, just list them under the first theme, for
example:

      public static function getThemes() {
          return array_merge(parent::getThemes(), array(
              'custom-theme' => _('Custom theme'),
              'anothertheme' => _('Another theme'),
              'onemoretheme' => _('One more theme')
          ));
      }

Note that every theme except the last one must have a trailing comma.

::: notetip
To change graph colours, the entry must be added in the
*graph\_theme* database table.
:::

[comment]: # ({/new-34bae249})

[comment]: # ({new-b67ccba3})
##### Step 3

Activate the new theme.

In Zabbix frontend, you may either set this theme to be the default one
or change your theme in the user profile.

Enjoy the new look and feel!

[comment]: # ({/new-b67ccba3})
