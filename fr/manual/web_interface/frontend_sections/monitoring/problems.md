[comment]: # translation:outdated

[comment]: # ({new-621a60b9})
# 2 Problems

[comment]: # ({/new-621a60b9})

[comment]: # ({new-d98c719a})
#### Overview

In *Monitoring → Problems* you can see what problems you currently have.
Problems are those triggers that are in the "Problem" state.

![](../../../../../assets/en/manual/web_interface/problems.png){width="600"}

|Column|Description|
|------|-----------|
|*Time*|Problem start time is displayed.|
|*Severity*|Problem severity is displayed.<br>Problem severity is based on the severity of the underlying problem trigger. Colour of the trigger severity is used as cell background. For resolved problems, green background is used.|
|*Recovery time*|Problem resolution time is displayed.|
|*Status*|Problem status is displayed:<br>**Problem** - unresolved problem<br>**Resolved** - recently resolved problem. You can hide recently resolved problems using the filter.<br>New and recently resolved problems blink for 30 minutes. Resolved problems are displayed for 30 minutes in total. Both of these values are configurable in *Administration* → *General* → *[Trigger displaying options](/manual/web_interface/frontend_sections/administration/general#trigger_displaying_options)*.|
|*Info*|An green information icon is displayed if a problem is closed by global correlation or manually by acknowledgement. Rolling a mouse over the icon will display more details:<br>![info2.png](../../../../../assets/en/manual/web_interface/info2.png){width="300"}|
|*Host*|Problem host is displayed.|
|*Problem*|Problem name is displayed.<br>Problem name is based on the name of the underlying problem trigger.|
|*Duration*|Problem duration is displayed.<br>See also: [negative problem duration](#negative_problem_duration)|
|*Ack*|The acknowledgement status of the problem is displayed:<br>**Yes** - green text indicating that the problem is acknowledged. A problem is considered to be acknowledged if all events for it are acknowledged.<br>**No** - a red link indicating unacknowledged events.<br>If you click on the link you will be taken to a bulk acknowledgement screen where all problems for this trigger can be acknowledged at once.<br>This column is displayed if problem acknowledgement is activated in *Administration* → *General*.|
|*Actions*|[Action](/manual/config/notifications/action) status is displayed:<br>**In progress** - action is being taken<br>**Done** - action is completed<br>**Failures** - action has failed<br>The number of actions taken on the problem (such as notifications sent or executed remote commands) is also displayed.|
|*Tags*|[Event tags](/manual/config/triggers/event_tags) are displayed (if any).|

[comment]: # ({/new-d98c719a})

[comment]: # ({new-8fa0f11a})
#### Negative problem duration

It is actually possible in some common situations to have negative
problem duration i.e. when the problem resolution time is earlier than
problem creation time, e. g.:

-   If some host is monitored by proxy and a network error happens,
    leading to no data received from the proxy for a while, the
    item.nodata() trigger will be fired by the server. When the
    connection is restored, the server will receive item data from the
    proxy having a time from the past. Then, the item.nodata() problem
    will be resolved and it will have a negative problem duration;
-   When item data that resolve the problem event are sent by Zabbix
    sender and contain a timestamp earlier than the problem creation
    time, a negative problem duration will also be displayed.

[comment]: # ({/new-8fa0f11a})

[comment]: # ({new-1848ebdc})
##### Using filter

You can use the filter to display only the problems you are interested
in. The filter is located above the table.

![](../../../../../assets/en/manual/web_interface/problems_filter_new.png){width="600"}

|Parameter|Description|
|---------|-----------|
|*Show*|Filter by problem status:<br>**Recent problems** - unresolved and recently resolved problems are displayed (default)<br>**Problems** - unresolved problems are displayed<br>**History** - history of all events is displayed|
|*Host group*|Filter by one or more host groups.<br>Specifying a parent host group implicitly selects all nested host groups.|
|*Host*|Filter by one or more hosts.|
|*Application*|Filter by application name.|
|*Trigger*|Filter by one or more triggers.|
|*Problem*|Filter by problem name.|
|*Minimum trigger severity*|Filter by minimum trigger severity.|
|*Age less than*|Filter by how old the problem is.|
|*Host inventory*|Filter by inventory type and value.|
|*Tags*|Filter by event tag name and value.<br>Several conditions can be set. There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having same tag name will be grouped by Or condition<br>**Or** - enough if one condition is met<br>There are two ways of matching the tag value:<br>**Like** - similar string match<br>**Equal** - exact case-sensitive string match<br>|
|*Show tags*|Select the number of displayed tags:<br>**None**- no *Tags* column in *Monitoring → Problems*<br>**1**- *Tags* column contains one tag<br>**2**- *Tags* column contains two tags<br>**3**- *Tags* column contains three tags<br>To see all tags for the problem roll your mouse over the three dots icon.|
|*Show hosts in maintenance*|Mark the checkbox to display problems of hosts in maintenance too.|
|*Compact view*|Mark the checkbox to enable compact view.|
|*Show details*|Mark the checkbox to display underlying trigger expressions of the problems. Disabled if *Compact view* checkbox is marked.|
|*Show unacknowledged only*|Mark the checkbox to display unacknowledged problems only.|
|*Show timeline*|Timeline row and grouping is visible when the checkbox is marked. Disabled if *Compact view* checkbox is marked.|
|*Highlight whole row*|Full line highlighting for unresolved problems is enabled when the checkbox is marked. Enabled only if *Compact view* checkbox is marked.|

[comment]: # ({/new-1848ebdc})

[comment]: # ({new-462c4f50})
#### Viewing details

The times for problem start and recovery in *Monitoring → Problems* are
links. Clicking on them opens more details of the event.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/event_details.png){width="600"}

[comment]: # ({/new-462c4f50})

[comment]: # ({new-eaadfbbc})
##### Buttons

The button to the right offers the following option:

|   |   |
|---|---|
|![](../../../../../assets/en/manual/web_interface/button_csv.png)|Export content from all pages to a CSV file.|

View mode buttons, being common for all sections, are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-eaadfbbc})

[comment]: # ({new-5dda91fc})
#### Using filter

You can use the filter to display only the problems you are interested
in. For better search performance, data is searched with macros
unresolved.

The filter is located above the table. Favorite filter settings can be
saved as tabs and then quickly accessed by clicking on the [tabs above
the
filter](/manual/web_interface/frontend_sections/monitoring/problems#tabs_for_favorite_filters).

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/problem_filter.png){width="600"}

|Parameter|Description|
|---------|-----------|
|*Show*|Filter by problem status:<br>**Recent problems** - unresolved and recently resolved problems are displayed (default)<br>**Problems** - unresolved problems are displayed<br>**History** - history of all events is displayed|
|*Host groups*|Filter by one or more host groups.<br>Specifying a parent host group implicitly selects all nested host groups.|
|*Hosts*|Filter by one or more hosts.|
|*Triggers*|Filter by one or more triggers.|
|*Problem*|Filter by problem name.|
|*Severity*|Filter by trigger (problem) severity.|
|*Age less than*|Filter by how old the problem is.|
|*Host inventory*|Filter by inventory type and value.|
|*Tags*|Filter by [event tag](/manual/config/tagging) name and value. It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met<br>When filtered, the tags specified here will be displayed first with the problem, unless overridden by the *Tag display priority* (see below) list.|
|*Show tags*|Select the number of displayed tags:<br>**None** - no *Tags* column in *Monitoring → Problems*<br>**1** - *Tags* column contains one tag<br>**2** - *Tags* column contains two tags<br>**3** - *Tags* column contains three tags<br>To see all tags for the problem roll your mouse over the three dots icon.|
|*Tag name*|Select tag name display mode:<br>**Full** - tag names and values are displayed in full<br>**Shortened** - tag names are shortened to 3 symbols; tag values are displayed in full<br>**None** - only tag values are displayed; no names|
|*Tag display priority*|Enter tag display priority for a problem, as a comma-separated list of tags (for example: `Services,Applications,Application`). Tag names only should be used, no values. The tags of this list will always be displayed first, overriding the natural ordering by alphabet.|
|*Show operational data*|Select the mode for displaying [operational data](#operational_data_of_problems):<br>**None** - no operational data is displayed<br>**Separately** - operational data is displayed in a separate column<br>**With problem name** - append operational data to the problem name, using parentheses for the operational data|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Compact view*|Mark the checkbox to enable compact view.|
|*Show details*|Mark the checkbox to display underlying trigger expressions of the problems. Disabled if *Compact view* checkbox is marked.|
|*Show unacknowledged only*|Mark the checkbox to display unacknowledged problems only.|
|*Show timeline*|Mark the checkbox to display the visual timeline and grouping. Disabled if *Compact view* checkbox is marked.|
|*Highlight whole row*|Mark the checkbox to highlight the full line for unresolved problems. The problem severity color is used for highlighting.<br>Enabled only if the *Compact view* checkbox is marked in the standard blue and dark themes. *Highlight whole row* is not available in the high-contrast themes.|

[comment]: # ({/new-5dda91fc})

[comment]: # ({new-6c375926})
##### Tabs for favorite filters

Frequently used sets of filter parameters can be saved in tabs.

To save a new set of filter parameters, open the Home tab, and configure
the filter settings, then press the *Save as* button. In a new popup
window, define *Filter properties*.

![problem\_filter0.png](../../../../../assets/en/manual/web_interface/problem_filter0.png)

|Parameter|Description|
|---------|-----------|
|Name|The name of the filter to display in the tab list.|
|Show number of records|Check, if you want the number of problems to be displayed next to the tab name.|
|Set custom time period|Check to set specific default time period for this filter set. If set, you will only be able to change the time period for this tab by updating filter settings. For tabs without a custom time period, the time range can be changed by pressing the time selector button in the top right corner (button name depends on selected time interval: This week, Last 30 minutes, Yesterday, etc.).<br>This option is available only for filters in *Monitoring→Problems*.|
|From/To|[Time period](/manual/config/visualization/graphs/simple#time_period_selector) start and end in absolute (`Y-m-d H:i:s`) or relative time syntax (`now-1d`). Available, if *Set custom time period* is checked.|

To edit *Filter properties* of an existing filter, press the gear symbol
next to the active tab name.

![problem\_filter2.png](../../../../../assets/en/manual/web_interface/problem_filter2.png)

Notes:

-   To hide a filter, press on the name of the current tab. Press on the
    active tab name again to open the filter.
-   Filter tabs can be re-arranged by dragging and dropping.
-   Keyboard navigation is supported: use arrows to switch between tabs,
    press *Enter* to open.
-   Pressing the arrow down icon in the upper right corner will open the
    full list of saved filter tabs as a drop-down menu.

::: noteclassic
 To share filters, copy and send to others a URL of an
active filter. After opening this URL, other users will be able to save
this set of parameters as a permanent filter in their Zabbix account.\
See also: [Page
parameters](/manual/web_interface/page_parameters).
:::

[comment]: # ({/new-6c375926})

[comment]: # ({new-75aa326d})
##### Filter buttons

|   |   |
|---|---|
|![filter\_apply.png](../../../../../assets/en/manual/web_interface/filter_apply.png)|Apply specified filtering criteria (without saving).|
|![filter\_reset.png](../../../../../assets/en/manual/web_interface/filter_reset.png)|Reset current filter and return to saved parameters of the current tab. On the Home tab, this will clear the filter.|
|![filter\_save\_as.png](../../../../../assets/en/manual/web_interface/filter_save_as.png)|Save current filter parameters in a new tab. Only available on the *Home* tab.|
|![filter\_update.png](../../../../../assets/en/manual/web_interface/filter_update.png)|Replace tab parameters with currently specified parameters. Not available on the *Home* tab.|

[comment]: # ({/new-75aa326d})


[comment]: # ({new-bd839887})
#### Viewing details

The times for problem start and recovery in *Monitoring → Problems* are
links. Clicking on them opens more details of the event.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/event_details.png){width="600"}

Note how the problem severity differs for the trigger and the problem
event - for the problem event it has been updated using the *Update
problem* [screen](/manual/acknowledges#updating_problems).

In the action list, the following icons are used to denote the activity
type:

-   ![icon\_generated.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_generated.png) -
    problem event generated
-   ![icon\_message.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_message.png) -
    message has been sent
-   ![icon\_acknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_acknowledged.png) -
    problem event acknowledged
-   ![icon\_unacknowledged.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_unacknowledged.png) -
    problem event unacknowledged
-   ![icon\_comment2.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_comment2.png) -
    a comment has been added
-   ![icon\_sev\_up1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_up1.png) -
    problem severity has been increased (e.g. Information → Warning)
-   ![icon\_sev\_down1.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_sev_down1.png) -
    problem severity has been decreased (e.g. Warning → Information)
-   ![icon\_severity\_back.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_severity_back.png) -
    problem severity has been changed, but returned to the original
    level (e.g. Warning → Information → Warning)
-   ![icon\_remote.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_remote.png) -
    a remote command has been executed
-   ![icon\_recovery.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_recovery.png) -
    problem event has recovered
-   ![icon\_closed.png](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/icon_closed.png) -
    the problem has been closed manually

[comment]: # ({/new-bd839887})
