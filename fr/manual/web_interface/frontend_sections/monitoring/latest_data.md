[comment]: # translation:outdated

[comment]: # ({new-1c52788a})
# 5 Latest data

[comment]: # ({/new-1c52788a})

[comment]: # ({new-a86c547b})
#### Overview

The section in *Monitoring → Latest data* can be used to view latest
values gathered by items as well as to access various graphs for the
items.

When you open this page for the first time, nothing is displayed.

![](../../../../../assets/en/manual/web_interface/latest_data_default.png){width="600"}

To access data, you need to make selections in the filter such as host
group, host, application or item name.

![](../../../../../assets/en/manual/web_interface/latest_data.png){width="600"}

In the list displayed, click on
![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/expand.png)
before a host and the relevant application to reveal latest values of
that host and application. You can expand all hosts and all
applications, thus revealing all items by clicking on
![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/expand.png)
in the header row.

*Note*: The name of a disabled host is displayed in red. Data of
disabled hosts, including graphs and item value lists, is accessible in
*Latest data* since Zabbix 2.2.0.

Items are displayed with their name, last check time, last value, change
amount and a link to a simple graph/history of item values.

Only values that fall within the last 24 hours are displayed by default.
This limit has been introduced with the aim of improving initial loading
times for large pages of latest data. It is also possible to change this
limitation by changing the value of ZBX\_HISTORY\_PERIOD
[constant](/fr/manual/web_interface/definitions) in
*include/defines.inc.php*.

**Using filter**

You can use the filter to display only the items you are interested in.
The *Filter* link is located above the table in the middle. You can use
it to filter items by host group, host, application, a string in the
item name; you can also select to display items that have no data
gathered.

Specifying a parent host group implicitly selects all nested host
groups.

*Show details* allows to extend displayable information on the items.
Such details as refresh interval, history and trends settings, item type
and item errors (fine/unsupported) are displayed. A link to item
configuration is also available.

![](../../../../../assets/en/manual/web_interface/latest_data2.png){width="600"}

By default, items without data are shown but details are not displayed.

**Ad-hoc graphs for comparing items**

You may use the checkbox in the second column to select several items
and then compare their data in a simple or stacked [ad-hoc
graph](/manual/config/visualisation/graphs/adhoc). To do that, select
items of interest, then click on the required graph button below the
table.

**Links to value history/simple graph**

The last column in the latest value list offers:

-   a **History** link (for all textual items) - leading to listings
    (*Values/500 latest values*) displaying the history of previous item
    values.

```{=html}
<!-- -->
```
-   a **Graph** link (for all numeric items) - leading to a [simple
    graph](/fr/manual/config/visualisation/graphs/simple). However, once
    the graph is displayed, a dropdown on the upper right offers a
    possibility to switch to *Values/500 latest values* as well.

![](../../../../../assets/en/manual/web_interface/values.png){width="600"}

The values displayed in this list are "raw", that is, no postprocessing
is applied.

::: noteclassic
The total amount of values displayed is defined by the value
of *Limit for search and filter results* parameter, set in
[Administration →
General](/fr/manual/web_interface/frontend_sections/administration/general).
:::

[comment]: # ({/new-a86c547b})


[comment]: # ({new-84099469})
##### Buttons

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.

[comment]: # ({/new-84099469})

[comment]: # ({new-dd13c026})
##### Mass actions

Buttons below the list offer mass actions with one or several selected items:

-   *Display stacked graph* - display a stacked [ad-hoc graph](/manual/config/visualization/graphs/adhoc)
-   *Display graph* - display a simple [ad-hoc graph](/manual/config/visualization/graphs/adhoc)
-   *Execute now* - execute a check for new item values immediately.
    Supported for **passive** checks only (see [more
    details](/manual/config/items/check_now)). This option is available only for hosts with read-write 
    access. Accessing this option for hosts with read-only permissions depends on the 
    [user role](/manual/web_interface/frontend_sections/administration/user_roles) option called 
    *Invoke "Execute now" on read-only hosts*.

To use these options, mark the checkboxes before the respective items,
then click on the required button.

[comment]: # ({/new-dd13c026})

[comment]: # ({new-b6e60197})
##### Using filter

You can use the filter to display only the items you are interested in.
For better search performance, data is searched with macros unresolved.

The *Filter* link is located above the table to the right. You can use
it to filter items by host group, host, a string in the item name and
tags; you can also select to display items that have no data gathered.

Specifying a parent host group implicitly selects all nested host
groups.

*Show details* allows extending displayable information on the items.
Such details as refresh interval, history and trends settings, item
type, and item errors (fine/unsupported) are displayed. A link to item
configuration is also available.

![](../../../../../assets/en/manual/web_interface/latest_data2.png){width="600"}

By default, items without data are shown but details are not displayed.
For better page performance, the *Show items without data* option is
checked and disabled if no host is selected in the filter.

**Ad-hoc graphs for comparing items**

You may use the checkbox in the first column to select several items and
then compare their data in a simple or stacked [ad-hoc
graph](/manual/config/visualization/graphs/adhoc). To do that, select
items of interest, then click on the required graph button below the
table.

**Links to value history/simple graph**

The last column in the latest value list offers:

-   a **History** link (for all textual items) - leading to listings
    (*Values/500 latest values*) displaying the history of previous item
    values.

```{=html}
<!-- -->
```
-   a **Graph** link (for all numeric items) - leading to a [simple
    graph](/manual/config/visualization/graphs/simple). However, once
    the graph is displayed, a dropdown on the upper right offers a
    possibility to switch to *Values/500 latest values* as well.

![](../../../../../assets/en/manual/web_interface/latest_values.png){width="600"}

The values displayed in this list are "raw", that is, no postprocessing
is applied.

::: noteclassic
The total amount of values displayed is defined by the value
of *Limit for search and filter results* parameter, set in
[Administration →
General](/manual/web_interface/frontend_sections/administration/general).
:::

[comment]: # ({/new-b6e60197})
