[comment]: # translation:outdated

[comment]: # ({new-563db206})
# 6 Favorite maps

[comment]: # ({/new-563db206})

[comment]: # ({new-49006132})
#### Overview

This widget contains shortcuts to the most needed maps, sorted
alphabetically.

The list of shortcuts is populated when you
[view](/manual/web_interface/frontend_sections/monitoring/maps#viewing_maps)
a map and then click on its
![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/button_add_fav.png)
*Add to favorites* button.

All configuration parameters are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets.

[comment]: # ({/new-49006132})
