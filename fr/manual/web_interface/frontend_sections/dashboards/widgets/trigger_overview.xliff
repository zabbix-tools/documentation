<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/trigger_overview.md">
    <body>
      <trans-unit id="da50605d" xml:space="preserve">
        <source># 22 Trigger overview</source>
      </trans-unit>
      <trans-unit id="8f45cbdc" xml:space="preserve">
        <source>#### Overview

In the trigger overview widget, you can display the trigger states for a group of hosts.

-   The trigger states are displayed as colored blocks
    (the color of the blocks for PROBLEM triggers depends on the problem severity color, which can be adjusted in the [problem update](/manual/acknowledgment#updating_problems) screen).
    Note that recent trigger state changes (within the last 2 minutes) will be displayed as blinking blocks.
-   Gray up and down arrows indicate triggers that have dependencies. On mouseover, dependency details are revealed.
-   A checkbox icon indicates acknowledged problems.
    All problems or resolved problems of the trigger must be acknowledged for this icon to be displayed.

Clicking on a trigger block provides context-dependent links to problem
events of the trigger, the problem acknowledgment screen, trigger
configuration, trigger URL or a simple graph/latest values list.

Note that 50 records are displayed by default (configurable in
*Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max number of columns and rows in overview tables* option).
If more records exist than are configured to display, a message is
displayed at the bottom of the table, asking to provide more specific
filtering criteria. There is no pagination. Note that this limit is applied 
first, before any further filtering of data, for example, by tags.</source>
      </trans-unit>
      <trans-unit id="d9e485e9" xml:space="preserve">
        <source>#### Configuration

To configure, select *Trigger overview* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/trigger_overview.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Show*|Filter triggers by trigger state:&lt;br&gt;**Recent problems** - *(default)* show triggers that recently have been or still are in a PROBLEM state (resolved and unresolved);&lt;br&gt;**Problems** - show triggers that are in a PROBLEM state (unresolved);&lt;br&gt;**Any** - show all triggers.|
|*Host groups*|Select the host group(s).&lt;br&gt;This field is auto-complete, so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Select hosts.&lt;br&gt;This field is auto-complete, so starting to type the name of a host will offer a dropdown of matching hosts.&lt;br&gt;Scroll down to select. Click on 'x' to remove the selected.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Problem tags*|Specify tags to filter the triggers displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;**Note:** If the parameter *Show* is set to 'Any', all triggers will be displayed even if tags are specified. However, while recent trigger state changes (displayed as blinking blocks) will update for all triggers, the trigger state details (problem severity color and whether the problem is acknowledged) will only update for triggers that match the specified tags.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hosts location*|Select host location - left or top.&lt;br&gt;This parameter is labeled *Host location* when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
