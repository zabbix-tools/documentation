[comment]: # translation:outdated

[comment]: # ({new-7b41e2d5})
# 1 Surveillance

[comment]: # ({/new-7b41e2d5})

[comment]: # ({new-c99a8958})
#### Vue d'ensemble

Le menu Surveillance regroupe les aspects d'affichage des données.
Quelques soient les informations pour lesquelles Zabbix est configuré
(collecte, visualisation et utilisation), elles seront affichées dans
les différentes sections du menu Surveillance.

[comment]: # ({/new-c99a8958})

[comment]: # ({new-e7927a8f})
#### View mode buttons

The following buttons located in the top right corner are common for
every section:

|   |   |
|---|---|
|![](../../../../assets/en/manual/web_interface/button_kiosk.png)|Display page in kiosk mode. In this mode only page content is displayed.<br>To exit kiosk mode, move the mouse cursor until the ![](../../../../assets/en/manual/web_interface/button_kiosk_leave.png) exit button appears and click on it. You will be taken back to normal mode.|

[comment]: # ({/new-e7927a8f})
