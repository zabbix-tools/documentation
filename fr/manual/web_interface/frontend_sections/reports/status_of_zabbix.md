[comment]: # translation:outdated

[comment]: # ({new-290553c7})
# 1 System information

[comment]: # ({/new-290553c7})

[comment]: # ({new-ede74e69})
#### Overview

In *Reports → System information* a summary of key system data is
displayed.

![](../../../../../assets/en/manual/web_interface/system_information.png){width="600"}

This report is also displayed as a widget in the
[Dashboard](/fr/manual/web_interface/frontend_sections/monitoring/dashboard).

[comment]: # ({/new-ede74e69})

[comment]: # ({new-bfa24672})
##### Displayed data

|Parameter|Value|Details|
|---------|-----|-------|
|*Zabbix server is running*|Status of Zabbix server:<br>**Yes** - server is running<br>**No** - server is not running<br>*Note:* To display the rest of the information the web frontend needs the server to be running and there must be at least one trapper process started on the server (StartTrappers parameter in [zabbix\_server.conf](/fr/manual/appendix/config/zabbix_server) file > 0).|Location and port of Zabbix server.|
|*Number of hosts*|Total number of hosts configured is displayed.<br>Templates are counted as a type of host too.|Number of monitored hosts/not monitored hosts/templates.|
|*Number of items*|Total number of items is displayed.|Number of monitored/disabled/unsupported items.<br>Items on disabled hosts are counted as disabled.|
|*Number of triggers*|Total number of triggers is displayed.|Number of enabled/disabled triggers. \[Triggers in problem/ok state.\]<br>Triggers assigned to disabled hosts or depending on disabled items are counted as disabled.|
|*Number of users*|Total number of users configured is displayed.|Number of users online.|
|*Required server performance, new values per second*|The expected number of new values processed by Zabbix server per second is displayed.|*Required server performance* is an estimate and can be useful as a guideline. For precise numbers of values processed, use the `zabbix[wcache,values,all]` [internal item](/manual/config/items/itemtypes/internal).<br><br>Enabled items from monitored hosts are included in the calculation. Log items are counted as one value per item update interval. Regular interval values are counted; flexible and scheduling interval values are not. The calculation is not adjusted during a "nodata" maintenance period. Trapper items are not counted.|

[comment]: # ({/new-bfa24672})

[comment]: # ({new-76697959})
#### High availability nodes

If [high availability cluster](/manual/concepts/server/ha) is enabled,
then another block of data is displayed with the status of each high
availability node.

![](../../../../../assets/en/manual/web_interface/ha_nodes.png){width="600"}

Displayed data:

|Column|Description|
|------|-----------|
|*Name*|Node name, as defined in server configuration.|
|*Address*|Node IP address and port.|
|*Last access*|Time of node last access.<br>Hovering over the cell shows the timestamp of last access in long format.|
|*Status*|Node status:<br>**Active** - node is up and working<br>**Unavailable** - node hasn't been seen for more than failover delay (you may want to find out why)<br>**Stopped** - node has been stopped or couldn't start (you may want to start it or delete it)<br>**Standby** - node is up and waiting|

[comment]: # ({/new-76697959})
