[comment]: # translation:outdated

[comment]: # ({new-d12677af})
# 2 Availability report

[comment]: # ({/new-d12677af})

[comment]: # ({new-75fed46f})
#### Overview

In *Reports → Availability report* you can see what proportion of time
each trigger has been in problem/ok state. The percentage of time for
each state is displayed.

Thus it is easy to determine the availability situation of various
elements on your system.

![](../../../../../assets/en/manual/web_interface/availability.png){width="600"}

From the dropdown in the upper right corner you can choose the selection
mode - whether to display triggers by hosts or by triggers belonging to
a template. Then in the filter you can narrow down the selection to the
desired options and the time period.

![](../../../../../assets/en/manual/web_interface/availability2.png){width="600"}

The name of the trigger is a link to the latest events of that trigger.

Clicking on *Show* in the Graph column displays a bar graph where
availability information is displayed in bar format each bar
representing a past week of the current year.

![](../../../../../assets/en/manual/web_interface/availability_graph.png){width="600"}

The green part of a bar stands for OK time and red for problem time.

[comment]: # ({/new-75fed46f})

[comment]: # ({new-f7f02710})
#### Using filter

The filter can help narrow down the number of hosts and/or triggers
displayed. For better search performance, data is searched with macros
unresolved.

The filter is located below the *Availability report* bar. It can be
opened and collapsed by clicking on the *Filter* tab on the left.

[comment]: # ({/new-f7f02710})

[comment]: # ({new-51ddfece})
##### Filtering by trigger template

In the *by trigger template* mode results can be filtered by one or
several parameters listed below.

|Parameter|Description|
|---------|-----------|
|*Template group*|Select all hosts with triggers from templates belonging to that group. Any host group that includes at least one template can be selected.|
|*Template*|Select hosts with triggers from the chosen template and all nested templates. Only triggers inherited from the selected template will be displayed. If a nested template has additional own triggers, those triggers will not be displayed.|
|//Template trigger //|Select hosts with chosen trigger. Other triggers of the selected hosts will not be displayed.|
|*Host group*|Select hosts belonging to the group.|

[comment]: # ({/new-51ddfece})

[comment]: # ({new-22214967})
##### Filtering by host

In the *by host* mode results can be filtered by a host or by the host
group. Specifying a parent host group implicitly selects all nested host
groups.

[comment]: # ({/new-22214967})

[comment]: # ({new-0533be8a})
#### Time period selector

The [time period
selector](/manual/config/visualization/graphs/simple#time_period_selector)
allows to select often required periods with one mouse click. The time
period selector can be opened by clicking on the time period tab next to
the filter.

Clicking on *Show* in the Graph column displays a bar graph where
availability information is displayed in bar format each bar
representing a past week of the current year.

![](../../../../../assets/en/manual/web_interface/availability_bar.png){width="600"}

The green part of a bar stands for OK time and red for problem time.

[comment]: # ({/new-0533be8a})
