[comment]: # translation:outdated

[comment]: # ({new-58cc1743})
# 6 Notifications

[comment]: # ({/new-58cc1743})

[comment]: # ({new-c29272bd})
#### Overview

In the *Reports → Notifications* section a report on the number of
notifications sent to each user is displayed.

From the dropdowns in the top right-hand corner you can choose the media
type (or all), period (data for each day/week/month/year) and year for
the notifications sent.

![](../../../../../assets/en/manual/web_interface/notifications.png){width="600"}

Each column displays totals per one system user.

[comment]: # ({/new-c29272bd})
