[comment]: # translation:outdated

[comment]: # ({new-376659f4})
# 3 Triggers top 100

[comment]: # ({/new-376659f4})

[comment]: # ({new-af962014})
#### Overview

In *Reports → Triggers top 100* you can see the triggers that have
changed their state most often within the period of evaluation, sorted
by the number of status changes.

![](../../../../../assets/en/manual/web_interface/triggers_top.png){width="600"}

Both host and trigger column entries are links that offer some useful
options:

-   for host - links to user-defined scripts, latest data, inventory,
    graphs and screens for the host
-   for trigger - links to latest events, the trigger configuration form
    and a simple graph

**Using filter**

You may use the filter to display triggers by host group, host, trigger
severity, predefined time period or a custom time period.

Specifying a parent host group implicitly selects all nested host
groups.

[comment]: # ({/new-af962014})
