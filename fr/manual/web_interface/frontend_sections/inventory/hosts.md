[comment]: # translation:outdated

[comment]: # ({new-cad9ee1a})
# 2 Hosts

[comment]: # ({/new-cad9ee1a})

[comment]: # ({new-50a329ce})
#### Overview

In the *Inventory → Hosts* section [inventory
data](/fr/manual/config/hosts/inventory) of hosts are displayed.

Select a group from the dropdown in the upper right corner to display
the inventory data of hosts in that group. You can also filter the hosts
by any inventory field to display only the hosts you are interested in.

![](../../../../../assets/en/manual/web_interface/inventory_filtered.png){width="600"}

To display all host inventories, select "all" in the group dropdown,
clear the comparison field in the filter and press "Filter".

While only some key inventory fields are displayed in the table, you can
also view all available inventory information for that host. To do that,
click on the host name in the first column.

[comment]: # ({/new-50a329ce})

[comment]: # ({new-1226a553})
#### Inventory details

The **Overview** tab contains some general information about the host
along with links to predefined scripts, latest monitoring data and host
configuration options:

![](../../../../../assets/en/manual/web_interface/inventory_host.png){width="600"}

The **Details** tab contains all available inventory details for the
host:

![](../../../../../assets/en/manual/web_interface/inventory_host2.png){width="600"}

The completeness of inventory data depends on how much inventory
information is maintained with the host. If no information is
maintained, the *Details* tab is disabled.

[comment]: # ({/new-1226a553})
