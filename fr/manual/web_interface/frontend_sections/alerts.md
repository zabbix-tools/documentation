[comment]: # translation:outdated

[comment]: # ({new-7574e81d})
# 7 Alerts

[comment]: # ({/new-7574e81d})

[comment]: # ({new-b7053644})
#### Overview

This menu features sections that are related to configuring 
alerts in Zabbix.

[comment]: # ({/new-b7053644})
