[comment]: # translation:outdated

[comment]: # ({new-824c08d8})
# 5 Maintenance

[comment]: # ({/new-824c08d8})

[comment]: # ({new-bae92d5d})
#### Overview

In the *Data collection → Maintenance* section users can configure and
maintain maintenance periods for hosts.

A listing of existing maintenance periods with their details is
displayed.

![](../../../../../assets/en/manual/web_interface/maintenance_periods1.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the maintenance period. Clicking on the maintenance period name opens the maintenance period [configuration form](/manual/maintenance#configuration).|
|*Type*|The type of maintenance is displayed: *With data collection* or *No data collection*|
|*Active since*|The date and time when executing maintenance periods becomes active.<br>Note: This time does not activate a maintenance period; maintenance periods need to be set separately.|
|*Active till*|The date and time when executing maintenance periods stops being active.|
|*State*|The state of the maintenance period:<br>**Approaching** - will become active soon<br>**Active** - is active<br>**Expired** - is not active any more|
|*Description*|Description of the maintenance period is displayed.|

To configure a new maintenance period, click on the *Create maintenance
period* button in the top right-hand corner.

[comment]: # ({/new-bae92d5d})

[comment]: # ({new-1d5bf59f})
##### Mass editing options

A button below the list offers one mass-editing option:

-   *Delete* - delete the maintenance periods

To use this option, mark the checkboxes before the respective
maintenance periods and click on *Delete*.

[comment]: # ({/new-1d5bf59f})

[comment]: # ({new-5378becb})
##### Using filter

You can use the filter to display only the maintenance periods you are
interested in. For better search performance, data is searched with
macros unresolved.

The *Filter* link is available above the list of maintenance periods. If
you click on it, a filter becomes available where you can filter
maintenance periods by host group, name and state.

![](../../../../../assets/en/manual/web_interface/maintenance_filter1.png){width="600"}

[comment]: # ({/new-5378becb})
