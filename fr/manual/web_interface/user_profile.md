[comment]: # translation:outdated

[comment]: # ({new-bba949fc})
# 2 User profile

[comment]: # ({/new-bba949fc})

[comment]: # ({new-2f38d1c3})
#### Overview

In the user profile you can customize some Zabbix frontend features,
such as the interface language, color theme, number of rows displayed in
the lists etc. The changes made here will apply for the user only.

To access the user profile configuration form, click on the
![](../../../assets/en/manual/web_interface/user_profile.png) user
profile link in the upper right corner of Zabbix window.

[comment]: # ({/new-2f38d1c3})

[comment]: # ({new-8fcf9738})
#### Configuration

The **User** tab allows you to set various user preferences.

![profile1.png](../../../assets/en/manual/web_interface/profile1.png)

|Parameter|Description|
|---------|-----------|
|*Password*|Click on the link to display two fields for entering a new password.|
|*Language*|Select the interface language of your choice.<br>The php gettext extension is required for the translations to work.|
|*Theme*|Select a color theme specifically for your profile:<br>**System default** - use default system settings<br>**Blue** - standard blue theme<br>**Dark** - alternative dark theme<br>**High-contrast light** - light theme with high contrast<br>**High-contrast dark** - dark theme with high contrast|
|*Auto-login*|Mark this checkbox to make Zabbix remember you and log you in automatically for 30 days. Browser cookies are used for this.|
|*Auto-logout*|With this checkbox marked you will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 90s, 5m, 2h, 1d.<br>Note that this option will not work:<br>\* If the "Show warning if Zabbix server is down" global configuration option is enabled and Zabbix frontend is kept open;<br>\* When Monitoring menu pages perform background information refreshes;<br>\* If logging in with the *Remember me for 30 days* option checked.|
|*Refresh*|You can set how often the information in the pages will be refreshed on the Monitoring menu, except for Dashboard, which uses its own refresh parameters for every widget.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 5m, 2h, 1d.|
|*Rows per page*|You can set how many rows will be displayed per page in the lists. Fewer rows (and fewer records to display) mean faster loading times.|
|*URL (after login)*|You can set a specific URL to be displayed after the login. Instead of the default *Monitoring* → *Dashboard* it can be, for example, the URL of *Monitoring* → *Triggers*.|

::: noteclassic
If some language is not available for selection in the user
profile it means that a locale for it is not installed on the web
server. See the [link](#see_also) at the bottom of this page to find out
how to install them.
:::

The **Media** tab allows you to specify the
[media](/fr/manual/config/notifications/media) details for the user,
such as the types, the addresses to use and when to use them to deliver
notifications.

![profile2.png](../../../assets/en/manual/web_interface/profile2.png){width="600"}

::: noteclassic
Only [admin
level](/fr/manual/config/users_and_usergroups/permissions) users (Admin
and Super Admin) can change their own media details.
:::

The **Messaging** tab allows you to set [global
notifications](/fr/manual/web_interface/user_profile/global_notifications).

[comment]: # ({/new-8fcf9738})

[comment]: # ({new-960ba61e})
### See also

1.  [How to install additional locales to be able to select unavailable
    languages in the user
    profile](http://www.zabbix.org/wiki/How_to/install_locale)

[comment]: # ({/new-960ba61e})
