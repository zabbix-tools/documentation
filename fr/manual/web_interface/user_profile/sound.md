[comment]: # translation:outdated

[comment]: # ({new-0429da2a})
# 2 Sound in browsers

[comment]: # ({/new-0429da2a})

[comment]: # ({new-ca9dfcae})
#### Overview

For the sounds to be played in Zabbix frontend, *Frontend messaging*
must be enabled in the user profile *Messaging* tab, with all trigger
severities checked, and sounds should also be enabled in the global
notification pop-up window.

The sounds of Zabbix frontend have been successfully tested in the
following web browser versions and no additional configuration was
required:

-   Firefox 3.5.16 on Linux
-   Opera 11.01 on Linux
-   Google Chrome 9.0 on Windows
-   Firefox 3.5.16 on Windows
-   IE7 browser on Windows
-   Opera v11.01 on Windows
-   Chrome v9.0 on Windows
-   Safari v5.0 on Windows, but this browser requires *Quick Time
    Player* to be installed

#### Additional requirements

##### Firefox v 3.5.16

For playing `wav` files in the Firefox browser you can use one of the
following applications:

-   Windows Media Player
-   Quick Time plug-in.

Then, in *Tools → Options → Applications*, in "Wave sound (audio/wav)"
set Windows Media Player to play these files.

##### Safari 5.0

*Quick Time Player* is required.

##### Microsoft Internet Explorer

To play sounds in MSIE7 and MSIE8:

-   In *Tools → Internet Options → Advanced* enable *Play sounds in
    webpages*
-   In *Tools → Manage Add-ons...* enable **Windows Media Player**
-   In the Windows Media Player, in *Tools→Options→File Types* enable
    *Windows audio file (wav)*

In the Windows Media Player, in Tools→Options tab, "File Types" is only
available if the user is a member of "Power Users" or "Administrators"
group, i.e. a regular user does not have access to this tab and does not
see it.

An additional thing - if IE does not have some \*.wav file in the local
cache directory (%userprofile%\\Local Settings\\Temporary Internet
Files) the sound will not play the first time.

##### Known not to work

Browsers where the sound did not work:

-   Opera 10.11 on Linux.

[comment]: # ({/new-ca9dfcae})
