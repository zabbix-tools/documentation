<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/web_interface/user_profile/global_notifications.md">
    <body>
      <trans-unit id="4cdbbfea" xml:space="preserve">
        <source># 1 Global notifications</source>
      </trans-unit>
      <trans-unit id="5c281f24" xml:space="preserve">
        <source>#### Overview

Global notifications are a way of displaying issues that are currently
happening right on the screen you're at in Zabbix frontend.

Without global notifications, working in some other location than
*Problems* or the *Dashboard* would not show any information regarding
issues that are currently happening. Global notifications will display
this information regardless of where you are.

Global notifications involve both showing a message and [playing a
sound](sound).

::: noteimportant
The auto play of sounds may be disabled in recent
browser versions by default. In this case, you need to change this
setting manually.
:::</source>
      </trans-unit>
      <trans-unit id="35c1ef59" xml:space="preserve">
        <source>#### Configuration

Global notifications can be enabled per user in the *Messaging* tab of
[profile configuration](/manual/web_interface/user_profile).

![profile\_c.png](../../../../assets/en/manual/web_interface/profile_c.png)

|Parameter|Description|
|--|--------|
|*Frontend messaging*|Mark the checkbox to enable global notifications.|
|*Message timeout*|You can set for how long the message will be displayed. By default, messages will stay on screen for 60 seconds.&lt;br&gt;[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 5m, 2h, 1d.|
|*Play sound*|You can set how long the sound will be played.&lt;br&gt;**Once** - sound is played once and fully.&lt;br&gt;**10 seconds** - sound is repeated for 10 seconds.&lt;br&gt;**Message timeout** - sound is repeated while the message is visible.|
|*Trigger severity*|You can set the trigger severities that global notifications and sounds will be activated for. You can also select the sounds appropriate for various severities.&lt;br&gt;If no severity is marked then no messages will be displayed at all.&lt;br&gt;Also, recovery messages will only be displayed for those severities that are marked. So if you mark *Recovery* and *Disaster*, global notifications will be displayed for the problems and the recoveries of disaster severity triggers.|
|*Show suppressed problems*|Mark the checkbox to display notifications for problems which would otherwise be suppressed (not shown) because of host maintenance.|</source>
      </trans-unit>
      <trans-unit id="933467f7" xml:space="preserve">
        <source>##### Global messages displayed

As the messages arrive, they are displayed in a floating section on the
right hand side. This section can be repositioned freely by dragging the
section header.

![global\_messages.png](../../../../assets/en/manual/web_interface/global_messages.png)

For this section, several controls are available:

-   ![](../../../../assets/en/manual/about/message_button_snooze.png)
    **Snooze** button silences the currently active alarm sound;
-   ![](../../../../assets/en/manual/about/message_button_mute.png)
    **Mute/Unmute** button switches between playing and not playing the
    alarm sounds at all.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
