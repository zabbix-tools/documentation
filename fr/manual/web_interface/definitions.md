[comment]: # translation:outdated

[comment]: # ({new-39f5a4d3})
# 6 Definitions

[comment]: # ({/new-39f5a4d3})

[comment]: # ({new-a9697948})
#### Overview

While many things in the frontend can be configured using the frontend
itself, some customisations are currently only possible by editing a
definitions file.

This file is `defines.inc.php` located in /include of the Zabbix HTML
document directory.

[comment]: # ({/new-a9697948})

[comment]: # ({new-3260c875})
#### Parameters

Parameters in this file that could be of interest to users:

-   ZBX\_MIN\_PERIOD

Minimum graph period, in seconds. One hour by default.

-   GRAPH\_YAXIS\_SIDE\_DEFAULT

Default location of Y axis in simple graphs and default value for drop
down box when adding items to custom graphs. Possible values: 0 - left,
1 - right.

Default: 0

-   SCREEN\_REFRESH\_TIMEOUT (available since 2.0.4)

Used in screens and defines the timeout seconds for a screen element
update. When the defined number of seconds after launching an update
pass and the screen element has still not been updated, the screen
element will be darkened.

Default: 30

-   SCREEN\_REFRESH\_RESPONSIVENESS (available since 2.0.4)

Used in screens and defines the number of seconds after which query
skipping will be switched off. Otherwise, if a screen element is in
update status all queries on update are skipped until a response is
received. With this parameter in use, another update query might be sent
after N seconds without having to wait for the response to the first
one.

Default: 10

-   QUEUE\_DETAIL\_ITEM\_COUNT

Defines retrieval limit of the total items queued. Since Zabbix 3.2.4
may be set higher than default value.

Default: 500

[comment]: # ({/new-3260c875})
