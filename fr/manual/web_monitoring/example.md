[comment]: # translation:outdated

[comment]: # ({new-f34132a6})
# 2 Real life scenario

[comment]: # ({/new-f34132a6})

[comment]: # ({new-d43d02d4})
#### Overview

This section presents a step-by-step real-life example of how web
monitoring can be used.

Let's use Zabbix web monitoring to monitor the web interface of Zabbix.
We want to know if it is available, provides the right content and how
quickly it works. To do that we also must log in with our user name and
password.

[comment]: # ({/new-d43d02d4})

[comment]: # ({new-51af514b})
#### Scenario

[comment]: # ({/new-51af514b})

[comment]: # ({new-6b72dd59})
##### Step 1

Add a new web scenario.

We will add a scenario to monitor the web interface of Zabbix. The
scenario will execute a number of steps.

Go to *Configuration → Hosts*, pick a host and click on *Web* in the row
of that host. Then click on *Create web scenario*.

![](../../../assets/en/manual/web_monitoring/new_scenario.png){width="600"}

All mandatory input fields are marked with a red asterisk.

In the new scenario form we will name the scenario as *Zabbix frontend*
and create a new *Zabbix frontend* application for it.

Note that we will also create two variables: {user} and {password}.

[comment]: # ({/new-6b72dd59})

[comment]: # ({new-475cbb1a})
##### Step 2

Define steps for the scenario.

Click on *Add* button in the *Steps* tab to add individual steps.

*Web scenario step 1*

We start by checking that the first page responds correctly, returns
with HTTP response code 200 and contains text "Zabbix SIA".

![](../../../assets/en/manual/web_monitoring/scenario_step1.png){width="600"}

When done configuring the step, click on *Add*.

*Web scenario step 2*

We continue by logging in to the Zabbix frontend, and we do so by
reusing the macros (variables) we defined on the scenario level - {user}
and {password}.

![](../../../assets/en/manual/web_monitoring/scenario_step2.png){width="600"}

::: noteimportant
Note that Zabbix frontend uses JavaScript redirect
when logging in, thus first we must log in, and only in further steps we
may check for logged-in features. Additionally, the login step must use
full URL to **index.php** file.
:::

Take note also of how we are getting the content of the `{sid}` variable
(session ID) using a variable syntax with regular expression:
`regex:name="sid" value="([0-9a-z]{16})"`. This variable will be
required in step 4.

*Web scenario step 3*

Being logged in, we should now verify the fact. To do so, we check for a
string that is only visible when logged in - for example,
**Administration**.

![](../../../assets/en/manual/web_monitoring/scenario_step3.png){width="600"}

*Web scenario step 4*

Now that we have verified that frontend is accessible and we can log in
and retrieve logged-in content, we should also log out - otherwise
Zabbix database will become polluted with lots and lots of open session
records.

![](../../../assets/en/manual/web_monitoring/scenario_step4.png){width="600"}

*Web scenario step 5*

We can also check that we have logged out by looking for the
**Username** string.

![](../../../assets/en/manual/web_monitoring/scenario_step5.png){width="600"}

*Complete configuration of steps*

A complete configuration of web scenario steps should look like this:

![](../../../assets/en/manual/web_monitoring/scenario_steps.png){width="600"}

[comment]: # ({/new-475cbb1a})

[comment]: # ({new-3e213338})
##### Step 3

Save the finished web monitoring scenario.

The scenario will appear in *Monitoring → Web*:

![](../../../assets/en/manual/web_monitoring/web_checks.png){width="600"}

Click on the scenario name to see more detailed statistics:

![](../../../assets/en/manual/web_monitoring/scenario_details.png){width="600"}

[comment]: # ({/new-3e213338})
