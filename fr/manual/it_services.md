[comment]: # translation:outdated

[comment]: # ({new-2ac76b92})
# 8. Supervision de service

[comment]: # ({/new-2ac76b92})

[comment]: # ({new-90a2641a})
#### Aperçu

La fonctionnalité de supervision des services est destinée à ceux qui
souhaitent disposer d'une vue de haut niveau (métier) de
l'infrastructure supervisée. Dans de nombreux cas, nous ne sommes pas
intéressés par les détails de bas niveau, tels que le manque d’espace
disque, la charge élevée du processeur, etc. Ce qui nous intéresse,
c’est la disponibilité du service fourni par notre service informatique.
Nous pouvons également être intéressés par l’identification des points
faibles de l’infrastructure informatique, des SLA des différents
services informatiques, de la structure de l’infrastructure informatique
existante et d’autres informations de haut niveau.

La supervision au niveau service de Zabbix fournie des réponses à toutes
ces questions.

Les Services correspondent à une représentation hierarchique des données
supervisées.

Un très simple exemple de structure de service pourrait ressembler à :

    Service
    |
    |-Ordinateurs
    | |
    | |-Ordinateur1
    | |
    | |-Ordinateur2
    |
    |-Serveurs

Chaque nœud de la structure a un attribut de statut. L'état est calculé
et propagé aux niveaux supérieurs en fonction de l'algorithme
sélectionné. Les déclencheur se trouvent au niveau le plus bas des
services. L'état de chaque nœud est affecté par l'état de ses
déclencheurs.

::: noteclassic
Notez que chaque déclencheur avec une sévérité *Non classée*
ou *Information* n'impactera pas le calcul des SLA.
:::

[comment]: # ({/new-90a2641a})

