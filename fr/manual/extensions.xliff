<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/extensions.md">
    <body>
      <trans-unit id="4f969495" xml:space="preserve">
        <source># 20. Extensions</source>
      </trans-unit>
      <trans-unit id="574b73cc" xml:space="preserve">
        <source>#### Overview

Although Zabbix offers a multiplicity of features, there is always room for additional functionality.
Extensions are a convenient way of modifying and enhancing the monitoring capabilities of Zabbix
without changing its source code.

You can extend Zabbix functionality either by using built-in extension options
(trapper items, user parameters, etc.) or by using or creating custom extensions (loadable modules, plugins, etc.).

This section provides an overview with references to all the options for extending Zabbix.</source>
      </trans-unit>
      <trans-unit id="7b2875dd" xml:space="preserve">
        <source>#### Data collection with custom commands</source>
      </trans-unit>
      <trans-unit id="2d4c92f9" xml:space="preserve">
        <source>##### Trapper items

[Trapper items](/manual/config/items/itemtypes/trapper) are items that accept incoming data instead of querying for it.
Trapper items are useful for sending specific data to Zabbix server,
for example, periodic availability and performance data in the case of long running user scripts.
You can do that by using the [Zabbix sender](/manual/concepts/sender) command-line utility
or by implementing a JSON-based [communication protocol](/manual/appendix/items/trapper)
(similar to that used in Zabbix sender) yourself.</source>
      </trans-unit>
      <trans-unit id="327d4393" xml:space="preserve">
        <source>##### External checks

An [external check](/manual/config/items/itemtypes/external) is an item for executing checks
by running an executable, for example, a [shell script](/manual/appendix/command_execution) or a binary.

External checks are executed by Zabbix server or proxy (when host is monitored by proxy),
and do not require an agent running on the host being monitored.</source>
      </trans-unit>
      <trans-unit id="ce750625" xml:space="preserve">
        <source>##### User parameters

A [user parameter](/manual/config/items/userparameters) is a user-defined command (associated with a user-defined key)
that, when executed, can retrieve the data you need from the host where Zabbix agent is running.
User parameters are useful for configuring agent or agent 2 items that are not predefined in Zabbix.</source>
      </trans-unit>
      <trans-unit id="014456ed" xml:space="preserve">
        <source>##### `system.run[]` Zabbix agent items

`system.run[]` Zabbix [agent item](/manual/config/items/itemtypes/zabbix_agent#system-data)
is an item for a user-defined command (associated with a predefined key `system.run[]`,
for example, `system.run[myscript.sh]`) that can be executed on the host where Zabbix agent is running. 

Note: `system.run[]` items are disabled by default and, if used,
must be enabled ([allowed](/manual/config/items/restrict_checks)) and defined
in the Zabbix agent or agent 2 configuration file (`AllowKey` configuration parameter).</source>
      </trans-unit>
      <trans-unit id="8ef20381" xml:space="preserve">
        <source>::: noteimportant
User-defined commands in items such as external checks, user parameters and `system.run[]` Zabbix agent items
are executed from the OS user that is used to run Zabbix components.
To execute these commands, this user must have the necessary permissions.
:::</source>
      </trans-unit>
      <trans-unit id="9530d8d5" xml:space="preserve">
        <source>##### HTTP agent items

[HTTP agent](/manual/config/items/itemtypes/http) item is an item for executing data requests over HTTP/HTTPS.
HTTP agent items are useful for sending requests to HTTP endpoints
to retrieve data from services such as *Elasticsearch* and *OpenWeatherMap*,
for checking the status of Zabbix API or the status of Apache or Nginx web server, etc.</source>
      </trans-unit>
      <trans-unit id="f6b462b1" xml:space="preserve">
        <source>##### Script items

A [script item](/manual/config/items/itemtypes/script)
is an item for executing user-defined JavaScript code that retrieves data over HTTP/HTTPS.
Script items are useful when the functionality provided by HTTP agent items is not enough.
For example, in demanding data collection scenarios that require multiple steps or complex logic,
a script item can be configured to make an HTTP call, then process the data received,
and then pass the transformed value to a second HTTP call.</source>
      </trans-unit>
      <trans-unit id="fc37db31" xml:space="preserve">
        <source>::: notetip
HTTP agent items and script items are supported by Zabbix server and proxy,
and do not require an agent running on the host being monitored.
:::</source>
      </trans-unit>
      <trans-unit id="37a255c2" xml:space="preserve">
        <source>#### Advanced extensions</source>
      </trans-unit>
      <trans-unit id="45ac5b61" xml:space="preserve">
        <source>##### Loadable modules

[Loadable modules](/manual/extensions/loadablemodules), written in C, are a versatile
and performance-minded option for extending the functionality of Zabbix components (server, proxy, agent)
on UNIX platforms. A loadable module is basically a shared library used by Zabbix daemon and loaded on startup.
The library should contain certain functions,
so that a Zabbix process may detect that the file is indeed a module it can load and work with.

Loadable modules have a number of benefits, including the ability to add new metrics or implement any other logic
(for example, Zabbix [history data export](/manual/extensions/loadablemodules#providing-history-export-callbacks)),
great performance, and the option to develop, use and share the functionality they provide.
It contributes to trouble-free maintenance
and helps to deliver new functionality easier and independently of the Zabbix core code base.

Loadable modules are especially useful in a complex monitoring setup.
When monitoring embedded systems, having a large number of monitored parameters
or heavy scripts with complex logic or long startup time,
extensions such as user parameters, `system.run[]` Zabbix agent items, and external checks
will have an impact on performance.
Loadable modules offer a way of extending Zabbix functionality without sacrificing performance.</source>
      </trans-unit>
      <trans-unit id="c3a7defc" xml:space="preserve">
        <source>##### Plugins

[Plugins](/manual/extensions/plugins) provide an alternative to loadable modules (written in C).
However, plugins are a way to extend Zabbix agent 2 only.

A plugin is a *Go* package that defines the structure and implements one or several plugin interfaces
(*Exporter*, *Collector*, *Configurator*, *Runner*, *Watcher*). Two types of Zabbix agent 2 plugins are supported:

-   [Built-in plugins](https://www.zabbix.com/documentation/guidelines/en/plugins/built_in_plugins)
    (supported since Zabbix 4.4.0)
-   [Loadable plugins](https://www.zabbix.com/documentation/guidelines/en/plugins/loadable_plugins)
    (supported since Zabbix 6.0.0)

See the list of [built-in plugins](/manual/extensions/plugins#built-in).

For instructions and tutorials on writing your own plugins, see [Developer center](/devel/plugins).</source>
      </trans-unit>
      <trans-unit id="d554dcca" xml:space="preserve">
        <source>#### Alert customization</source>
      </trans-unit>
      <trans-unit id="4957b4ed" xml:space="preserve">
        <source>##### Webhooks

A [webhook](/manual/config/notifications/media/webhook)
is a Zabbix [media type](/manual/config/notifications/media)
that provides an option to extend Zabbix alerting capabilities
to external software such as helpdesk systems, chats, or messengers.
Similarly to script items, webhooks are useful for making HTTP calls using custom JavaScript code,
for example, to push notifications to different platforms such as Microsoft Teams, Discord, and Jira.
It is also possible to return some data (for example, about created helpdesk tickets) that is then displayed in Zabbix.

Existing webhooks are available
in the Zabbix [Git repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media).
For custom webhook development,
see [Webhook development guidelines](https://www.zabbix.com/documentation/guidelines/en/webhooks).</source>
      </trans-unit>
      <trans-unit id="1ff47b17" xml:space="preserve">
        <source>##### Alert scripts

An [alert script](/manual/config/notifications/media/script)
is a Zabbix [media type](/manual/config/notifications/media)
that provides an option to create an alternative way (script) to handle Zabbix alerts.
Alert scripts are useful if you are not satisfied with the existing media types for sending alerts in Zabbix.</source>
      </trans-unit>
      <trans-unit id="6a53c965" xml:space="preserve">
        <source>#### Frontend customization</source>
      </trans-unit>
      <trans-unit id="a1752bc3" xml:space="preserve">
        <source>##### Frontend modules

[Frontend modules](/manual/extensions/frontendmodules) provide an option to extend Zabbix frontend functionality
by adding third-party modules or by developing your own.
With frontend modules you can add new menu items, their respective views, actions, etc.</source>
      </trans-unit>
      <trans-unit id="820a448e" xml:space="preserve">
        <source>##### Rebranding

[Rebranding](/manual/web_interface/rebranding) is a way to customize
the user interface of your Zabbix frontend installation.
You can customize the user interface in the following ways:

-   Replace the Zabbix logo with a desired one.
-   Hide links to *Zabbix Support* and *Zabbix Integrations*.
-   Set a custom link to the *Help* page.
-   Change the copyright notice in the footer.

Note: you can also create you own Zabbix [themes](/manual/web_interface/theming) to better fit the rebranding.</source>
      </trans-unit>
      <trans-unit id="485cf185" xml:space="preserve">
        <source>#### Global scripts

A [global script](/manual/web_interface/frontend_sections/alerts/scripts)
is a user-defined set of commands that can be executed on a monitoring target (by shell (/bin/sh) interpreter),
depending on the configured scope and user permissions.
Global scripts can be configured for the following actions:

-   Action [operation](/manual/config/notifications/action/operation)
-   [Manual host action](/manual/web_interface/menu/host_menu#overview)
-   [Manual event action](/manual/web_interface/menu/event_menu#content)

Global scripts are useful in many cases.
For example, if configured for action operations or manual host actions, you can use global scripts
to automatically or manually execute [remote commands](/manual/config/notifications/action/operation/remote_command)
such as restarting an application (web server, middleware, CRM, etc.)
or freeing disk space (removing older files, cleaning `/tmp`, etc).
Or, another example, if configured for manual event actions,
you can use global scripts to manage problem tickets in external systems.

Global scripts can be executed by Zabbix server, proxy or agent.</source>
      </trans-unit>
      <trans-unit id="44d06965" xml:space="preserve">
        <source>::: noteimportant
User-defined commands are executed from the OS user that is used to run Zabbix components.
To execute these commands, this user must have the necessary permissions.
:::</source>
      </trans-unit>
      <trans-unit id="ee438455" xml:space="preserve">
        <source>#### Zabbix API

[Zabbix API](/manual/api) is an HTTP-based API that is part of Zabbix frontend.
With Zabbix API, you can do any of the following operations:

-   Programmatically retrieve and modify the configuration of Zabbix.
-   Import and export Zabbix configuration.
-   Access Zabbix historical and trend data.
-   Configure applications to work with Zabbix.
-   Integrate Zabbix with third-party software.
-   Automate routine tasks.

Zabbix API consists of a multiplicity of methods that are nominally grouped into separate APIs.
Each method performs a specific task.
For the available methods, as well as an overview of the functions provided by Zabbix API,
see Zabbix API [Method reference](/manual/api/reference).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
