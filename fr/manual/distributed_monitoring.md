[comment]: # translation:outdated

[comment]: # ({new-9f1711bf})
# 16. Supervision distribuée

[comment]: # ({/new-9f1711bf})

[comment]: # ({new-e207d054})
#### Vue d'ensemble

Zabbix fournit un moyen efficace et fiable de surveiller une
infrastructure informatique distribuée à l'aide de serveurs
[proxys](/fr/manual/distributed_monitoring/proxies) Zabbix.

Les proxys peuvent être utilisés pour collecter des données localement
pour le compte d'un serveur Zabbix centralisé, puis rapporter les
données au serveur.

[comment]: # ({/new-e207d054})

[comment]: # ({new-6744cab9})
##### Fonctions du proxy

Lorsque vous choisissez d'utiliser ou de ne pas utiliser un proxy,
plusieurs considérations doivent être prises en compte.

|<|Proxy|
|-|-----|
|*Légerté*|**Oui**|
|*GUI*|Non|
|*Fonctionnement indépendamment*|**Oui**|
|*Facilité de maintenance*|**Oui**|
|*Création automatique de la BDD*^1^|**Oui**|
|*Administration locale*|Non|
|*Prêt pour le matériel embarqué*|**Oui**|
|*Connexions TCP à sens unique*|**Oui**|
|*Configuration centralisée*|**Oui**|
|*Génération de notifications*|Non|

::: noteclassic
\[1\] La fonctionnalité de création automatique de base de
données ne fonctionne qu'avec SQLite. Les autres bases de données
nécessitent une [configuration
manuelle](/fr/manual/appendix/install/db_scripts#creation_de_la_base_de_donnees).
:::

[comment]: # ({/new-6744cab9})
