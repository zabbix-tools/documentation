[comment]: # translation:outdated

[comment]: # ({new-1c6301ba})
# 3 Mise à jour groupée

[comment]: # ({/new-1c6301ba})

[comment]: # ({new-7205743f})
#### Aperçu

Parfois, vous voudrez peut-être changer certains attributs pour un
certain nombre d'hôtes à la fois. Au lieu d'ouvrir chaque hôte
individuellement pour l'édition, vous pouvez utiliser la fonction de
mise à jour groupée pour cela.

[comment]: # ({/new-7205743f})

[comment]: # ({new-929367ac})
#### Utilisation de la mise à jour groupée

Pour mettre à jour de manière groupée certains hôtes, procédez comme
suit :

-   Cochez les cases devant les hôtes que vous souhaitez mettre à jour
    dans la [liste des
    hôtes](/fr/manual/web_interface/frontend_sections/configuration/hosts)
-   Cliquez sur *Mise à jour groupée* sous la liste
-   Accédez à l'onglet avec les attributs requis (*Hôte*, *Modèles*,
    *IPMI*, *Inventaire* ou *Chiffrement*)
-   Cochez les cases de tous les attributs à mettre à jour et entrez une
    nouvelle valeur pour eux

![](../../../../assets/en/manual/config/host_mass_update1.png)

*Remplacer les groupes d'hôtes* supprimera l'hôte de tous les groupes
d'hôtes existants et les remplacera par ceux spécifiés dans ce champ.

*Ajouter de nouveaux groupes d'hôtes ou existants* permet de spécifier
des groupes d'hôtes supplémentaires par rapport aux groupes existants ou
d'entrer des groupes d'hôtes complètement nouveaux pour les hôtes.

*Supprimer les groups d’hôtes* supprimera des groupes d'hôtes
spécifiques des hôtes. Dans le cas où les hôtes sont déjà dans les
groupes sélectionnés, alors les hôtes seront supprimés de ces groupes.
Dans le cas où les hôtes ne sont pas dans les groupes sélectionnés, rien
ne sera ajouté ni supprimé. Dans le cas où les mêmes groupes d'hôtes
sont remplacés et supprimés en même temps, les hôtes sont réellement
laissés sans groupes.

Ces deux champs sont auto-complétés - commencez à les saisir les
premiers caractères, une liste déroulante des groupes d'hôtes
correspondants apparait alors. Si le groupe d'hôtes est nouveau, il
apparaît également dans la liste déroulante et il est indiqué par
*nouveau* après la chaîne. Faites défiler vers le bas pour sélectionner.

![](../../../../assets/en/manual/config/host_mass_update2.png)

Pour mettre à jour le lien de modèle dans l'onglet **Modèles**,
sélectionnez *Lier les modèles* et commencez à taper le nom du modèle
dans le champ de saisie semi-automatique jusqu'à ce qu'une liste
déroulante proposant les modèles correspondants s'affiche. Faites
défiler vers le bas pour sélectionner le modèle à lier.

L'option *Remplacer* permettra de lier un nouveau modèle tout en
dissociant tout modèle qui était lié aux hôtes auparavant. L'option
*Supprimer le lien et Nettoyer* permet non seulement de dissocier tous
les modèles précédemment liés, mais aussi de supprimer tous les éléments
hérités de ceux-ci (éléments, déclencheurs, etc.).

![](../../../../assets/en/manual/config/host_mass_update3.png)

![](../../../../assets/en/manual/config/host_mass_update4.png)

Pour pouvoir mettre à jour de manière groupée les champs d'inventaire,
le *mode d'inventaire* doit être réglé sur "Manuel" ou "Automatique".

![](../../../../assets/en/manual/config/host_mass_update5.png)

Lorsque vous avez terminé toutes les modifications requises, cliquez sur
*Mettre à jour*. Les attributs seront mis à jour en conséquence pour
tous les hôtes sélectionnés.

[comment]: # ({/new-929367ac})
