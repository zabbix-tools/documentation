[comment]: # translation:outdated

[comment]: # ({new-061a30b2})
# 2 Inventaire

[comment]: # ({/new-061a30b2})

[comment]: # ({new-4b8ec855})
#### Aperçu

Vous pouvez conserver l'inventaire des périphériques réseau dans Zabbix.

Il y a un menu spécial *Inventaire* dans l'interface Zabbix. Cependant,
vous ne verrez aucune donnée initialement et ce n'est pas là que vous
entrez les données. Construire les données d'inventaire est fait
manuellement lors de la configuration d'un hôte ou automatiquement en
utilisant certaines options de peuplement automatique.

[comment]: # ({/new-4b8ec855})

[comment]: # ({new-69c69326})
#### Construction de l’inventaire

[comment]: # ({/new-69c69326})

[comment]: # ({new-9a68f9a7})
##### Mode manuel

Lors de la [configuration d'un hôte](host), dans l'onglet *Hôte*, vous
pouvez entrer des informations telles que le type du périphérique, le
numéro de série, l'emplacement, la personne responsable, etc. - les
données qui peupleront les informations d'inventaire.

Si une URL est incluse dans les informations d'inventaire de l'hôte et
qu'elle commence par `http` ou `https`, un lien cliquable apparaît dans
la section *Inventaire*.

[comment]: # ({/new-9a68f9a7})

[comment]: # ({new-a9359c4e})
##### Mode automatique

L'inventaire de l'hôte peut également être rempli automatiquement. Pour
que cela fonctionne, lorsque vous configurez un hôte, le mode inventaire
dans l'onglet *Hôte* doit être défini sur *Automatique*.

Ensuite, vous pouvez [configurer les éléments de
l'hôte](/fr/manual/config/items/item) pour remplir tout champ
d'inventaire hôte avec leur valeur, en indiquant le champ de destination
avec l'attribut respectif dans la configuration de l'élément.

Les éléments particulièrement utiles pour la collecte automatique de
données d'inventaire sont :

-   system.hw.chassis\[full|type|vendor|model|serial\] - par défaut est
    \[full\], nécessite les droits root
-   system.hw.cpu\[all|cpunum,full|maxfreq|vendor|model|curfreq\] - par
    défaut est \[all,full\]
-   system.hw.devices\[pci|usb\] - par défaut est \[pci\]
-   system.hw.macaddr\[interface,short|full\] - par défaut est
    \[all,full\], interface est regexp
-   system.sw.arch
-   system.sw.os\[name|short|full\] - par défaut est \[name\]
-   system.sw.packages\[package,manager,short|full\] - par défaut est
    \[all,all,full\], package est regexp

[comment]: # ({/new-a9359c4e})

[comment]: # ({new-84351dfb})
##### Sélection du mode d'inventaire

Le mode d'inventaire peut être sélectionné dans le formulaire de
configuration de l'hôte.

Le mode d'inventaire par défaut pour les nouveaux hôtes est sélectionné
en se basant sur le paramètre *Mode d'inventaire par défaut de l'hôte*
dans *Administration* → *General* →
*[Autre](/manual/web_interface/frontend_sections/administration/general#other_parameters)*.

Pour les hôtes ajoutés via la découverte réseau ou via les actions
enregistrement automatique, il est possible de définir une opération de
*Définition du mode d'inventaire hôte* en sélectionnant le mode manuel
ou automatique. Cette opération remplace le paramètre *Mode d'inventaire
hôte par défaut*.

[comment]: # ({/new-84351dfb})

[comment]: # ({new-8291099a})
#### Aperçu de l'inventaire

Les détails de toutes les données d'inventaire existantes sont
disponibles dans le menu *Inventaire*.

Dans *Inventaire → Aperçu*, vous pouvez obtenir un nombre d'hôtes par
différents champs de l'inventaire.

Dans *Inventaire → Hôtes*, vous pouvez voir tous les hôtes qui ont des
informations d'inventaire. Un clic sur le nom de l'hôte révèlera les
détails de l'inventaire dans un formulaire.

![](../../../../assets/en/manual/web_interface/inventory_host.png){width="600"}

L'onglet **Aperçu** montre :

|Paramètre|<|<|<|<|Description|
|----------|-|-|-|-|-----------|
|*Nom de l'hôte*|<|<|<|<|Nom de l'hôte.<br>Cliquez sur le nom ouvre un menu avec les scripts définis pour l'hôte.<br>Le nom de l'hôte est affiché avec une icône orange, si l'hôte est en mode maintenance.|
|*Nom visible*|<|<|<|<|Nom visible de l'hôte (si défini).|
|*Interface de l'hôte (Agent, SNMP, JMX, IPMI)*|<|<|<|<|Cette partie fournie les détails des interfaces configurées pour l'hôte|
|*OS*|<|<|<|<|Champ d'inventaire du système d'exploitation de l'hôte (si défini).|
|*Matériel*|<|<|<|<|Champ d'inventaire du matériel de l'hôte (si défini).|
|*Logiciel*|<|<|<|<|Champ d'inventaire des logiciels de l'hôte (si défini).|
|*Description*|<|<|<|<|Description de l'hôte.|
|*Surveillance*|<|<|<|<|Liens vers les sections surveillance avec les données pour cet hôte : *Web*, *Dernières données*, *Déclencheurs*, *Problèmes*, *Graphiques*, *Ecrans*.|
|*Configuration*|<|<|<|<|Liens vers les sections de configuration pour cet hôte : *Hôte*, *Applications*, *Éléments*, *Déclencheurs*, *Graphiques*, *Découverte*, *Web*. <br>La quantité d'entités configurées est répertoriée entre parenthèses après chaque lien.|

L'onglet **Détails** montre tous les champs d'inventaire qui sont
peuplés (non vide).

[comment]: # ({/new-8291099a})

[comment]: # ({new-6a9ca6a1})
#### Macros d'inventaire

Des macros d'inventaire d’hôte {INVENTORY.\*} peuvent être utilisées
dans les notifications, par exemple :

"Le serveur dans {INVENTORY.LOCATION1} a un problème, la personne
responsable est {INVENTORY.CONTACT1}, numéro de téléphone
{INVENTORY.POC.PRIMARY.PHONE.A1}."

 Pour plus de détails, consultez la page [Macros prises en charge par
emplacement](/fr/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-6a9ca6a1})
