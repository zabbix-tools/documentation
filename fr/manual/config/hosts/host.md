[comment]: # translation:outdated

[comment]: # ({new-1da35dcd})
# 1 Configuration d’un hôte

[comment]: # ({/new-1da35dcd})

[comment]: # ({new-0ee38f7e})
#### Aperçu

Pour configurer un hôte dans l’interface web Zabbix, procédez comme suit
:

-   Aller dans : *Configuration → Hôtes*
-   Cliquez sur *Créer un hôte* à droite (ou sur le nom d'hôte pour
    modifier un hôte existant)
-   Entrez les paramètres de l'hôte dans le formulaire

Vous pouvez également utiliser les boutons *Cloner* et *Clonage complet*
dans le formulaire d’un hôte existant pour créer un nouvel hôte. En
cliquant sur *Cloner*, vous conserverez tous les paramètres de l'hôte et
le lien du modèle (en conservant toutes les entités de ces modèles). Le
*clonage complet* conservera en outre les entités directement rattachées
(applications, éléments, déclencheurs, graphiques, règles de découverte
de bas niveau et scénarios Web).

*Remarque* : Lorsqu'un hôte est cloné, il conserve toutes les entités de
modèle telles qu'elles sont à l'origine sur le modèle. Toutes les
modifications apportées à ces entités au niveau de l'hôte existant
(telles que l'intervalle d'élément modifié, l'expression régulière
modifiée ou les prototypes ajoutés à la règle de découverte de bas
niveau) ne seront pas clonées sur le nouvel hôte ; à la place, ils
seront comme sur le modèle.

[comment]: # ({/new-0ee38f7e})

[comment]: # ({new-e2f56cfc})
#### Configuration

L’onglet **Hôte** contient les attributs généraux de l’hôte :\
![](../../../../assets/en/manual/config/host_a.png){width="600"}

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

|Paramètre|Description|
|----------|-----------|
|*Nom de l'hôte*|Entrez un nom d'hôte unique. Les caractères alphanumériques, les espaces, les points, les tirets et les underscores sont autorisés.<br> *Remarque :* Lorsque l'agent Zabbix s'exécute sur l'hôte que vous configurez, le paramètre de [fichier de configuration](/fr/manual/appendix/config/zabbix_agentd) de l'agent *Hostname* doit avoir la même valeur que le nom d'hôte saisi ici. Le nom dans le paramètre est nécessaire dans le traitement des [vérifications actives](/fr/manual/appendix/items/activepassive).|
|*Nom visible*|Si vous définissez ce nom, ce sera celui visible dans les listes, les cartes, etc. Cet attribut prend en charge UTF-8.|
|*Groupes*|Sélectionnez les groupes d'hôtes auxquels l'hôte appartient. Un hôte doit appartenir à au moins un groupe hôte. Un nouveau groupe peut être créé et lié à un groupe d'hôtes en ajoutant un nom de groupe non existant|
|*Interfaces*|Plusieurs types d'interfaces hôte sont pris en charge pour un hôte : *Agent*, *SNMP*, *JMX* et *IPMI*.  \\\\Pour ajouter une nouvelle interface, cliquez sur Ajouter dans le bloc *Interfaces* et entrez les informations *IP/DNS*, *Connexion à* et *Port*.  \\\\Remarque : Les interfaces utilisées dans les éléments ne peuvent pas être supprimées et le lien *Supprimer* est grisé.  \\\\L'option *Utiliser les requêtes de masse* pour les interfaces SNMP permet d'activer/désactiver le [traitement groupé](/fr/manual/config/items/itemtypes/snmp#internal_workings_of_bulk_processing) des requêtes SNMP par interface.|
|*Adresse IP*|Adresse IP de l’hôte (optionel).|
|//Nom DNS //|Nom DNS de l’hôte (optionel).|
|*Connexion à*|En cliquant sur le bouton correspondant, vous indiquerez au serveur Zabbix ce qu'il faut utiliser pour extraire les données des agents :<br>**IP** - Se connecter à l'adresse IP de l'hôte (recommandé)<br> **DNS** - Se connecter au nom DNS de l'hôte|
|*Port*|Numéro de port TCP/UDP. Les valeurs par défaut sont : 10050 pour l’agent Zabbix, 161 pour l’agent SNMP, 12345 pour JMX et 623 pour IPMI.|
|*Défaut*|Cochez le bouton radio pour définir l'interface par défaut.|
|*Description*|Entrer la description de hôte.|
|// Surveillé via le proxy//|L'hôte peut être surveillé soit par le serveur Zabbix soit par l'un des proxys Zabbix :  <br>**(pas de proxy)** - l'hôte est supervisé par le serveur Zabbix <br>**Nom du proxy** - l'hôte est supervisé par le proxy Zabbix "Nom du proxy"|
|*Activé*|Cochez la case pour rendre l'hôte actif, prêt à être supervisé. Si cette case n'est pas cochée, l'hôte n'est pas actif et n'est donc pas supervisé.|

L'onglet **Modèles** vous permet de lier des
[modèles](/fr/manual/config/templates) à l'hôte. Toutes les entités
(éléments, déclencheurs, graphiques et applications) seront héritées du
modèle.

Pour lier un nouveau modèle, commencez à taper dans le champ *Lier un
nouveau modèle* jusqu'à ce qu'une liste de modèles correspondants
apparaisse. Faites défiler pour sélectionner. Lorsque tous les modèles à
associer sont sélectionnés, cliquez sur *Ajouter*. Pour dissocier un
modèle, utilisez l'une des deux options des *Modèles liés* :

-   // Supprimer lien// - dissocier le modèle, mais conserver ses
    éléments, déclencheurs et graphiques
-   // Supprimer lien et nettoyer// - dissocier le modèle et supprimer
    tous ses éléments, déclencheurs et graphiques

Les noms de modèles listés sont des liens cliquables menant au
formulaire de configuration du modèle.

L'onglet **IPMI** contient des attributs de gestion IPMI.

|Parameter|Description|
|---------|-----------|
|//Algorithme d’authentication //|Selectionner l’algorithme d’authentication.|
|*Niveau de privilège*|Selectionner le niveau de privilège.|
|*Nom d’utilisateur*|Nom d’utilisateur pour l’authentification.|
|*Mot de passe*|Mot de passe pour l’authentification.|

L'onglet **Macros** vous permet de définir des [macros
utilisateur](/fr/manual/config/macros/usermacros) au niveau de l'hôte.
Vous pouvez également afficher ici les macros au niveau du modèle et les
macros globales si vous sélectionnez l'option *Macros héritées et
d’hôtes*. C'est là que toutes les macros utilisateur définies pour
l'hôte sont affichées avec la valeur qu'elles résolvent ainsi que leur
origine.

![](../../../../assets/en/manual/config/host_d.png){width="600"}

Pour plus de commodité, des liens vers les modèles respectifs et la
configuration globale des macros sont fournis. Il est également possible
de modifier un modèle ou une macro globale au niveau de l'hôte, en
créant effectivement une copie de la macro sur l'hôte.

L'onglet **Inventaire d'hôtes** vous permet de saisir manuellement les
informations d'[inventaire](inventory) pour l'hôte. Vous pouvez
également choisir d'activer la population d'inventaire *Automatique* ou
désactiver la population d'inventaire pour cet hôte.

L'onglet **Chiffrement** vous permet d'exiger des connexions
[chiffrées](/fr/manual/encryption) avec l'hôte.

|Paramètre|Description|
|----------|-----------|
|*Connexions à l’hôte*|Comment le serveur ou le proxy Zabbix se connecte à l'agent Zabbix sur un hôte : pas de chiffrement (par défaut), en utilisant PSK (clé pré-partagée) ou certificat.|
|*Connexions depuis l’hôte*|Sélectionnez le type de connexion autorisée depuis l'hôte (par exemple, depuis l'agent Zabbix et le sender Zabbix). Plusieurs types de connexion peuvent être sélectionnés en même temps (utile pour tester et passer à un autre type de connexion). La valeur par défaut est "Pas de chiffrement".|
|*Délivré par*|Emetteur autorisé du certificat. Le certificat est d'abord validé avec une AC (autorité de certification). S'il est valide, signé par l'autorité de certification, le champ *Délivré par* peut être utilisé pour restreindre davantage l'autorité de certification autorisée. Ce champ est destiné à être utilisé si votre installation Zabbix utilise des certificats de plusieurs autorités de certification. Si ce champ est vide, toute autorité de certification est acceptée.|
|*Sujet*|Sujet autorisé du certificat. Le certificat est d'abord validé avec une AC. Si elle est valide, signée par l'autorité de certification, le champ *Sujet* peut être utilisé pour n'autoriser qu'une seule valeur de la chaîne *Sujet*. Si ce champ est vide, tout certificat valide signé par l'autorité de certification configurée est accepté.|
|// Identité PSK//|Chaîne d'identité de clé pré-partagée.|
|*PSK*|Clé pré-partagée (chaîne hexadécimale). Longueur maximale : 512 caractères hexadécimaux (PSK de 256 octets) si Zabbix utilise la bibliothèque GnuTLS ou OpenSSL, 64 caractères hexadécimaux (PSK de 32 octets) si Zabbix utilise la bibliothèque mbed TLS (PolarSSL). Exemple : 1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952|

[comment]: # ({/new-e2f56cfc})

[comment]: # ({new-6875b717})
#### Création d'un groupe d'hôtes

Pour créer un groupe d'hôtes dans l'interface Web de Zabbix, procédez
comme suit :

-   Allez dans : *Configuration → Groupes d'hôtes*
-   Cliquez sur *Créer un groupe d'hôtes* dans le coin supérieur droit
    de l'écran
-   Entrez les paramètres du groupe dans le formulaire

![](../../../../assets/en/manual/config/host_group.png){width="600"}

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

|Paramètre|Description|
|----------|-----------|
|*Nom du groupe*|Entrez un nom de groupe d'hôtes unique.<br>Pour créer un groupe d'hôtes imbriqué, utilisez le séparateur de barre oblique '/', par exemple les serveurs `Europe/Latvia/Riga/Zabbix`. Vous pouvez créer ce groupe même si aucun des trois groupes d'hôtes parents (`Europe/Lettonie/Riga`) n'existe. Dans ce cas, la création de ces groupes hôtes parents dépend de l'utilisateur ; ils ne seront pas créés automatiquement.<br> Les barres obliques de début et de fin, plusieurs barres obliques d'affilée ne sont pas autorisées. L'échappement de '/' n'est pas supporté. <br>La représentation imbriquée des groupes d'hôtes est supportée depuis Zabbix 3.2.0.|
|//Appliquer les autorisations à tous les sous-groupes //|Cette case est disponible uniquement aux utilisateurs Super Admin de Zabbix et seulement lors de l'édition d'un groupe d'hôtes existant.<br>Cochez cette case et cliquez sur *Mettre à jour* pour appliquer le même niveau d'autorisations à tous les groupes d'hôtes imbriqués. Pour les groupes d'utilisateurs susceptibles d'avoir des [autorisations](/manual/config/users_and_usergroups/usergroup#configuration) différentes attribuées à des groupes d'hôtes imbriqués, le niveau d'autorisation du groupe d'hôtes parent sera appliqué aux groupes imbriqués. C'est une option unique qui n'est pas enregistrée dans la base de données.<br> Cette option est supportée depuis Zabbix 3.4.0.|

**Autorisations pour les groupes d'hôtes imbriqués**

-   Lors de la création d'un groupe d'hôtes enfant dans un groupe
    d'hôtes parent existant, les autorisations de [groupe
    d'utilisateurs](/manual/config/users_and_usergroups/usergroup) sur
    l'enfant sont héritées du parent (par exemple, lors de la création
    de serveurs `Riga/Zabbix` si `Riga` existe déjà)
-   Lors de la création d'un groupe d'hôtes parent dans un groupe
    d'hôtes enfant existant, aucune autorisation n'est définie pour le
    parent (par exemple, lors de la création de `Riga` si les
    `serveurs Riga/Zabbix` existent déjà)

[comment]: # ({/new-6875b717})

[comment]: # ({new-513311ca})
##### Value mapping

The **Value mapping** tab allows to configure human-friendly
representation of item data in [value
mappings](/manual/config/items/mapping).

[comment]: # ({/new-513311ca})

[comment]: # ({new-7cc0883e})
#### Creating a host group

::: noteimportant
Only Super Admin users can create host groups.
:::

To create a host group in Zabbix frontend, do the following:

-   Go to: *Configuration → Host groups*
-   Click on *Create host group* in the upper right corner of the screen
-   Enter the group name in the form

![](../../../../assets/en/manual/config/host_group.png)

To create a nested host group, use the '/' forward slash separator, for example `Europe/Latvia/Riga/Zabbix servers`. You can create this group even if none of the three parent host groups (`Europe/Latvia/Riga/`) exist. In this case creating these parent host groups is up to the user; they will not be created automatically.<br>Leading and trailing slashes, several slashes in a row are not allowed. Escaping of '/' is not supported.

[comment]: # ({/new-7cc0883e})

[comment]: # ({new-22cf222a})

Once the group is created, you can click on the group name in the list to edit group name, clone the group or set additional option:

![](../../../../assets/en/manual/config/host_group2.png)

*Apply permissions and tag filters to all subgroups* - mark this checkbox and click on *Update* to apply the same level of permissions/tag filters to all nested host groups. For user groups that may have had differing [permissions](/manual/config/users_and_usergroups/usergroup#configuration) assigned to nested host groups, the permission level of the parent host group will be enforced on the nested groups. This is a one-time option that is not saved in the database.

**Permissions to nested host groups**

-   When creating a child host group to an existing parent host group,
    [user group](/manual/config/users_and_usergroups/usergroup)
    permissions to the child are inherited from the parent (for example,
    when creating `Riga/Zabbix servers` if `Riga` already exists)
-   When creating a parent host group to an existing child host group,
    no permissions to the parent are set (for example, when creating
    `Riga` if `Riga/Zabbix servers` already exists)

[comment]: # ({/new-22cf222a})


