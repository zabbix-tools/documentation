[comment]: # translation:outdated

[comment]: # ({new-526f6fc0})
# 2 Actions

[comment]: # ({/new-526f6fc0})

[comment]: # ({new-b19d9072})
#### Overview

If you want some operations taking place as a result of events (for
example, notifications sent), you need to configure actions.

Actions can be defined in response to events of all supported types:

-   Trigger events - when trigger status changes from *OK* to *PROBLEM*
    and back
-   Discovery events - when network discovery takes place
-   Auto registration events - when new active agents auto-register (or
    host metadata changes for registered ones)
-   Internal events - when items become unsupported or triggers go into
    an unknown state

[comment]: # ({/new-b19d9072})

[comment]: # ({new-cf7fda79})
#### Configuring an action

To configure an action, do the following:

-   Go to *Configuration → Actions*
-   From the *Event source* dropdown select the required source
-   Click on *Create action*
-   Name the action
-   Choose conditions upon which operations are carried out
-   Choose the
    [operations](/fr/manual/config/notifications/action/operation) to
    carry out
-   Choose the [recovery
    operations](/fr/manual/config/notifications/action/recovery_operations)
    to carry out

General action attributes:

![](../../../../assets/en/manual/config/action1.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Name*|Unique action name.|
|*Type of calculation*|Select the evaluation [option](/manual/config/notifications/action/conditions#type_of_calculation) for action conditions (with more than one condition):<br>**And** - all conditions must be met<br>**Or** - enough if one condition is met<br>**And/Or** - combination of the two: AND with different condition types and OR with the same condition type<br>**Custom expression** - a user-defined calculation formula for evaluating action conditions.|
|*Conditions*|List of action conditions.|
|*New condition*|Select a new action [condition](/manual/config/notifications/action/conditions) and click on *Add*.|
|*Enabled*|Mark the checkbox to enable the action. Otherwise it will be disabled.|

[comment]: # ({/new-cf7fda79})
