[comment]: # translation:outdated

[comment]: # ({new-b1624dc4})
# 4 Using macros in messages

[comment]: # ({/new-b1624dc4})

[comment]: # ({new-72998395})
#### Overview

In message subjects and message text you can use macros for more
efficient problem reporting.

A [full list of
macros](/fr/manual/appendix/macros/supported_by_location) supported by
Zabbix is available.

[comment]: # ({/new-72998395})

[comment]: # ({new-e5ad63df})
#### Examples

Examples here illustrate how you can use macros in messages.

[comment]: # ({/new-e5ad63df})

[comment]: # ({new-102863de})
##### Example 1

Message subject:

    Problem: {TRIGGER.NAME}

When you receive the message, the message subject will be replaced by
something like:

    Problem: Processor load is too high on Zabbix server

[comment]: # ({/new-102863de})

[comment]: # ({new-1344257c})
##### Example 2

Message:

    Processor load is: {zabbix.zabbix.com:system.cpu.load[,avg1].last()}

When you receive the message, the message will be replaced by something
like:

    Processor load is: 1.45

[comment]: # ({/new-1344257c})

[comment]: # ({new-3713b8a5})
##### Example 3

Message:

    Latest value: {{HOST.HOST}:{ITEM.KEY}.last()}
    MAX for 15 minutes: {{HOST.HOST}:{ITEM.KEY}.max(900)}
    MIN for 15 minutes: {{HOST.HOST}:{ITEM.KEY}.min(900)}

When you receive the message, the message will be replaced by something
like:

    Latest value: 1.45
    MAX for 15 minutes: 2.33
    MIN for 15 minutes: 1.01

[comment]: # ({/new-3713b8a5})

[comment]: # ({new-88afe91c})
##### Example 4

Message:

    http://<server_ip_or_name>/zabbix/events.php?triggerid={TRIGGER.ID}&filter_set=1

When you receive the message, it will contain a link to all events of
the problem trigger.

[comment]: # ({/new-88afe91c})

[comment]: # ({new-d41797c6})
##### Example 5

Informing about values from several hosts in a trigger expression.

Message:

    Problem name: {TRIGGER.NAME}
    Trigger expression: {TRIGGER.EXPRESSION}
     
    1. Item value on {HOST.NAME1}: {ITEM.VALUE1} ({ITEM.NAME1})
    2. Item value on {HOST.NAME2}: {ITEM.VALUE2} ({ITEM.NAME2})

When you receive the message, the message will be replaced by something
like:

    Problem name: Processor load is too high on a local host
    Trigger expression: {Myhost:system.cpu.load[percpu,avg1].last()}>5 | {Myotherhost:system.cpu.load[percpu,avg1].last()}>5

    1. Item value on Myhost: 0.83 (Processor load (1 min average per core))
    2. Item value on Myotherhost: 5.125 (Processor load (1 min average per core))

[comment]: # ({/new-d41797c6})

[comment]: # ({new-0ee2664c})
##### Example 6

Receiving details of both the problem event and recovery event in a
[recovery](/manual/config/notifications/action/recovery_operations)
message:

Message:

    Problem:

    Event ID: {EVENT.ID}
    Event value: {EVENT.VALUE} 
    Event status: {EVENT.STATUS} 
    Event time: {EVENT.TIME}
    Event date: {EVENT.DATE}
    Event age: {EVENT.AGE}
    Event acknowledgement: {EVENT.ACK.STATUS} 
    Event acknowledgement history: {EVENT.ACK.HISTORY}

    Recovery: 

    Event ID: {EVENT.RECOVERY.ID}
    Event value: {EVENT.RECOVERY.VALUE} 
    Event status: {EVENT.RECOVERY.STATUS} 
    Event time: {EVENT.RECOVERY.TIME}
    Event date: {EVENT.RECOVERY.DATE}

When you receive the message, the macros will be replaced by something
like:

    Problem:

    Event ID: 21874
    Event value: 1 
    Event status: PROBLEM 
    Event time: 13:04:30
    Event date: 2014.01.02
    Event age: 5m
    Event acknowledgement: Yes 
    Event acknowledgement history: 2014.01.02 13:05:51 "John Smith (Admin)"
    -acknowledged-

    Recovery: 

    Event ID: 21896
    Event value: 0 
    Event status: OK 
    Event time: 13:10:07
    Event date: 2014.01.02

::: noteimportant
Separate notification macros for the original
problem event and recovery event are supported since Zabbix
2.2.0.
:::

[comment]: # ({/new-0ee2664c})
