[comment]: # translation:outdated

[comment]: # ({new-ec342157})
# 3 Additional operations

[comment]: # ({/new-ec342157})

[comment]: # ({new-748f7486})
#### Overview

In this section you may find some details of [additional
operations](/manual/config/notifications/action/operation) for
discovery/auto-registration events.

[comment]: # ({/new-748f7486})

[comment]: # ({new-903c037b})
##### Adding host

Hosts are added during the discovery process, as soon as a host is
discovered, rather than at the end of the discovery process.

::: notetip
As network discovery can take some time due to many
unavailable hosts/services having patience and using reasonable IP
ranges is advisable.
:::

When adding a host, its name is decided by the standard
**gethostbyname** function. If the host can be resolved, resolved name
is used. If not, the IP address is used. Besides, if IPv6 address must
be used for a host name, then all ":" (colons) are replaced by "\_"
(underscores), since colons are not allowed in host names.

::: noteimportant
If performing discovery by a proxy, currently
hostname lookup still takes place on Zabbix server.
:::

::: noteimportant
If a host already exists in Zabbix configuration
with the same name as a newly discovered one, versions of Zabbix prior
to 1.8 would add another host with the same name. Zabbix 1.8.1 and later
adds **\_N** to the hostname, where **N** is increasing number, starting
with 2.
:::

[comment]: # ({/new-903c037b})
