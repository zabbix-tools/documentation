[comment]: # translation:outdated

[comment]: # ({new-d40475f4})
# 1 E-mail

[comment]: # ({/new-d40475f4})

[comment]: # ({new-9d59e93d})
#### Overview

To configure e-mail as the delivery channel for messages, you need to
configure e-mail as the media type and assign specific addresses to
users.

[comment]: # ({/new-9d59e93d})

[comment]: # ({new-3711e99d})
#### Configuration

To configure e-mail as the media type:

-   Go to *Administration → Media types*
-   Click on *Create media type* (or click on *E-mail* in the list of
    pre-defined media types).

The **Media type** tab contains general media type attributes:

![](../../../../../assets/en/manual/config/media_email.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Name*|Name of the media type.|
|*Type*|Select *Email* as the type.|
|*SMTP server*|Set an SMTP server to handle outgoing messages.|
|*SMTP server port*|Set the SMTP server port to handle outgoing messages.<br>This option is supported *starting with Zabbix 3.0*.|
|*SMTP helo*|Set a correct SMTP helo value, normally a domain name.|
|*SMTP email*|The address entered here will be used as the **From** address for the messages sent.<br>Adding a sender display name (like "Zabbix-HQ" in *Zabbix-HQ <zabbix\@company.com>* in the screenshot above) with the actual e-mail address is supported since Zabbix 2.2 version.<br>There are some restrictions on display names in Zabbix emails in comparison to what is allowed by RFC 5322, as illustrated by examples:<br>Valid examples:<br>*zabbix\@company.com* (only email address, no need to use angle brackets)<br>*Zabbix HQ <zabbix\@company.com>* (display name and email address in angle brackets)<br>*∑Ω-monitoring <zabbix\@company.com>* (UTF-8 characters in display name)<br>Invalid examples:<br>*Zabbix HQ zabbix\@company.com* (display name present but no angle brackets around email address)<br>*"Zabbix\\@\\<H(comment)Q\\>" <zabbix\@company.com>* (although valid by RFC 5322, quoted pairs and comments are not supported in Zabbix emails)|
|*Connection security*|Select the level of connection security:<br>**None** - do not use the [CURLOPT\_USE\_SSL](http://curl.haxx.se/libcurl/c/CURLOPT_USE_SSL.html) option<br>**STARTTLS** - use the CURLOPT\_USE\_SSL option with CURLUSESSL\_ALL value<br>**SSL/TLS** - use of CURLOPT\_USE\_SSL is optional<br>This option is supported *starting with Zabbix 3.0*.|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the SMTP server.<br>The value of "SSLCALocation" server configuration directive should be put into [CURLOPT\_CAPATH](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html) for certificate validation.<br>This sets cURL option [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the SMTP server certificate matches.<br>This sets cURL option [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*Authentication*|Select the level of authentication:<br>**None** - no cURL options are set<br>(since 3.4.2) **Username and password** - implies "AUTH=\*" leaving the choice of authentication mechanism to cURL<br>(until 3.4.2) **Normal password** - [CURLOPT\_LOGIN\_OPTIONS](http://curl.haxx.se/libcurl/c/CURLOPT_LOGIN_OPTIONS.html) is set to "AUTH=PLAIN"<br>This option is supported *starting with Zabbix 3.0*.|
|*Username*|User name to use in authentication.<br>This sets the value of [CURLOPT\_USERNAME](http://curl.haxx.se/libcurl/c/CURLOPT_USERNAME.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*Password*|Password to use in authentication.<br>This sets the value of [CURLOPT\_PASSWORD](http://curl.haxx.se/libcurl/c/CURLOPT_PASSWORD.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*Enabled*|Mark the checkbox to enable the media type.|

::: noteimportant
To make SMTP authentication options available,
Zabbix server should be compiled with the --with-libcurl
[compilation](/manual/installation/install#configure_the_sources) option
with cURL 7.20.0 or higher. 
:::

[comment]: # ({/new-3711e99d})

[comment]: # ({new-98f29ec0})

#### Media type testing

To test whether a configured e-mail media type works correctly:

-   Locate the relevant e-mail in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Enter a *Send to* recipient address, message body and, optionally,
    subject.
-   Click on *Test* to send a test message.

Test success or failure message will be displayed in the same window:

![](../../../../../assets/en/manual/config/notifications/media/test_email0.png){width="600"}

[comment]: # ({/new-98f29ec0})

[comment]: # ({new-5e74f274})
#### Options

The **Options** tab in the e-mail media type
[configuration](/manual/config/notifications/media/email#configuration)
contains alert processing settings. The same set of options are
configurable for other media types, too.

All media types are processed in parallel. The maximum number of
concurrent sessions is configurable per media type, but the total number
of alerter processes on server can only be limited by the StartAlerters
[parameter](/manual/appendix/config/zabbix_server). Alerts generated by
one trigger are processed sequentially.

![](../../../../../assets/en/manual/config/media_email2.png)

|Parameter|Description|
|---------|-----------|
|*Concurrent sessions*|Select the number of parallel alerter sessions for the media type:<br>**One** - one session<br>**Unlimited** - unlimited number of sessions<br>**Custom** - select a custom number of sessions<br>Unlimited/high values mean more parallel sessions and increased capacity for sending notifications. Unlimited/high values should be used in large environments where lots of notifications may need to be sent simultaneously.|
|*Attempts*|Number of attempts for trying to send a notification. Up to 10 attempts can be specified; default value is '3'. If '1' is specified Zabbix will send the notification only once and will not retry if the sending fails.|
|*Retry interval*|Frequency of trying to resend a notification in case the sending failed, in seconds (0-60). If '0' is specified, Zabbix will retry immediately.<br>Time suffixes are supported, e.g. 5s, 1m.|

#### User media

To assign a specific address to the user:

-   Go to *Administration → Users*
-   Open the user properties form
-   In Media tab, click on *Add*

![](../../../../../assets/en/manual/config/user_email2.png){width="600"}

User media attributes:

|Parameter|Description|
|---------|-----------|
|*Type*|Select *Email* as the type.|
|*Send to*|Specify e-mail addresses to send the messages to.<br>To add more than one address click on *Add* below the address field. If multiple e-mail addresses are specified, one e-mail will be sent to all the specified recipients.<br>You may add the recipient display name (like "Recipient name" in *Recipient name <address1\@company.com>* in the screenshot above) with the actual e-mail address. See examples and restrictions on display name and email address in media type attribute [SMTP email](email#configuration) description.|
|*When active*|You can limit the time when messages are sent, for example, the working days only (1-5,09:00-18:00).<br>See the [Time period specification](/fr/manual/appendix/time_period) page for description of the format. [User macros](/manual/config/macros/user_macros) are supported.|
|*Use if severity*|Mark the checkboxes of trigger severities that you want to receive notifications for.<br>*Note* that the default severity ('Not classified') **must be** checked if you want to receive notifications for non-trigger [events](/manual/config/events).<br>After saving, the selected trigger severities will be displayed in the corresponding severity colours while unselected ones will be greyed out.|
|*Status*|Status of the user media.<br>**Enabled** - is in use.<br>**Disabled** - is not being used.|

[comment]: # ({/new-5e74f274})
