[comment]: # translation:outdated

[comment]: # ({new-adbf1c90})
# 2 SMS

[comment]: # ({/new-adbf1c90})

[comment]: # ({new-657f31be})
#### Overview

Zabbix supports the sending of SMS messages using a serial GSM modem
connected to Zabbix server's serial port.

Make sure that:

-   The speed of the serial device (normally /dev/ttyS0 under Linux)
    matches that of the GSM modem. Zabbix does not set the speed of the
    serial link. It uses default settings.
-   The 'zabbix' user has read/write access to the serial device. Run
    the command ls –l /dev/ttyS0 to see current permissions of the
    serial device.
-   The GSM modem has PIN entered and it preserves it after power reset.
    Alternatively you may disable PIN on the SIM card. PIN can be
    entered by issuing command AT+CPIN="NNNN" (NNNN is your PIN number,
    the quotes must be present) in a terminal software, such as Unix
    minicom or Windows HyperTerminal.

Zabbix has been tested with these GSM modems:

-   Siemens MC35
-   Teltonika ModemCOM/G10

To configure SMS as the delivery channel for messages, you also need to
configure SMS as the media type and enter the respective phone numbers
for the users.

[comment]: # ({/new-657f31be})

[comment]: # ({new-e7ed9491})
#### Configuration

To configure SMS as the media type:

-   Go to *Administration → Media types*
-   Click on *Create media type* (or click on *SMS* in the list of
    pre-defined media types).

The **Media type** tab contains general media type attributes:

|Parameter|Description|
|---------|-----------|
|*Description*|Name of the media type.|
|*Type*|Select *SMS* as the type.|
|*GSM modem*|Set the serial device name of the GSM modem.|

The **Options** tab contains alert [processing
settings](/manual/config/notifications/media/email#options) that are
common for all media types. Note that parallel processing of sending SMS
notifications is not possible.

[comment]: # ({/new-e7ed9491})

[comment]: # ({new-84a44424})
#### User media

To assign a phone number to the user:

-   Go to *Administration → Users*
-   Open the user properties form
-   In Media tab, click on *Add*

User media attributes:

|Parameter|Description|
|---------|-----------|
|*Type*|Select *SMS* as the type.|
|*Send to*|Specify the phone number to send messages to.|
|*When active*|You can limit the time when messages are sent, for example, the working days only (1-5,09:00-18:00).<br>See the [Time period specification](/fr/manual/appendix/time_period) page for description of the format.|
|*Use if severity*|Mark the checkboxes of trigger severities that you want to receive notifications for.<br>*Note* that the default severity ('Not classified') **must be** checked if you want to receive notifications for non-trigger [events](/manual/config/events).<br>After saving, the selected trigger severities will be displayed in the corresponding severity colours while unselected ones will be greyed out.|
|*Status*|Status of the user media.<br>**Enabled** - is in use.<br>**Disabled** - is not being used.|

[comment]: # ({/new-84a44424})
