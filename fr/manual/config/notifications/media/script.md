[comment]: # translation:outdated

[comment]: # ({new-e25ccc09})
# 5 Custom alertscripts

[comment]: # ({/new-e25ccc09})

[comment]: # ({new-bc410af8})
#### Overview

If you are not satisfied with existing media types for sending alerts
there is an alternative way to do that. You can create a script that
will handle the notification your way.

Alert scripts are executed on Zabbix server. These scripts are located
in the directory defined in the server [configuration
file](/manual/appendix/config/zabbix_server) **AlertScriptsPath**
variable.

Here is an example alert script:

``` {.bash}
#!/bin/bash

to=$1
subject=$2
body=$3

cat <<EOF | mail -s "$subject" "$to"
$body
EOF
```

::: noteimportant
Starting from version 3.4 Zabbix checks for the
exit code of the executed commands and scripts. Any exit code which is
different from **0** is considered as a [command
execution](/fr/manual/appendix/command_execution) error. In such case
Zabbix will try to repeat failed execution.
:::

Environment variables are not preserved or created for the script, so
they should be handled explicitly.

[comment]: # ({/new-bc410af8})

[comment]: # ({new-47055351})
#### Configuration

To configure custom alertscripts as the media type:

-   Go to *Administration → Media types*
-   Click on *Create media type*

The **Media type** tab contains general media type attributes:

![](../../../../../assets/en/manual/config/media_script.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Name*|Enter name of the media type.|
|*Type*|Select *Script* as the type.|
|*Script name*|Enter the name of the script.|
|*Script parameters*|Add command-line parameters to the script.<br>{ALERT.SENDTO}, {ALERT.SUBJECT} and {ALERT.MESSAGE} [macros](/manual/appendix/macros/supported_by_location) are supported in script parameters.<br>Customizing script parameters is supported since Zabbix 3.0.|

The **Options** tab contains alert [processing
settings](/manual/config/notifications/media/email#options) that are
common for all media types.

::: noteimportant
As parallel processing of media types is
implemented since Zabbix 3.4.0, it is important to note that with more
than one script media type configured, these scripts may be processed in
parallel by alerter processes. The total number of alerter processes is
limited by the StartAlerters
[parameter](/manual/appendix/config/zabbix_server).
:::

[comment]: # ({/new-47055351})

[comment]: # ({new-6a270eac})

#### Media type testing

To test a configured script media type:

-   Locate the relevant script in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Edit the script parameter values, if needed (editing affects the test procedure only, the actual values will not be changed).
-   Click on *Test*.

![](../../../../../assets/en/manual/config/notifications/media/script_test.png){width="600"}


[comment]: # ({/new-6a270eac})

[comment]: # ({new-525cb972})
#### User media

To assign custom alertscripts to the user:

-   Go to *Administration → Users*
-   Open the user properties form
-   In Media tab, click on *Add*

User media attributes:

|Parameter|Description|
|---------|-----------|
|*Type*|Select the custom alertscripts media type.|
|*Send to*|Specify the recipient to receive the alerts.|
|*When active*|You can limit the time when alertscripts are executed, for example, the working days only (1-5,09:00-18:00).<br>See the [Time period specification](/manual/appendix/time_period) page for description of the format.|
|*Use if severity*|Mark the checkboxes of trigger severities that you want to activate the alertscript for.<br>*Note* that the default severity ('Not classified') **must be** checked if you want to receive notifications for non-trigger [events](/manual/config/events).<br>After saving, the selected trigger severities will be displayed in the corresponding severity colours while unselected ones will be greyed out.|
|*Status*|Status of the user media.<br>**Enabled** - is in use.<br>**Disabled** - is not being used.|

[comment]: # ({/new-525cb972})
