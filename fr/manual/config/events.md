[comment]: # translation:outdated

[comment]: # ({new-b4b0e364})
# - \#4 Events

[comment]: # ({/new-b4b0e364})

[comment]: # ({new-3825c91b})
#### Overview

There are several types of events generated in Zabbix:

-   trigger events - whenever a trigger changes its status
    (*OK→PROBLEM→OK*)
-   discovery events - when hosts or services are detected
-   auto registration events - when active agents are auto-registered by
    server
-   internal events - when an item/low-level discovery rule becomes
    unsupported or a trigger goes into an unknown state

::: noteclassic
Internal events are supported starting with Zabbix 2.2
version.
:::

Events are time-stamped and can be the basis of actions such as sending
notification e-mail etc.

To view details of events in the frontend, go to *Monitoring* →
*Problems*. There you can click on the event date and time to view
details of an event.

More information is available on:

-   [trigger events](/manual/config/events/trigger_events)
-   [other event sources](/manual/config/events/sources)

[comment]: # ({/new-3825c91b})
