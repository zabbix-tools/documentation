[comment]: # translation:outdated

[comment]: # ({new-31b6b7d8})
# 1 Hôtes et groupes d’hôtes

[comment]: # ({/new-31b6b7d8})

[comment]: # ({new-61c2f2bf})
#### Qu’est-ce qu’un hôte ?

Typiquement, les hôtes Zabbix sont les périphériques que vous souhaitez
superviser (serveurs, postes de travail, commutateurs, etc.).

La création d'hôtes est l'une des premières tâches de surveillance dans
Zabbix. Par exemple, si vous voulez surveiller certains paramètres sur
un serveur "x", vous devez d'abord créer un hôte appelé, par exemple,
"Serveur X", puis vous pouvez ajouter des éléments de supervision.

Les hôtes sont organisés en groupes hôtes.

Continuer vers la [création et la configuration d'un
hôte](/fr/manual/config/hosts/host).

[comment]: # ({/new-61c2f2bf})
