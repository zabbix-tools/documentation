<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/config/users_and_usergroups/user.md">
    <body>
      <trans-unit id="6f10822d" xml:space="preserve">
        <source># 1 Configuring a user</source>
      </trans-unit>
      <trans-unit id="5f8fb5e5" xml:space="preserve">
        <source>#### Overview

The initial Zabbix installation has two predefined users:

-   *Admin* - a Zabbix
    [superuser](/manual/config/users_and_usergroups/permissions#user_type)
    with full permissions.
-   *guest* - a special Zabbix
    [user](/manual/config/users_and_usergroups/permissions#user_type).
    The 'guest' user is disabled by default. If you add it to the Guests
    user group, you may access monitoring pages in Zabbix without being
    logged in. Note that by default, 'guest' has no permissions on
    Zabbix objects.

To configure a user:

-   Go to *Users → Users*.
-   Click on *Create user* (or on a user name to edit an existing
    user).
-   Edit user attributes in the form.</source>
      </trans-unit>
      <trans-unit id="2b69e181" xml:space="preserve">
        <source>#### General attributes

The *User* tab contains general user attributes:

![](../../../../assets/en/manual/config/user.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|--|--------|
|*Username*|Unique username, used as the login name.|
|*Name*|User first name (optional).&lt;br&gt;If not empty, visible in acknowledgment information and notification recipient information.|
|*Last name*|User last name (optional).&lt;br&gt;If not empty, visible in acknowledgment information and notification recipient information.|
|*Groups*|Select [user groups](usergroup) the user belongs to. Starting with Zabbix 3.4.3 this field is auto-complete so starting to type the name of a user group will offer a dropdown of matching groups. Scroll down to select. Alternatively, click on *Select* to add groups. Click on 'x' to remove the selected.&lt;br&gt;Adherence to user groups determines what host groups and hosts the user will have [access to](permissions).|
|*Password*|Two fields for entering the user password, or a *Change password* button if the user already exists.&lt;br&gt;Clicking on the *Change password* button opens two fields for entering a new password.&lt;br&gt;For the user with the *Super admin role* changing own password, clicking on the *Change password* button opens an additional field for entering the current (old) password.&lt;br&gt;On a successful password change, the user for which the password was changed will be logged out of all active sessions.&lt;br&gt;Note that the password can only be changed for users using Zabbix [internal authentication](/manual/web_interface/frontend_sections/users/authentication#default-authentication).|
|*Language*|Language of the Zabbix frontend.&lt;br&gt;The php gettext extension is required for the translations to work.|
|*Time zone*|Select the time zone to override global [time zone](/manual/web_interface/time_zone#overview) on user level or select **System default** to use global time zone settings.|
|*Theme*|Defines how the frontend looks like:&lt;br&gt;**System default** - use default system settings&lt;br&gt;**Blue** - standard blue theme&lt;br&gt;**Dark** - alternative dark theme&lt;br&gt;**High-contrast light** - light theme with high contrast&lt;br&gt;**High-contrast dark** - dark theme with high contrast|
|*Auto-login*|Mark this checkbox to make Zabbix remember the user and log the user in automatically for 30 days. Browser cookies are used for this.|
|*Auto-logout*|With this checkbox marked the user will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).&lt;br&gt;[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 90s, 5m, 2h, 1d.&lt;br&gt;Note that this option will not work:&lt;br&gt;\* If the "Show warning if Zabbix server is down" global configuration option is enabled and Zabbix frontend is kept open.&lt;br&gt;\* When Monitoring menu pages perform background information refreshes.&lt;br&gt;\* If logging in with the *Remember me for 30 days* option checked.|
|*Refresh*|Set the refresh rate used for graphs, plain text data, etc. Can be set to 0 to disable.|
|*Rows per page*|You can determine how many rows per page will be displayed in lists.|
|*URL (after login)*|You can make Zabbix transfer the user to a specific URL after successful login, for example, to *Problems* page.|</source>
      </trans-unit>
      <trans-unit id="f35e11e2" xml:space="preserve">
        <source>#### User media

The *Media* tab contains a listing of all media defined for the user.
Media are used for sending notifications. 

![](../../../../assets/en/manual/config/user_media.png){width="600"}

Click on *Add* to assign media to the user.

If the media type has been disabled:

-   A yellow info icon is displayed after the name.
-   *Disabled* is displayed in the Status column.

See the [Media types](/manual/config/notifications/media#user_media)
section for details on configuring user media.</source>
      </trans-unit>
      <trans-unit id="bc3b9c76" xml:space="preserve">
        <source>#### Permissions

The *Permissions* tab contains information on:

-   The user role. It is mandatory for any newly created user, and can only be changed 
    by a Super admin user.
-   The user type (User, Admin, Super Admin) that is defined in the role
    configuration.
-   Host and template groups the user has access to. Users of type 'User' and 'Admin'
    do not have access to any groups, templates and hosts by default. To get
    the access they need to be included in user groups that have access
    to respective entities.
-   Access rights to sections and elements of Zabbix frontend, modules,
    and API methods. Elements to which access is allowed are displayed
    in green color. Light gray color means that access to the element is
    denied.
-   Rights to perform certain actions. Actions that are allowed are
    displayed in green color. Light gray color means that a user does
    not have the rights to perform this action.

See the [User permissions](permissions) page for details.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
