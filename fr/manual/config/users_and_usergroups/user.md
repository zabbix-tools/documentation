[comment]: # translation:outdated

[comment]: # ({new-6f10822d})
# 1 Configuring a user

[comment]: # ({/new-6f10822d})

[comment]: # ({new-5f8fb5e5})
#### Overview

To configure a user:

-   Go to *Administration → Users*
-   Click on *Create user* (or on the user name to edit an existing
    user)
-   Edit user attributes in the form

[comment]: # ({/new-5f8fb5e5})

[comment]: # ({new-2b69e181})
#### General attributes

The *User* tab contains general user attributes:

![](../../../../assets/en/manual/config/user.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Alias*|Unique username, used as the login name.|
|*Name*|User first name (optional).<br>If not empty, visible in acknowledgement information and notification recipient information.|
|*Surname*|User second name (optional).<br>If not empty, visible in acknowledgement information and notification recipient information.|
|*Groups*|Select [user groups](usergroup) the user belongs to. Starting with Zabbix 3.4.3 this field is auto-complete so starting to type the name of a user group will offer a dropdown of matching groups. Scroll down to select. Alternatively, click on *Select* to add groups. Click on 'x' to remove the selected.<br>Adherence to user groups determines what host groups and hosts the user will have [access to](permissions).|
|*Password*|Two fields for entering the user password.<br>With an existing password, contains a *Password* button, clicking on which opens the password fields.|
|*Language*|Language of the Zabbix frontend.<br>The php gettext extension is required for the translations to work.|
|*Theme*|Defines how the frontend looks like:<br>**System default** - use default system settings<br>**Blue** - standard blue theme<br>**Dark** - alternative dark theme<br>**High-contrast light** - light theme with high contrast<br>**High-contrast dark** - dark theme with high contrast|
|*Auto-login*|Mark this checkbox to make Zabbix remember the user and log the user in automatically for 30 days. Browser cookies are used for this.|
|*Auto-logout*|With this checkbox marked the user will be logged out automatically, after the set amount of seconds (minimum 90 seconds, maximum 1 day).<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 90s, 5m, 2h, 1d.<br>Note that this option will not work:<br>\* If the "Show warning if Zabbix server is down" global configuration option is enabled and Zabbix frontend is kept open;<br>\* When Monitoring menu pages perform background information refreshes;<br>\* If logging in with the *Remember me for 30 days* option checked.|
|*Refresh*|Set the refresh rate used for graphs, screens, plain text data, etc. Can be set to 0 to disable.|
|*Rows per page*|You can determine how many rows per page will be displayed in lists.|
|*URL (after login)*|You can make Zabbix transfer the user to a specific URL after successful login, for example, to Problems page.|

[comment]: # ({/new-2b69e181})

[comment]: # ({new-f35e11e2})
#### User media

The *Media* tab contains a listing of all media defined for the user.
Media are used for sending notifications. Click on *Add* to assign media
to the user.

See the [Media types](/fr/manual/config/notifications/media) section for
details on configuring media types.

[comment]: # ({/new-f35e11e2})

[comment]: # ({new-bc3b9c76})
#### Permissions

The *Permissions* tab contains information on:

-   the user type (Zabbix User, Zabbix Admin, Zabbix Super Admin). Users
    cannot change their own type.
-   host groups the user has access to. 'Zabbix User' and 'Zabbix Admin'
    users do not have access to any host groups and hosts by default. To
    get access they need to be included in user groups that have access
    to respective host groups and hosts.

See the [User permissions](permissions) page for details.

[comment]: # ({/new-bc3b9c76})
