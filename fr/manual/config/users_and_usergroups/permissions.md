[comment]: # translation:outdated

[comment]: # ({new-ba72dbf1})
# 2 Permissions

[comment]: # ({/new-ba72dbf1})

[comment]: # ({new-a9157075})
#### Overview

You can differentiate user permissions in Zabbix by defining the
respective user type and then by including the unprivileged users in
user groups that have access to host group data.

[comment]: # ({/new-a9157075})

[comment]: # ({new-ac897fff})

#### User types

Permissions in Zabbix depend, primarily, on the user type:

   - *User* - has limited access rights to menu sections (see below) and no access to any resources by default. Any permissions to host or template groups must be explicitly assigned;
   - *Admin* - has incomplete access rights to menu sections (see below). The user has no access to any host groups by default. Any permissions to host or template groups must be explicitly given;
   - *Super admin* - has access to all menu sections. The user has a read-write access to all host and template groups. Permissions cannot be revoked by denying access to specific groups.

**Menu access**

The following table illustrates access to Zabbix menu sections per user type:

|Menu section|<|User|Admin|Super admin|
|-|-------------|------|------|------|
|**Dashboards**|<|+|+|+|
|**Monitoring**|<|+|+|+|
| |*Problems*|+|+|+|
|^|*Hosts*|+|+|+|
|^|*Latest data*|+|+|+|
|^|*Maps*|+|+|+|
|^|*Discovery*| |+|+|
|**Services**|<|+|+|+|
| |*Services*|+|+|+|
|^|*SLA*| |+|+|
|^|*SLA report*|+|+|+|
|**Inventory**|<|+|+|+|
| |*Overview*|+|+|+|
|^|*Hosts*|+|+|+|
|**Reports**|<|+|+|+|
| |*System information*| | |+|
|^|*Scheduled reports*| |+|+|
|^|*Availability report*|+|+|+|
|^|*Triggers top 100*|+|+|+|
|^|*Audit log*| | |+|
|^|*Action log*| | |+|
|^|*Notifications*| |+|+|
|**Data collection**|<| |+|+|
| |*Template groups*| |+|+|
|^|*Host groups*| |+|+|
|^|*Templates*| |+|+|
|^|*Hosts*| |+|+|
|^|*Maintenance*| |+|+|
|^|*Event correlation*| | |+|
|^|*Discovery*| |+|+|
|**Alerts**|<| |+|+|
| |*Trigger actions*| |+|+|
| |*Service actions*| |+|+|
| |*Discovery actions*| |+|+|
| |*Autoregistration actions*| |+|+|
| |*Internal actions*| |+|+|
|^|*Media types*| | |+|
|^|*Scripts*| | |+|
|**Users**|<| | |+|
| |*User groups*| | |+|
|^|*User roles*| | |+|
|^|*Users*| | |+|
|^|*API tokens*| | |+|
|^|*Authentication*| | |+|
|**Administration**|<| | |+|
| |*General*| | |+|
|^|*Audit log*| | |+|
|^|*Housekeeping*| | |+|
|^|*Proxies*| | |+|
|^|*Macros*| | |+|
|^|*Queue*| | |+|

[comment]: # ({/new-ac897fff})

[comment]: # ({new-a852da1b})
#### User type

The user type defines the level of access to administrative menus and
the default access to host group data.

|User type|Description|
|---------|-----------|
|*Zabbix User*|The user has access to the Monitoring menu. The user has no access to any resources by default. Any permissions to host groups must be explicitly assigned.|
|*Zabbix Admin*|The user has access to the Monitoring and Configuration menus. The user has no access to any host groups by default. Any permissions to host groups must be explicitly given.|
|*Zabbix Super Admin*|The user has access to everything: Monitoring, Configuration and Administration menus. The user has a read-write access to all host groups. Permissions cannot be revoked by denying access to specific host groups.|

[comment]: # ({/new-a852da1b})

[comment]: # ({new-f38cb50e})
#### Permissions to host groups

Access to any host data in Zabbix are granted to [user
groups](/manual/config/users_and_usergroups/usergroup) on host group
level only.

That means that an individual user cannot be directly granted access to
a host (or host group). It can only be granted access to a host by being
part of a user group that is granted access to the host group that
contains the host.

[comment]: # ({/new-f38cb50e})
