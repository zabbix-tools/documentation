[comment]: # translation:outdated

[comment]: # ({new-94205f99})
# 2 Éléments

[comment]: # ({/new-94205f99})

[comment]: # ({new-3f680eac})
#### Aperçu

Les éléments sont ceux qui collectent les données d'un hôte.

Une fois que vous avez configuré un hôte, vous devez ajouter des
éléments de surveillance pour commencer à obtenir des données réelles.

Un élément est une mesure individuelle. Une façon d'ajouter rapidement
de nombreux éléments consiste à attacher l'un des modèles prédéfinis à
un hôte. Cependant, pour optimiser les performances du système, vous
devrez peut-être affiner les modèles afin d'avoir uniquement les
éléments nécessaires et une fréquence de surveillance adaptée à votre
environnement.

Dans un élément individuel, vous spécifiez quel type de donnée sera
collectée à partir de l'hôte.

Pour ce faire, vous utilisez la [clé de
l'élément](/fr/manual/config/items/item/key). Ainsi, un élément avec la
clé **system.cpu.load** va rassembler les données de la charge du
processeur, tandis qu'un élément avec la clé **net.if.in** rassemblera
les informations de trafic entrant.

Pour spécifier d'autres paramètres avec une clé, vous devrez les inclure
entre crochets après le nom de la clé. Ainsi,
system.cpu.load**\[avg5\]** retournera la moyenne de charge du
processeur pour les 5 dernières minutes, alors que net.if.in**\[eth0\]**
affichera le trafic entrant dans l'interface eth0.

::: noteclassic
Pour tous les types d'éléments et les clés d'éléments pris
en charge, consultez les sections individuelles des [types
d'éléments](/fr/manual/config/items/itemtypes). 
:::

Passez à la [création et à la configuration d'un
élément](/fr/manual/config/items/item).

[comment]: # ({/new-3f680eac})
