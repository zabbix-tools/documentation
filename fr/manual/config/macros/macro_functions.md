[comment]: # translation:outdated

[comment]: # ({new-0458cee0})
# 1 Macro functions

[comment]: # ({/new-0458cee0})

[comment]: # ({new-c1632a9e})
#### Overview

Macro functions offer the ability to customize
[macro](/manual/config/macros) values.

Sometimes a macro may resolve to a value that is not necessarily easy to
work with. It may be long or contain a specific substring of interest
that you would like to extract. This is where macro functions can be
useful.

The syntax of a macro function is:

    {<macro>.<func>(<params>)}

where:

-   <macro> - the macro to customize (for example {ITEM.VALUE})
-   <func> - the function to apply
-   <params> - a comma-delimited list of function parameters.
    Parameters must be quoted if they start with '' '' (space), `"` or
    contain `)`, `,`.

For example:

    {{ITEM.VALUE}.regsub(pattern, output)}

[comment]: # ({/new-c1632a9e})

[comment]: # ({new-a0ad7754})
#### Supported macro functions

|FUNCTION|<|<|<|<|
|--------|-|-|-|-|
|<|**Description**|**Parameters**|**Supported for**|<|
|**regsub** (<pattern>,<output>)|<|<|<|<|
|<|Substring extraction by a regular expression match (case sensitive).|**pattern** - the regular expression to match<br>**output** - the output options. **\\1 - \\9** placeholders are supported to capture groups. **\\0** returns the matched text.<br><br>If `pattern` is not a correct regular expression 'UNKNOWN' is returned.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}|<|
|**iregsub** (<pattern>,<output>)|<|<|<|<|
|<|Substring extraction by a regular expression match (case insensitive).|**pattern** - the regular expression to match<br>**output** - the output options. **\\1 - \\9** placeholders are supported to capture groups. **\\0** returns the matched text.<br><br>If `pattern` is not a correct regular expression 'UNKNOWN' is returned.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}|<|

If a function is used in a [supported
location](/fr/manual/appendix/macros/supported_by_location), but applied
to a macro not supporting macro functions, then the macro evaluates to
'UNKNOWN'.

If a macro function is applied to the macro in locations not supporting
macro functions then the function is ignored.

[comment]: # ({/new-a0ad7754})

[comment]: # ({new-9423d893})
#### Examples

The ways in which macro functions can be used to customize macro values
is illustrated in the following examples containing log lines as
received value:

|Received value|Macro|Output|
|--------------|-----|------|
|`123Log line`|`{{ITEM.VALUE}.regsub(^[0-9]+, Problem)}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", "Problem")}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", Problem ID: \1)}`|`Problem ID: 123`|
|`Log line`|`{{ITEM.VALUE}.regsub(".*", "Problem ID: \1")}`|''Problem ID: ''|
|`MySQL crashed errno 123`|`{{ITEM.VALUE}.regsub("^([A-Z]+).*([0-9]+)", " Problem ID: \1_\2 ")}`|'' Problem ID: MySQL\_123 ''|
|`123 Log line`|`{{ITEM.VALUE}.regsub("([1-9]+", "Problem ID: \1")}`|`*UNKNOWN*` (invalid regular expression)|

[comment]: # ({/new-9423d893})
