[comment]: # translation:outdated

[comment]: # ({new-2d9d6e33})
# 3 Low-level discovery macros

[comment]: # ({/new-2d9d6e33})

[comment]: # ({new-bb12a85a})
#### Overview

There is a type of macro used within the [low-level
discovery](/fr/manual/discovery/low_level_discovery) function:

    {#MACRO} 

It is a macro that is used in an LLD rule and returns real values of
file system names, network interfaces and SNMP OIDs.

These macros can be used for creating item, trigger and graph
*prototypes*. Then, when discovering real file systems, network
interfaces etc., these macros are substituted with real values and are
the basis for creating real items, triggers and graphs.

These macros are also used in creating host and host group *prototypes*
in virtual machine [discovery](/manual/vm_monitoring#host_prototypes).

[comment]: # ({/new-bb12a85a})

[comment]: # ({new-fc325ce1})
#### Supported locations

LLD macros can be used:

-   for item prototypes in
    -   names
    -   key parameters
    -   units
    -   update intervals
    -   history storage periods
    -   trend storage periods
    -   SNMP OIDs
    -   IPMI sensor fields
    -   calculated item formulas
    -   SSH and Telnet scripts
    -   database monitoring SQL queries
    -   JMX item endpoint fields
    -   descriptions
    -   since Zabbix 4.0 also in:
        -   item value preprocessing steps
        -   HTTP agent URL field
        -   HTTP agent HTTP query fields field
        -   HTTP agent request body field
        -   HTTP agent required status codes field
        -   HTTP agent headers field key and value
        -   HTTP agent HTTP authentication username field
        -   HTTP agent HTTP authentication password field
        -   HTTP agent HTTP proxy field
        -   HTTP agent HTTP SSL certificate file field
        -   HTTP agent HTTP SSL key file field
        -   HTTP agent HTTP SSL key password field
        -   HTTP agent HTTP timeout field
-   for trigger prototypes in
    -   names
    -   expressions
    -   URLs
    -   descriptions
    -   event tag names and values
-   for graph prototypes in
    -   names
-   for host prototypes in
    -   names
    -   visible names
    -   host group prototype names
    -   (see the [full list](/manual/vm_monitoring/discovery_fields))

In all those places LLD macros can be used inside user [macro
context](/manual/config/macros/user_macros#user_macro_context).

Some low-level discovery macros come "pre-packaged" with the LLD
function in Zabbix - {\#FSNAME}, {\#FSTYPE}, {\#IFNAME}, {\#SNMPINDEX},
{\#SNMPVALUE}. However, adhering to these names is not compulsory when
creating a
[custom](/fr/manual/discovery/low_level_discovery#creating_custom_lld_rules)
low-level discovery rule. Then you may use any other LLD macro name and
refer to that name.

[comment]: # ({/new-fc325ce1})

[comment]: # ({new-5b26ab61})
#### Using macro functions

Macro functions are supported with low-level discovery macros (except in
low-level discovery rule filter), allowing to extract a certain part of
the macro value using a regular expression.

For example, you may want to extract the customer name and interface
number from the following LLD macro for the purposes of event tagging:

    {#IFALIAS}=customername_1

To do so, the `regsub` macro function can be used with the macro in the
event tag value field of a trigger prototype:

![](../../../../assets/en/manual/config/macros/lld_macro_function.png)

Note, that commas are not allowed in unquoted item [key
parameters](/manual/config/items/item/key#key_parameters), so the
parameter containing a macro function has to be quoted. The backslash
(`\`) character should be used to escape double quotes inside the
parameter. Example:

    net.if.in["{{#IFALIAS}.regsub(\"(.*)_([0-9]+)\", \1)}",bytes]

For more information on macro function syntax, see: [Macro
functions](/manual/config/macros/macro_functions)

Macro functions are supported in low-level discovery macros since Zabbix
4.0.

[comment]: # ({/new-5b26ab61})

[comment]: # ({new-573cd0dc})
##### Footnotes

^**1**^ In the fields marked with ^[1](lld_macros#footnotes)^ a single
macro has to fill the whole field. Multiple macros in a field or macros
mixed with text are not supported.

[comment]: # ({/new-573cd0dc})
