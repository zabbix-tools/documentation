[comment]: # translation:outdated

[comment]: # ({new-f733158d})
# 8 Templates out of the box

[comment]: # ({/new-f733158d})

[comment]: # ({new-0cc95643})
#### Overview

Zabbix strives to provide a growing list of useful out-of-the-box
[templates](/manual/config/templates). Out-of-the-box templates come
preconfigured and thus are a useful way for speeding up the deployment
of monitoring jobs.

[comment]: # ({/new-0cc95643})
