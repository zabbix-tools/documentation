[comment]: # translation:outdated

[comment]: # ({new-3a1b4ec0})
# 7 Mass update

[comment]: # ({/new-3a1b4ec0})

[comment]: # ({new-66063302})
#### Overview

With mass update you may change some attribute for a number of triggers
at once, saving you the need to open each individual trigger for
editing.

[comment]: # ({/new-66063302})

[comment]: # ({new-00b96653})
#### Using mass update

To mass-update some triggers, do the following:

-   Mark the checkboxes of the triggers to update in the list
-   Click on *Mass update* below the list
-   Mark the checkboxes of the attributes to update
-   Specify new values for the attributes and click on *Update*

![](../../../../assets/en/manual/config/trigger_mass_update.png)

*Replace dependencies* and *Replace tags* will replace existing trigger
dependencies/tags (if any) with the ones specified in mass update.

[comment]: # ({/new-00b96653})
