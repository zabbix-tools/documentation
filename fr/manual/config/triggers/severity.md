[comment]: # translation:outdated

[comment]: # ({new-f6de9b10})
# 4 Trigger severity

Trigger severity defines how important a trigger is. Zabbix supports the
following trigger severities:

|SEVERITY|DEFINITION|COLOUR|
|--------|----------|------|
|**Not classified**|Unknown severity.|Grey|
|**Information**|For information purposes.|Light blue|
|**Warning**|Be warned.|Yellow|
|**Average**|Average problem.|Orange|
|**High**|Something important has happened.|Light red|
|**Disaster**|Disaster. Financial losses, etc.|Red|

The severities are used for:

-   visual representation of triggers. Different colours for different
    severities.
-   audio in global alarms. Different audio for different severities.
-   user media. Different media (notification channel) for different
    severities. For example, SMS - high severity, email - other.
-   limiting actions by conditions against trigger severities

It is possible to [customise trigger severity names and
colours](customseverities).

[comment]: # ({/new-f6de9b10})
