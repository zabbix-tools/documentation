[comment]: # translation:outdated

[comment]: # ({new-7a75c626})
# 1 Standardized templates for network devices

[comment]: # ({/new-7a75c626})

[comment]: # ({new-1dc52a1f})
#### Overview

In order to provide monitoring for network devices such as switches and
routers, we have created two so-called models: for the network device
itself (its chassis basically) and for network interface.

Since Zabbix 3.4 templates for many families of network devices are
provided. All templates cover (where possible to get these items from
the device):

-   Chassis fault monitoring (power supplies, fans and temperature,
    overall status)
-   Chassis performance monitoring (CPU and memory items)
-   Chassis inventory collection (serial numbers, model name, firmware
    version)
-   Network interface monitoring with IF-MIB and EtherLike-MIB
    (interface status, interface traffic load, duplex status for
    Ethernet)

These templates are available:

-   In *Configuration* → *Templates* in new installations;
-   In the [official Zabbix template
    repository](https://www.zabbix.org/wiki/Zabbix_Templates/Official_Templates).
    If you have upgraded from a pre-3.4 Zabbix version, you can import
    these templates from XMLs.

If you are importing the new out-of-the-box templates, you may want to
also update the `@Network interfaces for discovery` global regular
expression to:

    Result is FALSE: ^Software Loopback Interface
    Result is FALSE: ^(In)?[lL]oop[bB]ack[0-9._]*$
    Result is FALSE: ^NULL[0-9.]*$
    Result is FALSE: ^[lL]o[0-9.]*$
    Result is FALSE: ^[sS]ystem$
    Result is FALSE: ^Nu[0-9.]*$

to filter out loopbacks and null interfaces on most systems.

[comment]: # ({/new-1dc52a1f})

[comment]: # ({new-b5f67cef})
#### Devices

List of device families for which templates are available:

|Template name|Vendor|Device family|Known models|OS|MIBs used|**[Tags](/manual/config/templates_out_of_the_box/network_devices#tags)**|
|-------------|------|-------------|------------|--|---------|------------------------------------------------------------------------|
|*Template Net Alcatel Timetra TiMOS SNMPv2*|Alcatel|Alcatel Timetra|ALCATEL SR 7750|TiMOS|TIMETRA-SYSTEM-MIB,TIMETRA-CHASSIS-MIB|Certified|
|*Template Net Brocade FC SNMPv2*|Brocade|Brocade FC switches|Brocade 300 SAN Switch-|\-|SW-MIB,ENTITY-MIB|Performance, Fault|
|*Template Net Brocade\_Foundry Stackable SNMPv2*|Brocade|Brocade ICX|Brocade ICX6610, Brocade ICX7250-48, Brocade ICX7450-48F|<|FOUNDRY-SN-AGENT-MIB, FOUNDRY-SN-STACKING-MIB|Certified|
|*Template Net Brocade\_Foundry Nonstackable SNMPv2*|Brocade, Foundry|Brocade MLX, Foundry|Brocade MLXe, Foundry FLS648, Foundry FWSX424|<|FOUNDRY-SN-AGENT-MIB|Performance, Fault|
|*Template Net Cisco IOS SNMPv2*|Cisco|Cisco IOS ver > 12.2 3.5|Cisco C2950|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Certified|
|*Template Net Cisco releases later than 12.0\_3\_T and prior to 12.2\_3.5\_ SNMPv2*|Cisco|Cisco IOS > 12.0 3 T and < 12.2 3.5|\-|IOS|CISCO-PROCESS-MIB,CISCO-MEMORY-POOL-MIB,CISCO-ENVMON-MIB|Certified|
|*Template Net Cisco releases prior to 12.0\_3\_T SNMPv2*|Cisco|Cisco IOS < 12.0 3 T|\-|IOS|OLD-CISCO-CPU-MIB,CISCO-MEMORY-POOL-MIB|Certified|
|*Template Net D-Link DES\_DGS Switch SNMPv2*|D-Link|DES/DGX switches|D-Link DES-xxxx/DGS-xxxx,DLINK DGS-3420-26SC|\-|DLINK-AGENT-MIB,EQUIPMENT-MIB,ENTITY-MIB|Certified|
|*Template Net D-Link DES 7200 SNMPv2*|D-Link|DES-7xxx|D-Link DES 7206|\-|ENTITY-MIB,MY-SYSTEM-MIB,MY-PROCESS-MIB,MY-MEMORY-MIB|Performance Fault Interfaces|
|*Template Net Dell Force S-Series SNMPv2*|Dell|Dell Force S-Series|S4810|<|F10-S-SERIES-CHASSIS-MIB|Certified|
|*Template Net Extreme Exos SNMPv2*|Extreme|Extreme EXOS|X670V-48x|EXOS|EXTREME-SYSTEM-MIB,EXTREME-SOFTWARE-MONITOR-MIB|Certified|
|*Template Net Huawei VRP SNMPv2*|Huawei|Huawei VRP|S2352P-EI|\-|ENTITY-MIB,HUAWEI-ENTITY-EXTENT-MIB|Certified|
|*Template Net Intel\_Qlogic Infiniband SNMPv2*|Intel/QLogic|Intel/QLogic Infiniband devices|Infiniband 12300|<|ICS-CHASSIS-MIB|Fault Inventory|
|*Template Net Juniper SNMPv2*|Juniper|MX,SRX,EX models|Juniper MX240, Juniper EX4200-24F|JunOS|JUNIPER-MIB|Certified|
|*Template Net Mellanox SNMPv2*|Mellanox|Mellanox Infiniband devices|SX1036|MLNX-OS|HOST-RESOURCES-MIB,ENTITY-MIB,ENTITY-SENSOR-MIB,MELLANOX-MIB|Certified|
|*Template Net Mikrotik SNMPv2*|Mikrotik|Mikrotik RouterOS devices|Mikrotik CCR1016-12G, Mikrotik RB2011UAS-2HnD, Mikrotik 912UAG-5HPnD, Mikrotik 941-2nD, Mikrotik 951G-2HnD, Mikrotik 1100AHx2|RouterOS|MIKROTIK-MIB,HOST-RESOURCES-MIB|Certified|
|*Template Net QTech QSW SNMPv2*|QTech|Qtech devices|Qtech QSW-2800-28T|\-|QTECH-MIB,ENTITY-MIB|Performance Inventory|
|*Template Net Ubiquiti AirOS SNMPv1*|Ubiquiti|Ubiquiti AirOS wireless devices|NanoBridge,NanoStation,Unifi|AirOS|FROGFOOT-RESOURCES-MIB,IEEE802dot11-MIB|Performance|
|*Template Net HP Comware HH3C SNMPv2*|HP|HP (H3C) Comware|HP A5500-24G-4SFP HI Switch|<|HH3C-ENTITY-EXT-MIB,ENTITY-MIB|Certified|
|*Template Net HP Enterprise Switch SNMPv2*|HP|HP Enterprise Switch|HP ProCurve J4900B Switch 2626, HP J9728A 2920-48G Switch|<|STATISTICS-MIB,NETSWITCH-MIB,HP-ICF-CHASSIS,ENTITY-MIB,SEMI-MIB|Certified|
|*Template Net TP-LINK SNMPv2*|TP-LINK|TP-LINK|T2600G-28TS v2.0|<|TPLINK-SYSMONITOR-MIB,TPLINK-SYSINFO-MIB|Performance Inventory|
|*Template Net Netgear Fastpath SNMPv2*|Netgear|Netgear Fastpath|M5300-28G|<|FASTPATH-SWITCHING-MIB,FASTPATH-BOXSERVICES-PRIVATE-MIB|Fault Inventory|

[comment]: # ({/new-b5f67cef})

[comment]: # ({new-07c98c5a})
#### Template design

Templates were designed with the following in mind:

-   User macros are used as much as possible so triggers can be tuned by
    the user
-   Low-level discovery is used as much as possible to minimize the
    number of unsuppported items
-   Templates are provided for SNMPv2. SNMPv1 is used if it is known
    that the majority of devices don't support SNMPv2.
-   All templates depend on Template ICMP Ping so all devices are also
    checked by ICMP
-   Items don't use any MIBs - SNMP OIDs are used in items and low-level
    discoveries. So it's not necessary to load any MIBs into Zabbix for
    templates to work.
-   Loopback network interfaces are filtered when discovering as well as
    interfaces with ifAdminStatus = down(2)
-   64bit counters are used from IF-MIB::ifXTable where possible. If it
    is not supported, default 32bit counters are used instead.
-   All discovered network interfaces have a trigger that controls its
    operational status(link).
    -   If you do no want to monitor this condition for a specific
        interface create a user macro with context with the value 0. For
        example:

![](../../../../assets/en/manual/config/template_ifcontrol.png)

where Gi0/0 is {\#IFNAME}. That way the trigger is not used any more for
this specific interface.

      * You can also change the default behaviour for all triggers not to fire and activate this trigger only to limited number of interfaces like uplinks

![](../../../../assets/en/manual/config/template_ifcontrol2.png)

[comment]: # ({/new-07c98c5a})

[comment]: # ({new-6d84c190})
#### Tags

-   Performance – device family MIBs provide a way to monitor CPU and
    memory items;
-   Fault - device family MIBs provide a way to monitor at least one
    temperature sensor;
-   Inventory – device family MIBs provide a way to collect at least the
    device serial number and model name;
-   Certified – all three main categories above are covered.

[comment]: # ({/new-6d84c190})
