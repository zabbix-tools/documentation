[comment]: # translation:outdated

[comment]: # ({new-03ce9d2a})
# 10 Macros

[comment]: # ({/new-03ce9d2a})

[comment]: # ({new-4c81df7a})
#### Overview

Zabbix supports a number of macros which may be used in various
situations. Macros are variables, identified by a specific syntax:

    {MACRO} 

Macros resolve to a specific value depending on the context.

Effective use of macros allows to save time and make Zabbix
configuration more transparent.

In one of typical uses, a macro may be used in a template. Thus a
trigger on a template may be named "Processor load is too high on
{HOST.NAME}". When the template is applied to the host, such as Zabbix
server, the name will resolve to "Processor load is too high on Zabbix
server" when the trigger is displayed in the Monitoring section.

Macros may be used in item key parameters. A macro may be used for only
a part of the parameter, for example
`item.key[server_{HOST.HOST}_local]`. Double-quoting the parameter is
not necessary as Zabbix will take care of any ambiguous special symbols,
if present in the resolved macro.

See also:

-   full list [of supported
    macros](/manual/appendix/macros/supported_by_location)
-   macro [functions](/manual/config/macros/macro_functions)
-   how to configure [user macros](/fr/manual/config/macros/usermacros)

[comment]: # ({/new-4c81df7a})
