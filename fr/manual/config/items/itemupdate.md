[comment]: # translation:outdated

[comment]: # ({new-109e2be5})
# 7 Mass update

[comment]: # ({/new-109e2be5})

[comment]: # ({new-7958bf1e})
#### Overview

Sometimes you may want to change some attribute for a number of items at
once. Instead of opening each individual item for editing, you may use
the mass update function for that.

[comment]: # ({/new-7958bf1e})

[comment]: # ({new-69655416})
#### Using mass update

To mass-update some items, do the following:

-   Mark the checkboxes of the items to update in the list
-   Click on *Mass update* below the list
-   Mark the checkboxes of the attributes to update
-   Enter new values for the attributes and click on *Update*

![](../../../../assets/en/manual/config/item_mass.png)

*Replace applications* will remove the item from any existing
applications and replace those with the one(s) specified in this field.

*Add new or existing applications* allows to specify additional
applications from the existing ones or enter completely new applications
for the items.

Both these fields are auto-complete - starting to type in them offers a
dropdown of matching applications. If the application is new, it also
appears in the dropdown and it is indicated by *(new)* after the string.
Just scroll down to select.

[comment]: # ({/new-69655416})
