[comment]: # translation:outdated

[comment]: # ({new-06812ee1})
# 2 Types d'éléments

[comment]: # ({/new-06812ee1})

[comment]: # ({new-f03e2e9a})
#### Aperçu

Les types d'élément couvrent différentes méthodes d'acquisition de
données à partir de votre système. Chaque type d'élément est livré avec
son propre ensemble de clés d'élément prises en charge et les paramètres
requis.

Les types d'éléments suivants sont actuellement proposés par Zabbix :

-   [Vérifications de l'agent
    Zabbix](/fr/manual/config/items/itemtypes/zabbix_agent)
-   [Vérifications de l'agent
    SNMP](/fr/manual/config/items/itemtypes/snmp)
-   [Traps SNMP](/fr/manual/config/items/itemtypes/snmptrap)
-   [Vérifications IPMI](/fr/manual/config/items/itemtypes/ipmi)
-   [Vérifications
    simple](/fr/manual/config/items/itemtypes/simple_checks)
-   [Supervision
    VMware](/fr/manual/config/items/itemtypes/simple_checks/vmware_keys)
-   [Supervision de fichier de
    log](/fr/manual/config/items/itemtypes/log_items)
-   [Eléments calculés](/fr/manual/config/items/itemtypes/calculated)
-   [Vérifications internes
    Zabbix](/fr/manual/config/items/itemtypes/internal)
-   [Vérifications SSH](/fr/manual/config/items/itemtypes/ssh_checks)
-   [Vérifications
    Telnet](/fr/manual/config/items/itemtypes/telnet_checks)
-   [Vérifications externes](/fr/manual/config/items/itemtypes/external)
-   [Vérifications
    aggrégées](/fr/manual/config/items/itemtypes/aggregate)
-   [Eléments trappeur](/fr/manual/config/items/itemtypes/trapper)
-   [Supervision JMX](/fr/manual/config/items/itemtypes/jmx_monitoring)
-   [Vérifications ODBC](/fr/manual/config/items/itemtypes/odbc_checks)
-   [Eléments
    dépendants](/fr/manual/config/items/itemtypes/dependent_items)

Les détails de tous les types d'éléments sont inclus dans les sous-pages
de cette section. Même si les types d'éléments offrent de nombreuses
options pour la collecte de données, il existe d'autres options via les
[paramètres utilisateurs](/fr/manual/config/items/userparameters) ou les
[modules chargeables](/fr/manual/config/items/loadablemodules).

Certaines vérifications sont effectuées uniquement par le serveur Zabbix
(surveillance sans agent) alors que d'autres nécessitent un agent Zabbix
ou même une passerelle Java Zabbix (surveillance JMX).

::: noteimportant
Si un type d'élément particulier nécessite une
interface particulière (comme une vérification IPMI nécessite une
interface IPMI sur l'hôte), cette interface doit exister dans la
définition de l'hôte. 
:::

Plusieurs interfaces peuvent être définies dans la définition de l'hôte
: agent Zabbix, agent SNMP, JMX et IPMI. Si un élément peut utiliser
plus d'une interface, il recherchera les interfaces hôtes disponibles
(dans l'ordre : Agent→SNMP→JMX →IPMI) pour la première connexion
appropriée.

Tous les éléments qui renvoient du texte (caractère, journaux de log,
information de type texte) peuvent renvoyer uniquement des espaces (le
cas échéant) en définissant la valeur de retour sur une chaîne vide
(prise en charge depuis la version 2.0).

[comment]: # ({/new-f03e2e9a})
