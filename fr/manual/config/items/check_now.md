[comment]: # translation:outdated

[comment]: # ({new-17ac0201})
# 12 Check now

[comment]: # ({/new-17ac0201})

[comment]: # ({new-e417ef3e})
#### Overview

Checking for a new item value in Zabbix is a cyclic process that is
based on configured update intervals. While for many items the update
intervals are quite short, there are others (including low-level
discovery rules) for which the update intervals are quite long, so in
real-life situations there may be a need to check for a new value
quicker - to pick up changes in discoverable resources, for example. To
accommodate such a necessity, it is possible to reschedule a passive
check and retrieve a new value immediately.

This functionality is supported for **passive** checks only. The
following item types are supported:

-   Zabbix agent (passive)
-   SNMPv1/v2/v3 agent
-   IPMI agent
-   Simple check
-   Zabbix internal
-   Zabbix aggregate
-   External check
-   Database monitor
-   JMX agent
-   SSH agent
-   Telnet
-   Calculated
-   HTTP agent

::: noteimportant
The check must be present in configuration cache
in order to get executed; for more information see
[CacheUpdateFrequency](/manual/appendix/config/zabbix_server). Before
executing the check, the configuration cache is **not** updated, thus
very recent changes to item/discovery rule configuration will not be
picked up. Therefore, it is also not possible to check for a new value
for an item/rule that has been created just now.
:::

[comment]: # ({/new-e417ef3e})

[comment]: # ({new-d51c596d})
#### Configuration

To execute a passive check immediately:

-   click on *Check now* in an existing item (or discovery rule)
    configuration form:

![check\_now.png](../../../../assets/en/manual/config/items/check_now.png)

-   click on *Check now* for selected items/rules in the list of
    items/discovery rules:

![](../../../../assets/en/manual/config/items/check_now_list.png)

In the latter case several items/rules can be selected and "checked now"
at once.

[comment]: # ({/new-d51c596d})
