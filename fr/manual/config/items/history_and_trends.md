[comment]: # translation:outdated

[comment]: # ({new-b5fd3df7})
# 3 History and trends

[comment]: # ({/new-b5fd3df7})

[comment]: # ({new-e9319443})
#### Overview

History and trends are the two ways of storing collected data in Zabbix.

Whereas history keeps each collected value, trends keep averaged
information on hourly basis and therefore are less resource-hungry.

[comment]: # ({/new-e9319443})

[comment]: # ({new-e5d848c2})
#### Keeping history

You can set for how many days history will be kept:

-   in the item properties [form](/manual/config/items/item)
-   when mass-updating items
-   when [setting
    up](/fr/manual/web_interface/frontend_sections/administration/general#housekeeper)
    housekeeper tasks

Any older data will be removed by the housekeeper.

The general strong advice is to keep history for the smallest possible
number of days and that way not to overload the database with lots of
historical values.

Instead of keeping a long history, you can keep longer data of trends.
For example, you could keep history for 14 days and trends for 5 years.

You can get a good idea of how much space is required by history versus
trends data by referring to the [database sizing
page](/fr/manual/installation/requirements#database_size).

While keeping shorter history, you will still be able to review older
data in graphs, as graphs will use trend values for displaying older
data.

::: noteimportant
If history is set to '0', the item will update
only inventory. No trigger functions will be evaluated.
:::

::: notetip
As an alternative way to preserve history consider to
use [history
export](/fr/manual/config/items/loadablemodules#providing_history_export_callbacks)
functionality of loadable modules.
:::

[comment]: # ({/new-e5d848c2})

[comment]: # ({new-3d94ff60})
#### Keeping trends

Trends is a built-in historical data reduction mechanism which stores
minimum, maximum, average and the total number of values per every hour
for numeric data types.

You can set for how many days trends will be kept:

-   in the item properties [form](/manual/config/items/item)
-   when mass-updating items
-   when setting up Housekeeper tasks

Trends usually can be kept for much longer than history. Any older data
will be removed by the housekeeper.

::: noteimportant
If trends are set to '0', Zabbix server does not
calculate or store trends at all.
:::

::: noteclassic
The trends are calculated and stored with the same data type
as the original values. As the result the average value calculations of
unsigned data type values are rounded and the less the value interval is
the less precise the result will be. For example if item has values 0
and 1, the average value will be 0, not 0.5.

Also restarting server might result in the precision loss of unsigned
data type average value calculations for the current hour.
:::

[comment]: # ({/new-3d94ff60})
