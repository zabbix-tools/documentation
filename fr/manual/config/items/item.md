[comment]: # translation:outdated

[comment]: # ({new-50342b23})
# 1 Création d'un élément

[comment]: # ({/new-50342b23})

[comment]: # ({new-d3a04061})
#### Aperçu

Pour créer un élément dans l’interface web Zabbix, procédez comme suit :

-   Allez à : *Configuration* → *Hôtes*
-   Cliquez sur *Éléments* dans la rangée de l'hôte
-   Cliquez sur *Créer un élément* dans le coin supérieur droit de
    l'écran
-   Entrez les paramètres de l'élément dans le formulaire

Vous pouvez également créer un élément en ouvrant un élément existant,
en appuyant sur le bouton *Cloner*, puis en enregistrant sous un autre
nom.

[comment]: # ({/new-d3a04061})

[comment]: # ({new-d0a0a311})
#### Configuration

L'onglet **Élément** contient des attributs généraux des éléments :\
![](../../../../assets/en/manual/config/item1.png){width="600"}

All mandatory input fields are marked with a red asterisk.

|Paramètre|Description|
|----------|-----------|
|*Nom*|C'est ainsi que l'objet sera nommé.<br> Les macros suivantes peuvent être utilisées :<br> **$1, $2 ... $9** - se référant au premier, deuxième ... neuvième paramètre de la clé de l'élément<br> Par exemple : Espace disque disponible sur 1$<br> Si la clé de l'élément est "vfs.fs.size\[/,free\]", la description changera automatiquement en "Espace disque libre sur /"|
|*Type*|Type de l'élément. Voir les sections de [type d'élément](itemtypes) individuellement.|
|*Clé*|Clé de l'élément.<br>Les [clés d'élément](itemtypes) prises en charge peuvent être trouvées dans les sections de type d'élément individuellement.<br> La clé doit être unique par hôte.<br> Si le type de clé est 'Agent Zabbix', 'Agent Zabbix (actif)', 'Vérification simple' ou 'Agrégation Zabbix', la valeur de clé doit être prise en charge par l'agent Zabbix ou par le serveur Zabbix.<br> Voir aussi : le [format de clé](/fr/manual/config/items/item/key) correct.|
|*Interface hôte*|Sélectionnez l'interface hôte. Ce champ est disponible lors de la modification d'un élément au niveau de l'hôte.|
|*Type d'information*|Type de données stockées dans la base de données après avoir effectué des conversions, si nécessaire.<br> **Numérique (non signé)** - Entier 64 bits non signé<br> **Numérique (float)** - nombre à virgule flottante<br> Les valeurs négatives peuvent être stockées.<br> Plage autorisée: -999999999999,9999 à 999999999999,9999.<br> À partir de Zabbix 2.2, la réception de valeurs en notation scientifique est également prise en charge. Par exemple 1e+7, 1e-4.<br>** Caractère** - données de texte court<br>** Log** - Données textuelles longues avec des propriétés de journalisation facultatives (horodatage, source, gravité, logeventid)<br> **Texte** - données de texte long.  Voir aussi les [limites de données textuelles](#Text_data_limits).|
|*Unités*|Si un symbole d'unité est défini, Zabbix ajoutera le post-traitement à la valeur reçue et l'affichera avec le suffixe d'unité défini.<br> Par défaut, si la valeur brute dépasse 1000, elle est divisée par 1000 et affichée en conséquence. Par exemple, si vous définissez *bps* et recevez une valeur de 881764, celle-ci s'affiche sous la forme 881,76 Kbps.<br> Un traitement spécial est utilisé pour les unités **B** (octets), **Bps** (octets par seconde), qui sont divisés par 1024. Ainsi, si les unités sont définies sur **B** ou **Bps**, Zabbix affichera :<br> 1 comme 1B/1Bps<br> 1024 comme 1KB/1KBps<br> 1536 comme 1.5KB/1.5KBps<br> Un traitement spécial est utilisé si les unités temporelles suivantes sont utilisées :<br> **unixtime** - traduit par "aaaa.mm.jj hh:mm:ss". Pour traduire correctement, la valeur reçue doit être un type d'information *Numérique (non signé)*.<br> **uptime** - traduit en "hh:mm:ss" ou "N jours, hh:mm:ss"<br> Par exemple, si vous recevez la valeur 881764 (secondes), elle s'affichera comme "10 jours, 04:56:04" ** s** - traduit en "aaa MMM jjj hhh sss ms"; le paramètre est traité en nombre de secondes.<br> Par exemple, si vous recevez la valeur 881764 (secondes), elle s'affichera comme "10j 4h 56m"<br> Seulement 3 unités principales supérieures sont montrées, comme "1m 15d 5h" ou "2h 4m 46s". S'il n'y a pas de jours à afficher, seulement deux niveaux sont affichés - "1m 5h" (pas de minutes, secondes ou millisecondes sont affichées). Et sera traduit en "<1 ms" si la valeur est inférieure à 0.001.<br>*Notez* que si une unité est préfixée avec `!`, alors aucune modification post-traitement ne sera réalisée à l'unité.  Voir aussi la [liste noire des unités](#unit_blacklisting).|
|*Intervalle de mise à jour*|Récupère une nouvelle valeur pour cet élément toutes les N secondes. L'intervalle de mise à jour maximum autorisé est 86400 secondes (1 jour).<br> Les [suffixes temporels](/fr/manual/appendix/suffixes) sont pris en charge, par exemple 30s, 1m, 2h, 1d, depuis Zabbix 3.4.0.  Les<br>[macros utilisateur](/manual/config/macros/user_macros) sont supportées, depuis Zabbix 3.4.0.<br>* Remarque* : S'il est défini sur "0", l'élément ne sera pas interrogé. Toutefois, si un intervalle personnalisé (flexible/planifié) existe également avec une valeur non nulle, l'élément sera interrogé pendant la durée de l'intervalle personnalisé.<br>*Notez* qu'un élément passif existant peu être interrogé immédiatement en poussant le [bouton](#form_buttons) *Vérifier maintenant*.|
|*Intervalles personnalisés*|Vous pouvez créer des règles personnalisées pour vérifier l'élément :<br>** Flexible** - crée une exception à l'*intervalle de mise à jour* (intervalle avec fréquence différente)<br>** Planification** - crée un calendrier d'interrogation personnalisé.<br> Pour plus d'informations, voir les [Intervalles personnalisés](/fr/manual/config/items/item/custom_intervals).<br> Les suffixes temporels sont pris en charge dans le champ *Intervalle*, par exemple 30s, 1m, 2h, 1d.<br> Les [macros utilisateur](/fr/manual/config/macros/usermacros) sont supportées.<br> La planification est prise en charge depuis Zabbix 3.0.0.<br> *Remarque* : Non disponible pour les éléments actifs de l'agent Zabbix.|
|*Période de stockage de l'historique*|Durée de conservation de l'historique détaillé dans la base de données (de 1 heure à 25 ans). Les données plus anciennes seront supprimées par la procédure de nettoyage.\\\\  Stocké en secondes. Les [suffixes temporels](/fr/manual/appendix/suffixes) sont supportés, par exemple 2h, 1d. \\\\ Les [macros utilisateurs](/fr/manual/config/macros/usermacros) sont supportées.\\\\  Cette valeur peut être remplacée globalement dans *Administration → Général → Nettoyage*. Si le paramètre global existe, un message d'avertissement s'affiche : \\\\  ![](/fr/manual/config/override_item.png)<br>Il est recommandé de conserver les valeurs enregistrées le plus petit possible afin de réduire la taille de l'historique des valeurs dans la base de données. Au lieu de conserver un long historique de valeurs, vous pouvez conserver des données plus longues sur les tendances.<br> Voir aussi [Historique et tendances](/fr/manual/config/items/history_and_trends).|
|*Période de stockage des tendances*|Durée de conservation de l'historique agrégé (horaire, min, max, moy, count) dans la base de données (de 1 jour à 25 ans). Les données plus anciennes seront supprimées par la procédure de nettoyage.<br> Stocké en secondes. Les [suffixes temporels](/fr/manual/appendix/suffixes) sont pris en charge, par exemple 24h, 1d.  Les [macros utilisateur](/fr/manual/config/macros/usermacros) sont supportées.<br> Cette valeur peut être remplacée globalement dans *Administration → Général → [Nettoyage](/fr/manual/web_interface/frontend_sections/administration/general#housekeeper).* Si le paramètre global existe, un message d'avertissement s'affiche :<br>![](/fr/manual/config/override_trends.png)<br>  *Remarque* : La conservation des tendances n'est pas disponible pour les données non numériques - caractère, journaux et texte.  Voir aussi [Historique et tendances](/fr/manual/config/items/history_and_trends).|
|*Afficher la valeur*|Appliquez le mappage des valeurs à cet élément. Le mappage des valeurs ne modifie pas les valeurs reçues, mais uniquement pour afficher les données.  \\\\Cela fonctionne avec des éléments entiers seulement.  Par exemple, \\\\"Etats de service Windows".|
|*Format de la date/heure dans les fichiers journaux*|Disponible pour les éléments de type **Log** uniquement. Espaces réservés pris en charge :<br>\* **y**: *Année (1970-2038)*<br>\* **M** : *Mois (01-12)*<br>\* **d** : *Jour (01-31)*<br>\* **h** : *Heure (00-23)*<br>\* **m**: *Minute (00-59)*<br>\* **s**: *Secondes (00-59)*<br> Si elle est vide, l'horodatage ne sera pas analysé.<br> Par exemple, considérez la ligne suivante du fichier de log de l'agent Zabbix :<br> "23480:20100328:154718.045 L'agent Zabbix a démarré. Zabbix 1.8.2 (révision 11211). "  Il commence par six positions de caractères pour le PID, suivies de la date, de l'heure et du reste de la ligne.<br> Le format de l'heure du journal pour cette ligne serait "pppppp:aaaaMMjj:hhmmss".<br> Notez que les caractères "p" et ":" ne sont que des espaces réservés et peuvent être tout sauf "yMdhms".|
|*Nouvelle application*|Entrez le nom d'une nouvelle application pour l'élément.|
|*Applications*|Lier un élément à une ou plusieurs applications existantes.|
|*Peupler le champs d'inventaire de l'hôte*|Vous pouvez sélectionner un champ d'inventaire hôte que la valeur de l'élément va remplir. Cela fonctionnera si le peuplement d'inventaire automatique est activée pour l'hôte.|
|*Description*|Entrez une description de l'élément.|
|*Activer*|Cochez la case pour activer l'élément afin qu'il soit traité.|

::: noteclassic
Lors de la modification d'un élément au niveau d’un
[modèle](/fr/manual/config/templates) existant sur un hôte, un certain
nombre de champs sont en lecture seule. Vous pouvez utiliser le lien
dans l'en-tête du formulaire et accéder au niveau du modèle et les
modifier à cet endroit là, en gardant à l'esprit que les modifications
au niveau du modèle modifieront l'élément pour tous les hôtes auxquels
le modèle est lié.
:::

[comment]: # ({/new-d0a0a311})

[comment]: # ({new-4f9077b8})
#### Pré-traitement de la valeur de l'élément

L'onglet **Pré-traitement** permet de définir des règles de
transformation pour les valeurs reçues. Une ou plusieurs transformations
sont possibles avant d'enregistrer des valeurs dans la base de données.
Les transformations sont exécutées dans l'ordre dans lequel elles sont
définies. Tout le pré-traitement est effectué par le serveur Zabbix.
Voir aussi : [Détails du
pré-traitement](/fr/manual/appendix/items/preprocessing)

![](../../../../assets/en/manual/config/item2.png)

Les [macros utilisateur](/fr/manual/config/macros/usermacros) et les
macros utilisateur avec contexte sont supportées dans les paramètres de
pré-traitement des valeurs d'élément.

|Transformation|Description|
|--------------|-----------|
|*Expression régulière*|Faites correspondre la valeur à l'expression régulière <pattern> et remplacez la valeur par <sortie>. L'expression régulière prend en charge l'extraction de maximum 10 groupes capturés avec la séquence \\N. Si la valeur d'entrée ne correspond pas, l'élément n'est pas pris en charge.\\\\ Paramètres :<br> **pattern** - expression régulière<br>** sortie** - modèle du format de sortie. Une séquence d'échappement \\N (où N=1...9) est remplacée par le Nième groupe correspondant. Une séquence d'échappement \\0 est remplacée par le texte correspondant.<br> Pris en charge depuis la version 3.4.0.<br> Veuillez vous référer à la section des [expressions régulières](/fr/manual/regular_expressions#example) pour quelques exemples.|
|*Réduire*|Supprime les caractères spécifiés au début et à la fin de la valeur.|
|*Réduire à droite*|Supprime les caractères spécifiés à la fin de la valeur.|
|*Réduire à gauche*|Supprime les caractères spécifiés au début de la valeur.|
|*XML XPath*|Extraire une valeur ou un fragment à partir des données XML à l'aide de la fonctionnalité XPath.<br>Pour que cette option fonctionne, le serveur Zabbix doit être compilé avec la prise en charge de libxml.<br>Exemples:<br>`number(/document/item/value)` va extraire `10` de `<document><item><value>10</value></item></document>`<br>`number(/document/item/@attribute)` va extraire `10` de `<document><item attribute="10"></item></document>`<br>`/document/item` va extraire `<item><value>10</value></item>` de `<document><item><value>10</value></item></document>`<br>Notez que les espaces de noms ne sont pas supportés.<br>Pris en charge depuis 3.4.0.|
|*Chemin JSON*|Extraire une valeur ou un fragment à partir de données JSON en utilisant un sous-ensemble simple de fonctionnalités JSONPath.<br>Exemples:<br>`$.document.item.value` va extraire `10` de `{"document":{"item":{"value": 10}}}`<br>`$.document.item` va extraire `{"value": 10}` de `{"document":{"item":{"value": 10}}}`<br>`$['a document'].item.value` va extraire `10` de `{"a document":{"item":{"value": 10}}}`<br>`$.document.items[1].value` va extraire `20` de `{"document":{"items":[{"value": 10}, {"value": 20}]}}`<br>Notez que seuls les chemins directs vers les objets uniques en notation avec des points ou des crochets sont pris en charge.<br>Seuls les caractères alphanumériques + soulignés peuvent être utilisés dans la notation par points JSONPath `($.a.b.c)`. Si le nom de l'objet JSON contient d'autres caractères, alors la notation avec des parenthèses doit être utilisée `($['a']['b']['c'])`. Les deux notation peuvent être mélangées `($.a['b'].c)`<br>L'extraction de plusieurs valeurs n'est pas supportée.<br>Pris en charge depuis since 3.4.0.|
|*Multiplicateur personnalisé*|Multiplie la valeur par l'entier ou la valeur à virgule flottante spécifiée.<br>Utilisez cette option pour convertir les valeurs reçues en KB, MBps, etc. en B,Bps. Sinon, Zabbix ne peut pas définir correctement les[prefixes](/fr/manual/appendix/suffixes) (K, M, G etc).<br>A partir de Zabbix 2.2, l'utilisation de la notation scientifique est également supportée. Exemple : 1e+70.|
|*Changement simple*|Calcule la différence entre la valeur actuelle et la valeur précédente.<br>Evalué en tant que **valeur**-**prev\_valeur**, où<br>*value* - value actuelle ; *prev\_valeur* - valeur reçue précédemment<br>Une seule opération de modification par élément est autorisé.|
|*Changement par seconde*|Calcule la variation de la valeur (différence entre la valeur actuelle et la précédente valeur) par seconde.<br>Evalué comme (**valeur**-**prev\_valeur**)/(**time**-**prev\_time**), où<br>*valeur* - valeur actuelle ; *prev\_valeur* - valeur précédente reçue ; *temps* - heure actuelle (timestamp); *prev\_temps* - heure de la précédente valeur (timestamp).<br>Ce paramètre est extrêmement utile pour obtenir la vitesse par seconde pour une valeur en constante augmentation. Si la valeur actuelle est inférieure à la valeur précédente, Zabbix supprime cette différence (n'enregistre rien) et attend une autre valeur. Cela permet de fonctionner correctement avec, par exemple, un wrapping (débordement) de compteurs SNMP 32 bits.<br>*Remarque*: Comme ce calcul peut produire des nombres à virgule flottante, il est recommandé de définir le 'Type d'information' à *Numérique (flottant)*, même si les valeurs brutes entrantes sont des entiers. Ceci est particulièrement pertinent pour les petits nombres où la partie décimale compte. Si les valeurs en virgule flottante sont grandes et peuvent dépasser la longueur du champ 'flottant', auquel cas la valeur entière peut être perdue, il est réellement suggéré d'utiliser *Numérique (non signé)* et de réduire ainsi seulement la partie décimale<br>Une seule opération de modification par élément est autorisée.|
|*Booléen à décimal*|Convertie la valeur du format booléen à décimal. La représentation textuelle est traduite en 0 ou 1. Ainsi, 'VRAI' est stocké en tant que 1 et 'FAUX' est stocké en tant que 0. Toutes les valeurs sont comparées de manière insensible à la casse. Les valeurs actuellement reconnues sont, pour :<br> *VRAI* - vrai, t, oui, y, ok, activé, up, en cours d'exécution, disponible, maître<br>* FAUX* - faux, f, non, n, err, désactivé, off, inutilisé, désactivé, indisponible, esclave<br> De plus, toute valeur numérique non nulle est considérée comme VRAIE et zéro est considéré comme FAUX.<br>Les valeurs suivantes sont supportées depuis la version 4.0.0: ok, master, err, slave.|
|*Octal à décimal*|Convertie la valeur au format octal en décimal.|
|*Hexadécimal à décimal*|Convertie la valeur au format hexadécimal en décimal.<br>Notez que les valeurs contenant un espace ne sont pas supportées en format numérique. En guise de solution de contournement, vous pouvez utiliser une étape de pré-traitement d'expression régulière en supprimant l'espace avant l'étape hexadécimal vers décimal.|

::: notetip
Si vous utilisez un multiplicateur personnalisé ou une
valeur de stockage dans *Modification par seconde* pour les éléments
ayant le type d'informations défini sur *Numérique (non signé)* et que
la valeur calculée résultante est réellement un nombre flottant, la
valeur calculée est toujours acceptée comme correcte, en tronquant la
partie décimale et en stockant la valeur en entier
:::

[comment]: # ({/new-4f9077b8})

[comment]: # ({new-006c45ea})
##### Boutons de formulaire

Les boutons en bas des formulaires permettent de réaliser plusieurs
opérations.

|   |   |
|---|---|
|![](../../../../assets/en/manual/config/button_add.png)|Ajoute un élément. Ce bouton est disponible uniquement pour les nouveaux éléments.|
|![](../../../../assets/en/manual/config/button_update.png)|Mets à jour les propriétés d'un élément.|
|![](../../../../assets/en/manual/config/button_clone.png)|Créé un autre élément basé sur les propriétés de l'élément actuel.|
|![](../../../../assets/en/manual/config/button_check_now.png)|Exécute une vérification pour un nouvel élément immédiatement. Supporté pour les vérifications **passives** uniquement (voir [plus de détails](/fr/manual/config/items/check_now)).<br>*Notez* que quand une valeur est vérifiée immédiatement, le cache de configuration n'est pas mis à jour, ainsi la valeur ne reflétera pas les modifications très récentes apportées à la configuration des éléments.|
|![](../../../../assets/en/manual/config/button_clear_history.png)|Supprime l'historique et les tendances de l'élément.|
|![](../../../../assets/en/manual/config/button_delete.png)|Supprime l'élément.|
|![](../../../../assets/en/manual/config/button_cancel.png)|Annule l'édition des propriétés de l'élément.|

[comment]: # ({/new-006c45ea})

[comment]: # ({new-4b126a62})
#### Limites de données de texte

Les limites de données de texte dépendent du moteur de base de données.
Avant de stocker les valeurs de texte dans la base de données, elles
sont tronquées pour correspondre à la limite du type de valeur de base
de données :

|Base de données|Type d'information|<|<|
|----------------|------------------|-|-|
|^|Caractère|Log|Texte|
|MySQL|255 caractères|65536 octets|65536 octets|
|PostgreSQL|255 caractères|65536 caractères|65536 caractères|
|Oracle|255 caractères|65536 caractères|65536 caractères|
|IBM DB2|255 octets|2048 octets|2048 octets|

[comment]: # ({/new-4b126a62})

[comment]: # ({new-8cb32bf5})
#### Liste noire des unités

Par défaut, la spécification d'une unité pour un élément entraîne
l'ajout d'un préfixe multiplicateur. Par exemple, une valeur entrante
'2048' avec l'unité 'B' serait affichée sous la forme '2KB'.

Toute unité, cependant, peut être empêchée d'être convertie en utilisant
un préfixe `!`, Par exemple ''!B ''. Pour mieux illustrer le
fonctionnement de la conversion avec et sans la liste noire, voir les
exemples de valeurs et d’unités suivants :

    1024 !B → 1024 B
    1024 B → 1 KB
    61 !s → 61 s
    61 s → 1m 1s
    0 !uptime → 0 uptime
    0 uptime → 00:00:00
    0 !! → 0 !
    0 ! → 0

::: noteclassic
 Avant Zabbix 4.0, il y avait une liste noire d'unité codée
en dur comprenant `ms`, `rpm`, `RPM`, `%`. Cette liste noire est devenue
obsolète, ainsi la méthode correcte de mise en liste noire de ces unités
est `!ms`, `!rpm`, `!RPM`, `!%`.
:::

[comment]: # ({/new-8cb32bf5})

[comment]: # ({new-aeb2b058})
#### Éléments non supportés

Un élément peut ne plus être pris en charge si sa valeur ne peut pas
être récupérée pour une raison quelconque. Ces éléments sont toujours
revérifiés à un intervalle fixe, configurable dans la [section
Administration](/fr/manual/web_interface/frontend_sections/administration/general?&#other_parameters).

[comment]: # ({/new-aeb2b058})

[comment]: # ({new-d4406a73})
#### Custom script limit

Available custom script length depends on the database used:

|   |   |   |
|---|---|---|
|*Database*|*Limit in characters*|*Limit in bytes*|
|**MySQL**|65535|65535|
|**Oracle Database**|2048|4000|
|**PostgreSQL**|65535|not limited|
|**SQLite (only Zabbix proxy)**|65535|not limited|

[comment]: # ({/new-d4406a73})

[comment]: # ({new-2cd40177})
#### Unsupported items

An item can become unsupported if its value cannot be retrieved for some
reason. Such items are still rechecked at their standard *[Update
interval](/manual/config/items/item?#configuration)*.

Unsupported items are reported as having a NOT SUPPORTED state.

[comment]: # ({/new-2cd40177})
