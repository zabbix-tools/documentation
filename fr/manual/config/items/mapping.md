[comment]: # translation:outdated

[comment]: # ({new-e8992262})
# 8 Value mapping

[comment]: # ({/new-e8992262})

[comment]: # ({new-2d2da55e})
#### Overview

For a more "human" representation of received values, you can use value
maps that contain the mapping between numeric values and string
representations.

Value mappings can be used in both the Zabbix frontend and notifications
sent by email/SMS/jabber etc.

For example, an item which has value '0' or '1' can use value mapping to
represent the values in a human-readable form:

-   '0' => 'Not Available'
-   '1' => 'Available'

Or, a backup related value map could be:

-   'F' → 'Full'
-   'D' → 'Differential'
-   'I' → 'Incremental'

Thus, when [configuring items](item) you can use a value map to
"humanize" the way an item value will be displayed. To do that, you
refer to the name of a previously defined value map in the *Show value*
field.

::: noteclassic
Value mapping can be used with items having *Numeric
(unsigned)*, *Numeric (float)* and *Character* type of
information.
:::

Value mappings, starting with Zabbix 3.0, can be exported/imported,
either separately, or with the respective template or host.

[comment]: # ({/new-2d2da55e})

[comment]: # ({new-18d7ef55})
#### Configuration

To define a value map:

-   Go to: *Administration → General*
-   Select *Value mapping* from the dropdown
-   Click on *Create value map* (or on the name of an existing map)

![](../../../../assets/en/manual/config/items/value_map.png)

Parameters of a value map:

|Parameter|Description|
|---------|-----------|
|*Name*|Unique name of a set of value mappings.|
|*Mappings*|Individual mappings - pairs of numeric values and their string representations.|

All mandatory input fields are marked with a red asterisk.

To add a new individual mapping, click on *Add*.

[comment]: # ({/new-18d7ef55})

[comment]: # ({new-6241051d})
#### How this works

For example, one of the predefined agent items 'Ping to the server
(TCP)' uses an existing value map called 'Service state' to display its
values.

![](../../../../assets/en/manual/config/items/value_map2.png)

In the item [configuration form](item) you can see a reference to this
value map in the *Show value* field:

![](../../../../assets/en/manual/config/items/show_value.png)

So in *Monitoring → Latest data* the mapping is put to use to display
'Up' (with the raw value in parentheses).

![](../../../../assets/en/manual/config/items/value_display.png){width="600"}

In the *Latest data* section displayed values are shortened to 20
symbols. If value mapping is used, this shortening is not applied to the
mapped value, but only to the raw value separately (displayed in
parenthesis).

::: notetip
A value being displayed in a human-readable form is also
easier to understand when receiving notifications.
:::

Without a predefined value map you would only get this:

![](../../../../assets/en/manual/config/items/value_display2.png){width="600"}

So in this case you would either have to guess what the '1' stands for
or do a search of documentation to find out.

[comment]: # ({/new-6241051d})
