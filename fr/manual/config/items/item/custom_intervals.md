[comment]: # translation:outdated

[comment]: # ({new-ffc3ea50})
# 2 Custom intervals

[comment]: # ({/new-ffc3ea50})

[comment]: # ({new-2c00d21c})
#### Aperçu

Il est possible de créer des règles personnalisées concernant le temps
où un élément est vérifié. Les deux méthodes pour cela sont les
*intervalles flexibles*, qui permettent de redéfinir l'intervalle de
mise à jour par défaut, et la *planification*, grâce à laquelle une
vérification d'élément peut être exécutée à un moment ou une séquence de
temps spécifique.

[comment]: # ({/new-2c00d21c})

[comment]: # ({new-e2fa2aee})

::: noteclassic
Zabbix agent 2 supports custom intervals for both passive and active checks, whereas Zabbix agent supports custom intervals only for passive checks. See  [Zabbix agent vs agent 2 comparison](/manual/appendix/agent_comparison). 
:::

[comment]: # ({/new-e2fa2aee})

[comment]: # ({new-41315093})
#### Intervalles flexibles

Les intervalles flexibles permettent de redéfinir l'intervalle de mise à
jour par défaut pour des périodes spécifiques. Un intervalle flexible
est défini avec *Intervalle* et *Période* où :

-   *Intervalle* - l'intervalle d’actualisation pour la période
    spécifiée
-   *Période* - la période pendant laquelle l'intervalle flexible est
    actif (voir les [périodes](/fr/manual/appendix/time_period) pour la
    description détaillée du format de la *Période*)

Jusqu'à sept intervalles flexibles peuvent être définis. Si plusieurs
intervalles flexibles se chevauchent, la plus petite valeur
d'*intervalle* est utilisée pour la période de chevauchement. Notez que
si la plus petite valeur où les intervalles se chevauchant est '0',
aucune interrogation n'aura lieu. En dehors des intervalles flexibles,
l'intervalle d’actualisation par défaut est utilisé.

Notez que si l'intervalle flexible est égal à la longueur de la période,
l'élément sera vérifié une seule fois. Si l'intervalle flexible est
supérieur à la période, l'élément peut être vérifié une fois ou il peut
ne pas être vérifié du tout (une telle configuration n'est donc pas
conseillée). Si l'intervalle flexible est inférieur à la période,
l'élément sera vérifié au moins une fois.

Si l'intervalle flexible est défini sur '0', l'élément n'est pas
interrogé pendant la période d'intervalle flexible et reprend
l'interrogation en fonction de l'*intervalle d’actualisation* par défaut
une fois la période terminée.

Exemples :

|Intervalle|Période|Description|
|----------|--------|-----------|
|10|1-5,09:00-18:00|L'élément sera vérifié toutes les 10 secondes pendant les heures de travail.|
|0|1-7,00:00-7:00|L'élément ne sera pas vérifié pendant la nuit.|
|0|7-7,00:00-24:00|L'élément ne sera pas vérifié le dimanche.|
|60|1-7,12:00-12:01|L'élément sera vérifié à 12h00 tous les jours. Notez que cela a été utilisé comme une solution de contournement pour les contrôles planifiés et à partir de Zabbix 3.0, il est recommandé d'utiliser des intervalles de planification pour ces vérifications.|

[comment]: # ({/new-41315093})

[comment]: # ({new-128c5833})
#### Intervalles de planification

Les intervalles de planification sont utilisés pour vérifier les
éléments à des moments précis. Alors que les intervalles flexibles sont
conçus pour redéfinir l'intervalle d’actualisation par défaut des
éléments, les intervalles de planification sont utilisés pour spécifier
un calendrier de contrôle indépendant, qui est exécuté en parallèle.

Un intervalle de planification est défini comme suit :
`md<filter>wd<filter>h<filter>m<filter>s<filter>` où :

-   **md** - jours du mois
-   **wd** - jours de la semaine
-   **h** - heures
-   **m** - minutes
-   **s** – secondes

`<filter>` est utilisé pour spécifier les valeurs du préfixe (jours,
heures, minutes, secondes) et est défini comme :
`[<from>[-<to>]][/<step>][,<filter>]` où :

-   `<from>` et `<to>` définissent la plage de valeurs correspondantes
    (incluses). Si `<to>` est omis alors le filtre correspond à une
    plage `<from> - <from>`. Si `<from>` est également omis, le filtre
    correspond à toutes les valeurs possibles.
-   `<step>` définit les sauts de la valeur numérique dans la plage. Par
    défaut, `<step>` a la valeur 1, ce qui signifie que toutes les
    valeurs de la plage définie correspondent.

Alors que les définitions de filtre sont facultatives, au moins un
filtre doit être utilisé. Un filtre doit avoir une plage ou la valeur
*<step>* définie.

Un filtre vide correspond à '0' si aucun filtre de niveau inférieur
n'est défini ou à toutes les valeurs possibles dans le cas contraire.
Par exemple, si le filtre d'heure est omis, seule l'heure '0'
correspondra, à condition que les filtres minutes et secondes soient
également omis, sinon un filtre d'heures vide correspondra à toutes les
valeurs d'heure.

Les valeurs `<from>` et `<to>` valides pour leur préfixe de filtre
respectif sont :

|Préfixe|Description|*<from>*|*<to>*|
|--------|-----------|--------------|------------|
|md|Jour du mois|1-31|1-31|
|wd|Jour de la semaine|1-7|1-7|
|h|Heures|0-23|0-23|
|m|Minutes|0-59|0-59|
|s|Secondes|0-59|0-59|

La valeur `<from>` doit être inférieure ou égale à la valeur `<to>`. La
valeur `<step>` doit être supérieure ou égale à 1 et inférieure ou égale
à `<to>` - `<from>`.

La valeur des jours, des heures, des minutes et des secondes à un seul
chiffre peuvent être préfixées avec 0. Par exemple, `md01-31` et `h/02`
sont des intervalles valides, mais `md01-031` et `wd01-07` ne le sont
pas.

Dans l'interface Zabbix, plusieurs intervalles de planification sont
entrés dans des lignes séparées. Dans l'API Zabbix, ils sont concaténés
en une seule chaîne avec un point-virgule ; en tant que séparateur.

Si un instant correspond à plusieurs intervalles, il n'est exécuté
qu'une seule fois. Par exemple, `wd1h9;h9` ne sera exécuté qu'une seule
fois le lundi à 9h.

Exemples :

|Intervalle|Description|
|----------|-----------|
|m0-59|exécuté chaque minute|
|h9-17/2|exécuté toutes les 2 heures en commençant à 9:00 (9:00, 11:00 ...)|
|m0,30 or m/30|exécuté toutes les heures à hh:00 et hh:30|
|m0,5,10,15,20,25,30,35,40,45,50,55 or m/5|toutes les 5 minutes|
|wd1-5h9|du lundi au vendredi à 9:00|
|wd1-5h9-18|du lundi au vendredi à 9:00,10:00,...,18:00|
|h9,10,11 or h9-11|chaque jour à 9:00, 10:00 et 11:00|
|md1h9m30|chaque premier jour de chaque mois à 9:30|
|md1wd1h9m30|chaque premier jour de chaque mois à 9:30 si c'est un lundi|
|h9m/30|exécuté à 9:00, 9:30|
|h9m0-59/30|exécuté à 9:00, 9:30|
|h9,10m/30|exécuté à 9:00, 9:30, 10:00, 10:30|
|h9-10m30|exécuté à 9:30, 10:30|
|h9m10-40/30|exécuté à 9:10, 9:40|
|h9,10m10-40/30|exécuté à 9:10, 9:40, 10:10, 10:40|
|h9-10m10-40/30|exécuté à 9:10, 9:40, 10:10, 10:40|
|h9m10-40|exécuté à 9:10, 9:11, 9:12, ... 9:40|
|h9m10-40/1|exécuté à 9:10, 9:11, 9:12, ... 9:40|
|h9-12,15|exécuté à 9:00, 10:00, 11:00, 12:00, 15:00|
|h9-12,15m0|exécuté à 9:00, 10:00, 11:00, 12:00, 15:00|
|h9-12,15m0s30|exécuté à 9:00:30, 10:00:30, 11:00:30, 12:00:30, 15:00:30|
|h9-12s30|exécuté à 9:00:30, 9:01:30, 9:02:30 ... 12:58:30, 12:59:30|
|h9m/30;h10|exécuté à 9:00, 9:30, 10:00|

[comment]: # ({/new-128c5833})
