[comment]: # translation:outdated

[comment]: # ({new-856c911b})
# 1 Format de clé d’élément

Le format de la clé d’élément, y compris les paramètres clés, doit
suivre les règles de syntaxe. Les illustrations suivantes illustrent la
syntaxe prise en charge. Les éléments et caractères autorisés à chaque
point peuvent être déterminés en suivant les flèches - si un bloc peut
être atteint par la ligne, il est autorisé, sinon - il n'est pas
autorisé.

![](../../../../../assets/en/manual/config/item_key_2.png){width="600"}

Pour construire une clé d'élément valide, on commence par spécifier le
nom de la clé, puis on a le choix d'avoir des paramètres ou non - comme
l'illustrent les deux lignes qui peuvent être suivies.

[comment]: # ({/new-856c911b})

[comment]: # ({new-63ccf8e4})
#### Nom de la clé

Le nom de la clé elle-même a une gamme limitée de caractères autorisés,
qui se suivent les uns les autres. Les caractères autorisés sont :

    0-9a-zA-Z_-.

Ce qui signifie:

-   tous les nombres ;
-   toutes les lettres minuscules ;
-   toutes les lettres majuscules ;
-   underscore ;
-   tiret ;
-   point.

![](../../../../../assets/en/manual/config/key_name.png)

[comment]: # ({/new-63ccf8e4})

[comment]: # ({new-af5ae7f1})
#### Paramètres des clés

Une clé d'élément peut avoir plusieurs paramètres séparés par des
virgules.

![](../../../../../assets/en/manual/config/key_parameters.png)

Chaque paramètre de clé peut être une chaîne entre guillemets, une
chaîne sans guillemets ou un tableau.

![](../../../../../assets/en/manual/config/item_parameter.png)

Le paramètre peut également être laissé vide, utilisant ainsi la valeur
par défaut. Dans ce cas, le nombre approprié de virgules doit être
ajouté si d'autres paramètres sont spécifiés. Par exemple, la clé
d'élément **icmpping\[,,200,,500\]** spécifie que l'intervalle entre les
pings individuels est de 200 millisecondes, avec un timeout de 500
millisecondes, et tous les autres paramètres sont laissés à leurs
valeurs par défaut.

[comment]: # ({/new-af5ae7f1})

[comment]: # ({new-b4a70ec9})
#### Paramètre - chaîne entre guillemets

Si le paramètre clé est une chaîne entre guillemets, tout caractère
Unicode est autorisé et les guillemets doubles inclus doivent être
protégés par un backslash.

![](../../../../../assets/en/manual/config/key_param_quoted_string.png)

::: notewarning
Pour mettre entre guillemets les paramètres clés
d'un élément, utilisez uniquement des guillemets doubles. Les guillemets
simples ne sont pas supportés.
:::

[comment]: # ({/new-b4a70ec9})

[comment]: # ({new-3376836e})
#### Paramètre - chaîne sans guillemets

Si le paramètre clé est une chaîne sans guillemets, tout caractère
Unicode est autorisé sauf la virgule et le crochet de droite (\]). Le
paramètre sans guillemet ne peut pas commencer avec le crochet carré
gauche (\[).

![](../../../../../assets/en/manual/config/key_param_unquoted_string.png)

[comment]: # ({/new-3376836e})

[comment]: # ({new-5289a973})
#### Paramètre - tableau

Si le paramètre de clé est un tableau, il est à nouveau placé entre
crochets, où les paramètres individuels sont alignés sur les règles et
la syntaxe de spécification de plusieurs paramètres.

![](../../../../../assets/en/manual/config/key_param_array.png)

::: noteimportant
Les tableaux de paramètres multi-niveaux, comme
`[a,[b,[c,d]],e]`, ne sont pas autorisés.
:::

[comment]: # ({/new-5289a973})
