[comment]: # translation:outdated

[comment]: # ({new-5e482050})
# 13 Trapper items

[comment]: # ({/new-5e482050})

[comment]: # ({new-03e7aedb})
#### Overview

Trapper items accept incoming data instead of querying for it.

It is useful for any data you might want to "push" into Zabbix.

To use a trapper item you must:

-   have a trapper item set up in Zabbix
-   send in the data into Zabbix

[comment]: # ({/new-03e7aedb})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-62c22fbb})
##### Item configuration

To configure a trapper item:

-   Go to: *Configuration* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/trapper_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for trapper items are:

|   |   |
|---|---|
|*Type*|Select **Zabbix trapper** here.|
|*Key*|Enter a key that will be used to recognize the item when sending in data.|
|*Type of information*|Select the type of information that will correspond the format of data that will be sent in.|
|*Allowed hosts*|List of comma delimited IP addresses, optionally in CIDR notation, or hostnames.<br>If specified, incoming connections will be accepted only from the hosts listed here.<br>If IPv6 support is enabled then '127.0.0.1', '::127.0.0.1', '::ffff:127.0.0.1' are treated equally and '::/0' will allow any IPv4 or IPv6 address.<br>'0.0.0.0/0' can be used to allow any IPv4 address.<br>Note, that "IPv4-compatible IPv6 addresses" (0000::/96 prefix) are supported but deprecated by [RFC4291](https://tools.ietf.org/html/rfc4291#section-2.5.5).<br>Example: Server=127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.domain<br>Spaces and [user macros](/fr/manual/config/macros/usermacros) are allowed in this field since Zabbix 2.2.0.|

::: noteclassic
You may have to wait up to 60 seconds after saving the item
until the server picks up the changes from a configuration cache update,
before you can send in values.
:::

[comment]: # ({/new-62c22fbb})

[comment]: # ({new-5f246209})
##### Sending in data

In the simplest of cases, we may use
[zabbix\_sender](/fr/manual/concepts/sender) utility to send in some
'test value':

    zabbix_sender -z <server IP address> -p 10051 -s "New host" -k trap -o "test value"

To send in the value we use these keys:

*-z* - to specify Zabbix server IP address

*-p* - to specify Zabbix server port number (10051 by default)

*-s* - to specify the host (make sure to use the 'technical' [host
name](/fr/manual/config/hosts/host#configuration) here, instead of the
'visible' name)

*-k* - to specify the key of the item we just defined

*-o* - to specify the actual value to send

::: noteimportant
Zabbix trapper process does not expand macros used
in the item key in attempt to check corresponding item key existence for
targeted host.
:::

[comment]: # ({/new-5f246209})

[comment]: # ({new-1e9d760a})
##### Display

This is the result in *Monitoring → Latest data*:

![](../../../../../assets/en/manual/config/items/itemtypes/trapped_data.png){width="600"}

##### Timestamps

If values are sent using `zabbix_sender` from a file with timestamps,
then these timestamps will be adjusted to match server time. For
instance, if an item's timestamp is "10:30:50", the current time on
`zabbix_sender`'s machine is "10:40:03", and the current time on Zabbix
server's machine is "10:40:05", then the item's value will be stored in
the database with a timestamp of "10:30:52".

Similarly, if a value is first sent to Zabbix proxy, which later sends
it to Zabbix server, the timestamp will be first adjusted to match
Zabbix proxy time, and then it will be adjusted to match Zabbix server
time.

[comment]: # ({/new-1e9d760a})
