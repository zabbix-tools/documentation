[comment]: # translation:outdated

[comment]: # ({new-b8c65ad1})
# 7 Eléments calculés

[comment]: # ({/new-b8c65ad1})

[comment]: # ({new-43ebe58c})
#### Aperçu

Avec les éléments calculés, vous pouvez créer des calculs sur la base
d'autres éléments.

Ainsi, les éléments calculés sont un moyen de créer des sources de
données virtuelles. Les valeurs seront calculées périodiquement en
fonction d'une expression arithmétique. Tous les calculs sont effectués
par le serveur Zabbix - aucun élément lié aux éléments calculés n'est
effectué sur les agents ou proxys Zabbix.

Les données résultantes seront stockées dans la base de données Zabbix
comme pour tout autre élément - cela signifie stocker à la fois
l'historique et les valeurs des tendances pour la génération rapide de
graphiques. Les éléments calculés peuvent être utilisés dans les
expressions de déclencheur, référencés par des macros ou d'autres
entités identiques à tout autre type d'élément.

Pour utiliser les éléments calculés, choisissez le type d'élément
**Calculé**.

[comment]: # ({/new-43ebe58c})

[comment]: # ({new-ddc35784})
#### Champs configurables

La **clé** est un identifiant d'élément unique (par hôte). Vous pouvez
créer un nom de clé à l'aide de symboles pris en charge.

La définition de calcul doit être entrée dans le champ **Formule**. Il
n'y a pratiquement aucun lien entre la formule et la clé. Les paramètres
clés ne sont en aucun cas utilisés dans la formule.

La syntaxe correcte d'une formule simple est :

    func(<key>|<hostname:key>,<parameter1>,<parameter2>,...)

Où :

|ARGUMENT|DEFINITION|
|--------|----------|
|**func**|Une des [fonctions](/fr/manual/appendix/triggers/functions) supportées dans les expressions de déclenchement : last, min, max, avg, count, etc|
|**clé**|La clé d'un autre élément dont vous voulez utiliser les données. Il peut être défini comme **clé** ou **nom d'hôte:clé**.<br>*Remarque :* Il est fortement recommandé de mettre toute la clé entre guillemets ("...") pour éviter une analyse incorrecte à cause des espaces ou des virgules dans la clé.<br>S'il existe également des paramètres entre guillemets dans la clé, ces guillemets doivent être échappés en utilisant la barre oblique inverse (\\). Voir **l'exemple 5** ci-dessous.|
|**paramètre(s)**|Paramètre(s) de fonction, si nécessaire..|

::: notetip
Tous les éléments référencés à partir de la formule
d'éléments calculée doivent exister et être en train de collecter des
données (exceptions dans [les fonctions et les éléments non
supportés](/fr/manual/appendix/triggers/functions#functions_and_unsupported_items)).
En outre, si vous modifiez la clé d'un élément référencé, vous devez
mettre à jour manuellement les formules à l'aide de cette
clé.
:::

::: noteimportant
[Les macros
utilisateur](/fr/manual/config/macros/usermacros) dans la formule seront
développées si elles sont utilisées pour référencer un paramètre de
fonction ou une constante. Les macros utilisateur NE seront PAS
développées si vous référencez une fonction, un nom d'hôte, une clé
d'élément, un paramètre de clé d'élément ou un opérateur.
:::

Une formule plus complexe peut utiliser une combinaison de fonctions,
d'opérateurs et de crochets. Vous pouvez utiliser toutes les fonctions
et tous les
[opérateurs](/fr/manual/config/triggers/expression#operators) pris en
charge dans les expressions de déclencheur. Notez que la syntaxe est
légèrement différente, mais la logique et la priorité des opérateurs
sont exactement les mêmes.

Contrairement aux expressions de déclencheur, Zabbix traite les éléments
calculés en fonction de l'intervalle d’actualisation de l'élément, et
non lors de la réception d'une nouvelle valeur.

::: noteclassic
Si le résultat du calcul est une valeur flottante, il sera
réduit à un entier si le type d'information calculé est *Numérique (non
signé)*.
:::

Un élément calculé peut ne plus être pris en charge dans plusieurs cas :

1.  élément(s) référencé(s)
    -   n'est pas trouvé
    -   est désactivé
    -   appartient à un hôte désactivé
    -   n'est pas supporté (voir les exceptions dans [les fonctions et
        les éléments non
        supportés](/fr/manual/appendix/triggers/functions#functions_and_unsupported_items),
        [les expressions avec des éléments non supportés et les valeurs
        inconnues](/fr/manual/config/triggers/expression#expressions_with_unsupported_items_and_unknown_values)
        et [les
        opérateurs](/fr/manual/config/triggers/expression#operators))
2.  pas de données pour calculer une fonction
3.  division par zero
4.  utilisation d'une syntaxe incorrecte

La prise en charge des éléments calculés a été introduite dans Zabbix
1.8.1.\
À partir de Zabbix 3.2, les éléments calculés dans certains cas peuvent
impliquer des éléments non supportés comme décrit dans [les fonctions et
les éléments non
supportés](/fr/manual/appendix/triggers/functions#functions_and_unsupported_items),
[les expressions avec des éléments non supportés et les valeurs
inconnues](/fr/manual/config/triggers/expression#expressions_with_unsupported_items_and_unknown_values)
et [les opérateurs](/fr/manual/config/triggers/expression#operators)).

[comment]: # ({/new-ddc35784})

[comment]: # ({new-82c22f4f})
#### Exemples d'utilisation

[comment]: # ({/new-82c22f4f})

[comment]: # ({new-426ab71a})
##### Exemple 1

Calcul du pourcentage d'espace disque libre sur '/'.

Utilisation de la fonction **last** :

    100*last("vfs.fs.size[/,free]")/last("vfs.fs.size[/,total]")

Zabbix prendra les dernières valeurs pour les espaces disque libres et
totaux et calculera le pourcentage en fonction de la formule donnée.

[comment]: # ({/new-426ab71a})

[comment]: # ({new-77bcdfd9})
##### Exemple 2

Calcul d'une moyenne de 10 minutes du nombre de valeurs traitées par
Zabbix.

Utilisation de la fonction **avg** :

    avg("Zabbix Server:zabbix[wcache,values]",600)

Notez que l'utilisation intensive d'éléments calculés avec de longues
périodes de temps peut affecter les performances du serveur Zabbix.

[comment]: # ({/new-77bcdfd9})

[comment]: # ({new-5d068733})
##### Exemple 3

Calcul de la bande passante totale sur eth0.

Somme de deux fonctions :

    last("net.if.in[eth0,bytes]")+last("net.if.out[eth0,bytes]")

[comment]: # ({/new-5d068733})

[comment]: # ({new-f8b53af1})
##### Exemple 4

Calcul du pourcentage de trafic entrant.

Expression plus complexe :

    100*last("net.if.in[eth0,bytes]")/(last("net.if.in[eth0,bytes]")+last("net.if.out[eth0,bytes]"))

##### Exemple 5

Utilisation correcte d'éléments agrégés dans un élément calculé.

Prenez note de la façon dont les guillemets doubles sont échappés dans
la clé entre guillemets :

    last("grpsum[\"video\",\"net.if.out[eth0,bytes]\",\"last\"]") / last("grpsum[\"video\",\"nginx_stat.sh[active]\",\"last\"]") 

[comment]: # ({/new-f8b53af1})
