[comment]: # translation:outdated

[comment]: # ({new-c5a81a82})
# 1 Index dynamiques

[comment]: # ({/new-c5a81a82})

[comment]: # ({new-e95b21bb})
#### Aperçu

Bien que vous puissiez trouver le numéro d'index nécessaire (par
exemple, d'une interface réseau) parmi les OID SNMP, il se peut que vous
ne puissiez pas toujours compter sur le numéro d'index qui ne reste pas
toujours identique.

Les numéros d'index peuvent être dynamiques - ils peuvent changer au fil
du temps et votre élément peut, en conséquence, cesser de fonctionner.

Pour éviter ce scénario, il est possible de définir un OID qui prend en
compte la possibilité de changement du numéro d'index.

Par exemple, si vous devez extraire la valeur d'index pour l'ajouter à
**ifInOctets** correspondant à l'interface **GigabitEthernet0/1** sur un
périphérique Cisco, utilisez l'OID :

    ifInOctets["index","ifDescr","GigabitEthernet0/1"]

[comment]: # ({/new-e95b21bb})

[comment]: # ({new-6a6d7709})
##### Syntaxe

Une syntaxe particulière pour les OID est utlisée :

**<OID of data>\["index","<base OID of index>","<string
to search for>"\]**

|Paramètre|Description|
|----------|-----------|
|OID of data|OID principal à utiliser pour la récupération de données sur l’élément.|
|index|Méthode de traitement. Actuellement, une méthode est supportée :<br>**index** – recherche index et ajoute à l'OID|
|base OID of index|Cet OID sera recherché pour obtenir la valeur d'index correspondant à la chaîne.|
|string to search for|Chaîne à utiliser pour une correspondance exacte avec une valeur lors de la recherche. Sensible aux majuscules et minuscules.|

[comment]: # ({/new-6a6d7709})

[comment]: # ({new-3da26466})
#### Exemple

Obtenir l'utilisation de la mémoire du processus *apache*.

Si vous utilisez cette syntaxe d'OID :

    HOST-RESOURCES-MIB::hrSWRunPerfMem["index","HOST-RESOURCES-MIB::hrSWRunPath", "/usr/sbin/apache2"]

Le numéro d'index sera recherché ici :

    ...
    HOST-RESOURCES-MIB::hrSWRunPath.5376 = STRING: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5377 = STRING: "/sbin/getty"
    HOST-RESOURCES-MIB::hrSWRunPath.5388 = STRING: "/usr/sbin/apache2"
    HOST-RESOURCES-MIB::hrSWRunPath.5389 = STRING: "/sbin/sshd"
    ...

Maintenant que nous avons l'index 5388. L'index sera ajouté à l'OID de
données afin de recevoir la valeur qui nous intéresse :

    HOST-RESOURCES-MIB::hrSWRunPerfMem.5388 = INTEGER: 31468 KBytes

[comment]: # ({/new-3da26466})

[comment]: # ({new-beab1c6d})
#### Mise en cache de la recherche d'index

Lorsqu'un élément d'index dynamique est demandé, Zabbix récupère et met
en cache toute la table SNMP sous l'OID de base pour l'indexation, même
si une correspondance a été trouvée plus tôt. Ceci est fait dans le cas
où un autre élément se rapporterait au même OID de base plus tard -
Zabbix rechercherait l'index dans le cache, au lieu d'interroger à
nouveau l'hôte surveillé. Notez que chaque processus poller utilise un
cache distinct.

Dans toutes les opérations de récupération de valeur ultérieures, seul
l'index trouvé est vérifié. Si cela n'a pas changé, la valeur est
demandée. Si elle a été modifiée, le cache est reconstruit. Chaque
poller qui rencontre un index modifié parcourt à nouveau la table
d'index SNMP.

[comment]: # ({/new-beab1c6d})
