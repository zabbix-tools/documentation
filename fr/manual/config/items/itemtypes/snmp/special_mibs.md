[comment]: # translation:outdated

[comment]: # ({new-78fa973d})
# 2 OID spéciaux

Certains des OID SNMP les plus utilisés sont traduits automatiquement en
une représentation numérique par Zabbix. Par exemple, **ifIndex** est
traduit en 1.3.6.1.2.1.2.2.1.1, **ifIndex.0** est traduit en
**1.3.6.1.2.1.2.2.1.1.0**.

Le tableau suivant contient la liste des OID spéciaux.

|OID Spéciaux|Identificateur|Description|
|-------------|--------------|-----------|
|ifIndex|1.3.6.1.2.1.2.2.1.1|Une valeur unique pour chaque interface.|
|ifDescr|1.3.6.1.2.1.2.2.1.2|Une chaîne textuelle contenant des informations sur l'interface. Cette chaîne doit inclure le nom du fabricant, le nom du produit et la version de l'interface matérielle.|
|ifType|1.3.6.1.2.1.2.2.1.3|Le type d'interface, distingué en fonction du (des) protocole(s) physique(s) / de liaison immédiatement "en dessous" de la couche réseau dans la pile de protocole.|
|ifMtu|1.3.6.1.2.1.2.2.1.4|La taille du plus grand datagramme pouvant être envoyé/reçu sur l'interface, spécifié en octets.|
|ifSpeed|1.3.6.1.2.1.2.2.1.5|Une estimation de la bande passante actuelle de l'interface en bits par seconde.|
|ifPhysAddress|1.3.6.1.2.1.2.2.1.6|L'adresse de l'interface au niveau de la couche de protocole est immédiatement inférieure à la couche réseau dans la pile de protocoles.|
|ifAdminStatus|1.3.6.1.2.1.2.2.1.7|L'état administratif actuel de l'interface.|
|ifOperStatus|1.3.6.1.2.1.2.2.1.8|L'état opérationnel actuel de l'interface.|
|ifInOctets|1.3.6.1.2.1.2.2.1.10|Nombre total d'octets reçus sur l'interface, y compris les caractères de cadre.|
|ifInUcastPkts|1.3.6.1.2.1.2.2.1.11|Le nombre de paquets sub-network-unicast livrés à un protocole de couche supérieure.|
|ifInNUcastPkts|1.3.6.1.2.1.2.2.1.12|Nombre de paquets non monodiffusion (c.-à-d., Sous-réseau diffusé ou sous-réseau-multidiffusion) transmis à un protocole de couche supérieure.|
|ifInDiscards|1.3.6.1.2.1.2.2.1.13|Le nombre de paquets entrants qui ont été choisis pour être rejetés même si aucune erreur n'a été détectée pour empêcher leur transmission à un protocole de couche supérieure. Une raison possible pour éliminer un tel paquet pourrait être de libérer de l'espace buffer.|
|ifInErrors|1.3.6.1.2.1.2.2.1.14|Nombre de paquets entrants contenant des erreurs empêchant leur transmission à un protocole de couche supérieure.|
|ifInUnknownProtos|1.3.6.1.2.1.2.2.1.15|Nombre de paquets reçus via l'interface qui ont été rejetés en raison d'un protocole inconnu ou non supporté.|
|ifOutOctets|1.3.6.1.2.1.2.2.1.16|Nombre total d'octets transmis hors de l'interface, y compris les caractères de cadre.|
|ifOutUcastPkts|1.3.6.1.2.1.2.2.1.17|Nombre total de paquets que les protocoles de niveau supérieur requis ont transmis et qui n'ont pas été adressés à une adresse de multidiffusion ou de diffusion de cette sous-couche, y compris ceux qui ont été rejetés ou non envoyés.|
|ifOutNUcastPkts|1.3.6.1.2.1.2.2.1.18|Nombre total de paquets que les protocoles de niveau supérieur requis ont transmis et qui ont été adressés à une adresse de multidiffusion ou de diffusion de cette sous-couche, y compris ceux qui ont été rejetés ou non envoyés.|
|ifOutDiscards|1.3.6.1.2.1.2.2.1.19|Le nombre de paquets sortants qui ont été choisis pour être rejetés même si aucune erreur n'a été détectée pour empêcher leur transmission. Une raison possible pour éliminer un tel paquet pourrait être de libérer de l'espace buffer.|
|ifOutErrors|1.3.6.1.2.1.2.2.1.20|Le nombre de paquets sortants qui n'ont pas pu être transmis en raison d'erreurs.|
|ifOutQLen|1.3.6.1.2.1.2.2.1.21|La longueur de la file d'attente de paquets de sortie (en paquets).|

[comment]: # ({/new-78fa973d})
