[comment]: # translation:outdated

[comment]: # ({new-52403269})
# 4 Vérifications IPMI

[comment]: # ({/new-52403269})

[comment]: # ({new-2f30e610})
#### Aperçu

Vous pouvez superviser l'intégrité et la disponibilité des périphériques
IPMI (Intelligent Platform Management Interface) dans Zabbix. Pour
effectuer les vérifications IPMI, le serveur Zabbix doit initialement
être [configuré](/fr/manual/installation/install#from_the_sources) avec
le support IPMI.

IPMI est une interface normalisée pour la gestion à distance de systèmes
informatiques. Il permet de surveiller l'état du matériel directement à
partir des cartes de gestion dites «out-of-band», indépendamment du
système d'exploitation ou de la mise sous tension de la machine.

La surveillance IPMI de Zabbix ne fonctionne que pour les périphériques
prenant en charge IPMI (HP iLO, DELL DRAC, IBM RSA, Sun SSP, etc.).

Depuis Zabbix 3.4, un nouveau processus de gestion IPMI a été ajouté
pour planifier les vérifications IPMI par les pollers IPMI. Maintenant,
un hôte est toujours interrogé par un seul poller IPMI à la fois, ce qui
réduit le nombre de connexions ouvertes aux contrôleurs BMC. Avec ces
modifications, il est sûr d'augmenter le nombre de pollers IPMI sans se
soucier de la surcharge du contrôleur BMC. Le processus du gestion IPMI
est automatiquement démarré lorsqu'au moins un poller IPMI est démarré.

Voir aussi les [problèmes
connus](/fr/manual/installation/known_issues#ipmi_checks) pour les
vérifications IPMI.

[comment]: # ({/new-2f30e610})

[comment]: # ({new-5d32b87c})
#### Configuration

[comment]: # ({/new-5d32b87c})

[comment]: # ({new-4f35db5e})
##### Configuration de l'hôte

Un hôte doit être configuré pour traiter les vérifications IPMI. Une
interface IPMI doit être ajoutée, avec les adresses IP et les numéros de
port respectifs, et les paramètres d'authentification IPMI doivent être
définis.

Voir la [configuration des hôtes](/fr/manual/config/hosts/host) pour
plus de détails.

[comment]: # ({/new-4f35db5e})

[comment]: # ({new-f7bb9e76})
##### Configuration du serveur

Par défaut, le serveur Zabbix n'est pas configuré pour démarrer les
pollers IPMI. Par conséquent, les éléments IPMI ajoutés ne
fonctionneront pas. Pour changer cela, ouvrez le fichier de
configuration du serveur Zabbix
([zabbix\_server.conf](/fr/manual/appendix/config/zabbix_server)) en
tant qu'utilisateur root et recherchez la ligne suivante :

    # StartIPMIPollers=0

Décommentez-la et définissez le nombre d'interrogation sur, disons, 3,
afin qu'il indique :

    StartIPMIPollers=3

Enregistrez le fichier et redémarrez ensuite zabbix\_server.

[comment]: # ({/new-f7bb9e76})

[comment]: # ({new-b0db4658})
##### Configuration de l'élément

Lors de la [configuration d'un élément](/fr/manual/config/items/item) au
niveau de l'hôte :

-   Pour l'*interface hôte*, sélectionnez l'IP IPMI et le port
-   Sélectionnez 'IPMI agent' comme *Type*
-   Spécifiez le *capteur IPMI* (par exemple 'FAN MOD 1A RPM' sur Dell
    Poweredge). Par défaut, l'ID du capteur doit être spécifié. Il est
    aussi possible d'utiliser des préfixe avant la valeur :
    -   `id:` - pour spécifier l'ID du capteur ;
    -   `name:` - pour spécifier le nom complet du capteur. Cela peut
        être utile dans certaines situations quand les capteurs peuvent
        seulement être distingués en spécifiant le nom complet.
-   Entrez une [clé](/fr/manual/config/items/item/key) d'élément qui
    unique pour l'hôte (disons, ipmi.fan.rpm)
-   Sélectionnez le type d'information correspondant ('Numérique
    (flottant)' dans ce cas, pour les capteurs discrets - 'Numérique
    (non signé)'), les unités (très probablement 'rpm') et tout autre
    attribut d'élément requis

[comment]: # ({/new-b0db4658})

[comment]: # ({new-af2b187b})
#### Délai d'expiration et fin de session

Les délais d'attente de messages IPMI et les compteurs de tentatives
sont définis dans la bibliothèque OpenIPMI. Du fait de la conception
actuelle d'OpenIPMI, il n'est pas possible de rendre ces valeurs
configurables dans Zabbix, ni au niveau de l'interface ni au niveau de
l'objet.

Le délai d'inactivité de la session IPMI pour le réseau local est de 60
+/- 3 secondes. Actuellement, il n'est pas possible d'implémenter
l'envoi périodique de la commande « Activate Session » avec OpenIPMI.
S'il n'y a pas de vérifications d'éléments IPMI depuis Zabbix vers un
contrôleur BMC particulier pendant plus de la temporisation de session
configurée dans BMC, la vérification IPMI suivante après l'expiration du
délai expire en raison des délais d'attente, des nouvelles tentatives ou
des erreurs de réception. Après cela, une nouvelle session est ouverte
et une nouvelle analyse du contrôleur BMC est lancée. Si vous souhaitez
éviter les rediffusions inutiles du contrôleur BMC, il est conseillé de
définir l'intervalle d'interrogation de l'élément IPMI sous le délai
d'inactivité de la session IPMI configuré dans le contrôleur BMC.

[comment]: # ({/new-af2b187b})

[comment]: # ({new-1e03516a})
#### Remarques sur les capteurs IPMI discrets

Pour trouver des capteurs sur un hôte, démarrez le serveur Zabbix avec
**DebugLevel=4** activé. Attendez quelques minutes et recherchez les
enregistrements de détection de capteur dans le fichier de log du
serveur Zabbix :

    $ grep 'Added sensor' zabbix_server.log
    8358:20130318:111122.170 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:7 id:'CATERR' reading_type:0x3 ('discrete_state') type:0x7 ('processor') full_name:'(r0.32.3.0).CATERR'
    8358:20130318:111122.170 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:15 id:'CPU Therm Trip' reading_type:0x3 ('discrete_state') type:0x1 ('temperature') full_name:'(7.1).CPU Therm Trip'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'System Event Log' reading_type:0x6f ('sensor specific') type:0x10 ('event_logging_disabled') full_name:'(7.1).System Event Log'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'PhysicalSecurity' reading_type:0x6f ('sensor specific') type:0x5 ('physical_security') full_name:'(23.1).PhysicalSecurity'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'IPMI Watchdog' reading_type:0x6f ('sensor specific') type:0x23 ('watchdog_2') full_name:'(7.7).IPMI Watchdog'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'Power Unit Stat' reading_type:0x6f ('sensor specific') type:0x9 ('power_unit') full_name:'(21.1).Power Unit Stat'
    8358:20130318:111122.171 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'P1 Therm Ctrl %' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(3.1).P1 Therm Ctrl %'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:16 id:'P1 Therm Margin' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(3.2).P1 Therm Margin'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:13 id:'System Fan 2' reading_type:0x1 ('threshold') type:0x4 ('fan') full_name:'(29.1).System Fan 2'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:13 id:'System Fan 3' reading_type:0x1 ('threshold') type:0x4 ('fan') full_name:'(29.1).System Fan 3'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'P1 Mem Margin' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).P1 Mem Margin'
    8358:20130318:111122.172 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'Front Panel Temp' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).Front Panel Temp'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:15 id:'Baseboard Temp' reading_type:0x1 ('threshold') type:0x1 ('temperature') full_name:'(7.6).Baseboard Temp'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:9 id:'BB +5.0V' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +5.0V'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'BB +3.3V STBY' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +3.3V STBY'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:9 id:'BB +3.3V' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +3.3V'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'BB +1.5V P1 DDR3' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.5V P1 DDR3'
    8358:20130318:111122.173 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:17 id:'BB +1.1V P1 Vccp' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.1V P1 Vccp'
    8358:20130318:111122.174 Added sensor: host:'192.168.1.12:623' id_type:0 id_sz:14 id:'BB +1.05V PCH' reading_type:0x1 ('threshold') type:0x2 ('voltage') full_name:'(7.1).BB +1.05V PCH'

Pour décoder les types et les états des capteurs IPMI, procurez-vous une
copie des spécifications IPMI 2.0 à l' adresse
<http://www.intel.com/content/www/us/en/servers/ipmi/ipmi-specifications.html>
(Au moment de la rédaction du le document le plus récent était
<http://www.intel.com/content/dam/www/public/us/en/documents/product-briefs/second-gen-interface-spec-v2.pdf>)

Le premier paramètre avec lequel commencer est "read\_type". Utilisez
"Tableau 42-1, Plages de codes de type d'événement/de lecture" dans les
spécifications pour décoder le code "read\_type". La plupart des
capteurs de notre exemple ont "read\_type:0x1" qui signifie capteur de
"seuil". "Le tableau 42-3, Codes de type de capteur" montre que
"type:0x1" signifie capteur de température, "type:0x2" - capteur de
tension, "type:0x4" - ventilateur etc. Les capteurs de seuil sont
parfois appelés "analogiques" car ils mesurent des paramètres continus
comme la température, la tension, les révolutions par minute.

Un autre exemple - un capteur avec "read\_type:0x3". "Tableau 42-1,
Plages de codes de type événement/lecture" indique que les codes de type
de lecture 02h-0Ch signifient "Generic Discrete". Les capteurs discrets
ont jusqu'à 15 états possibles (en d'autres termes, jusqu'à 15 octets
significatifs). Par exemple, pour le capteur 'CATERR' avec "type:0x7",
le "Tableau 42-3, Codes de type de capteur" montre que ce type signifie
"Processeur" et que la signification des octets individuels est : 00h
(l'octet le moins significatif) - IERR, 01h - Thermal Trip etc.

Il y a peu de capteurs avec "reading\_type:0x6f" dans notre exemple.
Pour ces capteurs, le "Tableau 42-1, Plages de codes de type
d'événement/lecture" conseille d'utiliser "Tableau 42-3, Codes de type
de capteur" pour décoder les significations des octets. Par exemple, le
capteur 'Power Unit Stat' a le type "type:0x9" qui signifie "Power
Unit". Offset 00h signifie "PowerOff/Power Down". En d'autres termes, si
l'octet le moins significatif est 1, le serveur est mis hors tension.
Pour tester cet octet, la fonction **band** avec le masque 1 peut être
utilisée. L'expression de déclenchement pourrait être comme

       {www.zabbix.com:Power Unit Stat.band(#1,1)}=1

pour avertir d'un serveur éteint.

[comment]: # ({/new-1e03516a})

[comment]: # ({new-3ea36861})
##### Notes sur les noms de capteurs discrets dans OpenIPMI-2.0.16, 2.0.17, 2.0.18 et 2.0.19

Les noms de capteurs discrets dans OpenIPMI-2.0.16, 2.0.17 et 2.0.18 ont
souvent un "`0`" supplémentaire (ou un autre chiffre ou lettre) ajouté à
la fin. Par exemple, si `ipmitool` et OpenIPMI-2.0.19 affichent les noms
des capteurs comme "`PhysicalSecurity`" ou "`CATERR`", dans
OpenIPMI-2.0.16, 2.0.17 et 2.0.18, les noms sont "`PhysicalSecurity0`"
ou "`CATERR0`", respectivement.

Lors de la configuration d'un élément IPMI avec le serveur Zabbix à
l'aide d'OpenIPMI-2.0.16, 2.0.17 et 2.0.18, utilisez ces noms se
terminant par "0" dans le champ *Capteur IPMI* des éléments de l'agent
IPMI. Lorsque votre serveur Zabbix est mis à jour vers une nouvelle
distribution Linux, qui utilise OpenIPMI-2.0.19 (ou une version
ultérieure), les éléments avec ces capteurs IPMI discrets deviendront
"NON SUPPORTE". Vous devrez changer leur nom de capteur IPMI (enlevez le
'0' à la fin) et attendre un moment avant de les réactiver.

[comment]: # ({/new-3ea36861})

[comment]: # ({new-761eb9da})
##### Remarques sur la disponibilité simultanée du capteur de seuil et du capteur discret

Certains agents IPMI fournissent à la fois un capteur de seuil et un
capteur discret sous le même nom. Dans les versions de Zabbix
antérieures à 2.2.8 et 2.4.3, le premier capteur fourni était choisi.
Depuis les versions 2.2.8 et 2.4.3, la préférence est toujours donnée au
capteur de seuil.

[comment]: # ({/new-761eb9da})

[comment]: # ({new-2adee003})
##### Remarques sur la fin de la connexion

Si les vérifications IPMI ne sont pas effectuées (pour une raison
quelconque : tous les éléments IPMI hôte désactivés/non pris en charge,
hôte désactivé/supprimé, hôte en maintenance, etc.) la connexion IPMI
sera terminée du serveur ou du proxy Zabbix en 3 à 4 heures selon
l'heure lorsque le serveur/proxy Zabbix a été démarré.

[comment]: # ({/new-2adee003})

[comment]: # ({new-69164d8f})
##### Notes on connection termination

If IPMI checks are not performed (by any reason: all host IPMI items
disabled/notsupported, host disabled/deleted, host in maintenance etc.)
the IPMI connection will be terminated from Zabbix server or proxy in 3
to 4 hours depending on the time when Zabbix server/proxy was started.

[comment]: # ({/new-69164d8f})
