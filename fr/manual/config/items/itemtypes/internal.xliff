<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/config/items/itemtypes/internal.md">
    <body>
      <trans-unit id="8981e17d" xml:space="preserve">
        <source># 8 Internal checks</source>
      </trans-unit>
      <trans-unit id="34928067" xml:space="preserve">
        <source>#### Overview

Internal checks allow to monitor the internal processes of Zabbix. In other words, 
you can monitor what goes on with Zabbix server or Zabbix proxy.

Internal checks are calculated:

-   on Zabbix server - if the host is monitored by server
-   on Zabbix proxy - if the host is monitored by proxy

Internal checks are processed by server or proxy regardless of the host maintenance status.

To use this item, choose the **Zabbix internal** item type.

::: notetip
Internal checks are processed by Zabbix pollers.
:::</source>
      </trans-unit>
      <trans-unit id="5d8f3b63" xml:space="preserve">
        <source>#### Performance

Using some internal items may negatively affect performance. These items are:

-   `zabbix[host,,items]`
-   `zabbix[host,,items_unsupported]`
-   `zabbix[hosts]`
-   `zabbix[items]`
-   `zabbix[items_unsupported]`
-   `zabbix[queue]`
-   `zabbix[requiredperformance]`
-   `zabbix[stats,,,queue]`
-   `zabbix[triggers]`

The [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix) and [Queue](/manual/web_interface/frontend_sections/administration/queue) 
frontend sections are also affected.</source>
      </trans-unit>
      <trans-unit id="74b446f4" xml:space="preserve">
        <source>#### Supported checks

The item keys are listed without optional parameters and additional information. Click on the item key to see the full details.

|Item key|Description|
|--|--------|
|[zabbix\[boottime\]](#boottime)|The startup time of Zabbix server or Zabbix proxy process in seconds.|
|[zabbix\[cluster,discovery,nodes\]](#cluster.discovery)|Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.|
|[zabbix\[connector_queue\]](#connector.queue)|The count of values enqueued in the connector queue.|
|[zabbix\[discovery_queue\]](#discovery.queue)|The count of network checks enqueued in the discovery queue.|
|[zabbix\[host,,items\]](#host.items)|The number of enabled items (supported and not supported) on the host.|
|[zabbix\[host,,items_unsupported\]](#host.items.unsupported)|The number of enabled unsupported items on the host.|
|[zabbix\[host,,maintenance\]](#maintenance)|The current maintenance status of the host.|
|[zabbix\[host,active_agent,available\]](#active.available)|The availability of active agent checks on the host.|
|[zabbix\[host,discovery,interfaces\]](#discovery.interfaces)|The details of all configured interfaces of the host in Zabbix frontend.|
|[zabbix\[host,available\]](#host.available)|The availability of the main interface of a particular type of checks on the host.|
|[zabbix\[hosts\]](#hosts)|The number of monitored hosts.|
|[zabbix\[items\]](#items)|The number of enabled items (supported and not supported).|
|[zabbix\[items_unsupported\]](#items.unsupported)|The number of unsupported items.|
|[zabbix\[java\]](#java)|The information about Zabbix Java gateway.|
|[zabbix\[lld_queue\]](#lld.queue)|The count of values enqueued in the low-level discovery processing queue.|
|[zabbix\[preprocessing_queue\]](#preprocessing.queue)|The count of values enqueued in the preprocessing queue.|
|[zabbix\[process\]](#process)|The percentage of time a particular Zabbix process or a group of processes (identified by \&lt;type\&gt; and \&lt;mode\&gt;) spent in \&lt;state\&gt;.|
|[zabbix\[proxy\]](#proxy)|The information about Zabbix proxy.|
|[zabbix\[proxy,discovery\]](#proxy.discovery)|The list of Zabbix proxies.|
|[zabbix\[proxy_history\]](#proxy.history)|The number of values in the proxy history table waiting to be sent to the server.|
|[zabbix\[queue\]](#queue)|The number of monitored items in the queue which are delayed at least by \&lt;from\&gt; seconds, but less than \&lt;to\&gt; seconds.|
|[zabbix\[rcache\]](#rcache)|The availability statistics of the Zabbix configuration cache.|
|[zabbix\[requiredperformance\]](#required.performance)|The required performance of Zabbix server or Zabbix proxy, in new values per second expected.|
|[zabbix\[stats\]](#stats)|The internal metrics of a remote Zabbix server or proxy.|
|[zabbix\[stats,,,queue\]](#stats.queue)|The internal queue metrics of a remote Zabbix server or proxy.|
|[zabbix\[tcache\]](#tcache)|The effectiveness statistics of the Zabbix trend function cache.|
|[zabbix\[triggers\]](#triggers)|The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.|
|[zabbix\[uptime\]](#uptime)|The uptime of the Zabbix server or proxy process in seconds.|
|[zabbix\[vcache,buffer\]](#vcache)|The availability statistics of the Zabbix value cache.|
|[zabbix\[vcache,cache\]](#vcache.parameter)|The effectiveness statistics of the Zabbix value cache.|
|[zabbix\[version\]](#version)|The version of Zabbix server or proxy.|
|[zabbix\[vmware,buffer\]](#vmware)|The availability statistics of the Zabbix vmware cache.|
|[zabbix\[wcache\]](#wcache)|The statistics and availability of the Zabbix write cache.|</source>
      </trans-unit>
      <trans-unit id="0f6066ab" xml:space="preserve">
        <source>
### Item key details

-   Parameters without angle brackets are constants - for example,
    'host' and 'available' in `zabbix[host,&lt;type&gt;,available]`. Use them in the item key *as is*.
-   Values for items and item parameters that are "not supported on proxy" can only be retrieved if the host is monitored by server. And vice versa, values "not supported on server" can only be retrieved if the host is monitored by proxy.</source>
      </trans-unit>
      <trans-unit id="e52bbd1a" xml:space="preserve">
        <source>
##### zabbix[boottime] {#boottime}

&lt;br&gt;
The startup time of Zabbix server or Zabbix proxy process in seconds.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="d1cde038" xml:space="preserve">
        <source>
##### zabbix[cluster,discovery,nodes] {#cluster.discovery}

&lt;br&gt;
Discovers the [high availability cluster](/manual/concepts/server/ha) nodes.&lt;br&gt;
Return value: *JSON object*.

This item can be used in low-level discovery.</source>
      </trans-unit>
      <trans-unit id="09c4f0cf" xml:space="preserve">
        <source>
##### zabbix[connector_queue] {#connector.queue}

&lt;br&gt;
The count of values enqueued in the connector queue.&lt;br&gt;
Return value: *Integer*.

This item is supported since Zabbix 6.4.0.</source>
      </trans-unit>
      <trans-unit id="fcd989ee" xml:space="preserve">
        <source>
##### zabbix[discovery_queue] {#discovery.queue}

&lt;br&gt;
The count of network checks enqueued in the discovery queue.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="f065c85f" xml:space="preserve">
        <source>
##### zabbix[host,,items] {#host.items}

&lt;br&gt;
The number of enabled items (supported and not supported) on the host.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="31a0bdcc" xml:space="preserve">
        <source>
##### zabbix[host,,items_unsupported] {#host.items.unsupported}

&lt;br&gt;
The number of enabled unsupported items on the host.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="6220db31" xml:space="preserve">
        <source>
##### zabbix[host,,maintenance] {#maintenance}

&lt;br&gt;
The current maintenance status of the host.&lt;br&gt;
Return values: *0* - normal state; *1* - maintenance with data collection; *2* - maintenance without data collection.

Comments:

-   This item is always processed by Zabbix server regardless of the host location (on server or proxy). The proxy will not receive this item with configuration data. 
-   The second parameter must be empty and is reserved for future use.</source>
      </trans-unit>
      <trans-unit id="ec7187d4" xml:space="preserve">
        <source>
##### zabbix[host,active_agent,available] {#active.available}

&lt;br&gt;
The availability of active agent checks on the host.&lt;br&gt;
Return values: *0* - unknown; *1* - available; *2* - not available.</source>
      </trans-unit>
      <trans-unit id="48ad1e26" xml:space="preserve">
        <source>
##### zabbix[host,discovery,interfaces] {#discovery.interfaces}

&lt;br&gt;
The details of all configured interfaces of the host in Zabbix frontend.&lt;br&gt;
Return value: *JSON object*.

Comments:

-   This item can be used in [low-level discovery](/manual/discovery/low_level_discovery/examples/host_interfaces);
-   This item is not supported on Zabbix proxy.</source>
      </trans-unit>
      <trans-unit id="9752a684" xml:space="preserve">
        <source>
##### zabbix[host,&lt;type&gt;,available] {#host.available}

&lt;br&gt;
The availability of the main interface of a particular type of checks on the host.&lt;br&gt;
Return values: *0* - not available; *1* - available; *2* - unknown.

Comments:

-   Valid **types** are: *agent*, *snmp*, *ipmi*, *jmx*;
-   The item value is calculated according to the configuration parameters regarding host [unreachability/unavailability](/manual/appendix/items/unreachability).</source>
      </trans-unit>
      <trans-unit id="a65ef5bf" xml:space="preserve">
        <source>
##### zabbix[hosts] {#hosts}

&lt;br&gt;
The number of monitored hosts.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="f291ac50" xml:space="preserve">
        <source>
##### zabbix[items] {#items}

&lt;br&gt;
The number of enabled items (supported and not supported).&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="53295b6d" xml:space="preserve">
        <source>
##### zabbix[items_unsupported] {#items.unsupported}

&lt;br&gt;
The number of unsupported items.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="ba37fff4" xml:space="preserve">
        <source>
##### zabbix[java,,&lt;param&gt;] {#java}

&lt;br&gt;
The information about Zabbix Java gateway.&lt;br&gt;
Return values: *1* - if &lt;param&gt; is *ping*; *Java gateway version* -  if &lt;param&gt; is *version* (for example: "2.0.0").

Comments:

-   Valid values for **param** are: *ping*, *version*;
-   This item can be used to check Java gateway availability using the `nodata()` trigger function;
-   The second parameter must be empty and is reserved for future use.</source>
      </trans-unit>
      <trans-unit id="8f8235d6" xml:space="preserve">
        <source>
##### zabbix[lld_queue] {#lld.queue}

&lt;br&gt;
The count of values enqueued in the low-level discovery processing queue.&lt;br&gt;
Return value: *Integer*.

This item can be used to monitor the low-level discovery processing queue length.</source>
      </trans-unit>
      <trans-unit id="3e07a93d" xml:space="preserve">
        <source>
##### zabbix[preprocessing_queue] {#preprocessing.queue}

&lt;br&gt;
The count of values enqueued in the preprocessing queue.&lt;br&gt;
Return value: *Integer*.

This item can be used to monitor the preprocessing queue length.</source>
      </trans-unit>
      <trans-unit id="b74b9537" xml:space="preserve">
        <source>
##### zabbix[process,&lt;type&gt;,&lt;mode&gt;,&lt;state&gt;] {#process}

&lt;br&gt;
The percentage of time a particular Zabbix process or a group of processes (identified by \&lt;type\&gt; and \&lt;mode\&gt;) spent in \&lt;state\&gt;. It is calculated for the last minute only.&lt;br&gt;
Return value: *Float*.

Parameters:

-   **type** - for [server processes](/manual/concepts/server#server_process_types): *alert manager*, *alert syncer*, *alerter*, *availability manager*, *configuration syncer*, *connector manager*, *connector worker*, *discovery manager*, *discovery worker*, *escalator*, *ha manager*, *history poller*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *lld manager*, *lld worker*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *proxy poller*, *self-monitoring*, *service manager*, *snmp trapper*, *task manager*, *timer*, *trapper*, *trigger housekeeper*, *unreachable poller*, *vmware collector*;&lt;br&gt;for [proxy processes](/manual/concepts/proxy#proxy_process_types): *availability manager*, *configuration syncer*, *data sender*, *discovery manager*, *discovery worker*, *history syncer*, *housekeeper*, *http poller*, *icmp pinger*, *ipmi manager*, *ipmi poller*, *java poller*, *odbc poller*, *poller*, *preprocessing manager*, *preprocessing worker*, *self-monitoring*, *snmp trapper*, *task manager*, *trapper*, *unreachable poller*, *vmware collector*
-   **mode** - *avg* - average value for all processes of a given type (default)&lt;br&gt;*count* - returns number of forks for a given process type, &lt;state&gt; should not be specified&lt;br&gt;*max* - maximum value&lt;br&gt;*min* - minimum value&lt;br&gt;*&lt;process number&gt;* - process number (between 1 and the number of pre-forked instances). For example, if 4 trappers are running, the value is between 1 and 4.
-   **state** - *busy* - process is in busy state, for example, the processing request (default); *idle* - process is in idle state doing nothing.

Comments:

-   If \&lt;mode\&gt; is a Zabbix process number that is not running (for example, with 5 pollers running the \&lt;mode\&gt; is specified to be 6), such an item will turn unsupported;
-   Minimum and maximum refers to the usage percentage for a single process. So if in a group of 3 pollers usage percentages per process were 2, 18 and 66, min would return 2 and max would return 66.
-   Processes report what they are doing in shared memory and the self-monitoring process summarizes that data each second. State changes (busy/idle) are registered upon change - thus a process that becomes busy registers as such and doesn't change or update the state until it becomes idle. This ensures that even fully hung processes will be correctly registered as 100% busy.
-   Currently, "busy" means "not sleeping", but in the future additional states might be introduced - waiting for locks, performing database queries, etc.
-   On Linux and most other systems, resolution is 1/100 of a second.

Examples:

    zabbix[process,poller,avg,busy] #the average time of poller processes spent doing something during the last minute
    zabbix[process,"icmp pinger",max,busy] #the maximum time spent doing something by any ICMP pinger process during the last minute
    zabbix[process,"history syncer",2,busy] #the time spent doing something by history syncer number 2 during the last minute
    zabbix[process,trapper,count] #the amount of currently running trapper processes</source>
      </trans-unit>
      <trans-unit id="d90bc125" xml:space="preserve">
        <source>
##### zabbix[proxy,&lt;name&gt;,&lt;param&gt;] {#proxy}

&lt;br&gt;
The information about Zabbix proxy.&lt;br&gt;
Return value: *Integer*.

Parameters:

-   **name** - the proxy name;
-   **param** - *delay* - how long the collected values are unsent, calculated as "proxy delay" (difference between the current proxy time and the timestamp of the oldest unsent value on proxy) + ("current server time" - "proxy lastaccess").</source>
      </trans-unit>
      <trans-unit id="fd9ea3f4" xml:space="preserve">
        <source>
##### zabbix[proxy,discovery] {#proxy.discovery}

&lt;br&gt;
The list of Zabbix proxies with name, mode, encryption, compression, version, last seen, host count, item count, required values per second (vps) and version status (current/outdated/unsupported).&lt;br&gt;
Return value: *JSON object*.</source>
      </trans-unit>
      <trans-unit id="5b463b30" xml:space="preserve">
        <source>
##### zabbix[proxy_history] {#proxy.history}

&lt;br&gt;
The number of values in the proxy history table waiting to be sent to the server.&lt;br&gt;
Return values: *Integer*.

This item is not supported on Zabbix server.</source>
      </trans-unit>
      <trans-unit id="ebe20dec" xml:space="preserve">
        <source>
##### zabbix[queue,&lt;from&gt;,&lt;to&gt;] {#queue}

&lt;br&gt;
The number of monitored items in the queue which are delayed at least by \&lt;from\&gt; seconds, but less than \&lt;to\&gt; seconds.&lt;br&gt;
Return value: *Integer*.

Parameters:

-   **from** - default: 6 seconds;
-   **to** - default: infinity.

[Time-unit symbols](/manual/appendix/suffixes) (s,m,h,d,w) are supported in the parameters.</source>
      </trans-unit>
      <trans-unit id="04aafd0c" xml:space="preserve">
        <source>
##### zabbix[rcache,&lt;cache&gt;,&lt;mode&gt;] {#rcache}

&lt;br&gt;
The availability statistics of the Zabbix configuration cache.&lt;br&gt;
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *total* - the total size of buffer&lt;br&gt;*free* - the size of free buffer&lt;br&gt;*pfree* - the percentage of free buffer&lt;br&gt;*used* - the size of used buffer&lt;br&gt;*pused* - the percentage of used buffer</source>
      </trans-unit>
      <trans-unit id="f2b37a90" xml:space="preserve">
        <source>
##### zabbix[requiredperformance] {#required.performance}

&lt;br&gt;
The required performance of Zabbix server or Zabbix proxy, in new values per second expected.&lt;br&gt;
Return value: *Float*.

Approximately correlates with "Required server performance, new values per second" in *Reports → [System information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)*.</source>
      </trans-unit>
      <trans-unit id="8054c8ab" xml:space="preserve">
        <source>
##### zabbix[stats,&lt;ip&gt;,&lt;port&gt;] {#stats}

&lt;br&gt;
The internal metrics of a remote Zabbix server or proxy.&lt;br&gt;
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).</source>
      </trans-unit>
      <trans-unit id="d7c2c9be" xml:space="preserve">
        <source>
##### zabbix[stats,&lt;ip&gt;,&lt;port&gt;,queue,&lt;from&gt;,&lt;to&gt;] {#stats.queue}

&lt;br&gt;
The internal queue metrics (see `zabbix[queue,&lt;from&gt;,&lt;to&gt;]`) of a remote Zabbix server or proxy.&lt;br&gt;
Return values: *JSON object*.

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051);
-   **from** - delayed by at least (default is 6 seconds);
-   **to** - delayed by at most (default is infinity).

Comments:

-   The stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance;
-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics).</source>
      </trans-unit>
      <trans-unit id="c688d590" xml:space="preserve">
        <source>
##### zabbix[tcache,&lt;cache&gt;,&lt;parameter&gt;] {#tcache}

&lt;br&gt;
The effectiveness statistics of the Zabbix trend function cache.&lt;br&gt;
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **cache** - *buffer*;
-   **mode** - *all* - total cache requests (default)&lt;br&gt;*hits* - cache hits&lt;br&gt;*phits* - percentage of cache hits&lt;br&gt;*misses* - cache misses&lt;br&gt;*pmisses* - percentage of cache misses&lt;br&gt;*items* - the number of cached items&lt;br&gt;*requests* - the number of cached requests&lt;br&gt;*pitems* - percentage of cached items from cached items + requests. Low percentage most likely means that the cache size can be reduced.

This item is not supported on Zabbix proxy.</source>
      </trans-unit>
      <trans-unit id="9699e4ca" xml:space="preserve">
        <source>
##### zabbix[triggers] {#triggers}

&lt;br&gt;
The number of enabled triggers in Zabbix database, with all items enabled on enabled hosts.&lt;br&gt;
Return value: *Integer*.

This item is not supported on Zabbix proxy.</source>
      </trans-unit>
      <trans-unit id="a5c8cdec" xml:space="preserve">
        <source>
##### zabbix[uptime] {#uptime}

&lt;br&gt;
The uptime of the Zabbix server or proxy process in seconds.&lt;br&gt;
Return value: *Integer*.</source>
      </trans-unit>
      <trans-unit id="304cb09c" xml:space="preserve">
        <source>
##### zabbix[vcache,buffer,&lt;mode&gt;] {#vcache}

&lt;br&gt;
The availability statistics of the Zabbix value cache.&lt;br&gt;
Return values: *Integer* (for size); *Float* (for percentage).

Parameter:

-   **mode** - *total* - the total size of buffer&lt;br&gt;*free* - the size of free buffer&lt;br&gt;*pfree* - the percentage of free buffer&lt;br&gt;*used* - the size of used buffer&lt;br&gt;*pused* - the percentage of used buffer

This item is not supported on Zabbix proxy.</source>
      </trans-unit>
      <trans-unit id="1454b994" xml:space="preserve">
        <source>
##### zabbix[vcache,cache,&lt;parameter&gt;] {#vcache.parameter}

&lt;br&gt;
The effectiveness statistics of the Zabbix value cache.&lt;br&gt;
Return values: *Integer*. With the *mode* parameter returns: 0 - normal mode; 1 - low memory mode.

Parameters:

-   **parameter** - *requests* - the total number of requests&lt;br&gt;*hits* - the number of cache hits (history values taken from the cache)&lt;br&gt;*misses* - the number of cache misses (history values taken from the database)&lt;br&gt;*mode* - the value cache operating mode

Comments:

-   Once the low-memory mode has been switched on, the value cache will remain in this state for 24 hours, even if the problem that triggered this mode is resolved sooner;
-   You may use this key with the *Change per second* preprocessing step in order to get values-per-second statistics;
-   This item is not supported on Zabbix proxy.</source>
      </trans-unit>
      <trans-unit id="ddde46a0" xml:space="preserve">
        <source>
##### zabbix[version] {#version}

&lt;br&gt;
The version of Zabbix server or proxy.&lt;br&gt;
Return value: *String*. For example: `6.0.0beta1`.</source>
      </trans-unit>
      <trans-unit id="dda0c008" xml:space="preserve">
        <source>
##### zabbix[vmware,buffer,&lt;mode&gt;] {#vmware}

&lt;br&gt;
The availability statistics of the Zabbix vmware cache.&lt;br&gt;
Return values: *Integer* (for size); *Float* (for percentage).

Parameters:

-   **mode** - *total* - the total size of buffer&lt;br&gt;*free* - the size of free buffer&lt;br&gt;*pfree* - the percentage of free buffer&lt;br&gt;*used* - the size of used buffer&lt;br&gt;*pused* - the percentage of used buffer</source>
      </trans-unit>
      <trans-unit id="9fa76fcd" xml:space="preserve">
        <source>
##### zabbix[wcache,&lt;cache&gt;,&lt;mode&gt;] {#wcache}

&lt;br&gt;
The statistics and availability of the Zabbix write cache.&lt;br&gt;
Return values: *Integer* (for number/size); *Float* (for percentage).

Parameters:

-   **cache** - *values*, *history*, *index*, or *trend*;
-   **mode** - (with *values*) *all* (default) - the total number of values processed by Zabbix server/proxy, except unsupported items (counter)&lt;br&gt;*float* - the number of processed float values (counter)&lt;br&gt;*uint* - the number of processed unsigned integer values (counter)&lt;br&gt;*str* - the number of processed character/string values (counter)&lt;br&gt;*log* - the number of processed log values (counter)&lt;br&gt;*text* - the number of processed text values (counter)&lt;br&gt;*not supported* - the number of times item processing resulted in item becoming unsupported or keeping that state (counter)&lt;br&gt;(with *history*, *index*, *trend* cache) *pfree* (default) - the percentage of free buffer&lt;br&gt;*total* - the total size of buffer&lt;br&gt;*free* - the size of free buffer&lt;br&gt;*used* - the size of used buffer&lt;br&gt;*pused* - the percentage of used buffer

Comments:

-   Specifying \&lt;cache\&gt; is mandatory. The `trend` cache parameter is not supported with Zabbix proxy.
-   The history cache is used to store item values. A low number indicates performance problems on the database side.
-   The history index cache is used to index the values stored in the history cache;
-   The trend cache stores the aggregate for the current hour for all items that receive data;
-   You may use the zabbix[wcache,values] key with the *Change per second* preprocessing step in order to get values-per-second statistics.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
