[comment]: # translation:outdated

[comment]: # ({new-5479dd79})
# 1 Clés d'élément de supervision VMware

[comment]: # ({/new-5479dd79})

[comment]: # ({new-98337d09})
#### Clés d'élément

Le tableau fournit des détails sur les vérifications simples pouvant
être utilisées pour superviser les [environnements
VMware](/fr/manual/vm_monitoring).

|Clé|<|<|<|<|
|----|-|-|-|-|
|<|Description|Valeur de retour|Paramètres|Commentaires|
|vmware.cluster.discovery\[<url>\]|<|<|<|<|
|<|Découverte des clusters VMware.|objet JSON|**url** - URL du service VMware|<|
|vmware.cluster.status\[<url>, <name>\]|<|<|<|<|
|<|Statut du cluster VMware.|Entier :<br>0 - gris ;<br>1 - vert ;<br>2 - jaune ;<br>3 - rouge|**url** - URL du service VMware<br>**name** - nom du cluster VMware|<|
|vmware.eventlog\[<url>\]|<|<|<|<|
|<|VMware event log.|Journal|**url** - URL du service VMware|<|
|vmware.fullname\[<url>\]|<|<|<|<|
|<|Nom complet du service VMware.|Chaîne|**url** - URL du service VMware|<|
|vmware.hv.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nom du cluster de l'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|Utilisation du processeur de l'hyperviseur VMware (Hz).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nom du datacenter de l'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.datastore.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Découverte des banques de données de l'hyperviseur VMware.|objet JSON|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.datastore.read\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Durée moyenne d'une opération de lecture dans le magasin de données (en millisecondes).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware<br>**datastore** - nom du datastore<br>**mode** - latence (par défaut)|<|
|vmware.hv.datastore.size\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Espace de stockage de données VMware en octets ou en pourcentage du total.|Entier - pour les octets<br>Flottant - pour le pourcentage|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware<br>**datastore** - nom du datastore<br>**mode** - valeurs possibles :<br>total (par défaut), free, pfree (libre en pourcentage), uncommitted|Disponible depuis les versions Zabbix 3.0.6, 3.2.2|
|vmware.hv.datastore.write\[<url>,<uuid>,<datastore>,<mode>\]|<|<|<|<|
|<|Durée moyenne d'une opération d'écriture dans le magasin de données (en millisecondes).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware<br>**datastore** - nom du datastore<br>**mode** - latence (par défaut)|<|
|vmware.hv.discovery\[<url>\]|<|<|<|<|
|<|Découverte des hyperviseurs VMware.|objet JSON|**url** - URL du service VMware|<|
|vmware.hv.fullname\[<url>,<uuid>\]|<|<|<|<|
|<|Nom de l'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.cpu.freq\[<url>,<uuid>\]|<|<|<|<|
|<|Fréquence du processeur de l'hyperviseur VMware (Hz).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.cpu.model\[<url>,<uuid>\]|<|<|<|<|
|<|Modèle de processeur d'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|Nombre de cœurs de processeur sur l'hyperviseur VMware.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.cpu.threads\[<url>,<uuid>\]|<|<|<|<|
|<|Nombre de threads de processeur sur l'hyperviseur VMware.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.memory\[<url>,<uuid>\]|<|<|<|<|
|<|Taille totale de la mémoire de l'hyperviseur VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.model\[<url>,<uuid>\]|<|<|<|<|
|<|Taille totale de la mémoire de l'hyperviseur VMware (octets).|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.uuid\[<url>,<uuid>\]|<|<|<|<|
|<|UUID du BIOS de l'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.hw.vendor\[<url>,<uuid>\]|<|<|<|<|
|<|Nom du fournisseur de l'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|Taille de la mémoire ballooned (octets) de l'hyperviseur VMware.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.memory.used\[<url>,<uuid>\]|<|<|<|<|
|<|Taille de la mémoire (octets) utilisée par l’hyperviseur VMWare.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.network.in\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|Statistiques d'entrée du réseau de l’hyperviseur VMware (octets par seconde).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware<br>**mode** - octets par seconde (par défaut)|<|
|vmware.hv.network.out\[<url>,<uuid>,<mode>\]|<|<|<|<|
|<|Statistiques de sortie du réseau hyperviseur VMware (octets par seconde).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware<br>**mode** - octets par seconde (par défaut)|<|
|vmware.hv.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|Valeur du compteur de performance de l'hyperviseur VMware.|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware<br>**path** - chemin du compteur de performance ^**[1](vmware_keys#footnotes)**^<br>**instance** - instance du compteur de performance. Utiliser une instance vide pour les valeurs agrégées (par défaut)|Disponible depuis les versions Zabbix 2.2.9, 2.4.4|
|vmware.hv.sensor.health.state\[<url>,<uuid>\]|<|<|<|<|
|<|Capteur de cumul d'état d'intégrité de l'hyperviseur VMware.|Entier :<br>0 - gris ;<br>1 - vert ;<br>2 - jaune ;<br>3 - rouge|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|Disponible depuis Zabbix 2.2.16, 3.0.6, 3.2.2|
|vmware.hv.status\[<url>,<uuid>\]|<|<|<|<|
|<|Statut de l'hyperviseur VMware.|Entier :<br>0 - gris ;<br>1 - vert ;<br>2 - jaune ;<br>3 - rouge|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|Utilise la propriété de statut général du système hôte depuis Zabbix 2.2.16, 3.0.6, 3.2.2|
|vmware.hv.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|Durée de fonctionnement de l'hyperviseur VMware (secondes).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.version\[<url>,<uuid>\]|<|<|<|<|
|<|Version de l'hyperviseur VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.hv.vm.num\[<url>,<uuid>\]|<|<|<|<|
|<|Nombre de machines virtuelles sur l'hyperviseur VMware.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de l'hyperviseur VMware|<|
|vmware.version\[<url>\]|<|<|<|<|
|<|VMware service version.|Chaîne|**url** - URL du service VMware|<|
|vmware.vm.cluster.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nom de la machine virtuelle VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.cpu.num\[<url>,<uuid>\]|<|<|<|<|
|<|Nombre de processeurs sur la machine virtuelle VMware,|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.cpu.ready\[<url>,<uuid>\]|<|<|<|<|
|<|Temps (en millisecondes) que la machine virtuelle est prête, mais ne peut pas être planifiée pour s'exécuter sur le processeur physique. Le temps de disponibilité du processeur dépend du nombre de machines virtuelles sur l'hôte et de leurs charges de processeur (%).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|Disponible depuis Zabbix version 3.0.0|
|vmware.vm.cpu.usage\[<url>,<uuid>\]|<|<|<|<|
|<|Utilisation du processeur de la machine virtuelle VMware (Hz).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.datacenter.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nom du datastore de machine virtuelle VMware|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.discovery\[<url>\]|<|<|<|<|
|<|Découverte des machines virtuelles VMware.|objet JSON|**url** - URL du service VMware|<|
|vmware.vm.hv.name\[<url>,<uuid>\]|<|<|<|<|
|<|Nom de l'hyperviseur de machine virtuelle VMware.|Chaîne|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size\[<url>,<uuid>\]|<|<|<|<|
|<|Taille totale de la mémoire de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.ballooned\[<url>,<uuid>\]|<|<|<|<|
|<|Taille de la mémoire (octets) ballooned de la machine virtuelle VMware.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.compressed\[<url>,<uuid>\]|<|<|<|<|
|<|Taille de la mémoire compressée de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.private\[<url>,<uuid>\]|<|<|<|<|
|<|Taille de la mémoire privée de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.shared\[<url>,<uuid>\]|<|<|<|<|
|<|Taille de la mémoire partagée de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.swapped\[<url>,<uuid>\]|<|<|<|<|
|<|La taille de la mémoire (octets) swap de la machine virtuelle VMware.|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.usage.guest\[<url>,<uuid>\]|<|<|<|<|
|<|Utilisation de la mémoire de l'invité de machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.memory.size.usage.host\[<url>,<uuid>\]|<|<|<|<|
|<|Utilisation de la mémoire de l'hôte de machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.net.if.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Découverte des interfaces réseau des machines virtuelles VMware.|objet JSON|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.net.if.in\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Statistiques en entrée de l'interface réseau de machine virtuelle VMware (octets/paquets par seconde).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware<br>**instance** - instance de l'interface réseau<br>**mode** - octets par seconde (par défaut)/paquets par seconde|<|
|vmware.vm.net.if.out\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Statistiques en sortie de l'interface réseau de machine virtuelle VMware (octets/paquets par seconde).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware<br>**instance** - instance de l'interface réseau<br>**mode** - octets par seconde (par défaut)/paquets par seconde|<|
|vmware.vm.perfcounter\[<url>,<uuid>,<path>,<instance>\]|<|<|<|<|
|<|Valeur du compteur de performances de la machine virtuelle VMware.|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware<br>**path** - chemin du compteur de performance ^**[1](vmware_keys#footnotes)**^<br>**instance** - instance du compteur de performance. Utilise une instance vide pour les valeurs agrégées (par défaut)|Disponible depuis les versions Zabbix 2.2.9, 2.4.4|
|vmware.vm.powerstate\[<url>,<uuid>\]|<|<|<|<|
|<|État d'alimentation de la machine virtuelle VMware,|Entier :<br>éteinte ;<br>1 - allumée ;<br>2 - suspendue|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.storage.committed\[<url>,<uuid>\]|<|<|<|<|
|<|Espace de stockage validé de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.storage.uncommitted\[<url>,<uuid>\]|<|<|<|<|
|<|Espace de stockage non validé de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.storage.unshared\[<url>,<uuid>\]|<|<|<|<|
|<|Espace de stockage non partagé de la machine virtuelle VMware (octets).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.uptime\[<url>,<uuid>\]|<|<|<|<|
|<|Durée de disponibilité de la machine virtuelle VMware (secondes).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.vfs.dev.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Découverte des unités de disque de machine virtuelle VMware.|objet JSON|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|<|
|vmware.vm.vfs.dev.read\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Statistiques de lecture sur les unités de disque de la machine virtuelle VMware (octets/opérations par seconde).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware<br>**instance** - instance de disque<br>**mode** - octets par seconde (par défaut)/opérations par seconde|<|
|vmware.vm.vfs.dev.write\[<url>,<uuid>,<instance>,<mode>\]|<|<|<|<|
|<|Statistiques d’écriture sur les unités de disque de machine virtuelle VMware (octets/opérations par seconde).|Entier ^**[2](vmware_keys#footnotes)**^|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware<br>**instance** - instance de disque<br>**mode** - octets par seconde (par défaut)/opérations par seconde|<|
|vmware.vm.vfs.fs.discovery\[<url>,<uuid>\]|<|<|<|<|
|<|Découverte des systèmes de fichiers de machine virtuelle VMware.|objet JSON|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware|VMware Tools doit être installé sur la machine virtuelle.|
|vmware.vm.vfs.fs.size\[<url>,<uuid>,<fsname>,<mode>\]|<|<|<|<|
|<|Statistiques du système de fichiers de la machine virtuelle VMware (octets/pourcentages).|Entier|**url** - URL du service VMware<br>**uuid** - Nom d'hôte de la machine virtuelle VMware<br>**fsname** - nom du système de fichier<br>**mode** - total/free/used/pfree/pused|VMware Tools doit être installé sur la machine virtuelle.|

[comment]: # ({/new-98337d09})


