[comment]: # translation:outdated

[comment]: # ({new-02a3a9ac})
# 16 Dependent items

[comment]: # ({/new-02a3a9ac})

[comment]: # ({new-4eb72974})
#### Overview

There are situations when one item gathers multiple metrics at a time or
it even makes more sense to collect related metrics simultaneously, for
example:

-   CPU utilization of individual cores
-   Incoming/outgoing/total network traffic

To allow for bulk metric collection and simultaneous use in several
related items, Zabbix supports dependent items. Dependent items use the
master item to collect their data simultaneously, in one query. A new
value for the master item automatically populates the values of the
dependent items.

Zabbix preprocessing options can be used to extract the part that is
needed for the dependent item from the master item data.

Preprocessing is managed by a `preprocessing manager` process, which has
been added in Zabbix 3.4, along with workers that perform the
preprocessing steps. All values (with or without preprocessing) from
different data gatherers pass through the preprocessing manager before
being added to the history cache. Socket-based IPC communication is used
between data gatherers (pollers, trappers, etc) and the preprocessing
process.

Only Zabbix server is performing preprocessing steps and is processing
dependent items.

Item of any type, even dependent item, can be set as master item.
Additional levels of dependent items can be used to extract smaller
parts from the value of an existing dependent item.

[comment]: # ({/new-4eb72974})

[comment]: # ({new-3e1d96f5})
##### Limitations

-   Only same host (template/discovery rule) dependencies are allowed
-   Maximum count of dependent items for one master item is limited to
    999
-   Maximum 3 dependency levels allowed
-   Dependent item with master item from template will be not exported
    to XML

[comment]: # ({/new-3e1d96f5})

[comment]: # ({new-3f705816})
#### Item configuration

A dependent item depends on its master item for data. That is why the
**master item** must be configured (or exist) first:

-   Go to: *Configuration* → *Hosts*
-   Click on *Items* in the row of the host
-   Click on *Create item*
-   Enter parameters of the item in the form

![](../../../../../assets/en/manual/config/items/itemtypes/master_item.png)

All mandatory input fields are marked with a red asterisk.

Click on *Add* to save the master item.

Then you can configure a **dependent item**.\
![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for dependent items are:

|   |   |
|---|---|
|*Type*|Select **Dependent item** here.|
|*Key*|Enter a key that will be used to recognize the item.|
|*Master item*|Select the master item. Master item value will be used to populate dependent item value.|
|*Type of information*|Select the type of information that will correspond the format of data that will be stored.|

You may use item value
[preprocessing](/manual/config/items/item#item_value_preprocessing) to
extract the required part of the master item value.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_preprocessing.png)

Without preprocessing, the dependent item value will be exactly the same
as the master item value.

Click on *Add* to save the dependent item.

A shortcut to creating a dependent item quicker is to use the wizard in
the item list:

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_item_wizard.png)

[comment]: # ({/new-3f705816})

[comment]: # ({new-8323a97a})
##### Display

In the item list dependent items are displayed with their master item
name as prefix.

![](../../../../../assets/en/manual/config/items/itemtypes/dependent_items.png){width="600"}

If a master item is deleted, so are all its dependent items.

[comment]: # ({/new-8323a97a})
