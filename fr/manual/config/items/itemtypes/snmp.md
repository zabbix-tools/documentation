[comment]: # translation:outdated

[comment]: # ({new-43646dd0})
# 2 Agent SNMP

[comment]: # ({/new-43646dd0})

[comment]: # ({new-f312fc02})
#### Aperçu

Vous pouvez utiliser la supervision SNMP sur des périphériques tels que
des imprimantes, des commutateurs réseau, des routeurs ou des onduleurs
qui sont généralement compatibles SNMP et sur lesquels il serait
impossible d'installer des systèmes d'exploitation complets et des
agents Zabbix.

Pour pouvoir récupérer les données fournies par les agents SNMP sur ces
périphériques, le serveur Zabbix doit être configuré initialement avec
le support SNMP.

Les vérifications SNMP sont effectuées uniquement par le protocole UDP.

Depuis Zabbix 2.2.3, les démons du serveur Zabbix et des proxys
interrogent les périphériques SNMP pour plusieurs valeurs dans une seule
requête. Cela affecte tous les types d'éléments SNMP (éléments SNMP
standard, éléments SNMP avec index dynamiques et découverte SNMP de bas
niveau) et rend le traitement SNMP beaucoup plus efficace. Veuillez
consulter la [section des détails
techniques](#internal_workings_of_bulk_processing) ci-dessous pour
savoir comment cela fonctionne en interne. Depuis Zabbix 2.4, il existe
également un paramètre "Utiliser les requêtes de masse" pour chaque
interface qui permet de désactiver les requêtes groupées pour les
périphériques qui ne peuvent pas les gérer correctement.

Depuis Zabbix 2.2.7 et Zabbix 2.4.2, les démons des serveurs Zabbix et
des proxys inscrivent dans les logs zabbix les lignes similaires à ce
qui suit s'ils reçoivent une réponse SNMP incorrecte : <code> SNMP
response from host "gateway" does not contain all of the requested
variable bindings </code> Bien qu'ils ne couvrent pas tous les cas
problématiques, ils sont utiles pour identifier les périphériques SNMP
individuels pour lesquels les requêtes de masse doivent être
désactivées. <code>

Depuis Zabbix 2.2, les démons du serveur Zabbix et des proxys utilisent
correctement le paramètre de configuration Timeout lors de l'exécution
des vérifications SNMP. De plus, les démons n'effectuent pas de
nouvelles tentatives après une seule requête SNMP infructueuse
(timeout/fausses informations d'identification). Auparavant, le délai
d'expiration par défaut de la bibliothèque SNMP et les valeurs de
nouvelle tentative (1 seconde et 5 tentatives respectivement) étaient
réellement utilisés.

Depuis Zabbix 2.2.8 et Zabbix 2.4.2, les démons du serveur Zabbix et des
proxy vont toujours réessayer au moins une fois : soit via le mécanisme
de nouvelle tentative de la bibliothèque SNMP, soit via le [mécanisme
interne de traitement des requêtes de
masse](#internal_workings_of_bulk_processing).

::: notewarning
 Si vous surveillez les périphériques SNMPv3,
assurez-vous que msgAuthoritarianEngineID (également appelé snmpEngineID
ou "Engine ID") n'est jamais partagé par deux périphériques. Selon la
[RFC 2571](http://www.ietf.org/rfc/rfc2571.txt) (section 3.1.1.1), il
doit être unique pour chaque périphérique. 
:::

[comment]: # ({/new-f312fc02})

[comment]: # ({new-cac544a8})
#### Configuration de la supervision SNMP

Pour démarrer la supervision d'un équipement par SNMP, les étapes
suivantes doivent être effectuées :

[comment]: # ({/new-cac544a8})

[comment]: # ({new-501ff4eb})
##### Étape 1

[Créer un hôte](/fr/manual/config/hosts/host) pour un équipement ayant
une interface SNMP.

Entrez l'adresse IP. Vous pouvez utiliser l'un des modèles SNMP fournis
(*Modèle de périphérique SNMP* et autres) pour ajouter automatiquement
un ensemble d'éléments. Cependant, le modèle peut ne pas être compatible
avec l'hôte. Cliquez sur *Ajouter* pour enregistrer l'hôte.

::: notetip
Les vérifications SNMP n'utilisent pas le *port de
l'agent*, il est ignorée. 
:::

[comment]: # ({/new-501ff4eb})

[comment]: # ({new-929235a8})
##### Étape 2

Trouvez la chaîne SNMP (ou l'OID) de l'élément que vous souhaitez
surveiller.

Pour obtenir une liste des chaînes SNMP, utilisez la commande
**snmpwalk** (partie du logiciel [net-snmp](http://www.net-snmp.org/)
que vous avez dû installer dans le cadre de l'installation de Zabbix) ou
un outil équivalent :

    shell> snmpwalk -v 2c -c public <host IP> .

Comme '2c' correspond à la version SNMP, vous pouvez également le
remplacer par '1' pour indiquer SNMP Version 1 sur le périphérique.

Cela devrait vous donner une liste de chaînes SNMP et leur dernière
valeur. Si ce n'est pas le cas, il est possible que la 'communauté' SNMP
soit différente de la communauté standard 'public', auquel cas vous avez
besoin de savoir de quelle communauté il s'agit. Vous pourrez ensuite
parcourir la liste jusqu'à trouver la chaîne que vous voulez superviser,
par exemple si vous voulez superviser les octets arrivant à votre
commutateur sur le port 3, vous utiliserez la chaîne
`IF-MIB::ifInOctets.3` partir de cette ligne :

    IF-MIB::ifInOctets.3 = Counter32: 3409739121

Vous pouvez maintenant utiliser la commande **snmpget** pour trouver
l'OID numérique pour 'IF-MIB::ifInOctets.3' :

shell> snmpget -v 2c -c public -On 10.62.1.22 IF-MIB::ifInOctets.3

Notez que le dernier numéro de la chaîne est le numéro de port que vous
cherchez à superviser. Voir aussi : [Index
dynamiques](/manual/config/items/itemtypes/snmp/dynamicindex).

Cela devrait vous donner quelque chose comme :

    .1.3.6.1.2.1.2.2.1.10.3 = Counter32: 3472126941

Encore une fois, le dernier numéro de l'OID est le numéro de port.

::: noteclassic
3COM semble utiliser des numéros de port par centaines, par
exemple, port 1 = port 101, port 3 = port 103, mais Cisco utilise des
numéros normaux, par exemple le port 3 = 3. 
:::

Dans le dernier exemple ci-dessus, le type de valeur est "Counter32",
qui correspond en interne au type ASN\_COUNTER. La liste complète des
types pris en charge est ASN\_COUNTER, ASN\_COUNTER64, ASN\_UINTEGER,
ASN\_UNSIGNED64, ASN\_INTEGER, ASN\_FLOAT, ASN\_DOUBLE, ASN\_TIMETICKS,
ASN\_GAUGE, ASN\_IPADDRESS, ASN\_OCTET\_STR et ASN\_OBJECT\_ID (depuis
2.2.8, 2.4.3). Ces types correspondent à peu près à "Counter32",
"Counter64", "UInteger32", "INTEGER", "Float", "Double", "Timeticks",
"Gauge32", "IpAddress", "OCTET STRING", "OBJECT IDENTIFIER" dans la
sortie **snmpget**, mais peut également être montré comme "STRING",
"Hex-STRING", "OID" et d'autres, selon la présence d'un indice
d'affichage.

[comment]: # ({/new-929235a8})

[comment]: # ({new-290ebad1})
##### Étape 3

Créer un élément pour la supervision.

Revenez maintenant à Zabbix et cliquez sur *Éléments* pour l'hôte SNMP
que vous avez créé précédemment. Selon que vous ayez utilisé un modèle
ou non lors de la création de votre hôte, vous disposez soit d'une liste
d'éléments SNMP associés à votre hôte, soit d'une liste vide. Nous
allons travailler sur l'hypothèse que vous allez créer vous-même
l'élément en utilisant les informations que vous venez de recueillir en
utilisant snmpwalk et snmpget, donc cliquez sur *Créer un élément*. Dans
le nouveau formulaire, entrez le 'Nom' de l'élément . Assurez-vous que
le champ 'Interface hôte' contient votre commutateur/routeur et
remplacez le champ 'Type' par "Agent SNMPv\*". Entrez la communauté
(généralement public) et entrez l'OID textuel ou numérique que vous avez
récupéré précédemment dans le champ 'OID SNMP', par exemple :
.1.3.6.1.2.1.2.2.1.10.3

Entrez le 'Port' SNMP comme 161 et la 'Clé' comme quelque chose de
significatif, par exemple SNMP-InOctets-Bps. Définissez le 'Type
d'information' sur Numeric (flottant) et l'étape de prétraitement sur
*Changement par seconde* (important, sinon vous obtiendrez des valeurs
cumulatives du périphérique SNMP au lieu de la dernière modification).
Choisissez un multiplicateur personnalisé si vous le souhaitez et entrez
un 'Intervalle d’actualisation' et une 'Période de stockage de
l'historique' si vous voulez qu'ils soient différents de la valeur par
défaut.\
![](../../../../../assets/en/manual/config/items/itemtypes/snmpv3_item.png)

Maintenant, enregistrez l'élément et allez dans Surveillance → Dernières
données pour vos données SNMP ! Prenez note des options spécifiques
disponibles pour les éléments SNMPv3 :

Tous les champs obligatoires sont marqués par un astérisque rouge.

Maintenant, enregistrez l'élément et allez dans *Surveillance* →
*Dernières données* pour voir vos données SNMP !

Prenez connaissance des options spécifiques disponibles pour les
éléments SNMPv3 :

|Paramètreer|Description|
|------------|-----------|
|*Nom de contexte*|Entrez le nom du contexte pour identifier l'élément sur le sous-réseau SNMP. \\\\Le *nom de contexte* est pris en charge pour les éléments SNMPv3 depuis Zabbix 2.2. \\\\Les macros utilisateur sont résolues dans ce champ|
|*Nom de la sécurité*|Entrez le nom de sécurité.\\\\Les macros utilisateur sont résolues dans ce champ.|
|*Niveau de la sécurité*|Sélectionnez le niveau de sécurité :<br>**noAuthNoPriv** - aucune authentification ni aucun protocole de confidentialité ne sont utilisés<br>**AuthNoPriv** - le protocole d'authentification est utilisé, le protocole de confidentialité ne l'est pas<br>**AuthPriv** - les protocoles d'authentification et de confidentialité sont utilisés|
|*Protocole d'authentification*|Sélectionnez le protocole d' authentication - *MD5* ou *SHA*.|
|*Phrase de passe d'authentification*|Entrez la phrase de passe d'authentication.<br>Les macros utilisateurs sont résolues dans ce champs.|
|*Protocole de confidentialité*|Sélectionnez le protocole de confidentialité - *DES* ou *AES*.|
|*Phrase de passe de confidentialité*|Entrez phrase de passe de confidentialité.<br>Les macros utilisateurs sont résolues dans ce champs.|

En cas d'informations d'identification SNMPv3 incorrectes (nom de la
sécurité, protocole/phrase de passe d'authentification, protocole de
confidentialité), Zabbix reçoit une erreur ERROR de net-snmp, à
l'exception de la *phrase de passe de confidentialité*, auquel cas
Zabbix reçoit une erreur TIMEOUT de net-snmp.

::: notewarning
 Le redémarrage du serveur/proxy est requis pour que
les modifications du *protocole d'authentification*, de la *phrase de
passe d'authentification*, du *protocole de confidentialité* ou du
*protocole de confidentialité* prennent effet.
:::

[comment]: # ({/new-290ebad1})

[comment]: # ({new-6021e1bd})
##### Exemple 1

Exemple générale :

|Paramètre|Description|
|----------|-----------|
|**Communauté**|public|
|**OID**|1.2.3.45.6.7.8.0 (or .1.2.3.45.6.7.8.0)|
|**Clé**|<Chaîne unique à utiliser comme référence aux triggers><br>Par exemple, "my\_param".|

Notez que l'OID peut être donné sous forme numérique ou de chaîne.
Cependant, dans certains cas, l'OID de chaîne doit être converti en
représentation numérique. L’utilitaire snmpget peut être utilisé à cette
fin :

    shell> snmpget -On localhost public enterprises.ucdavis.memory.memTotalSwap.0

La supervision des paramètres SNMP est possible si l'option
--with-net-snmp a été spécifiée lors de la configuration des sources
Zabbix.

[comment]: # ({/new-6021e1bd})

[comment]: # ({new-45af5cf0})
##### Exemple 2

Supervision du temps de fonctionnement :

|Paramètre|Description|
|----------|-----------|
|**Communauté**|public|
|**Oid**|MIB::sysUpTime.0|
|**Clé**|router.uptime|
|**Type de valeur**|Float|
|**Unités**|uptime|
|**Multiplicateur personnalisé**|0.01|

[comment]: # ({/new-45af5cf0})

[comment]: # ({new-ea70430a})

#### Native SNMP bulk requests

The **walk[OID1,OID2,...]** item allows to use native SNMP functionality for bulk requests (GetBulkRequest-PDUs), available in SNMP versions 2/3. 

A GetBulk request in SNMP executes multiple GetNext requests and returns the result in a single response. This may be used for regular SNMP items as well as for SNMP discovery to minimize network roundtrips.

The SNMP **walk[OID1,OID2,...]** item may be used as the master item that collects data in one request with dependent ityems that parse the response as needed using preprocessing. 

Note that using native SNMP bulk requests is not related to the option of combining SNMP requests, which is Zabbix own way of combining multiple SNMP requests (see next section).

[comment]: # ({/new-ea70430a})

[comment]: # ({new-c57b8645})
#### Fonctionnement interne du traitement de masse

À partir de la version 2.2.3, le serveur et les proxys Zabbix
interrogent les périphériques SNMP avec plusieurs valeurs dans une seule
requête. Cela affecte plusieurs types d'éléments SNMP :

-   éléments SNMP standards ;
-   [éléments SNMP avec des index
    dynamiques](/fr/manual/config/items/itemtypes/snmp/dynamicindex);
-   [règles de découverte de bas niveau
    SNMP](/fr/manual/discovery/low_level_discovery/snmp_oids).

Tous les éléments SNMP sur une même interface avec des paramètres
identiques doivent être interrogés en même temps. Les deux premiers
types d'éléments sont effectués par les pollers par lots de 128 éléments
maximum, tandis que les règles de découverte de bas niveau sont traitées
individuellement, comme précédemment.

Au niveau inférieur, il existe deux types d'opérations effectuées pour
interroger des valeurs : obtenir plusieurs objets spécifiés et parcourir
une arborescence OID.

Pour "obtenir plusieurs objets spécifiés", une GetRequest-PDU est
utilisée avec 128 liaisons de variables maximum. Pour "parcourir une
arborescence OID", une unité GetNextRequest-PDU est utilisée pour SNMPv1
et GetBulkRequest avec un champ "max-repetitions" d'au plus 128 est
utilisé pour SNMPv2 et SNMPv3.

Ainsi, les avantages du traitement de masse pour chaque type d'élément
SNMP sont décrits ci-dessous :

-   Les éléments SNMP habituels bénéficient d'améliorations au niveau de
    "l'obtention plusieurs objets spécifiés" ;
-   Les éléments SNMP avec des index dynamiques bénéficient à la fois
    des améliorations "l'obtention plusieurs objets spécifiés" et "du
    parcours d'une arborescence OID" : le premier est utilisé pour la
    vérification d'index et le second pour construire le cache ;
-   Les règles de découverte de bas niveau SNMP bénéficient
    d'améliorations du "parcours d'une arborescence OID".

Cependant, il existe un problème technique : tous les périphériques ne
sont pas capables de renvoyer 128 valeurs par requête. Certains
retournent toujours une réponse correcte, mais d'autres répondent avec
une erreur "tooBig(1)" ou ne répondent pas du tout une fois que la
réponse potentielle dépasse une certaine limite.

Afin de trouver un nombre optimal d'objets à interroger pour un
périphérique donné, Zabbix utilise la stratégie suivante. Il commence
prudemment à interroger 1 valeur dans une requête. Si cela réussit, il
interroge 2 valeurs dans une requête. Si cela réussit à nouveau, il
interroge 3 valeurs dans une requête et continue de la même manière en
multipliant le nombre d'objets interrogés par 1,5, ce qui entraîne la
séquence suivante de tailles de requêtes : 1, 2, 3, 4, 6, 9, 13, 19 28,
42, 63, 94, 128.

Cependant, une fois qu'un périphérique refuse de donner une réponse
appropriée (par exemple, pour 42 variables), Zabbix fait deux choses.

D'abord, pour le lot actuel, il divise par deux le nombre d'objets dans
une seule requête et interroge 21 variables. Si le périphérique est
actif, la requête devrait fonctionner dans la grande majorité des cas,
car 28 variables fonctionnaient et 21 étaient significativement
inférieures. Cependant, si cela échoue encore, Zabbix revient à
interroger les valeurs une par une. Si cela échoue encore à ce stade, le
périphérique ne répond définitivement pas et la taille de la requête
n'est pas un problème.

La deuxième chose que fait Zabbix pour les objets groupés suivants est
qu'il commence avec le dernier nombre de variables réussi (28 dans notre
exemple) et continue d'incrémenter la taille des requêtes de 1 jusqu'à
ce que la limite soit atteinte. Par exemple, en supposant que la taille
de réponse la plus grande est de 32 variables, les demandes suivantes
seront de tailles 29, 30, 31, 32 et 33. La dernière requête échouera et
Zabbix n'émettra plus jamais de demande de taille 33. À partir de ce
moment, Zabbix interrogera au maximum 32 variables pour ce périphérique.

Si de grandes requêtes échouent avec ce nombre de variables, cela peut
signifier deux choses. Les critères exacts qu'un périphérique utilise
pour limiter la taille de la réponse ne peuvent pas être connus, mais
nous essayons de l'approximer en utilisant le nombre de variables. La
première possibilité est donc que ce nombre de variables se situe autour
de la limite réelle de la taille de la réponse du périphérique dans le
cas général : parfois la réponse est inférieure à la limite, parfois
elle est supérieure à celle-ci. La deuxième possibilité est qu'un paquet
UDP dans l'une ou l'autre direction soit simplement perdu. Pour ces
raisons, si Zabbix obtient une requête échouée, il réduit le nombre
maximum de variables pour essayer d'aller plus loin dans la plage
confortable du périphérique, mais (à partir de 2.2.8) seulement jusqu'à
deux fois.

Dans l'exemple ci-dessus, si une requête avec 32 variables échoue,
Zabbix réduit le nombre à 31. Si cela échoue, Zabbix réduit le nombre à
30. Cependant, Zabbix ne réduit pas le nombre en dessous de 30, car il
supposera que d'autres échecs sont dus à la perte de paquets UDP, plutôt
qu'à la limite du périphérique.

Si, toutefois, un périphérique ne peut pas gérer correctement les
demandes massives pour d'autres raisons et que l'heuristique décrite
ci-dessus ne fonctionne pas, Zabbix 2.4 dispose d'un paramètre "Utiliser
les requêtes de masse" pour chaque interface permettant de désactiver
les requêtes groupées pour ce périphérique.

[comment]: # ({/new-c57b8645})
