[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-be4ab03d})
# 5 Vérifications simples

[comment]: # ({/new-be4ab03d})

[comment]: # ({new-76023448})
#### Aperçu

Des vérifications simples sont normalement utilisées pour les
vérifications de services à distance sans agent.

Notez que l'agent Zabbix n'est pas nécessaire pour les vérifications
simples. Le serveur/proxy Zabbix est responsable du traitement des
vérifications simples (établir des connexions externes, etc.).

Exemples d'utilisation de vérifications simples :

    net.tcp.service[ftp,,155]
    net.tcp.service[http]
    net.tcp.service.perf[http,,8080]
    net.udp.service.perf[ntp]

::: noteclassic
Les champs *Nom d'utilisateur* et *Mot de passe* dans la
configuration les éléments de vérifications simples sont utilisés pour
les éléments de supervision VMware, ignoré autrement.
:::

[comment]: # ({/new-76023448})

[comment]: # ({new-f59a5ccc})
#### Vérifications simples supportées

Liste des vérifications simples supportées :

Voir également :

-   [Clés d'éléments de supervision
    VMware](/fr/manual/config/items/itemtypes/simple_checks/vmware_keys)

|Clé|<|<|<|<|
|----|-|-|-|-|
|<|**Description**|**Valeur de retour**|**Paramètres**|**Commentaires**|
|icmpping\[<target>,<packets>,<interval>,<size>,<timeout>\]|<|<|<|<|
|<|Accessibilité de l'hôte par ping ICMP.|0 - ping ICMP échoué<br><br>1 - ping ICMP réussi|**target** - IP ou nom DNS de l'hôte<br>**packets** - nombre de paquets<br>**interval** - temps entre paquets successifs en millisecondes<br>**size** - taille du paquet en octet<br>**timeout** - timeout en millisecondes|Exemple :<br>=> icmpping\[,4\] → si au moins un paquet des quatre est retourné, l'élément retournera 1.<br><br>Voir aussi : tableau des [valeurs par défaut](simple_checks#icmp_pings).|
|icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>\]|<|<|<|<|
|<|Pourcentage de paquets perdus.|Flottant.|**target** - IP ou nom DNS de l'hôte<br>**packets** - nombre de paquets<br>**interval** - temps entre paquets successifs en millisecondes<br>**size** - taille du paquet en octet<br>**timeout** - timeout en millisecondes|Voir aussi : tableau des [valeurs par défaut](simple_checks#icmp_pings).|
|icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>\]|<|<|<|<|
|<|Temps de réponse du ping ICMP (en secondes).|Flottant.|**target** - IP ou nom DNS de l'hôte<br>**packets** - nombre de paquets<br>**interval** - temps entre paquets successifs en millisecondes<br>**size** - taille du paquet en octet<br>**timeout** - timeout en millisecondes<br>**mode** - valeurs possibles : *min*, *max*, *avg* (par défaut)|Si l'hôte n'est pas disponible (délai d'expiration atteint), l'élément renverra 0.<br>Si la valeur de retour est inférieure à 0,0001 seconde, la valeur sera définie sur 0,0001 secondes.<br><br>Voir aussi : tableau des [valeurs par défaut](simple_checks#icmp_pings).|
|net.tcp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|Vérifie que le service est en cours d'exécution et accepte les connexion TCP.|0 - le service est arrêté<br><br>1 - le service est en cours d'exécution|**service** - valeurs possibles : *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (voir les [détails](/fr/manual/appendix/items/service_check_details))<br>**ip** - adresse IP ou nom DNS (par défaut l'IP ou DNS de l'hôte est utilisé)<br>**port** - numéro du port (par défaut le numéro de port standard est utilisé).|Exemple :<br>=> net.tcp.service\[ftp,,45\] → peut être utilisé pour tester la disponibilité du serveur FTP sur le port TCP 45.<br><br>Notez qu'avec le service *tcp*, le port est obligatoire.<br>Ces vérifications peuvent entraîner des messages supplémentaires dans les fichiers de log du démon système (les sessions SMTP et SSH étant généralement enregistrées).<br>La vérification des protocoles cryptés (comme IMAP sur le port 993 ou POP sur le port 995) n'est actuellement non supporté. Pour contourner ce problème, utilisez net.tcp.service\[tcp,<ip>,port\] pour les vérifications de ce type.<br>Les services *https* et *telnet* sont supporté depuis Zabbix 2.0.|
|net.tcp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Vérifie la performance du service TCP.|Flottant.<br><br>0.000000 - le service est arrêté<br><br>seconds - le nombre de secondes passées lors de la connexion au service|**service** - valeurs possibles : *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (voir les [détails](/fr/manual/appendix/items/service_check_details))<br>**ip** - adresse IP ou nom DNS (par défaut, l'IP ou le DNS de l'hôte est utilisé)<br>**port** - numéro du port (par défaut le numéro de port standard est utilisé).|Exemple :<br>=> net.tcp.service.perf\[ssh\] → peut être utilisé pour tester la vitesse de la réponse initiale du serveur SSH.<br><br>Notez qu'avec le service *tcp*, le port est obligatoire.<br>La vérification des protocoles cryptés (comme IMAP sur le port 993 ou POP sur le port 995) n'est actuellement non supporté. Pour contourner ce problème, utilisez net.tcp.service\[tcp,<ip>,port\] pour les vérifications de ce type.<br>Les services *https* et *telnet* sont supporté depuis Zabbix 2.0.<br>Appelé tcp\_perf avant Zabbix 2.0.|
|net.udp.service\[service,<ip>,<port>\]|<|<|<|<|
|<|Vérifie que le service est en cours d'exécution et répond aux requêtes UDP.|0 - le service est arrêté<br><br>1 - le service est en cours d'exécution|**service** - valeurs possibles : *ntp* (voir les [détails](/fr/manual/appendix/items/service_check_details))<br>**ip** - adresse IP ou nom DNS (par défaut, l'IP ou le DNS de l'hôte est utilisé)<br>**port** - numéro du port (par défaut le numéro de port standard est utilisé).|Exemple :<br>=> net.udp.service\[ntp,,45\] → peut être utilisé pour tester la disponibilité du service NTP sur le port UDP 45.<br><br>Cet élément est supporté depuis Zabbix 3.0, mais la vérification du service *ntp* était disponible avec la clé net.tcp.service\[\] dans les versions précédentes.|
|net.udp.service.perf\[service,<ip>,<port>\]|<|<|<|<|
|<|Vérifie la performance du service UDP.|Flottant.<br><br>0.000000 - le service est arrêté<br><br>seconds - le nombre de secondes passées lors de l'attente d'une réponse du service|**service** - valeurs possibles : *ntp* (voir les [détails](/fr/manual/appendix/items/service_check_details))<br>**ip** - adresse IP address ou nom DNS (par défaut, l'IP ou le DNS de l'hôte est utilisé)<br>**port** - numéro du port (par défaut le numéro de port standard est utilisé).|Exemple :<br>=> net.udp.service.perf\[ntp\] → peut être utilisé pour tester le temps de réponse du service NTP.<br><br>Cet élément est supporté depuis Zabbix 3.0, mais la vérification du service *ntp* était disponible avec la clé net.tcp.service\[\] dans les versions précédentes.|

[comment]: # ({/new-f59a5ccc})

[comment]: # ({new-23d7246f})

### Item key details

[comment]: # ({/new-23d7246f})

[comment]: # ({new-7d8dff2e})

##### icmpping\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmpping}

<br>
The host accessibility by ICMP ping.<br>
Return value: *0* - ICMP ping fails; *1* - ICMP ping successful.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

Example:

    icmpping[,4] #If at least one packet of the four is returned, the item will return 1.

[comment]: # ({/new-7d8dff2e})

[comment]: # ({new-6cd0f1b1})

##### icmppingloss\[<target>,<packets>,<interval>,<size>,<timeout>,<options>\] {#icmppingloss}

<br>
The percentage of lost packets.<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

See also the table of [default values](#default-values).

[comment]: # ({/new-6cd0f1b1})

[comment]: # ({new-b5636838})

##### icmppingsec\[<target>,<packets>,<interval>,<size>,<timeout>,<mode>,<options>\] {#icmppingsec}

<br>
The ICMP ping response time (in seconds).<br>
Return value: *Float*.

Parameters:

-   **target** - the host IP or DNS name;
-   **packets** - the number of packets;
-   **interval** - the time between successive packets in milliseconds;
-   **size** - the packet size in bytes;
-   **timeout** - the timeout in milliseconds;
-   **mode** - possible values: *min*, *max*, or *avg* (default);
-   **options** - used for allowing redirect: if empty (default value), redirected responses are treated as target host down; if set to *allow_redirect*, redirected responses are treated as target host up.

Comments:

-   Packets which are lost or timed out are not used in the calculation;
-   If the host is not available (timeout reached), the item will return 0;
-   If the return value is less than 0.0001 seconds, the value will be set to 0.0001 seconds;
-   See also the table of [default values](#default-values).

[comment]: # ({/new-b5636838})

[comment]: # ({new-b0a71170})

##### net.tcp.service[service,<ip>,<port>] {#nettcpservice}

<br>
Checks if a service is running and accepting TCP connections.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service[ftp,,45] #This item can be used to test the availability of FTP server on TCP port 45.

[comment]: # ({/new-b0a71170})

[comment]: # ({new-946385b4})

##### net.tcp.service.perf[service,<ip>,<port>] {#nettcpserviceperf}

<br>
Checks the performance of a TCP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent while connecting to the service.

Parameters:

-   **service** - possible values: *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Note that with *tcp* service indicating the port is mandatory;
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service[tcp,<ip>,port]` for checks like these.

Example:

    net.tcp.service.perf[ssh] #This item can be used to test the speed of initial response from SSH server.

[comment]: # ({/new-946385b4})

[comment]: # ({new-2859223a})

##### net.udp.service[service,<ip>,<port>] {#netudpservice}

<br>
Checks if a service is running and responding to UDP requests.<br>
Return value: *0* - the service is down; *1* - the service is running.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #This item can be used to test the availability of NTP service on UDP port 45.

[comment]: # ({/new-2859223a})

[comment]: # ({new-9cf5922c})

##### net.udp.service.perf[service,<ip>,<port>] {#netudpserviceperf}

<br>
Checks the performance of a UDP service.<br>
Return value: *Float*: *0.000000* - the service is down; *seconds* - the number of seconds spent waiting for response from the service.

Parameters:

-   **service** - possible values: *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address or DNS name (by default the host IP/DNS is used);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #This item can be used to test the response time from NTP service.

[comment]: # ({/new-9cf5922c})

[comment]: # ({new-85612a2b})
  
*Note* that for SourceIP support in LDAP simple checks, OpenLDAP version 2.6.1 or above is required.

[comment]: # ({/new-85612a2b})

[comment]: # ({new-92a6c4ae})
##### Traitement du délai d'attente

Zabbix ne traitera pas une vérification simple plus longtemps que le
délai d'attente (Timeout) définis en seconde dans le fichier de
configuration du serveur/proxy Zabbix.

[comment]: # ({/new-92a6c4ae})

[comment]: # ({new-b08a4508})
#### Pings ICMP

Zabbix utilise l'utilitaire externe **fping** pour le traitement des
pings ICMP.

L'utilitaire ne fait pas partie de la distribution Zabbix et doit être
installé en plus. Si l'utilitaire est manquant, si les autorisations
sont incorrectes ou si son emplacement ne correspond pas à l'emplacement
défini dans le fichier de configuration du serveur/proxy Zabbix
(paramètre 'FpingLocation'), les pings ICMP (**icmpping**,
**icmppingloss**, **icmppingsec**) ne seront pas traités.

Voir aussi : [problèmes
connus](/fr/manual/installation/known_issues#simple_checks)

**fping** doit être exécutable par l'utilisateur. Les démons Zabbix
s'exécutent sous root. Exécutez ces commandes en tant qu'utilisateur
root afin de configurer les autorisations correctes :

    shell> chown root:zabbix /usr/sbin/fping
    shell> chmod 4710 /usr/sbin/fping

Après avoir exécuté les deux commandes ci-dessus, vérifiez les
propriétés de l'exécutable **fping**. Dans certains cas, la propriété
peut être réinitialisée en exécutant la commande chmod.

Vérifiez également, si l'utilisateur zabbix appartient au groupe zabbix
en exécutant :

    shell> groups zabbix

et si ce n'est pas le cas, l’ajouter en exécutant :

    shell> usermod -a -G zabbix zabbix

Valeurs par défaut, limites et description des valeurs pour les
paramètres de vérifications ICMP :

|Paramètre|Unités|Description|balise Fping|Defaults set by|<|Limites autorisées<br>par Zabbix|<|
|----------|-------|-----------|------------|---------------|-|---------------------------------|-|

::: notewarning
Avertissement : les valeurs par défaut de fping
peuvent varier selon la plate-forme et la version - en cas de doute,
consultez la documentation de fping.
:::

Zabbix écrit les adresses IP à vérifier par l'une des trois clés
*icmpping\** dans un fichier temporaire, qui est ensuite transmis à
**fping**. Si les éléments ont des paramètres de clés différents, seuls
ceux avec des paramètres de clés identiques sont écrits dans un seul
fichier. \\\\Toutes les adresses IP écrites dans le fichier unique
seront vérifiées par fping en parallèle, donc le processus Zabbix icmp
pinger passera une durée fixe sans tenir compte du nombre d'adresses IP
dans le fichier.

[comment]: # ({/new-b08a4508})

[comment]: # ({new-e3dd9826})

##### Installation

fping is not included with Zabbix and needs to be installed separately:

- Various Unix-based platforms have the fping package in their default repositories, but it is not pre-installed. In this case you can use the package manager to install fping.

- Zabbix provides [fping packages](http://repo.zabbix.com/non-supported/rhel/) for RHEL. Please note that these packages are provided without official support.

- fping can also be compiled [from source](https://github.com/schweikert/fping/blob/develop/README.md#installation).

[comment]: # ({/new-e3dd9826})

[comment]: # ({new-9ced6345})

##### Configuration

Specify fping location in the *[FpingLocation](/manual/appendix/config/zabbix_server#fpinglocation)* parameter of Zabbix server/proxy configuration file 
(or *[Fping6Location](/manual/appendix/config/zabbix_server#fping6location)* parameter for using IPv6 addresses).

fping should be executable by the user Zabbix server/proxy run as and this user should have sufficient rights.

See also: [Known issues](/manual/installation/known_issues#simple_checks) for processing simple checks with fping versions below 3.10.

[comment]: # ({/new-9ced6345})

[comment]: # ({new-3c21487d})

##### Default values

Defaults, limits and description of values for ICMP check parameters:

|Parameter|Unit|Description|Fping's flag|Defaults set by|<|Allowed limits<br>by Zabbix|<|
|--|--|--------|-|--|--|--|--|
|||||**fping**|**Zabbix**|**min**|**max**|
|packets|number|number of request packets sent to a target|-C||3|1|10000|
|interval|milliseconds|time to wait between successive packets to an individual target|-p|1000||20|unlimited|
|size|bytes|packet size in bytes<br>56 bytes on x86, 68 bytes on x86_64|-b|56 or 68||24|65507|
|timeout|milliseconds|**fping v3.x** - timeout to wait after last packet sent, affected by *-C* flag<br> **fping v4.x** - individual timeout for each packet|-t|**fping v3.x** - 500<br>**fping v4.x** and newer - inherited from *-p* flag, but not more than 2000||50|unlimited|

The defaults may differ slightly depending on the platform and version.

In addition, Zabbix uses fping options *-i interval ms* (do not mix up with the item parameter *interval* mentioned in the table above,
which corresponds to fping option *-p*) and *-S source IP address* (or *-I* in older fping versions).
These options are auto-detected by running checks with different option combinations.
Zabbix tries to detect the minimal value in milliseconds that fping allows to use with *-i* by trying 3 values: 0, 1 and 10.
The value that first succeeds is then used for subsequent ICMP checks.
This process is done by each [ICMP pinger](/manual/concepts/server#server_process_types) process individually.

Auto-detected fping options are invalidated every hour and detected again on the next attempt to perform ICMP check.
Set [DebugLevel](/manual/appendix/config/zabbix_server#debuglevel)>=4 in order to view details of this process in the server or proxy log file.

Zabbix writes IP addresses to be checked by any of the three *icmpping\** keys to a temporary file, which is then passed to fping.
If items have different key parameters, only the ones with identical key parameters are written to a single file.
All IP addresses written to the single file will be checked by fping in parallel,
so Zabbix ICMP pinger process will spend fixed amount of time disregarding the number of IP addresses in the file.

[comment]: # ({/new-3c21487d})
