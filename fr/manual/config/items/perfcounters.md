[comment]: # translation:outdated

[comment]: # ({new-f89c7380})
# 6 Windows performance counters

[comment]: # ({/new-f89c7380})

[comment]: # ({new-47e1ae8b})
#### Overview

You can effectively monitor Windows performance counters using the
*perf\_counter\[\]* key.

For example:

    perf_counter["\Processor(0)\Interrupts/sec"]

or

    perf_counter["\Processor(0)\Interrupts/sec", 10]

For more information on using this key, see [Windows-specific item
keys](/fr/manual/config/items/itemtypes/zabbix_agent/win_keys).

In order to get a full list of performance counters available for
monitoring, you may run:

    typeperf -qx

[comment]: # ({/new-47e1ae8b})

[comment]: # ({new-38d80c32})
#### Numeric representation

As the naming of performance counters may differ on different Windows
servers, depending on local settings, it introduces a certain problem
when creating a template for monitoring several Windows machines having
different locales.

At the same time every performance counter can also be referred to by
its numeric form, which is unique and exactly the same regardless of
language settings, so you might use the numeric representation instead
of strings.

To find out the numeric equivalents, run **regedit**, then find
*HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Microsoft\\Windows
NT\\CurrentVersion\\Perflib\\009*.

The registry entry contains information like this:

    1
    1847
    2
    System
    4
    Memory
    6
    % Processor Time
    10
    File Read Operations/sec
    12
    File Write Operations/sec
    14
    File Control Operations/sec
    16
    File Read Bytes/sec
    18
    File Write Bytes/sec
    ....

Here you can find the corresponding numbers for each string part of the
performance counter, like in '\\System\\% Processor Time':

    System → 2
    % Processor Time → 6

Then you can use these numbers to represent the path in numbers:

    \2\6

[comment]: # ({/new-38d80c32})

[comment]: # ({new-96efda14})
#### Performance counter parameters

You can deploy some PerfCounter parameters for the monitoring of Windows
performance counters.

For example, you can add these to the Zabbix agent configuration file:

       PerfCounter=UserPerfCounter1,"\Memory\Page Reads/sec",30
       or
       PerfCounter=UserPerfCounter2,"\4\24",30

With such parameters in place, you can then simply use
*UserPerfCounter1* or *UserPerfCounter2* as the keys for creating the
respective items.

Remember to restart Zabbix agent after making changes to the
configuration file.

#### Troubleshooting

Sometimes Zabbix agent cannot retrieve performance counter values in
Windows 2000-based systems, because the pdh.dll file is outdated. It
shows up as failure messages in Zabbix agent and server log files. In
this case pdh.dll should be updated to a newer 5.0.2195.2668 version.

[comment]: # ({/new-96efda14})
