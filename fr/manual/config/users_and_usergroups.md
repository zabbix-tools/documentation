[comment]: # translation:outdated

[comment]: # ({new-9685d873})
# 11 Users and user groups

[comment]: # ({/new-9685d873})

[comment]: # ({new-07f1c89b})
#### Overview

All users in Zabbix access the Zabbix application through the web-based
frontend. Each user is assigned a unique login name and a password.

All user passwords are encrypted and stored in the Zabbix database.
Users cannot use their user id and password to log directly into the
UNIX server unless they have also been set up accordingly to UNIX.
Communication between the web server and the user browser can be
protected using SSL.

With a flexible [user permission
schema](/fr/manual/config/users_and_usergroups/permissions) you can
restrict and differentiate access to:

-   administrative Zabbix frontend functions
-   monitored hosts in hostgroups

The initial Zabbix installation has two predefined users - 'Admin' and
'guest'. The 'guest' user is used for unauthenticated users. Before you
log in as 'Admin', you are 'guest'. Proceed to [configuring a
user](/fr/manual/config/users_and_usergroups/user) in Zabbix.

[comment]: # ({/new-07f1c89b})
