[comment]: # translation:outdated

[comment]: # ({new-09d7d659})
# 1 Par certificat

[comment]: # ({/new-09d7d659})

[comment]: # ({new-685d1488})
#### Vue d'ensemble

Zabbix peut utiliser des certificats RSA au format PEM, signés par une
autorité de certification publique ou interne (CA). La vérification du
certificat est effectuée sur un certificat CA pré-configuré. Les
certificats auto-signés ne sont pas pris en charge. Les listes de
révocation de certificats (CRL) peuvent éventuellement être utilisées.
Chaque composant Zabbix ne peut avoir qu'un seul certificat configuré.

Pour plus d'informations sur la configuration et l'utilisation de
l'autorité de certification interne, la génération de demandes de
certificats et leur signature, la révocation de certificats, vous
trouverez en ligne en grand nombre de modes d'emploi, par exemple,
[Didacticiel OpenSSL PKI
v1.1](http://pki-tutorial.readthedocs.org/en/latest/).

Examinez attentivement et testez vos extensions de certificat - voir
[Limitations sur les extensions de certificat X.509
v3](/fr/manual/encryption/using_certificates#limitations_sur_les_extensions_de_certificat_x509_v3).

[comment]: # ({/new-685d1488})

[comment]: # ({new-a572d979})
#### Paramètres de configuration du certificat

|Paramètre|Obligatoire|Description|
|----------|-----------|-----------|
|*TLSCAFile*|\*|Chemin d'accès complet d'un fichier contenant les certificats de niveau supérieur de l'autorité de certification pour la vérification des certificats homologues.<br>En cas de chaîne de certificats avec plusieurs membres, ils doivent être ordonnés: certificats de CA de niveau inférieur en premier, suivi par des certificats de niveau supérieur.<br>Les certificats de plusieurs CA peuvent être inclus dans un seul fichier.|
|*TLSCRLFile*|<|Chemin d'accès complet d'un fichier contenant des listes de révocation de certificats. Voir [Listes de révocation de certificats (CRL)](/fr/manual/encryption/using_certificates#listes_de_revocation_de_certificats_crl).|
|*TLSCertFile*|\*|Chemin d'accès complet d'un fichier contenant un certificat (chaîne de certificats).<br>En cas de chaîne de certificats avec plusieurs membres, ils doivent être ordonnés: certificat de serveur, de proxy ou d'agent en premier, suivi des certificats de niveau inférieur puis des certificats de l'autorité de certification de niveau supérieur.|
|*TLSKeyFile*|\*|Chemin d'accès complet d'un fichier contenant une clé privée. Définissez les droits d’accès à ce fichier - il ne doit être lisible que par l’utilisateur de Zabbix.|
|*TLSServerCertIssuer*|<|Émetteur de certificat de serveur autorisé.|
|*TLSServerCertSubject*|<|Détenteur du certificat de serveur autorisé.|

[comment]: # ({/new-a572d979})

[comment]: # ({new-34bcf646})
#### Configuration du certificat sur le serveur Zabbix

1\. Afin de vérifier les certificats homologues, le serveur Zabbix doit
avoir accès aux fichiers avec leurs certificats d'autorité de
certification racine auto-signés de niveau supérieur. Par exemple, si
nous souhaitons des certificats de deux autorités de certification
racine indépendantes, nous pouvons placer leurs certificats dans un
fichier. `/home/zabbix/zabbix_ca_file` ainsi:

    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 1 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
            Issuer: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Root1 CA
                ...
            Subject: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Root1 CA
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                ...
            X509v3 extensions:
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Basic Constraints: critical
                    CA:TRUE
                ...
    -----BEGIN CERTIFICATE-----
    MIID2jCCAsKgAwIBAgIBATANBgkqhkiG9w0BAQUFADB+MRMwEQYKCZImiZPyLGQB
    ....
    9wEzdN8uTrqoyU78gi12npLj08LegRKjb5hFTVmO
    -----END CERTIFICATE-----
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 1 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
            Issuer: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Root2 CA
                ...
            Subject: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Root2 CA
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                ....
            X509v3 extensions:
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Basic Constraints: critical
                    CA:TRUE
                ....       
    -----BEGIN CERTIFICATE-----
    MIID3DCCAsSgAwIBAgIBATANBgkqhkiG9w0BAQUFADB/MRMwEQYKCZImiZPyLGQB
    ...
    vdGNYoSfvu41GQAR5Vj5FnRJRzv5XQOZ3B6894GY1zY=
    -----END CERTIFICATE-----

2\. Placer la chaîne de certificats du serveur Zabbix dans un fichier,
par exemple, `/home/zabbix/zabbix_server.crt`:

    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 1 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
            Issuer: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Signing CA
            ...
            Subject: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Zabbix server
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                    ...
            X509v3 extensions:
                X509v3 Key Usage: critical
                    Digital Signature, Key Encipherment
                X509v3 Basic Constraints: 
                    CA:FALSE
                ...
    -----BEGIN CERTIFICATE-----
    MIIECDCCAvCgAwIBAgIBATANBgkqhkiG9w0BAQUFADCBgTETMBEGCgmSJomT8ixk
    ...
    h02u1GHiy46GI+xfR3LsPwFKlkTaaLaL/6aaoQ==
    -----END CERTIFICATE-----
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 2 (0x2)
        Signature Algorithm: sha1WithRSAEncryption
            Issuer: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Root1 CA
            ...
            Subject: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Signing CA
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                ...
            X509v3 extensions:
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
            ...
    -----BEGIN CERTIFICATE-----
    MIID4TCCAsmgAwIBAgIBAjANBgkqhkiG9w0BAQUFADB+MRMwEQYKCZImiZPyLGQB
    ...
    dyCeWnvL7u5sd6ffo8iRny0QzbHKmQt/wUtcVIvWXdMIFJM0Hw==
    -----END CERTIFICATE-----

Ici, le premier est le certificat de serveur Zabbix, suivi du certificat
intermédiaire de l'autorité de certification.

3\. Placer la clé privée du serveur Zabbix dans un fichier, par exemple,
`/home/zabbix/zabbix_server.key`:

    -----BEGIN PRIVATE KEY-----
    MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQC9tIXIJoVnNXDl
    ...
    IJLkhbybBYEf47MLhffWa7XvZTY=
    -----END PRIVATE KEY-----

4\. Modifier les paramètres TLS dans le fichier de configuration du
serveur Zabbix comme suit:

    TLSCAFile=/home/zabbix/zabbix_ca_file
    TLSCertFile=/home/zabbix/zabbix_server.crt
    TLSKeyFile=/home/zabbix/zabbix_server.key

[comment]: # ({/new-34bcf646})

[comment]: # ({new-54ac87c7})
#### Configuration du chiffrement par certificat pour le proxy Zabbix

1\. Préparez les fichiers avec les certificats de l'autorité de
certification de niveau supérieur, le certificat proxy (chaîne) et la
clé privée, comme décrit dans [Configuration du certificat sur le
serveur
Zabbix](/fr/manual/encryption/using_certificates#configuration_du_certificat_sur_le_serveur_zabbix).
Modifier les paramètres `TLSCAFile`, `TLSCertFile`, `TLSKeyFile` dans la
configuration du proxy en conséquence.

2\. Pour un proxy actif modifier le paramètre `TLSConnect` :

    TLSConnect=cert

Pour un proxy passif modifier le paramètre `TLSAccept` :

    TLSAccept=cert

3\. Vous disposez maintenant d'une configuration de proxy minimale basée
sur un certificat. Vous pouvez si vous le souhaitez améliorer la
sécurité du proxy en définissant les paramètres `TLSServerCertIssuer` et
`TLSServerCertSubject` (voir [Restriction de l'émetteur et du détenteur
autorisés](/fr/manual/encryption/using_certificates#restriction_de_l_emetteur_et_du_sujet_autorises)).

4\. Dans le fichier final de configuration du proxy, les paramètres TLS
peuvent ressembler à ceci:

    TLSConnect=cert
    TLSAccept=cert
    TLSCAFile=/home/zabbix/zabbix_ca_file
    TLSServerCertIssuer=CN=Signing CA,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    TLSServerCertSubject=CN=Zabbix server,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    TLSCertFile=/home/zabbix/zabbix_proxy.crt
    TLSKeyFile=/home/zabbix/zabbix_proxy.key

5\. Configurer le chiffrement pour ce proxy dans l'interface Zabbix:

-   Aller dans : *Administration → Proxys*
-   Sélectionner un proxy et cliquer sur l'onglet **Chiffrement**

Dans les exemples ci-dessous, les champs Délivré par et Sujet sont
remplis - voir dans [Restriction de l'émetteur et du sujet
autorisés](/fr/manual/encryption/using_certificates#restriction_de_l_emetteur_et_du_sujet_autorises)
pourquoi et comment utiliser ces champs.

Pour un proxy actif

![proxy\_active\_cert.png](../../../assets/fr/manual/encryption/proxy_active_cert.png)

Pour un proxy passif

![proxy\_passive\_cert.png](../../../assets/fr/manual/encryption/proxy_passive_cert.png)

[comment]: # ({/new-54ac87c7})

[comment]: # ({new-d2f8a033})
#### Configuration du chiffrement par certificat pour l'agent Zabbix

1\. Préparer les fichiers avec les certificats de l'autorité de
certification de niveau supérieur, le certificat agent (chaîne) et la
clé privée, comme décrit dans [Configuration du certificat sur le
serveur
Zabbix](/fr/manual/encryption/using_certificates#configuration_du_certificat_sur_le_serveur_zabbix).
Modifier les paramètres `TLSCAFile`, `TLSCertFile`, `TLSKeyFile` dans la
configuration de l'agent en conséquence.

2\. Pour les surveillances actives, modifier le paramètre `TLSConnect`:

    TLSConnect=cert

Pour les surveillances passives, modifier le paramètre`TLSAccept`:

    TLSAccept=cert

3\. Vous disposez maintenant d'une configuration minimale agent basée
sur certificat. Vous pouvez si vous le souhaitez améliorer la sécurité
des agents en définissant les paramètres `TLSServerCertIssuer` et
`TLSServerCertSubject`. (voir [Restriction de l'émetteur et du sujet
autorisés](/fr/manual/encryption/using_certificates#restriction_de_l_emetteur_et_du_sujet_autorises)).

4\. Dans le fichier final de configuration de l'agent, les paramètres
TLS peuvent ressembler à ceci::

    TLSConnect=cert
    TLSAccept=cert
    TLSCAFile=/home/zabbix/zabbix_ca_file
    TLSServerCertIssuer=CN=Signing CA,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    TLSServerCertSubject=CN=Zabbix proxy,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    TLSCertFile=/home/zabbix/zabbix_agentd.crt
    TLSKeyFile=/home/zabbix/zabbix_agentd.key

(L'exemple suppose que l'hôte est surveillé via un proxy, donc un sujet
de certificat proxy.)

5\. Configurer le chiffrement pour cet agent dans l'interface Zabbix:

-   Aller dans : *Administration → Hôtes*
-   Sélectionner l'agent et cliquer sur l'onglet **Chiffrement**

Dans l'exemple ci-dessous, les champs Délivré par et Sujet sont remplis
- voir dans [Restriction de l'émetteur et du sujet
autorisés](/fr/manual/encryption/using_certificates#restriction_de_l_emetteur_et_du_sujet_autorises)
pourquoi et comment utiliser ces champs.

![agent\_config.png](../../../assets/fr/manual/encryption/agent_config.png)

[comment]: # ({/new-d2f8a033})

[comment]: # ({new-0f34d758})
#### Restriction de l'émetteur et du sujet autorisés

Lorsque deux composants (par exemple serveur Zabbix et agent)
établissent une connexion TLS, ils vérifient chacun les certificats de
l'autre. Si un certificat homologue est signé par une autorité de
certification de confiance (avec un certificat de niveau supérieur
préconfiguré dans «TLSCAFile»), est valide, n'a pas expiré et passe
d'autres vérifications, alors la communication peut continuer.
L'émetteur et le sujet du certificat ne sont pas vérifiés dans ce cas le
plus simple.

Il existe un risque: toute personne possédant un certificat valide peut
usurper l'identité de quelqu'un d'autre (par exemple, un certificat
d'hôte peut être utilisé pour emprunter l'identité d'un serveur). Cela
peut être acceptable dans les petits environnements où les certificats
sont signés par une autorité de certification interne dédiée et où le
risque de personnification est faible.

Si votre autorité de certification de niveau supérieur est utilisée pour
émettre d'autres certificats qui ne doivent pas être acceptés par Zabbix
ou si vous souhaitez réduire le risque d'usurpation d'identité, vous
pouvez restreindre les certificats autorisés en spécifiant leurs chaînes
d'émetteur et de sujet.

Par exemple, vous pouvez écrire dans le fichier de configuration du
proxy Zabbix:

    TLSServerCertIssuer=CN=Signing CA,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    TLSServerCertSubject=CN=Zabbix server,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com

Avec ces paramètres, un proxy actif ne communiquera pas avec le serveur
Zabbix avec une chaîne émettrice ou une chaîne sujet différente dans le
certificat, un proxy passif n'acceptera pas les demandes de ce serveur.

Quelques notes sur la correspondance de chaîne émetteur ou sujet:

1.  Les chaînes émetteur et sujet sont vérifiées indépendamment. Les
    deux sont facultatives.
2.  Les caractères UTF-8 sont autorisés.
3.  Une chaîne non spécifiée signifie que toute chaîne est acceptée.
4.  Les chaînes sont comparées "telles quelles", elles doivent être
    exactement identiques pour correspondre.
5.  Les caractères génériques et les expressions rationnelles ne sont
    pas pris en charge dans la correspondance.
6.  Seules certaines exigences de la [RFC 4514 Lightweight Directory
    Access Protocol (LDAP): Représentation de chaîne des noms
    distinctifs](http://tools.ietf.org/html/rfc4514) sont implémentées:

```{=html}
<!-- -->
```
        - caractères d'échappement '"' (U+0022), '+' U+002B, ',' U+002C, ';' U+003B, '<' U+003C, '>' U+003E, '\' U+005C  n'importe où dans la chaîne.
        - caractères d'échappement (' ' U+0020) ou le signe dièse ('#' U+0023) au début de la chaîne.
        - caractères d'échappement (' ' U+0020) à la fin de la chaîne.
    - La correspondance échoue si un caractère nul (U+0000) est rencontré (La [[http://tools.ietf.org/html/rfc4514|RFC 4514]] le permet).
    - Les exigences de la [[http://tools.ietf.org/html/rfc4517| RFC 4517 Lightweight Directory Access Protocol (LDAP): Syntaxes et règles de correspondance]] et de la [[http://tools.ietf.org/html/rfc4518|RFC 4518 Lightweight Directory Access Protocol (LDAP): Préparation de chaînes internationalisée]] ne sont pas pris en charge en raison de la quantité de travail requise.

L'ordre des champs dans les chaînes Délivrer par et Sujet et le
formatage sont importants! Zabbix suit les recommandations de la [RFC
4514](http://tools.ietf.org/html/rfc4514) et utilise l'ordre "inverse"
des champs.

L'ordre inverse peut être illustré par l'exemple suivant:

    TLSServerCertIssuer=CN=Signing CA,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    TLSServerCertSubject=CN=Zabbix proxy,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com

Notez qu'il commence par le niveau bas (CN), passe au niveau
intermédiaire (OU, O) et termine par les champs de niveau supérieur
(DC).

*OpenSSL* affiche par défaut les champs Issuer et Subject du certificat
dans l'ordre "normal", en fonction des options supplémentaires
utilisées:

    $ openssl x509 -noout -in /home/zabbix/zabbix_proxy.crt -issuer -subject
    issuer= /DC=com/DC=zabbix/O=Zabbix SIA/OU=Development group/CN=Signing CA
    subject= /DC=com/DC=zabbix/O=Zabbix SIA/OU=Development group/CN=Zabbix proxy

    $ openssl x509 -noout -text -in /home/zabbix/zabbix_proxy.crt
    Certificate:
            ...
            Issuer: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Signing CA
        ...
            Subject: DC=com, DC=zabbix, O=Zabbix SIA, OU=Development group, CN=Zabbix proxy

Ici, les chaînes Issuer et Subject commencent par le niveau supérieur
(DC) et se terminent par un champ de bas niveau (CN), les espaces et les
séparateurs de champs dépendent des options utilisées. Aucune de ces
valeurs ne correspondra dans les champs Délivré et Sujet de Zabbix!

::: noteimportant
 Pour obtenir des chaînes Issuer et Subject
correctes utilisables dans Zabbix, exécutez OpenSSL avec les options
spéciales\
`-nameopt esc_2253,esc_ctrl,utf8,dump_nostr,dump_unknown,dump_der,sep_comma_plus,dn_rev,sname`:

:::

    $ openssl x509 -noout -issuer -subject \
            -nameopt esc_2253,esc_ctrl,utf8,dump_nostr,dump_unknown,dump_der,sep_comma_plus,dn_rev,sname \
            -in /home/zabbix/zabbix_proxy.crt
    issuer= CN=Signing CA,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com
    subject= CN=Zabbix proxy,OU=Development group,O=Zabbix SIA,DC=zabbix,DC=com

Maintenant, les champs de chaînes sont dans l'ordre inverse, les champs
sont séparés par des virgules, peuvent être utilisés dans les fichiers
de configuration et l'interface Zabbix.

[comment]: # ({/new-0f34d758})

[comment]: # ({new-29732466})
#### Limitations sur les extensions de certificat X.509 v3

-   Extension **Nom de sujet alternatif (*subjectAltName*)**.\
    Les noms de sujet alternatifs de l'extension *subjectAltName* (comme
    adresse IP, adresse e-mail) ne sont pas supportés par Zabbix. Seule
    la valeur du champ "Sujet" peut être vérifiée dans Zabbix (see
    [Restriction de l'émetteur et du sujet
    autorisés](/fr/manual/encryption/using_certificates#restriction_de_l_emetteur_et_du_sujet_autorises)).\
    Si un certificat utilise l'extension *subjectAltName* alors le
    résultat dépendra d'une combinaison particulière des kits d'outils
    cryptographiques avec lesquels les composants Zabbix sont compilés
    (cela peut fonctionner ou pas, Zabbix peut refuser d'accepter de
    tels certificats de ses pairs).
-   Extension **Utilisation de clé étendue**.\
    Si utilisé alors les deux paramètres *clientAuth* (Authentification
    du client WWW TLS) et *serverAuth* (Authentification du serveur WWW
    TLS) sont généralement nécessaires.\
    Par exemple, dans les contrôles passifs, l'agent Zabbix joue un rôle
    de serveur TLS, ainsi*serverAuth* doit être défini dans le
    certificat de l'agent. Pour les contrôles actifs, le certificat
    d'agent nécessite de définit *clientAuth*.\
    *GnuTLS* émet un avertissement en cas de violation de l'utilisation
    de la clé, mais autorise la communication.
-   Extension **Contraintes de nom**.\
    Toutes les boîtes à outils cryptographiques ne le prennent pas en
    charge. Cette extension peut empêcher Zabbix de charger des
    certificats CA pour lequels cette section est marquée comme
    *critical* (dépend du toolkit crypto ).

[comment]: # ({/new-29732466})

[comment]: # ({new-35b25767})
#### Listes de révocation de certificats (CRL)

Si un certificat est compromis, l'autorité de certification peut la
révoquer en l'incluant dans la liste de révocation de certificats. Les
listes de révocation de certificats peuvent être configurées dans le
fichier de configuration du serveur, du proxy et de l'agent à l'aide du
paramètre `TLSCRLFile`. Par exemple:

    TLSCRLFile=/home/zabbix/zabbix_crl_file

où `zabbix_crl_file` peut contenir des listes de révocation de
certificats de plusieurs autorités de certification et ressembler à:

    -----BEGIN X509 CRL-----
    MIIB/DCB5QIBATANBgkqhkiG9w0BAQUFADCBgTETMBEGCgmSJomT8ixkARkWA2Nv
    ...
    treZeUPjb7LSmZ3K2hpbZN7SoOZcAoHQ3GWd9npuctg=
    -----END X509 CRL-----
    -----BEGIN X509 CRL-----
    MIIB+TCB4gIBATANBgkqhkiG9w0BAQUFADB/MRMwEQYKCZImiZPyLGQBGRYDY29t
    ...
    CAEebS2CND3ShBedZ8YSil59O6JvaDP61lR5lNs=
    -----END X509 CRL-----

Le fichier CRL est chargé uniquement au démarrage de Zabbix. La mise à
jour de la liste de révocation de certificats nécessite un redémarrage.

::: noteimportant
 Si le composant Zabbix est compilé avec *OpenSSL*
et que des listes de révocation de certificats sont utilisées, chaque
autorité de certification de niveau supérieur et intermédiaire des
chaînes de certificats doit avoir une liste de révocation de certificats
(elle peut être vide) dans `TLSCRLFile`. 
:::

#### Limites d'utilisation des extensions CRL

-   Extension **Identifiant de la clé d'autorité**.\
    Les listes de révocation de certificats pour les autorités de
    certification avec des noms identiques peuvent ne pas fonctionner
    avec *mbedTLS* (*PolarSSL*), même avec l'extension "Identifiant de
    la clé d'autorité (Authority Key Identifier)".

[comment]: # ({/new-35b25767})
