[comment]: # translation:outdated

[comment]: # ({new-14d0ad91})
# 2 Par clés pré-partagées (PSK)

[comment]: # ({/new-14d0ad91})

[comment]: # ({new-3935bdee})
#### Vue d'ensemble

Chaque clé pré-partagée (PSK) de Zabbix est en fait une paire de:

-   chaîne d'identité PSK non secrète,
-   valeur de chaîne PSK secrète.

La chaîne d'identité PSK est une chaîne UTF-8 non vide. Par exemple,
"PSK ID 001 Zabbix agentd". C'est un nom unique par lequel ce composant
PSK spécifique est désigné par les composants Zabbix. Ne placez pas
d'informations sensibles dans la chaîne d'identité PSK - elles sont
transmises non cryptées sur le réseau.

La valeur PSK est une chaîne de caractères hexadécimaux difficile à
deviner, par exemple
"e560cb0d918d26d31b4f642181f5f570ad89a390931102e5391d08327ba434e9".

[comment]: # ({/new-3935bdee})

[comment]: # ({new-a75bbda5})
#### Limites de taille

Il existe des limites de taille pour l'identité et la valeur PSK dans
Zabbix, dans certains cas, une bibliothèque cryptographique peut avoir
une limite inférieure:

|Composant|Taille maximale de l'identité PSK|Taille minimale de la valeur PSK|Taille maximale de la valeur PSK|
|---------|----------------------------------|--------------------------------|--------------------------------|
|*Zabbix*|128 caractères UTF-8|128-bit (16-byte PSK, saisis comme 32 caractères hexadécimaux)|2048 bits (256 octets PSK, saisis comme 512 caractères hexadécimaux)|
|*GnuTLS*|128 octets (peut inclure des caractères UTF-8)|\-|2048 bits (256 octets PSK, saisis comme 512 caractères hexadécimaux)|
|*mbed TLS (PolarSSL)*|128 caractères UTF-8|\-|256 bits (limite par défaut) (32 octets PSK, saisis comme 64 caractères hexadécimaux)|
|*OpenSSL*|127 octets (peut inclure des caractères UTF-8)|\-|2048 bits (256 octets PSK, saisis comme 512 caractères hexadécimaux)|

::: noteimportant
 L'interface Zabbix permet de configurer une
chaîne d'identité PSK longue de 128 caractères et une longueur PSK
longue de 2048 bits quelles que soient les bibliothèques
cryptographiques utilisées.\
Si certains composants Zabbix supportent des limites inférieures, il est
de la responsabilité de l'utilisateur de configurer l'identité et la
valeur PSK avec la longueur autorisée pour ces composants.\
Le dépassement des limites de longueur entraîne des défaillances de
communication entre les composants Zabbix. 
:::

Avant que le serveur Zabbix se connecte à l'agent à l'aide de PSK, le
serveur recherche l'identité PSK et la valeur PSK configurées pour cet
agent dans la base de données (actuellement dans le cache de
configuration). À la réception d'une connexion, l'agent utilise
l'identité PSK et la valeur PSK de son fichier de configuration. Si les
deux parties ont la même chaîne d'identité PSK et la même valeur PSK, la
connexion peut réussir.

::: noteimportant
 Il est de la responsabilité de l'utilisateur de
s'assurer qu'il n'y a pas deux PSK ayant la même chaîne d'identité avec
des valeurs différentes. Ne pas le faire peut entraîner des
perturbations imprévisibles de la communication entre les composants
Zabbix utilisant des PSK avec cette chaîne d’identité PSK. 
:::

[comment]: # ({/new-a75bbda5})

[comment]: # ({new-3e166db9})
#### Génération de PSK

Par exemple, un PSK de 256 bits (32 octets) peut être généré à l'aide
des commandes suivantes:

-   avec *OpenSSL*:

```{=html}
<!-- -->
```
      $ openssl rand -hex 32
      af8ced32dfe8714e548694e2d29e1a14ba6fa13f216cb35c19d0feb1084b0429

-   avec *GnuTLS*:

```{=html}
<!-- -->
```
      $ psktool -u psk_identity -p database.psk -s 32
      Generating a random key for user 'psk_identity'
      Key stored to database.psk
      
      $ cat database.psk 
      psk_identity:9b8eafedfaae00cece62e85d5f4792c7d9c9bcc851b23216a1d300311cc4f7cb

Notez que la commande "psktool" ci-dessus génère un fichier de base de
données avec une identité PSK et son PSK associé. Zabbix ne prévoit
qu'un PSK dans le fichier PSK. Par conséquent, la chaîne d'identité et
les deux-points (':') doivent être supprimés du fichier.

[comment]: # ({/new-3e166db9})

[comment]: # ({new-0c5c40b9})
#### Configuration de PSK pour la communication serveur-agent (exemple)

Sur l'hôte de l'agent, écrire la valeur PSK dans un fichier, par
exemple, `/home/zabbix/zabbix_agentd.psk`. Le fichier doit contenir un
PSK dans la première chaîne de texte, par exemple:

    1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952

Définir les droits d’accès au fichier PSK - il ne doit être lisible que
par l’utilisateur Zabbix.

Modifier les paramètres TLS dans le fichier de configuration de l'agent
`zabbix_agentd.conf`, par exemple, définir:

    TLSConnect=psk
    TLSAccept=psk
    TLSPSKFile=/home/zabbix/zabbix_agentd.psk
    TLSPSKIdentity=PSK 001

L'agent se connecte au serveur (contrôles actifs) et accepte à partir du
serveur et de la commande `zabbix_get` uniquement les connexions
utilisant PSK. L'identité PSK sera "PSK 001".

Redémarrer l'agent. Maintenant, vous pouvez tester la connexion en
utilisant la commande `zabbix_get`, par exemple:

    $ zabbix_get -s 127.0.0.1 -k "system.cpu.load[all,avg1]" --tls-connect=psk \
                --tls-psk-identity="PSK 001" --tls-psk-file=/home/zabbix/zabbix_agentd.psk

(Pour minimiser les temps d'interruption, modifier le type de connexion
comme décrit dans [Gestion du chiffrement de la
connexion](/fr/manual/encryption#gestion_du_chiffrement_de_la_connexion)).

Configurer le chiffrement PSK pour cet agent dans l'interface Zabbix:

-   Aller dans: *Configuration → Hôtes*
-   Sélectionner l'hôte et cliquer sur l'onglet **Chiffrement**

Exemple:

![psk\_config.png](../../../assets/fr/manual/encryption/psk_config.png)

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Dès lors que le cache de configuration est synchronisé avec la base de
données, les nouvelles connexions utilisent PSK. Vérifier les fichiers
journaux du serveur et de l'agent à la recherche de messages d'erreur.

[comment]: # ({/new-0c5c40b9})

[comment]: # ({new-2200dfcb})
#### Configuration de PSK pour le serveur - communication proxy active (exemple)

Sur le proxy, écrivez la valeur PSK dans un fichier, par exemple,
`/home/zabbix/zabbix_proxy.psk`. Le fichier doit contenir le PSK dans la
première chaîne de texte, par exemple:

    e560cb0d918d26d31b4f642181f5f570ad89a390931102e5391d08327ba434e9

Définir les droits d’accès au fichier PSK - il ne doit être lisible que
par l’utilisateur Zabbix.

Modifier les paramètres TLS dans le fichier de configuration de proxy
`zabbix_proxy.conf`, par exemple, définir:

    TLSConnect=psk
    TLSPSKFile=/home/zabbix/zabbix_proxy.psk
    TLSPSKIdentity=PSK 002

Le proxy se connecte au serveur en utilisant PSK. L'identité PSK sera
"PSK 002".

(Pour minimiser les temps d'interruption, modifier le type de connexion
comme décrit dans [Gestion du chiffrement de la
connexion](/fr/manual/encryption#gestion_du_chiffrement_de_la_connexion)).

Configurer PSK pour ce proxy dans l'interface Zabbix. Aller dans //
Administration→ Proxys //, sélectionner le proxy et aller dans l'onglet
"Chiffrement". Dans "Connexions du proxy", cocher `PSK`. Coller dans le
champ "Identité PSK" "PSK 002" et
"e560cb0d918d26d31b4f642181f5f570ad89a390931102e5391d08327ba434e9" dans
le champ "PSK". Cliquez sur "Actualiser".

Redémarrer le proxy. Il commencera à utiliser des connexions au serveur
chiffrées basées sur PSK. Vérifiez les fichiers journaux du serveur et
du proxy à la recherche de messages d'erreur.

Pour un proxy passif, la procédure est similaire. La seule différence
est - définir `TLSAccept=psk` dans le fichier de configuration du proxy
et définir "Connections du proxy" dans l'interface Zabbix à `PSK`.

[comment]: # ({/new-2200dfcb})
