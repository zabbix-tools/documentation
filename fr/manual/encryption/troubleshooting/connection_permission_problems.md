[comment]: # translation:outdated

[comment]: # ({new-fb73521a})
# 1 Problèmes de type de connexion ou d'autorisation

[comment]: # ({/new-fb73521a})

[comment]: # ({new-0b784e66})
#### Le serveur est configuré pour se connecter en PSK à l'agent, mais l'agent n'accepte que les connexions non cryptées

Dans le journal du serveur ou du proxy (avec *mbed TLS* -// PolarSSL //-
1.3.11)

    Get value from agent failed: ssl_handshake(): SSL - The connection indicated an EOF

Dans le journal du serveur ou du proxy (avec *GnuTLS* 3.3.16)

    Get value from agent failed: zbx_tls_connect(): gnutls_handshake() failed: \
        -110 The TLS connection was non-properly terminated.

Dans le journal du serveur ou du proxy (avec *OpenSSL* 1.0.2c)

    Get value from agent failed: TCP connection successful, cannot establish TLS to [[127.0.0.1]:10050]: \
        Connection closed by peer. Check allowed connection types and access rights

[comment]: # ({/new-0b784e66})

[comment]: # ({new-ed16b359})
#### Un côté se connecte avec un certificat mais l'autre côté n'accepte que le PSK ou vice versa

Dans n'importe quel journal (avec *mbed TLS* -*PolarSSL*- ):

    failed to accept an incoming connection: from 127.0.0.1: ssl_handshake():\
        SSL - The server has no ciphersuites in common with the client

Dans n'importe quel journal (avec *GnuTLS*):

    failed to accept an incoming connection: from 127.0.0.1: zbx_tls_accept(): gnutls_handshake() failed:\
        -21 Could not negotiate a supported cipher suite.

Dans n'importe quel journal (avec *OpenSSL* 1.0.2c):

    failed to accept an incoming connection: from 127.0.0.1: TLS handshake returned error code 1:\
        file .\ssl\s3_srvr.c line 1411: error:1408A0C1:SSL routines:ssl3_get_client_hello:no shared cipher:\
        TLS write fatal alert "handshake failure"

[comment]: # ({/new-ed16b359})

[comment]: # ({new-16bf8f0a})
#### Attempting to use Zabbix sender compiled with TLS support to send data to Zabbix server/proxy compiled without TLS

[comment]: # ({/new-16bf8f0a})

[comment]: # ({new-01fece69})
##### In connecting-side log:

Linux:

    ...In zbx_tls_init_child()
    ...OpenSSL library (version OpenSSL 1.1.1  11 Sep 2018) initialized
    ...
    ...In zbx_tls_connect(): psk_identity:"PSK test sender"
    ...End of zbx_tls_connect():FAIL error:'connection closed by peer'
    ...send value error: TCP successful, cannot establish TLS to [[localhost]:10051]: connection closed by peer

Windows:

    ...OpenSSL library (version OpenSSL 1.1.1a  20 Nov 2018) initialized
    ...
    ...In zbx_tls_connect(): psk_identity:"PSK test sender"
    ...zbx_psk_client_cb() requested PSK identity "PSK test sender"
    ...End of zbx_tls_connect():FAIL error:'SSL_connect() I/O error: [0x00000000] The operation completed successfully.'
    ...send value error: TCP successful, cannot establish TLS to [[192.168.1.2]:10051]: SSL_connect() I/O error: [0x00000000] The operation completed successfully.

[comment]: # ({/new-01fece69})

[comment]: # ({new-2cb90759})
##### In accepting-side log:

    ...failed to accept an incoming connection: from 127.0.0.1: support for TLS was not compiled in

[comment]: # ({/new-2cb90759})

[comment]: # ({new-aa226719})
#### One side connects with PSK but other side uses LibreSSL or has been compiled without encryption support

LibreSSL does not support PSK.

In connecting-side log:

    ...TCP successful, cannot establish TLS to [[192.168.1.2]:10050]: SSL_connect() I/O error: [0] Success

In accepting-side log:

    ...failed to accept an incoming connection: from 192.168.1.2: support for PSK was not compiled in

In Zabbix frontend:

    Get value from agent failed: TCP successful, cannot establish TLS to [[192.168.1.2]:10050]: SSL_connect() I/O error: [0] Success

[comment]: # ({/new-aa226719})

[comment]: # ({new-c03da918})
#### One side connects with PSK but other side uses OpenSSL with PSK support disabled

In connecting-side log:

    ...TCP successful, cannot establish TLS to [[192.168.1.2]:10050]: SSL_connect() set result code to SSL_ERROR_SSL: file ../ssl/record/rec_layer_s3.c line 1536: error:14094410:SSL routines:ssl3_read_bytes:sslv3 alert handshake failure: SSL alert number 40: TLS read fatal alert "handshake failure"

In accepting-side log:

    ...failed to accept an incoming connection: from 192.168.1.2: TLS handshake set result code to 1: file ssl/statem/statem_srvr.c line 1422: error:1417A0C1:SSL routines:tls_post_process_client_hello:no shared cipher: TLS write fatal alert "handshake failure"

[comment]: # ({/new-c03da918})
