[comment]: # translation:outdated

[comment]: # ({new-58c98254})
# 3 Problèmes PSK

[comment]: # ({/new-58c98254})

[comment]: # ({new-aebe0aa7})
#### PSK contient un nombre impair de chiffres hexadécimaux

Le proxy ou l'agent ne démarre pas, message dans le journal du proxy ou
de l'agent:

    invalid PSK in file "/home/zabbix/zabbix_proxy.psk"

[comment]: # ({/new-aebe0aa7})

[comment]: # ({new-887ee505})
#### Une chaîne d'identité PSK supérieure à 128 octets est transmise à GnuTLS

Dans le journal côté client TLS:

    gnutls_handshake() failed: -110 The TLS connection was non-properly terminated.

Dans le journal côté serveur TLS:

    gnutls_handshake() failed: -90 The SRP username supplied is illegal.

[comment]: # ({/new-887ee505})

[comment]: # ({new-a2e71eaa})
#### Le PSK de plus de 32 octets est transmis à mbed TLS (PolarSSL)

Dans n'importe quel journal Zabbix:

    ssl_set_psk(): SSL - Bad input parameters to function

[comment]: # ({/new-a2e71eaa})
