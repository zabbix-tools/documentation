[comment]: # translation:outdated

[comment]: # ({new-64bb0393})
# 2 Problèmes de certificat

[comment]: # ({/new-64bb0393})

[comment]: # ({new-123d3f4b})
#### OpenSSL est utilisé avec les listes de révocation de certificats et pour certaines autorités de certification dans la chaîne de certificats, sa liste de révocation de certificats n'est pas incluse dans le fichier `TLSCRLFile`

Dans le journal du serveur TLS pour *mbed TLS (PolarSSL)* et *OpenSSL*:

    failed to accept an incoming connection: from 127.0.0.1: TLS handshake with 127.0.0.1 returned error code 1: \
        file s3_srvr.c line 3251: error:14089086: SSL routines:ssl3_get_client_certificate:certificate verify failed: \
        TLS write fatal alert "unknown CA"

Dans le journal du serveur TLS pour *GnuTLS* :

    failed to accept an incoming connection: from 127.0.0.1: TLS handshake with 127.0.0.1 returned error code 1: \
        file rsa_pk1.c line 103: error:0407006A: rsa routines:RSA_padding_check_PKCS1_type_1:\
        block type is not 01 file rsa_eay.c line 705: error:04067072: rsa routines:RSA_EAY_PUBLIC_DECRYPT:paddin

[comment]: # ({/new-123d3f4b})

[comment]: # ({new-7be8a029})
#### La liste de révocation de certificats a expiré ou expire pendant le fonctionnement du serveur

[*OpenSSL*]{.underline}, dans le journal du serveur:

-   avant expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
        SSL_connect() returned SSL_ERROR_SSL: file s3_clnt.c line 1253: error:14090086:\
        SSL routines:ssl3_get_server_certificate:certificate verify failed:\
        TLS write fatal alert "certificate revoked"

-   après expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
        SSL_connect() returned SSL_ERROR_SSL: file s3_clnt.c line 1253: error:14090086:\
        SSL routines:ssl3_get_server_certificate:certificate verify failed:\
        TLS write fatal alert "certificate expired"

Dans ce cas précis, avec une liste de révocation de certificats valide,
un certificat révoqué est signalé en tant que "certificat révoqué".
Lorsque la liste de révocation de certificats expire, le message
d'erreur devient "certificat expiré", ce qui induit en erreur.

[*GnuTLS*]{.underline}, dans le journal du serveur:

-   avant et après expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
          invalid peer certificate: The certificate is NOT trusted. The certificate chain is revoked.

[*mbed TLS*]{.underline} ([*PolarSSL*]{.underline}), dans le journal du
serveur:

-   avant expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
        invalid peer certificate: revoked

-   après expiration:

```{=html}
<!-- -->
```
    cannot connect to proxy "proxy-openssl-1.0.1e": TCP successful, cannot establish TLS to [[127.0.0.1]:20004]:\
          invalid peer certificate: revoked, CRL expired

[comment]: # ({/new-7be8a029})

[comment]: # ({new-e3030ed0})
#### Self-signed certificate, unknown CA

[*OpenSSL*]{.underline}, in log:

    error:'self signed certificate: SSL_connect() set result code to SSL_ERROR_SSL: file ../ssl/statem/statem_clnt.c\
          line 1924: error:1416F086:SSL routines:tls_process_server_certificate:certificate verify failed:\
          TLS write fatal alert "unknown CA"'

This was observed when server certificate by mistake had the same Issuer
and Subject string, although it was signed by CA. Issuer and Subject are
equal in top-level CA certificate, but they cannot be equal in server
certificate. (The same applies to proxy and agent certificates.)

[comment]: # ({/new-e3030ed0})
