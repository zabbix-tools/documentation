[comment]: # translation:outdated

[comment]: # ({new-ad64ebf5})
# 3 Dépannage

[comment]: # ({/new-ad64ebf5})

[comment]: # ({new-385ec312})
#### Recommandations générales

-   En cas de problème, commencez par comprendre quel composant agit en
    tant que client TLS et lequel agit en tant que serveur TLS.\
    Le serveur Zabbix, les proxies et les agents, en fonction de leur
    interaction, tous peuvent fonctionner en tant que serveurs et
    clients TLS. \\\\Par exemple , Le serveur Zabbix se connectant à
    l'agent pour une vérification passive, agit comme un client TLS.
    L'agent joue le rôle de serveur TLS.\
    L'agent Zabbix, qui demande une liste de contrôles actifs à partir
    du proxy, agit comme un client TLS. Le proxy joue le rôle de serveur
    TLS. Les utilitaires \\\\'' zabbix\_get'' et `zabbix_sender`
    agissent toujours comme des clients TLS.
-   Zabbix utilise une authentification mutuelle. Chaque côté vérifie
    son homologue et peut refuser la connexion.\
    Par exemple, le serveur Zabbix qui se connecte à l'agent peut fermer
    la connexion immédiatement si le certificat de l'agent n'est pas
    valide. Et vice-versa - l'agent Zabbix acceptant une connexion à
    partir du serveur peut fermer la connexion si l'agent n'est pas
    approuvé par le serveur.
-   Examinez les fichiers journaux des deux côtés - côté client TLS et
    côté serveur TLS. \\\\Le côté qui refuse la connexion peut inscrire
    une raison précise pour laquelle il l'a refusée. Un autre côté
    signale souvent une erreur plutôt générale (par exemple "Connexion
    fermée par un homologue", "la connexion a été interrompue de manière
    incorrecte").
-   Parfois, un chiffrement mal configuré donne lieu à des messages
    d'erreur déroutants n'indiquant en aucun cas une cause réelle.
    \\\\Dans les sous-sections qui suivent, nous essaierons de fournir
    une liste (loin d'être exhaustive) de messages et de causes
    possibles pouvant aider au dépannage.\
    Les toolkits (OpenSSL, GnuTLS, mbed TLS (PolarSSL)) produisent
    souvent des messages d'erreur différents dans les mêmes situations
    problématiques.\
    Parfois, les messages d'erreur dépendent même d'une combinaison
    particulière de kits d'outils cryptographiques de chaque côté.

[comment]: # ({/new-385ec312})
