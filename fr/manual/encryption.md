[comment]: # translation:outdated

[comment]: # ({new-822969ff})
# 17. Chiffrement

[comment]: # ({/new-822969ff})

[comment]: # ({new-0b118c23})
#### Vue d'ensemble

Zabbix prend en charge les communications chiffrées entre le serveur
Zabbix, le proxy Zabbix, les agents Zabbix, les utilitaires
zabbix\_sender et zabbix\_get utilisant le protocole TLS (Transport
Layer Security) v.1.2. Le chiffrement est pris en charge à partir de
Zabbix 3.0. Le chiffrement basé sur des certificats et sur des clés
pré-partagées est pris en charge.

Le chiffrement est facultatif et configurable pour chaque composant
individuellement (par exemple, certains proxys et agents peuvent être
configurés pour utiliser le chiffrement basé sur des certificats avec le
serveur, tandis que d'autres peuvent utiliser le chiffrement par clé
pré-partagée).

Le serveur (proxy) peut utiliser différentes configurations de
chiffrement pour différents hôtes.

Les démons Zabbix utilisent un seul port d'écoute pour les connexions
entrantes chiffrées et non chiffrées. L'ajout d'un chiffrement ne
nécessite pas l'ouverture de nouveaux ports sur les pare-feux.

[comment]: # ({/new-0b118c23})

[comment]: # ({new-bdc6b202})
#### Limitations

-   Les clés privées sont stockées en texte brut dans des fichiers
    lisibles par les composants Zabbix lors du démarrage.
-   Les clés pré-partagées sont saisies dans l'interface Zabbix et
    stockées dans la base de données Zabbix en texte brut.
-   Le chiffrement intégré ne protège pas les communications:

```{=html}
<!-- -->
```
       * entre le serveur Web exécutant l'interface Zabbix et le navigateur Web de l'utilisateur,
       * entre le serveur Web exécutant l'interface Zabbix et le serveur,
       * entre le serveur Zabbix (proxy) et la base de données Zabbix.
    * Actuellement, chaque connexion chiffrée s'ouvre par négociation TLS complète, aucune mise en cache de session et aucun ticket ne sont implémentés.
    * L'ajout du chiffrement augmente la durée des contrôles et des actions, en fonction de la latence du réseau. Par exemple, si le délai de transmission est de 100 ms, l'ouverture d'une connexion TCP et l'envoi d'une requête non chiffrée prennent environ 200 ms.\\ Avec le chiffrement, 1000ms environ sont ajoutés pour établir une connexion TLS.\\ Il peut être nécessaire d'augmenter les délais d'attente. A défaut, certains éléments et actions exécutant des scripts distants sur des agents fonctionneront en connexion non chiffrée, après échec dû à l'expiration du chiffrement.
    * Le chiffrement n'est pas pris en charge pour la [[fr:manual/discovery/network_discovery|découverte réseau]]. Les vérifications des agents Zabbix effectuées par découverte réseau ne seront pas chiffrées et si l'agent Zabbix est configuré pour rejeter les connexions non chiffrées, ces vérifications échoueront.

[comment]: # ({/new-bdc6b202})

[comment]: # ({new-ba7ecc58})
#### Compiler Zabbix avec le support du chiffrement

Pour prendre en charge le chiffrement, Zabbix doit être compilé et lié à
l'une des trois bibliothèques cryptographiques suivantes:

-   *mbed TLS* (anciennement *PolarSSL*)(version 1.3.9 et 1.3.x
    supérieure). *mbed TLS* 2.x n'est pas actuellement pris en charge,
    ce n'est pas un remplacement immédiat pour la branche 1.3, Zabbix ne
    pourra pas être compilé avec *mbed TLS* 2.x.
-   *GnuTLS* (à partir de la version 3.1.18)
-   *OpenSSL* (à partir de la version 1.0.1)

La bibliothèque est sélectionnée en spécifiant une des options suivantes
au script "configure":

-   `--with-mbedtls[=DIR]`
-   `--with-gnutls[=DIR]`
-   `--with-openssl[=DIR]`

Par exemple, pour configurer les sources pour le serveur et l'agent avec
*OpenSSL*, vous pouvez utiliser le script "configure" comme suit:\
./configure --enable-server --enable-agent --with-mysql --enable-ipv6
--with-net-snmp --with-libcurl --with-libxml2 --with-openssl

Différents composants Zabbix peuvent être compilés avec différentes
bibliothèques cryptographiques (par exemple, un serveur avec *OpenSSL*,
un agent avec *GnuTLS*).

::: noteimportant
 Si vous prévoyez d'utiliser des clés
pré-partagées (PSK), envisagez d'utiliser les bibliothèques *GnuTLS* ou
*mbed TLS* pour les composants Zabbix utilisant PSK. Les bibliothèques
*GnuTLS* et *mbed TLS* prennent en charge les suites de chiffrement PSK
avec [Confidentialité
persistante](https://fr.wikipedia.org/wiki/Confidentialit%C3%A9_persistante).
La bibliothèque *OpenSSL* (versions 1.0.1 et 1.0.2c) prend en charge les
PSK, mais les suites de chiffrement PSK disponibles ne fournissent pas
confidentialité persistante. 
:::

[comment]: # ({/new-ba7ecc58})

[comment]: # ({new-2d0e2b80})
#### Gestion du chiffrement de la connexion

Les connexions dans Zabbix peuvent utiliser:

-   aucun chiffrement (par défaut)
-   [le chiffrement basé sur un certificat
    RSA](/fr/manual/encryption/using_certificates)
-   [le chiffrement basé sur
    PSK](/fr/manual/encryption/using_pre_shared_keys)

Deux paramètres importants permettent de spécifier le chiffrement des
connexions entre les composants Zabbix:

-   `TLSConnect`
-   `TLSAccept`

`TLSConnect` spécifie le chiffrement à utiliser pour les connexions
sortantes et peut prendre [une des trois]{.underline} valeurs
(`unencrypted`, `PSK`, `certificate`). `TLSConnect` est utilisé dans les
fichiers de configuration pour le proxy Zabbix (en mode actif, spécifie
uniquement les connexions au serveur) et l'agent Zabbix (pour les
vérifications actives). Dans l'interface Zabbix, les équivalents de
`TLSConnect` sont le champ *Connexion à l'hôte* de l'onglet
*Configuration→Hôtes→<un hôte>→Chiffrement* et le champ *Connexion
au proxy* de l'onglet *Administration→Proxys→<un
proxy>→Chiffrement*. Si le type de chiffrement configuré pour la
connexion échoue, aucun autre type de chiffrement ne sera essayé.

`TLSAccept` indique quels types de connexions sont autorisés pour les
connexions entrantes. Les types de connexion sont les suivants:
`unencrypted`, `PSK`, `certificate`. [Une ou plusieurs]{.underline}
valeurs peuvent être spécifiées. `TLSAccept` est utilisé dans les
fichiers de configuration du proxy Zabbix (en mode passif, spécifie
uniquement les connexions du serveur) et du démon agentd Zabbix (pour
les vérifications passives). Dans l'interface Zabbix, les équivalents de
`TLSAccept` sont le champ *Connexion de l'hôte* de l'onglet
*Configuration→Hôtes→<un hôte>→Chiffrement* et le champ
*Connexions du proxy* de l'onglet *Administration→Proxys→<un
proxy>→Chiffrement*.

Normalement, vous ne configurez qu'un seul type de chiffrement pour les
chiffrements entrants. Mais vous pourriez vouloir changer de type de
chiffrement, par exemple du non chiffré au certificat avec un temps
d'arrêt minimum et une possibilité de retour en arrière.\
Pour y parvenir, vous pouvez définir `TLSAccept=unencrypted,cert` dans
le fichier de configuration du démon agentd, puis redémarrer l'agent
Zabbix.\
Ensuite, vous pourrez tester la connexion avec la commande `zabbix_get`
à destination de l'agent utilisant un certificat. Si cela fonctionne,
vous pourrez reconfigurer le chiffrement pour cet agent dans l'onglet
*Configuration→Hôtes→<un hôte>→Chiffrement* de l'interface Zabbix
en définissant *Connexion à l'hôte* à "Certificat".\
Lorsque le cache de configuration du serveur est mis à jour (et la
configuration du proxy est mise à jour si l'hôte supervisé par proxy)
les connexions à cet agent seront alors chiffrées.\
Si tout fonctionne comme prévu, vous pourrez définir `TLSAccept=cert`
dans le fichier de configuration de l'agent et redémarrer l'agent
Zabbix.\
Dés lors, l'agent n'acceptera que les connexions chiffrées basées sur
des certificats. Les connexions non chiffrées et basées sur PSK seront
rejetées.

Tout cela fonctionne de la même manière sur le serveur et le proxy. Si
dans le champ *Connexion de l'hôte* de l'onglet
*Configuration→Hôtes→<un hôte>→Chiffrement* de l'interface Zabbix
est défini à "Certificat", alors seules les connexions chiffrées basées
sur des certificats seront acceptées depuis l'agent (surveillances
actives) et du `zabbix_sender` (éléments trappeur).

Vous configurerez très probablement les connexions entrantes et
sortantes pour utiliser le même type de chiffrement ou aucun
chiffrement. Mais techniquement, il est possible de passer à une
configuration asymétrique, par exemple un chiffrement par certificat
pour les connexions entrantes et basées sur PSK pour les connexions
sortantes.

Pour une vue d'ensemble, la configuration de chiffrement pour chaque
hôte est affichée dans l'interface Zabbix *Configuration→ Hôtes* sur le
côté droit, dans la colonne *Chiffrement sur l'agent*. Exemples
d'affichage de configuration:

|Exemple|Connexions A l'hôte|Connexions autorisées DE l'hôte|Connexions rejetées DE l'hôte|
|-------|--------------------|---------------------------------|-------------------------------|
|![none.png](../../assets/fr/manual/encryption/none.png)|Non chiffrées|Non chiffrées|Chiffrées par certificat et PSK|
|![cert.png](../../assets/fr/manual/encryption/cert.png)|Chiffrées par certificat|Chiffrées par certificat|Non chiffrées et chiffrées par PSK|
|![psk\_psk.png](../../assets/fr/manual/encryption/psk_psk.png)|Chiffrées par PSK|Chiffrées par PSK|Non chiffrées et chiffrées par certificat|
|![psk\_none\_psk.png](../../assets/fr/manual/encryption/psk_none_psk.png)|Chiffrées par PSK|Non chiffrées et chiffrées par PSK|Chiffrées par certificat|
|![all.png](../../assets/fr/manual/encryption/all.png)|Chiffrées par certificat|Non chiffrées, chiffrées par certificat et chiffrées par PSK|\-|

::: noteimportant
 Par défaut, les connexions sont non chiffrées. Le
chiffrement doit être configuré pour chaque hôte et chaque proxy
individuellement. 
:::

[comment]: # ({/new-2d0e2b80})

[comment]: # ({new-ad15e6ff})
#### zabbix\_get et zabbix\_sender avec chiffrement

Voir les man-pages [zabbix\_get](/manpages/zabbix_get) et
[zabbix\_sender](/manpages/zabbix_sender) pour une utilisation par
chiffrement.

[comment]: # ({/new-ad15e6ff})

[comment]: # ({new-be2996e2})
#### Méthodes de chiffrement

Les méthodes de chiffrement sont configurés en interne lors du démarrage
de Zabbix et dépendent de la bibliothèque cryptographique, actuellement
ils ne sont pas configurables par l'utilisateur.

Configurations de chiffrements par type de bibliothèque, de la priorité
la plus élevée à la plus basse:

|Bibliothèque|Méthodes de chiffrement par certificat|Méthodes de chiffrement PSK|
|-------------|---------------------------------------|----------------------------|
|*mbed TLS (PolarSSL) 1.3.9*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256<br>TLS-ECDHE-RSA-WITH-AES-128-CBC-SHA256<br>TLS-ECDHE-RSA-WITH-AES-128-CBC-SHA<br>TLS-RSA-WITH-AES-128-GCM-SHA256<br>TLS-RSA-WITH-AES-128-CBC-SHA256<br>TLS-RSA-WITH-AES-128-CBC-SHA|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256<br>TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA<br>TLS-PSK-WITH-AES-128-GCM-SHA256<br>TLS-PSK-WITH-AES-128-CBC-SHA256<br>TLS-PSK-WITH-AES-128-CBC-SHA|
|*GnuTLS 3.1.18*|TLS\_ECDHE\_RSA\_AES\_128\_GCM\_SHA256<br>TLS\_ECDHE\_RSA\_AES\_128\_CBC\_SHA256<br>TLS\_ECDHE\_RSA\_AES\_128\_CBC\_SHA1<br>TLS\_RSA\_AES\_128\_GCM\_SHA256<br>TLS\_RSA\_AES\_128\_CBC\_SHA256<br>TLS\_RSA\_AES\_128\_CBC\_SHA1|TLS\_ECDHE\_PSK\_AES\_128\_CBC\_SHA256<br>TLS\_ECDHE\_PSK\_AES\_128\_CBC\_SHA1<br>TLS\_PSK\_AES\_128\_GCM\_SHA256<br>TLS\_PSK\_AES\_128\_CBC\_SHA256<br>TLS\_PSK\_AES\_128\_CBC\_SHA1|
|*OpenSSL 1.0.2c*|ECDHE-RSA-AES128-GCM-SHA256<br>ECDHE-RSA-AES128-SHA256<br>ECDHE-RSA-AES128-SHA<br>AES128-GCM-SHA256<br>AES128-SHA256<br>AES128-SHA|PSK-AES128-CBC-SHA|
|*OpenSSL 1.1.0*|ECDHE-RSA-AES128-GCM-SHA256<br>ECDHE-RSA-AES128-SHA256<br>ECDHE-RSA-AES128-SHA<br>AES128-GCM-SHA256<br>AES128-CCM8<br>AES128-CCM<br>AES128-SHA256<br>AES128-SHA<br>|ECDHE-PSK-AES128-CBC-SHA256<br>ECDHE-PSK-AES128-CBC-SHA<br>PSK-AES128-GCM-SHA256<br>PSK-AES128-CCM8<br>PSK-AES128-CCM<br>PSK-AES128-CBC-SHA256<br>PSK-AES128-CBC-SHA|

Méthodes de chiffrement utilisant des certificats:

|   |   |   |   |
|---|---|---|---|
|<|Serveur TLS|<|<|
|Client TLS|*mbed TLS (PolarSSL)*|*GnuTLS*|*OpenSSL 1.0.2*|
|*mbed TLS (PolarSSL)*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|
|*GnuTLS*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|
|*OpenSSL 1.0.2*|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256|

Méthodes de chiffrement utilisant PSK:

|   |   |   |   |
|---|---|---|---|
|<|Serveur TLS|<|<|
|Client TLS|*mbed TLS (PolarSSL)*|*GnuTLS*|*OpenSSL 1.0.2*|
|*mbed TLS (PolarSSL)*|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-PSK-WITH-AES-128-CBC-SHA|
|*GnuTLS*|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-ECDHE-PSK-WITH-AES-128-CBC-SHA256|TLS-PSK-WITH-AES-128-CBC-SHA|
|*OpenSSL 1.0.2*|TLS-PSK-WITH-AES-128-CBC-SHA|TLS-PSK-WITH-AES-128-CBC-SHA|TLS-PSK-WITH-AES-128-CBC-SHA|

[comment]: # ({/new-be2996e2})

[comment]: # ({new-b5e04652})
#### User-configured ciphersuites

The built-in ciphersuite selection criteria can be overridden with
user-configured ciphersuites.

::: noteimportant
User-configured ciphersuites is a feature intended
for advanced users who understand TLS ciphersuites, their security and
consequences of mistakes, and who are comfortable with TLS
troubleshooting.
:::

The built-in ciphersuite selection criteria can be overridden using the
following parameters:

|Override scope|Parameter|Value|Description|
|--------------|---------|-----|-----------|
|Ciphersuite selection for certificates|TLSCipherCert13|Valid OpenSSL 1.1.1 [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.3 protocol (their values are passed to the OpenSSL function SSL\_CTX\_set\_ciphersuites()).|Certificate-based ciphersuite selection criteria for TLS 1.3<br><br>Only OpenSSL 1.1.1 or newer.|
|^|TLSCipherCert|Valid OpenSSL [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.2 or valid GnuTLS [priority strings](https://gnutls.org/manual/html_node/Priority-Strings.html). Their values are passed to the SSL\_CTX\_set\_cipher\_list() or gnutls\_priority\_init() functions, respectively.|Certificate-based ciphersuite selection criteria for TLS 1.2/1.3 (GnuTLS), TLS 1.2 (OpenSSL)|
|Ciphersuite selection for PSK|TLSCipherPSK13|Valid OpenSSL 1.1.1 [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.3 protocol (their values are passed to the OpenSSL function SSL\_CTX\_set\_ciphersuites()).|PSK-based ciphersuite selection criteria for TLS 1.3<br><br>Only OpenSSL 1.1.1 or newer.|
|^|TLSCipherPSK|Valid OpenSSL [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.2 or valid GnuTLS [priority strings](https://gnutls.org/manual/html_node/Priority-Strings.html). Their values are passed to the SSL\_CTX\_set\_cipher\_list() or gnutls\_priority\_init() functions, respectively.|PSK-based ciphersuite selection criteria for TLS 1.2/1.3 (GnuTLS), TLS 1.2 (OpenSSL)|
|Combined ciphersuite list for certificate and PSK|TLSCipherAll13|Valid OpenSSL 1.1.1 [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.3 protocol (their values are passed to the OpenSSL function SSL\_CTX\_set\_ciphersuites()).|Ciphersuite selection criteria for TLS 1.3<br><br>Only OpenSSL 1.1.1 or newer.|
|^|TLSCipherAll|Valid OpenSSL [cipher strings](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html) for TLS 1.2 or valid GnuTLS [priority strings](https://gnutls.org/manual/html_node/Priority-Strings.html). Their values are passed to the SSL\_CTX\_set\_cipher\_list() or gnutls\_priority\_init() functions, respectively.|Ciphersuite selection criteria for TLS 1.2/1.3 (GnuTLS), TLS 1.2 (OpenSSL)|

To override the ciphersuite selection in
[zabbix\_get](/manpages/zabbix_get) and
[zabbix\_sender](/manpages/zabbix_sender) utilities - use the
command-line parameters:

-   `--tls-cipher13`
-   `--tls-cipher`

The new parameters are optional. If a parameter is not specified, the
internal default value is used. If a parameter is defined it cannot be
empty.

If the setting of a TLSCipher\* value in the crypto library fails then
the server, proxy or agent will not start and an error is logged.

It is important to understand when each parameter is applicable.

[comment]: # ({/new-b5e04652})

[comment]: # ({new-a74c28c7})
##### Outgoing connections

The simplest case is outgoing connections:

-   For outgoing connections with certificate - use TLSCipherCert13 or
    TLSCipherCert
-   For outgoing connections with PSK - use TLSCipherPSK13 and
    TLSCipherPSK
-   In case of zabbix\_get and zabbix\_sender utilities the command-line
    parameters `--tls-cipher13` and `--tls-cipher` can be used
    (encryption is unambiguously specified with a `--tls-connect`
    parameter)

[comment]: # ({/new-a74c28c7})

[comment]: # ({new-3edf3b2b})
##### Incoming connections

It is a bit more complicated with incoming connections because rules are
specific for components and configuration.

For Zabbix **agent**:

|Agent connection setup|Cipher configuration|
|----------------------|--------------------|
|TLSConnect=cert|TLSCipherCert, TLSCipherCert13|
|TLSConnect=psk|TLSCipherPSK, TLSCipherPSK13|
|TLSAccept=cert|TLSCipherCert, TLSCipherCert13|
|TLSAccept=psk|TLSCipherPSK, TLSCipherPSK13|
|TLSAccept=cert,psk|TLSCipherAll, TLSCipherAll13|

For Zabbix **server** and \*\* proxy\*\*:

|Connection setup|Cipher configuration|
|----------------|--------------------|
|Outgoing connections using PSK|TLSCipherPSK, TLSCipherPSK13|
|Incoming connections using certificates|TLSCipherAll, TLSCipherAll13|
|Incoming connections using PSK if server has no certificate|TLSCipherPSK, TLSCipherPSK13|
|Incoming connections using PSK if server has certificate|TLSCipherAll, TLSCipherAll13|

Some pattern can be seen in the two tables above:

-   TLSCipherAll and TLSCipherAll13 can be specified only if a combined
    list of certificate- **and** PSK-based ciphersuites is used. There
    are two cases when it takes place: server (proxy) with a configured
    certificate (PSK ciphersuites are always configured on server, proxy
    if crypto library supports PSK), agent configured to accept both
    certificate- and PSK-based incoming connections
-   in other cases TLSCipherCert\* and/or TLSCipherPSK\* are sufficient

The following tables show the `TLSCipher*` built-in default values. They
could be a good starting point for your own custom values.

|Parameter|GnuTLS 3.6.12|
|---------|-------------|
|TLSCipherCert|NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509|
|TLSCipherPSK|NONE:+VERS-TLS1.2:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL|
|TLSCipherAll|NONE:+VERS-TLS1.2:+ECDHE-RSA:+RSA:+ECDHE-PSK:+PSK:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+SHA1:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509|

|Parameter|OpenSSL 1.1.1d ^**1**^|
|---------|----------------------|
|TLSCipherCert13|<|
|TLSCipherCert|EECDH+aRSA+AES128:RSA+aRSA+AES128|
|TLSCipherPSK13|TLS\_CHACHA20\_POLY1305\_SHA256:TLS\_AES\_128\_GCM\_SHA256|
|TLSCipherPSK|kECDHEPSK+AES128:kPSK+AES128|
|TLSCipherAll13|<|
|TLSCipherAll|EECDH+aRSA+AES128:RSA+aRSA+AES128:kECDHEPSK+AES128:kPSK+AES128|

^**1**^ Default values are different for older OpenSSL versions (1.0.1,
1.0.2, 1.1.0), for LibreSSL and if OpenSSL is compiled without PSK
support.

\*\* Examples of user-configured ciphersuites \*\*

See below the following examples of user-configured ciphersuites:

-   [Testing cipher strings and allowing only PFS
    ciphersuites](#testing_cipher_strings_and_allowing_only_pfs_ciphersuites)
-   [Switching from AES128 to AES256](#switching_from_aes128_to_aes256)

[comment]: # ({/new-3edf3b2b})

[comment]: # ({new-bfb4ef88})
##### Testing cipher strings and allowing only PFS ciphersuites

To see which ciphersuites have been selected you need to set
'DebugLevel=4' in the configuration file, or use the `-vv` option for
zabbix\_sender.

Some experimenting with `TLSCipher*` parameters might be necessary
before you get the desired ciphersuites. It is inconvenient to restart
Zabbix server, proxy or agent multiple times just to tweak `TLSCipher*`
parameters. More convenient options are using zabbix\_sender or the
`openssl` command. Let's show both.

**1.** Using zabbix\_sender.

Let's make a test configuration file, for example
/home/zabbix/test.conf, with the syntax of a zabbix\_agentd.conf file:

      Hostname=nonexisting
      ServerActive=nonexisting
      
      TLSConnect=cert
      TLSCAFile=/home/zabbix/ca.crt
      TLSCertFile=/home/zabbix/agent.crt
      TLSKeyFile=/home/zabbix/agent.key
      TLSPSKIdentity=nonexisting
      TLSPSKFile=/home/zabbix/agent.psk

You need valid CA and agent certificates and PSK for this example.
Adjust certificate and PSK file paths and names for your environment.

If you are not using certificates, but only PSK, you can make a simpler
test file:

      Hostname=nonexisting
      ServerActive=nonexisting
      
      TLSConnect=psk
      TLSPSKIdentity=nonexisting
      TLSPSKFile=/home/zabbix/agentd.psk

The selected ciphersuites can be seen by running zabbix\_sender (example
compiled with OpenSSL 1.1.d):

      $ zabbix_sender -vv -c /home/zabbix/test.conf -k nonexisting_item -o 1 2>&1 | grep ciphersuites
      zabbix_sender [41271]: DEBUG: zbx_tls_init_child() certificate ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA AES128-GCM-SHA256 AES128-CCM8 AES128-CCM AES128-SHA256 AES128-SHA
      zabbix_sender [41271]: DEBUG: zbx_tls_init_child() PSK ciphersuites: TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA PSK-AES128-GCM-SHA256 PSK-AES128-CCM8 PSK-AES128-CCM PSK-AES128-CBC-SHA256 PSK-AES128-CBC-SHA
      zabbix_sender [41271]: DEBUG: zbx_tls_init_child() certificate and PSK ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA AES128-GCM-SHA256 AES128-CCM8 AES128-CCM AES128-SHA256 AES128-SHA ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA PSK-AES128-GCM-SHA256 PSK-AES128-CCM8 PSK-AES128-CCM PSK-AES128-CBC-SHA256 PSK-AES128-CBC-SHA

Here you see the ciphersuites selected by default. These default values
are chosen to ensure interoperability with Zabbix agents running on
systems with older OpenSSL versions (from 1.0.1).

With newer systems you can choose to tighten security by allowing only a
few ciphersuites, e.g. only ciphersuites with PFS (Perfect Forward
Secrecy). Let's try to allow only ciphersuites with PFS using
`TLSCipher*` parameters.

::: noteimportant
The result will not be interoperable with systems
using OpenSSL 1.0.1 and 1.0.2, if PSK is used. Certificate-based
encryption should work.
:::

Add two lines to the `test.conf` configuration file:

      TLSCipherCert=EECDH+aRSA+AES128
      TLSCipherPSK=kECDHEPSK+AES128

and test again:

      $ zabbix_sender -vv -c /home/zabbix/test.conf -k nonexisting_item -o 1 2>&1 | grep ciphersuites            
      zabbix_sender [42892]: DEBUG: zbx_tls_init_child() certificate ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA        
      zabbix_sender [42892]: DEBUG: zbx_tls_init_child() PSK ciphersuites: TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA        
      zabbix_sender [42892]: DEBUG: zbx_tls_init_child() certificate and PSK ciphersuites: TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA AES128-GCM-SHA256 AES128-CCM8 AES128-CCM AES128-SHA256 AES128-SHA ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA PSK-AES128-GCM-SHA256 PSK-AES128-CCM8 PSK-AES128-CCM PSK-AES128-CBC-SHA256 PSK-AES128-CBC-SHA        

The "certificate ciphersuites" and "PSK ciphersuites" lists have changed
- they are shorter than before, only containing TLS 1.3 ciphersuites and
TLS 1.2 ECDHE-\* ciphersuites as expected.

**2.** TLSCipherAll and TLSCipherAll13 cannot be tested with
zabbix\_sender; they do not affect "certificate and PSK ciphersuites"
value shown in the example above. To tweak TLSCipherAll and
TLSCipherAll13 you need to experiment with the agent, proxy or server.

So, to allow only PFS ciphersuites you may need to add up to three
parameters

      TLSCipherCert=EECDH+aRSA+AES128
      TLSCipherPSK=kECDHEPSK+AES128
      TLSCipherAll=EECDH+aRSA+AES128:kECDHEPSK+AES128

to zabbix\_agentd.conf, zabbix\_proxy.conf and zabbix\_server\_conf if
each of them has a configured certificate and agent has also PSK.

If your Zabbix environment uses only PSK-based encryption and no
certificates, then only one:

      TLSCipherPSK=kECDHEPSK+AES128

Now that you understand how it works you can test the ciphersuite
selection even outside of Zabbix, with the `openssl` command. Let's test
all three `TLSCipher*` parameter values:

      $ openssl ciphers EECDH+aRSA+AES128 | sed 's/:/ /g'
      TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA
      $ openssl ciphers kECDHEPSK+AES128 | sed 's/:/ /g'
      TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA
      $ openssl ciphers EECDH+aRSA+AES128:kECDHEPSK+AES128 | sed 's/:/ /g'
      TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_GCM_SHA256 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES128-SHA ECDHE-PSK-AES128-CBC-SHA256 ECDHE-PSK-AES128-CBC-SHA
      

You may prefer `openssl ciphers` with option `-V` for a more verbose
output:

      $ openssl ciphers -V EECDH+aRSA+AES128:kECDHEPSK+AES128
                0x13,0x02 - TLS_AES_256_GCM_SHA384  TLSv1.3 Kx=any      Au=any  Enc=AESGCM(256) Mac=AEAD
                0x13,0x03 - TLS_CHACHA20_POLY1305_SHA256 TLSv1.3 Kx=any      Au=any  Enc=CHACHA20/POLY1305(256) Mac=AEAD
                0x13,0x01 - TLS_AES_128_GCM_SHA256  TLSv1.3 Kx=any      Au=any  Enc=AESGCM(128) Mac=AEAD
                0xC0,0x2F - ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 Kx=ECDH     Au=RSA  Enc=AESGCM(128) Mac=AEAD
                0xC0,0x27 - ECDHE-RSA-AES128-SHA256 TLSv1.2 Kx=ECDH     Au=RSA  Enc=AES(128)  Mac=SHA256
                0xC0,0x13 - ECDHE-RSA-AES128-SHA    TLSv1 Kx=ECDH     Au=RSA  Enc=AES(128)  Mac=SHA1
                0xC0,0x37 - ECDHE-PSK-AES128-CBC-SHA256 TLSv1 Kx=ECDHEPSK Au=PSK  Enc=AES(128)  Mac=SHA256
                0xC0,0x35 - ECDHE-PSK-AES128-CBC-SHA TLSv1 Kx=ECDHEPSK Au=PSK  Enc=AES(128)  Mac=SHA1

Similarly, you can test the priority strings for GnuTLS:

      $ gnutls-cli -l --priority=NONE:+VERS-TLS1.2:+ECDHE-RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509
      Cipher suites for NONE:+VERS-TLS1.2:+ECDHE-RSA:+AES-128-GCM:+AES-128-CBC:+AEAD:+SHA256:+CURVE-ALL:+COMP-NULL:+SIGN-ALL:+CTYPE-X.509
      TLS_ECDHE_RSA_AES_128_GCM_SHA256                        0xc0, 0x2f      TLS1.2
      TLS_ECDHE_RSA_AES_128_CBC_SHA256                        0xc0, 0x27      TLS1.2
      
      Protocols: VERS-TLS1.2
      Ciphers: AES-128-GCM, AES-128-CBC
      MACs: AEAD, SHA256
      Key Exchange Algorithms: ECDHE-RSA
      Groups: GROUP-SECP256R1, GROUP-SECP384R1, GROUP-SECP521R1, GROUP-X25519, GROUP-X448, GROUP-FFDHE2048, GROUP-FFDHE3072, GROUP-FFDHE4096, GROUP-FFDHE6144, GROUP-FFDHE8192
      PK-signatures: SIGN-RSA-SHA256, SIGN-RSA-PSS-SHA256, SIGN-RSA-PSS-RSAE-SHA256, SIGN-ECDSA-SHA256, SIGN-ECDSA-SECP256R1-SHA256, SIGN-EdDSA-Ed25519, SIGN-RSA-SHA384, SIGN-RSA-PSS-SHA384, SIGN-RSA-PSS-RSAE-SHA384, SIGN-ECDSA-SHA384, SIGN-ECDSA-SECP384R1-SHA384, SIGN-EdDSA-Ed448, SIGN-RSA-SHA512, SIGN-RSA-PSS-SHA512, SIGN-RSA-PSS-RSAE-SHA512, SIGN-ECDSA-SHA512, SIGN-ECDSA-SECP521R1-SHA512, SIGN-RSA-SHA1, SIGN-ECDSA-SHA1

[comment]: # ({/new-bfb4ef88})

[comment]: # ({new-a549cda7})
##### Switching from AES128 to AES256

Zabbix uses AES128 as the built-in default for data. Let's assume you
are using certificates and want to switch to AES256, on OpenSSL 1.1.1.

This can be achieved by adding the respective parameters in
`zabbix_server.conf`:

      TLSCAFile=/home/zabbix/ca.crt
      TLSCertFile=/home/zabbix/server.crt
      TLSKeyFile=/home/zabbix/server.key
      TLSCipherCert13=TLS_AES_256_GCM_SHA384
      TLSCipherCert=EECDH+aRSA+AES256:-SHA1:-SHA384
      TLSCipherPSK13=TLS_CHACHA20_POLY1305_SHA256
      TLSCipherPSK=kECDHEPSK+AES256:-SHA1
      TLSCipherAll13=TLS_AES_256_GCM_SHA384
      TLSCipherAll=EECDH+aRSA+AES256:-SHA1:-SHA384

::: noteimportant
Although only certificate-related ciphersuites
will be used, `TLSCipherPSK*` parameters are defined as well to avoid
their default values which include less secure ciphers for wider
interoperability. PSK ciphersuites cannot be completely disabled on
server/proxy.
:::

And in `zabbix_agentd.conf`:

      TLSConnect=cert
      TLSAccept=cert
      TLSCAFile=/home/zabbix/ca.crt
      TLSCertFile=/home/zabbix/agent.crt
      TLSKeyFile=/home/zabbix/agent.key
      TLSCipherCert13=TLS_AES_256_GCM_SHA384
      TLSCipherCert=EECDH+aRSA+AES256:-SHA1:-SHA384

[comment]: # ({/new-a549cda7})
