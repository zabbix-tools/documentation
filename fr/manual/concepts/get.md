[comment]: # translation:outdated

[comment]: # ({new-14379c17})
# 6 Get

[comment]: # ({/new-14379c17})

[comment]: # ({new-f50cbd4e})
#### Aperçu

Le get Zabbix est un utilitaire en ligne de commande qui peut être
utilisé pour communiquer avec l'agent Zabbix et extraire les
informations requises depuis l'agent.

L'utilitaire est généralement utilisé pour le dépannage des agents
Zabbix.

[comment]: # ({/new-f50cbd4e})

[comment]: # ({new-c625ea25})
#### Exécution du get Zabbix

Un exemple d'exécution du get Zabbix sous UNIX pour obtenir la valeur de
chargement du processeur à partir de l'agent :

    shell> cd bin
    shell> ./zabbix_get -s 127.0.0.1 -p 10050 -k system.cpu.load[all,avg1]

Un autre exemple d'exécution du get Zabbix pour capturer une chaîne d'un
site Web :

    shell> cd bin
    shell> ./zabbix_get -s 192.168.1.1 -p 10050 -k "web.page.regexp[www.zabbix.com,,,\"USA: ([a-zA-Z0-9.-]+)\",,\1]"

Notez que la clé de l'élément contient ici un espace, donc les
guillemets sont utilisés pour marquer la clé de l'élément dans le shell.
Les guillemets ne font pas partie de la clé de l’élément ; ils seront
découpés par le shell et ne seront pas transmis à l'agent Zabbix.

Le get Zabbix accepte les paramètres de ligne de commande suivants :

      -s --host <host name or IP>      Spécifie le nom ou l’adresse IP de l’hôte.
      -p --port <port number>          Spécifie le port de l’agent. Par défaut : 10050
      -I --source-address <IP address> Spécifie l’adresse IP source.
      -k --key <item key>        Spécifie la clé de l’élément de la valeur à récupérer.
      -h --help                        Donne cette aide.
      -V --version                     Affiche le numéro de version.

Voir aussi [Zabbix get manpage](/fr/manpages/zabbix_get) pour plus
d’informations.

Le get Zabbix sur Windows peut être lancé de la manière suivante :

    zabbix_get.exe [options]

[comment]: # ({/new-c625ea25})
