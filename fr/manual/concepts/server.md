[comment]: # translation:outdated

[comment]: # ({new-cbb57ea2})
# 1 Serveur

[comment]: # ({/new-cbb57ea2})

[comment]: # ({23902651-0e549571})
#### Aperçu

Le serveur Zabbix est le processus central du logiciel Zabbix.

Le serveur effectue l'interrogation et la récupération des données, il calcule les déclencheurs, envoie des notifications aux utilisateurs. C'est le composant central auquel les agents et les proxys Zabbix rapportent des données de disponibilité et d'intégrité des systèmes. Le serveur peut lui-même vérifier à distance les services réseaux (tels que les serveurs Web et les serveurs de messagerie) à l'aide de simples vérifications de service.

Le serveur est le référentiel central dans lequel toutes les données de configuration, statistiques et opérationnelles sont stockées, et c'est l'entité de Zabbix qui alertera activement les administrateurs lorsque des problèmes surviennent dans l'un des systèmes surveillés.

Le fonctionnement d'un serveur Zabbix de base est divisé en trois composants distincts ; ce sont : le serveur Zabbix, l'interface Web et le stockage en base de données.

Toutes les informations de configuration de Zabbix sont stockées dans la base de données, avec laquelle le serveur et l'interface Web interagissent. Par exemple, lorsque vous créez un nouvel élément à l'aide de l'interface Web (ou de l'API), il est ajouté à la table des éléments de la base de données. Ensuite, environ une fois par minute, le serveur Zabbix interroge la table des éléments pour obtenir une liste des éléments actifs qui sont ensuite stockés dans un cache sur le serveur Zabbix. C'est pourquoi cela peut prendre jusqu'à deux minutes pour que toute modification apportée à l'interface Zabbix apparaisse dans la dernière section de données.

[comment]: # ({/23902651-0e549571})

[comment]: # ({42a02c80-c20247df})
#### Serveur en cours d'exécution

[comment]: # ({/42a02c80-c20247df})

[comment]: # ({f62bdec8-1314fd6f})
##### Si installé en tant que package

Le serveur Zabbix fonctionne comme un processus démon. Le serveur peut être démarré en exécutant :

    shell> service zabbix-server start

Cela fonctionnera sur la plupart des systèmes GNU/Linux. Sur d'autres systèmes, vous devrez peut-être exécuter :

    shell> /etc/init.d/zabbix-server start

De même, pour arrêter/redémarrer/afficher l'état, utilisez les commandes suivantes :

    shell> service zabbix-server stop
    shell> service zabbix-server restart
    shell> service zabbix-server status

[comment]: # ({/f62bdec8-1314fd6f})

[comment]: # ({new-bbb4c5d8})
##### Démarrage manuellement

Si ce qui précède ne fonctionne pas, vous devez le démarrer manuellement. Trouvez le chemin vers le binaire zabbix\_server et exécutez :

    shell> zabbix_server

Vous pouvez utiliser les paramètres de ligne de commande suivants avec le serveur Zabbix :

    -c --config <file>              path to the configuration file (default is /usr/local/etc/zabbix_server.conf)
    -f --foreground                 run Zabbix server in foreground
    -R --runtime-control <option>   perform administrative functions
    -h --help                       give this help
    -V --version                    display version number

::: noteclassic
Le contrôle d'exécution n'est pas pris en charge sur OpenBSD et NetBSD..
:::

Exemples d'exécution du serveur Zabbix avec des paramètres en ligne de commande :

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf
    shell> zabbix_server --help
    shell> zabbix_server -V

[comment]: # ({/new-bbb4c5d8})

[comment]: # ({new-a339702b})
##### Contrôle d'exécution
Options contrôle d'exécution :

|Option|Description|Cible|
|------|-----------|------|
|config\_cache\_reload|Recharger le cache de configuration. Ignoré si le cache est en cours de chargement| |
|diaginfo\[=<**target**>\]|Recueillir des informations de diagnostic dans le fichier journal du serveur.|**historycache** - statistiques du cache d'historique<br>**valuecache** - statistiques du cache de valeurs<br>**preprocessing** - statistiques du gestionnaire de prétraitement<br>**alerting** - statistiques du gestionnaire d'alertes<br>**lld** - statistiques du gestionnaire LLD<br>**locks** - liste des mutex|
|ha\_status|Journaliser l'état du cluster haute disponibilité (HA).| |
|ha\_remove\_node=target|Supprimer le nœud haute disponibilité (HA) spécifié par son numéro.<br>Notez que les nœuds actifs/en veille ne peuvent pas être supprimés.|**target** - numéro du nœud dans la liste (peut être obtenu en exécutant ha\_status)|
|ha\_set\_failover\_delay=delay|Définir le délai de basculement de la haute disponibilité (HA).<br>Les suffixes temporels sont pris en charge, par ex. 10s, 1m.| |
|secrets\_reload|Reload Recharger les informations d'identification depuis Vault.| |
|service\_cache\_reload|Recharger le cache du gestionnaire de services.| |
|snmp\_cache\_reload|Recharger le cache SNMP, effacer les propriétés SNMP (engine time, engine boots, engine id, informations d'identification) pour tous les hôtes.| |
|housekeeper\_execute|Démarrer la procédure de nettoyage. Ignoré si la procédure de nettoyage est actuellement en cours d'exécution.| |
|log\_level\_increase\[=<**target**>\]|Augmenter le niveau de journalisation, affecte tous les processus si la cible n'est pas spécifiée.|**process type** -Tous les processus du type spécifié (ex : poller)<br>Voir tous les [types de processus serveur](#server_process_types).<br>**process type,N** - Type de processus et numéro (ex : poller,3)<br>**pid** - Identifiant du processus (1 to 65535). Pour des valeurs plus grandes, spécifiez la cible ainsi : 'process type,N'.|
|log\_level\_decrease\[=<**target**>\]|Diminuer le niveau de journalisation, affecte tous les processus si la cible n'est pas spécifiée.|^|

Exemple d'utilisation du contrôle d'exécution pour recharger le cache de configuration du serveur :

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Exemple d'utilisation du contrôle d'exécution pour recueillir des informations de diagnostic :
    Rassembler toutes les informations de diagnostic disponibles dans le fichier journal du serveur :
    shell> zabbix_server -R diaginfo

    Rassembler les statistiques du cache d'historique dans le fichier journal du serveur :
    shell> zabbix_server -R diaginfo=historycache

Exemple d'utilisation du contrôle d'exécution pour recharger le cache SNMP :

    shell> zabbix_server -R snmp_cache_reload  

Exemple d'utilisation du contrôle d'exécution pour déclencher l'exécution du nettoyage :

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

Exemple d'utilisation du contrôle d'exécution pour modifier le niveau de journalisation :

    Augmenter le niveau de journalisation de tous les processus :
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    Augmenter le niveau de journalisation du deuxième processus de type poller:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    Augmenter le niveau de journalisation du processus avec le PID 1234 :
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    Diminuer le niveau de journalisation de tous les processus de type http poller :
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

Exemple de réglage du délai de basculement HA sur un minimum de 10 secondes :

    shell> zabbix_server -R ha_set_failover_delay=10s

[comment]: # ({/new-a339702b})

[comment]: # ({new-1cb7f51c})
Example of using runtime control to reload the server configuration
cache:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Examples of using runtime control to gather diagnostic information:

    Gather all available diagnostic information in the server log file:
    shell> zabbix_server -R diaginfo

    Gather history cache statistics in the server log file:
    shell> zabbix_server -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    shell> zabbix_server -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    Increase log level of all processes:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    Increase log level of second poller process:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    Increase log level of process with PID 1234:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    Decrease log level of all http poller processes:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

Example of setting the HA failover delay to the minimum of 10 seconds:

    shell> zabbix_server -R ha_set_failover_delay=10s

[comment]: # ({/new-1cb7f51c})

[comment]: # ({5294c75e-b4f10179})
##### Processus utilisateur

Le serveur Zabbix est conçu pour fonctionner en tant qu'utilisateur non root. Il fonctionnera avec n’importe quel utilisateur non-root qui lancera les processus. Vous pouvez donc exécuter le serveur en tant qu'utilisateur non root sans aucun problème.

Si vous essayez de l'exécuter en tant que 'root', il passera à un utilisateur 'zabbix' codé en dur, qui doit être présent sur votre système. Vous pouvez uniquement exécuter le serveur en tant que 'root' si vous modifiez le paramètre 'AllowRoot' dans le fichier de configuration associé du serveur.

Si le serveur et l'[agent](agent) Zabbix sont exécutés sur la même machine, il est recommandé d'utiliser un autre utilisateur pour exécuter le serveur que pour exécuter l'agent. Sinon, si les deux sont exécutés sous le même utilisateur, l'agent peut accéder au fichier de configuration du serveur et tout utilisateur de niveau Admin dans Zabbix peut facilement récupérer, par exemple, le mot de passe de la base de données.

[comment]: # ({/5294c75e-b4f10179})

[comment]: # ({16ccaa3b-0a81f475})
##### Fichier de configuration

Voir les options du [fichier de configuration](/manual/appendix/config/zabbix_server)
pour plus de détails sur la configuration de zabbix\_server.

[comment]: # ({/16ccaa3b-0a81f475})

[comment]: # ({78974be2-49247ffc})
##### Scripts de démarrage

Des scripts sont utilisés pour démarrer/arrêter automatiquement les processus Zabbix pendant le démarrage/l'arrêt du système. Les scripts sont situés dans le répertoire misc/init.d.

[comment]: # ({/78974be2-49247ffc})

[comment]: # ({d3d00c78-9f05badb})
#### Types de processus serveur

-   `alert manager` - gestionnaire de file d'attente d'alertes
-   `alert syncer` - processus d'écriture des alertes en base de données
-   `alerter` - processus d'envoi des notifications
-   `availability manager` - processus de mise à jour de la disponibilité des hôtes
-   `configuration syncer` - processus de gestion du cache en mémoire des données de configuration
-   `discoverer` - processus de découverte des équipements
-   `escalator` - processus d'escalade des actions
-   `history poller` - processus de traitements calculés et vérifications internes nécessitant une connexion à la base de données
-   `history syncer` - processus d'écriture de l'historique en base de données
-   `housekeeper` - processus de suppression des anciennes données d'historique
-   `http poller` - poller pour la supervision web
-   `icmp pinger` - poller pour les vérifications icmpping
-   `ipmi manager` - gestionnaires de poller IPMI
-   `ipmi poller` - poller pour les vérifications IPMI
-   `java poller` - poller pour les vérifications Java
-   `lld manager` - processus de gestion des tâches de découverte de bas niveau
-   `lld worker` - processus de traitement des tâches de découverte de bas niveau
-   `odbc poller` - poller pour les vérifications ODBC
-   `poller` - poller normal pour les vérifications passives
-   `preprocessing manager` - gestionnaire des tâches de pré-traitement
-   `preprocessing worker` - processus de pré-traitement des données
-   `problem housekeeper` - processus de suppression des problèmes liés à des déclencheurs supprimés
-   `proxy poller` - poller des proxys passifs
-   `report manager`- gestionnaire des tâches de génération de rapports planifiées
-   `report writer` - processus de génération de rapports planifiés
-   `self-monitoring` - processus de collecte des statistiques internes du serveur
-   `snmp trapper` - trapper pour les traps SNMP
-   `task manager` - processus pour l'exécution à distance de tâches demandées par d'autres composants (ex : fermeture d'un problème, acquittement d'un problème, vérification immédiate de la valeur d'un élément , fonctionnalité d'exécution de commande à distance)
-   `timer` - timer pour le traitement des maintenances
-   `trapper` - trapper pour les vérifications actives, les traps, la communication avec le proxy
-   `unreachable poller` - poller des équipements injoignables
-   `vmware collector` - collecte des données VMware responsable de la récupération des données pour les services VMware

Le fichier journal du serveur peut être utilisé pour observer les types de processus.

Plusieurs types de processus du serveur Zabbix peuvent être supervisés en utilisant l'[élément interne](/manual/config/items/itemtypes/internal) **zabbix\[process,<type>,<mode>,<state>\]** .

[comment]: # ({/d3d00c78-9f05badb})

[comment]: # ({1e409f69-cdd68340})
#### Plates-formes supportées

En raison des exigences de sécurité et de la nature critique du fonctionnement du serveur, UNIX est le seul système d'exploitation capable de fournir systématiquement les performances, la tolérance aux pannes et la résilience nécessaires. Zabbix fonctionne sur les meilleures versions du marché.

Le serveur Zabbix a été testé sur les plateformes suivantes :

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
Zabbix peut également fonctionner sur d'autres systèmes d'exploitation de type Unix.
:::

[comment]: # ({/1e409f69-cdd68340})

[comment]: # ({5e2aa8cf-982e2546})
#### Environnement local

Notez que le serveur requiert un environnement local UTF-8 afin que les éléments textuels puissent être interprétés correctement. La plupart des systèmes modernes de type Unix ont un paramètre régional UTF-8 par défaut, cependant, certains systèmes peuvent avoir besoin d'être définis spécifiquement.

[comment]: # ({/5e2aa8cf-982e2546})
