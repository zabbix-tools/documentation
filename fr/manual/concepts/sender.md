[comment]: # translation:outdated

[comment]: # ({new-43ef458c})
# 5 Sender

[comment]: # ({/new-43ef458c})

[comment]: # ({new-836aa47c})
#### Aperçu

Le sender Zabbix est un utilitaire de ligne de commande qui peut être
utilisé pour envoyer des données de performance au serveur Zabbix pour
traitement.

L'utilitaire est généralement utilisé dans les scripts utilisateur à
exécution longue pour l'envoi périodique de données de disponibilité et
de performance.

Pour l'envoi des résultats directement au serveur ou proxy Zabbix, un
type d'[élément trapper](/fr/manual/config/items/itemtypes/trapper) doit
être configuré.

[comment]: # ({/new-836aa47c})

[comment]: # ({new-fce06a66})
#### Exécution du sender Zabbix

Un exemple d’exécution d’un sender Zabbix sous UNIX :

    shell> cd bin
    shell> ./zabbix_sender -z zabbix -s "Linux DB3" -k db.connections -o 43

où :

-   z - Hôte du serveur Zabbix (l’adresse IP peut aussi être utilisée)
-   s - Nom technique de l’hôte surveillé (comme enregistré dans
    l’interface Web de Zabbix)
-   k - Clé de l’élément
-   o - Valeur à envoyer

::: noteimportant
Les options qui contiennent des espaces doivent
être positionnées entre guillemets. 
:::

Le sender Zabbix peut être utilisé pour envoyer plusieurs valeurs à
partir d'un fichier d'entrée. Voir la [page de manuel du sender
Zabbix](/fr/manpages/zabbix_sender) pour plus d'informations.

Le sender Zabbix accepte les chaînes au format UTF-8 (pour les systèmes
de type UNIX et Windows) sans BOM (byte order mark) en début de fichier.

Le sender Zabbix sur Windows peut être exécuté de la manière suivante :

    zabbix_sender.exe [options]

Depuis Zabbix 1.8.4, les scénarios d'envoi en temps réel de
zabbix\_sender ont été améliorés pour rassembler plusieurs valeurs qui
lui sont passées successivement et les envoyer au serveur en une seule
connexion. Une valeur qui n'est pas plus éloignée que la valeur
précédente de 0,2 seconde peut être placée dans la même pile, mais la
durée maximale de la mise en pool est toujours de 1 seconde.

::: noteclassic
Le sender Zabbix se termine si une entrée de paramètre non
valide (absence de notation *paramètre=valeur*) est présente dans le
fichier de configuration spécifié.
:::

[comment]: # ({/new-fce06a66})
