[comment]: # translation:outdated

[comment]: # ({new-f9458f4e})
# 3 Proxy

[comment]: # ({/new-f9458f4e})

[comment]: # ({new-05d8de8a})
#### Aperçu

Le proxy Zabbix est un processus qui peut collecter des données de
surveillance à partir d'un ou plusieurs équipements surveillés et
envoyer les informations au serveur Zabbix, en travaillant
essentiellement pour le compte du serveur. Toutes les données collectées
sont bufferisées localement puis transférées au serveur Zabbix auquel
appartient le proxy.

Le déploiement d'un proxy est facultatif, mais peut s'avérer très utile
pour répartir la charge d'un seul serveur Zabbix. Si seuls les proxys
collectent des données, le traitement sur le serveur devient moins
gourmand en CPU et en E/S disque.

Un proxy Zabbix est la solution idéale pour la surveillance centralisée
des sites distants, des succursales et des réseaux sans administrateurs
locaux.

Le proxy Zabbix nécessite une base de données séparée.

::: noteimportant
 Notez que les bases de données supportées avec le
proxy Zabbix sont SQLite, MySQL et PostgreSQL. L'utilisation d'Oracle ou
d'IBM DB2 est à vos propres risques et peut contenir certaines
limitations, par exemple, dans les [valeurs de
retour](/fr/manual/discovery/low_level_discovery#overview) des règles de
découverte de bas niveau.
:::

Voir aussi : [Utilisation des proxys dans un environnement
distribué](/fr/manual/distributed_monitoring/proxies)

[comment]: # ({/new-05d8de8a})

[comment]: # ({new-709824a5})
#### Processus Proxy

[comment]: # ({/new-709824a5})

[comment]: # ({new-a0a6c8d1})
##### Si installé en tant que package

Le proxy Zabbix s'exécute en tant que processus démon. Le proxy peut
être démarré en exécutant :

    shell> service zabbix-proxy start

Cela fonctionnera sur la plupart des systems GNU/Linux. Sur les autres
systèmes vous devrez exécuter :

    shell> /etc/init.d/zabbix-proxy start

De la même manière, pour arrêter/redémarrer/voir le statut du proxy
Zabbix, utilisez les commandes suivantes :

    shell> service zabbix-proxy stop
    shell> service zabbix-proxy restart
    shell> service zabbix-proxy status

[comment]: # ({/new-a0a6c8d1})

[comment]: # ({new-0b35181b})
##### Démarrage manuel

Si les commandes précédentes ne fonctionnent pas, vous devrez démarrer
le proxy manuellement. Trouvez le chemin des binaires de zabbix\_proxy
et exécutez :

    shell> zabbix_proxy

Vous pouvez utilizer les paramètres de lignes de commande suivants avec
le proxy Zabbix :

    -c --config <file>              chemin du fichier de configuration
    -R --runtime-control <option>   effectue des fonctions administratives
    -h --help                       affiche cette aide
    -V --version                    affiche le numéro de version

::: noteclassic
Le runtime control n’est pas supporté sur OpenBSD et
NetBSD.
:::

Exemples d’exécution du proxy Zabbix avec des paramètres de ligne de
commande :

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf
    shell> zabbix_proxy --help
    shell> zabbix_proxy -V             

[comment]: # ({/new-0b35181b})

[comment]: # ({new-ee4dd6f0})
##### Runtime control

Options du runtime control :

|Option|Description|Cible|
|------|-----------|-----|
|config\_cache\_reload|Recharge le cache de configuration. Ignoré si le cache est en cours de chargement.<br>Le proxy actif Zabbix se connectera au serveur Zabbix pour demande les données de configuration.|<|
|housekeeper\_execute|Démarre la procédure de nettoyage. Ignoré si la procédure de nettoyage est en cours d'exécution.|<|
|log\_level\_increase\[=<**target**>\]|Augmente le niveau de journalisation, affecte tous les processus si la cible n'est pas spécifiée.|**pid** - Process identifier (1 to 65535)<br>**process type** - Tous les processus du type spécifié (e.g., poller)<br>**process type,N** - Type de processus et numéro (e.g., poller,3)|
|log\_level\_decrease\[=<**target**>\]|Diminue le niveau de journalisation, affecte tous les processus si la cible n'est pas spécifiée.|^|

Notez que la plage utilisable de PID pour modifier le niveau de log d'un
seul processus Zabbix est comprise entre 1 et 65535. Sur les systèmes
dotés de PID volumineux, la cible <process type, N> peut être
utilisée pour modifier le niveau de journalisation d'un seul processus.

Exemple d’utilisation du runtime control pour recharger le cache de
configuration du proxy :

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R config_cache_reload

Exemple d’utilisation du runtime control pour déclencher l’exécution du
nettoyage :

    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R housekeeper_execute

Exemples d’utilisation du runtime control pour changer le niveau de
journalisation :

    Augmente le niveau de journalisation de tous les processus :
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase

    Augmente le niveau de journalisation du second processus poller:
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=poller,2

    Augmente le niveau de journalisation du processus ayant le PID 1234 :
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=1234

    Diminue le niveau de journalisation de tous les processus http poller :
    shell> zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_decrease="http poller"

[comment]: # ({/new-ee4dd6f0})

[comment]: # ({new-a9af55d3})
##### Processus utilisateur

Le proxy Zabbix est conçu pour s'exécuter en tant qu'utilisateur non
root. Il fonctionnera comme n'importe quel utilisateur non-root. Vous
pouvez donc exécuter le proxy comme n'importe quel utilisateur non root
sans aucun problème.

Si vous essayez de l'exécuter en tant que 'root', il passera à un
utilisateur 'zabbix' codé en dur, qui doit être présent sur votre
système. Vous pouvez uniquement exécuter le proxy en tant que 'root' si
vous modifiez le paramètre 'AllowRoot' dans le fichier de configuration
du proxy associé.

[comment]: # ({/new-a9af55d3})

[comment]: # ({new-441f5ec7})
##### Fichier de configuration

Voir les options du [fichier de
configuration](/fr/manual/appendix/config/zabbix_proxy) pour les détails
sur la configuration de zabbix\_proxy.

[comment]: # ({/new-441f5ec7})

[comment]: # ({new-2cd46511})
#### Plateformes supportées

Le proxy Zabbix s’exécute sur les mêmes
[server\#plateformes supportées](/fr/manual/concepts/server#plateformes supportées)
que le serveur Zabbix.

[comment]: # ({/new-2cd46511})

[comment]: # ({new-087c822f})
#### Environnement Local

Notez que le proxy requiert un environnement local UTF-8 afin que
certains éléments textuels puissent être interprétés correctement. La
plupart des systèmes modernes de type Unix ont un paramètre régional
UTF-8 par défaut, cependant, certains systèmes peuvent avoir besoin
d'être définis spécifiquement.

[comment]: # ({/new-087c822f})

[comment]: # ({new-c703b792})
#### Locale

Note that the proxy requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need
to be set specifically.

[comment]: # ({/new-c703b792})
