<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/concepts/proxy.md">
    <body>
      <trans-unit id="f9458f4e" xml:space="preserve">
        <source># 4 Proxy</source>
      </trans-unit>
      <trans-unit id="05d8de8a" xml:space="preserve">
        <source>#### Overview

Zabbix proxy is a process that may collect monitoring data from one or
more monitored devices and send the information to the Zabbix server,
essentially working on behalf of the server. All collected data is
buffered locally and then transferred to the Zabbix server the proxy
belongs to.

Deploying a proxy is optional, but may be very beneficial to distribute
the load of a single Zabbix server. If only proxies collect data,
processing on the server becomes less CPU and disk I/O hungry.

A Zabbix proxy is the ideal solution for centralized monitoring of
remote locations, branches and networks with no local administrators.

Zabbix proxy requires a separate database.

::: noteimportant
Note that databases supported with Zabbix proxy
are SQLite, MySQL and PostgreSQL. Using Oracle is at your own risk and
may contain some limitations as, for example, in [return
values](/manual/discovery/low_level_discovery#overview) of low-level
discovery rules.
:::

See also: [Using proxies in a distributed
environment](/manual/distributed_monitoring/proxies)</source>
      </trans-unit>
      <trans-unit id="709824a5" xml:space="preserve">
        <source>#### Running proxy</source>
      </trans-unit>
      <trans-unit id="a0a6c8d1" xml:space="preserve">
        <source>##### If installed as package

Zabbix proxy runs as a daemon process. The proxy can be started by
executing:

    service zabbix-proxy start

This will work on most of GNU/Linux systems. On other systems you may
need to run:

    /etc/init.d/zabbix-proxy start

Similarly, for stopping/restarting/viewing status of Zabbix proxy, use
the following commands:

    service zabbix-proxy stop
    service zabbix-proxy restart
    service zabbix-proxy status</source>
      </trans-unit>
      <trans-unit id="0b35181b" xml:space="preserve">
        <source>##### Start up manually

If the above does not work you have to start it manually. Find the path
to the zabbix\_proxy binary and execute:

    zabbix_proxy

You can use the following command line parameters with Zabbix proxy:

    -c --config &lt;file&gt;              path to the configuration file
    -f --foreground                 run Zabbix proxy in foreground
    -R --runtime-control &lt;option&gt;   perform administrative functions
    -h --help                       give this help
    -V --version                    display version number

Examples of running Zabbix proxy with command line parameters:

    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf
    zabbix_proxy --help
    zabbix_proxy -V</source>
      </trans-unit>
      <trans-unit id="ee4dd6f0" xml:space="preserve">
        <source>##### Runtime control

Runtime control options:

|Option|Description|Target|
|--|------|------|
|config\_cache\_reload|Reload configuration cache. Ignored if cache is being currently loaded.&lt;br&gt;Active Zabbix proxy will connect to the Zabbix server and request configuration data.&lt;br&gt;Passive Zabbix proxy will request configuration data from Zabbix server the next time when the server connects to the proxy.| |
|diaginfo\[=&lt;**section**&gt;\]|Gather diagnostic information in the proxy log file.|**historycache** - history cache statistics&lt;br&gt;**preprocessing** - preprocessing manager statistics&lt;br&gt;**locks** - list of mutexes (is empty on **BSD* systems)|
|snmp\_cache\_reload|Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.| |
|housekeeper\_execute|Start the housekeeping procedure. Ignored if the housekeeping procedure is currently in progress.| |
|log\_level\_increase\[=&lt;**target**&gt;\]|Increase log level, affects all processes if target is not specified. &lt;br&gt;Not supported on **BSD* systems.|**process type** - All processes of specified type (e.g., poller)&lt;br&gt;See all [proxy process types](#proxy_process_types).&lt;br&gt;**process type,N** - Process type and number (e.g., poller,3)&lt;br&gt;**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|
|log\_level\_decrease\[=&lt;**target**&gt;\]|Decrease log level, affects all processes if target is not specified.&lt;br&gt;Not supported on **BSD* systems.|^|
|prof\_enable\[=&lt;**target**&gt;\]|Enable profiling.&lt;br&gt;Affects all processes if target is not specified.&lt;br&gt;Enabled profiling provides details of all rwlocks/mutexes by function name.|**process type** - All processes of specified type (e.g., history syncer)&lt;br&gt;See all [proxy process types](#proxy_process_types).&lt;br&gt;**process type,N** - Process type and number (e.g., history syncer,1)&lt;br&gt;**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.&lt;br&gt;**scope** - `rwlock`, `mutex`, `processing` can be used with the process type and number (e.g., history syncer,1,processing) or all processes of type (e.g., history syncer,rwlock)|
|prof\_disable\[=&lt;**target**&gt;\]|Disable profiling.&lt;br&gt;Affects all processes if target is not specified.|**process type** - All processes of specified type (e.g., history syncer)&lt;br&gt;See all [proxy process types](#proxy_process_types).&lt;br&gt;**process type,N** - Process type and number (e.g., history syncer,1)&lt;br&gt;**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|

Example of using runtime control to reload the proxy configuration
cache:

    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R config_cache_reload

Examples of using runtime control to gather diagnostic information:

    # Gather all available diagnostic information in the proxy log file:
    zabbix_proxy -R diaginfo

    # Gather history cache statistics in the proxy log file:
    zabbix_proxy -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    zabbix_proxy -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper

    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    # Increase log level of all processes:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase

    # Increase log level of second poller process:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=poller,2

    # Increase log level of process with PID 1234:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_increase=1234

    # Decrease log level of all http poller processes:
    zabbix_proxy -c /usr/local/etc/zabbix_proxy.conf -R log_level_decrease="http poller"</source>
      </trans-unit>
      <trans-unit id="a9af55d3" xml:space="preserve">
        <source>##### Process user

Zabbix proxy is designed to run as a non-root user. It will run as
whatever non-root user it is started as. So you can run proxy as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be present on your system. You can only run
proxy as 'root' if you modify the 'AllowRoot' parameter in the proxy
configuration file accordingly.</source>
      </trans-unit>
      <trans-unit id="441f5ec7" xml:space="preserve">
        <source>##### Configuration file

See the [configuration file](/manual/appendix/config/zabbix_proxy)
options for details on configuring zabbix\_proxy.</source>
      </trans-unit>
      <trans-unit id="2cd46511" xml:space="preserve">
        <source>#### Proxy process types

-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `data sender` - proxy data sender
-   `discovery manager` - manager process for discovery of devices
-   `discovery worker` - process for handling discovery tasks from the discovery manager
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `odbc poller` - poller for ODBC checks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `self-monitoring` - process for collecting internal server
    statistics
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g. close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `trapper` - trapper for active checks, traps, proxy communication
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The proxy log file can be used to observe these process types.

Various types of Zabbix proxy processes can be monitored using the
**zabbix\[process,&lt;type&gt;,&lt;mode&gt;,&lt;state&gt;\]** internal
[item](/manual/config/items/itemtypes/internal).</source>
      </trans-unit>
      <trans-unit id="087c822f" xml:space="preserve">
        <source>#### Supported platforms

Zabbix proxy runs on the same list of
[supported platforms](/manual/concepts/server#supported_platforms)
as Zabbix server.</source>
      </trans-unit>
      <trans-unit id="c703b792" xml:space="preserve">
        <source>#### Locale

Note that the proxy requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need to be set specifically.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
