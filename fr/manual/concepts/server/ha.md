[comment]: # translation:outdated

[comment]: # ({0ef1b8c4-fb8f0d5a})
# 1 Cluster haute disponibilité

[comment]: # ({/0ef1b8c4-fb8f0d5a})

[comment]: # ({acb32688-ea8bc3db})
#### Aperçu

Le mode haute disponibilité offre une protection contre les pannes logiciels/matériels des serveur Zabbix et permet de minimiser les temps d'arrêt des maintenances logicielles/matérielles.

Le cluster haute disponibilité (HA) est une solution opt-in et il est supporté pour le serveur Zabbix. La solution HA native est conçue pour être simple d'utilisation, elle fonctionnera à travers vos différents sites et n'a pas d'exigences spécifiques pour les bases de données reconnues par Zabbix. Les utilisateurs sont libres d'utiliser la solution HA native Zabbix, ou une solution HA tierce, en fonction de ce qui convient le mieux aux exigences de haute disponibilité de leur environnement.

La solution consiste en plusieurs instances ou nœuds zabbix\_server.
Chaque nœud :

- est configuré séparément (fichier de configuration, scripts, chiffrement, exportation de données)
- utilise la même base de données
- dispose de plusieurs modes : actif (active), veille (standby), indisponible (unavailable), arrêté (stopped)

Un seul nœud peut être actif à la fois. Les nœuds de secours ne font pas de collecte, de traitement de données ou autres activités régulières du serveur ; ils n'écoutent pas sur les ports ; ils ont un minimum de connexions à la base de données.

Les nœuds actifs et en veille mettent à jour leur heure de dernier accès toutes les 5
secondes. Chaque nœud de secours surveille l'heure du dernier accès du nœud actif. Si le dernier temps d'accès au nœud actif est supérieur au 'délai de basculement' ('failover delay') en secondes, le nœud en veille bascule en nœud actif et attribue le statut 'indisponible' au nœud précédemment actif.

Le nœud actif surveille sa propre connectivité à la base de données - s'il est perdu
pendant plus de `failover delay-5` secondes, il doit arrêter tout traitement
et passer en mode veille. Le nœud actif surveille également l'état des nœuds en veille - si le dernier temps d'accès d'un nœud en veille est supérieur au 'délai de basculement' en secondes, le nœud en veille se voit attribuer l'état 'indisponible'.

Le délai de basculement est configurable, le minimum étant de 10 secondes.

Les nœuds sont conçus pour être compatibles entre versions mineures de Zabbix.

[comment]: # ({/acb32688-ea8bc3db})

[comment]: # ({cfd96af2-567d3671})
#### Activation du cluster HA

[comment]: # ({/cfd96af2-567d3671})

[comment]: # ({f8ed55a0-23e81771})
##### Configuration du serveur

Pour transformer n'importe quel serveur Zabbix d'un serveur autonome en un noeud du cluster HA, il faudra spécifier le paramètre HANodeName dans le [fichier de configuration du serveur](/manual/appendix/config/zabbix_server).

Le paramètre NodeAddress (adresse:port), s'il est défini, doit être utilisé par le
frontal pour le nœud actif, remplaçant la valeur dans zabbix.conf.php.

[comment]: # ({/f8ed55a0-23e81771})

[comment]: # ({new-3e9035e5})
##### Preparing frontend

Make sure that Zabbix server address:port is **not defined** in the 
frontend configuration.

Zabbix frontend will autodetect the active node by reading settings 
from the nodes table in Zabbix database. Node address of the active node 
will be used as the Zabbix server address.

[comment]: # ({/new-3e9035e5})

[comment]: # ({5d08f20e-d47b6ef3})
##### Configuration du proxy

Pour activer les connexions à plusieurs serveurs dans une configuration en haute disponibilité, répertorier les adresses des nœuds HA dans le [paramètre](/manual/appendix/config/zabbix_proxy) Server du proxy, séparées par un point-virgule.

[comment]: # ({/5d08f20e-d47b6ef3})

[comment]: # ({ae746357-f2fc0e77})
##### Configuration des agents

Pour activer les connexions à plusieurs serveurs dans une configuration en haute disponibilité, répertorier les adresses des nœuds HA dans le [paramètre](/manual/appendix/config/zabbix_agentd) ServerActive de l'agent, séparées par un point-virgule.

[comment]: # ({/ae746357-f2fc0e77})

[comment]: # ({new-311341fc})
### Failover to standby node

Zabbix will fail over to another node automatically if the active node stops. There 
must be at least one node in standby status for the failover to happen.

How fast will the failover be? All nodes update their last access time (and status, if 
it is changed) every 5 seconds. So: 

-   If the active node shuts down and manages to report its status 
as "shut down", another node will take over within **5 seconds**.

-   If the active node shuts down/becomes unavailable without being able to update 
its status, standby nodes will wait for the **failover delay** + 5 seconds to take over

The failover delay is configurable, with the supported range between 10 seconds and 15 
minutes (one minute by default). To change the failover delay, you may run:

```
zabbix_server -R ha_set_failover_delay=5m
```

[comment]: # ({/new-311341fc})

[comment]: # ({new-593144b8})
#### Managing HA cluster

The current status of the HA cluster can be managed using the dedicated
[runtime control](/manual/concepts/server#runtime_control) options:

-   ha\_status - log HA cluster status in the Zabbix server log;
-   ha\_remove\_node=target - remove an HA node identified by its
    <target> - number of the node in the list (the number can be
    obtained from the output of running ha\_status). Note that
    active/standby nodes cannot be removed.
-   ha\_set\_failover\_delay=delay - set HA failover delay (time
    suffixes are supported, e.g. 10s, 1m)

Node status can be monitored:

-   in *Reports* → *[System
    information](/manual/web_interface/frontend_sections/reports/status_of_zabbix#status_of_ha_cluster_nodes)*
-   in the *System information* dashboard widget
-   using the `ha_status` runtime control option of the server (see
    above).

The `zabbix[cluster,discovery,nodes]` internal item can be used for node
discovery, as it returns a JSON with high availability node information.

[comment]: # ({/new-593144b8})

[comment]: # ({new-82cd7e56})
#### Disabling HA cluster

To disable a high availability cluster:

-   make backup copies of configuration files
-   stop standby nodes
-   remove the HANodeName parameter from the active primary server
-   restart the primary server (it will start in standalone mode)

[comment]: # ({/new-82cd7e56})

[comment]: # ({new-7d944a36})
### Upgrading HA cluster

To perform a major version upgrade for the HA nodes:

-   stop all nodes;
-   create a full database backup;
-   if the database uses replication make sure that all nodes are in sync and have no issues. Do not upgrade if replication is broken.
-   select a single node that will perform database upgrade, change its configuration to standalone mode by commenting out HANodeName and [upgrade](/manual/installation/upgrade) it;
-   make sure that database upgrade is fully completed (*System information* should display that Zabbix server is running);
-   restart the node in HA mode;
-   upgrade and start the rest of nodes (it is not required to change them to standalone mode as the database is already upgraded at this point).

In a minor version upgrade it is sufficient to upgrade the first node, make sure it has upgraded and running, and then start upgrade on the next node.

[comment]: # ({/new-7d944a36})

[comment]: # ({new-f4d3143a})

### Implementation details

The high availability (HA) cluster is an opt-in solution and it is
supported for Zabbix server. The native HA solution is designed to be
simple in use, it will work across sites and does not have specific
requirements for the databases that Zabbix recognizes. Users are free to
use the native Zabbix HA solution, or a third party HA solution,
depending on what best suits the high availability requirements in their
environment.

The solution consists of multiple zabbix\_server instances or nodes.
Every node:

-   is configured separately
-   uses the same database
-   may have several modes: active, standby, unavailable, stopped

Only one node can be active (working) at a time. A standby node runs only one 
process - the HA manager. A standby node does no data collection, 
processing or other regular server activities; they do not listen 
on ports; they have minimum database connections.

Both active and standby nodes update their last access time every 5
seconds. Each standby node monitors the last access time of the active
node. If the last access time of the active node is over 'failover
delay' seconds, the standby node switches itself to be the active node
and assigns 'unavailable' status to the previously active node.

The active node monitors its own database connectivity - if it is lost
for more than `failover delay-5` seconds, it must stop all processing
and switch to standby mode. The active node also monitors the status of
the standby nodes - if the last access time of a standby node is over
'failover delay' seconds, the standby node is assigned the 'unavailable'
status.

The failover delay is configurable, with the minimum being 10 seconds.

The nodes are designed to be compatible across minor Zabbix versions.

[comment]: # ({/new-f4d3143a})
