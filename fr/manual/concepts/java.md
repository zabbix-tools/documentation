[comment]: # translation:outdated

[comment]: # ({new-00e36b2a})
# 4 Passerelle Java

[comment]: # ({/new-00e36b2a})

[comment]: # ({new-02637e76})
#### Aperçu

Le support natif pour la surveillance des applications JMX existe sous
la forme d'un démon Zabbix appelé "Zabbix Java gateway", disponible
depuis Zabbix 2.0. La passerelle Java de Zabbix est un démon écrit en
Java. Pour connaître la valeur d'un compteur JMX particulier sur un
hôte, le serveur Zabbix interroge la passerelle Java, qui utilise l'API
de gestion JMX pour interroger l'application concernée à distance.
L'application n'a pas besoin de l’installation d’un logiciel
supplémentaire, il suffit de démarrer avec l'option
-Dcom.sun.management.jmxremote dans la ligne de commande.

La passerelle Java accepte la connexion entrante du serveur ou du proxy
Zabbix et ne peut être utilisée que comme un "proxy passif".
Contrairement au proxy Zabbix, il peut également être utilisé depuis le
proxy Zabbix (les proxys Zabbix ne peuvent pas être chaînés). L'accès à
chaque passerelle Java est configuré directement dans le fichier de
configuration du serveur Zabbix ou du proxy, ainsi une seule passerelle
Java peut être configurée par serveur Zabbix ou par proxy Zabbix. Si un
hôte possède des éléments de type agent JMX et des éléments d'un autre
type, seuls les éléments de l'agent JMX sont transmis à la passerelle
Java pour être récupérés.

Lorsqu'un élément doit être mis à jour via la passerelle Java, le
serveur ou le proxy Zabbix se connecte à la passerelle Java et demande
la valeur, la passerelle Java la récupère à son tour et la renvoie au
serveur ou au proxy. En tant que tel, la passerelle Java ne garde aucune
valeur en cache.

Le serveur ou le proxy Zabbix a un type spécifique de processus qui se
connectent à la passerelle Java, contrôlée par l'option
**StartJavaPollers**. En interne, la passerelle Java démarre plusieurs
threads, contrôlés par l'option **START\_POLLERS**. Côté serveur, si une
connexion prend plus de **Timeout** secondes, elle sera terminée,
néanmoins la passerelle Java peut toujours être occupée à récupérer la
valeur du compteur JMX. Pour résoudre ce problème, depuis Zabbix 2.0.15,
Zabbix 2.2.10 et Zabbix 2.4.5, il existe l'option **TIMEOUT** dans la
passerelle Java qui permet de définir le délai d'attente pour les
opérations réseau JMX.

Le serveur ou le proxy Zabbix essaiera de regrouper autant que possible
les requêtes vers une seule cible JMX (affectées par les intervalles
d'éléments) et les enverra à la passerelle Java en une seule connexion
pour de meilleures performances.

Il est suggéré d'avoir le paramètre **StartJavaPollers** inférieur ou
égal à **START\_POLLERS**, sinon il peut y avoir des situations où aucun
thread n'est disponible dans la passerelle Java pour traiter les
demandes entrantes.

Les sections suivantes décrivent comment obtenir et exécuter la
passerelle Java Zabbix, comment configurer le serveur Zabbix (ou le
proxy Zabbix) pour utiliser la passerelle Java Zabbix pour la
surveillance JMX et comment configurer les éléments Zabbix dans
l'interface graphique Zabbix qui correspondent à des compteurs JMX
particuliers.

[comment]: # ({/new-02637e76})

[comment]: # ({new-05197937})
When an item has to be updated over Java gateway, Zabbix server or proxy
will connect to the Java gateway and request the value, which Java
gateway in turn retrieves and passes back to the server or proxy. As
such, Java gateway does not cache any values.

Zabbix server or proxy has a specific type of processes that connect to
Java gateway, controlled by the option **StartJavaPollers**. Internally,
Java gateway starts multiple threads, controlled by the
**START\_POLLERS** [option](/manual/appendix/config/zabbix_java). On the
server side, if a connection takes more than **Timeout** seconds, it
will be terminated, but Java gateway might still be busy retrieving
value from the JMX counter. To solve this, there is the **TIMEOUT**
option in Java gateway that allows to set timeout for JMX network
operations.

[comment]: # ({/new-05197937})

[comment]: # ({new-475ef799})

Zabbix server or proxy will try to pool requests to a single JMX target
together as much as possible (affected by item intervals) and send them
to the Java gateway in a single connection for better performance.

It is suggested to have **StartJavaPollers** less than or equal to
**START\_POLLERS**, otherwise there might be situations when no threads
are available in the Java gateway to service incoming requests; in such
a case Java gateway uses ThreadPoolExecutor.CallerRunsPolicy, meaning
that the main thread will service the incoming request and temporarily[label](https://git.zabbix.com/projects/WEB/repos/documentation/compare)
will not accept any new requests.

If you are trying to monitor Wildfly-based Java applications with Zabbix Java gateway, please install the latest jboss-client.jar available on the [Wildfly download page](https://www.wildfly.org/downloads/).

[comment]: # ({/new-475ef799})

[comment]: # ({new-d8d54db7})
#### - Obtenir la passerelle Java

Il existe deux façons d'obtenir une passerelle Java. L'un consiste à
télécharger le package « Java Gateway » du site Web Zabbix et l'autre à
compiler la passerelle Java à partir des sources.

##### - Télécharger depuis le site Web Zabbix

Les packages de la passerelle Java Zabbix (RHEL, Debian, Ubuntu) sont
disponibles pour le téléchargement sur
<http://www.zabbix.com/download.php>.

##### - Compilation depuis les sources

Pour compiler la passerelle Java, vous devez d'abord exécuter le script
`./configure` avec l'option `--enable-java`. Il est conseillé de
spécifier l'option `--prefix` pour positionner un chemin d'installation
autre que la valeur par défaut /usr/local, car l'installation de la
passerelle Java créera une arborescence de répertoires complète, pas
seulement un simple exécutable.

    $ ./configure --enable-java --prefix=$PREFIX

Pour compiler et packager la passerelle Java dans un fichier JAR,
exécutez `make`. Notez que pour cette étape, vous aurez besoin des
exécutables `javac` et `jar` dans votre chemin.

    $ make

Vous avez maintenant le fichier zabbix-java-gateway-$VERSION.jar dans
src/zabbix\_java/bin. Si vous êtes à l'aise avec l'exécution de la
passerelle Java à partir de src/zabbix\_java dans le répertoire de
distribution, vous pouvez alors passer aux instructions de configuration
et d'exécution de la passerelle Java. Sinon, assurez-vous de disposer de
suffisamment de privilèges et exécutez `make install`.

#### - Aperçu des fichiers dans la distribution de la passerelle Java

Peu importe la manière dont vous avez obtenu la passerelle Java, vous
devriez avoir une collection de scripts shell, de fichiers JAR et de
fichiers de configuration sous $PREFIX/sbin/zabbix\_java. Le rôle de ces
fichiers est résumé ci-dessous.

    bin/zabbix-java-gateway-$VERSION.jar

Le fichier JAR de la passerelle Java.

    lib/logback-core-0.9.27.jar
    lib/logback-classic-0.9.27.jar
    lib/slf4j-api-1.6.1.jar
    lib/android-json-4.3_r3.1.jar

Dépendances de la passerelle Java : [Logback](http://logback.qos.ch/),
[SLF4J](http://www.slf4j.org/), et la librairie [Android
JSON](https://android.googlesource.com/platform/libcore/+/master/json).

    lib/logback.xml  
    lib/logback-console.xml

Fichier de configuration du Logback.

    shutdown.sh  
    startup.sh

Scripts pratiques pour démarrer et arrêter la passerelle Java.

    settings.sh

Fichier de configuration provenant des scripts de démarrage et d'arrêt
ci-dessus.

#### - Configuration et exécution de la passerelle Java

Par défaut, la passerelle Java écoute sur le port 10052. Si vous
envisagez d'exécuter la passerelle Java sur un port différent, vous
pouvez le spécifier dans le script settings.sh. Voir la description du
[fichier de configuration de la passerelle
Java](/fr/manual/appendix/config/zabbix_java) pour savoir comment
spécifier cette option et d'autres options.

::: notewarning
Le port 10052 n'est pas [enregistré par
l'IANA](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.txt).
:::

Une fois que vous êtes à l'aise avec les paramètres, vous pouvez
démarrer la passerelle Java en exécutant le script de démarrage :

Once you are comfortable with the settings, you can start Java gateway
by running the startup script:

    $ ./startup.sh

De même, une fois que vous n'avez plus besoin de la passerelle Java,
exécutez le script d'arrêt pour l'arrêter :

    $ ./shutdown.sh

Notez que, contrairement au serveur ou au proxy, la passerelle Java est
légère et n'a pas besoin de base de données.

#### - Configuration du serveur pour l'utilisation de la passerelle Java

Maintenant que la passerelle Java est en cours d'exécution, vous devez
indiquer au serveur Zabbix où trouver la passerelle Java Zabbix. Ceci
est fait en spécifiant les paramètres JavaGateway et JavaGatewayPort
dans le [fichier de configuration du
serveur](/fr/manual/appendix/config/zabbix_server). Si l'hôte sur lequel
l'application JMX s'exécute est surveillé par le proxy Zabbix, vous
spécifiez les paramètres de connexion dans le [fichier de configuration
du proxy](/fr/manual/appendix/config/zabbix_proxy) à la place du
serveur.

    JavaGateway=192.168.3.14
    JavaGatewayPort=10052

Par défaut, le serveur ne démarre aucun processus lié à la surveillance
JMX. Toutefois, si vous souhaitez l'utiliser, vous devez spécifier le
nombre d'instances pré-forkées des pollers Java. Vous faites cela de la
même manière que vous spécifiez les pollers et les trappeurs.

    StartJavaPollers=5

N'oubliez pas de redémarrer le serveur ou le proxy, une fois que vous
avez fini de les configurer.

#### - Débogage de la passerelle Java

Dans le cas où il y aurait des problèmes avec la passerelle Java ou que
vous voyez un message d'erreur sur un élément dans l’interface
utilisateur qui n'est pas assez descriptif, vous pouvez jeter un œil au
fichier de logs de la passerelle Java.

Par défaut, la passerelle Java consigne ses activités dans le fichier
/tmp/zabbix\_java.log avec le niveau de journalisation "info". Parfois,
cette information n'est pas suffisante et il y a un besoin
d'informations au niveau "debug". Pour augmenter le niveau de
journalisation, modifiez le fichier lib/logback.xml et remplacez
l'attribut «level» de la balise <root> par "debug" :

    <root level="debug">
      <appender-ref ref="FILE" />
    </root>

Notez qu'à la différence du serveur ou du proxy Zabbix, il n'est pas
nécessaire de redémarrer la passerelle Java Zabbix après avoir modifié
le fichier logback.xml - les changements dans le fichier logback.xml
seront automatiquement récupérés. Lorsque vous avez terminé le débogage,
vous pouvez remettre le niveau de journalisation à "info".

Si vous souhaitez enregistrer la journalisation dans un fichier
différent ou dans un support complètement différent comme un base de
données, ajustez le fichier logback.xml pour répondre à vos besoins.
Voir le [manuel de logback](http://logback.qos.ch/manual/) pour plus de
détails.

Parfois, à des fins de débogage, il est utile de démarrer la passerelle
Java en tant qu'application en mode console plutôt qu'en tant que démon.
Pour ce faire, commentez la variable PID\_FILE dans settings.sh. Si
PID\_FILE est omis, le script startup.sh démarre la passerelle Java en
tant qu'application en mode console et fait que Logback utilise à la
place le fichier lib/logback-console.xml, qui non seulement se connecte
à la console, mais possède également un niveau de journalisation à
"debug" activé.

Enfin, notez que puisque la passerelle Java utilise SLF4J pour la
journalisation, vous pouvez remplacer Logback avec le framework de votre
choix en plaçant un fichier JAR approprié dans le répertoire lib. Voir
le [manuel de SLF4J](http://www.slf4j.org/manual.html) pour plus de
détails.

[comment]: # ({/new-d8d54db7})
