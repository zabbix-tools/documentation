[comment]: # translation:outdated

[comment]: # ({new-bade5a47})
# 4 Aperçu de Zabbix

[comment]: # ({/new-bade5a47})

[comment]: # ({09c2c4f2-b4d6d531})
#### Architecture

Zabbix se compose de plusieurs composants logiciels majeurs, dont les responsabilités sont décrites ci-dessous.

[comment]: # ({/09c2c4f2-b4d6d531})

[comment]: # ({0fc9db98-5957a1ba})
##### Serveur

Le [Serveur Zabbix](/manual/concepts/server) est le composant central auquel les agents envoient leur disponibilité, les informations d’intégrité et les statistiques. Le serveur est le référentiel central dans lequel toutes les données de configuration, statistiques et données opérationnelles sont stockées.

[comment]: # ({/0fc9db98-5957a1ba})

[comment]: # ({3f602980-494fb840})
##### Stockage des données

Toutes les informations de configuration ainsi que les données collectées par Zabbix sont stockées dans une base de données.

[comment]: # ({/3f602980-494fb840})

[comment]: # ({877be4ea-bd637ffa})
##### Interface Web

L’interface Web est fournie pour permettre un accès facile à Zabbix de n'importe où et de n'importe quelle plate-forme. L'interface fait partie du serveur Zabbix et fonctionne généralement (mais pas nécessairement) sur la même machine physique que celle qui exécute le serveur.

[comment]: # ({/877be4ea-bd637ffa})

[comment]: # ({09ffb01c-bfd730af})
##### Proxy

Le [Proxy Zabbix](/manual/concepts/proxy) peut collecter des données de performance et de disponibilité au nom du serveur Zabbix. Un proxy est un composant facultatif du déploiement de Zabbix. Cependant, il peut être très bénéfique de distribuer la charge d'un seul serveur Zabbix.

[comment]: # ({/09ffb01c-bfd730af})

[comment]: # ({45f659af-b9991630})
##### Agent

Les Agents Zabbix sont déployés sur des cibles de surveillance pour superviser activement les ressources locales et les applications, et envoyer les données collectées au serveur Zabbix.

Depuis Zabbix 4.4, il existe deux types d'agents disponibles : l'[Agent Zabbix](/manual/concepts/agent) (léger, pris en charge sur de nombreuses plates-formes, écrit en C) et l'[Agent Zabbix 2](/manual/concepts/agent) (extra-flexible, facilement extensible avec des plugins, écrit en Go).

[comment]: # ({/45f659af-b9991630})

[comment]: # ({affa06d9-9ec23332})
#### Flux de données

En outre, il est important de prendre du recul et de jeter un coup d'œil au flux de données global au sein de Zabbix. Pour créer un élément qui rassemble des données, vous devez d'abord créer un hôte. En passant à l'autre extrémité du spectre Zabbix, vous devez d'abord avoir un élément pour créer un déclencheur. Vous devez avoir un déclencheur pour créer une action. Ainsi, si vous voulez recevoir une alerte quand votre charge CPU est trop élevée sur le *Serveur X*, vous devez d'abord créer une entrée hôte pour le *Serveur X* suivi d'un élément pour surveiller son CPU, puis un déclencheur qui s'activera si le CPU est trop chargé, suivi par une action qui vous envoie un email. Bien que cela puisse sembler beaucoup d’étapes, avec l'utilisation des modèles, ce n'est pas vraiment le cas. Cependant, grâce à cette conception, il est possible de créer une configuration très flexible.

[comment]: # ({/affa06d9-9ec23332})
