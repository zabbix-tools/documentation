[comment]: # translation:outdated

[comment]: # ({new-09ce3b2e})
# 3 Fonctionnalités Zabbix

[comment]: # ({/new-09ce3b2e})

[comment]: # ({dcbb76fd-a70701e6})
#### Aperçu

Zabbix est une solution de supervision de réseaux hautement intégrée, offrant une multitude de fonctionnalités dans un seul package

**[Collecte de données](/manual/config/items)**

-   disponibilité et contrôles de performance
-   prise en charge de SNMP (réception et interrogation), IPMI, JMX, et de la surveillance VMware
-   vérifications personnalisées
-   collecte des données souhaitées à des intervalles personnalisés
-   effectuée par le serveur/proxy et par des agents

**[Définitions de seuils flexibles](/manual/config/triggers)**

-   vous pouvez définir des seuils de problèmes très flexibles, appelés déclencheurs, référençant des valeurs à partir de la base de données principale

**[Alertes hautement personnalisables](/manual/config/notifications)**

-   l'envoi de notifications peut être personnalisé selon le calendrier d'escalade, le destinataire ou le type de média
-   les notifications peuvent être significatives et utiles en utilisant des variables macro
-   les actions automatiques incluent des commandes à distance

**[Graphiques en temps réel](/manual/config/visualization/graphs/simple)**

-   les éléments supervisés sont immédiatement représentés graphiquement à l'aide de la fonctionnalité graphique intégrée

**[Capacités de supervision Web](/manual/web_monitoring)**

-   Zabbix peut suivre un chemin de clics de souris simulés sur un site Web et ainsi vérifier le bon fonctionnement et le temps de réponse

**[Options de visualisation étendues](/manual/config/visualization)**

-   possibilité de créer des graphiques personnalisés pouvant combiner plusieurs éléments dans une même vue
-   représentation du réseau sous forme de carte
-   des diaporamas pour un aperçu dans un tableau de bord
-   des rapports
-   vue de haut niveau (métier) des ressources surveillées

**[Stockage de données historiques](/manual/installation/requirements#database_size)**

-   données stockées dans une base de données
-   historique configurable
-   procédure de nettoyage intégrée

**[Configuration facile](/manual/config/hosts)**

-   ajouter des équipements surveillés en tant qu'hôtes
-   les hôtes sont pris en charge pour la surveillance, une fois dans la base de données
-   appliquer des modèles aux équipements surveillés

**[Utilisation de modèles](/manual/config/templates)**

-   grouper les vérifications dans des modèles
-   les modèles peuvent hériter d'autres modèles

**[Découverte du réseau](/manual/discovery)**

-   découverte automatique des équipements réseaux
-   enregistrement automatique de l'agent
-   découverte de systèmes de fichiers, d'interfaces réseau et d'OID SNMP

**[Interface Web rapide](/manual/web_interface)**

-   une interface web en PHP
-   accessible de n'importe où
-   la navigation se fait via des clics
-   journal d'audit

**[API Zabbix](/manual/api)**

-   L'API Zabbix fournit une interface programmable pour des manipulations de masse sur Zabbix, l'intégration de logiciels tiers et plus encore..

**[Système de permissions](/manual/config/users_and_usergroups)**

-   authentification utilisateur sécurisée
-   certains utilisateurs peuvent être limités à certaines vues

**[Agent complet et facilement extensible](/manual/concepts/agent)**

-   déployé sur des cibles de surveillance
-   peut être déployé sur Linux et Windows

**[Démons binaires](/manual/concepts/server)**

-   écrit en C, pour des performances et une faible empreinte mémoire
-   facilement portable

**[Prêt pour les environnements complexes](/manual/distributed_monitoring)**

-   supervision à distance facilitée en utilisant un proxy Zabbix

[comment]: # ({/dcbb76fd-a70701e6})
