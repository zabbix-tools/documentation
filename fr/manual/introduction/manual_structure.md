[comment]: # translation:outdated

[comment]: # ({3726d75e-cbbea730})
Structure du manuel

[comment]: # ({/3726d75e-cbbea730})

[comment]: # ({b2cbeb6b-14bd850a})
#### Structure

Le contenu de ce manuel est divisé en sections et
sous-sections pour fournir un accès facile à des sujets d'intérêt
particuliers.

Lorsque vous naviguez vers les sections respectives, assurez-vous que
vous développez des dossiers de section pour révéler le contenu complet
de ce qui est inclus en sous-sections et en pages individuelles.

Des liens croisés entre les pages dont le contenu est lié sont fournis
autant que possible pour assurer que les informations pertinentes ne
soient pas manquées par les utilisateurs.

[comment]: # ({/b2cbeb6b-14bd850a})

[comment]: # ({d3eb0350-4f79bd48})
#### Sections

L'[Introduction](about) fournit des informations générales sur la version en cours du logiciel Zabbix. La lecture de cette section devrait vous donner quelques bonnes raisons de choisir Zabbix.

Les [concepts Zabbix](/manual/definitions) expliquent la terminologie utilisée dans Zabbix et fournissent des détails sur les composants Zabbix.

Les sections [Installation](/manual/installation/getting_zabbix) et [Démarrage rapide](/manual/quickstart/item) devraient vous aider à démarrer avec Zabbix.

L'[Appliance Zabbix](/manuel/appliance) est une alternative pour avoir une idée rapide de ce que permet l'utilisation de Zabbix.

La section [Configuration](/manual/config) est l'une des sections les plus imposantes et les plus importantes de ce manuel. Elle contient nombre de conseils essentiels sur la configuration de Zabbix pour surveiller votre environnement, depuis la configuration des hôtes jusqu'à l'obtention des données essentielles en passant par la visualisation des données, la configuration des notifications et les commandes distantes à exécuter en cas de problèmes.

La section des [Services IT](/manual/it_services) explique comment utiliser Zabbix pour obtenir une vue d'ensemble de haut niveau de votre environnement de surveillance.

La section [Supervision Web](/manual/web_monitoring) devrait vous apprendre à
surveiller efficacement la disponibilité de vos sites Web.

La section [Surveillance des machines virtuelles](/manual/vm_monitoring) présente un guide pratique
pour configurer la surveillance d'un environnement VMware.

Les sections [Maintenance](/manual/maintenance), [Expressions régulières](/manual/acknowledges), [Acquittement d'événements](/manual/acknowledges) et [Import/Export XML](/manual/xml_export_import) sont d'autres précieuses sections qui révèlent comment utiliser ces différents aspects du logiciel Zabbix.

La section [Découverte](/manual/discovery) contient des instructions pour configurer la détection automatique des équipements réseaux, des agents actifs, des systèmes de fichiers, des interfaces réseau, etc.

La [Supervision distribuée](/manual/distributed_monitoring) traite des possibilités d'utilisation de Zabbix dans des environnements plus vastes et plus complexes.

La section [Chiffrement](/manual/encryption) aide à expliquer les possibilités de cryptage des communications entre les composants Zabbix.

La section [Interface Web](/manual/web_interface) contient des informations spécifiques pour en l'utilisation de l'interface web de Zabbix.

La section [API](/manual/api) présente les détails de l'utilisation de l'API Zabbix.

Des listes d'informations techniques détaillées sont incluses en [Annexes](/manuel/appendix). Vous y trouverez également une FAQ

[comment]: # ({/d3eb0350-4f79bd48})
