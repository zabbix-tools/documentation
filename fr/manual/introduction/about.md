[comment]: # translation:outdated

[comment]: # ({new-761c4a6f})
# 2 Qu'est-ce que Zabbix ?

[comment]: # ({/new-761c4a6f})

[comment]: # ({d6092b2c-319fca18})
### Aperçu

Zabbix a été créé par Alexei Vladishev, et est actuellement activement développé et maintenu par ZABBIX SIA.

Zabbix est une solution de supervision professionnelle libre de droit.

Zabbix est un logiciel qui permet de superviser de nombreux paramètres d'un réseau ainsi que la santé et l'intégrité des serveurs, des machines virtuelles, des applications, des services, des bases de données, des sites web, du cloud et plus encore. Zabbix utilise un mécanisme de notification flexible qui permet aux utilisateurs de configurer une alerte e-mail pour possiblement tout événement. Ceci permet une réponse rapide aux problèmes serveurs. Zabbix offre un excellent reporting et des fonctionnalités de visualisation de données basées sur les données stockées. Cela rend Zabbix idéal pour la gestion de la capacité.

Zabbix supporte à la fois l’interrogation (polling) et la réception (trapping) de données. Tous les rapports et statistiques de Zabbix, comme la configuration de paramètres, sont accessibles par l'interface web. L'interface web veille à ce que le statut de votre réseau et de vos serveurs puisse être évalué depuis n'importe quel endroit. Correctement configuré, Zabbix peux jouer un rôle important dans la supervision de l'infrastructure IT. Ceci est vrai pour les petites organisations avec peu de serveurs ainsi que pour les grandes entreprises avec une multitude de serveurs.

Zabbix est gratuit. Zabbix est écrit et distribué sous licence GPL (General Public License) version 2. Cela signifie que son code source est librement distribué et disponible pour le public.

Un [Support commercial](http://www.zabbix.com/support.php) est disponible et est fourni par Zabbix Company, ainsi que par tous ses partenaires à travers le monde.

En apprendre plus sur les [Fonctionnalités Zabbix](features).

[comment]: # ({/d6092b2c-319fca18})

[comment]: # ({94a2c08f-46ee774b})
#### Les utilisateurs Zabbix

De nombreuses entreprises de toutes tailles à travers le monde comptent sur Zabbix comme plate-forme de supervision principale.

[comment]: # ({/94a2c08f-46ee774b})
