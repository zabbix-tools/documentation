[comment]: # translation:outdated

[comment]: # ({new-ffc9e78e})
# 6 Nouveau modèle

[comment]: # ({/new-ffc9e78e})

[comment]: # ({new-3bb3ba5d})
#### Aperçu

Dans cette section, vous allez apprendre comment configurer un modèle.

Précédemment nous avons appris comment configurer un élément, un
déclencheur et comment recevoir une notification de problème pour un
hôte.

Bien que toutes ces étapes offrent une grande souplesse en elles-mêmes,
il peut sembler nécessaire de prendre de nombreuses mesures pour, par
exemple, un millier d'hôtes. Une certaine automatisation serait
pratique.

C'est là que les modèles viennent en aide. Les modèles permettent de
regrouper les éléments utiles, les déclencheurs et d'autres entités afin
que ceux-ci puissent être réutilisés à plusieurs reprises en
l'appliquant aux hôtes en une seule étape.

Lorsqu'un modèle est lié à un hôte, l'hôte hérite de toutes les entités
du modèle. Donc, une poignée de vérifications préparées à l'avance peut
être appliquée très rapidement.

[comment]: # ({/new-3bb3ba5d})

[comment]: # ({new-f182767e})
#### Ajout d'un modèle

Pour commencer à travailler avec les modèles, nous devons d'abord en
créer un. Pour faire cela, dans *Configuration → Modèles* cliquez sur
*Créer un modèle*. Cela nous présentera un formulaire de configuration
de modèle.\
![](../../../assets/en/manual/quickstart/new_template.png){width="550"}

Tous les champs obligatoires sont marqués avec un astérisque rouge.

Les paramètres obligatoire à renseigner ici sont :

***Nom du molèle***

-   Entrez un nom de modèle. Les caractères alpha-numeriques, les
    espaces et les underscores sont autorisés.

***Groupes***

-   Sélectionnez un ou plusieurs groupes en cliquant sur le bouton
    *Sélectionner*. Le modèle doit obligatoirement appartenir à un
    groupe.

Quand cela est fait, cliquez sur *Ajouter*. Votre nouveau modèle devrait
être visible dans la liste des modèles.

![](../../../assets/en/manual/quickstart/template_list.png){width="600"}

Comme vous pouvez le constater, le modèle est apparu, mais il ne
contient rien - pas d'élément, pas de déclencheur ni aucune autre
entité.

[comment]: # ({/new-f182767e})

[comment]: # ({new-ad341c27})
#### Ajout d'éléments à un modèle

Pour ajouter un élément à un modèle, allez dans la liste d'éléments pour
'Nouvel hôte'. Dans *Configuration → Hôtes* cliquez sur *Elément* à côté
de next to 'Nouvel hôte'.

Puis :

-   cochez la case de l'élément 'Charge CPU' dans la liste
-   cliquez sur *Copier* sous la liste
-   sélectionnez le modèle dans lequel vous souhaitez copier l'élément\

![](../../../assets/en/manual/quickstart/copy_to_template.png)

Tous les champs obligatoires sont marqués avec un astérisque rouge.

-   cliquez sur *Copier*

Si vous allez maintenant dans *Configuration → Modèles*, 'Nouveau
modèle' devrait contenir un nouvel élément.

Nous nous arrêterons à un seul élément pour le moment, mais de même,
vous pouvez ajouter d'autres éléments, déclencheurs ou autres entités au
modèle jusqu'à ce qu'il s'agisse d'un ensemble assez complet d'entités
spécifiques (surveillance du système d'exploitation, surveillance d'une
seule application).

[comment]: # ({/new-ad341c27})

[comment]: # ({new-230cf552})
#### Lier un modèle à un hôte

Avec un modèle prêt, il ne reste plus qu'à l'ajouter à un hôte. Pour
cela, accédez à *Configuration → Hôtes*, cliquez sur 'Nouvel hôte' pour
ouvrir sa fiche de propriétés et accédez à l'onglet **Modèles**.

Là, cliquez sur *Sélectionner* à côté de *Lier de nouveaux modèles*.
Dans la fenêtre contextuelle, cliquez sur le nom du modèle que nous
avons créé ('Nouveau modèle'). Comme il apparaît dans le champ *Lier de
nouveaux modèles*, cliquez sur *Ajouter*. Le modèle doit apparaître dans
la liste *Modèles liés*.

![](../../../assets/en/manual/quickstart/link_template.png)

Cliquez sur *Mettre à jour* dans le formulaire pour enregistrer les
modifications. Le modèle est maintenant ajouté à l'hôte, avec toutes les
entités qu'il contient.

Comme vous l'avez peut-être deviné, cette méthode peut également être
appliquée à n'importe quel autre hôte. Toute modification apportée aux
éléments, aux déclencheurs et aux autres entités au niveau du modèle se
propagera aux hôtes auxquels le modèle est lié.

[comment]: # ({/new-230cf552})

[comment]: # ({new-c7f1a5c4})
##### Lier des modèles prédéfinis à des hôtes

Comme vous l'avez peut-être remarqué, Zabbix est fourni avec un ensemble
de modèles prédéfinis pour divers systèmes d'exploitation, équipements
et applications. Pour commencer à surveiller très rapidement, vous
pouvez associer le modèle le plus approprié à un hôte, mais attention,
ces modèles doivent être adaptés à votre environnement. Certaines
vérifications peuvent ne pas être nécessaires et les intervalles
d'interrogation peuvent être trop fréquents.

Plus d'informations sur les [modèles](/fr/manual/config/templates) sont
disponibles.

[comment]: # ({/new-c7f1a5c4})
