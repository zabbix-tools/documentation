[comment]: # translation:outdated

[comment]: # ({new-8cf8bd5f})
# 3 Nouvel élément

[comment]: # ({/new-8cf8bd5f})

[comment]: # ({new-73502ab6})
#### Aperçu

Dans cette section, vous apprendrez comment configure un élément.

Les éléments sont la base de la collecte de données dans Zabbix. Sans
éléments, il n'y a pas de données - car seul un élément définit une
seule mesure ou les données à retirer d'un hôte.

[comment]: # ({/new-73502ab6})

[comment]: # ({new-8cca3d66})
#### Ajout d’élément

Tous les éléments sont regroupés autour des hôtes. C'est pourquoi, pour
configurer un exemple d’élément, nous allons dans *Configuration →
Hôtes* et trouvons le 'Nouvel hôte' que nous avons créé.

Le lien *Eléments* dans la ligne 'Nouvel hôte' doit afficher un compte
de '0'. Cliquez sur le lien, puis cliquez sur *Créer un élément*. Cela
nous présentera un formulaire de définition d’élément.\
![](../../../assets/en/manual/quickstart/new_item.png){width="600"}

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Pour notre exemple d’élément, les informations essentielles à renseigner
sont :

***Nom***

-   Entrez *Charge CPU* comme valeur. Ce sera le nom affiché de
    l’élément dans la liste et partout ailleurs.

***Clé***

-   Entrez manuellement *system.cpu.load* comme valeur. Ceci est un nom
    technique d'un élément qui identifie le type d'informations qui
    seront recueillies. La clé particulière est juste une parmi de
    nombreuses [clés
    prédéfinies](/fr/manual/config/items/itemtypes/zabbix_agent)
    fournies avec l'agent Zabbix.

***Type d’information***

-   Sélectionnez *Numerique (flottant)* ici. Cet attribute défini le
    format de la donnée attendue.

::: noteclassic
 Vous voudrez peut-être aussi réduire le nombre de jours
d’[historique de l’élément](/fr/manual/config/items/history_and_trends)
à garder, à 7 ou 14 jours. Ceci est une bonne pratique pour soulager la
base de données de garder beaucoup de valeurs historiques. 
:::

[Les autres options](/fr/manual/config/items/item#configuration) vous
conviendront avec leurs valeurs par défaut pour l’instant.

Lorsque vous avez terminé, cliquez sur *Ajouter*. Le nouvel élément doit
apparaître dans la liste d'éléments. Cliquez sur *Détails* au-dessus de
la liste pour voir ce qui a été fait exactement.

![](../../../assets/en/manual/quickstart/item_created.png)

[comment]: # ({/new-8cca3d66})

[comment]: # ({new-c33e07f1})
#### Visualiser les données

Avec un élément défini, vous pourriez être curieux de savoir s'il
collecte des données. Pour cela, allez dans *Surveillance → Dernières
données*, sélectionnez 'Nouvel hôte' dans le filtre et cliquez sur
*Appliquer*.

Puis cliquez sur **+** avant **- autre -** et attendez que votre article
soit là et affiche des données.

![](../../../assets/en/manual/quickstart/latest_data.png){width="600"}

Cela dit, les premières données peuvent prendre jusqu'à 60 secondes pour
arriver. Par défaut, c'est la fréquence à laquelle le serveur lit les
modifications de configuration et sélectionne les nouveaux éléments à
exécuter.

Si vous ne voyez aucune valeur dans la colonne 'Modifier', peut-être
qu'une seule valeur a été reçue jusqu'à présent. Attendez 30 secondes
pour qu'une autre valeur arrive.

Si vous ne voyez pas d'informations sur l'élément comme dans la capture
d'écran, assurez-vous que :

-   vous avez saisi les champs "Clé" et "Type d'information" exactement
    comme dans la capture d'écran
-   L'agent et le serveur sont en cours d'exécution
-   Le statut de l'hôte est 'Surveillé' et son icône de disponibilité
    est verte
-   l'hôte est sélectionné dans la liste déroulante de l'hôte, l'élément
    est actif

[comment]: # ({/new-c33e07f1})

[comment]: # ({new-7e0cbbcd})
##### Graphiques

Avec l'élément fonctionnant pendant un moment, il pourrait être temps de
voir quelque chose de visuel. Des [graphiques
simples](/fr/manual/config/visualisation/graphs/simple) sont disponibles
pour tout élément numérique surveillé sans configuration supplémentaire.
Ces graphiques sont générés à l'exécution.

Pour voir le graphique, allez à *Surveillance → Dernières données* et
cliquez sur le lien 'Graphique' à côté de l'élément.

![](../../../assets/en/manual/quickstart/simple_graph1.png){width="600"}

[comment]: # ({/new-7e0cbbcd})
