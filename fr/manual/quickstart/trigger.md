[comment]: # translation:outdated

[comment]: # ({new-848667e6})
# 4 Nouveau déclencheur

[comment]: # ({/new-848667e6})

[comment]: # ({new-97bd6e32})
#### Aperçu

Dans cette section, vous apprendrez comment configure un déclencheur.

Seuls les éléments collectent les données. Pour évaluer automatiquement
les données entrantes nous avons besoin de définir un déclencheur. Un
déclencheur contient une expression qui défini un seuil de niveau de
données acceptable.

Si ce niveau est dépassé par la données entrante, un déclencheur
s’activera pour passera en état de ‘problème’ – pour nous faire savoir
qu'il s'est passé quelque chose qui pourrait nécessiter de l'attention.
Si le niveau est de nouveau acceptable, le déclencheur revient à l'état
'Ok'.

[comment]: # ({/new-97bd6e32})

[comment]: # ({new-e69902f4})
#### Ajout d’un déclencheur

Pour configure un déclencheur à notre élément, allez dans *Configuration
→ Hôtes*, trouvez 'Nouvel hôte' et cliquez sur *Déclencheurs* à côté et
sur *Créer un déclencheur*. Cela nous présent un formulaire de
définition de déclencheur.\
![](../../../assets/en/manual/quickstart/new_trigger_a.png)

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Pour notre déclencheur, les informations essentielles à renseigner sont
:

*Nom*

-   Entrez : *Charge CPU trop élevée sur 'Nouvel hôte' depuis 3 minutes*
    comme valeur. Ce sera le nom du déclencheur affiché dans les listes
    et partout ailleurs.

*Expression*

-   Entrez : {Nouvel hôte:system.cpu.load.avg(3m)}>2

C'est l'expression de déclenchement. Assurez-vous que l'expression est
renseignée correctement, jusqu'au dernier symbole. La clé d’élément ici
(system.cpu.load) est utilisée pour faire référence à l’élément. Cette
expression particulière indique essentiellement que le seuil du problème
est dépassé lorsque la valeur moyenne de charge du processeur pendant 3
minutes est supérieure à 2. Vous pouvez en apprendre plus sur la
[syntaxe des expressions des
déclencheurs](/manual/config/triggers/expression).

Lorsque vous avez terminé, cliquez sur *Ajouter*. Le nouveau déclencheur
devrait apparaître dans la liste des déclencheurs.

[comment]: # ({/new-e69902f4})

[comment]: # ({new-7a7f221f})
#### Affichage du statut des déclencheurs

Avec un déclencheur défini, vous serez interessé de voir son statut.

Si la charge CPU a dépassé le seuil que vous avez défini pour le
déclencheur, le problème sera affiché dans *Surveillance → Problèmes*.

![](../../../assets/en/manual/quickstart/trigger_status1.png){width="600"}

Le clignotement indique un changement récent de l'état du déclencheur,
celui qui a eu lieu au cours des 30 dernières minutes.

[comment]: # ({/new-7a7f221f})
