[comment]: # translation:outdated

[comment]: # ({new-4c8ef099})
# 5 Réception des notifications de problèmes

[comment]: # ({/new-4c8ef099})

[comment]: # ({new-055c3bc8})
#### Aperçu

Dans cette section, vous apprendrez comment configurer les alertes sous
la forme de notifications dans Zabbix.

Avec des éléments collectant des données et des déclencheurs conçus pour
«déclencher» des situations problématiques, il serait également utile de
disposer d'un mécanisme d'alerte qui nous informerait des événements
importants même si nous ne regardons pas directement l’interface de
Zabbix.

C'est ce que les notifications font. L'e-mail étant la méthode de
livraison la plus populaire pour les notifications de problèmes, nous
allons apprendre à configurer une notification par e-mail.

[comment]: # ({/new-055c3bc8})

[comment]: # ({new-237d8e2e})
#### Paramètres e-mail

Initialement, il y a plusieurs [méthodes de
livraison](/fr/manual/config/notifications/media) de notifications
prédéfinies dans Zabbix.
L’[e-mail](/fr/manual/config/notifications/media/email) est l’une
d’entre elles.

Pour configurer les paramètres e-mail, allez dans *Administration →
Types de Media* et cliquez sur *Email* dans la liste de media types
prédéfinis.

![](../../../assets/en/manual/quickstart/media_types.png){width="600"}

Cela nous présentera le formulaire de définition des paramètres de
messagerie.\
![](../../../assets/en/manual/quickstart/media_type_email.png)

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Définissez les valeurs du serveur SMTP, SMTP helo et SMTP e-mail en
fonction de votre environnement.

::: noteclassic
 'Email SMTP' sera utilisé comme l'adresse 'De' pour les
notifications envoyées par Zabbix. 
:::

Appuyez sur *Mise à jour* lorsque vous êtes prêt.

Vous avez maintenant configuré "E-mail" comme type de support. Un type
de support doit être lié aux utilisateurs en définissant des adresses de
livraison spécifiques (comme nous l'avons fait lors de la [configuration
d’un nouvel utilisateur](login#adding_user)), sinon il ne sera pas
utilisé.

[comment]: # ({/new-237d8e2e})

[comment]: # ({new-c1762601})
#### Nouvelle action

La livraison des notifications est l’une des choses que les
[actions](/manual/config/notifications/action) font dans Zabbix. Par
conséquent, pour configurer une notification, allez dans *Configuration
→ Actions* et cliquez sur *Créer une action*.\
![](../../../assets/en/manual/quickstart/new_action1.png)

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Dans ce formulaire, entrez un nom pour l'action.

Dans le cas le plus simple, si nous n'ajoutons pas plus de
[conditions](/manual/config/notifications/action/conditions), l'action
sera prise lors de tout changement de déclencheur de 'Ok' à 'Problème'.

Nous devons toujours définir ce que l'action doit faire - et cela est
fait dans l'onglet *Opérations*. Cliquez sur *Nouveau* dans le bloc
Opérations, qui ouvre un nouveau formulaire d'opération.\
![](../../../assets/en/manual/quickstart/new_operation1.png){width="600"}

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Ici, cliquez sur *Ajouter* dans le bloc *Envoyer aux utilisateurs* et
sélectionnez l'utilisateur ('user') que nous avons défini. Sélectionnez
"E-mail" comme valeur pour *Envoyer uniquement vers*. Lorsque vous avez
terminé, cliquez sur *Ajouter* dans le bloc de détails de l'opération.

Les macros {TRIGGER.STATUS} et {TRIGGER.NAME} (ou variables), visibles
dans les champs *Objet par défaut* et *Message par défaut*, seront
remplacées par les valeurs actuelles de l'état du déclencheur et du nom
de déclencheur.

C'est tout pour une simple configuration d'action, donc cliquez
*Ajouter* dans le formulaire d'action.

[comment]: # ({/new-c1762601})

[comment]: # ({new-61999602})
#### Réception des notifications

Maintenant, avec la livraison des notifications configurées, il serait
amusant d'en recevoir une. Pour aider à cela, nous pouvons augmenter la
charge sur notre hôte de manière spécifique - de sorte que notre
déclencheur "se déclenche" et que nous recevions une notification de
problème.

Ouvrez la console sur votre hôte et exécutez :

    cat /dev/urandom | md5sum

Vous pouvez exécuter un ou plusieurs de [ces
processus](http://en.wikipedia.org/wiki/Md5sum).

Maintenant, allez dans *Surveillance → Dernières données* et regardez
comment ont augmenté les valeurs de 'Charge CPU'. Gardez en mémoire que
pour notre déclencheur s’active, la valeur de ‘Charge CPU’ doit être
supérieure à ‘2’ pendant 3 minutes. Quand cela arrive :

-   dans *Surveillance → Problèms* vous devriez voir le déclencheur avec
    un statut 'Problème' clignotant
-   vous devrirez recevoir une notification de problème dans vos
    e-mails.

::: noteimportant
Si les notifications ne fonctionnent pas :

-   verifies une nouvelle fois que les paramètres e-mail et l’action
    sont bien configurés.
-   assurez-vous que l’utilisateur que vous avez créé a au moins
    l’authorisation de lecture sur l’hôte qui a généré l’événement,
    comme note dans l’étape *[Ajout d’utilisateur](login#adding_user)*
    L'utilisateur faisant partie du groupe d'utilisateurs
    'Administrateurs Zabbix' doit avoir au moins un accès en lecture au
    groupe d'hôtes 'Serveurs Linux' auquel appartient notre hôte.

   \* En outre, vous pouvez consulter le journal des actions en allant
dans *Rapports → Journal des actions*. 
:::

[comment]: # ({/new-61999602})
