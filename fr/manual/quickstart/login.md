[comment]: # translation:outdated

[comment]: # ({new-3f54a0e9})
# 1 Identification et configuration des utilisateurs

[comment]: # ({/new-3f54a0e9})

[comment]: # ({new-f6cb6160})
#### Aperçu

Dans cette section vous apprendrez comment vous identifier et configurer
un utilisateur système dans Zabbix.

[comment]: # ({/new-f6cb6160})

[comment]: # ({new-6cb1478f})
#### Login

![](../../../assets/en/manual/quickstart/login.png){width="350"}

Il s'agit de l'écran "Bienvenue" de Zabbix. Entrez le nom d'utilisateur
**Admin** avec le mot de passe **zabbix** pour s'identifier comme un
[super-utilisateur
Zabbix](/fr/manual/config/users_and_usergroups/permissions).

Quand vous êtes identifié, vous verrez 'Connecté en tant qu'Admin' dans
le coin inférieur droit de la page. L'accès aux menus *Configuration* et
*Administration* sera autorisé.

[comment]: # ({/new-6cb1478f})

[comment]: # ({new-ecf369b8})
##### Protection contre les attaques par force brute

Dans le cas de cinq tentatives de connexion infructueuses consécutives,
l'interface Zabbix s'arrêtera pendant 30 secondes afin d'empêcher les
attaques par force brute et par dictionnaire.

L'adresse IP d'une tentative de connexion échouée sera affichée après
une connexion réussie.

[comment]: # ({/new-ecf369b8})

[comment]: # ({new-6be209b3})
#### Ajout d'un utilisateur

Pour voir les informations à propos des utilisateurs, allez dans
*Administration → Utilisateurs*.

![](../../../assets/en/manual/quickstart/userlist.png){width="600"}

Initialement, seuls deux utilisateurs sont définis dans Zabbix.

  \* L'utilisateur 'Admin' est un super-utilisateur Zabbix, qui a toutes
les permissions.   \* L'utilisateur 'invité' est un utilisateur spécial
par défaut. Si vous n'êtes pas connecté, vous accédez à Zabbix avec des
autorisations "invité". Par défaut, "invité" n'a aucune autorisation sur
les objets Zabbix.

Pour ajouter un nouvel utilisateur, cliquez sur *Créer un utilisateur*.

Dans le formulaire de nouvel utilisateur, assurez-vous d'ajouter votre
utilisateur dans l'un des [groupe
d'utilisateurs](/manual/config/users_and_usergroups/usergroup) existant,
par exemple 'Zabbix administrators'.

![](../../../assets/en/manual/quickstart/new_user.png)

Tous les champs de saisie obligatoires sont marqués avec un astérisque
rouge.

Par défaut, les nouveaux utilisateurs n'ont aucun média (méthodes de
livraison des notifications) défini pour eux. Pour en créer un, allez
dans l'onglet 'Media' et cliquez sur *Ajouter*.

![](../../../assets/en/manual/quickstart/new_media.png)

Dans cette fenêtre, entrez une adresse e-mail pour cet utilisateur.

Vous pouvez spécifier une période pendant laquelle le support sera actif
(voir la page [Spécification de
période](/fr/manual/appendix/time_period) pour la description du
format), par défaut un support est toujours actif. Vous pouvez également
personnaliser les niveaux de [gravité des
déclencheurs](/manual/config/triggers/severity) pour lesquels le support
sera actif, mais laissez-les tous activés pour le moment.

Cliquez sur *Ajouter*, puis sur *Ajouter* dans le formulaire des
propriétés de l'utilisateur. Le nouvel utilisateur apparaît dans la
liste des utilisateurs.

![](../../../assets/en/manual/quickstart/userlist2.png){width="600"}

[comment]: # ({/new-6be209b3})

[comment]: # ({new-6e8f46b1})
##### Ajout de permissions

Par défaut, un nouvel utilisateur n'a aucune permission pour accéder aux
hôtes. Pour accorder les droits à l'utilisateur, cliquez sur le groupe
de l'utilisateur dans la colonne *Groupes* (dans ce cas - 'Zabbix
administrators'). Dans le formulaire des propriétés du groupe, allez
dans l'onglet *Permissions*.

![](../../../assets/en/manual/quickstart/group_permissions.png){width="600"}

Cet utilisateur doit avoir un accès en lecture seule sur le groupe
*Linux servers*, donc, cliquez sur *Sélectionner* à côté du champs de
sélection du groupe d'utilisateur.

![](../../../assets/en/manual/quickstart/add_permissions.png)

Dans cette fenêtre, activez la case à côte de 'Linux servers', puis
cliquez sur *Sélectionner*. *Linux servers* devrait être affiché dans le
champs de sélection. Cliquez sur le bouton 'Lecture' pour attribuer le
niveau de permission et sur *Ajouter* pour ajouter le groupe dans la
liste des permissions. Dans le formulaire de propriétés du groupe
d'utilisateurs, cliquez sur *Mettre à jour*.

::: noteimportant
Dans Zabbix, les droits d'accès aux hôtes sont
assignés aux [groupes
d'utilisateurs](/fr/manual/config/users_and_usergroups/usergroup), pas
aux utilisateurs individuellement.
:::

Terminé ! Vous pouvez essayer de vous connecter en utilisant les
informations d'identification du nouvel utilisateur.

[comment]: # ({/new-6e8f46b1})
