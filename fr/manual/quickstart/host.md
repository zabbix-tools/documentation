[comment]: # translation:outdated

[comment]: # ({new-7905d51a})
# 2 Nouvel hôte

[comment]: # ({/new-7905d51a})

[comment]: # ({new-b1060785})
#### Aperçu

Dans cette section, vous apprendrez comment configure un nouvel hôte.

Un hôte dans Zabbix est une entité en réseau (physique, virtuelle) que
vous souhaitez superviser. La définition de ce qui peut être un "hôte"
dans Zabbix est assez flexible. Il peut s'agir d'un serveur physique,
d'un commutateur réseau, d'une machine virtuelle ou d'une application.

[comment]: # ({/new-b1060785})

[comment]: # ({new-9c1b7280})
#### Ajout d’un hôte

Les informations sur les hôtes configurés dans Zabbix sont disponibles
dans *Configuration → Hôtes*. Il y a déjà un hôte prédéfini, appelé
'serveur Zabbix', mais nous voulons apprendre à en ajouter un autre.

Pour ajouter un nouvel hôte, cliquez sur *Créer un hôte*. Cela nous
présentera un formulaire de configuration d'hôte.\
![](../../../assets/en/manual/quickstart/new_host.png){width="600"}

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Le strict minimum à renseigner ici est :

***Nom de l’hôte***

-   Entrez un nom d’hôte. Les caractères alphanumériques, les espaces,
    les points, les tirets et les underscores sont autorisés.

***Groupes***

-   Sélectionnez un ou plusieurs groupes existants en cliquant sur le
    bouton *Sélectionner* ou entrez un nom de groupe non existant pour
    créer un nouveau groupe.

::: noteclassic
 Toutes les autorisations d'accès sont attribuées à des
groupes d'hôtes et non à des hôtes individuels. C'est pourquoi un hôte
doit appartenir à au moins un groupe.
:::

***Adresse IP***

-   Entrez l'adresse IP de l'hôte. Notez que s'il s'agit de l'adresse IP
    du serveur Zabbix, elle doit être spécifiée dans la directive
    'Serveur' du fichier de configuration de l'agent Zabbix.

[Les autres options](/fr/manual/config/hosts/host#configuration) nous
conviendront avec leurs valeurs par défauts pour l'instant.

Lorsque vous avez terminé, cliquez sur *Ajouter*. Votre nouvel hôte
devrait être visible dans la liste des hôtes.

::: notetip
 Si l'icône *ZBX* de la colonne *Disponibilité* est
rouge, il y a une erreur de communication - déplacez le curseur de la
souris dessus pour voir le message d'erreur. Si cette icône est grise,
aucune mise à jour de statut n'a eu lieu jusqu'à présent. Vérifiez que
le serveur Zabbix est en cours d'exécution et essayez d'actualiser la
page ultérieurement. </ Note>

[comment]: # ({/new-9c1b7280})
