[comment]: # translation:outdated

[comment]: # ({new-c86bb2b9})
# 11. Maintenance

[comment]: # ({/new-c86bb2b9})

[comment]: # ({new-e70ea305})
#### Overview

You can define maintenance periods for hosts and host groups in Zabbix.
There are two maintenance types - with data collection and with no data
collection.

During a maintenance "with data collection" triggers are processed as
usual and events are created when required. However, problem escalations
are paused for hosts in maintenance, if the *Pause operations while in
maintenance* option is checked in action configuration. In this case,
escalation steps that may include sending notifications or remote
commands will be ignored for as long as the maintenance period lasts.

For example, if escalation steps are scheduled at 0, 30 and 60 minutes
after a problem start, and there is a half-hour long maintenance lasting
from 10 minutes to 40 minutes after a real problem arises, steps two and
three will be executed a half-hour later, or at 60 minutes and 90
minutes (providing the problem still exists). Similarly, if a problem
arises during the maintenance, the escalation will start after the
maintenance.

To receive problem notifications during the maintenance normally
(without delay), you have to uncheck the *Pause operations while in
maintenance* option in action configuration.

::: noteclassic
If at least one host (used in the trigger expression) is not
in maintenance mode, Zabbix will send a problem
notification.
:::

Zabbix server must be running during maintenance. Timer processes are
responsible for switching host status to/from maintenance at 0 seconds
of every minute. A proxy will always collect data regardless of the
maintenance type (including "no data" maintenance). The data is later
ignored by the server if 'no data collection' is set.

When "no data" maintenance ends, triggers using nodata() function will
not fire before the next check during the period they are checking.

If a log item is added while a host is in maintenance and the
maintenance ends, only new logfile entries since the end of the
maintenance will be gathered.

If a timestamped value is sent for a host that is in a “no data”
maintenance type (e.g. using [Zabbix sender](/manpages/zabbix_sender))
then this value will be dropped however it is possible to send a
timestamped value in for an expired maintenance period and it will be
accepted.

::: noteimportant
To ensure predictable behaviour of recurring
maintenance periods (daily, weekly, monthly), it is required to use a
common timezone for all parts of Zabbix.
:::

[comment]: # ({/new-e70ea305})


[comment]: # ({new-6ef20399})
#### Configuration

To configure a maintenance period:

-   Go to: *Configuration → Maintenance*
-   Click on *Create maintenance period* (or on the name of an existing
    maintenance period)

The **Maintenance** tab contains general maintenance period attributes:

![](../../assets/en/manual/maintenance/maintenance.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Name*|Name of the maintenance period.|
|*Maintenance type*|Two types of maintenance can be set:<br>**With data collection** - data will be collected by the server during maintenance, triggers will be processed<br>**No data collection** - data will not be collected by the server during maintenance|
|*Active since*|The date and time when executing maintenance periods becomes active.<br>*Note*: Setting this time alone does not activate a maintenance period; for that go to the *Periods* tab.|
|*Active till*|The date and time when executing maintenance periods stops being active.|
|*Description*|Description of maintenance period.|

The **Periods** tab allows you to define the exact days and hours when
the maintenance takes place. Clicking on *New* opens a flexible
*Maintenance period* form where you can define the times - for daily,
weekly, monthly or one-time maintenance.

![](../../assets/en/manual/maintenance/maintenance2.png)

Daily and weekly periods have an *Every day/Every week* parameter, which
defaults to 1. Setting it to 2 would make the maintenance take place
every two days or every two weeks and so on. The starting day or week is
the day or week that *Active since* time falls on.

For example, having *Active since* set to 2013-09-06 12:00 and an hour
long daily recurrent period every two days at 23:00 will result in the
first maintenance period starting on 2013-09-06 at 23:00, while the
second maintenance period will start on 2013-09-08 at 23:00. Or, with
the same *Active since* time and an hour long daily recurrent period
every two days at 01:00, the first maintenance period will start on
2013-09-08 at 01:00, and the second maintenance period on 2013-09-10 at
01:00.

The **Hosts & Groups** tab allows you to select the hosts and host
groups for maintenance.

![](../../assets/en/manual/maintenance/maintenance3.png)

Specifying a parent host group implicitly selects all nested host
groups. Thus the maintenance will also be executed on hosts from nested
groups.

[comment]: # ({/new-6ef20399})

[comment]: # ({new-4dc2aa1c})
#### Display

An orange wrench icon next to the host name indicates that this host is
in maintenance in the *Monitoring → Dashboard*, *Monitoring → Triggers*
and *Inventory → Hosts → Host inventory details* sections.

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Maintenance details are displayed when the mouse pointer is positioned
over the icon.

::: noteclassic
The display of hosts in maintenance in the Dashboard can be
unset altogether with the dashboard filtering function.
:::

Additionally, hosts in maintenance get an orange background in
*Monitoring → Maps* and in *Configuration → Hosts* their status is
displayed as 'In maintenance'.

[comment]: # ({/new-4dc2aa1c})

[comment]: # ({new-6aeb0a4c})

::: noteimportant
When creating a maintenance period, the [time zone](/manual/web_interface/time_zone) of the user who creates it is used.
However, when recurring maintenance periods (*Daily*, *Weekly*, *Monthly*) are scheduled, the time zone of the Zabbix server is used.
To ensure predictable behavior of recurring maintenance periods, it is required to use a common time zone for all parts of Zabbix.
:::

[comment]: # ({/new-6aeb0a4c})

[comment]: # ({new-3b17f2f3})

When done, press *Add* to add the maintenance period to the *Periods* block.

Note that Daylight Saving Time (DST) changes do not affect how long the maintenance will be.
For example, let's say that we have a two-hour maintenance configured that usually starts at 01:00 and finishes at 03:00:

-   if after one hour of maintenance (at 02:00) a DST change happens and current time changes from 02:00 to 03:00, the maintenance will continue for one more hour (till 04:00);
-   if after two hours of maintenance (at 03:00) a DST change happens and current time changes from 03:00 to 02:00, the maintenance will stop, because two hours have passed;
-   if a maintenance period starts during the hour that is skipped by a DST change, then the maintenance will not start.

If a maintenance period is set to "1 day" (the actual period of the maintenance is 24 hours, since Zabbix calculates days in hours), starts at 00:00 and finishes at 00:00 the next day:

-   the maintenance will stop at 01:00 the next day if current time changes forward one hour;
-   the maintenance will stop at 23:00 that day if current time changes back one hour.

[comment]: # ({/new-3b17f2f3})

[comment]: # ({new-924e8c68})
#### Display

[comment]: # ({/new-924e8c68})

[comment]: # ({new-0e33672d})
##### Displaying hosts in maintenance

An orange wrench icon
![](../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png)
next to the host name indicates that this host is in maintenance in:

-   *Monitoring → Dashboard*
-   *Monitoring → Problems*
-   *Inventory → Hosts → Host inventory details*
-   *Configuration → Hosts* (See 'Status' column)

![](../../assets/en/manual/maintenance/maintenance_icon.png)

Maintenance details are displayed when the mouse pointer is positioned
over the icon.

Additionally, hosts in maintenance get an orange background in
*Monitoring → Maps*.

[comment]: # ({/new-0e33672d})

[comment]: # ({new-a923614c})
##### Displaying suppressed problems

Normally problems for hosts in maintenance are suppressed, i.e. not
displayed in the frontend. However, it is also possible to configure
that suppressed problems are shown, by selecting the *Show suppressed
problems* option in these locations:

-   *Monitoring* → *Dashboard* (in *Problem hosts*, *Problems*,
    *Problems by severity*, *Trigger overview* widget configuration)
-   *Monitoring* → *Problems* (in the filter)
-   *Monitoring* → *Maps* (in map configuration)
-   Global
    [notifications](/manual/web_interface/user_profile/global_notifications)
    (in user profile configuration)

When suppressed problems are displayed, the following icon is displayed:
![](../../assets/en/manual/web_interface/icon_suppressed.png). Rolling a
mouse over the icon displays more details:

![](../../assets/en/manual/web_interface/info_suppressed2.png)

[comment]: # ({/new-a923614c})
