[comment]: # translation:outdated

[comment]: # ({new-8674a4c3})
# Discovery rule

This class is designed to work with network discovery rules.

::: notetip
This API is meant to work with network discovery rules.
For the low-level discovery rules see the [LLD rule
API](discoveryrule).
:::

Object references:\

-   [Discovery
    rule](/fr/manual/api/reference/drule/object#discovery_rule)

Available methods:\

-   [drule.create](/fr/manual/api/reference/drule/create) - create new
    discovery rules
-   [drule.delete](/fr/manual/api/reference/drule/delete) - delete
    discovery rules
-   [drule.get](/fr/manual/api/reference/drule/get) - retrieve discovery
    rules
-   [drule.update](/fr/manual/api/reference/drule/update) - update
    discovery rules

[comment]: # ({/new-8674a4c3})
