[comment]: # translation:outdated

[comment]: # ({new-79c270c0})
# Map

This class is designed to work with maps.

Object references:\

-   [Map](/fr/manual/api/reference/map/object#map)
-   [Map element](/fr/manual/api/reference/map/object#map_element)
-   [Map link](/fr/manual/api/reference/map/object#map_link)
-   [Map URL](/fr/manual/api/reference/map/object#map_url)
-   [Map user](/fr/manual/api/reference/map/object#map_user)
-   [Map user group](/fr/manual/api/reference/map/object#map_user_group)
-   [Map shape](/fr/manual/api/reference/map/object#map_shapes)
-   [Map line](/fr/manual/api/reference/map/object#map_lines)

Available methods:\

-   [map.create](/fr/manual/api/reference/map/create) - create new maps
-   [map.delete](/fr/manual/api/reference/map/delete) - delete maps
-   [map.get](/fr/manual/api/reference/map/get) - retrieve maps
-   [map.update](/fr/manual/api/reference/map/update) - update maps

[comment]: # ({/new-79c270c0})
