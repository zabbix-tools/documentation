[comment]: # translation:outdated

[comment]: # ({new-830462e8})
# Service

This class is designed to work with services.

Object references:\

-   [Service](/fr/manual/api/reference/service/object#service)
-   [Service time](/fr/manual/api/reference/service/object#service_time)
-   [Service
    dependency](/fr/manual/api/reference/service/object#service_dependency)
-   [Service
    alarm](/fr/manual/api/reference/service/object#service_alarm)

Available methods:\

-   [service.adddependencies](/fr/manual/api/reference/service/adddependencies) -
    adding dependencies between IT services
-   [service.addtimes](/fr/manual/api/reference/service/addtimes) -
    adding service times
-   [service.create](/fr/manual/api/reference/service/create) - creating
    new IT services
-   [service.delete](/fr/manual/api/reference/service/delete) - deleting
    IT services
-   [service.deletedependencies](/fr/manual/api/reference/service/deletedependencies) -
    deleting dependencies between IT services
-   [service.deletetimes](/fr/manual/api/reference/service/deletetimes) -
    deleting service times
-   [service.get](/fr/manual/api/reference/service/get) - retrieving IT
    services
-   [service.getsla](/fr/manual/api/reference/service/getsla) -
    retrieving availability information about IT services
-   [service.update](/fr/manual/api/reference/service/update) - updating
    IT services

[comment]: # ({/new-830462e8})
