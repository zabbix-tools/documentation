[comment]: # translation:outdated

[comment]: # ({new-66fb9690})
# dashboard.update

[comment]: # ({/new-66fb9690})

[comment]: # ({new-f235159f})
### Description

`object dashboard.update(object/array dashboards)`

This method allows to update existing dashboards.

[comment]: # ({/new-f235159f})

[comment]: # ({new-62947aa0})
### Parameters

`(object/array)` Dashboard properties to be updated.

The `dashboardid` property must be defined for each dashboard, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard dashboard properties](object#dashboard),
the method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|widgets|array|[Dashboard widgets](object#dashboard_widget) to replace existing dashboard widgets.<br><br>Dashboard widgets are updated by `widgetid` property. Widgets without `widgetid` property will be created.|
|users|array|[Dashboard user](object#dashboard_user) shares to replace the existing elements.|
|userGroups|array|[Dashboard user group](object#dashboard_user_group) shares to replace the existing elements.|

[comment]: # ({/new-62947aa0})

[comment]: # ({new-28e89b38})
### Return values

`(object)` Returns an object containing the IDs of the updated
dashboards under the `dashboardids` property.

[comment]: # ({/new-28e89b38})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7fcf7de4})
#### Renaming a dashboard

Rename a dashboard to "SQL server status".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "name": "SQL server status"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7fcf7de4})

[comment]: # ({new-b8348b8a})
#### Change dashboard owner

Available only for admins and super admins.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "userid": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-b8348b8a})

[comment]: # ({new-e4a57dd3})
### See also

-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)

[comment]: # ({/new-e4a57dd3})

[comment]: # ({new-299ae089})
### Source

CDashboard::update() in
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-299ae089})

[comment]: # ({new-456ac32b})
### Source

CDashboard::update() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-456ac32b})
