[comment]: # translation:outdated

[comment]: # ({new-daa6729c})
# 18 Problems by severity

[comment]: # ({/new-daa6729c})

[comment]: # ({new-4956ccbf})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Problems by severity*](/manual/web_interface/frontend_sections/dashboards/widgets/problems_severity) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-4956ccbf})

[comment]: # ({new-6a1dfd61})
### Parameters

The following parameters are supported for the *Problems by severity* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.|
|*Exclude host groups*|<|2|exclude_groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To exclude multiple host groups, create a dashboard widget field object for each host group.|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.|
|*Problem*|<|1|problem|Problem [event name](/manual/config/triggers/trigger#configuration) (case insensitive, full name or part of it).|
|*Severity*|<|0|severities|0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>Default: 1, 2, 3, 4, 5 (all enabled).<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>Parameter *Tag name* required if configuring *Tags*.|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>Parameter *Operator* required if configuring *Tags*.|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>Parameter *Tag value* required if configuring *Tags*.|
|*Show*|<|0|show_type|0 - *(default)* Host groups;<br>1 - Totals.|
|*Layout*|<|0|layout|0 - *(default)* Horizontal;<br>1 - Vertical.<br><br>Parameter *Layout* not available if *Show* is set to "Host groups".|
|*Show operational data*|<|0|show_opdata|0 - *(default)* None;<br>1 - Separately;<br>2 - With problem name.|
|*Show suppressed problems*|<|0|show_suppressed|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Hide groups without problems*|<|0|hide_empty_groups|0 - *(default)* Disabled;<br>1 - Enabled.<br><br>Parameter *Hide groups without problems* not available if *Show* is set to "Totals".|
|*Problem display*|<|0|ext_ack|0 - *(default)* All;<br>1 - Unacknowledged only;<br>2 - Separated.|
|*Show timeline*|<|0|show_timeline|0 - Disabled;<br>1 - *(default)* Enabled.|

[comment]: # ({/new-6a1dfd61})

[comment]: # ({new-8d4d4242})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Problems by severity* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-8d4d4242})

[comment]: # ({new-4346af58})
#### Configuring a *Problems by severity* widget

Configure a *Problems by severity* widget that displays problem totals for all host groups.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problemsbysv",
                        "name": "Problems by severity",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "show_type",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4346af58})

[comment]: # ({new-aacca901})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-aacca901})
