[comment]: # translation:outdated

[comment]: # ({new-dfa1aefb})
# 17 Problems

[comment]: # ({/new-dfa1aefb})

[comment]: # ({new-c733131f})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Problems*](/manual/web_interface/frontend_sections/dashboards/widgets/problems) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-c733131f})

[comment]: # ({new-07abd3fb})
### Parameters

The following parameters are supported for the *Problems* widget.

|Parameter|<|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-|--------|--|--------|-------------------------------|
|*Refresh interval*|<|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - *(default)* 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - 15 minutes.|
|*Show*|<|0|show|1 - *(default)* Recent problems;<br>2 - History;<br>3 - Problems.|
|*Host groups*|<|2|groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To configure multiple host groups, create a dashboard widget field object for each host group.|
|*Exclude host groups*|<|2|exclude_groupids|[Host group](/manual/api/reference/hostgroup/get) ID.<br><br>Note: To exclude multiple host groups, create a dashboard widget field object for each host group.|
|*Hosts*|<|3|hostids|[Host](/manual/api/reference/host/get) ID.<br><br>Note: To configure multiple hosts, create a dashboard widget field object for each host. For multiple hosts, the parameter *Host groups* must either be not configured at all or configured with at least one host group that the configured hosts belong to.|
|*Problem*|<|1|problem|Problem [event name](/manual/config/triggers/trigger#configuration) (case insensitive, full name or part of it).|
|*Severity*|<|0|severities|0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.<br><br>Default: 1, 2, 3, 4, 5 (all enabled).<br><br>Note: To configure multiple values, create a dashboard widget field object for each value.|
|*Tags* (the number in the property name (e.g. tags.tag.0) references tag order in the tag evaluation list)|<|<|<|<|
|<|*Evaluation type*|0|evaltype|0 - *(default)* And/Or;<br>2 - Or.|
|^|*Tag name*|1|tags.tag.0|Any string value.<br><br>Parameter *Tag name* required if configuring *Tags*.|
|^|*Operator*|0|tags.operator.0|0 - Contains;<br>1 - Equals;<br>2 - Does not contain;<br>3 - Does not equal;<br>4 - Exists;<br>5 - Does not exist.<br><br>Parameter *Operator* required if configuring *Tags*.|
|^|*Tag value*|1|tags.value.0|Any string value.<br><br>Parameter *Tag value* required if configuring *Tags*.|
|*Show tags*|<|0|show_tags|0 - *(default)* None;<br>1 - 1;<br>2 - 2;<br>3 - 3.|
|*Tag name* (format)|<|0|tag_name_format|0 - *(default)* Full;<br>1 - Shortened;<br>2 - None.<br><br>Parameter *Tag name* (format) not available if *Show tags* is set to "None".|
|*Tag display priority*|<|1|tag_priority|Comma-separated list of tags.<br><br>Parameter *Tag display priority* not available if *Show tags* is set to "None".|
|*Show operational data*|<|0|show_opdata|0 - *(default)* None;<br>1 - Separately;<br>2 - With problem name.|
|*Show suppressed problems*|<|0|show_suppressed|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Show unacknowledged only*|<|0|unacknowledged|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Sort entries by*|<|0|sort_triggers|1 - Severity (descending);<br>2 - Host (ascending);<br>3 - Time (ascending);<br>4 - *(default)* Time (descending);<br>13 - Severity (ascending);<br>14 - Host (descending);<br>15 - Problem (ascending);<br>16 - Problem (descending).<br><br>For all values, except "Time (descending)" and "Time (ascending)", the parameter *Show timeline* must be set to "Disabled".|
|*Show timeline*|<|0|show_timeline|0 - Disabled;<br>1 - *(default)* Enabled.<br><br>Parameter *Show timeline* available if *Sort entries by* is set to "Time (descending)" or "Time (ascending)".|
|*Show lines*|<|0|show_lines|Valid values range from 1-100.<br><br>Default: 25.|

[comment]: # ({/new-07abd3fb})

[comment]: # ({new-c49bef65})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Problems* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-c49bef65})

[comment]: # ({new-1317b036})
#### Configuring a *Problems* widget

Configure a *Problems* widget that displays problems for host group "4" that satisfy the following conditions:

-   Problems that have a tag with the name "scope" that contains values "performance" or "availability", or "capacity".
-   Problems that have the following severities: "Warning", "Average", "High", "Disaster".

In addition, configure the widget to show tags and operational data.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problems",
                        "name": "Problems",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 2,
                                "name": "groupids",
                                "value": 4
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "performance"
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.1",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.1",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.1",
                                "value": "availability"
                            },
                            {
                                "type": 1,
                                "name": "tags.tag.2",
                                "value": "scope"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.2",
                                "value": 0
                            },
                            {
                                "type": 1,
                                "name": "tags.value.2",
                                "value": "capacity"
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 4
                            },
                            {
                                "type": 0,
                                "name": "severities",
                                "value": 5
                            },
                            {
                                "type": 0,
                                "name": "show_tags",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "show_opdata",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1317b036})

[comment]: # ({new-a778f187})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)

[comment]: # ({/new-a778f187})
