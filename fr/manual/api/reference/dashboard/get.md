[comment]: # translation:outdated

[comment]: # ({new-f7884974})
# dashboard.get

[comment]: # ({/new-f7884974})

[comment]: # ({new-8d36c9b5})
### Description

`integer/array dashboard.get(object parameters)`

The method allows to retrieve dashboards according to the given
parameters.

[comment]: # ({/new-8d36c9b5})

[comment]: # ({new-342bbd26})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dashboardids|string/array|Return only dashboards with the given IDs.|
|selectWidgets|query|Return the dashboard widgets that are used in the dashboard with in `widgets` property.|
|selectUsers|query|Returns users that the dashboard is shared with in `users` property.|
|selectUserGroups|query|Returns user groups that the dashboard is shared with in `userGroups` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible value is: `dashboardid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-342bbd26})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-84a10cd6})
#### Retrieving a dashboard by ID

Retrieve all data about dashboards "1" and "2".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.get",
    "params": {
        "output": "extend",
        "selectWidgets": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "dashboardids": [
            "1",
            "2"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dashboardid": "1",
            "name": "Dashboard",
            "userid": "1",
            "private": "0",
            "users": [],
            "userGroups": [],
            "widgets": [
                {
                    "widgetid": "9",
                    "type": "systeminfo",
                    "name": "",
                    "x": "6",
                    "y": "8",
                    "width": "6",
                    "height": "5",
                    "fields": []
                },
                {
                    "widgetid": "8",
                    "type": "problemsbysv",
                    "name": "",
                    "x": "6",
                    "y": "4",
                    "width": "6",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "7",
                    "type": "problemhosts",
                    "name": "",
                    "x": "6",
                    "y": "0",
                    "width": "6",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "6",
                    "type": "discovery",
                    "name": "",
                    "x": "3",
                    "y": "9",
                    "width": "3",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "5",
                    "type": "web",
                    "name": "",
                    "x": "0",
                    "y": "9",
                    "width": "3",
                    "height": "4",
                    "fields": []
                },
                {
                    "widgetid": "4",
                    "type": "problems",
                    "name": "",
                    "x": "0",
                    "y": "3",
                    "width": "6",
                    "height": "6",
                    "fields": []
                },
                {
                    "widgetid": "3",
                    "type": "favmaps",
                    "name": "",
                    "x": "4",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                },
                {
                    "widgetid": "2",
                    "type": "favscreens",
                    "name": "",
                    "x": "2",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                },
                {
                    "widgetid": "1",
                    "type": "favgraphs",
                    "name": "",
                    "x": "0",
                    "y": "0",
                    "width": "2",
                    "height": "3",
                    "fields": []
                }
            ]
        },
        {
            "dashboardid": "2",
            "name": "My dashboard",
            "userid": "1",
            "private": "1",
            "users": [
                {
                    "userid": "4",
                    "permission": "3"
                }
            ],
            "userGroups": [
                {
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "widgets": [
                {
                    "widgetid": "10",
                    "type": "problems",
                    "name": "",
                    "x": "0",
                    "y": "0",
                    "width": "6",
                    "height": "5",
                    "fields": [
                        {
                            "type": "2",
                            "name": "groupids",
                            "value": "4"
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84a10cd6})

[comment]: # ({new-299ae089})
### See also

-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)

[comment]: # ({/new-299ae089})

[comment]: # ({new-75a9042e})
### Source

CDashboard::get() in
*frontends/php/include/classes/api/services/CDashboard.php*.

[comment]: # ({/new-75a9042e})
