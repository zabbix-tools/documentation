[comment]: # translation:outdated

[comment]: # ({new-a79e0e24})
# > Dashboard object

The following objects are directly related to the `dashboard` API.

[comment]: # ({/new-a79e0e24})

[comment]: # ({new-63e4049e})
### Dashboard

The dashboard object has the following properties:

|Property|Type|Description|
|--------|----|-----------|
|dashboardid|string|*(readonly)* ID of the dashboard.|
|**name**<br>(required)|string|Name of the dashboard.|
|userid|string|Dashboard owner user ID.|
|private|integer|Type of dashboard sharing.<br><br>Possible values:<br>0 - public dashboard;<br>1 - *(default)* private dashboard.|

[comment]: # ({/new-63e4049e})

[comment]: # ({new-b88f0a6a})
### Widget de tableau de bord

The dashboard widget object has the following properties:

|Property|Type|Description|
|--------|----|-----------|
|widgetid|string|*(readonly)* ID of the dashboard widget.|
|**type**<br>(required)|string|Type of the dashboard widget.<br><br>Possible values:<br>actionlog - Action log;<br>clock - Clock;<br>dataover - Data overview;<br>discovery - Discovery status;<br>favgraphs - Favourite graphs;<br>favmaps - Favourite maps;<br>favscreens - Favourite screens;<br>graph - Graph;<br>problemhosts - Problem hosts;<br>map - Map;<br>navtree - Map Navigation Tree;<br>plaintext - Plain text;<br>problems - Problems;<br>systeminfo - System information;<br>problemsbysv - Problems by severity;<br>trigover - Trigger overview;<br>url - URL;<br>web - Web monitoring;|
|name|string|Custom widget name.|
|x|integer|A horizontal position from the left side of the dashboard.<br><br>Valid values range from 0 to 11.|
|y|integer|A vertical position from the top of the dashboard.<br><br>Valid values range from 0 to 63.|
|width|integer|The widget width.<br><br>Valid values range from 1 to 12.|
|height|integer|The widget height.<br><br>Valid values range from 1 to 32.|
|fields|array|Array of the [dashboard widget field](object#dashboard_widget_field) objects.|

[comment]: # ({/new-b88f0a6a})

[comment]: # ({new-2938b685})
### Dashboard widget field

The dashboard widget field object has the following properties:

|Property|Type|Description|
|--------|----|-----------|
|**type**<br>(required)|integer|Type of the widget field.<br><br>Possible values:<br>0 - Integer;<br>1 - String;<br>2 - Host group;<br>3 - Host;<br>4 - Item;<br>6 - Graph;<br>8 - Map;|
|name|string|Widget field name.|
|**value**<br>(required)|mixed|Widget field value depending of type.|

[comment]: # ({/new-2938b685})

[comment]: # ({new-5b20bc04})
### Dashboard user group

List of dashboard permissions based on user groups. It has the following
properties:

|Property|Type|Description|
|--------|----|-----------|
|**usrgrpid**<br>(required)|string|User group ID.|
|**permission**<br>(required)|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write;|

[comment]: # ({/new-5b20bc04})

[comment]: # ({new-5c8b3411})
### Dashboard user

List of dashboard permissions based on users. It has the following
properties:

|Property|Type|Description|
|--------|----|-----------|
|**userid**<br>(required)|string|User ID.|
|**permission**<br>(required)|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write;|

[comment]: # ({/new-5c8b3411})

[comment]: # ({new-d34afa0c})
### Dashboard user

List of dashboard permissions based on users. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**userid**<br>(required)|string|User ID.|
|**permission**<br>(required)|integer|Type of permission level.<br><br>Possible values:<br>2 - read only;<br>3 - read-write;|

[comment]: # ({/new-d34afa0c})
