[comment]: # translation:outdated

[comment]: # ({new-f92dbd28})
# User group

This class is designed to work with user groups.

Object references:\

-   [User group](/fr/manual/api/reference/usergroup/object#user_group)

Available methods:\

-   [usergroup.create](/fr/manual/api/reference/usergroup/create) -
    creating new user groups
-   [usergroup.delete](/fr/manual/api/reference/usergroup/delete) -
    deleting user groups
-   [usergroup.get](/fr/manual/api/reference/usergroup/get) - retrieving
    user groups
-   [usergroup.update](/fr/manual/api/reference/usergroup/update) -
    updating user groups

[comment]: # ({/new-f92dbd28})
