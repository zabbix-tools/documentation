[comment]: # translation:outdated

[comment]: # ({new-3c277f74})
# httptest.create

[comment]: # ({/new-3c277f74})

[comment]: # ({new-c4bddb28})
### Description

`object httptest.create(object/array webScenarios)`

This method allows to create new web scenarios.

::: noteclassic
Creating a web scenario will automatically create a set of
[web monitoring items](/fr/manual/web_monitoring/items).
:::

[comment]: # ({/new-c4bddb28})

[comment]: # ({new-5bd5a38b})
### Parameters

`(object/array)` Web scenarios to create.

Additionally to the [standard web scenario
properties](object#web_scenario), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**steps**<br>(required)|array|Web scenario steps.|

[comment]: # ({/new-5bd5a38b})

[comment]: # ({new-0f4da548})
### Return values

`(object)` Returns an object containing the IDs of the created web
scenarios under the `httptestids` property. The order of the returned
IDs matches the order of the passed web scenarios.

[comment]: # ({/new-0f4da548})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0b10bb11})
#### Creating a web scenario

Create a web scenario to monitor the company home page. The scenario
will have two steps, to check the home page and the "About" page and
make sure they return the HTTP status code 200.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.create",
    "params": {
        "name": "Homepage check",
        "hostid": "10085",
        "steps": [
            {
                "name": "Homepage",
                "url": "http://mycompany.com",
                "status_codes": "200",
                "no": 1
            },
            {
                "name": "Homepage / About",
                "url": "http://mycompany.com/about",
                "status_codes": "200",
                "no": 2
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0b10bb11})

[comment]: # ({new-e8ca015f})
### See also

-   [Scenario step](object#scenario_step)

[comment]: # ({/new-e8ca015f})

[comment]: # ({new-6b938292})
### Source

CHttpTest::create() in
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-6b938292})
