[comment]: # translation:outdated

[comment]: # ({new-a17bb95b})
# > Web scenario object

The following objects are directly related to the `webcheck` API.

[comment]: # ({/new-a17bb95b})

[comment]: # ({new-e60908bb})
### Web scenario

The web scenario object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|httptestid|string|*(readonly)* ID of the web scenario.|
|**hostid**<br>(required)|string|ID of the host that the web scenario belongs to.|
|**name**<br>(required)|string|Name of the web scenario.|
|agent|string|User agent string that will be used by the web scenario.<br><br>Default: Zabbix|
|applicationid|string|ID of the application that the web scenario belongs to.|
|authentication|integer|Authentication method that will be used by the web scenario.<br><br>Possible values:<br>0 - *(default)* none;<br>1 - basic HTTP authentication;<br>2 - NTLM authentication.|
|delay|string|Execution interval of the web scenario. Accepts seconds, time unit with suffix and user macro.<br><br>Default: 1m.|
|headers|string *(deprecated)*<br>array of [HTTP fields](/fr/manual/api/reference/httptest/object#HTTP field)|HTTP headers that will be sent when performing a request.|
|http\_password|string|Password used for authentication.<br><br>Required for web scenarios with basic HTTP or NTLM authentication.|
|http\_proxy|string|Proxy that will be used by the web scenario given as *http://\[username\[:password\]@\]proxy.example.com\[:port\]*.|
|http\_user|string|User name used for authentication.<br><br>Required for web scenarios with basic HTTP or NTLM authentication.|
|nextcheck|timestamp|*(readonly)* Time of the next web scenario execution.|
|retries|integer|Number of times a web scenario will try to execute each step before failing.<br><br>Default: 1.|
|ssl\_cert\_file|string|Name of the SSL certificate file used for client authentication (must be in PEM format).|
|ssl\_key\_file|string|Name of the SSL private key file used for client authentication (must be in PEM format).|
|ssl\_key\_password|string|SSL private key password.|
|status|integer|Whether the web scenario is enabled.<br><br>Possible values are:<br>0 - *(default)* enabled;<br>1 - disabled.|
|templateid|string|*(readonly)* ID of the parent template web scenario.|
|variables|string *(deprecated)*<br>array of [HTTP fields](/fr/manual/api/reference/httptest/object#HTTP field)|Web scenario variables.|
|verify\_host|integer|Whether to verify that the host name specified in the SSL certificate matches the one used in the scenario.<br><br>Possible values are:<br>0 - *(default)* skip host verification;<br>1 - verify host.|
|verify\_peer|integer|Whether to verify the SSL certificate of the web server.<br><br>Possible values are:<br>0 - *(default)* skip peer verification;<br>1 - verify peer.|

[comment]: # ({/new-e60908bb})

[comment]: # ({new-c7ab7fb0})
### Scenario step

The scenario step object defines a specific web scenario check. It has
the following properties.

|Property|Type|Description|
|--------|----|-----------|
|httpstepid|string|*(readonly)* ID of the scenario step.|
|**name**<br>(required)|string|Name of the scenario step.|
|**no**<br>(required)|integer|Sequence number of the step in a web scenario.|
|**url**<br>(required)|string|URL to be checked.|
|follow\_redirects|integer|Whether to follow HTTP redirects.<br><br>Possible values are:<br>0 - don't follow redirects;<br>1 - *(default)* follow redirects.|
|headers|string *(deprecated)*<br>array of [HTTP fields](/fr/manual/api/reference/httptest/object#HTTP field)|HTTP headers that will be sent when performing a request. Scenario step headers will overwrite headers specified for the web scenario.|
|httptestid|string|*(readonly)* ID of the web scenario that the step belongs to.|
|posts|string<br>array of [HTTP fields](/fr/manual/api/reference/httptest/object#HTTP field)|HTTP POST variables as a string (raw post data) or as an array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field) (form field data).|
|required|string|Text that must be present in the response.|
|retrieve\_mode|integer|Part of the HTTP response that the scenario step must retrieve.<br><br>Possible values are:<br>0 - *(default)* only body;<br>1 - only headers.|
|status\_codes|string|Ranges of required HTTP status codes separated by commas.|
|timeout|string|Request timeout in seconds. Accepts seconds, time unit with suffix and user macro.<br><br>Default: 15s.|
|variables|string *(deprecated)*<br>array of [HTTP fields](/fr/manual/api/reference/httptest/object#HTTP field)|Scenario step variables.|
|query\_fields|array of [HTTP fields](/fr/manual/api/reference/httptest/object#HTTP field)|Query fields - array of [HTTP fields](/manual/api/reference/httptest/object#HTTP field) that will be added to URL when performing a request|

::: noteimportant
Both string and array of [HTTP
fields](/fr/manual/api/reference/httptest/object#HTTP field) types are
allowed for `headers` and `variables` fields of both web scenario and
web scenario step object.\
String data type for `headers` and `variables` is deprecated and will be
removed in future versions.
:::

[comment]: # ({/new-c7ab7fb0})

[comment]: # ({new-eb20f47d})
### HTTP field

The HTTP field object defines a name and value that is used to specify
variable, HTTP header, POST form field data of query field data. It has
the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**name**<br>(required)|string|Name of header / variable / POST or GET field.|
|**value**<br>(required)|string|Value of header / variable / POST or GET field.|

[comment]: # ({/new-eb20f47d})

[comment]: # ({new-d69e8b7b})
### HTTP field

The HTTP field object defines a name and value that is used to specify
variable, HTTP header, POST form field data of query field data. It has
the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Name of header / variable / POST or GET field.|
|**value**<br>(required)|string|Value of header / variable / POST or GET field.|

[comment]: # ({/new-d69e8b7b})
