[comment]: # translation:outdated

[comment]: # ({new-c289fd9d})
# httptest.update

[comment]: # ({/new-c289fd9d})

[comment]: # ({new-b09ad465})
### Description

`object httptest.update(object/array webScenarios)`

This method allows to update existing web scenarios.

[comment]: # ({/new-b09ad465})

[comment]: # ({new-83b60d28})
### Parameters

`(object/array)` Web scenario properties to be updated.

The `httptestid` property must be defined for each web scenario, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard web scenario
properties](object#web_scenario), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|steps|array|Scenario steps to replace existing steps.|

[comment]: # ({/new-83b60d28})

[comment]: # ({new-02282fdb})
### Return values

`(object)` Returns an object containing the IDs of the updated web
scenarios under the `httptestid` property.

[comment]: # ({/new-02282fdb})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4c3bf00e})
#### Enabling a web scenario

Enable a web scenario, that is, set its status to "0".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.update",
    "params": {
        "httptestid": "5",
        "status": 0
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4c3bf00e})

[comment]: # ({new-e8ca015f})
### See also

-   [Scenario step](object#scenario_step)

[comment]: # ({/new-e8ca015f})

[comment]: # ({new-628fc1ff})
### Source

CHttpTest::update() in
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-628fc1ff})
