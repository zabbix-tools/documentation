[comment]: # translation:outdated

[comment]: # ({new-629f1e7c})
# httptest.delete

[comment]: # ({/new-629f1e7c})

[comment]: # ({new-67a29a65})
### Description

`object httptest.delete(array webScenarioIds)`

This method allows to delete web scenarios.

[comment]: # ({/new-67a29a65})

[comment]: # ({new-16d66a94})
### Parameters

`(array)` IDs of the web scenarios to delete.

[comment]: # ({/new-16d66a94})

[comment]: # ({new-fd0832d6})
### Return values

`(object)` Returns an object containing the IDs of the deleted web
scenarios under the `httptestids` property.

[comment]: # ({/new-fd0832d6})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4924488e})
#### Deleting multiple web scenarios

Delete two web scenarios.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4924488e})

[comment]: # ({new-0ba69a51})
### Source

CHttpTest::delete() in
*frontends/php/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-0ba69a51})
