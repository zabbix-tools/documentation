[comment]: # translation:outdated

[comment]: # ({new-03e77d6d})
# Dashboard

This class is designed to work with dashboards.

Object references:\

-   [Dashboard](/fr/manual/api/reference/dashboard/object#dashboard)
-   [Dashboard
    widget](/fr/manual/api/reference/dashboard/object#dashboard_widget)
-   [Dashboard widget
    field](/fr/manual/api/reference/dashboard/object#dashboard_widget_field)
-   [Dashboard user
    group](/fr/manual/api/reference/dashboard/object#dashboard_user_group)
-   [Dashboard
    user](/fr/manual/api/reference/dashboard/object#dashboard_user)

Available methods:\

-   [dashboard.create](/fr/manual/api/reference/dashboard/create) -
    creating new dashboards
-   [dashboard.delete](/fr/manual/api/reference/dashboard/delete) -
    deleting dashboards
-   [dashboard.get](/fr/manual/api/reference/dashboard/get) - retrieving
    dashboards
-   [dashboard.update](/fr/manual/api/reference/dashboard/update) -
    updating dashboards

[comment]: # ({/new-03e77d6d})
