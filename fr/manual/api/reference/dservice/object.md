[comment]: # translation:outdated

[comment]: # ({new-01e2c406})
# > Discovered service object

The following objects are directly related to the `dservice` API.

[comment]: # ({/new-01e2c406})

[comment]: # ({new-507852d3})
### Discovered service

::: noteclassic
Discovered services are created by the Zabbix server and
cannot be modified via the API.
:::

The discovered service object contains information about a service
discovered by a network discovery rule on a host. It has the following
properties.

|Property|Type|Description|
|--------|----|-----------|
|dserviceid|string|ID of the discovered service.|
|dcheckid|string|ID of the discovery check used to detect the service.|
|dhostid|string|ID of the discovered host running the service.|
|dns|string|DNS of the host running the service.|
|ip|string|IP address of the host running the service.|
|lastdown|timestamp|Time when the discovered service last went down.|
|lastup|timestamp|Time when the discovered service last went up.|
|port|integer|Service port number.|
|status|integer|Status of the service.<br><br>Possible values:<br>0 - service up;<br>1 - service down.|
|value|string|Value returned by the service when performing a Zabbix agent, SNMPv1, SNMPv2 or SNMPv3 discovery check.|

[comment]: # ({/new-507852d3})
