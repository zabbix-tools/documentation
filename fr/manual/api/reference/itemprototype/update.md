[comment]: # translation:outdated

[comment]: # ({new-cc00d3db})
# itemprototype.update

[comment]: # ({/new-cc00d3db})

[comment]: # ({new-63442cbb})
### Description

`object itemprototype.update(object/array itemPrototypes)`

This method allows to update existing item prototypes.

[comment]: # ({/new-63442cbb})

[comment]: # ({new-16fcb884})
### Parameters

`(object/array)` Item prototype properties to be updated.

The `itemid` property must be defined for each item prototype, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard item prototype
properties](object#item_prototype), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|applications|array|IDs of the applications to replace the current applications.|
|applicationPrototypes|array|Names of the application prototypes to replace the current application prototypes.|
|preprocessing|array|Item prototype preprocessing options to replace the current preprocessing options.|

[comment]: # ({/new-16fcb884})

[comment]: # ({new-f7f1feb9})
### Return values

`(object)` Returns an object containing the IDs of the updated item
prototypes under the `itemids` property.

[comment]: # ({/new-f7f1feb9})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-48cea947})
#### Changing the interface of an item prototype

Change the host interface that will be used by discovered items.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "27428",
        "interfaceid": "132"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27428"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-48cea947})

[comment]: # ({new-e1d8fc73})
#### Update dependent item prototype

Update Dependent item prototype with new Master item prototype ID. Only
dependencies on same host (template/discovery rule) are allowed,
therefore Master and Dependent item should have same hostid and ruleid.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "master_itemid": "25570",
        "itemid": "189030"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189030"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-e1d8fc73})

[comment]: # ({new-d7fe7206})
#### Update HTTP agent item prototype

Change query fields and remove all custom headers.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid":"28305",
        "query_fields": [
            {
                "random": "qwertyuiopasdfghjklzxcvbnm"
            }
        ],
        "headers": []
    }
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28305"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d7fe7206})

[comment]: # ({new-e5fc44e0})
### Source

CItemPrototype::update() in
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-e5fc44e0})

[comment]: # ({new-15ba4525})
#### Updating a script item prototype

Update a script item prototype with a different script and remove
unnecessary parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-15ba4525})

[comment]: # ({new-bc6956c2})
### Source

CItemPrototype::update() in
*ui/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-bc6956c2})
