[comment]: # translation:outdated

[comment]: # ({new-1b9515ab})
# itemprototype.delete

[comment]: # ({/new-1b9515ab})

[comment]: # ({new-ee2fdfa0})
### Description

`object itemprototype.delete(array itemPrototypeIds)`

This method allows to delete item prototypes.

[comment]: # ({/new-ee2fdfa0})

[comment]: # ({new-7755a7b3})
### Parameters

`(array)` IDs of the item prototypes to delete.

[comment]: # ({/new-7755a7b3})

[comment]: # ({new-db63ca62})
### Return values

`(object)` Returns an object containing the IDs of the deleted item
prototypes under the `prototypeids` property.

[comment]: # ({/new-db63ca62})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0c135019})
#### Deleting multiple item prototypes

Delete two item prototypes.\
Dependent item protorypes are removed automatically if master item
prototype is deleted.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "itemprototype.delete",
    "params": [
        "27352",
        "27356"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "prototypeids": [
            "27352",
            "27356"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0c135019})

[comment]: # ({new-8cee4f1f})
### Source

CItemPrototype::delete() in
*frontends/php/include/classes/api/services/CItemPrototype.php*.

[comment]: # ({/new-8cee4f1f})
