[comment]: # translation:outdated

[comment]: # ({new-9dae9ddc})
# Trigger

This class is designed to work with triggers.

Object references:\

-   [Trigger](/fr/manual/api/reference/trigger/object#trigger)

Available methods:\

-   [trigger.adddependencies](/fr/manual/api/reference/trigger/adddependencies) -
    adding new trigger dependencies
-   [trigger.create](/fr/manual/api/reference/trigger/create) - creating
    new triggers
-   [trigger.delete](/fr/manual/api/reference/trigger/delete) - deleting
    triggers
-   [trigger.deletedependencies](/fr/manual/api/reference/trigger/deletedependencies) -
    deleting trigger dependencies
-   [trigger.get](/fr/manual/api/reference/trigger/get) - retrieving
    triggers
-   [trigger.update](/fr/manual/api/reference/trigger/update) - updating
    triggers

[comment]: # ({/new-9dae9ddc})
