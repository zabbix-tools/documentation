[comment]: # translation:outdated

[comment]: # ({new-70dbc2c9})
# proxy.get

[comment]: # ({/new-70dbc2c9})

[comment]: # ({new-b91ae38b})
### Description

`integer/array proxy.get(object parameters)`

The method allows to retrieve proxies according to the given parameters.

[comment]: # ({/new-b91ae38b})

[comment]: # ({new-74f0aaac})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|proxyids|string/array|Return only proxies with the given IDs.|
|selectHosts|query|Return hosts monitored by the proxy in the `hosts` property.|
|selectInterface|query|Return the proxy interface used by a passive proxy in the `interface` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `hostid`, `host` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-74f0aaac})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b40fb513})
#### Retrieve all proxies

Retrieve all configured proxies and their interfaces.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.get",
    "params": {
        "output": "extend",
        "selectInterface": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "interface": [],
            "host": "Active proxy",
            "status": "5",
            "lastaccess": "0",
            "proxyid": "30091",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "interface": {
                "interfaceid": "30109",
                "hostid": "30092",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "10051"
            ],
            "host": "Passive proxy",
            "status": "6",
            "lastaccess": "0",
            "proxyid": "30092",
            "description": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-b40fb513})

[comment]: # ({new-273e0fd8})
### See also

-   [Host](/fr/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/new-273e0fd8})

[comment]: # ({new-b9a96f2c})
### Source

CProxy::get() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-b9a96f2c})
