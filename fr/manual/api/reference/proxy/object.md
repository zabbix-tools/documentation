[comment]: # translation:outdated

[comment]: # ({new-7b0425dd})
# > Proxy object

The following objects are directly related to the `proxy` API.

[comment]: # ({/new-7b0425dd})

[comment]: # ({new-52ccb0be})
### Proxy

The proxy object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|proxyid|string|*(readonly)* ID of the proxy.|
|**host**<br>(required)|string|Name of the proxy.|
|**status**<br>(required)|integer|Type of proxy.<br><br>Possible values:<br>5 - active proxy;<br>6 - passive proxy.|
|description|text|Description of the proxy.|
|lastaccess|timestamp|*(readonly)* Time when the proxy last connected to the server.|
|tls\_connect|integer|Connections to host.<br><br>Possible values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_accept|integer|Connections from host.<br><br>Possible bitmap values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_issuer|string|Certificate issuer.|
|tls\_subject|string|Certificate subject.|
|tls\_psk\_identity|string|PSK identity. Required if either `tls_connect` or `tls_accept` has PSK enabled.|
|tls\_psk|string|The preshared key, at least 32 hex digits. Required if either `tls_connect` or `tls_accept` has PSK enabled.|
|auto\_compress|integer|*(readonly)* Indicates if communication between Zabbix server and proxy is compressed.<br><br>Possible values are:<br>0 - No compression;<br>1 - Compression enabled;|

[comment]: # ({/new-52ccb0be})

[comment]: # ({new-4e21547f})
### Proxy interface

The proxy interface object defines the interface used to connect to a
passive proxy. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|interfaceid|string|*(readonly)* ID of the interface.|
|**dns**<br>(required)|string|DNS name to connect to.<br><br>Can be empty if connections are made via IP address.|
|**ip**<br>(required)|string|IP address to connect to.<br><br>Can be empty if connections are made via DNS names.|
|**port**<br>(required)|string|Port number to connect to.|
|**useip**<br>(required)|integer|Whether the connection should be made via IP address.<br><br>Possible values are:<br>0 - connect using DNS name;<br>1 - connect using IP address.|
|hostid|string|*(readonly)* ID of the proxy the interface belongs to.|

[comment]: # ({/new-4e21547f})
