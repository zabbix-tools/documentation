[comment]: # translation:outdated

[comment]: # ({new-07099686})
# proxy.create

[comment]: # ({/new-07099686})

[comment]: # ({new-e0abed38})
### Description

`object proxy.create(object/array proxies)`

This method allows to create new proxies.

[comment]: # ({/new-e0abed38})

[comment]: # ({new-fc26a415})
### Parameters

`(object/array)` Proxies to create.

Additionally to the [standard proxy properties](object#proxy), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|hosts|array|Hosts to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.<br><br>The hosts must have the `hostid` property defined.|
|interface|object|Host interface to be created for the passive proxy.<br><br>Required for passive proxies.|

[comment]: # ({/new-fc26a415})

[comment]: # ({new-606a1465})
### Return values

`(object)` Returns an object containing the IDs of the created proxies
under the `proxyids` property. The order of the returned IDs matches the
order of the passed proxies.

[comment]: # ({/new-606a1465})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-86fb9238})
#### Create an active proxy

Create an action proxy "Active proxy" and assign a host to be monitored
by it.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Active proxy",
        "status": "5",
        "hosts": [
            {
                "hostid": "10279"
            }
        ]
    },
    "auth": "ab9638041ec6922cb14b07982b268f47",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10280"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-86fb9238})

[comment]: # ({new-8e1098f9})
#### Create a passive proxy

Create a passive proxy "Passive proxy" and assign two hosts to be
monitored by it.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.create",
    "params": {
        "host": "Passive proxy",
        "status": "6",
        "interface": {
            "ip": "127.0.0.1",
            "dns": "",
            "useip": "1",
            "port": "10051"
        },
        "hosts": [
            {
                "hostid": "10192"
            },
            {
                "hostid": "10139"
            }
        ]
    },
    "auth": "ab9638041ec6922cb14b07982b268f47",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10284"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8e1098f9})

[comment]: # ({new-273e0fd8})
### See also

-   [Host](/fr/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/new-273e0fd8})

[comment]: # ({new-453e4545})
### Source

CProxy::create() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-453e4545})
