[comment]: # translation:outdated

[comment]: # ({new-226837df})
# proxy.delete

[comment]: # ({/new-226837df})

[comment]: # ({new-a16e4b7f})
### Description

`object proxy.delete(array proxies)`

This method allows to delete proxies.

[comment]: # ({/new-a16e4b7f})

[comment]: # ({new-0c678a75})
### Parameters

`(array)` IDs of proxies to delete.

[comment]: # ({/new-0c678a75})

[comment]: # ({new-426b06a5})
### Return values

`(object)` Returns an object containing the IDs of the deleted proxies
under the `proxyids` property.

[comment]: # ({/new-426b06a5})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-dcfc3683})
#### Delete multiple proxies

Delete two proxies.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.delete",
    "params": [
        "10286",
        "10285"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10286",
            "10285"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-dcfc3683})

[comment]: # ({new-4dd75afc})
### Source

CProxy::delete() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-4dd75afc})
