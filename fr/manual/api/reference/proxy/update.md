[comment]: # translation:outdated

[comment]: # ({new-92a86fab})
# proxy.update

[comment]: # ({/new-92a86fab})

[comment]: # ({new-381a99b9})
### Description

`object proxy.update(object/array proxies)`

This method allows to update existing proxies.

[comment]: # ({/new-381a99b9})

[comment]: # ({new-4d6a0721})
### Parameters

`(object/array)` Proxy properties to be updated.

The `proxyid` property must be defined for each proxy, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard proxy properties](object#proxy), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|hosts|array|Hosts to be monitored by the proxy. If a host is already monitored by a different proxy, it will be reassigned to the current proxy.<br><br>The hosts must have the `hostid` property defined.|
|interface|object|Host interface to replace the existing interface for the passive proxy.|

[comment]: # ({/new-4d6a0721})

[comment]: # ({new-bab02241})
### Return values

`(object)` Returns an object containing the IDs of the updated proxies
under the `proxyids` property.

[comment]: # ({/new-bab02241})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3613ce9a})
#### Change hosts monitored by a proxy

Update the proxy to monitor the two given hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "hosts": [
            "10294",
            "10295"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3613ce9a})

[comment]: # ({new-f06b99dc})
#### Change proxy status

Change the proxy to an active proxy and rename it to "Active proxy".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "proxy.update",
    "params": {
        "proxyid": "10293",
        "host": "Active proxy",
        "status": "5"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "proxyids": [
            "10293"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f06b99dc})

[comment]: # ({new-273e0fd8})
### See also

-   [Host](/fr/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)

[comment]: # ({/new-273e0fd8})

[comment]: # ({new-8237a78a})
### Source

CProxy::update() in
*frontends/php/include/classes/api/services/CProxy.php*.

[comment]: # ({/new-8237a78a})
