[comment]: # translation:outdated

[comment]: # ({new-c5a9e121})
# > LLD rule object

The following objects are directly related to the `discoveryrule` API.

[comment]: # ({/new-c5a9e121})

[comment]: # ({new-c17fa830})
### LLD rule

The low-level discovery rule object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|itemid|string|*(readonly)* ID of the LLD rule.|
|**delay**<br>(required)|string|Update interval of the LLD rule. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/fr/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon.|
|**hostid**<br>(required)|string|ID of the host that the LLD rule belongs to.|
|**interfaceid**<br>(required)|string|ID of the LLD rule's host interface. Used only for host LLD rules.<br><br>Optional for Zabbix agent (active), Zabbix internal, Zabbix trapper and database monitor LLD rules.|
|**key\_**<br>(required)|string|LLD rule key.|
|**name**<br>(required)|string|Name of the LLD rule.|
|**type**<br>(required)|integer|Type of the LLD rule.<br><br>Possible values:<br>0 - Zabbix agent;<br>1 - SNMPv1 agent;<br>2 - Zabbix trapper;<br>3 - simple check;<br>4 - SNMPv2 agent;<br>5 - Zabbix internal;<br>6 - SNMPv3 agent;<br>7 - Zabbix agent (active);<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>16 - JMX agent;<br>19 - HTTP agent;|
|**url**<br>(required)|string|URL string, required for HTTP agent LLD rule. Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.|
|allow\_traps|integer|HTTP agent LLD rule field. Allow to populate value as in trapper item type also.<br><br>0 - *(default)* Do not allow to accept incoming data.<br>1 - Allow to accept incoming data.|
|authtype|integer|Used only by SSH agent or HTTP agent LLD rules.<br><br>SSH agent authentication method possible values:<br>0 - *(default)* password;<br>1 - public key.<br><br>HTTP agent authentication method possible values:<br>0 - *(default)* none<br>1 - basic<br>2 - NTLM|
|description|string|Description of the LLD rule.|
|error|string|*(readonly)* Error text if there are problems updating the LLD rule.|
|follow\_redirects|integer|HTTP agent LLD rule field. Follow respose redirects while pooling data.<br><br>0 - Do not follow redirects.<br>1 - *(default)* Follow redirects.|
|headers|object|HTTP agent LLD rule field. Object with HTTP(S) request headers, where header name is used as key and header value as value.<br><br>Example:<br>{ "User-Agent": "Zabbix" }|
|http\_proxy|string|HTTP agent LLD rule field. HTTP(S) proxy connection string.|
|ipmi\_sensor|string|IPMI sensor. Used only by IPMI LLD rules.|
|jmx\_endpoint|string|JMX agent custom connection string.<br><br>Default value:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|
|lifetime|string|Time period after which items that are no longer discovered will be deleted. Accepts seconds, time unit with suffix and user macro.<br><br>Default: `30d`.|
|output\_format|integer|HTTP agent LLD rule field. Should response converted to JSON.<br><br>0 - *(default)* Store raw.<br>1 - Convert to JSON.|
|params|string|Additional parameters depending on the type of the LLD rule:<br>- executed script for SSH and Telnet LLD rules;<br>- SQL query for database monitor LLD rules;<br>- formula for calculated LLD rules.|
|password|string|Password for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent LLD rules.|
|port|string|Port used by the LLD rule. Used only by SNMP LLD rules.|
|post\_type|integer|HTTP agent LLD rule field. Type of post data body stored in posts property.<br><br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.|
|posts|string|HTTP agent LLD rule field. HTTP(S) request body data. Used with post\_type.|
|privatekey|string|Name of the private key file.|
|publickey|string|Name of the public key file.|
|query\_fields|array|HTTP agent LLD rule field. Query parameters. Array of objects with 'key':'value' pairs, where value can be empty string.|
|request\_method|integer|HTTP agent LLD rule field. Type of request method.<br><br>0 - GET<br>1 - *(default)* POST<br>2 - PUT<br>3 - HEAD|
|retrieve\_mode|integer|HTTP agent LLD rule field. What part of response should be stored.<br><br>0 - *(default)* Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.<br><br>For request\_method HEAD only 1 is allowed value.|
|snmp\_community|string|SNMP community.<br><br>Required for SNMPv1 and SNMPv2 LLD rules.|
|snmp\_oid|string|SNMP OID.|
|snmpv3\_authpassphrase|string|SNMPv3 auth passphrase. Used only by SNMPv3 LLD rules.|
|snmpv3\_authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 LLD rules.<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA.|
|snmpv3\_contextname|string|SNMPv3 context name. Used only by SNMPv3 checks.|
|snmpv3\_privpassphrase|string|SNMPv3 priv passphrase. Used only by SNMPv3 LLD rules.|
|snmpv3\_privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 LLD rules.<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES.|
|snmpv3\_securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 LLD rules.<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|string|SNMPv3 security name. Used only by SNMPv3 LLD rules.|
|ssl\_cert\_file|string|HTTP agent LLD rule field. Public SSL Key file path.|
|ssl\_key\_file|string|HTTP agent LLD rule field. Private SSL Key file path.|
|ssl\_key\_password|string|HTTP agent LLD rule field. Password for SSL Key file.|
|state|integer|*(readonly)* State of the LLD rule.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - not supported.|
|status|integer|Status of the LLD rule.<br><br>Possible values:<br>0 - *(default)* enabled LLD rule;<br>1 - disabled LLD rule.|
|status\_codes|string|HTTP agent LLD rule field. Ranges of required HTTP status codes separated by commas. Also supports user macros as part of comma separated list.<br><br>Example: 200,200-{$M},{$M},200-400|
|templateid|string|(readonly) ID of the parent template LLD rule.|
|timeout|string|HTTP agent LLD rule field. Item data polling request timeout. Support user macros.<br><br>default: 3s<br>maximum value: 60s|
|trapper\_hosts|string|Allowed hosts. Used by trapper LLD rules or HTTP agent LLD rules.|
|username|string|Username for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent LLD rules.<br><br>Required by SSH and Telnet LLD rules.|
|verify\_host|integer|HTTP agent LLD rule field. Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|
|verify\_peer|integer|HTTP agent LLD rule field. Validate is host certificate authentic.<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|

[comment]: # ({/new-c17fa830})

[comment]: # ({new-b0412737})
### LLD rule filter

The LLD rule filter object defines a set of conditions that can be used
to filter discovered objects. It has the following properties:

|Property|Type|Description|
|--------|----|-----------|
|**conditions**<br>(required)|array|Set of filter conditions to use for filtering results.|
|**evaltype**<br>(required)|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.<br><br>Required for custom expression filters.|

[comment]: # ({/new-b0412737})

[comment]: # ({new-be83ef5c})
#### LLD rule filter condition

The LLD rule filter condition object defines a separate check to perform
on the value of an LLD macro. It has the following properties:

|Property|Type|Description|
|--------|----|-----------|
|**macro**<br>(required)|string|LLD macro to perform the check on.|
|**value**<br>(required)|string|Value to compare with.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[discoveryrule.get](get#retrieving_filter_conditions) and
[discoveryrule.create](create#using_a_custom_expression_filter) method
pages.
:::

[comment]: # ({/new-be83ef5c})

[comment]: # ({new-4a2b52b9})
### LLD macro path

The LLD macro path has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**lld\_macro**<br>(required)|string|LLD macro.|
|**path**<br>(required)|string|Selector for value which will be assigned to corresponding macro.|

[comment]: # ({/new-4a2b52b9})

[comment]: # ({new-14dc55c1})
### LLD rule preprocessing

The LLD rule preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>5 - Regular expression matching;<br>11 - XML XPath;<br>12 - JSONPath;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>20 - Discard unchanged with heartbeat;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>27 - XML to JSON.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|
|**error\_handler**<br>(required)|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.|
|**error\_handler\_params**<br>(required)|string|Error handler parameters. Used with `error_handler`.<br><br>Must be empty, if `error_handler` is 0 or 1.<br>Can be empty if, `error_handler` is 2.<br>Cannot be empty, if `error_handler` is 3.|

The following parameters and error handlers are supported for each
preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|5|Regular expression|pattern^1^|output^2^|<|0, 1, 2, 3|
|11|XML XPath|path^3^|<|<|0, 1, 2, 3|
|12|JSONPath|path^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^1^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^3^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^3^|<|<|0, 1, 2, 3|
|20|Discard unchanged with heartbeat|seconds^4,\ 5,\ 6^|<|<|<|
|23|Prometheus to JSON|pattern^5,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|27|XML to JSON|<|<|<|0, 1, 2, 3|

^1^ regular expression\
^2^ string\
^3^ JSONPath or XML XPath\
^4^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^5^ user macro\
^6^ LLD macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro.\
^8^ Prometheus output following the syntax: `<label name>`.

[comment]: # ({/new-14dc55c1})

[comment]: # ({new-2f32e5f0})
### LLD rule overrides

The LLD rule overrides object defines a set of rules (filters,
conditions and operations) that are used to override properties of
different prototype objects. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Unique override name.|
|**step**<br>(required)|integer|Unique order number of the override.|
|stop|integer|Stop processing next overrides if matches.<br><br>Possible values:<br>0 - *(default)* don't stop processing overrides;<br>1 - stop processing overrides if filter matches.|
|filter|object|Override filter.|
|operations|array|Override operations.|

[comment]: # ({/new-2f32e5f0})

[comment]: # ({new-573b52c4})
#### LLD rule override filter

The LLD rule override filter object defines a set of conditions that if
they match the discovered object the override is applied. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**evaltype**<br>(required)|integer|Override filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|**conditions**<br>(required)|array|Set of override filter conditions to use for matching the discovered objects.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating override filter conditions. The expression contains IDs that reference specific override filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.|
|formula|string|User-defined expression to be used for evaluating conditions of override filters with a custom expression. The expression must contain IDs that reference specific override filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the override filter conditions: no condition can remain unused or omitted.<br><br>Required for custom expression override filters.|

[comment]: # ({/new-573b52c4})

[comment]: # ({new-e47f6c6a})
#### LLD rule override filter condition

The LLD rule override filter condition object defines a separate check
to perform on the value of an LLD macro. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**macro**<br>(required)|string|LLD macro to perform the check on.|
|**value**<br>(required)|string|Value to compare with.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>8 - *(default)* matches regular expression;<br>9 - does not match regular expression;<br>12 - exists;<br>13 - does not exist.|

[comment]: # ({/new-e47f6c6a})

[comment]: # ({new-e7b08f3c})
#### LLD rule override operation

The LLD rule override operation is combination of conditions and actions
to perform on the prototype object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**operationobject**<br>(required)|integer|Type of discovered object to perform the action.<br><br>Possible values:<br>0 - Item prototype;<br>1 - Trigger prototype;<br>2 - Graph prototype;<br>3 - Host prototype.|
|operator|integer|Override condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>1 - does not equal;<br>2 - contains;<br>3 - does not contain;<br>8 - matches;<br>9 - does not match.|
|value|string|Pattern to match item, trigger, graph or host prototype name depending on selected object.|
|opstatus|object|Override operation status object for item, trigger and host prototype objects.|
|opdiscover|object|Override operation discover status object (all object types).|
|opperiod|object|Override operation period (update interval) object for item prototype object.|
|ophistory|object|Override operation history object for item prototype object.|
|optrends|object|Override operation trends object for item prototype object.|
|opseverity|object|Override operation severity object for trigger prototype object.|
|optag|array|Override operation tag object for trigger and host prototype objects.|
|optemplate|array|Override operation template object for host prototype object.|
|opinventory|object|Override operation inventory object for host prototype object.|

[comment]: # ({/new-e7b08f3c})

[comment]: # ({new-52d3b579})
##### LLD rule override operation status

LLD rule override operation status that is set to discovered object. It
has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**status**<br>(required)|integer|Override the status for selected object.<br><br>Possible values:<br>0 - Create enabled;<br>1 - Create disabled.|

[comment]: # ({/new-52d3b579})

[comment]: # ({new-ff620350})
##### LLD rule override operation discover

LLD rule override operation discover status that is set to discovered
object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**discover**<br>(required)|integer|Override the discover status for selected object.<br><br>Possible values:<br>0 - Yes, continue discovering the objects;<br>1 - No, new objects will not be discovered and existing ones will be marked as lost.|

[comment]: # ({/new-ff620350})

[comment]: # ({new-6e469c1b})
##### LLD rule override operation period

LLD rule override operation period is an update interval value (supports
custom intervals) that is set to discovered item. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**delay**<br>(required)|string|Override the update interval of the item prototype. Accepts seconds or a time unit with suffix (30s,1m,2h,1d) as well as flexible and scheduling intervals and user macros or LLD macros. Multiple intervals are separated by a semicolon.|

[comment]: # ({/new-6e469c1b})

[comment]: # ({new-ff97489f})
##### LLD rule override operation history

LLD rule override operation history value that is set to discovered
item. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**history**<br>(required)|string|Override the history of item prototype which is a time unit of how long the history data should be stored. Also accepts user macro and LLD macro.|

[comment]: # ({/new-ff97489f})

[comment]: # ({new-10b52601})
##### LLD rule override operation trends

LLD rule override operation trends value that is set to discovered item.
It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**trends**<br>(required)|string|Override the trends of item prototype which is a time unit of how long the trends data should be stored. Also accepts user macro and LLD macro.|

[comment]: # ({/new-10b52601})

[comment]: # ({new-991f3bdc})
##### LLD rule override operation severity

LLD rule override operation severity value that is set to discovered
trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**severity**<br>(required)|integer|Override the severity of trigger prototype.<br><br>Possible values are: 0 - *(default)* not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|

[comment]: # ({/new-991f3bdc})

[comment]: # ({new-0693b8f3})
##### LLD rule override operation tag

LLD rule override operation tag object contains tag name and value that
are set to discovered object. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|New tag name.|
|value|string|New tag value.|

[comment]: # ({/new-0693b8f3})

[comment]: # ({new-5eebc05a})
##### LLD rule override operation template

LLD rule override operation template object that is linked to discovered
host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**templateid**<br>(required)|string|Override the template of host prototype linked templates.|

[comment]: # ({/new-5eebc05a})

[comment]: # ({new-9c5cc3d1})
##### LLD rule override operation inventory

LLD rule override operation inventory mode value that is set to
discovered host. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**inventory\_mode**<br>(required)|integer|Override the host prototype inventory mode.<br><br>Possible values are:<br>-1 - disabled;<br>0 - *(default)* manual;<br>1 - automatic.|

[comment]: # ({/new-9c5cc3d1})
