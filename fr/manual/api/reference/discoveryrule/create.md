[comment]: # translation:outdated

[comment]: # ({new-2709706a})
# discoveryrule.create

[comment]: # ({/new-2709706a})

[comment]: # ({new-78136d05})
### Description

`object discoveryrule.create(object/array lldRules)`

This method allows to create new LLD rules.

[comment]: # ({/new-78136d05})

[comment]: # ({new-1c165cc1})
### Parameters

`(object/array)` LLD rules to create.

Additionally to the [standard LLD rule properties](object#lld_rule), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|filter|object|LLD rule filter object for the LLD rule.|

[comment]: # ({/new-1c165cc1})

[comment]: # ({new-08a72633})
### Return values

`(object)` Returns an object containing the IDs of the created LLD rules
under the `itemids` property. The order of the returned IDs matches the
order of the passed LLD rules.

[comment]: # ({/new-08a72633})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c30fe277})
#### Creating an LLD rule

Create a Zabbix agent LLD rule to discover mounted file systems.
Discovered items will be updated every 30 seconds.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Mounted filesystem discovery",
        "key_": "vfs.fs.discovery",
        "hostid": "10197",
        "type": "0",
        "interfaceid": "112",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c30fe277})

[comment]: # ({new-01b95865})
#### Using a filter

Create an LLD rule with a set of conditions to filter the results by.
The conditions will be grouped together using the logical "and"
operator.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Filtered LLD rule",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex1"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex2"
                },
                {
                    "macro": "{#MACRO3}",
                    "value": "@regex3"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-01b95865})

[comment]: # ({new-a201a7a1})
#### Using a custom expression filter

Create an LLD rule with a filter that will use a custom expression to
evaluate the conditions. The LLD rule must only discover objects the
"{\#MACRO1}" macro value of which matches both regular expression
"regex1" and "regex2", and the value of "{\#MACRO2}" matches either
"regex3" or "regex4". The formula IDs "A", "B", "C" and "D" have been
chosen arbitrarily.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Filtered LLD rule",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "filter": {
            "evaltype": 3,
            "formula": "(A and B) and (C or D)",
            "conditions": [
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex1",
                    "formulaid": "A"
                },
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex2",
                    "formulaid": "B"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex3",
                    "formulaid": "C"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex4",
                    "formulaid": "D"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a201a7a1})

[comment]: # ({new-32af137e})
#### Using custom query fields and headers

Create LLD rule with custom query fields and headers.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "hostid": "10257",
        "interfaceid": "5",
        "type": "19",
        "name": "API HTTP agent",
        "key_": "api_discovery_rule",
        "value_type": "3",
        "delay": "5s",
        "url": "http://127.0.0.1?discoverer.php",
        "query_fields": [
            {
                "mode": "json"
            },
            {
                "elements":"2"
            }
        ],
        "headers": {
            "X-Type": "api",
            "Authorization": "Bearer mF_A.B5f-2.1JcM"
        },
        "allow_traps": "1",
        "trapper_hosts": "127.0.0.1",
        "id": 35,
        "auth": "d678e0b85688ce578ff061bd29a20d3b",
    }
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 35
}
```

[comment]: # ({/new-32af137e})

[comment]: # ({new-001d915b})
### See also

-   [LLD rule filter](object#lld_rule_filter)

[comment]: # ({/new-001d915b})

[comment]: # ({new-3e76a4a9})
### Source

CDiscoveryRule::create() in
*frontends/php/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-3e76a4a9})

[comment]: # ({new-f9343aca})
#### Creating a LLD rule with overrides

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Discover database host",
        "key_": "lld.with.overrides",
        "hostid": "10001",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "interfaceid": "1155",
        "overrides": [
            {
                "name": "Discover MySQL host",
                "step": "1",
                "stop": "1",
                "filter": {
                    "evaltype": "2",
                    "conditions": [
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^mysqld\\.service$"
                        },
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^mariadb\\.service$"
                        }
                    ]
                },
                "operations": [
                    {
                        "operationobject": "3",
                        "operator": "2",
                        "value": "Database host",
                        "opstatus": {
                            "status": "0"
                        },
                        "optemplate": [
                            {
                                "templateid": "10170"
                            }
                        ],
                        "optag": [
                            {
                                "tag": "Database",
                                "value": "MySQL"
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Discover PostgreSQL host",
                "step": "2",
                "stop": "1",
                "filter": {
                    "evaltype": "0",
                    "conditions": [
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^postgresql\\.service$"
                        }
                    ]
                },
                "operations": [
                    {
                        "operationobject": "3",
                        "operator": "2",
                        "value": "Database host",
                        "opstatus": {
                            "status": "0"
                        },
                        "optemplate": [
                            {
                                "templateid": "10263"
                            }
                        ],
                        "optag": [
                            {
                                "tag": "Database",
                                "value": "PostgreSQL"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "30980"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f9343aca})

[comment]: # ({new-c75d7d1e})
#### Create script LLD rule

Create a simple data collection using a script LLD rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Script example",
        "key_": "custom.script.lldrule",
        "hostid": "12345",
        "type": 21,
        "value_type": 4,
        "params": "var request = new CurlHttpRequest();\nreturn request.Post(\"https://postman-echo.com/post\", JSON.parse(value));",
        "parameters": [{
            "name": "host",
            "value": "{HOST.CONN}"
        }],
        "timeout": "6s",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 3
}
```

[comment]: # ({/new-c75d7d1e})

[comment]: # ({new-f40c8a6e})
### See also

-   [LLD rule filter](object#lld_rule_filter)
-   [LLD macro paths](object#lld_macro_path)
-   [LLD rule preprocessing](object#lld_rule_preprocessing)

[comment]: # ({/new-f40c8a6e})

[comment]: # ({new-73c4009f})
### Source

CDiscoveryRule::create() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/new-73c4009f})
