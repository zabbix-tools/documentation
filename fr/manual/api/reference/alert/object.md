[comment]: # translation:outdated

[comment]: # ({new-05585755})
# > Objet Alerte

Les objets suivants sont directement liés à l'API `alert`.

[comment]: # ({/new-05585755})

[comment]: # ({new-933fed24})
### Alerte

::: noteclassic
Les alertes sont créées par le serveur Zabbix et ne peuvent
pas être modifiées via l'API.
:::

L'objet Alerte contient des informations indiquant si des opérations
spécifiques ont été exécutées avec succès. Il a les propriétés
suivantes.

|Propriété|Type|Description|
|-----------|----|-----------|
|alertid|string|ID de l'alerte.|
|actionid|string|ID de l'action ayant généré l'alerte.|
|alerttype|integer|Type d'alerte.<br><br>Valeurs possibles:<br>0 - message;<br>1 - commande distante.|
|clock|timestamp|L'heure de génération de l'alerte.|
|error|string|Texte d'erreur s'il y a eu des problèmes pour envoyer un message ou exécuter une commande.|
|esc\_step|integer|Étape d'escalade de l'action pendant laquelle l'alerte a été générée.|
|eventid|string|ID de l'événement qui a déclenché l'action.|
|mediatypeid|string|ID du type de média utilisé pour envoyer le message.|
|message|text|Texte du message. Utilisé pour les alertes de message.|
|retries|integer|Nombre de fois que Zabbix a tenté d'envoyer le message.|
|sendto|string|Adresse, nom d'utilisateur ou autre identifiant du destinataire. pour les messages d'alerte.|
|status|integer|Statut indiquant si l'opération a été exécutée avec succès.<br><br>Valeurs possibles pour les alertes de message:<br>0 - message non envoyé.<br>1 - message envoyé.<br>2 - en échec après un certain nombre de tentatives.<br>3 - nouvelle alerte pas encore traitée par le gestionnaire d'alertes.<br><br>Valeurs possibles pour les alertes de commande:<br>0 - commande non exécutée.<br>1 - commande exécutée.<br>2 - tentative d'exécution de la commande sur l'agent Zabbix, mais il n'était pas disponible.|
|subject|string|Objet du message. Utilisé pour les alertes de message.|
|userid|string|ID de l'utilisateur auquel le message a été envoyé.|
|p\_eventid|string|ID de l'événement qui a généré l'alerte.|
|acknowledgeid|string|ID de l'acquittement qui a généré l'alerte.|

[comment]: # ({/new-933fed24})
