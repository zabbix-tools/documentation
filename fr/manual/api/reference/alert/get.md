[comment]: # translation:outdated

[comment]: # ({new-4c82c7ad})
# alert.get

[comment]: # ({/new-4c82c7ad})

[comment]: # ({new-ed55724c})
### Description

`integer/array alert.get(object paramètres)`

La méthode permet de récupérer des alertes en fonction des paramètres
spécifiés.

[comment]: # ({/new-ed55724c})

[comment]: # ({new-218fa8cf})
### Paramètres

`(object)` Paramètres définissant la sortie souhaitée.

La méthode prend en charge les paramètres suivants.

|Paramètre|Type|Description|
|----------|----|-----------|
|alertids|string/array|Renvoie uniquement les alertes avec les IDs spécifiés.|
|actionids|string/array|Renvoie uniquement les alertes générées par les IDs d'action spécifiés.|
|eventids|string/array|Renvoie uniquement les alertes générées par les IDs d'évènement spécifiés.|
|groupids|string/array|Renvoie uniquement les alertes générées par les objets des IDs des groupes d'hôtes spécifiés.|
|hostids|string/array|Renvoie uniquement les alertes générées par les IDs d'hôte spécifiés.|
|mediatypeids|string/array|Renvoie uniquement les alertes utilisant les IDs de type de média spécifiés.|
|objectids|string/array|Renvoie uniquement les alertes générées par les IDs d'objet spécifiés.|
|userids|string/array|Renvoie uniquement les alertes transmises aux utilisateurs spécifiés.|
|eventobject|integer|Renvoie uniquement les alertes générées par les événements liés aux objets du type spécifié.<br><br>Se référer à la propriété ["object" de l'évènement](/fr/manual/api/reference/event/object#evenement) pour une liste des types d'objet pris en charge.<br><br>Par défaut: 0 - déclencheur.|
|eventsource|integer|Renvoie uniquement les alertes générées par les événements du type spécifié.<br><br>Se référer à la propriété ["source" de l'évènement](/fr/manual/api/reference/event/object#evenement) pour une liste des types d'événement pris en charge.<br><br>Par défaut: 0 - évènements déclenchés.|
|time\_from|timestamp|Renvoie uniquement les alertes générées après l'heure spécifiée.|
|time\_till|timestamp|Renvoie uniquement les alertes générées avant l'heure spécifiée.|
|selectHosts|query|Renvoie les hôtes ayant déclenché l'opération d'action dans la propriété `hosts`.|
|selectMediatypes|query|Renvoie le(s) type(s) de média utilisé(s) pour le message d'alerte sous forme de tableau dans la propriété `mediatypes`.|
|selectUsers|query|Renvoie le(s) utilisateur(s) au(x)quel(s) le message a été adressé en tant que tableau dans la propriété `users`.|
|sortfield|string/array|Trier le résultat par les propriétés spécifiées.<br><br>Valeurs possibles: `alertid`, `clock`, `eventid` et `status`.|
|countOutput|boolean|Ces paramètres, communs à toutes les méthodes "get", sont décrits dans les [commentaires de référence](/fr/manual/api/reference_commentary#Parametres_communs_de_la_methode_get).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-218fa8cf})

[comment]: # ({new-7223bab1})
### Valeurs de retour

`(integer/array)` Retourne soit:

-   un tableau d'objets;
-   le nombre d'objets récupérés, si le paramètre `countOutput` a été
    utilisé.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8d56d57c})
#### Récupérer des alertes par ID d'action

Récupérer toutes les alertes générées par l'action "3".

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "alert.get",
    "params": {
        "output": "extend",
        "actionids": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "alertid": "1",
            "actionid": "3",
            "eventid": "21243",
            "userid": "1",
            "clock": "1362128008",
            "mediatypeid": "1",
            "sendto": "support@company.com",
            "subject": "PROBLEM: Zabbix agent on Linux server is unreachable for 5 minutes: ",
            "message": "Trigger: Zabbix agent on Linux server is unreachable for 5 minutes: \nTrigger status: PROBLEM\nTrigger severity: Not classified",
            "status": "0",
            "retries": "3",
            "error": "",
            "esc_step": "1",
            "alerttype": "0",
            "p_eventid": "0",
            "acknowledgeid": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8d56d57c})

[comment]: # ({new-5371aed2})
### Voir également

-   [Hôte](/fr/manual/api/reference/host/object#hote)
-   [Type de
    media](/fr/manual/api/reference/mediatype/object#type_de_media)
-   [Utilisateur](/fr/manual/api/reference/user/object#utilisateur)

[comment]: # ({/new-5371aed2})

[comment]: # ({new-14a85885})
### Source

CAlert::get() dans
*frontends/php/include/classes/api/services/CAlert.php*.

[comment]: # ({/new-14a85885})
