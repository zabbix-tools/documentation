[comment]: # translation:outdated

[comment]: # ({new-b1a4e412})
# Corrélation

Cette classe est conçue pour travailler avec les corrélations.

Référentiel des objets:\

-   [Corrélation](/fr/manual/api/reference/correlation/object#correlation)

Méthodes disponibles:\

-   [correlation.create](/fr/manual/api/reference/correlation/create) -
    créer de nouvelles corrélations
-   [correlation.delete](/fr/manual/api/reference/correlation/delete) -
    supprimer des corrélations
-   [correlation.get](/fr/manual/api/reference/correlation/get) -
    récupérer des corrélations
-   [correlation.update](/fr/manual/api/reference/correlation/update) -
    mettre à jour des corrélations

[comment]: # ({/new-b1a4e412})
