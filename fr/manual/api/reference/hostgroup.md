[comment]: # translation:outdated

[comment]: # ({new-8eb68faf})
# Host group

This class is designed to work with host groups.

Object references:\

-   [Host group](/fr/manual/api/reference/hostgroup/object#host_group)

Available methods:\

-   [hostgroup.create](/fr/manual/api/reference/hostgroup/create) -
    creating new host groups
-   [hostgroup.delete](/fr/manual/api/reference/hostgroup/delete) -
    deleting host groups
-   [hostgroup.get](/fr/manual/api/reference/hostgroup/get) - retrieving
    host groups
-   [hostgroup.massadd](/fr/manual/api/reference/hostgroup/massadd) -
    adding related objects to host groups
-   [hostgroup.massremove](/fr/manual/api/reference/hostgroup/massremove) -
    removing related objects from host groups
-   [hostgroup.massupdate](/fr/manual/api/reference/hostgroup/massupdate) -
    replacing or removing related objects from host groups
-   [hostgroup.update](/fr/manual/api/reference/hostgroup/update) -
    updating host groups

[comment]: # ({/new-8eb68faf})
