[comment]: # translation:outdated

[comment]: # ({new-56486046})
# trigger.create

[comment]: # ({/new-56486046})

[comment]: # ({new-8efa90c6})
### Description

`object trigger.create(object/array triggers)`

This method allows to create new triggers.

[comment]: # ({/new-8efa90c6})

[comment]: # ({new-c2c3e90f})
### Parameters

`(object/array)` Triggers to create.

Additionally to the [standard trigger properties](object#trigger) the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dependencies|array|Triggers that the trigger is dependent on.<br><br>The triggers must have the `triggerid` property defined.|
|tags|array|Trigger tags.|

::: noteimportant
The trigger expression has to be given in its
expanded form.
:::

[comment]: # ({/new-c2c3e90f})

[comment]: # ({new-4938d22d})
### Return values

`(object)` Returns an object containing the IDs of the created triggers
under the `triggerids` property. The order of the returned IDs matches
the order of the passed triggers.

[comment]: # ({/new-4938d22d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-f57689d3})
#### Creating a trigger

Create a trigger with a single trigger dependency.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": {
        {
            "description": "Processor load is too high on {HOST.NAME}",
            "expression": "{Linux server:system.cpu.load[percpu,avg1].last()}>5",
            "dependencies": [
                {
                    "triggerid": "17367"
                }
            ]
        },
        {
            "description": "Service status",
            "expression": "{Linux server:log[/var/log/system,Service .* has stopped].strlen()}<>0",
            "dependencies": [
                {
                    "triggerid": "17368"
                }
            ],
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f57689d3})

[comment]: # ({new-7d49da78})
### Source

CTrigger::create() in
*frontends/php/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-7d49da78})
