[comment]: # translation:outdated

[comment]: # ({new-7416ad19})
# correlation.get

[comment]: # ({/new-7416ad19})

[comment]: # ({new-858936aa})
### Description

`integer/array correlation.get(object parametres)`

La méthode permet de récupérer des corrélations en fonction des
paramètres spécifiés.

[comment]: # ({/new-858936aa})

[comment]: # ({new-06a650a0})
### Paramètres

`(object)` Paramètres définissant la sortie souhaitée.

La méthode prend en charge les paramètres suivants.

|Paramètre|Type|Description|
|----------|----|-----------|
|correlationids|string/array|Renvoie uniquement les corrélations avec les identifiants spécifiés.|
|selectFilter|query|Renvoie le filtre de corrélation dans la propriété `filter`.|
|selectOperations|query|Renvoie les opérations de corrélation dans la propriété `operations`.|
|sortfield|string/array|Trier le résultat par les propriétés spécifiées.<br><br>Valeurs possibles: `correlationid`, `name` et `status`.|
|countOutput|boolean|Ces paramètres, communs à toutes les méthodes "get", sont décrits dans les [commentaires de référence](/fr/manual/api/reference_commentary#Parametres_communs_de_la_methode_get).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-06a650a0})

[comment]: # ({new-7223bab1})
### Valeurs de retour

`(integer/array)` Retourne soit:

-   un tableau d'objets;
-   le nombre d'objets récupérés, si le paramètre `countOutput` a été
    utilisé.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d14c4f20})
#### Récupérer des corrélations

Récupérer toutes les corrélations configurées ainsi que les conditions
et opérations de corrélation. Le filtre utilise le type d'évaluation
"et/ou", ainsi la propriété `formula` est vide et la propriété
`eval_formula` est générée automatiquement.

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-d14c4f20})

[comment]: # ({new-e7d15410})
### Voir également

-   [Filtre de Corrélation](object#filtre_de_correlation)
-   [Opération de Corrélation](object#operation_de_correlation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-bbab04bd})
### Source

CCorrelation::get() dans
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-bbab04bd})
