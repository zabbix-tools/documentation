[comment]: # translation:outdated

[comment]: # ({new-c71a2e54})
# correlation.update

[comment]: # ({/new-c71a2e54})

[comment]: # ({new-b9562236})
### Description

`object correlation.update(object/array correlations)`

Cette méthode permet de mettre à jour les corrélations existantes.

[comment]: # ({/new-b9562236})

[comment]: # ({new-326bc80e})
### Paramètres

`(object/array)` Propriétés de corrélation à mettre à jour.

La propriété `correlationid` doit être définie pour chaque corrélation,
toutes les autres propriétés sont facultatives. Seules les propriétés
passées seront mises à jour, toutes les autres resteront inchangées.

En plus des [propriétés de corrélation standard](object#correlation), la
méthode accepte les paramètres suivants.

|Paramètre|Type|Description|
|----------|----|-----------|
|filter|object|Objet filtre de corrélation pour remplacer le filtre actuel.|
|operations|array|Opérations de corrélation pour remplacer les opérations existantes.|

[comment]: # ({/new-326bc80e})

[comment]: # ({new-13e1dc13})
### Valeurs de retour

`(object)` Retourne un objet contenant les identifiants des corrélations
mises à jour dans la propriété `correlationids`.

[comment]: # ({/new-13e1dc13})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-67bb2dfb})
#### Désactiver la corrélation

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "status": "1"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-67bb2dfb})

[comment]: # ({new-d6ef1778})
#### Remplacer les conditions, mais conserver la méthode d'évaluation

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "filter": {
            "conditions": [
                {
                    "type": 3,
                    "oldtag": "error",
                    "newtag": "ok"
                }
            ]
        }
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d6ef1778})

[comment]: # ({new-e7d15410})
### Voir également

-   [Filtre de Corrélation](object#filtre_de_correlation)
-   [Opération de Corrélation](object#operation_de_correlation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-40fa665f})
### Source

CCorrelation::update() dans
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-40fa665f})
