[comment]: # translation:outdated

[comment]: # ({new-89029829})
# correlation.delete

[comment]: # ({/new-89029829})

[comment]: # ({new-4922e104})
### Description

`object correlation.delete(array correlationids)`

Cette méthode permet de supprimer les corrélations.

[comment]: # ({/new-4922e104})

[comment]: # ({new-b445698c})
### Paramètres

`(array)` IDs des corrélations à supprimer.

[comment]: # ({/new-b445698c})

[comment]: # ({new-855af217})
### Valeurs de retour

`(object)` Renvoie un objet contenant les identifiants des corrélations
supprimées dans la variable `correlationids`.

[comment]: # ({/new-855af217})

[comment]: # ({new-c9f65268})
### Exemple

[comment]: # ({/new-c9f65268})

[comment]: # ({new-bfc49991})
#### Supprimer plusieurs corrélations

Supprimer deux corrélations.

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlaionids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bfc49991})

[comment]: # ({new-b0713c0f})
### Source

CCorrelation::delete() dans
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-b0713c0f})
