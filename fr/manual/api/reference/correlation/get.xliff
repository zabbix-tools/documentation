<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/api/reference/correlation/get.md">
    <body>
      <trans-unit id="7416ad19" xml:space="preserve">
        <source># correlation.get</source>
      </trans-unit>
      <trans-unit id="858936aa" xml:space="preserve">
        <source>### Description

`integer/array correlation.get(object parameters)`

The method allows to retrieve correlations according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="06a650a0" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|correlationids|string/array|Return only correlations with the given IDs.|
|selectFilter|query|Return a [`filter`](/manual/api/reference/correlation/object#correlation_filter) property with the correlation conditions.|
|selectOperations|query|Return an [`operations`](/manual/api/reference/correlation/object#correlation_operation) property with the correlation operations.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `correlationid`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="d14c4f20" xml:space="preserve">
        <source>#### Retrieve correlations

Retrieve all configured correlations together with correlation
conditions and operations. The filter uses the "and/or" evaluation type,
so the `formula` property is empty and `eval_formula` is generated
automatically.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="e7d15410" xml:space="preserve">
        <source>### See also

-   [Correlation filter](object#correlation_filter)
-   [Correlation operation](object#correlation_operation)</source>
      </trans-unit>
      <trans-unit id="bbab04bd" xml:space="preserve">
        <source>### Source

CCorrelation::get() in
*ui/include/classes/api/services/CCorrelation.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
