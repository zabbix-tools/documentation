[comment]: # translation:outdated

[comment]: # ({new-e205e572})
# > Objet Corrélation

Les objets suivants sont directement liés à l'API `correlation`.

[comment]: # ({/new-e205e572})

[comment]: # ({new-935ba6ef})
### Corrélation

L'objet de corrélation a les propriétés suivantes.

|Propriété|Type|Description|
|-----------|----|-----------|
|correlationid|string|*(lecture seule)* ID de la corrélation.|
|**name**<br>(requis)|string|Nom de la corrélation.|
|description|string|Description de la corrélation.|
|status|integer|Indique si la corrélation est activée ou désactivée.<br><br>Valeurs possibles:<br>0 - *(par défaut)* enabled;<br>1 - disabled.|

[comment]: # ({/new-935ba6ef})

[comment]: # ({new-d12c2169})
### Opération de Corrélation

L'objet d'opération de corrélation définit une opération qui sera
effectuée lorsqu'une corrélation est exécutée. Il a les propriétés
suivantes.

|Propriété|Type|Description|
|-----------|----|-----------|
|**type**<br>(requis)|integer|Type d'opération.<br><br>Valeurs possibles:<br>0 - ferme l'ancien évènement;<br>1 - ferme le nouvel évènement.|

[comment]: # ({/new-d12c2169})

[comment]: # ({new-d0662dba})
### Filtre de Corrélation

L'objet filtre de corrélation définit un ensemble de conditions à
respecter pour effectuer les opérations de corrélation configurées. Il a
les propriétés suivantes.

|Propriété|Type|Description|
|-----------|----|-----------|
|**evaltype**<br>(requis)|integer|Méthode d'évaluation de la condition du filtre.<br><br>Valeurs possibles:<br>0 - et/ou;<br>1 - et;<br>2 - ou;<br>3 - expression personnalisée.|
|**conditions**<br>(requis)|array|Ensemble de conditions de filtre à utiliser pour filtrer les résultats.|
|eval\_formula|string|*(lecture seule)* Expression générée qui sera utilisée pour évaluer les conditions de filtrage. L'expression contient des IDs qui référencent des conditions de filtrage spécifiques par son `formulaid`. La valeur de `eval_formula` est égale à la valeur de `formula` pour les filtres avec une expression personnalisée.|
|formula|string|Expression définie par l'utilisateur à utiliser pour évaluer les conditions des filtres avec une expression personnalisée. L'expression doit contenir des IDs qui référencent des conditions de filtrage spécifiques par son `formulaid`. Les IDs utilisés dans l'expression doivent correspondre exactement à ceux définis dans les conditions de filtrage: aucune condition ne peut rester inutilisée ou omise.<br><br>Obligatoire pour les filtres d'expression personnalisés.|

[comment]: # ({/new-d0662dba})

[comment]: # ({new-72ee5420})
#### Condition du filtre de corrélation

L'objet de condition de filtre de corrélation définit une condition
spécifique qui doit être vérifiée avant d'exécuter les opérations de
corrélation.

|Propriété|Type|Description|
|-----------|----|-----------|
|**type**<br>(requis)|integer|Type de condition.<br><br>Valeurs possibles:<br>0 - ancienne balise d'événement;<br>1 - nouvelle balise d'événement;<br>2 - nouveau groupe d'hôtes de l'événement;<br>3 - paire de balises d'événement;<br>4 - ancienne valeur de balise d'événement;<br>5 - nouvelle valeur de balise d'événement.|
|tag|string|Balise d'événement (ancienne ou nouvelle). Requis lorsque le type de condition est : 0, 1, 4, 5.|
|groupid|string|ID du groupe d'hôtes. Requis lorsque le type de condition est : 2.|
|oldtag|string|Ancienne balise d'événement. Requis lorsque le type de condition est : 3.|
|newtag|string|Ancienne balise d'événement. Requis lorsque le type de condition est : 3.|
|value|string|Valeur de la balise d'événement (ancienne ou nouvelle). Requis lorsque le type de condition est : 4, 5.|
|formulaid|string|ID unique arbitraire utilisé pour référencer la condition à partir d'une expression personnalisée. Ne peut contenir que des lettres majuscules. L'ID doit être défini par l'utilisateur lors de la modification des conditions de filtrage, mais sera généré de nouveau lors d'une demande ultérieure.|
|operator|integer|Opérateur de condition.<br><br>Requis lorsque le type de condition est : 2, 4, 5.|

::: notetip
Pour mieux comprendre comment utiliser des filtres avec
différents types d’expressions, voir des exemples sur les pages des
méthodes [correlation.get](get#retrieve_correlations) and
[correlation.create](create#using_a_custom_expression_filter).
:::

Les opérateurs et les valeurs suivants sont pris en charge pour chaque
type de condition.

|Condition|Nom de la Condition|Opérateurs pris en charge|Valeur attendue|
|---------|-------------------|--------------------------|---------------|
|2|Groupe d'hôtes|=, <>|ID du groupe d'hôtes.|
|4|Ancienne valeur de la balise d'événement|=, <>, like, not like|string|
|5|Nouvelle valeur de la balise d'événement|=, <>, like, not like|string|

[comment]: # ({/new-72ee5420})
