[comment]: # translation:outdated

[comment]: # ({new-fbeccd23})
# correlation.create

[comment]: # ({/new-fbeccd23})

[comment]: # ({new-c06fb521})
### Description

`object correlation.create(object/array correlations)`

Cette méthode permet de créer de nouvelles corrélations.

[comment]: # ({/new-c06fb521})

[comment]: # ({new-8a55e636})
### Paramètres

`(object/array)` Correlations à créer.

En plus des [propriétés de corrélation standard](object#correlation), la
méthode accepte les paramètres suivants.

|Paramètres|Type|Description|
|-----------|----|-----------|
|**operations**<br>(requis)|array|Opérations de corrélation à créer pour la corrélation.|
|**filter**<br>(requis)|object|Objet de filtre de corrélation pour la corrélation.|

[comment]: # ({/new-8a55e636})

[comment]: # ({new-88660193})
### Valeurs de retour

`(object)` Retourne un objet contenant les identifiants des corrélations
créées sous la variable `correlationids`. L'ordre des ID renvoyés
correspond à l'ordre des corrélations transmises.

[comment]: # ({/new-88660193})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7325d5f8})
#### Créer une nouvelle corrélation d'étiquette d'évènement

Créez une corrélation à l'aide de la méthode d'évaluation `ET/OU` avec
une condition et une opération. Par défaut, la corrélation sera activée.

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new event tag correlation",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "type": 1,
                    "tag": "ok"
                }
            ]
        },
        "operations": [
            {
                "type": 0
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7325d5f8})

[comment]: # ({new-a12df7f6})
#### Utilisation d'un filtre d'expression personnalisé

Créez une corrélation qui utilisera une condition de filtre
personnalisée. Les identifiants de formule "A" ou "B" ont été choisis
arbitrairement. Le type de condition sera "Groupe d'hôtes" avec
l'opérateur "<>".

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new host group correlation",
        "description": "a custom description",
        "status": 0,
        "filter": {
            "evaltype": 3,
            "formula": "A or B",
            "conditions": [
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "A"
                },
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "B"
                }
            ]
        },
        "operations": [
            {
                "type": 1
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a12df7f6})

[comment]: # ({new-e7d15410})
### Voir également

-   [Filtre de Corrélation](object#filtre_de_correlation)
-   [Opération de Corrélation](object#operation_de_correlation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-934ba89b})
### Source

CCorrelation::create() dans
*frontends/php/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-934ba89b})
