[comment]: # translation:outdated

[comment]: # ({new-31d5554a})
# hostprototype.get

[comment]: # ({/new-31d5554a})

[comment]: # ({new-d1534f8a})
### Description

`integer/array hostprototype.get(object parameters)`

The method allows to retrieve host prototypes according to the given
parameters.

[comment]: # ({/new-d1534f8a})

[comment]: # ({new-0d6ab1f2})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|hostids|string/array|Return only host prototypes with the given IDs.|
|discoveryids|string/array|Return only host prototype that belong to the given LLD rules.|
|inherited|boolean|If set to `true` return only items inherited from a template.|
|selectDiscoveryRule|query|Return the LLD rule that the host prototype belongs to in the `discoveryRule` property.|
|selectGroupLinks|query|Return the group links of the host prototype in the `groupLinks` property.|
|selectGroupPrototypes|query|Return the group prototypes of the host prototype in the `groupPrototypes` property.|
|selectInventory|query|Return the host prototype inventory in the `inventory` property.|
|selectParentHost|query|Return the host that the host prototype belongs to in the `parentHost` property.|
|selectTemplates|query|Return the templates linked to the host prototype in the `templates` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `hostid`, `host`, `name` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail on the [Generic Zabbix API information](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-0d6ab1f2})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a91c8b17})
#### Retrieving host prototypes from an LLD rule

Retrieve all host prototypes and their group links and group prototypes
from an LLD rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.get",
    "params": {
        "output": "extend",
        "selectGroupLinks": "extend",
        "selectGroupPrototypes": "extend",
        "discoveryids": "23554"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10092",
            "host": "{#HV.UUID}",
            "status": "0",
            "name": "{#HV.NAME}",
            "templateid": "0",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": "",
            "groupLinks": [
                {
                    "group_prototypeid": "4",
                    "hostid": "10092",
                    "groupid": "7",
                    "templateid": "0"
                }
            ],
            "groupPrototypes": [
                {
                    "group_prototypeid": "7",
                    "hostid": "10092",
                    "name": "{#CLUSTER.NAME}",
                    "templateid": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-a91c8b17})

[comment]: # ({new-749ec170})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype inventory](object#host_prototype_inventory)

[comment]: # ({/new-749ec170})

[comment]: # ({new-437ab274})
### Source

CHostPrototype::get() in
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-437ab274})
