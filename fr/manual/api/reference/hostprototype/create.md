[comment]: # translation:outdated

[comment]: # ({new-984854ac})
# hostprototype.create

[comment]: # ({/new-984854ac})

[comment]: # ({new-3ddb6b5d})
### Description

`object hostprototype.create(object/array hostPrototypes)`

This method allows to create new host prototypes.

[comment]: # ({/new-3ddb6b5d})

[comment]: # ({new-c4127cc1})
### Parameters

`(object/array)` Host prototypes to create.

Additionally to the [standard host prototype
properties](object#host_prototype), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**groupLinks**<br>(required)|array|Group links to be created for the host prototype.|
|**ruleid**<br>(required)|string|ID of the LLD rule that the host prototype belongs to.|
|groupPrototypes|array|Group prototypes to be created for the host prototype.|
|inventory|object|Host prototype inventory properties.|
|templates|object/array|Templates to be linked to the host prototype.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-c4127cc1})

[comment]: # ({new-b9613119})
### Return values

`(object)` Returns an object containing the IDs of the created host
prototypes under the `hostids` property. The order of the returned IDs
matches the order of the passed host prototypes.

[comment]: # ({/new-b9613119})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0ce023ac})
#### Creating a host prototype

Create a host prototype "{\#VM.NAME}" on LLD rule "23542" with a group
prototype "{\#HV.NAME}". Link it to host group "2".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.create",
    "params": {
        "host": "{#VM.NAME}",
        "ruleid": "23542",
        "groupLinks": [
            {
                "groupid": "2"
            }
        ],
        "groupPrototypes": [
            {
                "name": "{#HV.NAME}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0ce023ac})

[comment]: # ({new-c4a38bf3})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [Host prototype inventory](object#host_prototype_inventory)

[comment]: # ({/new-c4a38bf3})

[comment]: # ({new-4d10a354})
### Source

CHostPrototype::create() in
*frontends/php/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-4d10a354})
