[comment]: # translation:outdated

[comment]: # ({new-6e26e29b})
# > Host prototype object

The following objects are directly related to the `hostprototype` API.

[comment]: # ({/new-6e26e29b})

[comment]: # ({new-e69daa4d})
### Host prototype

The host prototype object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|hostid|string|*(readonly)* ID of the host prototype.|
|**host**<br>(required)|string|Technical name of the host prototype.|
|name|string|Visible name of the host prototype.<br><br>Default: `host` property value.|
|status|integer|Status of the host prototype.<br><br>Possible values are:<br>0 - *(default)* monitored host;<br>1 - unmonitored host.|
|templateid|string|*(readonly)* ID of the parent template host prototype.|
|tls\_connect|integer|Connections to host.<br><br>Possible values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_accept|integer|Connections from host.<br><br>Possible bitmap values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_issuer|string|Certificate issuer.|
|tls\_subject|string|Certificate subject.|
|tls\_psk\_identity|string|PSK identity. Required if either `tls_connect` or `tls_accept` has PSK enabled.|
|tls\_psk|string|The preshared key, at least 32 hex digits. Required if either `tls_connect` or `tls_accept` has PSK enabled.|

[comment]: # ({/new-e69daa4d})

[comment]: # ({new-e3297546})
### Host prototype inventory

The host prototype inventory object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|inventory\_mode|integer|Host prototype inventory population mode.<br><br>Possible values are:<br>-1 - disabled;<br>0 - *(default)* manual;<br>1 - automatic.|

[comment]: # ({/new-e3297546})

[comment]: # ({new-dabb4716})
### Group link

The group link object links a host prototype with a host group and has
the following properties.

|Property|Type|Description|
|--------|----|-----------|
|group\_prototypeid|string|*(readonly)* ID of the group link.|
|**groupid**<br>(required)|string|ID of the host group.|
|hostid|string|*(readonly)* ID of the host prototype|
|templateid|string|*(readonly)* ID of the parent template group link.|

[comment]: # ({/new-dabb4716})

[comment]: # ({new-d42c6308})
### Group prototype

The group prototype object defines a group that will be created for a
discovered host and has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|group\_prototypeid|string|*(readonly)* ID of the group prototype.|
|**name**<br>(required)|string|Name of the group prototype.|
|hostid|string|*(readonly)* ID of the host prototype|
|templateid|string|*(readonly)* ID of the parent template group prototype.|

[comment]: # ({/new-d42c6308})

[comment]: # ({new-ed8d8550})
### Custom interface

The custom interface object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|dns|string|DNS name used by the interface.<br><br>**Required** if the connection is made via DNS. Can contain macros.|
|ip|string|IP address used by the interface.<br><br>**Required** if the connection is made via IP. Can contain macros.|
|**main**<br>(required)|integer|Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.<br><br>Possible values are:<br>0 - not default;<br>1 - default.|
|**port**<br>(required)|string|Port number used by the interface. Can contain user and LLD macros.|
|**type**<br>(required)|integer|Interface type.<br><br>Possible values are:<br>1 - agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br>|
|**useip**<br>(required)|integer|Whether the connection should be made via IP.<br><br>Possible values are:<br>0 - connect using host DNS name;<br>1 - connect using host IP address for this host interface.|
|details|array|Additional object for interface. **Required** if interface 'type' is SNMP.|

[comment]: # ({/new-ed8d8550})

[comment]: # ({new-6ea1b012})
### Custom interface details

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**version**<br>(required)|integer|SNMP interface version.<br><br>Possible values are:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - *(default)* - use bulk requests.|
|community|string|SNMP community. Used only by SNMPv1 and SNMPv2 interfaces.|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - *(default)* - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/new-6ea1b012})
