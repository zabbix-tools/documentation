[comment]: # translation:outdated

[comment]: # ({new-9793e988})
# report.create

[comment]: # ({/new-9793e988})

[comment]: # ({new-a8301e64})
### Description

`object report.create(object/array reports)`

This method allows to create new scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-a8301e64})

[comment]: # ({new-869414cf})
### Parameters

`(object/array)` Scheduled reports to create.

Additionally to the [standard scheduled report
properties](object#report), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|users|object/array of objects|[Users](object#users) to send the report to.|
|user\_groups|object/array of objects|[User groups](object#user_groups) to send the report to.|

[comment]: # ({/new-869414cf})

[comment]: # ({new-05ef7a50})
### Return values

`(object)` Returns an object containing the IDs of the created scheduled
reports under the `reportids` property. The order of the returned IDs
matches the order of the passed scheduled reports.

[comment]: # ({/new-05ef7a50})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-1cfccf99})
#### Creating a scheduled report

Create a weekly report that will be prepared for the previous week every
Monday-Friday at 12:00 from 2021-04-01 to 2021-08-31.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "report.create",
    "params": {
        "userid": "1",
        "name": "Weekly report",
        "dashboardid": "1",
        "period": "1",
        "cycle": "1",
        "start_time": "43200",
        "weekdays": "31",
        "active_since": "2021-04-01",
        "active_till": "2021-08-31",
        "subject": "Weekly report",
        "message": "Report accompanying text",
        "status": "1",
        "description": "Report description",
        "users": [
            {
                "userid": "1",
                "access_userid": "1",
                "exclude": "0"
            },
            {
                "userid": "2",
                "access_userid": "0",
                "exclude": "1"
            }
        ],
        "user_groups": [
            {
                "usrgrpid": "7",
                "access_userid": "0"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1cfccf99})

[comment]: # ({new-c52572c8})
### See also

-   [Users](object#users)
-   [User groups](object#user_groups)

[comment]: # ({/new-c52572c8})

[comment]: # ({new-f8046691})
### Source

CReport::create() in *ui/include/classes/api/services/CReport.php*.

[comment]: # ({/new-f8046691})
