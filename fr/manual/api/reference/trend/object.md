[comment]: # translation:outdated

[comment]: # ({new-915ea6d8})
# > Trend object

The following objects are directly related to the `trend` API.

::: noteclassic
Trend objects differ depending on the item's type of
information. They are created by the Zabbix server and cannot be
modified via the API.
:::

[comment]: # ({/new-915ea6d8})

[comment]: # ({new-bf6c9e59})
### Float trend

The float trend object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|num|integer|Number of values within this hour.|
|value\_min|float|Hourly minimum value.|
|value\_avg|float|Hourly average value.|
|value\_max|float|Hourly maximum value.|

[comment]: # ({/new-bf6c9e59})

[comment]: # ({new-fa526f68})
### Integer trend

The integer trend object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|clock|timestamp|Time when that value was received.|
|itemid|string|ID of the related item.|
|num|integer|Number of values within this hour.|
|value\_min|integer|Hourly minimum value.|
|value\_avg|integer|Hourly average value.|
|value\_max|integer|Hourly maximum value.|

[comment]: # ({/new-fa526f68})
