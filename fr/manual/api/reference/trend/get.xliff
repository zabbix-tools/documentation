<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/api/reference/trend/get.md">
    <body>
      <trans-unit id="669b84c5" xml:space="preserve">
        <source># trend.get</source>
      </trans-unit>
      <trans-unit id="b49a69bd" xml:space="preserve">
        <source>### Description

`integer/array trend.get(object parameters)`

The method allows to retrieve trend data according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="e33350ec" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemids|string/array|Return only trends with the given item IDs.|
|time\_from|timestamp|Return only values that have been collected after or at the given time.|
|time\_till|timestamp|Return only values that have been collected before or at the given time.|
|countOutput|boolean|Count the number of retrieved objects.|
|limit|integer|Limit the amount of retrieved objects.|
|output|query|Set fields to output.|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="98fd9c3f" xml:space="preserve">
        <source>#### Retrieving item trend data

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "trend.get",
    "params": {
        "output": [
            "itemid",
            "clock",
            "num",
            "value_min",
            "value_avg",
            "value_max",
        ],
        "itemids": [
            "23715"
        ],
        "limit": "1"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23715",
            "clock": "1446199200",
            "num": "60",
            "value_min": "0.165",
            "value_avg": "0.2168",
            "value_max": "0.35",
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="852a9753" xml:space="preserve">
        <source>### Source

CTrend::get() in *ui/include/classes/api/services/CTrend.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
