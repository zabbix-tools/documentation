[comment]: # translation:outdated

[comment]: # ({new-669b84c5})
# trend.get

[comment]: # ({/new-669b84c5})

[comment]: # ({new-b49a69bd})
### Description

`integer/array trend.get(object parameters)`

The method allows to retrieve trend data according to the given
parameters.

[comment]: # ({/new-b49a69bd})

[comment]: # ({new-e33350ec})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|itemids|string/array|Return only trends with the given item IDs.|
|time\_from|timestamp|Return only values that have been collected after or at the given time.|
|time\_till|timestamp|Return only values that have been collected before or at the given time.|
|countOutput|boolean|Count the number of retrieved objects.|
|limit|integer|Limit the amount of retrieved objects.|
|output|query|Set fields to output.|

[comment]: # ({/new-e33350ec})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98fd9c3f})
#### Retrieving item trend data

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trend.get",
    "params": {
        "output": [
            "itemid",
            "clock",
            "num",
            "value_min",
            "value_avg",
            "value_max",
        ],
        "itemids": [
            "23715"
        ],
        "limit": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23715",
            "clock": "1446199200",
            "num": "60",
            "value_min": "0.1650",
            "value_avg": "0.2168",
            "value_max": "0.3500",
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-98fd9c3f})

[comment]: # ({new-852a9753})
### Source

CTrend::get() in
*frontends/php/include/classes/api/services/CTrend.php*.

[comment]: # ({/new-852a9753})
