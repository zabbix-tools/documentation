[comment]: # translation:outdated

[comment]: # ({new-43e64f56})
# graphitem.get

[comment]: # ({/new-43e64f56})

[comment]: # ({new-fe22ea70})
### Description

`integer/array graphitem.get(object parameters)`

The method allows to retrieve graph items according to the given
parameters.

[comment]: # ({/new-fe22ea70})

[comment]: # ({new-d30698ba})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|gitemids|string/array|Return only graph items with the given IDs.|
|graphids|string/array|Return only graph items that belong to the given graphs.|
|itemids|string/array|Return only graph items with the given item IDs.|
|type|integer|Return only graph items with the given type.<br><br>Refer to the [graph item object page](object#graph_item) for a list of supported graph item types.|
|selectGraphs|query|Return the graph that the item belongs to as an array in the `graphs` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `gitemid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|sortorder|string/array|^|

[comment]: # ({/new-d30698ba})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-37da5f20})
#### Retrieving graph items from a graph

Retrieve all graph items used in a graph with additional information
about the item and the host.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphitem.get",
    "params": {
        "output": "extend",
        "graphids": "387"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "gitemid": "1242",
            "graphid": "387",
            "itemid": "22665",
            "drawtype": "1",
            "sortorder": "1",
            "color": "FF5555",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,steal]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        },
        {
            "gitemid": "1243",
            "graphid": "387",
            "itemid": "22668",
            "drawtype": "1",
            "sortorder": "2",
            "color": "55FF55",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,softirq]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        },
        {
            "gitemid": "1244",
            "graphid": "387",
            "itemid": "22671",
            "drawtype": "1",
            "sortorder": "3",
            "color": "009999",
            "yaxisside": "0",
            "calc_fnc": "2",
            "type": "0",
            "key_": "system.cpu.util[,interrupt]",
            "hostid": "10001",
            "flags": "0",
            "host": "Template OS Linux"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-37da5f20})

[comment]: # ({new-09a26e71})
### See also

-   [Graph](/fr/manual/api/reference/graph/object#graph)

[comment]: # ({/new-09a26e71})

[comment]: # ({new-8d13cada})
### Source

CGraphItem::get() in
*frontends/php/include/classes/api/services/CGraphItem.php*.

[comment]: # ({/new-8d13cada})
