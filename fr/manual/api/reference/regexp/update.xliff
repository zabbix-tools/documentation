<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/api/reference/regexp/update.md">
    <body>
      <trans-unit id="ffe55246" xml:space="preserve">
        <source># regexp.update</source>
      </trans-unit>
      <trans-unit id="596faacb" xml:space="preserve">
        <source>### Description

`object regexp.update(object/array regularExpressions)`

This method allows to update existing global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="f26d83f8" xml:space="preserve">
        <source>### Parameters

`(object/array)` Regular expression properties to be updated.

The `regexpid` property must be defined for each object, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard properties](object#expressions),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|expressions|array|[Expressions](/manual/api/reference/regexp/object#expressions) options.|</source>
      </trans-unit>
      <trans-unit id="7a7252f7" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated regular
expressions under the `regexpids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="aef504e5" xml:space="preserve">
        <source>#### Updating global regular expression for file systems discovery.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "regexp.update",
    "params": {
      "regexpid": "1",
      "name": "File systems for discovery",
      "test_string": "",
      "expressions": [
        {
          "expression": "^(btrfs|ext2|ext3|ext4|reiser|xfs|ffs|ufs|jfs|jfs2|vxfs|hfs|apfs|refs|zfs)$",
          "expression_type": "3",
          "exp_delimiter": ",",
          "case_sensitive": "0"
        },
        {
          "expression": "^(ntfs|fat32|fat16)$",
          "expression_type": "3",
          "exp_delimiter": ",",
          "case_sensitive": "0"
        }
      ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "1"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="90c4873e" xml:space="preserve">
        <source>### Source

CRegexp::update() in *ui/include/classes/api/services/CRegexp.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
