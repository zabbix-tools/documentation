[comment]: # translation:outdated

[comment]: # ({new-06845577})
# Action

Cette classe est conçue pour travailler avec les actions.

Référentiel des objets:\

-   [Action](/fr/manual/api/reference/action/object#action)
-   [Condition des
    actions](/fr/manual/api/reference/action/object#action_condition)
-   [Opération des
    actions](/fr/manual/api/reference/action/object#action_operation)

Méthodes disponibles:\

-   [action.create](/fr/manual/api/reference/action/create) - créer de
    nouvelles actions
-   [action.delete](/fr/manual/api/reference/action/delete) - supprimer
    des actions
-   [action.get](/fr/manual/api/reference/action/get) - récupérer des
    actions
-   [action.update](/fr/manual/api/reference/action/update) - mettre à
    jour des actions

[comment]: # ({/new-06845577})
