[comment]: # translation:outdated

[comment]: # ({new-4f0fdd87})
# Template

This class is designed to work with templates.

Object references:\

-   [Template](/fr/manual/api/reference/template/object#template)

Available methods:\

-   [template.create](/fr/manual/api/reference/template/create) -
    creating new templates
-   [template.delete](/fr/manual/api/reference/template/delete) -
    deleting templates
-   [template.get](/fr/manual/api/reference/template/get) - retrieving
    templates
-   [template.massadd](/fr/manual/api/reference/template/massadd) -
    adding related objects to templates
-   [template.massremove](/fr/manual/api/reference/template/massremove) -
    removing related objects from templates
-   [template.massupdate](/fr/manual/api/reference/template/massupdate) -
    replacing or removing related objects from templates
-   [template.update](/fr/manual/api/reference/template/update) -
    updating templates

[comment]: # ({/new-4f0fdd87})
