[comment]: # translation:outdated

[comment]: # ({new-52ad42a2})
# Configuration

Cette classe est conçue pour exporter et importer des données de
configuration Zabbix.

Méthodes disponibles:\

-   [configuration.export](/fr/manual/api/reference/configuration/export) -
    exportation de la configuration
-   [configuration.import](/fr/manual/api/reference/configuration/import) -
    importation de la configuration

[comment]: # ({/new-52ad42a2})
