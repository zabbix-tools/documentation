[comment]: # translation:outdated

[comment]: # ({new-2593ef3e})
# LLD rule

This class is designed to work with low level discovery rules.

Object references:\

-   [LLD rule](/fr/manual/api/reference/discoveryrule/object#lld_rule)

Available methods:\

-   [discoveryrule.copy](/fr/manual/api/reference/discoveryrule/copy) -
    copying LLD rules
-   [discoveryrule.create](/fr/manual/api/reference/discoveryrule/create) -
    creating new LLD rules
-   [discoveryrule.delete](/fr/manual/api/reference/discoveryrule/delete) -
    deleting LLD rules
-   [discoveryrule.get](/fr/manual/api/reference/discoveryrule/get) -
    retrieving LLD rules
-   [discoveryrule.update](/fr/manual/api/reference/discoveryrule/update) -
    updating LLD rules

[comment]: # ({/new-2593ef3e})
