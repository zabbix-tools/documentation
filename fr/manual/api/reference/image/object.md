[comment]: # translation:outdated

[comment]: # ({new-e9ddc4ef})
# > Image object

The following objects are directly related to the `image` API.

[comment]: # ({/new-e9ddc4ef})

[comment]: # ({new-eee50efa})
### Image

The image object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|imageid|string|*(readonly)* ID of the image.|
|**name**<br>(required)|string|Name of the image.|
|imagetype|integer|Type of image.<br><br>Possible values:<br>1 - *(default)* icon;<br>2 - background image.|

[comment]: # ({/new-eee50efa})
