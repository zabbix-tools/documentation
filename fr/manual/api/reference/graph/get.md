[comment]: # translation:outdated

[comment]: # ({new-cdc53a5f})
# graph.get

[comment]: # ({/new-cdc53a5f})

[comment]: # ({new-d2f8406d})
### Description

`integer/array graph.get(object parameters)`

The method allows to retrieve graphs according to the given parameters.

[comment]: # ({/new-d2f8406d})

[comment]: # ({new-5b1c5b22})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|graphids|string/array|Return only graphs with the given IDs.|
|groupids|string/array|Return only graphs that belong to hosts in the given host groups.|
|templateids|string/array|Return only graph that belong to the given templates.|
|hostids|string/array|Return only graphs that belong to the given hosts.|
|itemids|string/array|Return only graphs that contain the given items.|
|templated|boolean|If set to `true` return only graphs that belong to templates.|
|inherited|boolean|If set to `true` return only graphs inherited from a template.|
|expandName|flag|Expand macros in the graph name.|
|selectGroups|query|Return the host groups that the graph belongs to in the `groups` property.|
|selectTemplates|query|Return the templates that the graph belongs to in the `templates` property.|
|selectHosts|query|Return the hosts that the graph belongs to in the `hosts` property.|
|selectItems|query|Return the items used in the graph in the `items` property.|
|selectGraphDiscovery|query|Return the graph discovery object in the `graphDiscovery` property. The graph discovery objects links the graph to a graph prototype from which it was created.<br><br>It has the following properties:<br>`graphid` - `(string)` ID of the graph;<br>`parent_graphid` - `(string)` ID of the graph prototype from which the graph has been created.|
|selectGraphItems|query|Return the graph items used in the graph in the `gitems` property.|
|selectDiscoveryRule|query|Return the low-level discovery rule that created the graph in the `discoveryRule` property.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the graph belongs to;<br>`hostid` - ID of the host that the graph belongs to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `graphid`, `name` and `graphtype`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-5b1c5b22})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-e7fae381})
#### Retrieving graphs from hosts

Retrieve all graphs from host "10107" and sort them by name.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.get",
    "params": {
        "output": "extend",
        "hostids": 10107,
        "sortfield": "name"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "612",
            "name": "CPU jumps",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "439",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "613",
            "name": "CPU load",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "433",
            "show_work_period": "1",
            "show_triggers": "1",
            "graphtype": "0",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "1",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "614",
            "name": "CPU utilization",
            "width": "900",
            "height": "200",
            "yaxismin": "0.0000",
            "yaxismax": "100.0000",
            "templateid": "387",
            "show_work_period": "1",
            "show_triggers": "0",
            "graphtype": "1",
            "show_legend": "1",
            "show_3d": "0",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "1",
            "ymax_type": "1",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "0"
        },
        {
            "graphid": "645",
            "name": "Disk space usage /",
            "width": "600",
            "height": "340",
            "yaxismin": "0.0000",
            "yaxismax": "0.0000",
            "templateid": "0",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0",
            "flags": "4"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-e7fae381})

[comment]: # ({new-e4f96e80})
### See also

-   [Discovery
    rule](/fr/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph item](/fr/manual/api/reference/graphitem/object#graph_item)
-   [Item](/fr/manual/api/reference/item/object#item)
-   [Host](/fr/manual/api/reference/host/object#host)
-   [Host group](/fr/manual/api/reference/hostgroup/object#host_group)
-   [Template](/fr/manual/api/reference/template/object#template)

[comment]: # ({/new-e4f96e80})

[comment]: # ({new-6f7b2d0a})
### Source

CGraph::get() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-6f7b2d0a})
