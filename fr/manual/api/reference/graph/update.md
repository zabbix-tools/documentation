[comment]: # translation:outdated

[comment]: # ({new-d2516189})
# graph.update

[comment]: # ({/new-d2516189})

[comment]: # ({new-992bc0de})
### Description

`object graph.update(object/array graphs)`

This method allows to update existing graphs.

[comment]: # ({/new-992bc0de})

[comment]: # ({new-72c99624})
### Parameters

`(object/array)` Graph properties to be updated.

The `graphid` property must be defined for each graph, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard graph properties](object#graph) the method
accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|gitems|array|Graph items to replace existing graph items. If a graph item has the `gitemid` property defined it will be updated, otherwise a new graph item will be created.|

[comment]: # ({/new-72c99624})

[comment]: # ({new-9a0b4500})
### Return values

`(object)` Returns an object containing the IDs of the updated graphs
under the `graphids` property.

[comment]: # ({/new-9a0b4500})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7b33f5ce})
#### Setting the maximum for the Y scale

Set the the maximum of the Y scale to a fixed value of 100.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.update",
    "params": {
        "graphid": "439",
        "ymax_type": 1,
        "yaxismax": 100
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "439"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7b33f5ce})

[comment]: # ({new-84b1446b})
### Source

CGraph::update() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-84b1446b})
