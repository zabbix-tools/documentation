[comment]: # translation:outdated

[comment]: # ({new-c0099aca})
# > Graph object

The following objects are directly related to the `graph` API.

[comment]: # ({/new-c0099aca})

[comment]: # ({new-9849a58f})
### Graph

The graph object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|graphid|string|*(readonly)* ID of the graph.|
|**height**<br>(required)|integer|Height of the graph in pixels.|
|**name**<br>(required)|string|Name of the graph|
|**width**<br>(required)|integer|Width of the graph in pixels.|
|flags|integer|*(readonly)* Origin of the graph.<br><br>Possible values are:<br>0 - *(default)* a plain graph;<br>4 - a discovered graph.|
|graphtype|integer|Graph's layout type.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - stacked;<br>2 - pie;<br>3 - exploded.|
|percent\_left|float|Left percentile.<br><br>Default: 0.|
|percent\_right|float|Right percentile.<br><br>Default: 0.|
|show\_3d|integer|Whether to show pie and exploded graphs in 3D.<br><br>Possible values:<br>0 - *(default)* show in 2D;<br>1 - show in 3D.|
|show\_legend|integer|Whether to show the legend on the graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|show\_work\_period|integer|Whether to show the working time on the graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|templateid|string|*(readonly)* ID of the parent template graph.|
|yaxismax|float|The fixed maximum value for the Y axis.<br><br>Default: 100.|
|yaxismin|float|The fixed minimum value for the Y axis.<br><br>Default: 0.|
|ymax\_itemid|string|ID of the item that is used as the maximum value for the Y axis.|
|ymax\_type|integer|Maximum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|
|ymin\_itemid|string|ID of the item that is used as the minimum value for the Y axis.|
|ymin\_type|integer|Minimum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|

[comment]: # ({/new-9849a58f})
