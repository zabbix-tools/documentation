[comment]: # translation:outdated

[comment]: # ({new-000fdd04})
# graph.create

[comment]: # ({/new-000fdd04})

[comment]: # ({new-e0ee1e5d})
### Description

`object graph.create(object/array graphs)`

This method allows to create new graphs.

[comment]: # ({/new-e0ee1e5d})

[comment]: # ({new-fc8b02f4})
### Parameters

`(object/array)` Graphs to create.

Additionally to the [standard graph properties](object#graph), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**gitems**<br>(required)|array|Graph items to be created for the graph.|

[comment]: # ({/new-fc8b02f4})

[comment]: # ({new-d64b873d})
### Return values

`(object)` Returns an object containing the IDs of the created graphs
under the `graphids` property. The order of the returned IDs matches the
order of the passed graphs.

[comment]: # ({/new-d64b873d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd12048c})
#### Creating a graph

Create a graph with two items.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.create",
    "params": {
        "name": "MySQL bandwidth",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00"
            },
            {
                "itemid": "22829",
                "color": "3333FF"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cd12048c})

[comment]: # ({new-88266563})
### See also

-   [Graph item](/fr/manual/api/reference/graphitem/object#graph_item)

[comment]: # ({/new-88266563})

[comment]: # ({new-4e535c8e})
### Source

CGraph::create() in
*frontends/php/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-4e535c8e})
