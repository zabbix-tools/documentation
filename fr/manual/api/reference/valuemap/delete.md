[comment]: # translation:outdated

[comment]: # ({new-a1e429c6})
# valuemap.delete

[comment]: # ({/new-a1e429c6})

[comment]: # ({new-b73af783})
### Description

`object valuemap.delete(array valuemapids)`

This method allows to delete value maps.

[comment]: # ({/new-b73af783})

[comment]: # ({new-db6b3809})
### Parameters

`(array)` IDs of the value maps to delete.

[comment]: # ({/new-db6b3809})

[comment]: # ({new-59feda24})
### Return values

`(object)` Returns an object containing the IDs of the deleted value
maps under the `valuemapids` property.

[comment]: # ({/new-59feda24})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bf1c93a7})
#### Deleting multiple value maps

Delete two value maps.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bf1c93a7})

[comment]: # ({new-908d048e})
### Source

CValueMap::delete() in
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-908d048e})
