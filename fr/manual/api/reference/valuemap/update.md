[comment]: # translation:outdated

[comment]: # ({new-6b085503})
# valuemap.update

[comment]: # ({/new-6b085503})

[comment]: # ({new-ab463b0c})
### Description

`object valuemap.update(object/array valuemaps)`

This method allows to update existing value maps.

[comment]: # ({/new-ab463b0c})

[comment]: # ({new-0094db2a})
### Parameters

`(object/array)` [Value map properties](object#value_map) to be updated.

The `valuemapid` property must be defined for each value map, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

[comment]: # ({/new-0094db2a})

[comment]: # ({new-0dbe7e67})
### Return values

`(object)` Returns an object containing the IDs of the updated value
maps under the `valuemapids` property.

[comment]: # ({/new-0dbe7e67})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-571fcc62})
#### Changing value map name

Change value map name to "Device status".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "name": "Device status"
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-571fcc62})

[comment]: # ({new-7b3b6bc4})
#### Changing mappings for one value map.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "valuemap.update",
    "params": {
        "valuemapid": "2",
        "mappings": [
            {
                "value": "0",
                "newvalue": "Online"
            },
            {
                "value": "1",
                "newvalue": "Offline"
            }
        ]
    },
    "auth": "57562fd409b3b3b9a4d916d45207bbcb",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "valuemapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7b3b6bc4})

[comment]: # ({new-fc02e5cf})
### Source

CValueMap::update() in
*frontends/php/include/classes/api/services/CValueMap.php*.

[comment]: # ({/new-fc02e5cf})
