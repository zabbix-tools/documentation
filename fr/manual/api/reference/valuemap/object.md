[comment]: # translation:outdated

[comment]: # ({new-41154bde})
# > Value map object

The following objects are directly related to the `valuemap` API.

[comment]: # ({/new-41154bde})

[comment]: # ({new-65471f47})
### Value map

The value map object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|valuemapid|string|*(readonly)* ID of the value map.|
|**name**<br>(required)|string|Name of the value map.|
|**mappings**<br>(required)|array|Value mappings for current value map. The mapping object is [described in detail below](object#value_mappings).|

[comment]: # ({/new-65471f47})

[comment]: # ({new-145a9918})
### Value mappings

The value mappings object defines value mappings of the value map. It
has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**value**<br>(required)|string|Original value.|
|**newvalue**<br>(required)|string|Value to which the original value is mapped to.|

[comment]: # ({/new-145a9918})
