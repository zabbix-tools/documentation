[comment]: # translation:outdated

[comment]: # ({new-9b6159cd})
# Discovery check

This class is designed to work with discovery checks.

Object references:\

-   [Discovery
    check](/fr/manual/api/reference/dcheck/object#discovery_check)

Available methods:\

-   [dcheck.get](/fr/manual/api/reference/dcheck/get) - retrieve
    discovery checks

[comment]: # ({/new-9b6159cd})
