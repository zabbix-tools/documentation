[comment]: # translation:outdated

[comment]: # ({new-2d53bfd4})
# Trigger prototype

This class is designed to work with trigger prototypes.

Object references:\

-   [Trigger
    prototype](/fr/manual/api/reference/triggerprototype/object#trigger_prototype)

Available methods:\

-   [triggerprototype.create](/fr/manual/api/reference/triggerprototype/create) -
    creating new trigger prototypes
-   [triggerprototype.delete](/fr/manual/api/reference/triggerprototype/delete) -
    deleting trigger prototypes
-   [triggerprototype.get](/fr/manual/api/reference/triggerprototype/get) -
    retrieving trigger prototypes
-   [triggerprototype.update](/fr/manual/api/reference/triggerprototype/update) -
    updating trigger prototypes

[comment]: # ({/new-2d53bfd4})
