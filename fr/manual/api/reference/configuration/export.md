[comment]: # translation:outdated

[comment]: # ({new-690a015b})
# configuration.export

[comment]: # ({/new-690a015b})

[comment]: # ({new-98d36809})
### Description

`string configuration.export(object parametres)`

Cette méthode permet d'exporter les données de configuration sous la
forme d'une chaîne sérialisée.

[comment]: # ({/new-98d36809})

[comment]: # ({new-ef6ad2c8})
### Paramètres

`(object)` Paramètres définissant les objets à exporter et le format à
utiliser.

|Paramètre|Type|Description|
|----------|----|-----------|
|**format**<br>(requis)|string|Format dans lequel les données doivent être exportées.<br><br>Valeurs possibles:<br>`json` - JSON;<br>`xml` - XML.|
|**options**<br>(requis)|object|Objets à exporter.<br><br>L'objet `options` a les paramètres suivants:<br>`groups` - `(array)` ID des groupes d'hôtes à exporter;<br>`hosts` - `(array)` IDs des hôtes à exporter;<br>`images` - `(array)` IDs des images à exporter;<br>`maps` - `(array)` IDs des cartes à exporter;<br>`screens` - `(array)` IDs des écrans à exporter;<br>`templates` - `(array)` IDs des modèles à exporter;<br>`valueMaps` - `(array)` ID des cartes de valeur à exporter.<br>|

[comment]: # ({/new-ef6ad2c8})

[comment]: # ({new-0bfd9762})
### Valeurs de retour

`(string)` Renvoie une chaîne sérialisée contenant les données de
configuration demandées.

[comment]: # ({/new-0bfd9762})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c446795})
#### Exporter un hôte

Exportez la configuration d'un hôte en tant que chaîne XML.

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.export",
    "params": {
        "options": {
            "hosts": [
                "10161"
            ]
        },
        "format": "xml"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>4.0</version><date>2018-03-29T06:54:34Z</date><groups><group><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><description/><proxy/><status>0</status><ipmi_authtype>-1</ipmi_authtype><ipmi_privilege>2</ipmi_privilege><ipmi_username/><ipmi_password/><tls_connect>1</tls_connect><tls_accept>1</tls_accept><tls_issuer/><tls_subject/><tls_psk_identity/><tls_psk/><templates/><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><default>1</default><type>1</type><useip>1</useip><ip>127.0.0.1</ip><dns/><port>10050</port><bulk>1</bulk><interface_ref>if1</interface_ref></interface></interfaces><applications><application><name>Application</name></application></applications><items><item><name>Item</name><type>0</type><snmp_community/><snmp_oid/><key>item.key</key><delay>30s</delay><history>90d</history><trends>365d</trends><status>0</status><value_type>3</value_type><allowed_hosts/><units/><snmpv3_contextname/><snmpv3_securityname/><snmpv3_securitylevel>0</snmpv3_securitylevel><snmpv3_authprotocol>0</snmpv3_authprotocol><snmpv3_authpassphrase/><snmpv3_privprotocol>0</snmpv3_privprotocol><snmpv3_privpassphrase/><params/><ipmi_sensor/><authtype>0</authtype><username/><password/><publickey/><privatekey/><port/><description/><inventory_link>0</inventory_link><applications><application><name>Application</name></application></applications><valuemap><name>Host status</name></valuemap><logtimefmt/><preprocessing/><jmx_endpoint/><timeout>3s</timeout><url/><query_fields/><posts/><status_codes>200</status_codes><follow_redirects>1</follow_redirects><post_type>0</post_type><http_proxy/><headers/><retrieve_mode>0</retrieve_mode><request_method>1</request_method><output_format>0</output_format><allow_traps>0</allow_traps><ssl_cert_file/><ssl_key_file/><ssl_key_password/><verify_peer>0</verify_peer><verify_host>0</verify_host><master_item/><interface_ref>if1</interface_ref></item></items><discovery_rules/><httptests/><macros/><inventory/></host></hosts><value_maps><value_map><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></value_map></value_maps></zabbix_export>\n",
    "id": 1
}
```

[comment]: # ({/new-5c446795})

[comment]: # ({new-a3a5fdbf})
### Source

CConfiguration::export() dans
*frontends/php/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/new-a3a5fdbf})
