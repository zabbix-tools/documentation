[comment]: # translation:outdated

[comment]: # ({new-1359fe15})
# configuration.import

[comment]: # ({/new-1359fe15})

[comment]: # ({new-fe885a4a})
### Description

`boolean configuration.import(object parametres)`

Cette méthode permet d'importer des données de configuration à partir
d'une chaîne sérialisée.

[comment]: # ({/new-fe885a4a})

[comment]: # ({new-147fd272})
### Paramètres

`(object)` Paramètres contenant les données à importer et les règles de
traitement des données.

|Paramètre|Type|Description|
|----------|----|-----------|
|**format**<br>(requis)|string|Format de la chaîne sérialisée.<br><br>Valeurs possibles:<br>`json` - JSON;<br>`xml` - XML.|
|**source**<br>(requis)|string|Chaîne sérialisée contenant les données de configuration.|
|**rules**<br>(requis)|object|Règles sur la façon dont les objets nouveaux et existants doivent être importés.<br><br>Le paramètre `rules` est décrit en détail dans le tableau ci-dessous.|

::: notetip
Si aucune règle n'est donnée, la configuration ne sera
pas mise à jour.
:::

L'objet `rules` prend en charge les paramètres suivants.

|Paramètre|Type|Description|
|----------|----|-----------|
|applications|object|Règles sur la façon d'importer des applications.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouvelles applications seront créées; par défaut: `false`;<br>`deleteMissing` - `(boolean)` Si initialisé à `true`, les applications non présentes dans les données importées seront supprimées de la base de données; par défaut: `false`.|
|discoveryRules|object|Règles sur la façon d'importer les règles LLD.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouvelles règles LLD seront créées; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les règles LLD existantes seront mises à jour; par défaut: `false`;<br>`deleteMissing` - `(boolean)` Si initialisé à `true`, Les règles LLD non présentes dans les données importées seront supprimées de la base de données; par défaut: `false`.|
|graphs|object|Règles sur la façon d'importer des graphiques.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux graphiques seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les graphiques existants seront mis à jour; par défaut: `false`;<br>`deleteMissing` - `(boolean)` Si initialisé à `true`, les graphiques non présents dans les données importées seront supprimés de la base de données; par défaut: `false`.|
|groups|object|Règles sur l'importation des groupes d'hôtes.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveau groupes d'hôtes seront créés; par défaut: `false`.|
|hosts|object|Règles sur la façon d'importer des hôtes.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux hôtes seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les hôtes existants seront mis à jour; par défaut: `false`.|
|images|object|Règles sur la façon d'importer des images.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouvelles images seront créées; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les images existantes seront mises à jour; par défaut: `false`.|
|items|object|Règles sur la façon d'importer des éléments.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux éléments seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les éléments existants seront mis à jour; par défaut: `false`;<br>`deleteMissing` - `(boolean)` Si initialisé à `true`, les éléments non présents dans les données importées seront supprimés de la base de données; par défaut: `false`.|
|maps|object|Règles sur la façon d'importer des cartes.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouvelles cartes seront créées; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les cartes existantes seront mises à jour; par défaut: `false`.|
|screens|object|Règles sur la façon d'importer des écrans.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux écrans seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les écrans existants seront mis à jour; par défaut: `false`.|
|templateLinkage|object|Règles sur la façon d'importer des liens de modèles.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux liens entre modèles et hôtes seront créés; par défaut: `false`.|
|templates|object|Règles sur la façon d'importer des modèles.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux modèles seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, mes modèles existants seront mis à jour; par défaut: `false`.|
|templateScreens|object|Règles sur la façon d'importer des écrans de modèle.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux écrans de modèle seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les écrans de modèle existants seront mis à jour; par défaut: `false`;<br>`deleteMissing` - `(boolean)` Si initialisé à `true`, les écrans de modèle non présents dans les données importées seront supprimés de la base de données; par défaut: `false`.|
|triggers|object|Règles sur la façon d'importer des déclencheurs.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouveaux déclencheurs seront créés; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les déclencheurs existants seront mis à jour; par défaut: `false`;<br>`deleteMissing` - `(boolean)` Si initialisé à `true`, les déclencheurs non présents dans les données importées seront supprimés de la base de données; par défaut: `false`.|
|valueMaps|object|Règles sur la façon d'importer des tables de correspondance.<br><br>Paramètres pris en charge:<br>`createMissing` - `(boolean)` Si initialisé à `true`, les nouvelles tables de correspondance seront créées; par défaut: `false`;<br>`updateExisting` - `(boolean)` Si initialisé à `true`, les tables de correspondance existantes seront mises à jour; par défaut: `false`.|

[comment]: # ({/new-147fd272})

[comment]: # ({new-08d02880})
### Valeurs de retour

`(boolean)` Renvoie `true` si l'importation a réussi.

[comment]: # ({/new-08d02880})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0801380e})
#### Importer des hôtes et des éléments

Importez l'hôte et les éléments contenus dans la chaîne XML. Si des
éléments de la chaîne XML sont manquants, ils seront supprimés de la
base de données et tout le reste ne sera pas modifié.

Requête:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.import",
    "params": {
        "format": "xml",
        "rules": {
            "applications": {
                "createMissing": true,
                "deleteMissing": false
            },
            "valueMaps": {
                "createMissing": true,
                "updateExisting": false
            },
            "hosts": {
                "createMissing": true,
                "updateExisting": true
            },
            "items": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            }
        },
        "source": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><zabbix_export><version>4.0</version><date>2012-04-18T11:20:14Z</date><groups><group><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><description/><proxy/><status>0</status><ipmi_authtype>-1</ipmi_authtype><ipmi_privilege>2</ipmi_privilege><ipmi_username/><ipmi_password/><tls_connect>1</tls_connect><tls_accept>1</tls_accept><tls_issuer/><tls_subject/><tls_psk_identity/><tls_psk/><templates/><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><default>1</default><type>1</type><useip>1</useip><ip>127.0.0.1</ip><dns/><port>10050</port><bulk>1</bulk><interface_ref>if1</interface_ref></interface></interfaces><applications><application><name>Application</name></application></applications><items><item><name>Item</name><type>0</type><snmp_community/><snmp_oid/><key>item.key</key><delay>30s</delay><history>90d</history><trends>365d</trends><status>0</status><value_type>3</value_type><allowed_hosts/><units/><snmpv3_contextname/><snmpv3_securityname/><snmpv3_securitylevel>0</snmpv3_securitylevel><snmpv3_authprotocol>0</snmpv3_authprotocol><snmpv3_authpassphrase/><snmpv3_privprotocol>0</snmpv3_privprotocol><snmpv3_privpassphrase/><params/><ipmi_sensor/><authtype>0</authtype><username/><password/><publickey/><privatekey/><port/><description/><inventory_link>0</inventory_link><applications><application><name>Application</name></application></applications><valuemap><name>Host status</name></valuemap><logtimefmt/><preprocessing/><interface_ref>if1</interface_ref><jmx_endpoint/><timeout>3s</timeout><url/><query_fields/><posts/><status_codes>200</status_codes><follow_redirects>1</follow_redirects><post_type>0</post_type><http_proxy/><headers/><retrieve_mode>0</retrieve_mode><request_method>1</request_method><output_format>0</output_format><allow_traps>0</allow_traps><ssl_cert_file/><ssl_key_file/><ssl_key_password/><verify_peer>0</verify_peer><verify_host>0</verify_host><master_item/></item></items><discovery_rules/><macros/><inventory/></host></hosts><triggers><trigger><expression>{Export host:item.key.last()}=0</expression><name>Trigger</name><url/><status>0</status><priority>2</priority><description>Host trigger</description><type>0</type><recovery_mode>1</recovery_mode><recovery_expression>{Export host:item.key.last()}=2</recovery_expression><dependencies/><tags/><correlation_mode>1</correlation_mode><correlation_tag>Tag 01</correlation_tag><manual_close>0</manual_close></trigger></triggers><graphs><graph><name>Graph</name><width>900</width><height>200</height><yaxismin>0.0000</yaxismin><yaxismax>100.0000</yaxismax><show_work_period>1</show_work_period><show_triggers>1</show_triggers><type>0</type><show_legend>1</show_legend><show_3d>0</show_3d><percent_left>0.0000</percent_left><percent_right>0.0000</percent_right><ymin_type_1>0</ymin_type_1><ymax_type_1>0</ymax_type_1><ymin_item_1>0</ymin_item_1><ymax_item_1>0</ymax_item_1><graph_items><graph_item><sortorder>0</sortorder><drawtype>0</drawtype><color>C80000</color><yaxisside>0</yaxisside><calc_fnc>7</calc_fnc><type>0</type><item><host>Export host</host><key>item.key</key></item></graph_item></graph_items></graph></graphs><value_maps><value_map><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></value_map></value_maps></zabbix_export>"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Réponse:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-0801380e})

[comment]: # ({new-c5744b74})
### Source

CConfiguration::import() dans
*frontends/php/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/new-c5744b74})
