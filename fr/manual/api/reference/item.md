[comment]: # translation:outdated

[comment]: # ({new-3816cd64})
# Item

This class is designed to work with items.

Object references:\

-   [Item](/fr/manual/api/reference/item/object#item)

Available methods:\

-   [item.create](/fr/manual/api/reference/item/create) - creating new
    items
-   [item.delete](/fr/manual/api/reference/item/delete) - deleting items
-   [item.get](/fr/manual/api/reference/item/get) - retrieving items
-   [item.update](/fr/manual/api/reference/item/update) - updating items

[comment]: # ({/new-3816cd64})
