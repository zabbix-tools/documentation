[comment]: # translation:outdated

[comment]: # ({new-ca37dbd6})
# Task

This class is designed to work with tasks.

Available methods:\

-   [task.create](/fr/manual/api/reference/task/create) - creating new
    tasks

[comment]: # ({/new-ca37dbd6})
