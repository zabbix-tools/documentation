[comment]: # translation:outdated

[comment]: # ({new-6bf4ac44})
# Media type

This class is designed to work with media types.

Object references:\

-   [Media type](/fr/manual/api/reference/mediatype/object#media_type)

Available methods:\

-   [mediatype.create](/fr/manual/api/reference/mediatype/create) -
    creating new media types
-   [mediatype.delete](/fr/manual/api/reference/mediatype/delete) -
    deleting media types
-   [mediatype.get](/fr/manual/api/reference/mediatype/get) - retrieving
    media types
-   [mediatype.update](/fr/manual/api/reference/mediatype/update) -
    updating media types

[comment]: # ({/new-6bf4ac44})
