[comment]: # translation:outdated

[comment]: # ({new-d373938d})
# Script

This class is designed to work with scripts.

Object references:\

-   [Script](/fr/manual/api/reference/script/object#script)

Available methods:\

-   [script.create](/fr/manual/api/reference/script/create) - create new
    scripts
-   [script.delete](/fr/manual/api/reference/script/delete) - delete
    scripts
-   [script.execute](/fr/manual/api/reference/script/execute) - run
    scripts
-   [script.get](/fr/manual/api/reference/script/get) - retrieve scripts
-   [script.getscriptsbyhosts](/fr/manual/api/reference/script/getscriptsbyhosts) -
    retrieve scripts for hosts
-   [script.update](/fr/manual/api/reference/script/update) - update
    scripts

[comment]: # ({/new-d373938d})
