[comment]: # translation:outdated

[comment]: # ({new-4312b82f})
# Event

This class is designed to work with events.

Object references:\

-   [Event](/fr/manual/api/reference/event/object#host)

Available methods:\

-   [event.get](/fr/manual/api/reference/event/get) - retrieving events
-   [event.acknowledge](/fr/manual/api/reference/event/acknowledge) -
    acknowledging events

[comment]: # ({/new-4312b82f})
