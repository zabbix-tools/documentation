[comment]: # translation:outdated

[comment]: # ({new-5ee95992})
# Icon map

This class is designed to work with icon maps.

Object references:\

-   [Icon map](/fr/manual/api/reference/iconmap/object#icon_map)
-   [Icon mapping](/fr/manual/api/reference/iconmap/object#icon_mapping)

Available methods:\

-   [iconmap.create](/fr/manual/api/reference/iconmap/create) - create
    new icon maps
-   [iconmap.delete](/fr/manual/api/reference/iconmap/delete) - delete
    icon maps
-   [iconmap.get](/fr/manual/api/reference/iconmap/get) - retrieve icon
    maps
-   [iconmap.update](/fr/manual/api/reference/iconmap/update) - update
    icon maps

[comment]: # ({/new-5ee95992})
