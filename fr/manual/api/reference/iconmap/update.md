[comment]: # translation:outdated

[comment]: # ({new-b7611ecd})
# iconmap.update

[comment]: # ({/new-b7611ecd})

[comment]: # ({new-15ec7450})
### Description

`object iconmap.update(object/array iconMaps)`

This method allows to update existing icon maps.

[comment]: # ({/new-15ec7450})

[comment]: # ({new-6f92ef73})
### Parameters

`(object/array)` Icon map properties to be updated.

The `iconmapid` property must be defined for each icon map, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard icon map properties](object#icon_map), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|mappings|array|Icon mappings to replace the existing icon mappings.|

[comment]: # ({/new-6f92ef73})

[comment]: # ({new-a1dbbc92})
### Return values

`(object)` Returns an object containing the IDs of the updated icon maps
under the `iconmapids` property.

[comment]: # ({/new-a1dbbc92})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7013c996})
#### Rename icon map

Rename an icon map to "OS icons".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.update",
    "params": {
        "iconmapid": "1",
        "name": "OS icons"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7013c996})

[comment]: # ({new-8df05cf6})
### See also

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-f35a7b0b})
### Source

CIconMap::update() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-f35a7b0b})
