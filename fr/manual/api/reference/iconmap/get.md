[comment]: # translation:outdated

[comment]: # ({new-8e76cf8c})
# iconmap.get

[comment]: # ({/new-8e76cf8c})

[comment]: # ({new-04265e54})
### Description

`integer/array iconmap.get(object parameters)`

The method allows to retrieve icon maps according to the given
parameters.

[comment]: # ({/new-04265e54})

[comment]: # ({new-efcde208})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|iconmapids|string/array|Return only icon maps with the given IDs.|
|sysmapids|string/array|Return only icon maps that are used in the given maps.|
|selectMappings|query|Return used icon mappings in the `mappings` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `iconmapid` and `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-efcde208})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-016ff9fc})
#### Retrieve an icon map

Retrieve all data about icon map "3".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.get",
    "params": {
        "iconmapids": "3",
        "output": "extend",
        "selectMappings": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mappings": [
                {
                    "iconmappingid": "3",
                    "iconmapid": "3",
                    "iconid": "6",
                    "inventory_link": "1",
                    "expression": "server",
                    "sortorder": "0"
                },
                {
                    "iconmappingid": "4",
                    "iconmapid": "3",
                    "iconid": "10",
                    "inventory_link": "1",
                    "expression": "switch",
                    "sortorder": "1"
                }
            ],
            "iconmapid": "3",
            "name": "Host type icons",
            "default_iconid": "2"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-016ff9fc})

[comment]: # ({new-8df05cf6})
### See also

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-2420d0f0})
### Source

CIconMap::get() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-2420d0f0})
