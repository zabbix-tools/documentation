[comment]: # translation:outdated

[comment]: # ({new-94ac227e})
# iconmap.create

[comment]: # ({/new-94ac227e})

[comment]: # ({new-ac3800fa})
### Description

`object iconmap.create(object/array iconMaps)`

This method allows to create new icon maps.

[comment]: # ({/new-ac3800fa})

[comment]: # ({new-ca8671b9})
### Parameters

`(object/array)` Icon maps to create.

Additionally to the [standard icon map properties](object#icon_map), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**mappings**<br>(required)|array|Icon mappings to be created for the icon map.|

[comment]: # ({/new-ca8671b9})

[comment]: # ({new-9dbb2dc6})
### Return values

`(object)` Returns an object containing the IDs of the created icon maps
under the `iconmapids` property. The order of the returned IDs matches
the order of the passed icon maps.

[comment]: # ({/new-9dbb2dc6})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-39a9c07f})
#### Create an icon map

Create an icon map to display hosts of different types.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.create",
    "params": {
        "name": "Type icons",
        "default_iconid": "2",
        "mappings": [
            {
                "inventory_link": 1,
                "expression": "server",
                "iconid": "3"
            },
            {
                "inventory_link": 1,
                "expression": "switch",
                "iconid": "4"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-39a9c07f})

[comment]: # ({new-8df05cf6})
### See also

-   [Icon mapping](object#icon_mapping)

[comment]: # ({/new-8df05cf6})

[comment]: # ({new-25aae98d})
### Source

CIconMap::create() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-25aae98d})
