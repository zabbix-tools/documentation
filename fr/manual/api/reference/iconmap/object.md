[comment]: # translation:outdated

[comment]: # ({new-b72559a5})
# > Icon map object

The following objects are directly related to the `iconmap` API.

[comment]: # ({/new-b72559a5})

[comment]: # ({new-74882a27})
### Icon map

The icon map object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|iconmapid|string|*(readonly)* ID of the icon map.|
|**default\_iconid**<br>(reqiured)|string|ID of the default icon.|
|**name**<br>(required)|string|Name of the icon map.|

[comment]: # ({/new-74882a27})

[comment]: # ({new-e88b40af})
### Icon mapping

The icon mapping object defines a specific icon to be used for hosts
with a certain inventory field value. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|iconmappingid|string|*(readonly)* ID of the icon map.|
|**iconid**<br>(required)|string|ID of the icon used by the icon mapping.|
|**expression**<br>(required)|string|Expression to match the inventory field against.|
|**inventory\_link**<br>(required)|integer|ID of the host inventory field.<br><br>Refer to the [host inventory object](/fr/manual/api/reference/host/object#host_inventory) for a list of supported inventory fields.|
|iconmapid|string|*(readonly)* ID of the icon map that the icon mapping belongs to.|
|sortorder|integer|*(readonly)* Position of the icon mapping in the icon map.|

[comment]: # ({/new-e88b40af})
