[comment]: # translation:outdated

[comment]: # ({new-5c9f356a})
# iconmap.delete

[comment]: # ({/new-5c9f356a})

[comment]: # ({new-5e250e88})
### Description

`object iconmap.delete(array iconMapIds)`

This method allows to delete icon maps.

[comment]: # ({/new-5e250e88})

[comment]: # ({new-f911029a})
### Parameters

`(array)` IDs of the icon maps to delete.

[comment]: # ({/new-f911029a})

[comment]: # ({new-44afa603})
### Return values

`(object)` Returns an object containing the IDs of the deleted icon maps
under the `iconmapids` property.

[comment]: # ({/new-44afa603})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c7690d07})
#### Delete multiple icon maps

Delete two icon maps.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "iconmap.delete",
    "params": [
        "2",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "iconmapids": [
            "2",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c7690d07})

[comment]: # ({new-6198efd4})
### Source

CIconMap::delete() in
*frontends/php/include/classes/api/services/CIconMap.php*.

[comment]: # ({/new-6198efd4})
