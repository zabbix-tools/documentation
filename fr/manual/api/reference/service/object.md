[comment]: # translation:outdated

[comment]: # ({new-b1a89f1b})
# > Service object

The following objects are directly related to the `service` API.

[comment]: # ({/new-b1a89f1b})

[comment]: # ({new-5d7a3d70})
### Service

The service object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|serviceid|string|*(readonly)* ID of the service.|
|**algorithm**<br>(required)|integer|Algorithm used to calculate the state of the service.<br><br>Possible values:<br>0 - do not calculate;<br>1 - problem, if at least one child has a problem;<br>2 - problem, if all children have problems.|
|**name**<br>(required)|string|Name of the service.|
|**showsla**<br>(required)|integer|Whether SLA should be calculated.<br><br>Possible values:<br>0 - do not calculate;<br>1 - calculate.|
|**sortorder**<br>(required)|integer|Position of the service used for sorting.|
|goodsla|float|Minimum acceptable SLA value. If the SLA drops lower, the service is considered to be in problem state.<br><br>Default: 99.9.|
|status|integer|*(readonly)* Whether the service is in OK or problem state.<br><br>If the service is in problem state, `status` is equal either to:<br>- the priority of the linked trigger if it is set to 2, "Warning" or higher (priorities 0, "Not classified" and 1, "Information" are ignored);<br>- the highest status of a child service in problem state.<br><br>If the service is in OK state, `status` is equal to 0.|
|triggerid|string|Trigger associated with the service. Can only be set for services that don't have children.<br><br>Default: 0|

[comment]: # ({/new-5d7a3d70})

[comment]: # ({new-c2041847})
### Service time

The service time object defines periods, when an service is scheduled to
be up or down. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|timeid|string|*(readonly)* ID of the service time.|
|**serviceid**<br>(required)|string|ID of the service.<br><br>Cannot be updated.|
|**ts\_from**<br>(required)|integer|Time when the service time comes into effect.<br><br>For onetime downtimes `ts_from` must be set as a Unix timestamp, for other types - as a specific time in a week, in seconds, for example, 90000 for Tue, 2:00 AM.|
|**ts\_to**<br>(required)|integer|Time when the service time ends.<br><br>For onetime uptimes `ts_to` must be set as a Unix timestamp, for other types - as a specific time in a week, in seconds, for example, 90000 for Tue, 2:00 AM.|
|**type**<br>(required)|integer|Service time type.<br><br>Possible values:<br>0 - planned uptime, repeated every week;<br>1 - planned downtime, repeated every week;<br>2 - one-time downtime.|
|note|string|Additional information about the service time.|

[comment]: # ({/new-c2041847})

[comment]: # ({new-c301cfb2})
### Service dependency

The service dependency object represents a dependency between services.
It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|linkid|string|*(readonly)* ID of the service dependency.|
|**servicedownid**<br>(required)|string|ID of the service, that a service depends on, that is, the child service. An service can have multiple children.|
|**serviceupid**<br>(required)|string|ID of the service, that is dependent on a service, that is, the parent service. An service can have multiple parents forming a directed graph.|
|**soft**<br>(required)|integer|Type of dependency between services.<br><br>Possible values:<br>0 - hard dependency;<br>1 - soft dependency.<br><br>An service can have only one hard-dependent parent. This attribute has no effect on status or SLA calculation and is only used to create a core service tree. Additional parents can be added as soft dependencies forming a graph.<br><br>An service can not be deleted if it has hard-dependent children.|

[comment]: # ({/new-c301cfb2})


[comment]: # ({new-e6daa7b4})
### Service alarm

::: noteclassic
Service alarms cannot be directly created, updated or
deleted via the Zabbix API.
:::

The service alarm objects represent a service's state change. It has
the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|clock|timestamp|Time when the service state change has happened.|
|value|integer|Status of the service.<br><br>Refer to the [service status property](object#service) for a list of possible values.|

[comment]: # ({/new-e6daa7b4})

[comment]: # ({new-6b88d3dc})
### Problem tag

Problem tags allow linking services with problem events. The problem tag
object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Problem tag name.|
|operator|integer|Mapping condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>2 - like.|
|value|string|Problem tag value.|

[comment]: # ({/new-6b88d3dc})
