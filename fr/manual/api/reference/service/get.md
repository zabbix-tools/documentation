[comment]: # translation:outdated

[comment]: # ({new-2587902b})
# service.get

[comment]: # ({/new-2587902b})

[comment]: # ({new-bfd5de7f})
### Description

`integer/array service.get(object parameters)`

The method allows to retrieve services according to the given
parameters.

[comment]: # ({/new-bfd5de7f})

[comment]: # ({new-e7637a1d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|serviceids|string/array|Return only services with the given IDs.|
|parentids|string/array|Return only services with the given hard-dependent parent services.|
|childids|string/array|Return only services that are hard-dependent on the given child services.|
|selectParent|query|Return the hard-dependent parent service in the `parent` property.|
|selectDependencies|query|Return child service dependencies in the `dependencies` property.|
|selectParentDependencies|query|Return parent service dependencies in the `parentDependencies` property.|
|selectTimes|query|Return service times in the `times` property.|
|selectAlarms|query|Return service alarms in the `alarms` property.|
|selectTrigger|query|Return the associated trigger in the `trigger` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `name` and `sortorder`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-e7637a1d})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cd4e4011})
#### Retrieving all services

Retrieve all data about all services and their dependencies.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.get",
    "params": {
        "output": "extend",
        "selectDependencies": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "serviceid": "2",
            "name": "Server 1",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9000",
            "sortorder": "0",
            "dependencies": []
        },
        {
            "serviceid": "3",
            "name": "Data center 1",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9000",
            "sortorder": "0",
            "dependencies": [
                {
                    "linkid": "11",
                    "serviceupid": "3",
                    "servicedownid": "2",
                    "soft": "0",
                    "sortorder": "0",
                    "serviceid": "2"
                },
                {
                    "linkid": "10",
                    "serviceupid": "3",
                    "servicedownid": "5",
                    "soft": "0",
                    "sortorder": "1",
                    "serviceid": "5"
                }
            ]
        },
        {
            "serviceid": "5",
            "name": "Server 2",
            "status": "0",
            "algorithm": "1",
            "triggerid": "0",
            "showsla": "1",
            "goodsla": "99.9900",
            "sortorder": "1",
            "dependencies": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cd4e4011})

[comment]: # ({new-05e3cf18})
### Source

CService::get() in
*frontends/php/include/classes/api/services/CService.php*.

[comment]: # ({/new-05e3cf18})
