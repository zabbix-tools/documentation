[comment]: # translation:outdated

[comment]: # ({new-f5d7e62f})
# service.create

[comment]: # ({/new-f5d7e62f})

[comment]: # ({new-c44d5a50})
### Description

`object service.create(object/array services)`

This method allows to create new services.

[comment]: # ({/new-c44d5a50})

[comment]: # ({new-69be5e01})
### Parameters

`(object/array)` services to create.

Additionally to the [standard service properties](object#service), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dependencies|array|Service dependencies.<br><br>Each service dependency has the following parameters:<br>- `dependsOnServiceid` - *(string)* ID of an service the service depends on, that is, the child service.<br>- `soft` - *(integer)* type of service dependency; refer to the [service dependency object page](object#service_dependency) for more information on dependency types.|
|parentid|string|ID of a hard-linked parent service.|
|times|array|Service times to be created for the service.|

[comment]: # ({/new-69be5e01})

[comment]: # ({new-f5eb6cfd})
### Return values

`(object)` Returns an object containing the IDs of the created services
under the `serviceids` property. The order of the returned IDs matches
the order of the passed services.

[comment]: # ({/new-f5eb6cfd})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-fc6bed3c})
#### Creating an service

Create an service that will be switched to problem state, if at least
one child has a problem. SLA calculation will be on and the minimum
acceptable SLA is 99.99%.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.create",
    "params": {
        "name": "Server 1",
        "algorithm": 1,
        "showsla": 1,
        "goodsla": 99.99,
        "sortorder": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-fc6bed3c})

[comment]: # ({new-fda6729e})
### Source

CService::create() in
*frontends/php/include/classes/api/services/CService.php*.

[comment]: # ({/new-fda6729e})
