[comment]: # translation:outdated

[comment]: # ({new-cd4a930c})
# service.update

[comment]: # ({/new-cd4a930c})

[comment]: # ({new-79afe9b7})
### Description

`object service.update(object/array services)`

This method allows to update existing services.

[comment]: # ({/new-79afe9b7})

[comment]: # ({new-5fb5ebb3})
### Parameters

`(object/array)` service properties to be updated.

The `serviceid` property must be defined for each service, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard service properties](object#service), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dependencies|array|Service dependencies to replace the current service dependencies.<br><br>Each service dependency has the following parameters:<br>- `dependsOnServiceid` - *(string)* ID of an service the service depends on, that is, the child service.<br>- `soft` - *(integer)* type of service dependency; refer to the [service dependency object page](object#service_dependency) for more information on dependency types.|
|parentid|string|ID of a hard-linked parent service.|
|times|array|Service times to replace the current service times.|

[comment]: # ({/new-5fb5ebb3})

[comment]: # ({new-9465ee4d})
### Return values

`(object)` Returns an object containing the IDs of the updated services
under the `serviceids` property.

[comment]: # ({/new-9465ee4d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b9918036})
#### Setting the parent of an service

Make service "3" the hard-linked parent of service "5".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "service.update",
    "params": {
        "serviceid": "5",
        "parentid": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "serviceids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b9918036})

[comment]: # ({new-263b1baa})
### See also

-   [service.adddependencies](adddependencies)
-   [service.addtimes](addtimes)
-   [service.deletedependencies](deletedependencies)
-   [service.deletetimes](deletetimes)

[comment]: # ({/new-263b1baa})

[comment]: # ({new-fd1b6894})
### Source

CService::update() in
*frontends/php/include/classes/api/services/CService.php*.

[comment]: # ({/new-fd1b6894})
