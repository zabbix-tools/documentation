[comment]: # translation:outdated

[comment]: # ({new-37c3aad7})
# dhost.get

[comment]: # ({/new-37c3aad7})

[comment]: # ({new-1309d6bb})
### Description

`integer/array dhost.get(object parameters)`

The method allows to retrieve discovered hosts according to the given
parameters.

[comment]: # ({/new-1309d6bb})

[comment]: # ({new-061d7cf0})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dhostids|string/array|Return only discovered hosts with the given IDs.|
|druleids|string/array|Return only discovered hosts that have been created by the given discovery rules.|
|dserviceids|string/array|Return only discovered hosts that are running the given services.|
|selectDRules|query|Return the discovery rule that detected the host as an array in the `drules` property.|
|selectDServices|query|Return the discovered services running on the host in the `dservices` property.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectDServices` - results will be sorted by `dserviceid`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `dhostid` and `druleid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-061d7cf0})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4b9c1e99})
#### Retrieve discovered hosts by discovery rule

Retrieve all hosts and the discovered services they are running that
have been detected by discovery rule "4".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dhost.get",
    "params": {
        "output": "extend",
        "selectDServices": "extend",
        "druleids": "4"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dservices": [
                {
                    "dserviceid": "1",
                    "dhostid": "1",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697227",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.1",
                    "dns": "station.company.lan"
                }
            ],
            "dhostid": "1",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697227",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "2",
                    "dhostid": "2",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.4",
                    "dns": "john.company.lan"
                }
            ],
            "dhostid": "2",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "3",
                    "dhostid": "3",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.26",
                    "dns": "printer.company.lan"
                }
            ],
            "dhostid": "3",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "4",
                    "dhostid": "4",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.7",
                    "dns": "mail.company.lan"
                }
            ],
            "dhostid": "4",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4b9c1e99})

[comment]: # ({new-e6b09247})
### See also

-   [Discovered
    service](/fr/manual/api/reference/dservice/object#discovered_service)
-   [Discovery
    rule](/fr/manual/api/reference/drule/object#discovery_rule)

[comment]: # ({/new-e6b09247})

[comment]: # ({new-8dc0c01a})
### Source

CDHost::get() in
*frontends/php/include/classes/api/services/CDHost.php*.

[comment]: # ({/new-8dc0c01a})
