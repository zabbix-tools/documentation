[comment]: # translation:outdated

[comment]: # ({4559774b-7fceb632})
# API info

Cette classe est conçue pour récupérer les informations relatives à l'API.

Méthodes disponibles :\

-   [apiinfo.version](/manual/api/reference/apiinfo/version) - récupérer la version de l'API Zabbix.

[comment]: # ({/4559774b-7fceb632})
