[comment]: # translation:outdated

[comment]: # ({new-517ea62c})
# Host prototype

This class is designed to work with host prototypes.

Object references:\

-   [Host
    prototype](/fr/manual/api/reference/hostprototype/object#host_prototype)
-   [Host prototype
    inventory](/fr/manual/api/reference/hostprototype/object#host_prototype_inventory)
-   [Group
    link](/fr/manual/api/reference/hostprototype/object#group_link)
-   [Group
    prototype](/fr/manual/api/reference/hostprototype/object#group_prototype)

Available methods:\

-   [hostprototype.create](/fr/manual/api/reference/hostprototype/create) -
    creating new host prototypes
-   [hostprototype.delete](/fr/manual/api/reference/hostprototype/delete) -
    deleting host prototypes
-   [hostprototype.get](/fr/manual/api/reference/hostprototype/get) -
    retrieving host prototypes
-   [hostprototype.update](/fr/manual/api/reference/hostprototype/update) -
    updating host prototypes

[comment]: # ({/new-517ea62c})
