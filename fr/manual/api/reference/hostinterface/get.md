[comment]: # translation:outdated

[comment]: # ({new-25793162})
# hostinterface.get

[comment]: # ({/new-25793162})

[comment]: # ({new-29074792})
### Description

`integer/array hostinterface.get(object parameters)`

The method allows to retrieve host interfaces according to the given
parameters.

[comment]: # ({/new-29074792})

[comment]: # ({new-a40e6018})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|hostids|string/array|Return only host interfaces used by the given hosts.|
|interfaceids|string/array|Return only host interfaces with the given IDs.|
|itemids|string/array|Return only host interfaces used by the given items.|
|triggerids|string/array|Return only host interfaces used by items in the given triggers.|
|selectItems|query|Return the items that use the interface in the `items` property.<br><br>Supports `count`.|
|selectHosts|query|Return the host that uses the interface as an array in the `hosts` property.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectItems`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `interfaceid`, `dns`, `ip`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|nodeids|string/array|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-a40e6018})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cbd2e952})
#### Retrieve host interfaces

Retrieve all data about the interfaces used by host "30057."

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.get",
    "params": {
        "output": "extend",
        "hostids": "30057"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "interfaceid": "30050",
            "hostid": "30057",
            "main": "1",
            "type": "1",
            "useip": "1",
            "ip": "127.0.0.1",
            "dns": "",
            "port": "10050",
            "bulk": "1"
        },
        {
            "interfaceid": "30067",
            "hostid": "30057",
            "main": "0",
            "type": "1",
            "useip": "0",
            "ip": "",
            "dns": "localhost",
            "port": "10050",
            "bulk": "1"
        },
        {
            "interfaceid": "30068",
            "hostid": "30057",
            "main": "1",
            "type": "2",
            "useip": "1",
            "ip": "127.0.0.1",
            "dns": "",
            "port": "161",
            "bulk": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-cbd2e952})

[comment]: # ({new-1c57700e})
### See also

-   [Host](/fr/manual/api/reference/host/object#host)
-   [Item](/fr/manual/api/reference/item/object#item)

[comment]: # ({/new-1c57700e})

[comment]: # ({new-d0cee877})
### Source

CHostInterface::get() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-d0cee877})
