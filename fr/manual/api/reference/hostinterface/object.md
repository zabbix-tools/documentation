[comment]: # translation:outdated

[comment]: # ({new-7ed888d2})
# > Host interface object

The following objects are directly related to the `hostinterface` API.

[comment]: # ({/new-7ed888d2})

[comment]: # ({new-ce77a3b0})
### Host interface

The host interface object has the following properties.

::: noteimportant
Note that both IP and DNS are required. If you do
not want to use DNS, set it to an empty string.
:::

|Property|Type|Description|
|--------|----|-----------|
|interfaceid|string|*(readonly)* ID of the interface.|
|**dns**<br>(required)|string|DNS name used by the interface.<br><br>Can be empty if the connection is made via IP.|
|**hostid**<br>(required)|string|ID of the host the interface belongs to.|
|**ip**<br>(required)|string|IP address used by the interface.<br><br>Can be empty if the connection is made via DNS.|
|**main**<br>(required)|integer|Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.<br><br>Possible values are:<br>0 - not default;<br>1 - default.|
|**port**<br>(required)|string|Port number used by the interface. Can contain user macros.|
|**type**<br>(required)|integer|Interface type.<br><br>Possible values are:<br>1 - agent;<br>2 - SNMP;<br>3 - IPMI;<br>4 - JMX.<br>|
|**useip**<br>(required)|integer|Whether the connection should be made via IP.<br><br>Possible values are:<br>0 - connect using host DNS name;<br>1 - connect using host IP address for this host interface.|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - *(default)* use bulk requests.|

[comment]: # ({/new-ce77a3b0})

[comment]: # ({new-3bb71fcc})
### Details tag

The details object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**version**<br>(required)|integer|SNMP interface version.<br><br>Possible values are:<br>1 - SNMPv1;<br>2 - SNMPv2c;<br>3 - SNMPv3|
|bulk|integer|Whether to use bulk SNMP requests.<br><br>Possible values are:<br>0 - don't use bulk requests;<br>1 - (default) - use bulk requests.|
|community|string|SNMP community (required). Used only by SNMPv1 and SNMPv2 interfaces.|
|securityname|string|SNMPv3 security name. Used only by SNMPv3 interfaces.|
|securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|authpassphrase|string|SNMPv3 authentication passphrase. Used only by SNMPv3 interfaces.|
|privpassphrase|string|SNMPv3 privacy passphrase. Used only by SNMPv3 interfaces.|
|authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - MD5;<br>1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 interfaces.<br><br>Possible values are:<br>0 - (default) - DES;<br>1 - AES128;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|contextname|string|SNMPv3 context name. Used only by SNMPv3 interfaces.|

[comment]: # ({/new-3bb71fcc})
