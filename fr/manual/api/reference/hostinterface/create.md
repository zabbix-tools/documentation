[comment]: # translation:outdated

[comment]: # ({new-0bfa3a60})
# hostinterface.create

[comment]: # ({/new-0bfa3a60})

[comment]: # ({new-e87d74af})
### Description

`object hostinterface.create(object/array hostInterfaces)`

This method allows to create new host interfaces.

[comment]: # ({/new-e87d74af})

[comment]: # ({new-e81b8e01})
### Parameters

`(object/array)` Host interfaces to create. The method accepts host
interfaces with the [standard host interface
properties](object#host_interface).

[comment]: # ({/new-e81b8e01})

[comment]: # ({new-4b59ae6b})
### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property. The order of the returned
IDs matches the order of the passed host interfaces.

[comment]: # ({/new-4b59ae6b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-150295f4})
#### Create a new interface

Create a secondary IP agent interface on host "30052."

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.create",
    "params": {
        "hostid": "30052",
        "dns": "",
        "ip": "127.0.0.1",
        "main": 0,
        "port": "10050",
        "type": 1,
        "useip": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30062"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-150295f4})

[comment]: # ({new-c22176c4})
### See also

-   [hostinterface.massadd](massadd)
-   [host.massadd](/fr/manual/api/reference/host/massadd)

[comment]: # ({/new-c22176c4})

[comment]: # ({new-9afed0b0})
### Source

CHostInterface::create() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9afed0b0})

[comment]: # ({new-5b41f219})
### Source

CHostInterface::create() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-5b41f219})
