[comment]: # translation:outdated

[comment]: # ({new-59b410c0})
# hostinterface.update

[comment]: # ({/new-59b410c0})

[comment]: # ({new-253ed57f})
### Description

`object hostinterface.update(object/array hostInterfaces)`

This method allows to update existing host interfaces.

[comment]: # ({/new-253ed57f})

[comment]: # ({new-a99ac0ad})
### Parameters

`(object/array)` [Host interface properties](object#host_interface) to
be updated.

The `interfaceid` property must be defined for each host interface, all
other properties are optional. Only the given properties will be
updated, all others will remain unchanged.

[comment]: # ({/new-a99ac0ad})

[comment]: # ({new-d3be3202})
### Return values

`(object)` Returns an object containing the IDs of the updated host
interfaces under the `interfaceids` property.

[comment]: # ({/new-d3be3202})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-eb8b35e8})
#### Changing a host interface port

Change the port of a host interface.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.update",
    "params": {
        "interfaceid": "30048",
        "port": "30050"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30048"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-eb8b35e8})

[comment]: # ({new-97e4c8d0})
### Source

CHostInterface::update() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-97e4c8d0})
