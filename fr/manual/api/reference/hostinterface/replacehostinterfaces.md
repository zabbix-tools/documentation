[comment]: # translation:outdated

[comment]: # ({new-2898fc4b})
# hostinterface.replacehostinterfaces

[comment]: # ({/new-2898fc4b})

[comment]: # ({new-f14e7778})
### Description

`object hostinterface.replacehostinterfaces(object parameters)`

This method allows to replace all host interfaces on a given host.

[comment]: # ({/new-f14e7778})

[comment]: # ({new-f5cf0b40})
### Parameters

`(object)` Parameters containing the ID of the host to be updated and
the new host interfaces.

|Parameter|Type|Description|
|---------|----|-----------|
|**hostid**<br>(required)|string|ID of the host to be updated.|
|**interfaces**<br>(required)|object/array|Host interfaces to replace the current host interfaces with.|

[comment]: # ({/new-f5cf0b40})

[comment]: # ({new-88283807})
### Return values

`(object)` Returns an object containing the IDs of the created host
interfaces under the `interfaceids` property.

[comment]: # ({/new-88283807})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-98e67db1})
#### Replacing host interfaces

Replace all host interfaces with a single agent interface.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.replacehostinterfaces",
    "params": {
        "hostid": "30052",
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "main": 1,
            "port": "10050",
            "type": 1,
            "useip": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30081"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-98e67db1})

[comment]: # ({new-272c42ef})
### See also

-   [host.update](/fr/manual/api/reference/host/update)
-   [host.massupdate](/fr/manual/api/reference/host/massupdate)

[comment]: # ({/new-272c42ef})

[comment]: # ({new-9d1e0b55})
### Source

CHostInterface::replaceHostInterfaces() in
*frontends/php/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-9d1e0b55})
