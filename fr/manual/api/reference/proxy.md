[comment]: # translation:outdated

[comment]: # ({new-b9b7d8da})
# Proxy

This class is designed to work with proxies.

Object references:\

-   [Proxy](/fr/manual/api/reference/proxy/object#proxy)
-   [Proxy
    interface](/fr/manual/api/reference/proxy/object#proxy_interface)

Available methods:\

-   [proxy.create](/fr/manual/api/reference/proxy/create) - create new
    proxies
-   [proxy.delete](/fr/manual/api/reference/proxy/delete) - delete
    proxies
-   [proxy.get](/fr/manual/api/reference/proxy/get) - retrieve proxies
-   [proxy.update](/fr/manual/api/reference/proxy/update) - update
    proxies

[comment]: # ({/new-b9b7d8da})
