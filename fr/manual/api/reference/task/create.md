[comment]: # translation:outdated

[comment]: # ({new-c9bc126a})
# task.create

[comment]: # ({/new-c9bc126a})

[comment]: # ({new-0e3179d3})
### Description

`object task.create(object task)`

This method allows to create new task.

[comment]: # ({/new-0e3179d3})

[comment]: # ({new-2574c234})
### Parameters

`(object)` A task to create.

The method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**type**<br>(required)|integer|Task type.<br><br>Possible values:<br>6 - Check now.|
|**itemids**<br>(required)|string/array|IDs of items and low-level discovery rules.<br><br>Item or discovery rule must of the following type:<br>0 - Zabbix agent;<br>1 - SNMPv1 agent;<br>3 - simple check;<br>4 - SNMPv2 agent;<br>5 - Zabbix internal;<br>6 - SNMPv3 agent;<br>8 - Zabbix aggregate;<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>15 - calculated;<br>16 - JMX agent.|

[comment]: # ({/new-2574c234})

[comment]: # ({new-918de761})
If item or discovery ruls is of type *Dependent item*, then top level master item must be of type:
-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent

[comment]: # ({/new-918de761})

[comment]: # ({new-ab87ce2a})
### Return values

`(object)` Returns an object containing the IDs of the created tasks
under the `taskids` property. One task is created for each item and
low-level discovery rule. The order of the returned IDs matches the
order of the passed `itemids`.

[comment]: # ({/new-ab87ce2a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c5fa6f1})
#### Creating a task

Create a task `check now` for two items. One is an item, the other is a
low-level discovery rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": {
        "type": "6",
        "itemids": ["10092", "10093"],
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5c5fa6f1})

[comment]: # ({new-d87c906b})
### Source

CTask::create() in
*frontends/php/include/classes/api/services/CTask.php*.

[comment]: # ({/new-d87c906b})

[comment]: # ({new-be350bd3})
### Source

CTask::create() in *ui/include/classes/api/services/CTask.php*.

[comment]: # ({/new-be350bd3})
