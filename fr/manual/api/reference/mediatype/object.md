[comment]: # translation:outdated

[comment]: # ({new-2fbbc9a6})
# > Media type object

The following objects are directly related to the `mediatype` API.

[comment]: # ({/new-2fbbc9a6})

[comment]: # ({new-4630c87a})
### Media type

The media type object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|mediatypeid|string|*(readonly)* ID of the media type.|
|**description**<br>(required)|string|Name of the media type.|
|**type**<br>(required)|integer|Transport used by the media type.<br><br>Possible values:<br>0 - e-mail;<br>1 - script;<br>2 - SMS;<br>3 - Jabber;<br>100 - Ez Texting.|
|exec\_path|string|For script media types `exec_path` contains the name of the executed script.<br><br>For Ez Texting `exec_path` contains the message text limit.<br>Possible text limit values:<br>0 - USA (160 characters);<br>1 - Canada (136 characters).<br><br>Required for script and Ez Texting media types.|
|gsm\_modem|string|Serial device name of the GSM modem.<br><br>Required for SMS media types.|
|passwd|string|Authentication password.<br><br>Required for Jabber and Ez Texting media types.|
|smtp\_email|string|Email address from which notifications will be sent.<br><br>Required for email media types.|
|smtp\_helo|string|SMTP HELO.<br><br>Required for email media types.|
|smtp\_server|string|SMTP server.<br><br>Required for email media types.|
|status|integer|Whether the media type is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|username|string|Username or Jabber identifier.<br><br>Required for Jabber and Ez Texting media types.|
|exec\_params|string|Script parameters.<br><br>Each parameter ends with a new line feed.|
|maxsessions|integer|The maximum number of alerts that can be processed in parallel.<br><br>Possible values for SMS:<br>1 - *(default)*<br><br>Possible values for other media types:<br>0-100|
|maxattempts|integer|The maximum number of attempts to send an alert.<br><br>Possible values:<br>1-10<br><br>Default value:<br>3|
|attempt\_interval|string|The interval between retry attempts. Accepts seconds and time unit with suffix.<br><br>Possible values:<br>0-60s<br><br>Default value:<br>10s|

[comment]: # ({/new-4630c87a})

[comment]: # ({new-46e1e59a})
### Webhook parameters

Parameters passed to webhook script when it is called, have the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value, support macros. Supported macros described on [page](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-46e1e59a})

[comment]: # ({new-parameters})
### Script parameters

Parameters passed to a script when it is being called have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|**sortorder**<br>(required)|integer|The order in which the parameters will be passed to the script as command-line arguments. Starting with 0 as the first one.|
|value|string|Parameter value, supports macros.<br>Supported macros are described on the [Supported macros](/manual/appendix/macros/supported_by_location) page.|

[comment]: # ({/new-parameters})

[comment]: # ({new-9c81491f})
### Message template

The message template object defines a template that will be used as a
default message for action operations to send a notification. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**eventsource**<br>(required)|integer|Event source.<br><br>Possible values:<br>0 - triggers;<br>1 - discovery;<br>2 - autoregistration;<br>3 - internal;<br>4 - services.|
|**recovery**<br>(required)|integer|Operation mode.<br><br>Possible values:<br>0 - operations;<br>1 - recovery operations;<br>2 - update operations.|
|subject|string|Message subject.|
|message|string|Message text.|

[comment]: # ({/new-9c81491f})
