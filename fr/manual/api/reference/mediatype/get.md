[comment]: # translation:outdated

[comment]: # ({new-16011390})
# mediatype.get

[comment]: # ({/new-16011390})

[comment]: # ({new-10e7fdda})
### Description

`integer/array mediatype.get(object parameters)`

The method allows to retrieve media types according to the given
parameters.

[comment]: # ({/new-10e7fdda})

[comment]: # ({new-dde2e026})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|mediatypeids|string/array|Return only media types with the given IDs.|
|mediaids|string/array|Return only media types used by the given media.|
|userids|string/array|Return only media types used by the given users.|
|selectUsers|query|Return the users that use the media type in the `users` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `mediatypeid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-dde2e026})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-173c5ab7})
#### Retrieving media types

Retrieve all configured media types.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mediatypeid": "1",
            "type": "0",
            "description": "Email",
            "smtp_server": "mail.company.com",
            "smtp_helo": "company.com",
            "smtp_email": "zabbix@company.com",
            "exec_path": "",
            "gsm_modem": "",
            "username": "",
            "passwd": "",
            "status": "0",
            "maxsessions": "1",
            "maxattempts": "7",
            "attempt_interval": "10s"
        },
        {
            "mediatypeid": "2",
            "type": "3",
            "description": "Jabber",
            "smtp_server": "",
            "smtp_helo": "",
            "smtp_email": "",
            "exec_path": "",
            "gsm_modem": "",
            "username": "jabber@company.com",
            "passwd": "zabbix",
            "status": "0",
            "maxsessions": "1",
            "maxattempts": "7",
            "attempt_interval": "10s"
        },
        {
            "mediatypeid": "3",
            "type": "2",
            "description": "SMS",
            "smtp_server": "",
            "smtp_helo": "",
            "smtp_email": "",
            "exec_path": "",
            "gsm_modem": "/dev/ttyS0",
            "username": "",
            "passwd": "",
            "status": "0",
            "maxsessions": "1",
            "maxattempts": "7",
            "attempt_interval": "10s"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-173c5ab7})

[comment]: # ({new-621ff52f})

#### Retrieve script and webhook media types

The following example returns three media types:

-   script media type with parameters;
-   script media type without parameters;
-   webhook media type with parameters.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "mediatype.get",
    "params": {
        "output": ["mediatypeid", "name", "parameters"],
        "filter": {
            "type": [1, 4]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mediatypeid": "10",
            "name": "Script with parameters",
            "parameters": [
                {
                    "sortorder": "0",
                    "value": "{ALERT.SENDTO}"
                },
                {
                    "sortorder": "1",
                    "value": "{EVENT.NAME}"
                },
                {
                    "sortorder": "2",
                    "value": "{ALERT.MESSAGE}"
                },
                {
                    "sortorder": "3",
                    "value": "Zabbix alert"
                }
            ]
        },
        {
            "mediatypeid": "13",
            "name": "Script without parameters",
            "parameters": []
        },
        {
            "mediatypeid": "11",
            "name": "Webhook",
            "parameters": [
                {
                    "name": "alert_message",
                    "value": "{ALERT.MESSAGE}"
                },
                {
                    "name": "event_update_message",
                    "value": "{EVENT.UPDATE.MESSAGE}"
                },
                {
                    "name": "host_name",
                    "value": "{HOST.NAME}"
                },
                {
                    "name": "trigger_description",
                    "value": "{TRIGGER.DESCRIPTION}"
                },
                {
                    "name": "trigger_id",
                    "value": "{TRIGGER.ID}"
                },
                {
                    "name": "alert_source",
                    "value": "Zabbix"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-621ff52f})

[comment]: # ({new-039ccba1})
### See also

-   [User](/fr/manual/api/reference/user/object#user)

[comment]: # ({/new-039ccba1})

[comment]: # ({new-b9d01f00})
### Source

CMediaType::get() in
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-b9d01f00})
