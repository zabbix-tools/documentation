[comment]: # translation:outdated

[comment]: # ({new-97b0e55a})
# mediatype.update

[comment]: # ({/new-97b0e55a})

[comment]: # ({new-4cbfe789})
### Description

`object mediatype.update(object/array mediaTypes)`

This method allows to update existing media types.

[comment]: # ({/new-4cbfe789})

[comment]: # ({new-7db8f4bd})
### Parameters

`(object/array)` [Media type properties](object#media_type) to be
updated.

The `mediatypeid` property must be defined for each media type, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

[comment]: # ({/new-7db8f4bd})

[comment]: # ({new-665d000d})
### Return values

`(object)` Returns an object containing the IDs of the updated media
types under the `mediatypeids` property.

[comment]: # ({/new-665d000d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8d3ba0cc})
#### Enabling a media type

Enable a media type, that is, set its status to 0.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.update",
    "params": {
        "mediatypeid": "6",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8d3ba0cc})

[comment]: # ({new-b9cdc31e})
### Source

CMediaType::update() in
*frontends/php/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-b9cdc31e})
