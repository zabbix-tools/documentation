[comment]: # translation:outdated

[comment]: # ({new-e4b5aac4})
# action.update

[comment]: # ({/new-e4b5aac4})

[comment]: # ({new-b1673b49})
### Description

`object action.update(object/array actions)`

This method allows to update existing actions.

[comment]: # ({/new-b1673b49})

[comment]: # ({new-703e85ce})
### Parameters

`(object/array)` Action properties to be updated.

The `actionid` property must be defined for each action, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard action properties](object#action), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|filter|object|Action filter object to replace the current filter.|
|operations|array|Action operations to replace existing operations.|
|recovery\_operations|array|Action recovery operations to replace existing recovery operations.|
|acknowledge\_operations|array|Action acknowledge operations to replace existing acknowledge operations.|

[comment]: # ({/new-703e85ce})

[comment]: # ({new-8cbaa103})
### Return values

`(object)` Returns an object containing the IDs of the updated actions
under the `actionids` property.

[comment]: # ({/new-8cbaa103})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-77b78ec5})
#### Disable action

Disable action, that is, set its status to "1".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-77b78ec5})

[comment]: # ({new-755496f7})
### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

[comment]: # ({/new-755496f7})

[comment]: # ({new-df9f1d35})
### Source

CAction::update() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-df9f1d35})
