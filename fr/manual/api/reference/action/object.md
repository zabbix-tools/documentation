[comment]: # translation:outdated

[comment]: # ({new-502c65c3})
# > Action object

The following objects are directly related to the `action` API.

[comment]: # ({/new-502c65c3})

[comment]: # ({new-73117797})
### Action

The action object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|actionid|string|*(readonly)* ID of the action.|
|**esc\_period**<br>(required)|string|Default operation step duration. Must be greater than 60 seconds. Accepts seconds, time unit with suffix and user macro.|
|**eventsource**<br>(required)|integer|*(constant)* Type of events that the action will handle.<br><br>Refer to the [event "source" property](/fr/manual/api/reference/event/object#event) for a list of supported event types.|
|**name**<br>(required)|string|Name of the action.|
|def\_longdata|string|Problem message text.|
|def\_shortdata|string|Problem message subject.|
|r\_longdata|string|Recovery message text.|
|r\_shortdata|string|Recovery message subject.|
|ack\_longdata|string|Acknowledge operation message text.|
|ack\_shortdata|string|Acknowledge operation message subject.|
|status|integer|Whether the action is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|maintenance\_mode|integer|Whether to pause escalation during maintenance periods or not.<br><br>Possible values:<br>0 - Don't pause escalation;<br>1 - *(default)* Pause escalation.|

[comment]: # ({/new-73117797})

[comment]: # ({new-8757165b})
### Action operation

The action operation object defines an operation that will be performed
when an action is executed. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|**operationtype**<br>(required)|integer|Type of operation.<br><br>Possible values:<br>0 - send message;<br>1 - remote command;<br>2 - add host;<br>3 - remove host;<br>4 - add to host group;<br>5 - remove from host group;<br>6 - link to template;<br>7 - unlink from template;<br>8 - enable host;<br>9 - disable host;<br>10 - set host inventory mode.|
|actionid|string|ID of the action that the operation belongs to.|
|esc\_period|string|Duration of an escalation step in seconds. Must be greater than 60 seconds. Accepts seconds, time unit with suffix and user macro. If set to 0 or 0s, the default action escalation period will be used.<br><br>Default: 0s.|
|esc\_step\_from|integer|Step to start escalation from.<br><br>Default: 1.|
|esc\_step\_to|integer|Step to end escalation at.<br><br>Default: 1.|
|evaltype|integer|Operation condition evaluation method.<br><br>Possible values:<br>0 - *(default)* AND / OR;<br>1 - AND;<br>2 - OR.|
|opcommand|object|Object containing the data about the command run by the operation.<br><br>The operation command object is [described in detail below](/fr/manual/api/reference/action/object#action_operation_command).<br><br>Required for remote command operations.|
|opcommand\_grp|array|Host groups to run remote commands on.<br><br>Each object has the following properties:<br>`opcommand_grpid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string)* ID of the operation;<br>`groupid` - *(string)* ID of the host group.<br><br>Required for remote command operations if `opcommand_hst` is not set.|
|opcommand\_hst|array|Host to run remote commands on.<br><br>Each object has the following properties:<br>`opcommand_hstid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string)* ID of the operation;<br>`hostid` - *(string)* ID of the host; if set to 0 the command will be run on the current host.<br><br>Required for remote command operations if `opcommand_grp` is not set.|
|opconditions|array|Operation conditions used for trigger actions.<br><br>The operation condition object is [described in detail below](/fr/manual/api/reference/action/object#action_operation_condition).|
|opgroup|array|Host groups to add hosts to.<br><br>Each object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`groupid` - *(string)* ID of the host group.<br><br>Required for "add to host group" and "remove from host group" operations.|
|opmessage|object|Object containing the data about the message sent by the operation.<br><br>The operation message object is [described in detail below](/fr/manual/api/reference/action/object#action_operation_message).<br><br>Required for message operations.|
|opmessage\_grp|array|User groups to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`usrgrpid` - *(string)* ID of the user group.<br><br>Required for message operations if `opmessage_usr` is not set.|
|opmessage\_usr|array|Users to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`userid` - *(string)* ID of the user.<br><br>Required for message operations if `opmessage_grp` is not set.|
|optemplate|array|Templates to link the hosts to to.<br><br>Each object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`templateid` - *(string)* ID of the template.<br><br>Required for "link to template" and "unlink from template" operations.|
|opinventory|object|Inventory mode set host to.<br><br>Object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`inventory_mode` - *(string)* Inventory mode.<br><br>Required for "Set host inventory mode" operations.|

[comment]: # ({/new-8757165b})

[comment]: # ({new-4aeccf9f})
#### Action operation command

The operation command object contains data about the command that will
be run by the operation.

|Property|Type|Description|
|--------|----|-----------|
|operationid|string|*(readonly)* ID of the operation.|
|command|string|Command to run. Required when type IN (0,1,2,3)|
|**type**<br>(required)|integer|Type of operation command.<br><br>Possible values:<br>0 - custom script;<br>1 - IPMI;<br>2 - SSH;<br>3 - Telnet;<br>4 - global script.|
|authtype|integer|Authentication method used for SSH commands.<br><br>Possible values:<br>0 - password;<br>1 - public key.<br><br>Required for SSH commands.|
|execute\_on|integer|Target on which the custom script operation command will be executed.<br><br>Possible values:<br>0 - Zabbix agent;<br>1 - Zabbix server;<br>2 - Zabbix server (proxy).<br><br>Required for custom script commands.|
|password|string|Password used for SSH commands with password authentication and Telnet commands.|
|port|string|Port number used for SSH and Telnet commands.|
|privatekey|string|Name of the private key file used for SSH commands with public key authentication.<br><br>Required for SSH commands with public key authentication.|
|publickey|string|Name of the public key file used for SSH commands with public key authentication.<br><br>Required for SSH commands with public key authentication.|
|scriptid|string|ID of the script used for global script commands.<br><br>Required for global script commands.|
|username|string|User name used for authentication.<br><br>Required for SSH and Telnet commands.|

[comment]: # ({/new-4aeccf9f})

[comment]: # ({new-6762fd66})
#### Action operation message

The operation message object contains data about the message that will
be sent by the operation.

|Property|Type|Description|
|--------|----|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|default\_msg|integer|Whether to use the default action message text and subject.<br><br>Possible values:<br>0 - *(default)* use the data from the operation;<br>1 - use the data from the action.|
|mediatypeid|string|ID of the media type that will be used to send the message.|
|message|string|Operation message text.|
|subject|string|Operation message subject.|

[comment]: # ({/new-6762fd66})

[comment]: # ({new-d1e6762b})
#### Action operation condition

The action operation condition object defines a condition that must be
met to perform the current operation. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|opconditionid|string|*(readonly)* ID of the action operation condition|
|**conditiontype**<br>(required)|integer|Type of condition.<br><br>Possible values:<br>14 - event acknowledged.|
|**value**<br>(required)|string|Value to compare with.|
|operationid|string|*(readonly)* ID of the operation.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - *(default)* =.|

The following operators and values are supported for each operation
condition type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|14|Event acknowledged|=|Whether the event is acknowledged.<br><br>Possible values:<br>0 - not acknowledged;<br>1 - acknowledged.|

[comment]: # ({/new-d1e6762b})

[comment]: # ({new-f41f59ea})
### Action recovery operation

The action recovery operation object defines an operation that will be
performed when a problem is resolved. Recovery operations are possible
for trigger actions and internal actions. It has the following
properties.

|Property|Type|Description|
|--------|----|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|**operationtype**<br>(required)|integer|Type of operation.<br><br>Possible values for trigger actions:<br>0 - send message;<br>1 - remote command;<br>11 - notify all involved.<br><br>Possible values for internal actions:<br>0 - send message;<br>11 - notify all involved.|
|actionid|string|ID of the action that the recovery operation belongs to.|
|opcommand|object|Object containing the data about the command run by the recovery operation.<br><br>The operation command object is [described in detail above](/fr/manual/api/reference/action/object#action_operation_command).<br><br>Required for remote command operations.|
|opcommand\_grp|array|Host groups to run remote commands on.<br><br>Each object has the following properties:<br>`opcommand_grpid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string)* ID of the operation;<br>`groupid` - *(string)* ID of the host group.<br><br>Required for remote command operations if `opcommand_hst` is not set.|
|opcommand\_hst|array|Host to run remote commands on.<br><br>Each object has the following properties:<br>`opcommand_hstid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string)* ID of the operation;<br>`hostid` - *(string)* ID of the host; if set to 0 the command will be run on the current host.<br><br>Required for remote command operations if `opcommand_grp` is not set.|
|opmessage|object|Object containing the data about the message sent by the recovery operation.<br><br>The operation message object is [described in detail above](/fr/manual/api/reference/action/object#action_operation_message).<br><br>Required for message operations.|
|opmessage\_grp|array|User groups to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`usrgrpid` - *(string)* ID of the user group.<br><br>Required for message operations if `opmessage_usr` is not set.|
|opmessage\_usr|array|Users to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string)* ID of the operation;<br>`userid` - *(string)* ID of the user.<br><br>Required for message operations if `opmessage_grp` is not set.|

[comment]: # ({/new-f41f59ea})

[comment]: # ({new-0d8b3bb1})
### Action acknowledge operation

The action acknowledge operation object defines an operation that will
be performed when a problem is acknowledged. Acknowledge operations are
possible for trigger actions. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|**operationtype**<br>(required)|integer|Type of operation.<br><br>Possible values for trigger actions:<br>0 - send message;<br>1 - remote command;<br>12 - notify all involved.|
|opcommand|object|Object containing the data about the command run by the recovery operation.<br><br>The operation command object is [described in detail above](/fr/manual/api/reference/action/object#action_operation_command).<br><br>Required for remote command operations.|
|opcommand\_grp|array|Host groups to run remote commands on.<br><br>Each object has the following properties:<br>`groupid` - *(string)* ID of the host group.<br><br>Required for remote command operations if `opcommand_hst` is not set.|
|opcommand\_hst|array|Host to run remote commands on.<br><br>Each object has the following properties:<br>`hostid` - *(string)* ID of the host; if set to 0 the command will be run on the current host.<br><br>Required for remote command operations if `opcommand_grp` is not set.|
|opmessage|object|Object containing the data about the message sent by the recovery operation.<br><br>The operation message object is [described in detail above](/fr/manual/api/reference/action/object#action_operation_message).|
|opmessage\_grp|array|User groups to send messages to.<br><br>Each object has the following properties:<br>`usrgrpid` - *(string)* ID of the user group.<br><br>Required only for `send message` operations if `opmessage_usr` is not set.<br>Is ignored for `send acknowledge message` operations.|
|opmessage\_usr|array|Users to send messages to.<br><br>Each object has the following properties:<br>`userid` - *(string)* ID of the user.<br><br>Required only for `send message` operations if `opmessage_grp` is not set.<br>Is ignored for `send acknowledge message` operations.|

[comment]: # ({/new-0d8b3bb1})

[comment]: # ({new-dd860074})
### Action filter

The action filter object defines a set of conditions that must be met to
perform the configured action operations. It has the following
properties.

|Property|Type|Description|
|--------|----|-----------|
|**conditions**<br>(required)|array|Set of filter conditions to use for filtering results.|
|**evaltype**<br>(required)|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.<br><br>Required for custom expression filters.|

#### Action filter condition

The action filter condition object defines a specific condition that
must be checked before running the action operations.

|Property|Type|Description|
|--------|----|-----------|
|conditionid|string|*(readonly)* ID of the action condition.|
|**conditiontype**<br>(required)|integer|Type of condition.<br><br>Possible values for trigger actions:<br>0 - host group;<br>1 - host;<br>2 - trigger;<br>3 - trigger name;<br>4 - trigger severity;<br>6 - time period;<br>13 - host template;<br>15 - application;<br>16 - maintenance status;<br>25 - event tag;<br>26 - event tag value.<br><br>Possible values for discovery actions:<br>7 - host IP;<br>8 - discovered service type;<br>9 - discovered service port;<br>10 - discovery status;<br>11 - uptime or downtime duration;<br>12 - received value;<br>18 - discovery rule;<br>19 - discovery check;<br>20 - proxy;<br>21 - discovery object.<br><br>Possible values for auto-registration actions:<br>20 - proxy;<br>22 - host name;<br>24 - host metadata.<br><br>Possible values for internal actions:<br>0 - host group;<br>1 - host;<br>13 - host template;<br>15 - application;<br>23 - event type.|
|**value**<br>(required)|string|Value to compare with.|
|value2<br>|string|Secondary value to compare with. Requried for trigger actions when condition type is *26*.|
|actionid|string|*(readonly)* ID of the action that the condition belongs to.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - *(default)* =;<br>1 - <>;<br>2 - like;<br>3 - not like;<br>4 - in;<br>5 - >=;<br>6 - <=;<br>7 - not in.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[action.get](get#retrieve_discovery_actions) and
[action.create](create#using_a_custom_expression_filter) method
pages.
:::

The following operators and values are supported for each condition
type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|0|Host group|=, <>|Host group ID.|
|1|Host|=, <>|Host ID.|
|2|Trigger|=, <>|Trigger ID.|
|3|Trigger name|like, not like|Trigger name.|
|4|Trigger severity|=, <>, >=, <=|Trigger severity. Refer to the [trigger "severity" property](/fr/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.|
|5|Trigger value|=|Trigger value. Refer to the [trigger "value" property](/fr/manual/api/reference/trigger/object#trigger) for a list of supported trigger values.|
|6|Time period|in, not in|Time when the event was triggered as a [time period](/fr/manual/appendix/time_period).|
|7|Host IP|=, <>|One or several IP ranges to check separated by commas. Refer to the [network discovery configuration](/fr/manual/discovery/network_discovery/rule) section for more information on supported formats of IP ranges.|
|8|Discovered service type|=, <>|Type of discovered service. The type of service matches the type of the discovery check used to detect the service. Refer to the [discovery check "type" property](/fr/manual/api/reference/dcheck/object#discovery_check) for a list of supported types.|
|9|Discovered service port|=, <>|One or several port ranges separated by commas.|
|10|Discovery status|=|Status of a discovered object.<br><br>Possible values:<br>0 - host or service up;<br>1 - host or service down;<br>2 - host or service discovered;<br>3 - host or service lost.|
|11|Uptime or downtime duration|>=, <=|Time indicating how long has the discovered object been in the current status in seconds.|
|12|Received values|=, <>, >=, <=, like, not like|Value returned when performing a Zabbix agent, SNMPv1, SNMPv2 or SNMPv3 discovery check.|
|13|Host template|=, <>|Linked template ID.|
|15|Application|=, like, not like|Name of the application.|
|16|Maintenance status|in, not in|No value required: using the "in" operator means that the host must be in maintenance, "not in" - not in maintenance.|
|18|Discovery rule|=, <>|ID of the discovery rule.|
|19|Discovery check|=, <>|ID of the discovery check.|
|20|Proxy|=, <>|ID of the proxy.|
|21|Discovery object|=|Type of object that triggered the discovery event.<br><br>Possible values:<br>1 - discovered host;<br>2 - discovered service.|
|22|Host name|like, not like|Host name.|
|23|Event type|=|Specific internal event.<br><br>Possible values:<br>0 - item in "not supported" state;<br>1 - item in "normal" state;<br>2 - LLD rule in "not supported" state;<br>3 - LLD rule in "normal" state;<br>4 - trigger in "unknown" state;<br>5 - trigger in "normal" state.|
|24|Host metadata|like, not like|Metadata of the auto-registered host.|
|25|Tag|=, <>, like, not like|Event tag.|
|26|Tag value|=, <>, like, not like|Event tag value.|

[comment]: # ({/new-dd860074})
