[comment]: # translation:outdated

[comment]: # ({new-3d0c523c})
# action.delete

[comment]: # ({/new-3d0c523c})

[comment]: # ({new-c056f978})
### Description

`object action.delete(array actionIds)`

This method allows to delete actions.

[comment]: # ({/new-c056f978})

[comment]: # ({new-f66aff82})
### Parameters

`(array)` IDs of the actions to delete.

[comment]: # ({/new-f66aff82})

[comment]: # ({new-04c6deae})
### Return values

`(object)` Returns an object containing the IDs of the deleted actions
under the `actionids` property.

[comment]: # ({/new-04c6deae})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-36abb590})
#### Delete multiple actions

Delete two actions.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.delete",
    "params": [
        "17",
        "18"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17",
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-36abb590})

[comment]: # ({new-70602a31})
### Source

CAction::delete() in
*frontends/php/include/classes/api/services/CAction.php*.

[comment]: # ({/new-70602a31})
