[comment]: # translation:outdated

[comment]: # ({new-bef38847})
# > Event object

The following objects are directly related to the `event` API.

[comment]: # ({/new-bef38847})

[comment]: # ({new-355941f0})
### Évènement

::: noteclassic
Events are created by the Zabbix server and cannot be
modified via the API.
:::

The event object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|eventid|string|ID of the event.|
|source|integer|Type of the event.<br><br>Possible values:<br>0 - event created by a trigger;<br>1 - event created by a discovery rule;<br>2 - event created by active agent auto-registration;<br>3 - internal event.|
|object|integer|Type of object that is related to the event.<br><br>Possible values for trigger events:<br>0 - trigger.<br><br>Possible values for discovery events:<br>1 - discovered host;<br>2 - discovered service.<br><br>Possible values for auto-registration events:<br>3 - auto-registered host.<br><br>Possible values for internal events:<br>0 - trigger;<br>4 - item;<br>5 - LLD rule.|
|objectid|string|ID of the related object.|
|acknowledged|integer|Whether the event has been acknowledged.|
|clock|timestamp|Time when the event was created.|
|ns|integer|Nanoseconds when the event was created.|
|name|string|Resolved event name.|
|value|integer|State of the related object.<br><br>Possible values for trigger events:<br>0 - OK;<br>1 - problem.<br><br>Possible values for discovery events:<br>0 - host or service up;<br>1 - host or service down;<br>2 - host or service discovered;<br>3 - host or service lost.<br><br>Possible values for internal events:<br>0 - "normal" state;<br>1 - "unknown" or "not supported" state.<br><br>This parameter is not used for active agent auto-registration events.|
|severity|integer|Event current severity.<br><br>Possible values:<br>0 - not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|r\_eventid|string|Recovery event ID|
|c\_eventid|string|Problem event ID who generated OK event|
|correlationid|string|Correlation ID|
|userid|string|User ID if the event was manually closed.|

[comment]: # ({/new-355941f0})

[comment]: # ({new-b89991b4})
### Event tag

The event tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tag|string|Event tag name.|
|value|string|Event tag value.|

[comment]: # ({/new-b89991b4})

[comment]: # ({new-52ff7df7})
### Media type URLs

Object with media type url have the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|name|string|Media type defined URL name.|
|url|string|Media type defined URL value.|

Results will contain entries only for active media types with enabled
event menu entry. Macro used in properties will be expanded, but if one
of properties contain non expanded macro both properties will be
excluded from results. Supported macros described on
[page](/manual/appendix/macros/supported_by_location).

[comment]: # ({/new-52ff7df7})
