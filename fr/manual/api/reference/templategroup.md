[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# Template group

This class is designed to work with template groups.

Object references:\

-   [Template group](/manual/api/reference/templategroup/object#template_group)

Available methods:\

-   [templategroup.create](/manual/api/reference/templategroup/create) -
    creating new template groups
-   [templategroup.delete](/manual/api/reference/templategroup/delete) -
    deleting template groups
-   [templategroup.get](/manual/api/reference/templategroup/get) - retrieving
    template groups
-   [templategroup.massadd](/manual/api/reference/templategroup/massadd) -
    adding related objects to template groups
-   [templategroup.massremove](/manual/api/reference/templategroup/massremove) -
    removing related objects from template groups
-   [templategroup.massupdate](/manual/api/reference/templategroup/massupdate) -
    replacing or removing related objects from template groups
-   [templategroup.propagate](/manual/api/reference/templategroup/propagate) -
    propagating permissions to template groups' subgroups
-   [templategroup.update](/manual/api/reference/templategroup/update) -
    updating template groups

[comment]: # ({/new-f9e8f670})
