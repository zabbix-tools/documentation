[comment]: # translation:outdated

[comment]: # ({new-be32ecd0})
# Trend

This class is designed to work with trend data.

Object references:\

-   [Trend](/fr/manual/api/reference/trend/object#trend)

Available methods:\

-   [trend.get](/fr/manual/api/reference/trend/get) - retrieving trends

[comment]: # ({/new-be32ecd0})
