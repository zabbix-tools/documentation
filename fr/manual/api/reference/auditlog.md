[comment]: # translation:outdated

[comment]: # ({edcc3ac5-4a04064b})
# Journal d'audit

Cette classe est conçue pour fonctionner avec le journal d'audit.

Références d'objet :\

- [Audit log object](/manual/api/reference/auditlog/object)

Méthodes disponibles :\

- [auditlog.get](/manual/api/reference/auditlog/get) - récupère le journal d'audit.

[comment]: # ({/edcc3ac5-4a04064b})
