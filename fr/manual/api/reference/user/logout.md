[comment]: # translation:outdated

[comment]: # ({new-860084e0})
# user.logout

[comment]: # ({/new-860084e0})

[comment]: # ({new-d6ff52eb})
### Description

`string/object user.logout(array)`

This method allows to log out of the API and invalidates the current
authentication token.

[comment]: # ({/new-d6ff52eb})

[comment]: # ({new-4fa8a419})
### Parameters

`(array)` The method accepts an empty array.

[comment]: # ({/new-4fa8a419})

[comment]: # ({new-66018290})
### Return values

`(boolean)` Returns `true` if the user has been logged out successfully.

[comment]: # ({/new-66018290})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b5326b2b})
#### Logging out

Log out from the API.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.logout",
    "params": [],
    "id": 1,
    "auth": "16a46baf181ef9602e1687f3110abf8a"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-b5326b2b})

[comment]: # ({new-02f79c40})
### See also

-   [user.login](login)

[comment]: # ({/new-02f79c40})

[comment]: # ({new-7fd9f53c})
### Source

CUser::login() in
*frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-7fd9f53c})
