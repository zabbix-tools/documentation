[comment]: # translation:outdated

[comment]: # ({new-2485e0f7})
# user.get

[comment]: # ({/new-2485e0f7})

[comment]: # ({new-36f89e39})
### Description

`integer/array user.get(object parameters)`

The method allows to retrieve users according to the given parameters.

[comment]: # ({/new-36f89e39})

[comment]: # ({new-888512e9})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|mediaids|string/array|Return only users that use the given media.|
|mediatypeids|string/array|Return only users that use the given media types.|
|userids|string/array|Return only users with the given IDs.|
|usrgrpids|string/array|Return only users that belong to the given user groups.|
|getAccess|flag|Adds additional information about user permissions.<br><br>Adds the following properties for each user:<br>`gui_access` - *(integer)* user's frontend authentication method. Refer to the `gui_access` property of the [user group object](/fr/manual/api/reference/usergroup/object#user_group) for a list of possible values.<br>`debug_mode` - *(integer)* indicates whether debug is enabled for the user. Possible values: 0 - debug disabled, 1 - debug enabled.<br>`users_status` - *(integer)* indicates whether the user is disabled. Possible values: 0 - user enabled, 1 - user disabled.|
|selectMedias|query|Return media used by the user in the `medias` property.|
|selectMediatypes|query|Return media types used by the user in the `mediatypes` property.|
|selectUsrgrps|query|Return user groups that the user belongs to in the `usrgrps` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `userid` and `alias`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-888512e9})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7b5c610b})
#### Retrieving users

Retrieve all of the configured users.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "userid": "1",
            "alias": "Admin",
            "name": "Zabbix",
            "surname": "Administrator",
            "url": "",
            "autologin": "1",
            "autologout": "0s",
            "lang": "ru_RU",
            "refresh": "0s",
            "type": "3",
            "theme": "default",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "50"
        },
        {
            "userid": "2",
            "alias": "guest",
            "name": "Default2",
            "surname": "User",
            "url": "",
            "autologin": "0",
            "autologout": "15m",
            "lang": "en_GB",
            "refresh": "30s",
            "type": "1",
            "theme": "default",
            "attempt_failed": "0",
            "attempt_ip": "",
            "attempt_clock": "0",
            "rows_per_page": "50"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-7b5c610b})

[comment]: # ({new-d85f9654})
### See also

-   [Media](/fr/manual/api/reference/user/object#media)
-   [Media type](/fr/manual/api/reference/mediatype/object#media_type)
-   [User group](/fr/manual/api/reference/usergroup/object#user_group)

[comment]: # ({/new-d85f9654})

[comment]: # ({new-8d892781})
### Source

CUser::get() in *frontends/php/include/classes/api/services/CUser.php*.

[comment]: # ({/new-8d892781})

[comment]: # ({new-842f2a42})
### Source

CUser::get() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-842f2a42})
