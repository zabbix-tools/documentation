[comment]: # translation:outdated

[comment]: # ({new-6a1da71f})
# Host

This class is designed to work with hosts.

Object references:\

-   [Host](/fr/manual/api/reference/host/object#host)
-   [Host
    inventory](/fr/manual/api/reference/host/object#host_inventory)

Available methods:\

-   [host.create](/fr/manual/api/reference/host/create) - creating new
    hosts
-   [host.delete](/fr/manual/api/reference/host/delete) - deleting hosts
-   [host.get](/fr/manual/api/reference/host/get) - retrieving hosts
-   [host.massadd](/fr/manual/api/reference/host/massadd) - adding
    related objects to hosts
-   [host.massremove](/fr/manual/api/reference/host/massremove) -
    removing related objects from hosts
-   [host.massupdate](/fr/manual/api/reference/host/massupdate) -
    replacing or removing related objects from hosts
-   [host.update](/fr/manual/api/reference/host/update) - updating hosts

[comment]: # ({/new-6a1da71f})
