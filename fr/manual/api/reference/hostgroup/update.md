[comment]: # translation:outdated

[comment]: # ({new-259f747f})
# hostgroup.update

[comment]: # ({/new-259f747f})

[comment]: # ({new-32bf01b5})
### Description

`object hostgroup.update(object/array hostGroups)`

This method allows to update existing hosts groups.

[comment]: # ({/new-32bf01b5})

[comment]: # ({new-9c20e34d})
### Parameters

`(object/array)` [Host group properties](object#host_group) to be
updated.

The `groupid` property must be defined for each host group, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

[comment]: # ({/new-9c20e34d})

[comment]: # ({new-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a296ad88})
#### Renaming a host group

Rename a host group to "Linux hosts."

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.update",
    "params": {
        "groupid": "7",
        "name": "Linux hosts"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "7"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a296ad88})

[comment]: # ({new-b75d89b4})
### Source

CHostGroup::update() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-b75d89b4})
