[comment]: # translation:outdated

[comment]: # ({new-8703e8df})
# hostgroup.massremove

[comment]: # ({/new-8703e8df})

[comment]: # ({new-bcf1ded4})
### Description

`object hostgroup.massremove(object parameters)`

This method allows to remove related objects from multiple host groups.

[comment]: # ({/new-bcf1ded4})

[comment]: # ({new-6b7b7475})
### Parameters

`(object)` Parameters containing the IDs of the host groups to update
and the objects that should be removed.

|Parameter|Type|Description|
|---------|----|-----------|
|**groupids**<br>(required)|string/array|IDs of the host groups to be updated.|
|hostids|string/array|Hosts to remove from all host groups.|
|templateids|string/array|Templates to remove from all host groups.|

[comment]: # ({/new-6b7b7475})

[comment]: # ({new-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cf36d4e8})
#### Removing hosts from host groups

Remove two hosts from the given host groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massremove",
    "params": {
        "groupids": [
            "5",
            "6"
        ],
        "hostids": [
            "30050",
            "30001"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cf36d4e8})

[comment]: # ({new-3700b99d})
### Source

CHostGroup::massRemove() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-3700b99d})
