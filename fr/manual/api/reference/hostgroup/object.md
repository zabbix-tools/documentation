[comment]: # translation:outdated

[comment]: # ({new-524e7e9d})
# > Host group object

The following objects are directly related to the `hostgroup` API.

[comment]: # ({/new-524e7e9d})

[comment]: # ({new-52ddc167})
### Host group

The host group object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|groupid|string|*(readonly)* ID of the host group.|
|**name**<br>(required)|string|Name of the host group.|
|flags|integer|*(readonly)* Origin of the host group.<br><br>Possible values:<br>0 - a plain host group;<br>4 - a discovered host group.|
|internal|integer|*(readonly)* Whether the group is used internally by the system. An internal group cannot be deleted.<br><br>Possible values:<br>0 - *(default)* not internal;<br>1 - internal.|

[comment]: # ({/new-52ddc167})
