[comment]: # translation:outdated

[comment]: # ({new-3f9b711d})
# hostgroup.massadd

[comment]: # ({/new-3f9b711d})

[comment]: # ({new-30b8c9f4})
### Description

`object hostgroup.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to all
the given host groups.

[comment]: # ({/new-30b8c9f4})

[comment]: # ({new-b8ff6430})
### Parameters

`(object)` Parameters containing the IDs of the host groups to update
and the objects to add to all the host groups.

The method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**groups**<br>(required)|object/array|Host groups to be updated.<br><br>The host groups must have the `groupid` property defined.|
|hosts|object/array|Hosts to add to all host groups.<br><br>The hosts must have the `hostid` property defined.|
|templates|object/array|Templates to add to all host groups.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-b8ff6430})

[comment]: # ({new-736f3b05})
### Return values

`(object)` Returns an object containing the IDs of the updated host
groups under the `groupids` property.

[comment]: # ({/new-736f3b05})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7698103b})
#### Adding hosts to host groups

Add two hosts to host groups with IDs 5 and 6.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.massadd",
    "params": {
        "groups": [
            {
                "groupid": "5"
            },
            {
                "groupid": "6"
            }
        ],
        "hosts": [
            {
                "hostid": "30050"
            },
            {
                "hostid": "30001"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "5",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7698103b})

[comment]: # ({new-e5f0abe1})
### See also

-   [Host](/fr/manual/api/reference/host/object#host)
-   [Template](/fr/manual/api/reference/template/object#template)

[comment]: # ({/new-e5f0abe1})

[comment]: # ({new-19273fb6})
### Source

CHostGroup::massAdd() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-19273fb6})
