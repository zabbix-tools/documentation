[comment]: # translation:outdated

[comment]: # ({new-6a96957a})
# hostgroup.delete

[comment]: # ({/new-6a96957a})

[comment]: # ({new-3c7f9c00})
### Description

`object hostgroup.delete(array hostGroupIds)`

This method allows to delete host groups.

A host group can not be deleted if:

-   it contains hosts that belong to this group only;
-   it is marked as internal;
-   it is used by a host prototype;
-   it is used in a global script;
-   it is used in a correlation condition.

[comment]: # ({/new-3c7f9c00})

[comment]: # ({new-af1f0187})
### Parameters

`(array)` IDs of the host groups to delete.

[comment]: # ({/new-af1f0187})

[comment]: # ({new-288daefc})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
groups under the `groupids` property.

[comment]: # ({/new-288daefc})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a6d8d11e})
#### Deleting multiple host groups

Delete two host groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostgroup.delete",
    "params": [
        "107824",
        "107825"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107824",
            "107825"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a6d8d11e})

[comment]: # ({new-f496d50e})
### Source

CHostGroup::delete() in
*frontends/php/include/classes/api/services/CHostGroup.php*.

[comment]: # ({/new-f496d50e})
