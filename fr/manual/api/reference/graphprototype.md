[comment]: # translation:outdated

[comment]: # ({new-62384434})
# Graph prototype

This class is designed to work with graph prototypes.

Object references:\

-   [Graph
    prototype](/fr/manual/api/reference/graphprototype/object#graph_prototype)

Available methods:\

-   [graphprototype.create](/fr/manual/api/reference/graphprototype/create) -
    creating new graph prototypes
-   [graphprototype.delete](/fr/manual/api/reference/graphprototype/delete) -
    deleting graph prototypes
-   [graphprototype.get](/fr/manual/api/reference/graphprototype/get) -
    retrieving graph prototypes
-   [graphprototype.update](/fr/manual/api/reference/graphprototype/update) -
    updating graph prototypes

[comment]: # ({/new-62384434})
