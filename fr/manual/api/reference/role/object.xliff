<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/api/reference/role/object.md">
    <body>
      <trans-unit id="2135842b" xml:space="preserve">
        <source># &gt; Role object

The following objects are directly related to the `role` API.</source>
      </trans-unit>
      <trans-unit id="1708e020" xml:space="preserve">
        <source>### Role

The role object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|roleid|string|ID of the role.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the role.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|type|integer|User type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;1 - *(default)* User;&lt;br&gt;2 - Admin;&lt;br&gt;3 - Super admin.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|readonly|integer|Whether the role is readonly.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* No;&lt;br&gt;1 - Yes.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|</source>
      </trans-unit>
      <trans-unit id="8a9b3008" xml:space="preserve">
        <source>### Role rules

The role rules object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|ui|array|Array of the [UI element](object#ui_element) objects.|
|ui.default\_access|integer|Whether access to new UI elements is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|services.read.mode|integer|Read-only access to services.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Read-only access to the services, specified by the `services.read.list` or matched by the `services.read.tag` properties;&lt;br&gt;1 - *(default)* Read-only access to all services.|
|services.read.list|array|Array of [Service](object#service) objects.&lt;br&gt;&lt;br&gt;The specified services, including child services, will be granted a read-only access to the user role. Read-only access will not override read-write access to the services.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `services.read.mode` is set to "0"|
|services.read.tag|object|Array of [Service tag](object#service_tag) object.&lt;br&gt;&lt;br&gt;The tag matched services, including child services, will be granted a read-only access to the user role. Read-only access will not override read-write access to the services.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `services.read.mode` is set to "0"|
|services.write.mode|integer|Read-write access to services.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Read-write access to the services, specified by the `services.write.list` or matched by the `services.write.tag` properties;&lt;br&gt;1 - Read-write access to all services.|
|services.write.list|array|Array of [Service](object#service) objects.&lt;br&gt;&lt;br&gt;The specified services, including child services, will be granted a read-write access to the user role. Read-write access will override read-only access to the services.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `services.write.mode` is set to "0"|
|services.write.tag|object|Array of [Service tag](object#service_tag) object.&lt;br&gt;&lt;br&gt;The tag matched services, including child services, will be granted a read-write access to the user role. Read-write access will override read-only access to the services.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `services.write.mode` is set to "0"|
|modules|array|Array of the [module](object#module) objects.|
|modules.default\_access|integer|Whether access to new modules is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|api.access|integer|Whether access to API is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|api.mode|integer|Mode for treating API methods listed in the `api` property.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Deny list;&lt;br&gt;1 - Allow list.|
|api|array|Array of API methods.|
|actions|array|Array of the [action](object#action) objects.|
|actions.default\_access|integer|Whether access to new actions is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|</source>
      </trans-unit>
      <trans-unit id="006cdfb5" xml:space="preserve">
        <source>### UI element

The UI element object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the UI element.&lt;br&gt;&lt;br&gt;Possible values if `type` of [Role object](#role) is set to "User", "Admin", or "Super admin":&lt;br&gt;`monitoring.dashboard` - *Dashboards*;&lt;br&gt;`monitoring.problems` - *Monitoring → Problems*;&lt;br&gt;`monitoring.hosts` - *Monitoring → Hosts*;&lt;br&gt;`monitoring.latest_data` - *Monitoring → Latest data*;&lt;br&gt;`monitoring.maps` - *Monitoring → Maps*;&lt;br&gt;`services.services` - *Services → Services*;&lt;br&gt;`services.sla_report` - *Services → SLA report*;&lt;br&gt;`inventory.overview` - *Inventory → Overview*;&lt;br&gt;`inventory.hosts` - *Inventory → Hosts*;&lt;br&gt;`reports.availability_report` - *Reports → Availability report*;&lt;br&gt;`reports.top_triggers` - *Reports → Triggers top 100*.&lt;br&gt;&lt;br&gt;Possible values if `type` of [Role object](#role) is set to "Admin" or "Super admin":&lt;br&gt;`monitoring.discovery` - *Monitoring → Discovery*;&lt;br&gt;`services.sla` - *Services → SLA*;&lt;br&gt;`reports.scheduled_reports` - *Reports → Scheduled reports*;&lt;br&gt;`reports.notifications` - *Reports → Notifications*;&lt;br&gt;`configuration.template_groups` - *Data collection → Template groups*;&lt;br&gt;`configuration.host_groups` - *Data collection → Host groups*;&lt;br&gt;`configuration.templates` - *Data collection → Templates*;&lt;br&gt;`configuration.hosts` - *Data collection → Hosts*;&lt;br&gt;`configuration.maintenance` - *Data collection → Maintenance*;&lt;br&gt;`configuration.discovery` - *Data collection → Discovery*;&lt;br&gt;`configuration.trigger_actions` - *Alerts → Actions → Trigger actions*;&lt;br&gt;`configuration.service_actions` - *Alerts → Actions → Service actions*;&lt;br&gt;`configuration.discovery_actions` - *Alerts → Actions → Discovery actions*;&lt;br&gt;`configuration.autoregistration_actions` - *Alerts → Actions → Autoregistration actions*;&lt;br&gt;`configuration.internal_actions` - *Alerts → Actions → Internal actions*.&lt;br&gt;&lt;br&gt;Possible values if `type` of [Role object](#role) is set to "Super admin":&lt;br&gt;`reports.system_info` - *Reports → System information*;&lt;br&gt;`reports.audit` - *Reports → Audit log*;&lt;br&gt;`reports.action_log` - *Reports → Action log*;&lt;br&gt;`configuration.event_correlation` - *Data collection → Event correlation*;&lt;br&gt;`administration.media_types` - *Alerts → Media types*;&lt;br&gt;`administration.scripts` - *Alerts → Scripts*;&lt;br&gt;`administration.user_groups` - *Users → User groups*;&lt;br&gt;`administration.user_roles` - *Users → User roles*;&lt;br&gt;`administration.users` - *Users → Users*;&lt;br&gt;`administration.api_tokens` - *Users → API tokens*;&lt;br&gt;`administration.authentication` - *Users → Authentication*;&lt;br&gt;`administration.general` - *Administration → General*;&lt;br&gt;`administration.audit_log` - *Administration → Audit log*;&lt;br&gt;`administration.housekeeping` - *Administration → Housekeeping*;&lt;br&gt;`administration.proxies` - *Administration → Proxies*;&lt;br&gt;`administration.macros` - *Administration → Macros*;&lt;br&gt;`administration.queue` - *Administration → Queue*.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|status|integer|Whether access to the UI element is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|</source>
      </trans-unit>
      <trans-unit id="ebf4bb3a" xml:space="preserve">
        <source>### Service

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|serviceid|string|ID of the Service.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="300e6d5b" xml:space="preserve">
        <source>### Service tag

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Tag name.&lt;br&gt;&lt;br&gt;If empty string is specified, the service tag will not be used for service matching.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|Tag value.&lt;br&gt;&lt;br&gt;If no value or empty string is specified, only the tag name will be used for service matching.|</source>
      </trans-unit>
      <trans-unit id="f70e75e9" xml:space="preserve">
        <source>### Module

The module object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|moduleid|string|ID of the module.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|status|integer|Whether access to the module is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|</source>
      </trans-unit>
      <trans-unit id="957a00bb" xml:space="preserve">
        <source>### Action

The action object has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|name|string|Name of the action.&lt;br&gt;&lt;br&gt;Possible values if `type` of [Role object](#role) is set to "User", "Admin", or "Super admin:&lt;br&gt;`edit_dashboards` - Create and edit dashboards;&lt;br&gt;`edit_maps` - Create and edit maps;&lt;br&gt;`add_problem_comments` - Add problem comments;&lt;br&gt;`change_severity` - Change problem severity;&lt;br&gt;`acknowledge_problems` - Acknowledge problems;&lt;br&gt;`suppress_problems` - Suppress problems;&lt;br&gt;`close_problems` - Close problems;&lt;br&gt;`execute_scripts` - Execute scripts;&lt;br&gt;`manage_api_tokens` - Manage API tokens.&lt;br&gt;&lt;br&gt;Possible values if `type` of [Role object](#role) is set to "Admin" or "Super admin":&lt;br&gt;`edit_maintenance` - Create and edit maintenances;&lt;br&gt;`manage_scheduled_reports` - Manage scheduled reports,&lt;br&gt;`manage_sla` - Manage SLA.&lt;br&gt;&lt;br&gt;Possible values if `type` of [Role object](#role) is set to "User" or "Admin":&lt;br&gt;`invoke_execute_now` - allows to execute item checks for users that have only read permissions on host.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|status|integer|Whether access to perform the action is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
