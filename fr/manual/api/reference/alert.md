[comment]: # translation:outdated

[comment]: # ({8a5182a2-a68f24c8})
# Alert

Cette classe est conçue pour fonctionner avec les alertes.

Réferences d'objets :\

-   [Alert](/manual/api/reference/alert/object#alert)

Méthodes disponibles :\

-   [alert.get](/manual/api/reference/alert/get) - récupérer les alertes

[comment]: # ({/8a5182a2-a68f24c8})
