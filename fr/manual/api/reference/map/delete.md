[comment]: # translation:outdated

[comment]: # ({new-bc1a928b})
# map.delete

[comment]: # ({/new-bc1a928b})

[comment]: # ({new-24398e69})
### Description

`object map.delete(array mapIds)`

This method allows to delete maps.

[comment]: # ({/new-24398e69})

[comment]: # ({new-57a8b77a})
### Parameters

`(array)` IDs of the maps to delete.

[comment]: # ({/new-57a8b77a})

[comment]: # ({new-f04e039f})
### Return values

`(object)` Returns an object containing the IDs of the deleted maps
under the `sysmapids` property.

[comment]: # ({/new-f04e039f})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-87539545})
#### Delete multiple maps

Delete two maps.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "map.delete",
    "params": [
        "12",
        "34"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "sysmapids": [
            "12",
            "34"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-87539545})

[comment]: # ({new-4880bc2b})
### Source

CMap::delete() in *frontends/php/include/classes/api/services/CMap.php*.

[comment]: # ({/new-4880bc2b})
