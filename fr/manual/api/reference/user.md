[comment]: # translation:outdated

[comment]: # ({new-392125b2})
# User

This class is designed to work with users.

Object references:\

-   [User](/fr/manual/api/reference/user/object#user)

Available methods:\

-   [user.create](/fr/manual/api/reference/user/create) - creating new
    users
-   [user.delete](/fr/manual/api/reference/user/delete) - deleting users
-   [user.get](/fr/manual/api/reference/user/get) - retrieving users
-   [user.login](/fr/manual/api/reference/user/login) - logging in to
    the API
-   [user.logout](/fr/manual/api/reference/user/logout) - logging out of
    the API
-   [user.update](/fr/manual/api/reference/user/update) - updating users

[comment]: # ({/new-392125b2})
