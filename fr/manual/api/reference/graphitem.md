[comment]: # translation:outdated

[comment]: # ({new-ba6b9d7f})
# Graph item

This class is designed to work with hosts.

Object references:\

-   [Graph item](/fr/manual/api/reference/graphitem/object#graph_item)

Available methods:\

-   [graphitem.get](/fr/manual/api/reference/graphitem/get) - retrieving
    graph items

[comment]: # ({/new-ba6b9d7f})
