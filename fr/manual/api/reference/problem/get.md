[comment]: # translation:outdated

[comment]: # ({new-48cd3524})
# problem.get

[comment]: # ({/new-48cd3524})

[comment]: # ({new-a18b2713})
### Description

`integer/array problem.get(object parameters)`

The method allows to retrieve problems according to the given
parameters.

[comment]: # ({/new-a18b2713})

[comment]: # ({new-98f39c6a})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|eventids|string/array|Return only problems with the given IDs.|
|groupids|string/array|Return only problems created by objects that belong to the given host groups.|
|hostids|string/array|Return only problems created by objects that belong to the given hosts.|
|objectids|string/array|Return only problems created by the given objects.|
|applicationids|string/array|Return only problems created by objects that belong to the given applications. Applies only if object is trigger or item.|
|source|integer|Return only problems with the given type.<br><br>Refer to the [problem event object page](object#problem) for a list of supported event types.<br><br>Default: 0 - problem created by a trigger.|
|object|integer|Return only problems created by objects of the given type.<br><br>Refer to the [problem event object page](object#problem) for a list of supported object types.<br><br>Default: 0 - trigger.|
|acknowledged|boolean|`true` - return acknowledged problems only;<br>`false` - unacknowledged only.|
|severities|integer/array|Return only problems with given event severities. Applies only if object is trigger.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or.|
|tags|array of objects|Return only problems with given tags. Exact match by tag and case-insensitive search by value and operator.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all problems.<br><br>Possible operator types:<br>0 - (default) Like;<br>1 - Equal.|
|recent|string|true - return PROBLEM and recently RESOLVED problems (depends on Display OK triggers for N seconds)<br>Default: false - UNRESOLVED problems only|
|eventid\_from|string|Return only problems with IDs greater or equal to the given ID.|
|eventid\_till|string|Return only problems with IDs less or equal to the given ID.|
|time\_from|timestamp|Return only problems that have been created after or at the given time.|
|time\_till|timestamp|Return only problems that have been created before or at the given time.|
|selectAcknowledges|query|Return problem's updates in the `acknowledges` property. Problem updates are sorted in reverse chronological order.<br><br>The problem update object has the following properties:<br>`acknowledgeid` - `(string)` update's ID;<br>`userid` - `(string)` ID of the user that updated the event;<br>`eventid` - `(string)` ID of the updated event;<br>`clock` - `(timestamp)` time when the event was updated;<br>`message` - `(string)` text of the message;<br>`action` - `(integer)` update action that was performed see [event.acknowledge](/fr/manual/api/reference/event/acknowledge);<br>`old_severity` - `(integer)` event severity before this update action;<br>`new_severity` - `(integer)` event severity after this update action;<br><br>Supports `count`.|
|selectTags|query|Return problem's tags. Output format: `[{"tag": "<tag>", "value": "<value>"}, ...]`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `eventid`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-98f39c6a})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4d83529d})
#### Retrieving trigger problem events

Retrieve recent events from trigger "15112."

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": "extend",
        "selectTags": "extend",
        "objectids": "15112",
        "recent": "true",
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "auth": "67f45d3eb1173338e1b1647c4bdc1916",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1245463",
            "source": "0",
            "object": "0",
            "objectid": "15112",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "1",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "acknowledges": [
                {
                    "acknowledgeid": "14443",
                    "userid": "1",
                    "eventid": "1245463",
                    "clock": "1472457281",
                    "message": "problem solved",
                    "action": "6",
                    "old_severity": "0",
                    "new_severity": "0"
                }
            ],
            "tags": [
                {
                    "tag": "test tag",
                    "value": "test value"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-4d83529d})

[comment]: # ({new-by})
#### Retrieving problems acknowledged by specified user

Retrieving problems acknowledged by user with ID=10

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "problem.get",
    "params": {
        "output": "extend",
        "selectAcknowledges": ["userid", "action"],
        "filter": {
            "action": 2,
            "action_userid": 10
        },
        "sortfield": ["eventid"],
        "sortorder": "DESC"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "eventid": "1248566",
            "source": "0",
            "object": "0",
            "objectid": "15142",
            "clock": "1472457242",
            "ns": "209442442",
            "r_eventid": "1245468",
            "r_clock": "1472457285",
            "r_ns": "125644870",
            "correlationid": "0",
            "userid": "10",
            "name": "Zabbix agent on localhost is unreachable for 5 minutes",
            "acknowledged": "1",
            "severity": "3",
            "cause_eventid": "0",
            "opdata": "",
            "acknowledges": [
                {
                    "userid": "10",
                    "action": "2"
                }
            ],
            "suppressed": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-by})

[comment]: # ({new-c6eb3ba0})
### See also

-   [Alert](/fr/manual/api/reference/alert/object)
-   [Item](/fr/manual/api/reference/item/object)
-   [Host](/fr/manual/api/reference/host/object)
-   [LLD rule](/fr/manual/api/reference/discoveryrule/object#lld_rule)
-   [Trigger](/fr/manual/api/reference/trigger/object)

[comment]: # ({/new-c6eb3ba0})

[comment]: # ({new-15f9267d})
### Source

CEvent::get() in
*frontends/php/include/classes/api/services/CProblem.php*.

[comment]: # ({/new-15f9267d})
