[comment]: # translation:outdated

[comment]: # ({new-e35cf787})
# Discovered host

This class is designed to work with discovered hosts.

Object references:\

-   [Discovered
    host](/fr/manual/api/reference/dhost/object#discovered_host)

Available methods:\

-   [dhost.get](/fr/manual/api/reference/dhost/get) - retrieve
    discovered hosts

[comment]: # ({/new-e35cf787})
