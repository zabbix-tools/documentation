[comment]: # translation:outdated

[comment]: # ({new-8a5e44d6})
# housekeeping.get

[comment]: # ({/new-8a5e44d6})

[comment]: # ({new-695e57b4})
### Description

`object housekeeping.get(object parameters)`

The method allows to retrieve housekeeping object according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-695e57b4})

[comment]: # ({new-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/new-24b20f8d})

[comment]: # ({new-490df090})
### Return values

`(object)` Returns housekeeping object.

[comment]: # ({/new-490df090})

[comment]: # ({new-1d4e9444})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "housekeeping.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hk_events_mode": "1",
        "hk_events_trigger": "365d",
        "hk_events_service": "1d",
        "hk_events_internal": "1d",
        "hk_events_discovery": "1d",
        "hk_events_autoreg": "1d",
        "hk_services_mode": "1",
        "hk_services": "365d",
        "hk_audit_mode": "1",
        "hk_audit": "365d",
        "hk_sessions_mode": "1",
        "hk_sessions": "365d",
        "hk_history_mode": "1",
        "hk_history_global": "0",
        "hk_history": "90d",
        "hk_trends_mode": "1",
        "hk_trends_global": "0",
        "hk_trends": "365d",
        "db_extension": "",
        "compression_status": "0",
        "compress_older": "7d"
    },
    "id": 1
}
```

[comment]: # ({/new-1d4e9444})

[comment]: # ({new-523212cf})
### Source

CHousekeeping ::get() in
*ui/include/classes/api/services/CHousekeeping.php*.

[comment]: # ({/new-523212cf})
