<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manual/api/reference/maintenance/object.md">
    <body>
      <trans-unit id="c9539601" xml:space="preserve">
        <source># &gt; Maintenance object

The following objects are directly related to the `maintenance` API.</source>
      </trans-unit>
      <trans-unit id="6c1a70bc" xml:space="preserve">
        <source>### Maintenance

The maintenance object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|maintenanceid|string|ID of the maintenance.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the maintenance.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|active\_since|timestamp|Time when the maintenance becomes active.&lt;br&gt;&lt;br&gt;The given value will be rounded down to minutes.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|active\_till|timestamp|Time when the maintenance stops being active.&lt;br&gt;&lt;br&gt;The given value will be rounded down to minutes.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|description|string|Description of the maintenance.|
|maintenance\_type|integer|Type of maintenance.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* with data collection;&lt;br&gt;1 - without data collection.|
|tags\_evaltype|integer|Problem tag evaluation method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* And/Or;&lt;br&gt;2 - Or.|</source>
      </trans-unit>
      <trans-unit id="506f52aa" xml:space="preserve">
        <source>### Time period

The time period object is used to define periods when the maintenance must come into effect.
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|period|integer|Duration of the maintenance period in seconds.&lt;br&gt;&lt;br&gt;The given value will be rounded down to minutes.&lt;br&gt;&lt;br&gt;Default: 3600.|
|timeperiod\_type|integer|Type of time period.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* one time only;&lt;br&gt;2 - daily;&lt;br&gt;3 - weekly;&lt;br&gt;4 - monthly.|
|start\_date|timestamp|Date when the maintenance period must come into effect.&lt;br&gt;The given value will be rounded down to minutes.&lt;br&gt;&lt;br&gt;Default: current date.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `timeperiod_type` is set to "one time only"|
|start\_time|integer|Time of day when the maintenance starts in seconds.&lt;br&gt;The given value will be rounded down to minutes.&lt;br&gt;&lt;br&gt;Default: 0.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `timeperiod_type` is set to "daily", "weekly", or "monthly"|
|every|integer|For daily and weekly periods `every` defines the day or week intervals at which the maintenance must come into effect.&lt;br&gt;Default value if `timeperiod_type` is set to "daily" or "weekly": 1.&lt;br&gt;&lt;br&gt;For monthly periods when `day` is set, the `every` property defines the day of the month when the maintenance must come into effect.&lt;br&gt;Default value if `timeperiod_type` is set to "monthly" and `day` is set: 1.&lt;br&gt;&lt;br&gt;For monthly periods when `dayofweek` is set, the `every` property defines the week of the month when the maintenance must come into effect.&lt;br&gt;Possible values if `timeperiod_type` is set to "monthly" and `dayofweek` is set:&lt;br&gt;1 - *(default)* first week;&lt;br&gt;2 - second week;&lt;br&gt;3 - third week;&lt;br&gt;4 - fourth week;&lt;br&gt;5 - last week.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `timeperiod_type` is set to "daily", "weekly", or "monthly"|
|dayofweek|integer|Days of the week when the maintenance must come into effect.&lt;br&gt;&lt;br&gt;Days are stored in binary form with each bit representing the corresponding day. For example, 4 equals 100 in binary and means, that maintenance will be enabled on Wednesday.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `timeperiod_type` is set to "weekly", or if `timeperiod_type` is set to "monthly" and `day` is not set&lt;br&gt;- *supported* if `timeperiod_type` is set to "monthly"|
|day|integer|Day of the month when the maintenance must come into effect.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `timeperiod_type` is set to "monthly" and `dayofweek` is not set&lt;br&gt;- *supported* if `timeperiod_type` is set to "monthly"|
|month|integer|Months when the maintenance must come into effect.&lt;br&gt;&lt;br&gt;Months are stored in binary form with each bit representing the corresponding month. For example, 5 equals 101 in binary and means, that maintenance will be enabled in January and March.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `timeperiod_type` is set to "monthly"|</source>
      </trans-unit>
      <trans-unit id="2913a3ae" xml:space="preserve">
        <source>### Problem tag

The problem tag object is used to define which problems must be suppressed when the maintenance comes into effect.
Tags can only be specified if `maintenance_type` of [Maintenance object](#maintenance) is set to "with data collection".
It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Problem tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|operator|integer|Condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Equals;&lt;br&gt;2 - *(default)* Contains.|
|value|string|Problem tag value.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
