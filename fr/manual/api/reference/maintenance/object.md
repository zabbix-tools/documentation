[comment]: # translation:outdated

[comment]: # ({new-c9539601})
# > Maintenance object

The following objects are directly related to the `maintenance` API.

[comment]: # ({/new-c9539601})

[comment]: # ({new-6c1a70bc})
### Maintenance

The maintenance object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|maintenanceid|string|*(readonly)* ID of the maintenance.|
|**name**<br>(required)|string|Name of the maintenance.|
|**active\_since**<br>(required)|timestamp|Time when the maintenance becomes active.|
|**active\_till**<br>(required)|timestamp|Time when the maintenance stops being active.|
|description|string|Description of the maintenance.|
|maintenance\_type|integer|Type of maintenance.<br><br>Possible values:<br>0 - *(default)* with data collection;<br>1 - without data collection.|

[comment]: # ({/new-6c1a70bc})

[comment]: # ({new-506f52aa})
### Time period

The time period object is used to define periods when the maintenance
must come into effect. It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|timeperiodid|string|*(readonly)* ID of the maintenance.|
|day|integer|Day of the month when the maintenance must come into effect.<br><br>Required only for monthly time periods.|
|dayofweek|integer|Days of the week when the maintenance must come into effect.<br><br>Days are stored in binary form with each bit representing the corresponding day. For example, 4 equals 100 in binary and means, that maintenance will be enabled on Wednesday.<br><br>Used for weekly and monthly time periods. Required only for weekly time periods.|
|every|integer|For daily and weekly periods `every` defines day or week intervals at which the maintenance must come into effect.<br><br>For monthly periods `every` defines the week of the month when the maintenance must come into effect.<br>Possible values:<br>1 - first week;<br>2 - second week;<br>3 - third week;<br>4 - fourth week;<br>5 - last week.|
|month|integer|Months when the maintenance must come into effect.<br><br>Months are stored in binary form with each bit representing the corresponding month. For example, 5 equals 101 in binary and means, that maintenance will be enabled in January and March.<br><br>Required only for monthly time periods.|
|period|integer|Duration of the maintenance period in seconds.<br><br>Default: 3600.|
|start\_date|timestamp|Date when the maintenance period must come into effect.<br><br>Required only for one time periods.<br><br>Default: current date.|
|start\_time|integer|Time of day when the maintenance starts in seconds.<br><br>Required for daily, weekly and monthly periods.|
|timeperiod\_type|integer|Type of time period.<br><br>Possible values:<br>0 - *(default)* one time only;<br>2 - daily;<br>3 - weekly;<br>4 - monthly.|

[comment]: # ({/new-506f52aa})

[comment]: # ({new-2913a3ae})
### Problem tag

The problem tag object is used to define which problems must be
suppressed when the maintenance comes into effect. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Problem tag name.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - Equals;<br>2 - *(default)* Contains.|
|value|string|Problem tag value.|

[comment]: # ({/new-2913a3ae})
