[comment]: # translation:outdated

[comment]: # ({new-34f25b55})
# apiinfo.version

[comment]: # ({/new-34f25b55})

[comment]: # ({60e0a91f-bc32199a})
### Description

`string apiinfo.version(array)`

Cette méthode permet de récupérer la version de l'API Zabbix.

::: noteimportant
Cette méthode n'est disponible que pour les utilisateurs non authentifiés et doit être appelée sans le paramètre "auth" dans la requête JSON-RPC.
:::

[comment]: # ({/60e0a91f-bc32199a})

[comment]: # ({b326596c-4fa8a419})
### Paramètres

`(array)` La méthode accepte un tableau vide.

[comment]: # ({/b326596c-4fa8a419})

[comment]: # ({e8bd16e8-53521d11})
### Valeurs de retour

`(string)` Renvoie la version de l\'API Zabbix.

::: notetip
A partir de Zabbix 2.0.4, la version de l\'API correspond à la version de Zabbix.
:::

[comment]: # ({/e8bd16e8-53521d11})

[comment]: # ({new-b41637d2})
### Exemples

[comment]: # ({/new-b41637d2})

[comment]: # ({0e081ffa-6ba4bdd0})
#### Récupération de la version de l'API

Récupère la version de l'API Zabbix.

Requête :

``` {.java}
{
 · "jsonrpc": "2.0",
 · "method": "apiinfo.version",
 · "params": [],
 · "id": 1
}
```

Réponse :

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "4.0.0",
    "id": 1
}
```

[comment]: # ({/0e081ffa-6ba4bdd0})

[comment]: # ({25499b5e-25499b5e})
### Source

CAPIInfo::version() in *ui/include/classes/api/services/CAPIInfo.php*.

[comment]: # ({/25499b5e-25499b5e})
