[comment]: # translation:outdated

[comment]: # ({new-86ab9f61})
# > Trigger prototype object

The following objects are directly related to the `triggerprototype`
API.

[comment]: # ({/new-86ab9f61})

[comment]: # ({new-bdd02d4b})
### Trigger

The trigger prototype object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|triggerid|string|*(readonly)* ID of the trigger prototype.|
|**description**<br>(required)|string|Name of the trigger prototype.|
|**expression**<br>(required)|string|Reduced trigger expression.|
|comments|string|Additional comments to the trigger prototype.|
|priority|integer|Severity of the trigger prototype.<br><br>Possible values:<br>0 - *(default)* not classified;<br>1 - information;<br>2 - warning;<br>3 - average;<br>4 - high;<br>5 - disaster.|
|status|integer|Whether the trigger prototype is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|templateid|string|*(readonly)* ID of the parent template trigger prototype.|
|type|integer|Whether the trigger prototype can generate multiple problem events.<br><br>Possible values:<br>0 - *(default)* do not generate multiple events;<br>1 - generate multiple events.|
|url|string|URL associated with the trigger prototype.|
|recovery\_mode|integer|OK event generation mode.<br><br>Possible values are:<br>0 - *(default)* Expression;<br>1 - Recovery expression;<br>2 - None.|
|recovery\_expression|string|Reduced trigger recovery expression.|
|correlation\_mode|integer|OK event closes.<br><br>Possible values are:<br>0 - *(default)* All problems;<br>1 - All problems if tag values match.|
|correlation\_tag|string|Tag for matching.|
|manual\_close|integer|Allow manual close.<br><br>Possible values are:<br>0 - *(default)* No;<br>1 - Yes.|

[comment]: # ({/new-bdd02d4b})

[comment]: # ({new-efc78bd6})
### Trigger prototype tag

The trigger prototype tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Trigger prototype tag name.|
|value|string|Trigger prototype tag value.|

[comment]: # ({/new-efc78bd6})
