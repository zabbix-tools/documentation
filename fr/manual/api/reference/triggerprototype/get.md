[comment]: # translation:outdated

[comment]: # ({new-c7072ca6})
# triggerprototype.get

[comment]: # ({/new-c7072ca6})

[comment]: # ({new-12701833})
### Description

`integer/array triggerprototype.get(object parameters)`

The method allows to retrieve trigger prototypes according to the given
parameters.

[comment]: # ({/new-12701833})

[comment]: # ({new-14171778})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|active|flag|Return only enabled trigger prototypes that belong to monitored hosts.|
|applicationids|string/array|Return only trigger prototypes that contain items from the given applications.|
|discoveryids|string/array|Return only trigger prototypes that belong to the given LLD rules.|
|functions|string/array|Return only triggers that use the given functions.<br><br>Refer to the [supported trigger functions](/fr/manual/appendix/triggers/functions) page for a list of supported functions.|
|group|string|Return only trigger prototypes that belong to hosts from the host groups with the given name.|
|groupids|string/array|Return only trigger prototypes that belong to hosts from the given host groups.|
|host|string|Return only trigger prototypes that belong to hosts with the given name.|
|hostids|string/array|Return only trigger prototypes that belong to the given hosts.|
|inherited|boolean|If set to `true` return only trigger prototypes inherited from a template.|
|maintenance|boolean|If set to `true` return only enabled trigger prototypes that belong to hosts in maintenance.|
|min\_severity|integer|Return only trigger prototypes with severity greater or equal than the given severity.|
|monitored|flag|Return only enabled trigger prototypes that belong to monitored hosts and contain only enabled items.|
|templated|boolean|If set to `true` return only trigger prototypes that belong to templates.|
|templateids|string/array|Return only trigger prototypes that belong to the given templates.|
|triggerids|string/array|Return only trigger prototypes with the given IDs.|
|expandExpression|flag|Expand functions and macros in the trigger expression.|
|selectDiscoveryRule|query|Return the LLD rule that the trigger prototype belongs to.|
|selectFunctions|query|Return functions used in the trigger prototype in the `functions` property.<br><br>The function objects represents the functions used in the trigger expression and has the following properties:<br>`functionid` - *(string)* ID of the function;<br>`itemid` - *(string)* ID of the item used in the function;<br>`function` - *(string)* name of the function;<br>`parameter` - *(string)* parameter passed to the function.|
|selectGroups|query|Return the host groups that the trigger prototype belongs to in the `groups` property.|
|selectHosts|query|Return the hosts that the trigger prototype belongs to in the `hosts` property.|
|selectItems|query|Return items and item prototypes used the trigger prototype in the `items` property.|
|selectDependencies|query|Return trigger prototypes and triggers that the trigger prototype depends on in the `dependencies` property.|
|selectTags|query|Return the trigger prototype tags in `tags` property.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the trigger prototype belongs to;<br>`hostid` - ID of the host that the trigger prototype belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectHosts` - results will be sorted by `host`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `triggerid`, `description`, `status` and `priority`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-14171778})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-81de28c1})
#### Retrieve trigger prototypes from an LLD rule

Retrieve all trigger prototypes and their functions from an LLD rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": "extend",
        "selectFunctions": "extend",
        "discoveryids": "22450"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "functions": [
                {
                    "functionid": "12598",
                    "itemid": "22454",
                    "function": "last",
                    "parameter": "0"
                }
            ],
            "triggerid": "13272",
            "expression": "{12598}<20",
            "description": "Free inodes is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "priority": "2",
            "comments": "",
            "templateid": "0",
            "type": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0"
        },
        {
            "functions": [
                {
                    "functionid": "13500",
                    "itemid": "22686",
                    "function": "last",
                    "parameter": "0"
                }
            ],
            "triggerid": "13266",
            "expression": "{13500}<201",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "url": "",
            "status": "0",
            "priority": "2",
            "comments": "",
            "templateid": "0",
            "type": "0",
            "flags": "2",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-81de28c1})

[comment]: # ({new-2cb80f2d})
#### Retrieving a specific trigger prototype with tags

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ]
        "selectTags": "extend",
        "triggerids": [
            "17373"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17373",
            "description": "Free disk space is less than 20% on volume {#FSNAME}",
            "tags": [
                {
                    "tag": "volume",
                    "value": "{#FSNAME}"
                },
                {
                    "tag": "type",
                    "value": "{#FSTYPE}"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-2cb80f2d})

[comment]: # ({new-222d46a0})
### See also

-   [Discovery
    rule](/fr/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Item](/fr/manual/api/reference/item/object#item)
-   [Host](/fr/manual/api/reference/host/object#host)
-   [Host group](/fr/manual/api/reference/hostgroup/object#host_group)

[comment]: # ({/new-222d46a0})

[comment]: # ({new-1136c1c7})
### Source

CTriggerPrototype::get() in
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/new-1136c1c7})
