[comment]: # translation:outdated

[comment]: # ({new-be70f0cc})
# triggerprototype.update

[comment]: # ({/new-be70f0cc})

[comment]: # ({new-842b5955})
### Description

`object triggerprototype.update(object/array triggerPrototypes)`

This method allows to update existing trigger prototypes.

[comment]: # ({/new-842b5955})

[comment]: # ({new-827e4920})
### Parameters

`(object/array)` [Trigger prototype
properties](object#trigger_prototype) to be updated.

The `triggerid` property must be defined for each trigger prototype, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard trigger prototype
properties](object#trigger_prototype) the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dependencies|array|Triggers and trigger prototypes that the trigger prototype is dependent on.<br><br>The triggers must have the `triggerid` property defined.|
|tags|array|Trigger prototype tags.|

::: noteimportant
The trigger expression has to be given in its
expanded form and must contain at least one item prototype.
:::

[comment]: # ({/new-827e4920})

[comment]: # ({new-200d7b9d})
### Return values

`(object)` Returns an object containing the IDs of the updated trigger
prototypes under the `triggerids` property.

[comment]: # ({/new-200d7b9d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-bcca41dc})
#### Enabling a trigger prototype

Enable a trigger prototype, that is, set its status to 0.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "13938",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bcca41dc})

[comment]: # ({new-724af3ce})
#### Replacing trigger prototype tags

Replace tags for one trigger prototype.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.update",
    "params": {
        "triggerid": "17373",
        "tags": [
            {
                "tag": "volume",
                "value": "{#FSNAME}"
            },
            {
                "tag": "type",
                "value": "{#FSTYPE}"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17373"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-724af3ce})

[comment]: # ({new-585ae96a})
### Source

CTriggerPrototype::update() in
*frontends/php/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/new-585ae96a})
