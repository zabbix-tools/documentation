[comment]: # translation:outdated

[comment]: # ({new-ce3b3e73})
# host.create

[comment]: # ({/new-ce3b3e73})

[comment]: # ({new-2e20b613})
### Description

`object host.create(object/array hosts)`

This method allows to create new hosts.

[comment]: # ({/new-2e20b613})

[comment]: # ({new-48df9d2f})
### Parameters

`(object/array)` Hosts to create.

Additionally to the [standard host properties](object#host), the method
accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**groups**<br>(required)|object/array|Host groups to add the host to.<br><br>The host groups must have the `groupid` property defined.|
|**interfaces**<br>(required)|object/array|Interfaces to be created for the host.|
|templates|object/array|Templates to be linked to the host.<br><br>The templates must have the `templateid` property defined.|
|macros|object/array|User macros to be created for the host.|
|inventory|object|Host inventory properties.|

[comment]: # ({/new-48df9d2f})

[comment]: # ({new-de799b9d})
### Return values

`(object)` Returns an object containing the IDs of the created hosts
under the `hostids` property. The order of the returned IDs matches the
order of the passed hosts.

[comment]: # ({/new-de799b9d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-26320041})
#### Creating a host

Create a host called "Linux server" with an IP interface, add it to a
group, link a template to it and set the MAC addresses in the host
inventory.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ],
        "groups": [
            {
                "groupid": "50"
            }
        ],
        "templates": [
            {
                "templateid": "20045"
            }
        ],
        "inventory_mode": 0,
        "inventory": {
            "macaddress_a": "01234",
            "macaddress_b": "56768"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "107819"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-26320041})

[comment]: # ({new-9572ca08})
### See also

-   [Host group](/fr/manual/api/reference/hostgroup/object#host_group)
-   [Template](/fr/manual/api/reference/template/object#template)
-   [User
    macro](/fr/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/fr/manual/api/reference/hostinterface/object#host_interface)
-   [Host inventory](object#host_inventory)

[comment]: # ({/new-9572ca08})

[comment]: # ({new-4c198338})
#### Creating a host with PSK encryption

Create a host called "PSK host" with PSK encryption configured.
Note that the host has to be [pre-configured to use PSK](/manual/encryption/using_pre_shared_keys#configuring-psk-for-server-agent-communication-example).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "PSK host",
        "interfaces": [
            {
                "type": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050",
                "useip": 1,
                "main": 1
            }
        ],
        "groups": [
            {
                "groupid": "2"
            }
        ],
        "tls_accept": 2,
        "tls_connect": 2,
        "tls_psk_identity": "PSK 001",
        "tls_psk": "1f87b595725ac58dd977beef14b97461a7c1045b9a1c963065002c5473194952"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10590"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4c198338})

[comment]: # ({new-203ca033})
### Source

CHost::create() in
*frontends/php/include/classes/api/services/CHost.php*.

[comment]: # ({/new-203ca033})

[comment]: # ({new-f7e47995})
### Source

CHost::create() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-f7e47995})
