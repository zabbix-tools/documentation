[comment]: # translation:outdated

[comment]: # ({new-bf4e6957})
# User macro

This class is designed to work with host and global macros.

Object references:\

-   [Global
    macro](/fr/manual/api/reference/usermacro/object#global_macro)
-   [Host macro](/fr/manual/api/reference/usermacro/object#host_macro)

Available methods:\

-   [usermacro.create](/fr/manual/api/reference/usermacro/create) -
    creating new host macros
-   [usermacro.createglobal](/fr/manual/api/reference/usermacro/createglobal) -
    creating new global macros
-   [usermacro.delete](/fr/manual/api/reference/usermacro/delete) -
    deleting host macros
-   [usermacro.deleteglobal](/fr/manual/api/reference/usermacro/deleteglobal) -
    deleting global macros
-   [usermacro.get](/fr/manual/api/reference/usermacro/get) - retrieving
    host and global macros
-   [usermacro.update](/fr/manual/api/reference/usermacro/update) -
    updating host macros
-   [usermacro.updateglobal](/fr/manual/api/reference/usermacro/updateglobal) -
    updating global macros

[comment]: # ({/new-bf4e6957})
