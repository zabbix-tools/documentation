[comment]: # translation:outdated

[comment]: # ({new-0a247a7d})
# graphprototype.delete

[comment]: # ({/new-0a247a7d})

[comment]: # ({new-8256b270})
### Description

`object graphprototype.delete(array graphPrototypeIds)`

This method allows to delete graph prototypes.

[comment]: # ({/new-8256b270})

[comment]: # ({new-3e4a53cc})
### Parameters

`(array)` IDs of the graph prototypes to delete.

[comment]: # ({/new-3e4a53cc})

[comment]: # ({new-82123f70})
### Return values

`(object)` Returns an object containing the IDs of the deleted graph
prototypes under the `graphids` property.

[comment]: # ({/new-82123f70})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d24b4f06})
#### Deleting multiple graph prototypes

Delete two graph prototypes.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.delete",
    "params": [
        "652",
        "653"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d24b4f06})

[comment]: # ({new-21f88b2f})
### Source

CGraphPrototype::delete() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-21f88b2f})
