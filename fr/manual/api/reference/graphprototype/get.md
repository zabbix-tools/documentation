[comment]: # translation:outdated

[comment]: # ({new-6a93039d})
# graphprototype.get

[comment]: # ({/new-6a93039d})

[comment]: # ({new-27b69e53})
### Description

`integer/array graphprototype.get(object parameters)`

The method allows to retrieve graph prototypes according to the given
parameters.

[comment]: # ({/new-27b69e53})

[comment]: # ({new-de0f74af})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|discoveryids|string/array|Return only graph prototypes that belong to the given discovery rules.|
|graphids|string/array|Return only graph prototypes with the given IDs.|
|groupids|string/array|Return only graph prototypes that belong to hosts in the given host groups.|
|hostids|string/array|Return only graph prototypes that belong to the given hosts.|
|inherited|boolean|If set to `true` return only graph prototypes inherited from a template.|
|itemids|string/array|Return only graph prototypes that contain the given item prototypes.|
|templated|boolean|If set to `true` return only graph prototypes that belong to templates.|
|templateids|string/array|Return only graph prototypes that belong to the given templates.|
|selectDiscoveryRule|query|Return the LLD rule that the graph prototype belongs to in the `discoveryRule` property.|
|selectGraphItems|query|Return the graph items used in the graph prototype in the `gitems` property.|
|selectGroups|query|Return the host groups that the graph prototype belongs to in the `groups` property.|
|selectHosts|query|Return the hosts that the graph prototype belongs to in the `hosts` property.|
|selectItems|query|Return the items and item prototypes used in the graph prototype in the `items` property.|
|selectTemplates|query|Return the templates that the graph prototype belongs to in the `templates` property.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the graph prototype belongs to;<br>`hostid` - ID of the host that the graph prototype belongs to.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `graphid`, `name` and `graphtype`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-de0f74af})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-62edff03})
#### Retrieving graph prototypes from a LLD rule

Retrieve all graph prototypes from an LLD rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.get",
    "params": {
        "output": "extend",
        "discoveryids": "27426"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "graphid": "1017",
            "parent_itemid": "27426",
            "name": "Disk space usage {#FSNAME}",
            "width": "600",
            "height": "340",
            "yaxismin": "0.0000",
            "yaxismax": "0.0000",
            "templateid": "442",
            "show_work_period": "0",
            "show_triggers": "0",
            "graphtype": "2",
            "show_legend": "1",
            "show_3d": "1",
            "percent_left": "0.0000",
            "percent_right": "0.0000",
            "ymin_type": "0",
            "ymax_type": "0",
            "ymin_itemid": "0",
            "ymax_itemid": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-62edff03})

[comment]: # ({new-e4f96e80})
### See also

-   [Discovery
    rule](/fr/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph item](/fr/manual/api/reference/graphitem/object#graph_item)
-   [Item](/fr/manual/api/reference/item/object#item)
-   [Host](/fr/manual/api/reference/host/object#host)
-   [Host group](/fr/manual/api/reference/hostgroup/object#host_group)
-   [Template](/fr/manual/api/reference/template/object#template)

[comment]: # ({/new-e4f96e80})

[comment]: # ({new-fc80fda8})
### Source

CGraphPrototype::get() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-fc80fda8})
