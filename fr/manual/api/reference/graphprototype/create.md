[comment]: # translation:outdated

[comment]: # ({new-576421be})
# graphprototype.create

[comment]: # ({/new-576421be})

[comment]: # ({new-fc8d3493})
### Description

`object graphprototype.create(object/array graphPrototypes)`

This method allows to create new graph prototypes.

[comment]: # ({/new-fc8d3493})

[comment]: # ({new-9beca929})
### Parameters

`(object/array)` Graph prototypes to create.

Additionally to the [standard graph prototype
properties](object#graph_prototype), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|**gitems**<br>(required)|array|Graph items to be created for the graph prototypes. Graph items can reference both items and item prototypes, but at least one item prototype must be present.|

[comment]: # ({/new-9beca929})

[comment]: # ({new-f1317691})
### Return values

`(object)` Returns an object containing the IDs of the created graph
prototypes under the `graphids` property. The order of the returned IDs
matches the order of the passed graph prototypes.

[comment]: # ({/new-f1317691})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-24ec4c22})
#### Creating a graph prototype

Create a graph prototype with two items.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graphprototype.create",
    "params": {
        "name": "Disk space usage {#FSNAME}",
        "width": 900,
        "height": 200,
        "gitems": [
            {
                "itemid": "22828",
                "color": "00AA00"
            },
            {
                "itemid": "22829",
                "color": "3333FF"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-24ec4c22})

[comment]: # ({new-88266563})
### See also

-   [Graph item](/fr/manual/api/reference/graphitem/object#graph_item)

[comment]: # ({/new-88266563})

[comment]: # ({new-c4b4834a})
### Source

CGraphPrototype::create() in
*frontends/php/include/classes/api/services/CGraphPrototype.php*.

[comment]: # ({/new-c4b4834a})
