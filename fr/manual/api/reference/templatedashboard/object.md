[comment]: # translation:outdated

[comment]: # ({new-1d7fc2e5})
# > Template dashboard object

The following objects are directly related to the `templatedashboard`
API.

[comment]: # ({/new-1d7fc2e5})

[comment]: # ({new-e0343baf})
### Template dashboard

The template dashboard object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|dashboardid|string|*(readonly)* ID of the template dashboard.|
|**name**<br>(required)|string|Name of the template dashboard.|
|**templateid**<br>(required)|string|ID of the template the dashboard belongs to.|
|display\_period|integer|Default page display period (in seconds).<br><br>Possible values: 10, 30, 60, 120, 600, 1800, 3600.<br><br>Default: 30.|
|auto\_start|integer|Auto start slideshow.<br><br>Possible values:<br>0 - do not auto start slideshow;<br>1 - *(default)* auto start slideshow.|
|uuid|string|Universal unique identifier, used for linking imported template dashboards to already existing ones. Auto-generated, if not given.<br><br>For update operations this field is *readonly*.|

[comment]: # ({/new-e0343baf})

[comment]: # ({new-69f1df50})
### Template dashboard page

The template dashboard page object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|dashboard\_pageid|string|*(readonly)* ID of the dashboard page.|
|name|string|Dashboard page name.<br><br>Default: empty string.|
|display\_period|integer|Dashboard page display period (in seconds).<br><br>Possible values: 0, 10, 30, 60, 120, 600, 1800, 3600.<br><br>Default: 0 (will use the default page display period).|
|widgets|array|Array of the [template dashboard widget](object#template_dashboard_widget) objects.|

[comment]: # ({/new-69f1df50})

[comment]: # ({new-9c7cc873})
### Template dashboard widget

The template dashboard widget object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|widgetid|string|*(readonly)* ID of the dashboard widget.|
|**type**<br>(required)|string|Type of the dashboard widget.<br><br>Possible values:<br>clock - Clock;<br>graph - Graph (classic);<br>graphprototype - Graph prototype;<br>item - Item value;<br>plaintext - Plain text;<br>url - URL;|
|name|string|Custom widget name.|
|x|integer|A horizontal position from the left side of the dashboard.<br><br>Valid values range from 0 to 23.|
|y|integer|A vertical position from the top of the dashboard.<br><br>Valid values range from 0 to 62.|
|width|integer|The widget width.<br><br>Valid values range from 1 to 24.|
|height|integer|The widget height.<br><br>Valid values range from 2 to 32.|
|view\_mode|integer|The widget view mode.<br><br>Possible values:<br>0 - (default) default widget view;<br>1 - with hidden header;|
|fields|array|Array of the [template dashboard widget field](object#template_dashboard_widget_field) objects.|

[comment]: # ({/new-9c7cc873})

[comment]: # ({new-76d2eb3b})
### Template dashboard widget field

The template dashboard widget field object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|Type of the widget field.<br><br>Possible values:<br>0 - Integer;<br>1 - String;<br>4 - Item;<br>5 - Item prototype;<br>6 - Graph;<br>7 - Graph prototype.|
|name|string|Widget field name.|
|**value**<br>(required)|mixed|Widget field value depending of type.|

[comment]: # ({/new-76d2eb3b})
