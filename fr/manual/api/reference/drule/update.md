[comment]: # translation:outdated

[comment]: # ({new-0a1af107})
# drule.update

[comment]: # ({/new-0a1af107})

[comment]: # ({new-ac17a9e2})
### Description

`object drule.update(object/array discoveryRules)`

This method allows to update existing discovery rules.

[comment]: # ({/new-ac17a9e2})

[comment]: # ({new-0dd5b0c8})
### Parameters

`(object/array)` Discovery rule properties to be updated.

The `druleid` property must be defined for each discovery rule, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard discovery rule
properties](object#discovery_rule), the method accepts the following
parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|dchecks|array|Discovery checks to replace existing checks.|

[comment]: # ({/new-0dd5b0c8})

[comment]: # ({new-495f933e})
### Return values

`(object)` Returns an object containing the IDs of the updated discovery
rules under the `druleids` property.

[comment]: # ({/new-495f933e})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-11119b8e})
#### Change the IP range of a discovery rule

Change the IP range of a discovery rule to "192.168.2.1-255".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.update",
    "params": {
        "druleid": "6",
        "iprange": "192.168.2.1-255"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-11119b8e})

[comment]: # ({new-54664cce})
### See also

-   [Discovery
    check](/fr/manual/api/reference/dcheck/object#discovery_check)

[comment]: # ({/new-54664cce})

[comment]: # ({new-d9252108})
### Source

CDRule::update() in
*frontends/php/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-d9252108})
