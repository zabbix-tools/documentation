[comment]: # translation:outdated

[comment]: # ({new-896915aa})
# template.get

[comment]: # ({/new-896915aa})

[comment]: # ({new-4e400711})
### Description

`integer/array template.get(object parameters)`

The method allows to retrieve templates according to the given
parameters.

[comment]: # ({/new-4e400711})

[comment]: # ({new-97f0e40b})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|templateids|string/array|Return only templates with the given template IDs.|
|groupids|string/array|Return only templates that belong to the given host groups.|
|parentTemplateids|string/array|Return only templates that are children of the given templates.|
|hostids|string/array|Return only templates that are linked to the given hosts.|
|graphids|string/array|Return only templates that contain the given graphs.|
|itemids|string/array|Return only templates that contain the given items.|
|triggerids|string/array|Return only templates that contain the given triggers.|
|with\_items|flag|Return only templates that have items.|
|with\_triggers|flag|Return only templates that have triggers.|
|with\_graphs|flag|Return only templates that have graphs.|
|with\_httptests|flag|Return only templates that have web scenarios.|
|selectGroups|query|Return the host groups that the template belongs to in the `groups` property.|
|selectHosts|query|Return the hosts that are linked to the template in the `hosts` property.<br><br>Supports `count`.|
|selectTemplates|query|Return the child templates in the `templates` property.<br><br>Supports `count`.|
|selectParentTemplates|query|Return the parent templates in the `parentTemplates` property.<br><br>Supports `count`.|
|selectHttpTests|query|Return the web scenarios from the template in the `httpTests` property.<br><br>Supports `count`.|
|selectItems|query|Return items from the template in the `items` property.<br><br>Supports `count`.|
|selectDiscoveries|query|Return low-level discoveries from the template in the `discoveries` property.<br><br>Supports `count`.|
|selectTriggers|query|Return triggers from the template in the `triggers` property.<br><br>Supports `count`.|
|selectGraphs|query|Return graphs from the template in the `graphs` property.<br><br>Supports `count`.|
|selectApplications|query|Return applications from the template in the `applications` property.<br><br>Supports `count`.|
|selectMacros|query|Return the macros from the template in the `macros` property..|
|selectScreens|query|Return screens from the template in the `screens` property.<br><br>Supports `count`.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectTemplates` - results will be sorted by `name`;<br>`selectHosts` - sorted by `host`;<br>`selectParentTemplates` - sorted by `host`;<br>`selectItems` - sorted by `name`;<br>`selectDiscoveries` - sorted by `name`;<br>`selectTriggers` - sorted by `description`;<br>`selectGraphs` - sorted by `name`;<br>`selectApplications` - sorted by `name`;<br>`selectScreens` - sorted by `name`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `hostid`, `host`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-97f0e40b})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7e546e9a})
#### Retrieving templates by name

Retrieve all data about two templates named "Template OS Linux" and
"Template OS Windows".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": "extend",
        "filter": {
            "host": [
                "Template OS Linux",
                "Template OS Windows"
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "proxy_hostid": "0",
            "host": "Template OS Linux",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Template OS Linux",
            "flags": "0",
            "templateid": "10001",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        },
        {
            "proxy_hostid": "0",
            "host": "Template OS Windows",
            "status": "3",
            "disable_until": "0",
            "error": "",
            "available": "0",
            "errors_from": "0",
            "lastaccess": "0",
            "ipmi_authtype": "0",
            "ipmi_privilege": "2",
            "ipmi_username": "",
            "ipmi_password": "",
            "ipmi_disable_until": "0",
            "ipmi_available": "0",
            "snmp_disable_until": "0",
            "snmp_available": "0",
            "maintenanceid": "0",
            "maintenance_status": "0",
            "maintenance_type": "0",
            "maintenance_from": "0",
            "ipmi_errors_from": "0",
            "snmp_errors_from": "0",
            "ipmi_error": "",
            "snmp_error": "",
            "jmx_disable_until": "0",
            "jmx_available": "0",
            "jmx_errors_from": "0",
            "jmx_error": "",
            "name": "Template OS Windows",
            "flags": "0",
            "templateid": "10081",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "tls_psk_identity": "",
            "tls_psk": ""
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-7e546e9a})

[comment]: # ({new-84d0cbf3})
#### Retrieving template groups

Retrieve template groups that the template "Linux by Zabbix agent" is a member of.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.get",
    "params": {
        "output": ["hostid"],
        "selectTemplateGroups": "extend",
        "filter": {
            "host": [
                "Linux by Zabbix agent"
            ]
        }
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "templateid": "10001",
            "templategroups": [
                {
                    "groupid": "10",
                    "name": "Templates/Operating systems",
                    "uuid": "846977d1dfed4968bc5f8bdb363285bc"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-84d0cbf3})

[comment]: # ({new-0fe2d603})
### See also

-   [Host group](/fr/manual/api/reference/hostgroup/object#host_group)
-   [Template](object#template)
-   [User
    macro](/fr/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/fr/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-0fe2d603})

[comment]: # ({new-ee3fc022})
### Source

CTemplate::get() in
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-ee3fc022})

[comment]: # ({new-4fdbde5d})
### Source

CTemplate::get() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-4fdbde5d})
