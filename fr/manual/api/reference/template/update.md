[comment]: # translation:outdated

[comment]: # ({new-36f834f7})
# template.update

[comment]: # ({/new-36f834f7})

[comment]: # ({new-f8425904})
### Description

`object template.update(object/array templates)`

This method allows to update existing templates.

[comment]: # ({/new-f8425904})

[comment]: # ({new-5d06cf34})
### Parameters

`(object/array)` Template properties to be updated.

The `templateid` property must be defined for each template, all other
properties are optional. Only the given properties will be updated, all
others will remain unchanged.

Additionally to the [standard template properties](object#template), the
method accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|groups|object/array|Host groups to replace the current host groups the templates belong to.<br><br>The host groups must have the `groupid` property defined.|
|hosts|object/array|Hosts and templates to replace the ones the templates are currently linked to.<br><br>Both hosts and templates must use the `hostid` property to pass an ID.|
|macros|object/array|User macros to replace the current user macros on the given templates.|
|templates|object/array|Templates to replace the currently linked templates. Templates that are not passed are only unlinked.<br><br>The templates must have the `templateid` property defined.|
|templates\_clear|object/array|Templates to unlink and clear from the given templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-5d06cf34})

[comment]: # ({new-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-0080b19f})
#### Renaming a template

Rename the template to "Template OS Linux".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.update",
    "params": {
        "templateid": "10086",
        "name": "Template OS Linux"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0080b19f})

[comment]: # ({new-e7972b69})
### Source

CTemplate::update() in
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-e7972b69})

[comment]: # ({new-eb72cc1c})
### Source

CTemplate::update() in *ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-eb72cc1c})
