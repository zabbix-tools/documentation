[comment]: # translation:outdated

[comment]: # ({new-a7242998})
# template.delete

[comment]: # ({/new-a7242998})

[comment]: # ({new-484b4ad2})
### Description

`object template.delete(array templateIds)`

This method allows to delete templates.

[comment]: # ({/new-484b4ad2})

[comment]: # ({new-1fbebaa3})
### Parameters

`(array)` IDs of the templates to delete.

[comment]: # ({/new-1fbebaa3})

[comment]: # ({new-fdd33aa6})
### Return values

`(object)` Returns an object containing the IDs of the deleted templates
under the `templateids` property.

[comment]: # ({/new-fdd33aa6})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3c8960bb})
#### Deleting multiple templates

Delete two templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.delete",
    "params": [
        "13",
        "32"
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "13",
            "32"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3c8960bb})

[comment]: # ({new-bfde186e})
### Source

CTemplate::delete() in
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-bfde186e})
