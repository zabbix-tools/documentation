[comment]: # translation:outdated

[comment]: # ({new-ac6966b9})
# > Template object

The following objects are directly related to the `template` API.

[comment]: # ({/new-ac6966b9})

[comment]: # ({new-ccfaad06})
### Template

The template object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|templateid|string|*(readonly)* ID of the template.|
|**host**<br>(required)|string|Technical name of the template.|
|description|text|Description of the template.|
|name|string|Visible name of the host.<br><br>Default: `host` property value.|

[comment]: # ({/new-ccfaad06})

[comment]: # ({new-2248aef2})
### Template tag

The template tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Template tag name.|
|value|string|Template tag value.|

[comment]: # ({/new-2248aef2})
