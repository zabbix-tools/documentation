[comment]: # translation:outdated

[comment]: # ({new-ae0db7b8})
# template.massremove

[comment]: # ({/new-ae0db7b8})

[comment]: # ({new-ba911402})
### Description

`object template.massremove(object parameters)`

This method allows to remove related objects from multiple templates.

[comment]: # ({/new-ba911402})

[comment]: # ({new-bd7f8b44})
### Parameters

`(object)` Parameters containing the IDs of the templates to update and
the objects that should be removed.

|Parameter|Type|Description|
|---------|----|-----------|
|**templateids**<br>(required)|string/array|IDs of the templates to be updated.|
|groupids|string/array|Host groups to remove the given templates from.|
|hostids|string/array|Hosts or templates to unlink the given templates from (downstream).|
|macros|string/array|User macros to delete from the given templates.|
|templateids\_clear|string/array|Templates to unlink and clear from the given templates (upstream).|
|templateids\_link|string/array|Templates to unlink from the given templates (upstream).|

[comment]: # ({/new-bd7f8b44})

[comment]: # ({new-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-20c1ddc2})
#### Removing templates from a group

Remove two templates from group "2".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": [
            "10085",
            "10086"
        ],
        "groupids": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-20c1ddc2})

[comment]: # ({new-6459bc33})
#### Unlinking templates from a host

Unlink templates "10106" and "10104" from template "10085".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massremove",
    "params": {
        "templateids": "10085",
        "templateids_link": [
            "10106",
            "10104"
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6459bc33})


[comment]: # ({new-186aec66})
### See also

-   [template.update](update)
-   [User
    macro](/fr/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-186aec66})

[comment]: # ({new-aba9c6f9})
### Source

CTemplate::massRemove() in
*frontends/php/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-aba9c6f9})
