[comment]: # translation:outdated

[comment]: # ({new-b8d971b2})
# Graph

This class is designed to work with items.

Object references:\

-   [Graph](/fr/manual/api/reference/graph/object#graph)

Available methods:\

-   [graph.create](/fr/manual/api/reference/graph/create) - creating new
    graphs
-   [graph.delete](/fr/manual/api/reference/graph/delete) - deleting
    graphs
-   [graph.get](/fr/manual/api/reference/graph/get) - retrieving graphs
-   [graph.update](/fr/manual/api/reference/graph/update) - updating
    graphs

[comment]: # ({/new-b8d971b2})
