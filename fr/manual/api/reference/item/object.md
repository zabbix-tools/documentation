[comment]: # translation:outdated

[comment]: # ({new-6bb9f60f})
# > Item object

The following objects are directly related to the `item` API.

[comment]: # ({/new-6bb9f60f})

[comment]: # ({new-385ac8b5})
### Item

::: noteclassic
Web items cannot be directly created, updated or deleted via
the Zabbix API.
:::

The item object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|itemid|string|*(readonly)* ID of the item.|
|**delay**<br>(required)|string|Update interval of the item. Accepts seconds or time unit with suffix and with or without one or more [custom intervals](/fr/manual/config/items/item/custom_intervals) that consist of either flexible intervals and scheduling intervals as serialized strings. Also accepts user macros. Flexible intervals could be written as two macros separated by a forward slash. Intervals are separated by a semicolon.<br><br>Optional for Zabbix trapper or Dependent item.|
|**hostid**<br>(required)|string|ID of the host that the item belongs to.<br><br>For update operations this field is *readonly*.|
|**interfaceid**<br>(required)|string|ID of the item's host interface. Used only for host items.<br><br>Optional for Zabbix agent (active), Zabbix internal, Zabbix trapper, Dependent item, Zabbix aggregate, database monitor and calculated items.|
|**key\_**<br>(required)|string|Item key.|
|**name**<br>(required)|string|Name of the item.|
|**type**<br>(required)|integer|Type of the item.<br><br>Possible values:<br>0 - Zabbix agent;<br>1 - SNMPv1 agent;<br>2 - Zabbix trapper;<br>3 - simple check;<br>4 - SNMPv2 agent;<br>5 - Zabbix internal;<br>6 - SNMPv3 agent;<br>7 - Zabbix agent (active);<br>8 - Zabbix aggregate;<br>9 - web item;<br>10 - external check;<br>11 - database monitor;<br>12 - IPMI agent;<br>13 - SSH agent;<br>14 - TELNET agent;<br>15 - calculated;<br>16 - JMX agent;<br>17 - SNMP trap;<br>18 - Dependent item;<br>19 - HTTP agent;|
|**url**<br>(required)|string|URL string, required only for HTTP agent item type. Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.|
|**value\_type**<br>(required)|integer|Type of information of the item.<br><br>Possible values:<br>0 - numeric float;<br>1 - character;<br>2 - log;<br>3 - numeric unsigned;<br>4 - text.|
|allow\_traps|integer|HTTP agent item field. Allow to populate value as in trapper item type also.<br><br>0 - *(default)* Do not allow to accept incoming data.<br>1 - Allow to accept incoming data.|
|authtype|integer|Used only by SSH agent items or HTTP agent items.<br><br>SSH agent authentication method possible values:<br>0 - *(default)* password;<br>1 - public key.<br><br>HTTP agent authentication method possible values:<br>0 - *(default)* none<br>1 - basic<br>2 - NTLM|
|description|string|Description of the item.|
|error|string|*(readonly)* Error text if there are problems updating the item.|
|flags|integer|*(readonly)* Origin of the item.<br><br>Possible values:<br>0 - a plain item;<br>4 - a discovered item.|
|follow\_redirects|integer|HTTP agent item field. Follow respose redirects while pooling data.<br><br>0 - Do not follow redirects.<br>1 - *(default)* Follow redirects.|
|headers|object|HTTP agent item field. Object with HTTP(S) request headers, where header name is used as key and header value as value.<br><br>Example:<br>{ "User-Agent": "Zabbix" }|
|history|string|A time unit of how long the history data should be stored. Also accepts user macro.<br><br>Default: 90d.|
|http\_proxy|string|HTTP agent item field. HTTP(S) proxy connection string.|
|inventory\_link|integer|ID of the host inventory field that is populated by the item.<br><br>Refer to the [host inventory page](/fr/manual/api/reference/host/object#host_inventory) for a list of supported host inventory fields and their IDs.<br><br>Default: 0.|
|ipmi\_sensor|string|IPMI sensor. Used only by IPMI items.|
|jmx\_endpoint|string|JMX agent custom connection string.<br><br>Default value:<br>service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi|
|lastclock|timestamp|*(readonly)* Time when the item was last updated.<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/fr/manual/web_interface/definitions).|
|lastns|integer|*(readonly)* Nanoseconds when the item was last updated.<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/fr/manual/web_interface/definitions).|
|lastvalue|string|*(readonly)* Last value of the item.<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/fr/manual/web_interface/definitions).|
|logtimefmt|string|Format of the time in log entries. Used only by log items.|
|master\_itemid|integer|Master item ID.<br>Recursion up to 3 dependent items and maximum count of dependent items equal to 999 are allowed.<br><br>Required by Dependent items.|
|mtime|timestamp|Time when the monitored log file was last updated. Used only by log items.|
|output\_format|integer|HTTP agent item field. Should response converted to JSON.<br><br>0 - *(default)* Store raw.<br>1 - Convert to JSON.|
|params|string|Additional parameters depending on the type of the item:<br>- executed script for SSH and Telnet items;<br>- SQL query for database monitor items;<br>- formula for calculated items.|
|password|string|Password for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br>When used by JMX, username should also be specified together with password or both properties should be left blank.|
|port|string|Port monitored by the item. Used only by SNMP items.|
|post\_type|integer|HTTP agent item field. Type of post data body stored in posts property.<br><br>0 - *(default)* Raw data.<br>2 - JSON data.<br>3 - XML data.|
|posts|string|HTTP agent item field. HTTP(S) request body data. Used with post\_type.|
|prevvalue|string|*(readonly)* Previous value of the item.<br><br>This property will only return a value for the period configured in [ZBX\_HISTORY\_PERIOD](/fr/manual/web_interface/definitions).|
|privatekey|string|Name of the private key file.|
|publickey|string|Name of the public key file.|
|query\_fields|array|HTTP agent item field. Query parameters. Array of objects with 'key':'value' pairs, where value can be empty string.|
|request\_method|integer|HTTP agent item field. Type of request method.<br><br>0 - GET<br>1 - *(default)* POST<br>2 - PUT<br>3 - HEAD|
|retrieve\_mode|integer|HTTP agent item field. What part of response should be stored.<br><br>0 - *(default)* Body.<br>1 - Headers.<br>2 - Both body and headers will be stored.<br><br>For request\_method HEAD only 1 is allowed value.|
|snmp\_community|string|SNMP community. Used only by SNMPv1 and SNMPv2 items.|
|snmp\_oid|string|SNMP OID.|
|snmpv3\_authpassphrase|string|SNMPv3 auth passphrase. Used only by SNMPv3 items.|
|snmpv3\_authprotocol|integer|SNMPv3 authentication protocol. Used only by SNMPv3 items.<br><br>Possible values:<br>0 - *(default)* MD5;<br>1 - SHA.|
|snmpv3\_contextname|string|SNMPv3 context name. Used only by SNMPv3 items.|
|snmpv3\_privpassphrase|string|SNMPv3 priv passphrase. Used only by SNMPv3 items.|
|snmpv3\_privprotocol|integer|SNMPv3 privacy protocol. Used only by SNMPv3 items.<br><br>Possible values:<br>0 - *(default)* DES;<br>1 - AES.|
|snmpv3\_securitylevel|integer|SNMPv3 security level. Used only by SNMPv3 items.<br><br>Possible values:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|string|SNMPv3 security name. Used only by SNMPv3 items.|
|ssl\_cert\_file|string|HTTP agent item field. Public SSL Key file path.|
|ssl\_key\_file|string|HTTP agent item field. Private SSL Key file path.|
|ssl\_key\_password|string|HTTP agent item field. Password for SSL Key file.|
|state|integer|*(readonly)* State of the item.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - not supported.|
|status|integer|Status of the item.<br><br>Possible values:<br>0 - *(default)* enabled item;<br>1 - disabled item.|
|status\_codes|string|HTTP agent item field. Ranges of required HTTP status codes separated by commas. Also supports user macros as part of comma separated list.<br><br>Example: 200,200-{$M},{$M},200-400|
|templateid|string|(readonly) ID of the parent template item.|
|timeout|string|HTTP agent item field. Item data polling request timeout. Support user macros.<br><br>default: 3s<br>maximum value: 60s|
|trapper\_hosts|string|Allowed hosts. Used by trapper items or HTTP agent items.|
|trends|string|A time unit of how long the trends data should be stored. Also accepts user macro.<br><br>Default: 365d.|
|units|string|Value units.|
|username|string|Username for authentication. Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.<br><br>Required by SSH and Telnet items.<br>When used by JMX, password should also be specified together with username or both properties should be left blank.|
|valuemapid|string|ID of the associated value map.|
|verify\_host|integer|HTTP agent item field. Validate host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|
|verify\_peer|integer|HTTP agent item field. Validate is host certificate authentic.<br><br>0 - *(default)* Do not validate.<br>1 - Validate.|

[comment]: # ({/new-385ac8b5})

[comment]: # ({new-c1b98afa})
### Item preprocessing

The item preprocessing object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|

[comment]: # ({/new-c1b98afa})

[comment]: # ({new-2169bb78})
### Item preprocessing

The item preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|The preprocessing option type.<br><br>Possible values:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second;<br>11 - XML XPath;<br>12 - JSONPath;<br>13 - In range;<br>14 - Matches regular expression;<br>15 - Does not match regular expression;<br>16 - Check for error in JSON;<br>17 - Check for error in XML;<br>18 - Check for error using regular expression;<br>19 - Discard unchanged;<br>20 - Discard unchanged with heartbeat;<br>21 - JavaScript;<br>22 - Prometheus pattern;<br>23 - Prometheus to JSON;<br>24 - CSV to JSON;<br>25 - Replace;<br>26 - Check unsupported;<br>27 - XML to JSON.|
|**params**<br>(required)|string|Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n) character.|
|**error\_handler**<br>(required)|integer|Action type used in case of preprocessing step failure.<br><br>Possible values:<br>0 - Error message is set by Zabbix server;<br>1 - Discard value;<br>2 - Set custom value;<br>3 - Set custom error message.|
|**error\_handler\_params**<br>(required)|string|Error handler parameters. Used with `error_handler`.<br><br>Must be empty, if `error_handler` is 0 or 1.<br>Can be empty if, `error_handler` is 2.<br>Cannot be empty, if `error_handler` is 3.|

The following parameters and error handlers are supported for each
preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|1|Custom multiplier|number^1,\ 6^|<|<|0, 1, 2, 3|
|2|Right trim|list of characters^2^|<|<|<|
|3|Left trim|list of characters^2^|<|<|<|
|4|Trim|list of characters^2^|<|<|<|
|5|Regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|6|Boolean to decimal|<|<|<|0, 1, 2, 3|
|7|Octal to decimal|<|<|<|0, 1, 2, 3|
|8|Hexadecimal to decimal|<|<|<|0, 1, 2, 3|
|9|Simple change|<|<|<|0, 1, 2, 3|
|10|Change per second|<|<|<|0, 1, 2, 3|
|11|XML XPath|path^4^|<|<|0, 1, 2, 3|
|12|JSONPath|path^4^|<|<|0, 1, 2, 3|
|13|In range|min^1,\ 6^|max^1,\ 6^|<|0, 1, 2, 3|
|14|Matches regular expression|pattern^3^|<|<|0, 1, 2, 3|
|15|Does not match regular expression|pattern^3^|<|<|0, 1, 2, 3|
|16|Check for error in JSON|path^4^|<|<|0, 1, 2, 3|
|17|Check for error in XML|path^4^|<|<|0, 1, 2, 3|
|18|Check for error using regular expression|pattern^3^|output^2^|<|0, 1, 2, 3|
|19|Discard unchanged|<|<|<|<|
|20|Discard unchanged with heartbeat|seconds^5,\ 6^|<|<|<|
|21|JavaScript|script^2^|<|<|<|
|22|Prometheus pattern|pattern^6,\ 7^|output^6,\ 8^|<|0, 1, 2, 3|
|23|Prometheus to JSON|pattern^6,\ 7^|<|<|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|<|<|
|26|Check unsupported|<|<|<|1, 2, 3|
|27|XML to JOSN|<|<|<|0, 1, 2, 3|

^1^ integer or floating-point number\
^2^ string\
^3^ regular expression\
^4^ JSONPath or XML XPath\
^5^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^6^ user macro\
^7^ Prometheus pattern following the syntax:
`<metric name>{<label name>="<label value>", ...} == <value>`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro.\
^8^ Prometheus output following the syntax: `<label name>`.

[comment]: # ({/new-2169bb78})
