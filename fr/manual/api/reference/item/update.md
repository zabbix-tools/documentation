[comment]: # translation:outdated

[comment]: # ({new-0ec30c22})
# item.update

[comment]: # ({/new-0ec30c22})

[comment]: # ({new-38961105})
### Description

`object item.update(object/array items)`

This method allows to update existing items.

::: noteclassic
Web items cannot be updated via the Zabbix API.
:::

[comment]: # ({/new-38961105})

[comment]: # ({new-4e151fd6})
### Parameters

`(object/array)` Item properties to be updated.

The `itemid` property must be defined for each item, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard item properties](object#item), the method
accepts the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|applications|array|IDs of the applications to replace the current applications.|
|preprocessing|array|Item preprocessing options to replace the current preprocessing options.|

[comment]: # ({/new-4e151fd6})

[comment]: # ({new-aefff750})
### Return values

`(object)` Returns an object containing the IDs of the updated items
under the `itemids` property.

[comment]: # ({/new-aefff750})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5388578b})
#### Enabling an item

Enable an item, that is, set its status to "0".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-5388578b})

[comment]: # ({new-0bf89382})
#### Update dependent item

Update Dependent item name and Master item ID. Only dependencies on same
host are allowed, therefore Master and Dependent item should have same
hostid.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "name": "Dependent item updated name",
        "master_itemid": "25562",
        "itemid": "189019"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189019"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0bf89382})

[comment]: # ({new-8f365363})
#### Update HTTP agent item

Enable item value trapping.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "allow_traps": "1"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-8f365363})

[comment]: # ({new-44f323b8})
### Source

CItem::update() in
*frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-44f323b8})

[comment]: # ({new-dcfb98a0})
#### Updating a script item

Update a script item with a different script and remove unnecessary
parameters that were used by previous script.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.Log(3, 'Log test');\nreturn 1;"
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-dcfb98a0})

[comment]: # ({new-646fe32a})
### Source

CItem::update() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-646fe32a})
