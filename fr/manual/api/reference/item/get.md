[comment]: # translation:outdated

[comment]: # ({new-58fbd3bc})
# item.get

[comment]: # ({/new-58fbd3bc})

[comment]: # ({new-673edf7f})
### Description

`integer/array item.get(object parameters)`

The method allows to retrieve items according to the given parameters.

[comment]: # ({/new-673edf7f})

[comment]: # ({new-cc946363})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|itemids|string/array|Return only items with the given IDs.|
|groupids|string/array|Return only items that belong to the hosts from the given groups.|
|templateids|string/array|Return only items that belong to the given templates.|
|hostids|string/array|Return only items that belong to the given hosts.|
|proxyids|string/array|Return only items that are monitored by the given proxies.|
|interfaceids|string/array|Return only items that use the given host interfaces.|
|graphids|string/array|Return only items that are used in the given graphs.|
|triggerids|string/array|Return only items that are used in the given triggers.|
|applicationids|string/array|Return only items that belong to the given applications.|
|webitems|flag|Include web items in the result.|
|inherited|boolean|If set to `true` return only items inherited from a template.|
|templated|boolean|If set to `true` return only items that belong to templates.|
|monitored|boolean|If set to `true` return only enabled items that belong to monitored hosts.|
|group|string|Return only items that belong to a group with the given name.|
|host|string|Return only items that belong to a host with the given name.|
|application|string|Return only items that belong to an application with the given name.|
|with\_triggers|boolean|If set to `true` return only items that are used in triggers.|
|selectHosts|query|Returns the host that the item belongs to as an array in the `hosts` property.|
|selectInterfaces|query|Returns the host interface used by the item as an array in the `interfaces` property.|
|selectTriggers|query|Return triggers that the item is used in in the `triggers` property.<br><br>Supports `count`.|
|selectGraphs|query|Return graphs that contain the item in the `graphs` property.<br><br>Supports `count`.|
|selectApplications|query|Return the applications that the item belongs to in the `applications` property.|
|selectDiscoveryRule|query|Return the LLD rule that created the item in the `discoveryRule` property.|
|selectItemDiscovery|query|Return the item discovery object in the `itemDiscovery` property. The item discovery object links the item to an item prototype from which it was created.<br><br>It has the following properties:<br>`itemdiscoveryid` - `(string)` ID of the item discovery;<br>`itemid` - `(string)` ID of the discovered item;<br>`parent_itemid` - `(string)` ID of the item prototype from which the item has been created;<br>`key_` - `(string)` key of the item prototype;<br>`lastcheck` - `(timestamp)` time when the item was last discovered;<br>`ts_delete` - `(timestamp)` time when an item that is no longer discovered will be deleted.|
|selectPreprocessing|query|Return item preprocessing options in `preprocessing` property.<br><br>It has the following properties:<br>`type` - `(string)` The preprocessing option types:<br>1 - Custom multiplier;<br>2 - Right trim;<br>3 - Left trim;<br>4 - Trim;<br>5 - Regular expression matching;<br>6 - Boolean to decimal;<br>7 - Octal to decimal;<br>8 - Hexadecimal to decimal;<br>9 - Simple change;<br>10 - Change per second.<br><br>`params` - `(string)` Additional parameters used by preprocessing option. Multiple parameters are separated by LF (\\n)character.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the item belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectGraphs` - results will be sorted by `name`;<br>`selectTriggers` - results will be sorted by `description`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `itemid`, `name`, `key_`, `delay`, `history`, `trends`, `type` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-cc946363})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-59b84d93})
#### Finding items by key

Retrieve all items from host with ID "10084" that have the word "system"
in the key and sort them by name.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "10084",
        "search": {
            "key_": "system"
        },
        "sortfield": "name"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "23298",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "Context switches per second",
            "key_": "system.cpu.switches",
            "delay": "1m",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "2552",
            "lastclock": "1351090998",
            "prevvalue": "2641",
            "state": "0",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "sps",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "22680",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564054253",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "23299",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "CPU $2 time",
            "key_": "system.cpu.util[,idle]",
            "delay": "1m",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "86.031879",
            "lastclock": "1351090999",
            "prevvalue": "85.306944",
            "state": "0",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "%",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "17354",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564256864",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "The time the CPU has spent doing nothing.",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        },
        {
            "itemid": "23300",
            "type": "0",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10084",
            "name": "CPU $2 time",
            "key_": "system.cpu.util[,interrupt]",
            "history": "7d",
            "trends": "365d",
            "lastvalue": "0.008389",
            "lastclock": "1351091000",
            "prevvalue": "0.000000",
            "state": "0",
            "status": "0",
            "value_type": "0",
            "trapper_hosts": "",
            "units": "%",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "snmpv3_contextname": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "22671",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "lastns": "564661387",
            "flags": "0",
            "interfaceid": "1",
            "port": "",
            "description": "The amount of time the CPU has been servicing hardware interrupts.",
            "inventory_link": "0",
            "lifetime": "0s",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-59b84d93})

[comment]: # ({new-674b0856})
#### Finding dependent items by key

Retrieve all dependent items from host with ID "10116" that have the
word "apache" in the key.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "10116",
        "search": {
            "key_": "apache"
        },
        "filter": {
            "type": "18"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "25550",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Days",
            "key_": "apache.status.uptime.days",
            "delay": "",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        },
        {
            "itemid": "25555",
            "type": "18",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10116",
            "name": "Hours",
            "key_": "apache.status.uptime.hours",
            "delay": "0",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "master_itemid": "25545",
            "jmx_endpoint": "",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "1",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-674b0856})

[comment]: # ({new-94dc4b21})
#### Find HTTP agent item

Find HTTP agent item with post body type XML for specific host id.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "hostids": "10255",
        "filter": {
            "type": "19",
            "post_type": "3"
        }
    },
    "id": 3,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28252",
            "type": "19",
            "snmp_community": "",
            "snmp_oid": "",
            "hostid": "10255",
            "name": "template item",
            "key_": "ti",
            "delay": "30s",
            "history": "90d",
            "trends": "365d",
            "status": "0",
            "value_type": "3",
            "trapper_hosts": "",
            "units": "",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "formula": "",
            "error": "",
            "lastlogsize": "0",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "mtime": "0",
            "flags": "0",
            "interfaceid": "0",
            "port": "",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "state": "0",
            "snmpv3_contextname": "",
            "evaltype": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "localhost",
            "query_fields": [
                {
                    "mode": "xml"
                }
            ],
            "posts": "<body>\r\n<![CDATA[{$MACRO}<foo></bar>]]>\r\n</body>",
            "status_codes": "200",
            "follow_redirects": "0",
            "post_type": "3",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "1",
            "request_method": "3",
            "output_format": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "lastclock": "0",
            "lastns": "0",
            "lastvalue": "0",
            "prevvalue": "0"
        }
    ],
    "id": 3
}
```

[comment]: # ({/new-94dc4b21})

[comment]: # ({new-f4b01bbe})
### See also

-   [Application](/fr/manual/api/reference/application/object#application)
-   [Discovery
    rule](/fr/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Graph](/fr/manual/api/reference/graph/object#graph)
-   [Host](/fr/manual/api/reference/host/object#host)
-   [Host
    interface](/fr/manual/api/reference/hostinterface/object#host_interface)
-   [Trigger](/fr/manual/api/reference/trigger/object#trigger)

[comment]: # ({/new-f4b01bbe})

[comment]: # ({new-d3276b35})
### Source

CItem::get() in *frontends/php/include/classes/api/services/CItem.php*.

[comment]: # ({/new-d3276b35})

[comment]: # ({new-a6ef1d0f})
### Source

CItem::get() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-a6ef1d0f})
