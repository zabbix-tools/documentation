[comment]: # translation:outdated

[comment]: # ({new-6bf804b5})
# Host interface

This class is designed to work with host interfaces.

Object references:\

-   [Host
    interface](/fr/manual/api/reference/hostinterface/object#host_interface)

Available methods:\

-   [hostinterface.create](/fr/manual/api/reference/hostinterface/create) -
    creating new host interfaces
-   [hostinterface.delete](/fr/manual/api/reference/hostinterface/delete) -
    deleting host interfaces
-   [hostinterface.get](/fr/manual/api/reference/hostinterface/get) -
    retrieving host interfaces
-   [hostinterface.massadd](/fr/manual/api/reference/hostinterface/massadd) -
    adding host interfaces to hosts
-   [hostinterface.massremove](/fr/manual/api/reference/hostinterface/massremove) -
    removing host interfaces from hosts
-   [hostinterface.replacehostinterfaces](/fr/manual/api/reference/hostinterface/replacehostinterfaces) -
    replacing host interfaces on a host
-   [hostinterface.update](/fr/manual/api/reference/hostinterface/update) -
    updating host interfaces

[comment]: # ({/new-6bf804b5})
