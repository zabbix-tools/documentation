[comment]: # translation:outdated

[comment]: # ({new-7e2bad53})
# Item prototype

This class is designed to work with item prototypes.

Object references:\

-   [Item
    prototype](/fr/manual/api/reference/itemprototype/object#item_prototype)

Available methods:\

-   [itemprototype.create](/fr/manual/api/reference/itemprototype/create) -
    creating new item prototypes
-   [itemprototype.delete](/fr/manual/api/reference/itemprototype/delete) -
    deleting item prototypes
-   [itemprototype.get](/fr/manual/api/reference/itemprototype/get) -
    retrieving item prototypes
-   [itemprototype.update](/fr/manual/api/reference/itemprototype/update) -
    updating item prototypes

[comment]: # ({/new-7e2bad53})
