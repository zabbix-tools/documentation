[comment]: # translation:outdated

[comment]: # ({new-8961fc33})
# script.create

[comment]: # ({/new-8961fc33})

[comment]: # ({new-d7a51ffa})
### Description

`object script.create(object/array scripts)`

This method allows to create new scripts.

[comment]: # ({/new-d7a51ffa})

[comment]: # ({new-bcb9a453})
### Parameters

`(object/array)` Scripts to create.

The method accepts scripts with the [standard script
properties](/manual/api/reference/script/object#script).

[comment]: # ({/new-bcb9a453})

[comment]: # ({new-15216b8d})
### Return values

`(object)` Returns an object containing the IDs of the created scripts
under the `scriptids` property. The order of the returned IDs matches
the order of the passed scripts.

[comment]: # ({/new-15216b8d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-55d911d7})
#### Create a script

Create a script that will reboot a server. The script will require write
access to the host and will display a configuration message before
running in the frontend.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "Reboot server",
        "command": "reboot server 1",
        "host_access": 3,
        "confirmation": "Are you sure you would like to reboot the server?"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-55d911d7})

[comment]: # ({new-16a09744})
### Source

CScript::create() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-16a09744})

[comment]: # ({new-0e2eb663})
#### Create a custom script

Create a custom script that will reboot a server. The script will
require write access to the host and will display a configuration
message before running in the frontend.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "Reboot server",
        "command": "reboot server 1",
        "confirmation": "Are you sure you would like to reboot the server?",
        "scope": 2,
        "type": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "4"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0e2eb663})

[comment]: # ({new-c363dc9b})
#### Create an URL type script

Create an URL type script that for host scope and remains in same window and has confirmation text.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "URL script",
        "type": 6,
        "scope": 2,
        "url": "http://zabbix/ui/zabbix.php?action=host.edit&hostid={HOST.ID}",
        "confirmation": "Edit host {HOST.NAME}?",
        "new_window": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "56"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c363dc9b})

[comment]: # ({new-8c09f129})
### Source

CScript::create() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/new-8c09f129})
