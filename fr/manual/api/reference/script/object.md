[comment]: # translation:outdated

[comment]: # ({new-8b1700c1})
# > Script object

The following objects are directly related to the `script` API.

[comment]: # ({/new-8b1700c1})

[comment]: # ({new-9c6db006})
### Script

The script object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|scriptid|string|*(readonly)* ID of the script.|
|**command**<br>(required)|string|Command to run.|
|**name**<br>(required)|string|Name of the script.|
|confirmation|string|Confirmation pop up text. The pop up will appear when trying to run the script from the Zabbix frontend.|
|description|string|Description of the script.|
|execute\_on|integer|Where to run the script.<br><br>Possible values:<br>0 - run on Zabbix agent;<br>1 - run on Zabbix server.<br>2 - *(default)* run on Zabbix server (proxy).|
|groupid|string|ID of the host group that the script can be run on. If set to 0, the script will be available on all host groups.<br><br>Default: 0.|
|host\_access|integer|Host permissions needed to run the script.<br><br>Possible values:<br>2 - *(default)* read;<br>3 - write.|
|type|integer|Script type.<br><br>Possible values:<br>0 - *(default)* script;<br>1 - IPMI.|
|usrgrpid|string|ID of the user group that will be allowed to run the script. If set to 0, the script will be available for all user groups.<br><br>Default: 0.|

[comment]: # ({/new-9c6db006})

[comment]: # ({new-05b0e37d})
### Webhook parameters

Parameters passed to webhook script when it is called have the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**name**<br>(required)|string|Parameter name.|
|value|string|Parameter value. Supports [macros](/manual/appendix/macros/supported_by_location).|

[comment]: # ({/new-05b0e37d})

[comment]: # ({new-587d2fd0})
### Debug

Debug information of executed webhook script. The debug object has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|logs|array|Array of [log entries](/manual/api/reference/script/object#Log entry).|
|ms|string|Script execution duration in milliseconds.|

[comment]: # ({/new-587d2fd0})

[comment]: # ({new-8ea23127})
### Log entry

The log entry object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|level|integer|Log level.|
|ms|string|The time elapsed in milliseconds since the script was run before log entry was added.|
|message|string|Log message.|

[comment]: # ({/new-8ea23127})
