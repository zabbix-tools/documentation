[comment]: # translation:outdated

[comment]: # ({new-cd91b3fe})
# script.get

[comment]: # ({/new-cd91b3fe})

[comment]: # ({new-96ec37bc})
### Description

`integer/array script.get(object parameters)`

The method allows to retrieve scripts according to the given parameters.

[comment]: # ({/new-96ec37bc})

[comment]: # ({new-c425a65a})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|groupids|string/array|Return only scripts that can be run on the given host groups.|
|hostids|string/array|Return only scripts that can be run on the given hosts.|
|scriptids|string/array|Return only scripts with the given IDs.|
|usrgrpids|string/array|Return only scripts that can be run by users in the given user groups.|
|selectGroups|query|Return host groups that the script can be run on in the `groups` property.|
|selectHosts|query|Return hosts that the script can be run on in the `hosts` property.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `scriptid` and `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-c425a65a})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6d428f32})
#### Retrieve all scripts

Retrieve all configured scripts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [    
        {
            "scriptid": "1",
            "name": "Ping",
            "command": "/bin/ping -c 3 {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "0",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        },
        {
            "scriptid": "2",
            "name": "Traceroute",
            "command": "/usr/bin/traceroute {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "0",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        },
        {
            "scriptid": "3",
            "name": "Detect operating system",
            "command": "sudo /usr/bin/nmap -O {HOST.CONN} 2>&1",
            "host_access": "2",
            "usrgrpid": "7",
            "groupid": "0",
            "description": "",
            "confirmation": "",
            "type": "0",
            "execute_on": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-6d428f32})

[comment]: # ({new-b0b740ec})
### See also

-   [Host](/fr/manual/api/reference/host/object#object_details)
-   [Host
    group](/fr/manual/api/reference/hostgroup/object#object_details)

[comment]: # ({/new-b0b740ec})

[comment]: # ({new-90dfc753})
### Source

CScript::get() in
*frontends/php/include/classes/api/services/CScript.php*.

[comment]: # ({/new-90dfc753})
