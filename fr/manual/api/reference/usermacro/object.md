[comment]: # translation:outdated

[comment]: # ({new-28092341})
# > User macro object

The following objects are directly related to the `usermacro` API.

[comment]: # ({/new-28092341})

[comment]: # ({new-e654cd12})
### Global macro

The global macro object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|globalmacroid|string|*(readonly)* ID of the global macro.|
|**macro**<br>(required)|string|Macro string.|
|**value**<br>(required)|string|Value of the macro.|

[comment]: # ({/new-e654cd12})

[comment]: # ({new-5a3a9f9f})
### Host macro

The host macro object defines a macro available on a host or template.
It has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|hostmacroid|string|*(readonly)* ID of the host macro.|
|**hostid**<br>(required)|string|ID of the host that the macro belongs to.|
|**macro**<br>(required)|string|Macro string.|
|**value**<br>(required)|string|Value of the macro.|

[comment]: # ({/new-5a3a9f9f})
