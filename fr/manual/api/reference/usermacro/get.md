[comment]: # translation:outdated

[comment]: # ({new-02ea8022})
# usermacro.get

[comment]: # ({/new-02ea8022})

[comment]: # ({new-28befc82})
### Description

`integer/array usermacro.get(object parameters)`

The method allows to retrieve host and global macros according to the
given parameters.

[comment]: # ({/new-28befc82})

[comment]: # ({new-141835f9})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|Type|Description|
|---------|----|-----------|
|globalmacro|flag|Return global macros instead of host macros.|
|globalmacroids|string/array|Return only global macros with the given IDs.|
|groupids|string/array|Return only host macros that belong to hosts or templates from the given host groups.|
|hostids|string/array|Return only macros that belong to the given hosts or templates.|
|hostmacroids|string/array|Return only host macros with the given IDs.|
|selectGroups|query|Return host groups that the host macro belongs to in the `groups` property.<br><br>Used only when retrieving host macros.|
|selectHosts|query|Return hosts that the host macro belongs to in the `hosts` property.<br><br>Used only when retrieving host macros.|
|selectTemplates|query|Return templates that the host macro belongs to in the `templates` property.<br><br>Used only when retrieving host macros.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible value: `macro`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/fr/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-141835f9})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7089e052})
#### Retrieving host macros for a host

Retrieve all host macros defined for host "10198".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "hostids": "10198"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostmacroid": "9",
            "hostid": "10198",
            "macro": "{$INTERFACE}",
            "value": "eth0"
        },
        {
            "hostmacroid": "11",
            "hostid": "10198",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-7089e052})

[comment]: # ({new-29b64a7a})
#### Retrieving global macros

Retrieve all global macros.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.get",
    "params": {
        "output": "extend",
        "globalmacro": true
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "globalmacroid": "6",
            "macro": "{$SNMP_COMMUNITY}",
            "value": "public"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-29b64a7a})

[comment]: # ({new-26db0ec2})
### Source

CUserMacro::get() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-26db0ec2})
