[comment]: # translation:outdated

[comment]: # ({new-edb56cfa})
# usermacro.delete

[comment]: # ({/new-edb56cfa})

[comment]: # ({new-5eb2a780})
### Description

`object usermacro.delete(array hostMacroIds)`

This method allows to delete host macros.

[comment]: # ({/new-5eb2a780})

[comment]: # ({new-dd1b319f})
### Parameters

`(array)` IDs of the host macros to delete.

[comment]: # ({/new-dd1b319f})

[comment]: # ({new-cd694080})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
macros under the `hostmacroids` property.

[comment]: # ({/new-cd694080})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-b2f5bee1})
#### Deleting multiple host macros

Delete two host macros.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usermacro.delete",
    "params": [
        "32",
        "11"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostmacroids": [
            "32",
            "11"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b2f5bee1})

[comment]: # ({new-9d01c95a})
### Source

CUserMacro::delete() in
*frontends/php/include/classes/api/services/CUserMacro.php*.

[comment]: # ({/new-9d01c95a})
