[comment]: # translation:outdated

[comment]: # ({new-f759709d})
# Image

This class is designed to work with images.

Object references:\

-   [Image](/fr/manual/api/reference/image/object#image)

Available methods:\

-   [image.create](/fr/manual/api/reference/image/create) - create new
    images
-   [image.delete](/fr/manual/api/reference/image/delete) - delete
    images
-   [image.get](/fr/manual/api/reference/image/get) - retrieve images
-   [image.update](/fr/manual/api/reference/image/update) - update
    images

[comment]: # ({/new-f759709d})
