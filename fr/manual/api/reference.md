[comment]: # translation:outdated

[comment]: # ({df55af4b-b0d207c5})
# Référentiel des méthodes

Cette section fournit une vue d'ensemble des fonctions fournies par l'API Zabbix et vous aidera à vous repérer dans les classes et méthodes disponibles.

[comment]: # ({/df55af4b-b0d207c5})

[comment]: # ({b8c2b441-53edf72e})
### Surveillance

L'API Zabbix vous permet d'accéder à l'historique et aux autres données collectées lors de la surveillance.

[comment]: # ({/b8c2b441-53edf72e})

[comment]: # ({new-ead86ed5})
#### Historique

Récupérer les valeurs historiques collectées par les processus de
surveillance Zabbix pour présentation ou traitement ultérieur.

[History API](/fr/manual/api/reference/history)

[comment]: # ({/new-ead86ed5})

[comment]: # ({fd1ca6ae-46aecf9d})
#### Historique

Récupérer les valeurs historiques collectées par les processus de surveillance Zabbix pour présentation ou traitement ultérieur.

[API Historique](/manual/api/reference/history)

[comment]: # ({/fd1ca6ae-46aecf9d})

[comment]: # ({90f80e06-ce08707b})
#### Tendances

Récupérer les valeurs de tendance calculées par le serveur Zabbix pour présentation ou un traitement ultérieur.

[API Tendance](/manual/api/reference/trend)

[comment]: # ({/90f80e06-ce08707b})

[comment]: # ({ffbd2594-4e280d13})
#### Evénements

Récupérer les événements générés par les déclencheurs, la découverte du réseau et les autres systèmes Zabbix pour une gestion plus flexible des situations ou une intégration à des outils tiers.

[API Evénement](/manual/api/reference/event)

[comment]: # ({/ffbd2594-4e280d13})

[comment]: # ({adce0596-087f1047})
#### Problèmes

Récupérer les problèmes en fonction de paramètres donnés.

[API Problème](/manual/api/reference/problem)

[comment]: # ({/adce0596-087f1047})


[comment]: # ({new-2b35beaa})
#### Service Level Agreement

Define Service Level Objectives (SLO), retrieve detailed Service Level Indicators (SLI)
information about service performance.

[SLA API](/manual/api/reference/sla)

[comment]: # ({/new-2b35beaa})

[comment]: # ({df080b28-ea863fe1})
#### Tâches

Interagir avec le gestionnaire de tâches du serveur Zabbix, créer des tâches et récuperer leurs retours.

[API Tâche](/manual/api/reference/task)

[comment]: # ({/df080b28-ea863fe1})

[comment]: # ({new-services})
## Services

The Zabbix API allows you to access data gathered
during service monitoring.

[comment]: # ({/new-services})

[comment]: # ({f2d96c57-467838af})
### Configuration

L'API Zabbix vous permet de gérer la configuration de votre système de supervision.

[API Configuration](/manual/api/reference/configuration)

[comment]: # ({/f2d96c57-467838af})

[comment]: # ({f0c985c0-4fe0de77})
#### Hôtes et groupes d'hôtes

Gérer les groupes d'hôtes, les hôtes et tout ce qui s'y rapporte, y compris les interfaces des hôtes, les macros hôtes et les périodes de maintenance.

[API Hôte](/manual/api/reference/host) | [API Groupe d'hôte](/manual/api/reference/hostgroup) | [API Interface d'hôte](/manual/api/reference/hostinterface) | [API Macro utilisateur
API](/manual/api/reference/usermacro) | [API Table de correspondance(/manual/api/reference/valuemap) | [API Maintenance](/manual/api/reference/maintenance)

[comment]: # ({/f0c985c0-4fe0de77})

[comment]: # ({d799b539-29ae0d97})
#### Eléments 

Définir les éléments à surveiller.

[API Elément](/manual/api/reference/item)

[comment]: # ({/d799b539-29ae0d97})

[comment]: # ({a6721a50-de0198b3})
#### Déclencheurs

Configurer des déclencheurs pour être informé des problèmes de votre système. Gérer les dépendances des déclencheurs.

[API Déclencheur](/manual/api/reference/trigger)

[comment]: # ({/a6721a50-de0198b3})

[comment]: # ({60b0c396-f61b98cf})
#### Graphiques

Modifier des graphiques ou des éléments de graphique séparés pour une meilleure présentation des données recueillies.

[API Graphique](/manual/api/reference/graph) | [API Élément de graphique](/manual/api/reference/graphitem)

[comment]: # ({/60b0c396-f61b98cf})

[comment]: # ({4e0ab28a-7a170077})
#### Modèles

Gérer les modèles et les associer à des hôtes ou à d'autres modèles.

[API Modèle](/manual/api/reference/template) | [API Table de correspondance](/manual/api/reference/valuemap)

[comment]: # ({/4e0ab28a-7a170077})

[comment]: # ({58c804e6-56f63ece})
#### Exportation et importation

Exporter et importer les données de configuration Zabbix pour les sauvegardes de configuration, les migrations ou les mises à jour de configuration à grande échelle.

[API Configuration ](/manual/api/reference/configuration)

[comment]: # ({/58c804e6-56f63ece})

[comment]: # ({b0f91698-61993c62})
#### Découverte de bas niveau

Configurer des règles de découverte de bas niveau ainsi que des prototypes d'éléments, de déclencheurs et de graphiques pour surveiller les entités dynamiques.

[API Règle de découverte bas niveau](/manual/api/reference/discoveryrule) |  [API Prototype d'élément](/manual/api/reference/itemprototype) | [API Prototype de déclencheur](/manual/api/reference/triggerprototype) | [API Prototype de graphique](/manual/api/reference/graphprototype) | [API Prototype d'hôte](/manual/api/reference/hostprototype)

[comment]: # ({/b0f91698-61993c62})

[comment]: # ({new-63c25731})
#### Actions et alertes

Définir des actions et des opérations pour informer les utilisateurs de
certains événements ou exécuter automatiquement des commandes à
distance. Accéder à des informations sur les alertes générées et leurs
récepteurs.

[Action API](/fr/manual/api/reference/action) | [Alert
API](/fr/manual/api/reference/alert)

[comment]: # ({/new-63c25731})

[comment]: # ({new-3ea1b33b})
#### Services

Gérer les services pour la surveillance du niveau de service et
récupérer des informations détaillées sur les SLA de tous les services.

[Service API](/fr/manual/api/reference/service)

[comment]: # ({/new-3ea1b33b})

[comment]: # ({new-media})
#### Media types

Configure media types and multiple ways users will receive alerts.

[Media type API](/manual/api/reference/mediatype)

[comment]: # ({/new-media})

[comment]: # ({new-891cbcad})
#### Tableaux de bord

Gérer les tableaux de bord.

[Dashboard API](/fr/manual/api/reference/dashboard)

[comment]: # ({/new-891cbcad})

[comment]: # ({new-1a3dcbcf})
#### Écrans

Modifier les écrans globaux et au niveau du modèle ou chaque élément
d'écran individuellement.

[Screen API](/fr/manual/api/reference/screen) | [Screen item
API](/fr/manual/api/reference/screenitem) | [Template screen
API](/fr/manual/api/reference/templatescreen) | [Template screen item
API](/fr/manual/api/reference/templatescreenitem)

[comment]: # ({/new-1a3dcbcf})

[comment]: # ({new-29b6c9a4})
#### Cartes

Configurer des cartes pour créer des représentations dynamiques
détaillées de votre infrastructure informatique.

[Map API](/fr/manual/api/reference/map)

[comment]: # ({/new-29b6c9a4})

[comment]: # ({new-e083e0e0})
#### Surveillance Web

Configurer des scénarios Web pour surveiller vos applications et
services Web.

[Web scenario API](/fr/manual/api/reference/httptest)

[comment]: # ({/new-e083e0e0})

[comment]: # ({new-alerts})
## Alerts

The Zabbix API allows you to manage the actions and alerts of your monitoring system.

[comment]: # ({/new-alerts})

[comment]: # ({new-91788676})
#### Découverte du réseau

Gérer les règles de découverte réseau pour rechercher et surveiller
automatiquement les nouveaux hôtes. Obtenir un accès complet aux
informations sur les services et les hôtes découverts.

[Discovery rule API](/fr/manual/api/reference/drule) | [Discovery check
API](/fr/manual/api/reference/dcheck) | [Discovery host
API](/fr/manual/api/reference/dhost) | [Discovery service
API](/fr/manual/api/reference/dservice)

[comment]: # ({/new-91788676})

[comment]: # ({new-ad3cb7b9})
### Administration

Avec l'API Zabbix, vous pouvez modifier les paramètres d'administration
de votre système de surveillance.

[comment]: # ({/new-ad3cb7b9})

[comment]: # ({new-c2d096e3})
#### Utilisateurs

Ajouter des utilisateurs qui auront accès à Zabbix, les affecter aux
groupes d'utilisateurs et accorder des autorisations. Configurer les
types de média et les manières dont les utilisateurs recevront des
alertes.

[User API](/fr/manual/api/reference/user) | [User group
API](/fr/manual/api/reference/usergroup) | [Media type
API](/fr/manual/api/reference/mediatype)

[comment]: # ({/new-c2d096e3})

[comment]: # ({new-925840a1})
#### Général

Modifier certaines options de configuration globales.

[Icon map API](/fr/manual/api/reference/iconmap) | [Image
API](/fr/manual/api/reference/image) | [User macro
API](/fr/manual/api/reference/usermacro)

[comment]: # ({/new-925840a1})

[comment]: # ({new-audit})
#### Audit log

Track configuration changes each user has done.

[Audit log API](/manual/api/reference/auditlog)

[comment]: # ({/new-audit})

[comment]: # ({new-housekeeping})
#### Housekeeping

Configure housekeeping.

[Housekeeping API](/manual/api/reference/housekeeping)

[comment]: # ({/new-housekeeping})


[comment]: # ({new-feb914d9})
#### Scripts

Configurer et exécuter des scripts pour vous aider dans vos tâches
quotidiennes.

[Script API](/fr/manual/api/reference/script)

[comment]: # ({/new-feb914d9})

[comment]: # ({new-macros})
#### Macros

Manage macros.

[User macro API](/manual/api/reference/usermacro)

[comment]: # ({/new-macros})

[comment]: # ({new-a1c36c8e})
### Informations API

Récupérer la version de l'API Zabbix afin que votre application puisse
utiliser les fonctionnalités spécifiques à la version.

[API info API](/fr/manual/api/reference/apiinfo)

[comment]: # ({/new-a1c36c8e})

[comment]: # ({new-4050de56})
#### API Tokens

Manage authorization tokens.

[Token API](/manual/api/reference/token)

[comment]: # ({/new-4050de56})

[comment]: # ({new-4e5421dd})
#### Scripts

Configure and execute scripts to help you with your daily tasks.

[Script API](/manual/api/reference/script)

[comment]: # ({/new-4e5421dd})

[comment]: # ({new-users})
## Users

The Zabbix API allows you to manage users of your monitoring system.

[comment]: # ({/new-users})

[comment]: # ({new-3b3700b0})
### API information

Retrieve the version of the Zabbix API so that your application could
use version-specific features.

[API info API](/manual/api/reference/apiinfo)

[comment]: # ({/new-3b3700b0})
