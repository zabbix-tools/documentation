[comment]: # translation:outdated

[comment]: # ({new-734ebac5})
# 9. Web monitoring

[comment]: # ({/new-734ebac5})

[comment]: # ({new-f37bc18c})
#### Aperçu

Avec Zabbix, vous pouvez vérifier plusieurs aspects de la disponibilité
des sites Web.\

::: noteimportant
Pour effectuer une supervision Web, le serveur
Zabbix doit être initialement
[configuré](/fr/manual/installation/install#from_the_sources) avec le
support cURL (libcurl).
:::

Pour activer la supervision Web, vous devez définir des scénarios Web.
Un scénario Web consiste en une ou plusieurs requêtes HTTP ou "étapes".
Les étapes sont exécutées périodiquement par le serveur Zabbix dans un
ordre prédéfini. Si un hôte est supervisé par proxy, les étapes sont
exécutées par le proxy.

Depuis Zabbix 2.2, les scénarios Web sont associés aux hôtes/modèles de
la même manière que les éléments, les déclencheurs, etc. Cela signifie
que les scénarios Web peuvent également être créés au niveau du modèle,
puis appliqués à plusieurs hôtes en même temps.

Les informations suivantes sont collectées dans n'importe quel scénario
Web :

-   vitesse moyenne de téléchargement par seconde pour toutes les étapes
    du scénario entier
-   numéro de l'étape qui a échouée
-   dernier message d'erreur

Les informations suivantes sont collectées dans chaque étape de scénario
Web :

-   vitesse de téléchargement par seconde
-   Temps de réponse
-   Code de réponse

Pour plus de détails, voir les [éléments de supervision
web](/fr/manual/web_monitoring/items).

Les données collectées lors de l'exécution de scénarios Web sont
conservées dans la base de données. Les données sont automatiquement
utilisées pour les graphiques, les déclencheurs et les notifications.

Zabbix peut également vérifier si une page HTML récupérée contient une
chaîne prédéfinie. Il peut exécuter une connexion simulée et suivre un
chemin de clics simulés sur la page.

La supervision Web Zabbix prend en charge à la fois HTTP et HTTPS. Lors
de l'exécution d'un scénario Web, Zabbix suit éventuellement les
redirections (voir l'option *Suivre les redirections* en dessous). Le
nombre maximal de redirections est codé en dur à 10 (en utilisant
l'option cURL
[CURLOPT\_MAXREDIRS](http://curl.haxx.se/libcurl/c/CURLOPT_MAXREDIRS.html)).
Tous les cookies sont conservés lors de l'exécution d'un scénario
unique.

Voir aussi les [problèmes
connus](/fr/manual/installation/known_issues#https_checks) pour la
supervision Web à l'aide du protocole HTTPS.

[comment]: # ({/new-f37bc18c})

[comment]: # ({new-f007debe})
#### Configurer un scenario web

Pour configurer un scenario web :

-   Allez dans : *Configuration → Hôtes* (ou *Modèles*)
-   Cliquez sur *Web* dans la ligne de l'hôte/modèle
-   Cliquez sur *Créer un scénario web* à droite (ou sur le nom du
    scénario pour modifier un scénario existant)
-   Entrez les paramètres du scénario dans le formulaire

L'onglet **Scénario** vous permet de configurer les paramètres généraux
d'un scénario Web.

![](../../assets/en/manual/config/scenario.png){width="600"}

Tous les champs de saisie obligatoires sont marqués d'un astérisque
rouge.

Paramètres de scénario :

|Paramètre|Description|<|<|<|
|----------|-----------|-|-|-|
|*Hôte*|Nom de l'hôte/modèle auquel le scénario appartient.|<|<|<|
|*Nom*|Nom unique du scénario.<br>Les macros utilisateurs et les [macros](/fr/manual/appendix/macros/supported_by_location) {HOST.\*} sont supportées depuis Zabbix 2.2.|<|<|<|
|*Application*|Sélectionnez une application à laquelle le scénario appartiendra.<br>Les éléments du scénario Web seront regroupés sous l'application sélectionnée dans // Surveillance → Dernières données *. \| \|*Nouvelle application//|Entrez le nom de la nouvelle application pour le scénario.|<|<|
|*Intervalle de mise à jour*|A quelle fréquence le scénario sera exécuté.<br>Les [suffixes de temps](/fr/manual/appendix/suffixes) sont supportés, ex : 30s, 1m, 2h, 1d, depuis Zabbix 3.4.0.<br>Les [macros utilisateurs](/fr/manual/config/macros/usermacros) sont supportées depuis Zabbix 3.4.0.<br>*Notez* que si une macro utilisateur est utilisée et que sa valeur change (e.g. 5m → 30s), la prochaine vérification sera exécutée selon la valeur précédente (plus tard dans le futur avec les valeurs d'exemple).|<|<|<|
|*Tentatives*|Nombre de tentatives d'exécution des étapes du scénario Web. En cas de problèmes réseau (timeout, absence de connectivité, etc.), Zabbix peut répéter plusieurs fois l'exécution d'une étape. Le chiffre positionné affectera également chaque étape du scénario. Vous pouvez spécifier jusqu'à 10 tentatives, la valeur par défaut est 1.<br>// Note //: Zabbix ne répètera pas une étape en raison d'un code de réponse erroné ou d'une non-concordance d'une chaîne requise.<br>Ce paramètre est pris en charge à partir de *Zabbix 2.2*.|<|<|<|
|*Agent*|Sélectionnez un agent client.<br>Zabbix fera semblant d'être le navigateur sélectionné. Ceci est utile lorsqu'un site Web renvoie un contenu différent pour différents navigateurs.<br>Les macros utilisateur peuvent être utilisées dans ce champ, // à partir de Zabbix 2.2 *. \| \|*Proxy HTTP//|Vous pouvez spécifier un proxy HTTP à utiliser, en utilisant le format suivant : *http://\[username\[:password\]@\]proxy.mycompany.com\[:port\]*<br>Par défaut, le port 1080 sera utilisé.<br>Si spécifié, le proxy remplacera les variables d'environnement liées au proxy, telles que http\_proxy, HTTPS\_PROXY. S'il n'est pas spécifié, le proxy n'écrasera pas les variables d'environnement liées au proxy.<br>La valeur saisie est transmise "telle quelle", aucune vérification de la validité n'est effectuée. Vous pouvez également entrer une adresse proxy SOCKS. Si vous spécifiez le mauvais protocole, la connexion échouera et l'élément ne sera plus pris en charge. En l'absence de protocole spécifié, le proxy sera traité comme un proxy HTTP.<br>//Remarque //: Seule une authentification simple est prise en charge avec le proxy HTTP.<br>Les macros utilisateur peuvent être utilisées dans ce champ.<br>Ce paramètre est pris en charge à partir de // Zabbix 2.2 *. \| \|*Variables//|Variables pouvant être utilisées dans les étapes du scénario (URL, variables post).<br>Elles ont le format suivant :<br>**{macro1}**=value1<br>**{macro2}**=value2<br>**{macro3}**=regex:<regular expression><br>Par exemple :<br>{username}=Alexei<br>{password}=kj3h5kJ34bd<br>{hostid}=regex:hostid is (\[0-9\]+)<br>Les macros peuvent ensuite être référencées dans les étapes sous la forme {username}, {password} et {hostid}. Zabbix les remplacera automatiquement par des valeurs réelles. Notez que les variables avec `regex:` ont besoin d'une étape pour obtenir la valeur de l'expression régulière afin que la valeur extraite ne puisse être appliquée qu'à l'étape suivante.<br>Si la partie valeur commence par "regex:", la partie après celle-ci est traitée comme une expression régulière qui recherche la page Web et, si elle est trouvée, stocke la correspondance dans la variable. Au moins un sous-groupe doit être présent pour que la valeur correspondante puisse être extraite. La correspondance des expressions régulières dans les variables est prise en charge // depuis Zabbix 2.2 *.<br>Les macros utilisateur et les [macros](/manual/appendix/macros/supported_by_location) {HOST.\*} sont pris en charge, depuis Zabbix 2.2.<br>Les variables sont automatiquement encodées en URL lorsqu'elles sont utilisées dans des champs de requête ou des données de formulaire pour des variables post, mais doivent être encodées manuellement lorsqu'elles sont utilisées en post brut ou directement dans l'URL. \| \|*Headers//|Les en-têtes HTTP personnalisés qui seront envoyés lors de l'exécution d'une requête.<br>Les en-têtes doivent être répertoriés en utilisant la même syntaxe qu'ils apparaîssent dans le protocole HTTP, en utilisant éventuellement certaines fonctionnalités supplémentaires prises en charge par l'option cURL [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html).<br>Par exemple :<br>Accept-Charset=utf-8<br>Accept-Language=en-US<br>Content-Type=application/xml; charset=utf-8<br>Les macros utilisateur et les [macros](/fr/manual/appendix/macros/supported_by_location) {HOST.\*} sont supportées.<br>La spécification des en-têtes personnalisées est supportée *à partir de Zabbix 2.4*.|
|*Activer*|Le scénario est activé sur cette case est cochée, sinon - désactivé.|<|<|<|

Notez que lors de l'édition d'un scénario existant, deux boutons
supplémentaires sont disponibles dans le formulaire :

|   |   |
|---|---|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clone.png)|Créez un autre scénario basé sur les propriétés d'un scénario existant.|
|![](../../assets/en/manual/web_monitoring/scenario_edit_clear.png)|Supprimer l'historique et les données de tendance du scénario. Cela obligera le serveur à exécuter le scénario immédiatement après la suppression des données.|

::: notetip
Si le champs *proxy HTTP* est laissé vide, un autre
moyen d'utiliser un proxy HTTP est de positionner les variables
d'environnement liés au proxy.

Pour les vérifications HTTP - positionnez la variable d'environnement
**http\_proxy**pour l'utilisateur du serveur Zabbix server user. Par
exemple :
//http\_proxy=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).

Pour les vérifications HTTPS - positionnez la variable d'environnement
**HTTPS\_PROXY**. Par exemple,
//HTTPS\_PROXY=[http:%%//%%proxy\_ip:proxy\_port//](http:%%//%%proxy_ip:proxy_port//).
Plus de détails sont disponibles en exécutant la commande : *\# man
curl*.
:::

L'onglet **Etapes** vous permet de configurer les étapes du scénario
web. Pour ajouter une étape d'un scénario web, cliquez sur *Ajouter*
dans le bloc *Etapes*.

![](../../assets/en/manual/config/scenario2.png){width="600"}

[comment]: # ({/new-f007debe})

[comment]: # ({new-74c8baab})
#### Configuring steps

![](../../assets/en/manual/config/scenario_step.png){width="600"}

Step parameters:

|Parameter|Description|
|---------|-----------|
|*Name*|Unique step name.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*URL*|URL to connect to and retrieve data. For example:<br>https://www.google.com<br>http://www.zabbix.com/download<br>Domain names can be specified in Unicode characters since Zabbix 3.4. They are automatically punycode-converted to ASCII when executing the web scenario step.<br>The *Parse* button can be used to separate optional query fields (like ?name=Admin&password=mypassword) from the URL, moving the attributes and values into *Query fields* for automatic URL-encoding.<br>Variables can be used in the URL, using the {macro} syntax. Variables can be URL-encoded manually using a {{macro}.urlencode()} syntax.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.<br>Limited to 2048 characters *starting with Zabbix 2.4*.|
|*Query fields*|HTTP GET variables for the URL.<br>Specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from scenario variables, user macros or {HOST.\*} macros are resolved and then URL-encoded automatically. Using a {{macro}.urlencode()} syntax will double URL-encode them.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported since Zabbix 2.2.|
|*Post*|HTTP POST variables.<br>In **Form data** mode, specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from scenario variables, user macros or {HOST.\*} macros are resolved and then URL-encoded automatically.<br>In **Raw data** mode, attributes/values are displayed on a single line and concatenated with a **&** symbol.<br>Raw values can be URL-encoded/decoded manually using a {{macro}.urlencode()} or {{macro}.urldecode()} syntax.<br>For example: id=2345&userid={user}<br>If {user} is defined as a variable of the web scenario, it will be replaced by its value when the step is executed. If you wish to URL-encode the variable, substitute {user} with {{user}.urlencode()}.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*Variables*|Step-level variables that may be used for GET and POST functions.<br>Specified as attribute and value pairs.<br>Step-level variables override scenario-level variables or variables from the previous step. However, the value of a step-level variable only affects the step after (and not the current step).<br>They have the following format:<br>**{macro}**=value<br>**{macro}**=regex:<regular expression><br>For more information see variable description on the [scenario](web_monitoring#configuring_a_web_scenario) level.<br>Having step-level variables is supported since Zabbix 2.2.<br>Variables are automatically URL-encoded when used in query fields or form data for post variables, but must be URL-encoded manually when used in raw post or directly in URL.|
|*Headers*|Custom HTTP headers that will be sent when performing a request.<br>Specified as attribute and value pairs.<br>Headers on the step level will overwrite the headers specified for the scenario.<br>For example, setting a 'User-Agent' attribute with no value will remove the User-Agent value set on scenario level.<br>User macros and {HOST.\*} macros are supported.<br>This sets the [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL option.<br>Specifying custom headers is supported *starting with Zabbix 2.4*.|
|*Follow redirects*|Mark the checkbox to follow HTTP redirects.<br>This sets the [CURLOPT\_FOLLOWLOCATION](http://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*Retrieve only headers*|Mark the checkbox to retrieve only headers from the HTTP response.<br>This sets the [CURLOPT\_NOBODY](http://curl.haxx.se/libcurl/c/CURLOPT_NOBODY.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*Timeout*|Zabbix will not spend more than the set amount of time on processing the URL (maximum is 1 hour). Actually this parameter defines the maximum time for making connection to the URL and maximum time for performing an HTTP request. Therefore, Zabbix will not spend more than **2 x Timeout** seconds on the step.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 1h. [User macros](/manual/config/macros/usermacros) are supported.|
|*Required string*|Required regular expressions pattern.<br>Unless retrieved content (HTML) matches required pattern the step will fail. If empty, no check is performed.<br>For example:<br>Homepage of Zabbix<br>Welcome.\*admin<br>*Note*: Referencing [regular expressions](regular_expressions) created in the Zabbix frontend is not supported in this field.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported, since Zabbix 2.2.|
|*Required status codes*|List of expected HTTP status codes. If Zabbix gets a code which is not in the list, the step will fail.<br>If empty, no check is performed.<br>For example: 200,201,210-299<br>User macros are supported since Zabbix 2.2.|

::: noteclassic
Any changes in web scenario steps will only be saved when
the whole scenario is saved.
:::

See also a [real-life example](/fr/manual/web_monitoring/example) of how
web monitoring steps can be configured.

[comment]: # ({/new-74c8baab})

[comment]: # ({new-6f40ea68})
#### Configuring authentication

The **Authentication** tab allows you to configure scenario
authentication options.

![](../../assets/en/manual/config/scenario3.png)

Authentication parameters:

|Parameter|Description|
|---------|-----------|
|*Authentication*|Authentication options.<br>**None** - no authentication used.<br>**Basic authentication** - basic authentication is used.<br>**NTLM authentication** - NTLM ([Windows NT LAN Manager)](http://en.wikipedia.org/wiki/NTLM) authentication is used.<br>Selecting an authentication method will provide two additional fields for entering a user name and password.<br>User macros can be used in user and password fields, *starting with Zabbix 2.2*.|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the web server.<br>The server certificate will be automatically taken from system-wide certificate authority (CA) location. You can override the location of CA files using Zabbix server or proxy configuration parameter [SSLCALocation](/fr/manual/appendix/config/zabbix_server).<br>This sets the [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the web server certificate matches.<br>This sets the [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL certificate file*|Name of the SSL certificate file used for client authentication. The certificate file must be in PEM^1^ format. If the certificate file contains also the private key, leave the *SSL key file* field empty. If the key is encrypted, specify the password in *SSL key password* field. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLCertLocation](/fr/manual/appendix/config/zabbix_server).<br>`HOST.*` macros and user macros can be used in this field.<br>This sets the [CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL key file*|Name of the SSL private key file used for client authentication. The private key file must be in PEM^1^ format. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLKeyLocation](/fr/manual/appendix/config/zabbix_server).<br>`HOST.*` macros and user macros can be used in this field.<br>This sets the [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|
|*SSL key password*|SSL private key file password.<br>User macros can be used in this field.<br>This sets the [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html) cURL option.<br>This option is supported *starting with Zabbix 2.4*.|

::: noteimportant
 \[1\] Zabbix supports certificate and private key
files in PEM format only. In case you have your certificate and private
key data in PKCS \#12 format file (usually with extention \*.p12 or
\*.pfx) you may generate the PEM file from it using the following
commands:

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key


:::

::: noteclassic
 Zabbix server picks up changes in certificates without a
restart. 
:::

::: noteclassic
 If you have client certificate and private key in a single
file just specify it in a "SSL certificate file" field and leave "SSL
key file" field empty. The certificate and key must still be in PEM
format. Combining certificate and key is easy:

    cat client.crt client.key > client.pem


:::

[comment]: # ({/new-6f40ea68})

[comment]: # ({new-4cce2d90})
#### Display

To view detailed data of defined web scenarios, go to *Monitoring → Web*
or *Latest data*. Click on the scenario name to see more detailed
statistics.

![](../../assets/en/manual/web_monitoring/scenario_details.png){width="600"}

An overview of web monitoring scenarios can be viewed in *Monitoring →
Dashboard*.

[comment]: # ({/new-4cce2d90})

[comment]: # ({new-ae869848})
#### Extended monitoring

Sometimes it is necessary to log received HTML page content. This is
especially useful if some web scenario step fails. Debug level 5 (trace)
serves that purpose. This level can be set in
[server](/fr/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option (`-R log_level_increase="http poller,N"`,
where N is the process number). The following examples demonstrate how
extended monitoring can be started provided debug level 4 is already
set:

    Increase log level of all http pollers:
    shell> zabbix_server -R log_level_increase="http poller"

    Increase log level of second http poller:
    shell> zabbix_server -R log_level_increase="http poller,2"

If extended web monitoring is not required it can be stopped using the
`-R log_level_decrease` option.

[comment]: # ({/new-ae869848})

[comment]: # ({new-42ab5787})
#### Extended monitoring

Sometimes it is necessary to log received HTML page content. This is
especially useful if some web scenario step fails. Debug level 5 (trace)
serves that purpose. This level can be set in
[server](/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option (`-R log_level_increase="http poller,N"`,
where N is the process number). The following examples demonstrate how
extended monitoring can be started provided debug level 4 is already
set:

    Increase log level of all http pollers:
    shell> zabbix_server -R log_level_increase="http poller"

    Increase log level of second http poller:
    shell> zabbix_server -R log_level_increase="http poller,2"

If extended web monitoring is not required it can be stopped using the
`-R log_level_decrease` option.

[comment]: # ({/new-42ab5787})
