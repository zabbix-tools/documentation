[comment]: # aside: 2

[comment]: # translation:outdated

[comment]: # ({new-e6df368c})
# 1 Virtual machine discovery key fields

The following table lists fields returned by virtual machine related
discovery keys.

|**Item key**|<|<|
|------------|-|-|
|**Description**|**Field**|**Retrieved content**|
|vmware.cluster.discovery|<|<|
|Performs cluster discovery.|{\#CLUSTER.ID}|Cluster identifier.|
|^|{\#CLUSTER.NAME}|Cluster name.|
|vmware.hv.discovery|<|<|
|Performs hypervisor discovery.|{\#HV.UUID}|Unique hypervisor identifier.|
|^|{\#HV.ID}|Hypervisor identifier (HostSystem managed object name).|
|^|{\#HV.NAME}|Hypervisor name.|
|^|{\#CLUSTER.NAME}|Cluster name, might be empty.|
|^|{\#DATACENTER.NAME}|Datacenter name.|
|vmware.hv.datastore.discovery|<|<|
|Performs hypervisor datastore discovery. Note that multiple hypervisors can use the same datastore.|{\#DATASTORE}|Datastore name.|
|vmware.vm.discovery|<|<|
|Performs virtual machine discovery.|{\#VM.UUID}|Unique virtual machine identifier.|
|^|{\#VM.ID}|Virtual machine identifier (VirtualMachine managed object name).|
|^|{\#VM.NAME}|Virtual machine name.|
|^|{\#HV.NAME}|Hypervisor name.|
|^|{\#CLUSTER.NAME}|Cluster name, might be empty.|
|^|{\#DATACENTER.NAME}|Datacenter name.|
|vmware.vm.net.if.discovery|<|<|
|Performs virtual machine network interface discovery.|{\#IFNAME}|Network interface name.|
|vmware.vm.vfs.dev.discovery|<|<|
|Performs virtual machine disk device discovery.|{\#DISKNAME}|Disk device name.|
|vmware.vm.vfs.fs.discovery|<|<|
|Performs virtual machine file system discovery.|{\#FSNAME}|File system name.|

[comment]: # ({/new-e6df368c})
