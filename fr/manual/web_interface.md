[comment]: # translation:outdated

[comment]: # ({new-5c86be0b})
# 18. Interface Web

[comment]: # ({/new-5c86be0b})

[comment]: # ({new-8e039e89})
#### Vue d'ensemble

Pour un accès facile à Zabbix depuis n'importe où et depuis n'importe
quelle plate-forme, une interface Web est fournie.

::: noteclassic
Tenter d'accéder à deux installations d'interface Zabbix sur
le même hôte, sur des ports différents, échouera. La connexion à la
seconde mettra fin à la session sur la première et
inversement.
:::

[comment]: # ({/new-8e039e89})

[comment]: # ({new-278be5f6})
### Frontend help

A help link ![](../../assets/en/manual/web_interface/help_link.png) is provided in Zabbix frontend forms with direct links to the corresponding parts of the documentation.

[comment]: # ({/new-278be5f6})
