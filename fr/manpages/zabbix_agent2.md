[comment]: # translation:outdated

[comment]: # ({845fd40d-9beb7cfa})
# zabbix\_agent2

Section: Commandes de maintenance (8)\
Updated: 2019-01-29\
[Index](#index) [Retourner au contenu principal](/documentation/5.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/845fd40d-9beb7cfa})

[comment]: # ({952ee2c4-942e0d82})
## NOM

zabbix\_agent2 - Zabbix agent 2\
\
[ ]{#lbAC}

[comment]: # ({/952ee2c4-942e0d82})

[comment]: # ({new-5535a811})
## SYNOPSIS

**zabbix\_agent2** \[**-c** *config-file*\]\
**zabbix\_agent2** \[**-c** *config-file*\] **-p**\
**zabbix\_agent2** \[**-c** *config-file*\] **-t** *item-key*\
**zabbix\_agent2** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_agent2 -h**\
**zabbix\_agent2 -V**\
\
[ ]{#lbAD}

[comment]: # ({/new-5535a811})

[comment]: # ({0d9ae307-5b789849})
## DESCRIPTION

**zabbix\_agent2** est une application pour surveiller les paramètres de divers services.\
\
[ ]{#lbAE}

[comment]: # ({/0d9ae307-5b789849})

[comment]: # ({f6339ee4-7197fda6})
## OPTIONS

**-c**, **--config** *config-file*  
Utiliser le fichier de configuration indiqué dans *config-file* plutôt que le fichier par défaut.

**-R**, **--runtime-control** *runtime-option*  
Exécuter les fonctions administratives selon les options indiqués dans *runtime-option*.

[comment]: # ({/f6339ee4-7197fda6})

[comment]: # ({new-c754f1e2})
### 

  

[comment]: # ({/new-c754f1e2})

[comment]: # ({a270940c-c730d427})
#### Options de contrôle d'exécution :

  
**userparameter reload**  
Recharger les paramètres utilisateur à partir du fichier de configuration

  
**loglevel increase**  
Augmenter le niveau de journalisation

  
**loglevel decrease**  
Diminuer le niveau de journalisation

  
**help**  
Lister les options de contrôle d'exécution disponibles

  
**metrics**  
Lister les métriques disponibles

  
**version**  
Afficher la version

```{=html}
<!-- -->
```
**-p**, **--print**  
Afficher les éléments connus et quitter. Pour chaque élément, soit des valeurs génériques par défaut sont utilisées, soit des valeurs par défaut spécifiques sont fournies pour les tests. Ces valeurs par défaut sont répertoriées entre crochets en tant que paramètres clés de l'élément. Les  valeurs renvoyées se trouvent entre crochets et sont préfixées par le type de la valeur renvoyée, séparées par pipe. Pour les paramètres utilisateur, le type est toujours **t**, car l'agent ne peut pas déterminer toutes les valeurs de retour possibles. Les éléments, affichés comme fonctionnant, ne sont pas garantis de fonctionner à partir du serveur Zabbix ou de zabbix\_get lors de l'interrogation d'un démon d'agent en cours d'exécution, car les autorisations ou l'environnement peuvent être différents. Les types de valeurs renvoyées sont :
  

d  
Nombre avec une partie décimale.

  
m  
Non supporté. Cela peut être dû à l'interrogation d'une élément qui ne fonctionne que en mode actif comme par exemple un élément de surveillance de fichier de log ou une élément qui nécessite que plusieurs valeurs soient collectées.
Les problèmes d'autorisations ou des paramètres utilisateur incorrects peut également entraîner l'état non supporté.

  
s  
Texte. Longueur maximum non limitée.

  
t  
Texte. Idem que **s**.

  
u  
Entier non signé.

\

**-t**, **--test** *item-key*  
Tester un seul élément et quitter. Voir **--print** pour la description de l'affichage.

**-h**, **--help**  
Afficher cet aide et quitter.

**-V**, **--version**  
Afficher les informations concernant la version et quiiter.

[ ]{#lbAG}

[comment]: # ({/a270940c-c730d427})

[comment]: # ({4ac2575f-37997f11})
## FICHIERS

*/usr/local/etc/zabbix\_agent2.conf*
Emplacement par défaut du fichier de configuration de l'agent Zabbix 2 (s'il n'est pas modifié lors de compilation).

[ ]{#lbAH}

[comment]: # ({/4ac2575f-37997f11})

[comment]: # ({0c353db4-ad1bba15})
## VOIR AUSSI 

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_js](zabbix_js)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAI}

[comment]: # ({/0c353db4-ad1bba15})

[comment]: # ({ba301169-96162c7c})
## AUTEUR

Zabbix LLC

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/ba301169-96162c7c})

[comment]: # ({d6e0e831-8d9d9852})
## Index

[NOM](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

  

[FICHIERS](#lbAG)

[VOIR AUSSI](#lbAH)

[AUTHEUR](#lbAI)

------------------------------------------------------------------------

[comment]: # ({/d6e0e831-8d9d9852})
