[comment]: # translation:outdated

[comment]: # ({new-e0cabf53})
# zabbix\_proxy

Section: Maintenance Commands (8)\
Updated: 2016-01-13\
[Index](#index) [Return to Main Contents](/documentation/3.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-e0cabf53})

[comment]: # ({new-c7ba44c5})
## NAME

zabbix\_proxy - Zabbix proxy daemon [ ]{#lbAC}

[comment]: # ({/new-c7ba44c5})

[comment]: # ({new-6590818d})
## SYNOPSIS

**zabbix\_proxy** \[**-c** *config-file*\]\
**zabbix\_proxy** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_proxy -h**\
**zabbix\_proxy -V** [ ]{#lbAD}

[comment]: # ({/new-6590818d})

[comment]: # ({new-5c6b9587})
## DESCRIPTION

**zabbix\_proxy** is a daemon that collects monitoring data from devices
and sends it to Zabbix server. [ ]{#lbAE}

[comment]: # ({/new-5c6b9587})

[comment]: # ({new-daee5c55})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one. Path to the
file should be specified.

**-f**, **--foreground**  
Run Zabbix proxy in foreground.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

[ ]{#lbAF}

[comment]: # ({/new-daee5c55})

[comment]: # ({new-045894c6})
### 

  
Runtime control options

  
**config\_cache\_reload**  
Reload configuration cache. Ignored if cache is being currently loaded.
Active Zabbix proxy will connect to the Zabbix server and request
configuration data. Default configuration file (unless **-c** option is
specified) will be used to find PID file and signal will be sent to
process, listed in PID file.

  
**housekeeper\_execute**  
Execute the housekeeper. Ignored if housekeeper is being currently
executed.

  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-045894c6})

[comment]: # ({new-b863520e})
### 

  
Log level control targets

  
*pid*  
Process identifier

  
*process-type*  
All processes of specified type (e.g., poller)

  
*process-type,N*  
Process type and number (e.g., poller,3)

```{=html}
<!-- -->
```
**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAH}

[comment]: # ({/new-b863520e})

[comment]: # ({new-583a1725})
## FILES

*/usr/local/etc/zabbix\_proxy.conf*  
Default location of Zabbix proxy configuration file (if not modified
during compile time).

[ ]{#lbAI}

[comment]: # ({/new-583a1725})

[comment]: # ({new-41feb9bb})
## SEE ALSO

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAJ}

[comment]: # ({/new-41feb9bb})

[comment]: # ({new-e55ed07e})
## AUTHOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-9f3a9d2a})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

[](#lbAG)

  

[FILES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created by man2html, using the manual pages.\
Time: 09:10:13 GMT, January 19, 2016

[comment]: # ({/new-9f3a9d2a})
