[comment]: # translation:outdated

[comment]: # ({new-50528bfc})
# zabbix\_js

Section: User Commands (1)\
Updated: 2019-01-29\
[Index](#index) [Return to Main Contents](/documentation/5.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-50528bfc})

[comment]: # ({new-42249a0d})
## NAME

zabbix\_js - Zabbix JS utility [ ]{#lbAC}

[comment]: # ({/new-42249a0d})

[comment]: # ({new-8c5d99cc})
## SYNOPSIS

**zabbix\_js -s** *script-file* **-p** *input-param* \[**-l**
*log-level*\] \[**-t** *timeout*\]\
**zabbix\_js -s** *script-file* **-i** *input-file* \[**-l**
*log-level*\] \[**-t** *timeout*\]\
**zabbix\_js -h**\
**zabbix\_js -V** [ ]{#lbAD}

[comment]: # ({/new-8c5d99cc})

[comment]: # ({new-b7046c11})
## DESCRIPTION

**zabbix\_js** is a command line utility that can be used for embedded
script testing. [ ]{#lbAE}

[comment]: # ({/new-b7046c11})

[comment]: # ({new-02064894})
## OPTIONS

**-s**, **--script** *script-file*  
Specify the file name of the script to execute. If '-' is specified as
file name, the script will be read from stdin.

**-p**, **--param** *input-param*  
Specify the input parameter.

**-i**, **--input** *input-file*  
Specify the file name of the input parameter. If '-' is specified as
file name, the input will be read from stdin.

**-l**, **--loglevel** *log-level*  
Specify the log level.

**-t**, **--timeout** *timeout*  
Specify the timeout in seconds.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAF}

[comment]: # ({/new-02064894})

[comment]: # ({new-74bea5ed})
## EXAMPLES

**zabbix\_js -s script-file.js -p example** [ ]{#lbAG}

[comment]: # ({/new-74bea5ed})

[comment]: # ({new-f909573a})
## SEE ALSO

Documentation <https://www.zabbix.com/manuals>

**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8)

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-f909573a})

[comment]: # ({new-af76d2f6})
## Index

[NAME](#lbAB)  

[SYNOPSIS](#lbAC)  

[DESCRIPTION](#lbAD)  

[OPTIONS](#lbAE)  

[EXAMPLES](#lbAF)  

[SEE ALSO](#lbAG)  

------------------------------------------------------------------------

This document was created by [man2html](/cgi-bin/man/man2html), using
the manual pages.\
Time: 21:23:35 GMT, March 18, 2020

[comment]: # ({/new-af76d2f6})
