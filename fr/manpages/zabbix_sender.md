[comment]: # translation:outdated

[comment]: # ({new-4dfa5936})
# zabbix\_sender

Section: User Commands (1)\
Updated: 2015-10-16\
[Index](#index) [Return to Main Contents](/documentation/3.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-4dfa5936})

[comment]: # ({new-1fb3a028})
## NAME

zabbix\_sender - Zabbix sender utility [ ]{#lbAC}

[comment]: # ({/new-1fb3a028})

[comment]: # ({new-3f811652})
## SYNOPSIS

**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *port*\] \[**-I**
*IP-address*\] **-s** *host* **-k** *key* **-o** *value*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *port*\] \[**-I**
*IP-address*\] \[**-s** *host*\] \[**-T**\] \[**-r**\] **-i**
*input-file*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *port*\] \[**-I** *IP-address*\] \[**-s** *host*\] **-k** *key*
**-o** *value*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *port*\] \[**-I** *IP-address*\] \[**-s** *host*\] \[**-T**\]
\[**-r**\] **-i** *input-file*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *port*\] \[**-I**
*IP-address*\] **-s** *host* **--tls-connect** **cert**
**--tls-ca-file** *CA-file* \[**--tls-crl-file** *CRL-file*\]
\[**--tls-server-cert-issuer** *cert-issuer*\]
\[**--tls-server-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-file* **--tls-key-file** *key-file* **-k** *key* **-o** *value*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *port*\] \[**-I**
*IP-address*\] \[**-s** *host*\] **--tls-connect** **cert**
**--tls-ca-file** *CA-file* \[**--tls-crl-file** *CRL-file*\]
\[**--tls-server-cert-issuer** *cert-issuer*\]
\[**--tls-server-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-file* **--tls-key-file** *key-file* \[**-T**\] \[**-r**\] **-i**
*input-file*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *port*\] \[**-I** *IP-address*\] \[**-s** *host*\]
**--tls-connect** **cert** **--tls-ca-file** *CA-file*
\[**--tls-crl-file** *CRL-file*\] \[**--tls-server-cert-issuer**
*cert-issuer*\] \[**--tls-server-cert-subject** *cert-subject*\]
**--tls-cert-file** *cert-file* **--tls-key-file** *key-file* **-k**
*key* **-o** *value*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *port*\] \[**-I** *IP-address*\] \[**-s** *host*\]
**--tls-connect** **cert** **--tls-ca-file** *CA-file*
\[**--tls-crl-file** *CRL-file*\] \[**--tls-server-cert-issuer**
*cert-issuer*\] \[**--tls-server-cert-subject** *cert-subject*\]
**--tls-cert-file** *cert-file* **--tls-key-file** *key-file* \[**-T**\]
\[**-r**\] **-i** *input-file*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *port*\] \[**-I**
*IP-address*\] **-s** *host* **--tls-connect** **psk**
**--tls-psk-identity** *PSK-identity* **--tls-psk-file** *PSK-file*
**-k** *key* **-o** *value*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *port*\] \[**-I**
*IP-address*\] \[**-s** *host*\] **--tls-connect** **psk**
**--tls-psk-identity** *PSK-identity* **--tls-psk-file** *PSK-file*
\[**-T**\] \[**-r**\] **-i** *input-file*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *port*\] \[**-I** *IP-address*\] \[**-s** *host*\]
**--tls-connect** **psk** **--tls-psk-identity** *PSK-identity*
**--tls-psk-file** *PSK-file* **-k** *key* **-o** *value*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *port*\] \[**-I** *IP-address*\] \[**-s** *host*\]
**--tls-connect** **psk** **--tls-psk-identity** *PSK-identity*
**--tls-psk-file** *PSK-file* \[**-T**\] \[**-r**\] **-i** *input-file*\
**zabbix\_sender -h**\
**zabbix\_sender -V** [ ]{#lbAD}

[comment]: # ({/new-3f811652})

[comment]: # ({new-14895cca})
## DESCRIPTION

**zabbix\_sender** is a command line utility for sending monitoring data
to Zabbix server or proxy. On the Zabbix server an item of type **Zabbix
trapper** should be created with corresponding key. Note that incoming
values will only be accepted from hosts specified in **Allowed hosts**
field for this item. [ ]{#lbAE}

[comment]: # ({/new-14895cca})

[comment]: # ({new-cd7de714})
## OPTIONS

**-c**, **--config** *config-file*  
Use *config-file*. **Zabbix sender** reads server details from the
agentd configuration file. By default **Zabbix sender** does not read
any configuration file. Path to the file should be specified. Only
parameters **Hostname**, **ServerActive** and **SourceIP** are
supported. First entry from the **ServerActive** parameter is used.

**-z**, **--zabbix-server** *server*  
Hostname or IP address of Zabbix server. If a host is monitored by a
proxy, proxy hostname or IP address should be used instead. When used
together with **--config**, overrides the first entry of
**ServerActive** parameter specified in agentd configuration file.

**-p**, **--port** *port*  
Specify port number of Zabbix server trapper running on the server.
Default is 10051. When used together with **--config**, overrides the
port of first entry of **ServerActive** parameter specified in agentd
configuration file.

**-I**, **--source-address** *IP-address*  
Specify source IP address. When used together with **--config**,
overrides **SourceIP** parameter specified in agentd configuration file.

**-s**, **--host** *host*  
Specify host name the item belongs to (as registered in Zabbix
frontend). Host IP address and DNS name will not work. When used
together with **--config**, overrides **Hostname** parameter specified
in agentd configuration file.

**-k**, **--key** *key*  
Specify item key to send value to.

**-o**, **--value** *value*  
Specify item value.

**-i**, **--input-file** *input-file*  
Load values from input file. Specify **-** as **<input-file>** to
read values from standard input. Each line of file contains whitespace
delimited: **<hostname> <key> <value>**. Each value
must be specified on its own line. Each line must contain 3 whitespace
delimited entries: **<hostname> <key> <value>**, where
"hostname" is the name of monitored host as registered in Zabbix
frontend, "key" is target item key and "value" - the value to send.
Specify **-** as **<hostname>** to use hostname from agent
configuration file or from **--host** argument.

An example of a line of an input file:

**"Linux DB3" db.connections 43**

The value type must be correctly set in item configuration of Zabbix
frontend. Zabbix sender will send up to 250 values in one connection.
Contents of the input file must be in the UTF-8 encoding. All values
from the input file are sent in a sequential order top-down. Entries
must be formatted using the following rules:

  
  
•  
Quoted and non-quoted entries are supported.

•  
Double-quote is the quoting character.

•  
Entries with whitespace must be quoted.

•  
Double-quote and backslash characters inside quoted entry must be
escaped with a backslash.

•  
Escaping is not supported in non-quoted entries.

•  
Linefeed escape sequences (\\n) are supported in quoted strings.

•  
Linefeed escape sequences are trimmed from the end of an entry.

**-T**, **--with-timestamps**  
This option can be only used with **--input-file** option.

Each line of the input file must contain 4 whitespace delimited entries:
**<hostname> <key> <timestamp> <value>**.
Timestamp should be specified in Unix timestamp format. If target item
has triggers referencing it, all timestamps must be in an increasing
order, otherwise event calculation will not be correct.

An example of a line of the input file:

**"Linux DB3" db.connections 1429533600 43**

For more details please see option **--input-file**.

If a timestamped value is sent for a host that is in a “no data”
[maintenance](/documentation/4.0/manual/maintenance) type then this
value will be dropped however it is possible to send a timestamped value
in for an expired maintenance period and it will be accepted.

**-r**, **--real-time**  
Send values one by one as soon as they are received. This can be used
when reading from standard input.

**--tls-connect** *value*  
How to connect to server or proxy. Values:

[ ]{#lbAF}

[comment]: # ({/new-cd7de714})

[comment]: # ({new-4029bd0a})
### 

  
**unencrypted**  
connect without encryption

```{=html}
<!-- -->
```
  
**psk**  
connect using TLS and a pre-shared key

```{=html}
<!-- -->
```
  
**cert**  
connect using TLS and a certificate

```{=html}
<!-- -->
```
**--tls-ca-file** *CA-file*  
Full pathname of a file containing the top-level CA(s) certificates for
peer certificate verification.

**--tls-crl-file** *CRL-file*  
Full pathname of a file containing revoked certificates.

**--tls-server-cert-issuer** *cert-issuer*  
Allowed server certificate issuer.

**--tls-server-cert-subject** *cert-subject*  
Allowed server certificate subject.

**--tls-cert-file** *cert-file*  
Full pathname of a file containing the certificate or certificate chain.

**--tls-key-file** *key-file*  
Full pathname of a file containing the private key.

**--tls-psk-identity** *PSK-identity*  
PSK-identity string.

**--tls-psk-file** *PSK-file*  
Full pathname of a file containing the pre-shared key.

**-v**, **--verbose**  
Verbose mode, **-vv** for more details.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAG}

[comment]: # ({/new-4029bd0a})

[comment]: # ({new-965bb13b})
## EXIT STATUS

The exit status is 0 if the values were sent and all of them were
successfully processed by server. If data was sent, but processing of at
least one of the values failed, the exit status is 2. If data sending
failed, the exit status is 1.

[ ]{#lbAH}

[comment]: # ({/new-965bb13b})

[comment]: # ({new-9b276166})
## EXAMPLES

**zabbix\_sender -c /etc/zabbix/zabbix\_agentd.conf -k mysql.queries -o
342.45**\

  
Send **342.45** as the value for **mysql.queries** item of monitored
host. Use monitored host and Zabbix server defined in agent
configuration file.

**zabbix\_sender -c /etc/zabbix/zabbix\_agentd.conf -s "Monitored Host"
-k mysql.queries -o 342.45**\

  
Send **342.45** as the value for **mysql.queries** item of **Monitored
Host** host using Zabbix server defined in agent configuration file.

\
**zabbix\_sender -z 192.168.1.113 -i data\_values.txt**

  
\
Send values from file **data\_values.txt** to Zabbix server with IP
**192.168.1.113**. Host names and keys are defined in the file.

\
**echo "- hw.serial.number 1287872261 SQ4321ASDF" | zabbix\_sender -c
/usr/local/etc/zabbix\_agentd.conf -T -i -**\

  
Send a timestamped value from the commandline to Zabbix server,
specified in the agent configuration file. Dash in the input data
indicates that hostname also should be used from the same configuration
file.

\
**echo '"Zabbix server" trapper.item ""' | zabbix\_sender -z
192.168.1.113 -p 10000 -i -**\

  
Send empty value of an item to the Zabbix server with IP address
**192.168.1.113** on port **10000** from the commandline. Empty values
must be indicated by empty double quotes.

**zabbix\_sender -z 192.168.1.113 -s "Monitored Host" -k mysql.queries
-o 342.45 --tls-connect cert --tls-ca-file /home/zabbix/zabbix\_ca\_file
--tls-cert-file /home/zabbix/zabbix\_agentd.crt --tls-key-file
/home/zabbix/zabbix\_agentd.key**\

  
Send **342.45** as the value for **mysql.queries** item in **Monitored
Host** host to server with IP **192.168.1.113** using TLS with
certificate.

**zabbix\_sender -z 192.168.1.113 -s "Monitored Host" -k mysql.queries
-o 342.45 --tls-connect psk --tls-psk-identity "PSK ID Zabbix agentd"
--tls-psk-file /home/zabbix/zabbix\_agentd.psk**\

  
Send **342.45** as the value for **mysql.queries** item in **Monitored
Host** host to server with IP **192.168.1.113** using TLS with
pre-shared key (PSK).

[ ]{#lbAI}

[comment]: # ({/new-9b276166})

[comment]: # ({new-554afaaf})
## SEE ALSO

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAI}

[comment]: # ({/new-554afaaf})

[comment]: # ({new-c84769dd})
## AUTHOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-c84769dd})

[comment]: # ({new-0c5a6f3e})
## Index

[NAME](#lbAB)  

[SYNOPSIS](#lbAC)  

[DESCRIPTION](#lbAD)  

[OPTIONS](#lbAE)  

[EXIT STATUS](#lbAF)  

[EXAMPLES](#lbAG)  

[SEE ALSO](#lbAH)  

[AUTHOR](#lbAI)  

------------------------------------------------------------------------

This document was created by man2html, using the manual pages.\
Time: 09:21:17 GMT, January 08, 2016

[comment]: # ({/new-0c5a6f3e})
