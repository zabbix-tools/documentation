[comment]: # translation:outdated

[comment]: # ({new-fdbd84bb})
# zabbix\_server

Section: Maintenance Commands (8)\
Updated: 2016-01-13\
[Index](#index) [Return to Main Contents](/documentation/3.0/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}

[comment]: # ({/new-fdbd84bb})

[comment]: # ({new-1090a00e})
## NAME

zabbix\_server - Zabbix server daemon [ ]{#lbAC}

[comment]: # ({/new-1090a00e})

[comment]: # ({new-2a5507db})
## SYNOPSIS

**zabbix\_server** \[**-c** *config-file*\]\
**zabbix\_server** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_server -h**\
**zabbix\_server -V** [ ]{#lbAD}

[comment]: # ({/new-2a5507db})

[comment]: # ({new-f39aa640})
## DESCRIPTION

**zabbix\_server** is the core daemon of Zabbix software. [ ]{#lbAE}

[comment]: # ({/new-f39aa640})

[comment]: # ({new-455125db})
## OPTIONS

**-c**, **--config** *config-file*  
Use the alternate *config-file* instead of the default one. Path to the
file should be specified.

**-f**, **--foreground**  
Run Zabbix server in foreground.

**-R**, **--runtime-control** *runtime-option*  
Perform administrative functions according to *runtime-option*.

[ ]{#lbAF}

[comment]: # ({/new-455125db})

[comment]: # ({new-d7d7f728})
### 

  
Runtime control options

  
**config\_cache\_reload**  
Reload configuration cache. Ignored if cache is being currently loaded.
Default configuration file (unless **-c** option is specified) will be
used to find PID file and signal will be sent to process, listed in PID
file.

  
**housekeeper\_execute**  
Execute the housekeeper. Ignored if housekeeper is being currently
executed.

  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-d7d7f728})

[comment]: # ({new-800a1525})

**ha\_remove\_node**\[=*target*\]  
Remove the high availability (HA) node specified by its name or ID.
Note that active/standby nodes cannot be removed.

**ha\_set\_failover\_delay**\[=*delay*\]  
Set high availability (HA) failover delay.
Time suffixes are supported, e.g. 10s, 1m.

**secrets\_reload**  
Reload secrets from Vault.

**service\_cache\_reload**  
Reload the service manager cache.

**snmp\_cache\_reload**  
Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.

**prof\_enable**\[=*target*\]  
Enable profiling.
Affects all processes if target is not specified.
Enabled profiling provides details of all rwlocks/mutexes by function name.
Supported since Zabbix 6.0.13.

**prof\_disable**\[=*target*\]  
Disable profiling.
Affects all processes if target is not specified.
Supported since Zabbix 6.0.13.
  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-800a1525})

[comment]: # ({new-858c64d1})
### 

  
Log level control targets

  
*pid*  
Process identifier

  
*process-type*  
All processes of specified type (e.g., poller)

  
*process-type,N*  
Process type and number (e.g., poller,3)

```{=html}
<!-- -->
```
**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAH}

[comment]: # ({/new-858c64d1})

[comment]: # ({new-00ee0d1e})
## FILES

*/usr/local/etc/zabbix\_server.conf*  
Default location of Zabbix server configuration file (if not modified
during compile time).

[ ]{#lbAI}

[comment]: # ({/new-00ee0d1e})

[comment]: # ({new-d095490c})
## SEE ALSO

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8) [ ]{#lbAJ}

[comment]: # ({/new-d095490c})

[comment]: # ({new-e55ed07e})
## AUTHOR

Alexei Vladishev <<alex@zabbix.com>>

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-e55ed07e})

[comment]: # ({new-b105a0f1})
## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

[](#lbAG)

  

[FILES](#lbAH)

[SEE ALSO](#lbAI)

[AUTHOR](#lbAJ)

------------------------------------------------------------------------

This document was created by man2html, using the manual pages.\
Time: 09:11:11 GMT, January 19, 2016

[comment]: # ({/new-b105a0f1})
