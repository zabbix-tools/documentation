<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="fr" datatype="plaintext" original="manpages/zabbix_get.md">
    <body>
      <trans-unit id="dabb0bd5" xml:space="preserve">
        <source># zabbix\_get

Section: User Commands (1)\
Updated: 2021-06-01\
[Index](#index) [Return to Main Contents](/manpages)

------------------------------------------------------------------------

[ ]{#lbAB}</source>
      </trans-unit>
      <trans-unit id="0ba1ce53" xml:space="preserve">
        <source>## NAME

zabbix\_get - Zabbix get utility [ ]{#lbAC}</source>
      </trans-unit>
      <trans-unit id="60e7e93b" xml:space="preserve">
        <source>## SYNOPSIS

**zabbix\_get -s** *host-name-or-IP* \[**-p** *port-number*\] \[**-I**
*IP-address*\] \[**-t** *timeout*\] **-k** *item-key*\
**zabbix\_get -s** *host-name-or-IP* \[**-p** *port-number*\] \[**-I**
*IP-address*\] \[**-t** *timeout*\] **--tls-connect** **cert**
**--tls-ca-file** *CA-file* \[**--tls-crl-file** *CRL-file*\]
\[**--tls-agent-cert-issuer** *cert-issuer*\]
\[**--tls-agent-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-file* **--tls-key-file** *key-file* \[**--tls-cipher13**
*cipher-string*\] \[**--tls-cipher** *cipher-string*\] **-k**
*item-key*\
**zabbix\_get -s** *host-name-or-IP* \[**-p** *port-number*\] \[**-I**
*IP-address*\] \[**-t** *timeout*\] **--tls-connect** **psk**
**--tls-psk-identity** *PSK-identity* **--tls-psk-file** *PSK-file*
\[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cipher-string*\] **-k** *item-key*\
**zabbix\_get -h**\
**zabbix\_get -V** [ ]{#lbAD}</source>
      </trans-unit>
      <trans-unit id="da08bc1a" xml:space="preserve">
        <source>## DESCRIPTION

**zabbix\_get** is a command line utility for getting data from Zabbix
agent. [ ]{#lbAE}</source>
      </trans-unit>
      <trans-unit id="4ab1a759" xml:space="preserve">
        <source>## OPTIONS

**-s**, **--host** *host-name-or-IP*  
Specify host name or IP address of a host.

**-p**, **--port** *port-number*  
Specify port number of agent running on the host. Default is 10050.

**-I**, **--source-address** *IP-address*  
Specify source IP address.

**-t**, **--timeout** *seconds*  
Specify timeout. Valid range: 1-30 seconds (default: 30)

**-k**, **--key** *item-key*  
Specify key of item to retrieve value for.

**--tls-connect** *value*  
How to connect to agent. Values:

[ ]{#lbAF}</source>
      </trans-unit>
      <trans-unit id="465bc4c8" xml:space="preserve">
        <source>### 

  
**unencrypted**  
connect without encryption (default)

```{=html}
&lt;!-- --&gt;
```
  
**psk**  
connect using TLS and a pre-shared key

```{=html}
&lt;!-- --&gt;
```
  
**cert**  
connect using TLS and a certificate

```{=html}
&lt;!-- --&gt;
```
**--tls-ca-file** *CA-file*  
Full pathname of a file containing the top-level CA(s) certificates for
peer certificate verification.

**--tls-crl-file** *CRL-file*  
Full pathname of a file containing revoked certificates.

**--tls-agent-cert-issuer** *cert-issuer*  
Allowed agent certificate issuer.

**--tls-agent-cert-subject** *cert-subject*  
Allowed agent certificate subject.

**--tls-cert-file** *cert-file*  
Full pathname of a file containing the certificate or certificate chain.

**--tls-key-file** *key-file*  
Full pathname of a file containing the private key.

**--tls-psk-identity** *PSK-identity*  
PSK-identity string.

**--tls-psk-file** *PSK-file*  
Full pathname of a file containing the pre-shared key.

**--tls-cipher13** *cipher-string*  
Cipher string for OpenSSL 1.1.1 or newer for TLS 1.3. Override the
default ciphersuite selection criteria. This option is not available if
OpenSSL version is less than 1.1.1.

**--tls-cipher** *cipher-string*  
GnuTLS priority string (for TLS 1.2 and up) or OpenSSL cipher string
(only for TLS 1.2). Override the default ciphersuite selection criteria.

**-h**, **--help**  
Display this help and exit.

**-V**, **--version**  
Output version information and exit.

[ ]{#lbAG}</source>
      </trans-unit>
      <trans-unit id="c5e4d00b" xml:space="preserve">
        <source>## EXAMPLES

**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"**\
**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect cert --tls-ca-file /home/zabbix/zabbix\_ca\_file
--tls-agent-cert-issuer "CN=Signing CA,OU=IT operations,O=Example
Corp,DC=example,DC=com" --tls-agent-cert-subject "CN=server1,OU=IT
operations,O=Example Corp,DC=example,DC=com" --tls-cert-file
/home/zabbix/zabbix\_get.crt --tls-key-file
/home/zabbix/zabbix\_get.key\
zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect psk --tls-psk-identity "PSK ID Zabbix agentd"
--tls-psk-file /home/zabbix/zabbix\_agentd.psk** [ ]{#lbAH}</source>
      </trans-unit>
      <trans-unit id="0ed7cca7" xml:space="preserve">
        <source>## SEE ALSO

Documentation &lt;https://www.zabbix.com/manuals&gt;

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_web\_service](zabbix_web_service)**(8) [ ]{#lbAI}</source>
      </trans-unit>
      <trans-unit id="aaed5529" xml:space="preserve">
        <source>## AUTHOR

Alexei Vladishev &lt;&lt;alex@zabbix.com&gt;&gt;

------------------------------------------------------------------------

[ ]{#index}</source>
      </trans-unit>
      <trans-unit id="bdb17437" xml:space="preserve">
        <source>## Index

[NAME](#lbAB)

[SYNOPSIS](#lbAC)

[DESCRIPTION](#lbAD)

[OPTIONS](#lbAE)

[](#lbAF)

  

[EXAMPLES](#lbAG)

[SEE ALSO](#lbAH)

[AUTHOR](#lbAI)

------------------------------------------------------------------------

This document was created on: 08:42:29 GMT, June 11, 2021</source>
      </trans-unit>
    </body>
  </file>
</xliff>
