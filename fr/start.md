[comment]: # translation:outdated

[comment]: # ({083ea49b-63820343})
# Documentation Zabbix 

Ces pages contiennent la documentation officielle de Zabbix.

Utilisez la barre de navigation latérale pour parcourir les pages de documentation.

Pour pouvoir regarder des pages, connectez-vous avec votre nom d'utilisateur et mot de passe des [forums Zabbix](http://www.zabbix.com/forum/).

[comment]: # ({/083ea49b-63820343})

