[comment]: # translation:outdated

[comment]: # ({new-734ebac5})
# 9. Web monitoring

[comment]: # ({/new-734ebac5})

[comment]: # ({new-f37bc18c})
#### Overview

With Zabbix you can check several availability aspects of web sites.\

::: noteimportant
To perform web monitoring Zabbix server must be
initially [configured](/manual/installation/install#from_the_sources)
with cURL (libcurl) support.
:::

To activate web monitoring you need to define web scenarios. A web
scenario consists of one or several HTTP requests or "steps". The steps
are periodically executed by Zabbix server in a pre-defined order. If a
host is monitored by proxy, the steps are executed by the proxy.

Web scenarios are attached to hosts/templates in the same way as items,
triggers, etc. That means that web scenarios can also be created on a
template level and then applied to multiple hosts in one move.

The following information is collected in any web scenario:

-   average download speed per second for all steps of whole scenario
-   number of the step that failed
-   last error message

The following information is collected in any web scenario step:

-   download speed per second
-   response time
-   response code

For more details, see [web monitoring
items](/manual/web_monitoring/items).

Data collected from executing web scenarios is kept in the database. The
data is automatically used for graphs, triggers and notifications.

Zabbix can also check if a retrieved HTML page contains a pre-defined
string. It can execute a simulated login and follow a path of simulated
mouse clicks on the page.

Zabbix web monitoring supports both HTTP and HTTPS. When running a web
scenario, Zabbix will optionally follow redirects (see option *Follow
redirects* below). Maximum number of redirects is hard-coded to 10
(using cURL option
[CURLOPT\_MAXREDIRS](http://curl.haxx.se/libcurl/c/CURLOPT_MAXREDIRS.html)).
All cookies are preserved during the execution of a single scenario.

See also [known issues](/manual/installation/known_issues#https_checks)
for web monitoring using HTTPS protocol.

[comment]: # ({/new-f37bc18c})

[comment]: # ({new-f007debe})
#### Configuring a web scenario

To configure a web scenario:

-   Go to: *Configuration → Hosts* (or *Templates*)
-   Click on *Web* in the row of the host/template
-   Click on *Create scenario* to the right (or on the scenario name to
    edit an existing scenario)
-   Enter parameters of the scenario in the form

The **Scenario** tab allows you to configure the general parameters of a
web scenario.

![](../../assets/en/manual/config/scenario.png)

All mandatory input fields are marked with a red asterisk.

Scenario parameters:

|Parameter|Description|
|---------|-----------|
|*Host*|Name of the host/template that the scenario belongs to.|
|*Name*|Unique scenario name.|
|*Update interval*|How often the scenario will be executed.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 2h, 1d.<br>[User macros](/manual/config/macros/user_macros) are supported. *Note* that if a user macro is used and its value is changed (e.g. 5m → 30s), the next check will be executed according to the previous value (farther in the future with the example values).|
|*Attempts*|The number of attempts for executing web scenario steps. In case of network problems (timeout, no connectivity, etc) Zabbix can repeat executing a step several times. The figure set will equally affect each step of the scenario. Up to 10 attempts can be specified, default value is 1.<br>*Note*: Zabbix will not repeat a step because of a wrong response code or the mismatch of a required string.|
|*Agent*|Select a client agent.<br>Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.<br>User macros can be used in this field.|
|*HTTP proxy*|You can specify an HTTP proxy to use, using the format `[protocol://][username[:password]@]proxy.example.com[:port]`.<br>This sets the [CURLOPT\_PROXY](https://curl.haxx.se/libcurl/c/CURLOPT_PROXY.html) cURL option.<br>The optional `protocol://` prefix may be used to specify alternative proxy protocols (the protocol prefix support was added in cURL 7.21.7). With no protocol specified, the proxy will be treated as an HTTP proxy.<br>By default, 1080 port will be used.<br>If specified, the proxy will overwrite proxy related environment variables like http\_proxy, HTTPS\_PROXY. If not specified, the proxy will not overwrite proxy-related environment variables. The entered value is passed on "as is", no sanity checking takes place.<br>You may also enter a SOCKS proxy address. If you specify the wrong protocol, the connection will fail and the item will become unsupported.<br>*Note* that only simple authentication is supported with HTTP proxy.<br>User macros can be used in this field.|
|*Variables*|Variables that may be used in scenario steps (URL, post variables).<br>They have the following format:<br>**{macro1}**=value1<br>**{macro2}**=value2<br>**{macro3}**=regex:<regular expression><br>For example:<br>{username}=Alexei<br>{password}=kj3h5kJ34bd<br>{hostid}=regex:hostid is (\[0-9\]+)<br>The macros can then be referenced in the steps as {username}, {password} and {hostid}. Zabbix will automatically replace them with actual values. Note that variables with `regex:` need one step to get the value of the regular expression so the extracted value can only be applied to the step after.<br>If the value part starts with `regex:` then the part after it is treated as a regular expression that searches the web page and, if found, stores the match in the variable. At least one subgroup must be present so that the matched value can be extracted.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.<br>Variables are automatically URL-encoded when used in query fields or form data for post variables, but must be URL-encoded manually when used in raw post or directly in URL.|
|*Headers*|Custom HTTP headers that will be sent when performing a request.<br>Headers should be listed using the same syntax as they would appear in the HTTP protocol, optionally using some additional features supported by the [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL option.<br>For example:<br>Accept-Charset=utf-8<br>Accept-Language=en-US<br>Content-Type=application/xml; charset=utf-8<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.|
|*Enabled*|The scenario is active if this box is checked, otherwise - disabled.|

Note that when editing an existing scenario, two extra buttons are
available in the form:

|   |   |
|---|---|
|![](../../assets/en/manual/web_monitoring/buttons_clone.png)|Create another scenario based on the properties of the existing one.|
|![](../../assets/en/manual/web_monitoring/buttons_clear.png)|Delete history and trend data for the scenario. This will make the server perform the scenario immediately after deleting the data.|

::: notetip
If *HTTP proxy* field is left empty, another way for
using an HTTP proxy is to set proxy related environment variables.

For HTTP checks - set the **http\_proxy** environment variable for the
Zabbix server user. For example,
`http_proxy=http://proxy_ip:proxy_port`.

For HTTPS checks - set the **HTTPS\_PROXY** environment variable. For
example, `HTTPS_PROXY=http://proxy_ip:proxy_port`. More details are
available by running a shell command: *\# man curl*.
:::

The **Steps** tab allows you to configure the web scenario steps. To add
a web scenario step, click on *Add* in the *Steps* block.

![](../../assets/en/manual/config/scenario_b.png){width="600"}

::: noteclassic
Secret [user
macros](/manual/config/macros/user_macros#configuration) must not be
used in URLs as they will resolve to "\*\*\*\*\*\*".
:::

[comment]: # ({/new-f007debe})

[comment]: # ({new-74c8baab})
#### Configuring steps

![](../../assets/en/manual/config/scenario_step.png){width="600"}

Step parameters:

|Parameter|Description|
|---------|-----------|
|*Name*|Unique step name.|
|*URL*|URL to connect to and retrieve data. For example:<br>https://www.example.com<br>http://www.example.com/download<br>Domain names can be specified in Unicode characters. They are automatically punycode-converted to ASCII when executing the web scenario step.<br>The *Parse* button can be used to separate optional query fields (like ?name=Admin&password=mypassword) from the URL, moving the attributes and values into *Query fields* for automatic URL-encoding.<br>Variables can be used in the URL, using the {macro} syntax. Variables can be URL-encoded manually using a {{macro}.urlencode()} syntax.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.<br>Limited to 2048 characters.|
|*Query fields*|HTTP GET variables for the URL.<br>Specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from scenario variables, user macros or {HOST.\*} macros are resolved and then URL-encoded automatically. Using a {{macro}.urlencode()} syntax will double URL-encode them.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.|
|*Post*|HTTP POST variables.<br>In **Form data** mode, specified as attribute and value pairs.<br>Values are URL-encoded automatically. Values from scenario variables, user macros or {HOST.\*} macros are resolved and then URL-encoded automatically.<br>In **Raw data** mode, attributes/values are displayed on a single line and concatenated with a **&** symbol.<br>Raw values can be URL-encoded/decoded manually using a {{macro}.urlencode()} or {{macro}.urldecode()} syntax.<br>For example: id=2345&userid={user}<br>If {user} is defined as a variable of the web scenario, it will be replaced by its value when the step is executed. If you wish to URL-encode the variable, substitute {user} with {{user}.urlencode()}.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.|
|*Variables*|Step-level variables that may be used for GET and POST functions.<br>Specified as attribute and value pairs.<br>Step-level variables override scenario-level variables or variables from the previous step. However, the value of a step-level variable only affects the step after (and not the current step).<br>They have the following format:<br>**{macro}**=value<br>**{macro}**=regex:<regular expression><br>For more information see variable description on the [scenario](web_monitoring#configuring_a_web_scenario) level.<br>Variables are automatically URL-encoded when used in query fields or form data for post variables, but must be URL-encoded manually when used in raw post or directly in URL.|
|*Headers*|Custom HTTP headers that will be sent when performing a request.<br>Specified as attribute and value pairs.<br>Headers on the step level will overwrite the headers specified for the scenario.<br>For example, setting a 'User-Agent' attribute with no value will remove the User-Agent value set on scenario level.<br>User macros and {HOST.\*} macros are supported.<br>This sets the [CURLOPT\_HTTPHEADER](http://curl.haxx.se/libcurl/c/CURLOPT_HTTPHEADER.html) cURL option.|
|*Follow redirects*|Mark the checkbox to follow HTTP redirects.<br>This sets the [CURLOPT\_FOLLOWLOCATION](http://curl.haxx.se/libcurl/c/CURLOPT_FOLLOWLOCATION.html) cURL option.|
|*Retrieve mode*|Select the retrieve mode:<br>**Body** - retrieve only body from the HTTP response<br>**Headers** - retrieve only headers from the HTTP response<br>**Body and headers** - retrieve body and headers from the HTTP response|
|*Timeout*|Zabbix will not spend more than the set amount of time on processing the URL (from one second to maximum of 1 hour). Actually this parameter defines the maximum time for making connection to the URL and maximum time for performing an HTTP request. Therefore, Zabbix will not spend more than **2 x Timeout** seconds on the step.<br>[Time suffixes](/manual/appendix/suffixes) are supported, e.g. 30s, 1m, 1h. [User macros](/manual/config/macros/user_macros) are supported.|
|*Required string*|Required regular expression pattern.<br>Unless retrieved content (HTML) matches the required pattern the step will fail. If empty, no check on required string is performed.<br>For example:<br>Homepage of Zabbix<br>Welcome.\*admin<br>*Note*: Referencing [regular expressions](regular_expressions) created in the Zabbix frontend is not supported in this field.<br>User macros and {HOST.\*} [macros](/manual/appendix/macros/supported_by_location) are supported.|
|*Required status codes*|List of expected HTTP status codes. If Zabbix gets a code which is not in the list, the step will fail.<br>If empty, no check on status codes is performed.<br>For example: 200,201,210-299<br>User macros are supported.|

::: noteclassic
Any changes in web scenario steps will only be saved when
the whole scenario is saved.
:::

See also a [real-life example](/manual/web_monitoring/example) of how
web monitoring steps can be configured.

[comment]: # ({/new-74c8baab})

[comment]: # ({new-6f40ea68})
#### Configuring tags

The **Tags** tab allows to define scenario-level
[tags](/manual/config/tagging).

![](../../assets/en/manual/config/scenario_c.png)

Tagging allows to filter web scenarios and web monitoring
[items](/manual/web_monitoring/items).

[comment]: # ({/new-6f40ea68})

[comment]: # ({new-4cce2d90})
#### Configuring authentication

The **Authentication** tab allows you to configure scenario
authentication options. A green dot next to the tab name indicates that
some type of HTTP authentication is enabled.

![](../../assets/en/manual/config/scenario_d.png)

Authentication parameters:

|Parameter|Description|
|---------|-----------|
|*Authentication*|Authentication options.<br>**None** - no authentication used.<br>**Basic** - basic authentication is used.<br>**NTLM** - NTLM ([Windows NT LAN Manager)](http://en.wikipedia.org/wiki/NTLM) authentication is used.<br>**Kerberos** - Kerberos authentication is used. See also: [Configuring Kerberos with Zabbix](/manual/appendix/items/kerberos).<br>**Digest** - Digest authentication is used.<br>Selecting an authentication method will provide two additional fields for entering a user name and password.<br>User macros can be used in user and password fields.|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the web server.<br>The server certificate will be automatically taken from system-wide certificate authority (CA) location. You can override the location of CA files using Zabbix server or proxy configuration parameter [SSLCALocation](/manual/appendix/config/zabbix_server).<br>This sets the [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html) cURL option.|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the web server certificate matches.<br>This sets the [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html) cURL option.|
|*SSL certificate file*|Name of the SSL certificate file used for client authentication. The certificate file must be in PEM^1^ format. If the certificate file contains also the private key, leave the *SSL key file* field empty. If the key is encrypted, specify the password in *SSL key password* field. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLCertLocation](/manual/appendix/config/zabbix_server).<br>`HOST.*` macros and user macros can be used in this field.<br>This sets the [CURLOPT\_SSLCERT](http://curl.haxx.se/libcurl/c/CURLOPT_SSLCERT.html) cURL option.|
|*SSL key file*|Name of the SSL private key file used for client authentication. The private key file must be in PEM^1^ format. The directory containing this file is specified by Zabbix server or proxy configuration parameter [SSLKeyLocation](/manual/appendix/config/zabbix_server).<br>`HOST.*` macros and user macros can be used in this field.<br>This sets the [CURLOPT\_SSLKEY](http://curl.haxx.se/libcurl/c/CURLOPT_SSLKEY.html) cURL option.|
|*SSL key password*|SSL private key file password.<br>User macros can be used in this field.<br>This sets the [CURLOPT\_KEYPASSWD](http://curl.haxx.se/libcurl/c/CURLOPT_KEYPASSWD.html) cURL option.|

::: noteimportant
 \[1\] Zabbix supports certificate and private key
files in PEM format only. In case you have your certificate and private
key data in PKCS \#12 format file (usually with extension \*.p12 or
\*.pfx) you may generate the PEM file from it using the following
commands:

    openssl pkcs12 -in ssl-cert.p12 -clcerts -nokeys -out ssl-cert.pem
    openssl pkcs12 -in ssl-cert.p12 -nocerts -nodes  -out ssl-cert.key


:::

::: noteclassic
 Zabbix server picks up changes in certificates without a
restart. 
:::

::: noteclassic
 If you have client certificate and private key in a single
file just specify it in a "SSL certificate file" field and leave "SSL
key file" field empty. The certificate and key must still be in PEM
format. Combining certificate and key is easy:

    cat client.crt client.key > client.pem


:::

[comment]: # ({/new-4cce2d90})

[comment]: # ({new-ae869848})
#### Display

To view web scenarios configured for a host, go to *Monitoring → Hosts*,
locate the host in the list and click on the *Web* hyperlink in the last
column. Click on the scenario name to get detailed information.

![](../../assets/en/manual/web_monitoring/scenario_details2.png){width="600"}

An overview of web scenarios can also be displayed in *Monitoring →
Dashboard* by a Web monitoring widget.

Recent results of the web scenario execution are available in the
*Monitoring → Latest data* section.

[comment]: # ({/new-ae869848})

[comment]: # ({new-42ab5787})
#### Extended monitoring

Sometimes it is necessary to log received HTML page content. This is
especially useful if some web scenario step fails. Debug level 5 (trace)
serves that purpose. This level can be set in
[server](/manual/appendix/config/zabbix_server) and
[proxy](/manual/appendix/config/zabbix_proxy) configuration files or
using a runtime control option (`-R log_level_increase="http poller,N"`,
where N is the process number). The following examples demonstrate how
extended monitoring can be started provided debug level 4 is already
set:

    Increase log level of all http pollers:
    shell> zabbix_server -R log_level_increase="http poller"

    Increase log level of second http poller:
    shell> zabbix_server -R log_level_increase="http poller,2"

If extended web monitoring is not required it can be stopped using the
`-R log_level_decrease` option.

[comment]: # ({/new-42ab5787})
