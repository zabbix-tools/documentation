[comment]: # translation:outdated

[comment]: # ({new-e2c1904c})
# 4 Installation from packages

[comment]: # ({/new-e2c1904c})

[comment]: # ({new-8d855b7c})
#### From Zabbix official repository

Zabbix SIA provides official RPM and DEB packages for:

-   [Red Hat Enterprise
    Linux/CentOS](/manual/installation/install_from_packages/rhel_centos)
-   [Debian/Ubuntu/Raspbian](/manual/installation/install_from_packages/debian_ubuntu)
-   [SUSE Linux Enterprise
    Server](/manual/installation/install_from_packages/suse)

Package files for yum/dnf, apt and zypper repositories for various OS
distributions are available at
[repo.zabbix.com](https://repo.zabbix.com/).

Note, that though some OS distributions (in particular, Debian-based
distributions) provide their own Zabbix packages, these packages are not
supported by Zabbix. Zabbix packages provided by 3rd parties can be out
of date and may lack the latest features and bug fixes. It is
recommended to use only official packages from
[repo.zabbix.com](https://repo.zabbix.com/). If you have previously used
unofficial Zabbix packages, see notes about [upgrading Zabbix packages
from OS
repositories](/manual/installation/upgrade/packages#zabbix_packages_from_os_repositories).

[comment]: # ({/new-8d855b7c})
