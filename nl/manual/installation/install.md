[comment]: # translation:outdated

[comment]: # ({new-af2eaf2f})
# 3 Installation from sources

You can get the very latest version of Zabbix by compiling it from the
sources.

A step-by-step tutorial for installing Zabbix from the sources is
provided here.

[comment]: # ({/new-af2eaf2f})

[comment]: # ({new-0040d992})
#### - Installing Zabbix daemons

[comment]: # ({/new-0040d992})

[comment]: # ({new-8cc2c1b1})
##### 1 Download the source archive

Go to the [Zabbix download page](http://www.zabbix.com/download_sources)
and download the source archive. Once downloaded, extract the sources,
by running:

    $ tar -zxvf zabbix-6.0.0.tar.gz

::: notetip
Enter the correct Zabbix version in the command. It must
match the name of the downloaded archive.
:::

[comment]: # ({/new-8cc2c1b1})

[comment]: # ({new-87e1101d})
##### 2 Create user account

For all of the Zabbix daemon processes, an unprivileged user is
required. If a Zabbix daemon is started from an unprivileged user
account, it will run as that user.

However, if a daemon is started from a 'root' account, it will switch to
a 'zabbix' user account, which must be present. To create such a user
account (in its own group, "zabbix"),

on a RedHat-based system, run:

    groupadd --system zabbix
    useradd --system -g zabbix -d /usr/lib/zabbix -s /sbin/nologin -c "Zabbix Monitoring System" zabbix

on a Debian-based system, run:

    addgroup --system --quiet zabbix
    adduser --quiet --system --disabled-login --ingroup zabbix --home /var/lib/zabbix --no-create-home zabbix

::: noteimportant
Zabbix processes do not need a home directory,
which is why we do not recommend creating it. However, if you are using
some functionality that requires it (e. g. store MySQL credentials in
`$HOME/.my.cnf`) you are free to create it using the following
commands.\
\

On RedHat-based systems, run:

    mkdir -m u=rwx,g=rwx,o= -p /usr/lib/zabbix
    chown zabbix:zabbix /usr/lib/zabbix

On Debian-based systems, run:

    mkdir -m u=rwx,g=rwx,o= -p /var/lib/zabbix
    chown zabbix:zabbix /var/lib/zabbix


:::

A separate user account is not required for Zabbix frontend
installation.

If Zabbix [server](/manual/concepts/server) and
[agent](/manual/concepts/agent) are run on the same machine it is
recommended to use a different user for running the server than for
running the agent. Otherwise, if both are run as the same user, the
agent can access the server configuration file and any Admin level user
in Zabbix can quite easily retrieve, for example, the database password.

::: noteimportant
Running Zabbix as `root`, `bin`, or any other
account with special rights is a security risk.
:::

[comment]: # ({/new-87e1101d})

[comment]: # ({new-06256db0})
##### 3 Create Zabbix database

For Zabbix [server](/manual/concepts/server) and
[proxy](/manual/concepts/proxy) daemons, as well as Zabbix frontend, a
database is required. It is not needed to run Zabbix
[agent](/manual/concepts/agent).

SQL [scripts are provided](/manual/appendix/install/db_scripts) for
creating database schema and inserting the dataset. Zabbix proxy
database needs only the schema while Zabbix server database requires
also the dataset on top of the schema.

Having created a Zabbix database, proceed to the following steps of
compiling Zabbix.

[comment]: # ({/new-06256db0})

[comment]: # ({new-8ff1b41c})
##### 4 Configure the sources

When configuring the sources for a Zabbix server or proxy, you must
specify the database type to be used. Only one database type can be
compiled with a server or proxy process at a time.

To see all of the supported configuration options, inside the extracted
Zabbix source directory run:

    ./configure --help

To configure the sources for a Zabbix server and agent, you may run
something like:

    ./configure --enable-server --enable-agent --with-mysql --enable-ipv6 --with-net-snmp --with-libcurl --with-libxml2 --with-openipmi

To configure the sources for a Zabbix server (with PostgreSQL etc.), you
may run:

    ./configure --enable-server --with-postgresql --with-net-snmp

To configure the sources for a Zabbix proxy (with SQLite etc.), you may
run:

    ./configure --prefix=/usr --enable-proxy --with-net-snmp --with-sqlite3 --with-ssh2

To configure the sources for a Zabbix agent, you may run:

    ./configure --enable-agent

or, for Zabbix agent 2:

    ./configure --enable-agent2

::: noteclassic
 A configured Go environment with a currently supported [Go
version](https://golang.org/doc/devel/release#policy) is required for
building Zabbix agent 2. See
[golang.org](https://golang.org/doc/install) for installation
instructions.
:::

Notes on compilation options:

-   Command-line utilities zabbix\_get and zabbix\_sender are compiled
    if --enable-agent option is used.
-   --with-libcurl and --with-libxml2 configuration options are required
    for virtual machine monitoring; --with-libcurl is also required for
    SMTP authentication and `web.page.*` Zabbix agent
    [items](/manual/config/items/itemtypes/zabbix_agent). Note that cURL
    7.20.0 or higher is [required](/manual/installation/requirements)
    with the --with-libcurl configuration option.
-   Zabbix always compiles with the PCRE library (since version 3.4.0);
    installing it is not optional. --with-libpcre=\[DIR\] only allows
    pointing to a specific base install directory, instead of searching
    through a number of common places for the libpcre files.
-   You may use the --enable-static flag to statically link libraries.
    If you plan to distribute compiled binaries among different servers,
    you must use this flag to make these binaries work without required
    libraries. Note that --enable-static does not work in
    [Solaris](https://docs.oracle.com/cd/E18659_01/html/821-1383/bkajp.html).
-   Using --enable-static option is not recommended when building
    server. In order to build the server statically, you must have a
    static version of every external library needed. There is no strict
    check for that in configure script.
-   Add optional path to the MySQL configuration file
    --with-mysql=/<path\_to\_the\_file>/mysql\_config to select
    the desired MySQL client library when there is a need to use one
    that is not located in the default location. It is useful when there
    are several versions of MySQL installed or MariaDB installed
    alongside MySQL on the same system.
-   Use --with-oracle flag to specify location of the OCI API.

::: noteimportant
 If ./configure fails due to missing libraries or
some other circumstance, please see the `config.log` file for more
details on the error. For example, if `libssl` is missing, the immediate
error message may be misleading:

    checking for main in -lmysqlclient... no
    configure: error: Not found mysqlclient library

While `config.log` has a more detailed description:

    /usr/bin/ld: cannot find -lssl
    /usr/bin/ld: cannot find -lcrypto


:::

See also:

-   [Compiling Zabbix with encryption
    support](/manual/encryption#compiling_zabbix_with_encryption_support)
    for encryption support
-   [Known
    issues](/manual/installation/known_issues#compiling_zabbix_agent_on_hp-ux)
    with compiling Zabbix agent on HP-UX

[comment]: # ({/new-8ff1b41c})

[comment]: # ({new-08edd3dc})
##### 5 Make and install everything

::: noteclassic
If installing from [Zabbix Git
repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), it
is required to run first:

`$ make dbschema` 
:::

    make install

This step should be run as a user with sufficient permissions (commonly
'root', or by using `sudo`).

Running `make install` will by default install the daemon binaries
(zabbix\_server, zabbix\_agentd, zabbix\_proxy) in /usr/local/sbin and
the client binaries (zabbix\_get, zabbix\_sender) in /usr/local/bin.

::: noteclassic
To specify a different location than /usr/local, use a
--prefix key in the previous step of configuring sources, for example
--prefix=/home/zabbix. In this case daemon binaries will be installed
under <prefix>/sbin, while utilities under <prefix>/bin. Man
pages will be installed under <prefix>/share.
:::

[comment]: # ({/new-08edd3dc})

[comment]: # ({new-112b74f7})
##### 6 Review and edit configuration files

-   edit the Zabbix agent configuration file
    **/usr/local/etc/zabbix\_agentd.conf**

You need to configure this file for every host with zabbix\_agentd
installed.

You must specify the Zabbix server **IP address** in the file.
Connections from other hosts will be denied.

-   edit the Zabbix server configuration file
    **/usr/local/etc/zabbix\_server.conf**

You must specify the database name, user and password (if using any).

The rest of the parameters will suit you with their defaults if you have
a small installation (up to ten monitored hosts). You should change the
default parameters if you want to maximize the performance of Zabbix
server (or proxy) though. See the [performance
tuning](/manual/appendix/performance_tuning) section for more details.

-   if you have installed a Zabbix proxy, edit the proxy configuration
    file **/usr/local/etc/zabbix\_proxy.conf**

You must specify the server IP address and proxy hostname (must be known
to the server), as well as the database name, user and password (if
using any).

::: noteclassic
With SQLite the full path to database file must be
specified; DB user and password are not required.
:::

[comment]: # ({/new-112b74f7})

[comment]: # ({new-bbdd82ac})
##### 7 Start up the daemons

Run zabbix\_server on the server side.

    shell> zabbix_server

::: noteclassic
Make sure that your system allows allocation of 36MB (or a
bit more) of shared memory, otherwise the server may not start and you
will see "Cannot allocate shared memory for <type of cache>." in
the server log file. This may happen on FreeBSD, Solaris 8.\
See the ["See also"](#see_also) section at the bottom of this page to
find out how to configure shared memory.
:::

Run zabbix\_agentd on all the monitored machines.

    shell> zabbix_agentd

::: noteclassic
Make sure that your system allows allocation of 2MB of
shared memory, otherwise the agent may not start and you will see
"Cannot allocate shared memory for collector." in the agent log file.
This may happen on Solaris 8.
:::

If you have installed Zabbix proxy, run zabbix\_proxy.

    shell> zabbix_proxy

[comment]: # ({/new-bbdd82ac})

[comment]: # ({new-c9f154ca})
#### - Installing Zabbix web interface

[comment]: # ({/new-c9f154ca})

[comment]: # ({new-ed4e56ce})
##### Copying PHP files

Zabbix frontend is written in PHP, so to run it a PHP supported
webserver is needed. Installation is done by simply copying the PHP
files from the ui directory to the webserver HTML documents directory.

Common locations of HTML documents directories for Apache web servers
include:

-   /usr/local/apache2/htdocs (default directory when installing Apache
    from source)
-   /srv/www/htdocs (OpenSUSE, SLES)
-   /var/www/html (Debian, Ubuntu, Fedora, RHEL, CentOS)

It is suggested to use a subdirectory instead of the HTML root. To
create a subdirectory and copy Zabbix frontend files into it, execute
the following commands, replacing the actual directory:

    mkdir <htdocs>/zabbix
    cd ui
    cp -a . <htdocs>/zabbix

If planning to use any other language than English, see [Installation of
additional frontend languages](/manual/appendix/install/locales) for
instructions.

[comment]: # ({/new-ed4e56ce})

[comment]: # ({new-63583cbb})
##### Installing frontend

Please see [Web interface installation](/manual/installation/frontend)
page for information about Zabbix frontend installation wizard.

[comment]: # ({/new-63583cbb})

[comment]: # ({new-1a519c06})
#### 3 Installing Java gateway

It is required to install Java gateway only if you want to monitor JMX
applications. Java gateway is lightweight and does not require a
database.

To install from sources, first
[download](/manual/installation/install#download_the_source_archive) and
extract the source archive.

To compile Java gateway, run the `./configure` script with
`--enable-java` option. It is advisable that you specify the `--prefix`
option to request installation path other than the default /usr/local,
because installing Java gateway will create a whole directory tree, not
just a single executable.

    $ ./configure --enable-java --prefix=$PREFIX

To compile and package Java gateway into a JAR file, run `make`. Note
that for this step you will need `javac` and `jar` executables in your
path.

    $ make

Now you have a zabbix-java-gateway-$VERSION.jar file in
src/zabbix\_java/bin. If you are comfortable with running Java gateway
from src/zabbix\_java in the distribution directory, then you can
proceed to instructions for configuring and running [Java
gateway](/manual/concepts/java#overview_of_files_in_java_gateway_distribution).
Otherwise, make sure you have enough privileges and run `make install`.

    $ make install

Proceed to [setup](/manual/concepts/java/from_sources) for more details
on configuring and running Java gateway.

[comment]: # ({/new-1a519c06})

[comment]: # ({new-76c01064})
#### 4 Installing Zabbix web service

Installing Zabbix web service is only required if you want to use
[scheduled
reports](/manual/web_interface/frontend_sections/reports/scheduled).

To install from sources, first
[download](/manual/installation/install#download_the_source_archive) and
extract the source archive.

To compile Zabbix web service, run the `./configure` script with
`--enable-webservice` option.

::: noteclassic
 A configured [Go](https://golang.org/doc/install) version
1.13+ environment is required for building Zabbix web service.

:::

[comment]: # ({/new-76c01064})

