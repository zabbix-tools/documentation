[comment]: # translation:outdated

[comment]: # ({new-83c2fbfb})
# 1 Red Hat Enterprise Linux

[comment]: # ({/new-83c2fbfb})

[comment]: # ({new-f5a2912b})
#### Overview

This section provides the steps required for a successful
[upgrade](/manual/installation/upgrade) from Zabbix **6.0**.x to Zabbix
**6.2**.x using official Zabbix packages for Red Hat Enterprise
Linux.

::: notewarning
Before the upgrade make sure to read the relevant
[**upgrade notes**](/manual/installation/upgrade)!
:::

You may also want to check the
[requirements](/manual/installation/requirements) for 6.2.

::: notetip
It may be handy to run two parallel SSH sessions during
the upgrade, executing the upgrade steps in one and monitoring the
server/proxy logs in another. For example, run
`tail -f zabbix_server.log` or `tail -f zabbix_proxy.log` in the second
SSH session showing you the latest log file entries and possible errors
in real time. This can be critical for production
instances.
:::

[comment]: # ({/new-f5a2912b})

[comment]: # ({new-93ff8b03})
#### Upgrade procedure

[comment]: # ({/new-93ff8b03})

[comment]: # ({new-165cddb5})
##### 1 Stop Zabbix processes

Stop Zabbix server to make sure that no new data is inserted into
database.

    # systemctl stop zabbix-server

If upgrading the proxy, stop proxy too.

    # systemctl stop zabbix-proxy

[comment]: # ({/new-165cddb5})

[comment]: # ({new-ab13a6a4})
##### 2 Back up the existing Zabbix database

This is a very important step. Make sure that you have a backup of your
database. It will help if the upgrade procedure fails (lack of disk
space, power off, any unexpected problem).

[comment]: # ({/new-ab13a6a4})

[comment]: # ({new-bddc3d2b})
##### 3 Back up configuration files, PHP files and Zabbix binaries

Make a backup copy of Zabbix binaries, configuration files and the PHP
file directory.

Configuration files:

    # mkdir /opt/zabbix-backup/
    # cp /etc/zabbix/zabbix_server.conf /opt/zabbix-backup/
    # cp /etc/httpd/conf.d/zabbix.conf  /opt/zabbix-backup/

PHP files and Zabbix binaries:

    # cp -R /usr/share/zabbix/ /opt/zabbix-backup/
    # cp -R /usr/share/zabbix-* /opt/zabbix-backup/

[comment]: # ({/new-bddc3d2b})

[comment]: # ({new-f9da6e1f})
##### 4 Update repository configuration package

To proceed with the upgrade your current repository package has to be
updated.

    # rpm -Uvh https://repo.zabbix.com/zabbix/6.2/rhel/8/x86_64/zabbix-release-6.2-1.el8.noarch.rpm

[comment]: # ({/new-f9da6e1f})

[comment]: # ({new-9524156f})
##### 5 Upgrade Zabbix components

To upgrade Zabbix components you may run something like:

    # yum upgrade zabbix-server-mysql zabbix-web-mysql zabbix-agent

If using PostgreSQL, substitute `mysql` with `pgsql` in the command. If
upgrading the proxy, substitute `server` with `proxy` in the command. If
upgrading the agent 2, substitute `zabbix-agent` with `zabbix-agent2` in
the command.

To upgrade the web frontend with Apache **on RHEL 8** correctly, also
run:

    # yum install zabbix-apache-conf 

and make the necessary
[changes](/manual/installation/install_from_packages/rhel_centos#zabbix_frontend_configuration)
to this file.

To upgrade the web frontend **on RHEL 7** follow [distribution-specific
instructions](/manual/installation/frontend/frontend_on_rhel7) (extra
steps are required to install PHP 7.2 or newer).

[comment]: # ({/new-9524156f})

[comment]: # ({new-f25076e1})
##### 6 Review component configuration parameters

See the upgrade notes for details on [mandatory
changes](/manual/installation/upgrade_notes_600#configuration_parameters).

[comment]: # ({/new-f25076e1})

[comment]: # ({new-3190fa72})
##### 7 Start Zabbix processes

Start the updated Zabbix components.

    # systemctl start zabbix-server
    # systemctl start zabbix-proxy
    # systemctl start zabbix-agent
    # systemctl start zabbix-agent2

[comment]: # ({/new-3190fa72})

[comment]: # ({new-49e4f43e})
##### 8 Clear web browser cookies and cache

After the upgrade you may need to clear web browser cookies and web
browser cache for the Zabbix web interface to work properly.

[comment]: # ({/new-49e4f43e})

[comment]: # ({new-ad85b70b})
#### Upgrade between minor versions

It is possible to upgrade between minor versions of 6.2.x (for example,
from 6.2.1 to 6.2.3). Upgrading between minor versions is easy.

To execute Zabbix minor version upgrade it is required to run:

    $ sudo yum upgrade 'zabbix-*'

To execute Zabbix server minor version upgrade run:

    $ sudo yum upgrade 'zabbix-server-*'

To execute Zabbix agent minor version upgrade run:

    $ sudo yum upgrade 'zabbix-agent-*'

or, for Zabbix agent 2:

    $ sudo yum upgrade 'zabbix-agent2-*'

Note that you may also use 'update' instead of 'upgrade' in these
commands. While 'upgrade' will delete obsolete packages, 'update' will
preserve them.

[comment]: # ({/new-ad85b70b})
