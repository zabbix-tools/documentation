[comment]: # translation:outdated

[comment]: # ({new-22c598e9})
# Building Zabbix agent 2 on Windows

[comment]: # ({/new-22c598e9})

[comment]: # ({new-84651997})
#### Overview

This section demonstrates how to build Zabbix agent 2 (Windows) from
sources.

[comment]: # ({/new-84651997})

[comment]: # ({new-2a4da91c})
#### Installing MinGW Compiler

1\. Download MinGW-w64 with SJLJ (set jump/long jump) Exception Handling
and Windows threads (for example
*x86\_64-8.1.0-release-win32-sjlj-rt\_v6-rev0.7z*)\
2. Extract and move to *c:\\mingw*\
3. Setup environmental variable

    @echo off
    set PATH=%PATH%;c:\mingw\bin
    cmd

When compiling use Windows prompt instead of MSYS terminal provided by
MinGW

[comment]: # ({/new-2a4da91c})

[comment]: # ({new-dec81522})
#### Compiling PCRE development libraries

The following instructions will compile and install 64-bit PCRE
libraries in *c:\\dev\\pcre* and 32-bit libraries in *c:\\dev\\pcre32*:

1\. Download PCRE library version 8.XX from pcre.org
(<http://ftp.pcre.org/pub/pcre/>) and extract\
2. Open *cmd* and navigate to the extracted sources

[comment]: # ({/new-dec81522})

[comment]: # ({new-5def516d})
##### Build 64bit PCRE

1\. Delete old configuration/cache if exists:

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

2\. Run cmake (CMake can be installed from
<https://cmake.org/download/>):

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-O2 -g" -DCMAKE_CXX_FLAGS="-O2 -g" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre

3\. Next, run:

    mingw32-make clean
    mingw32-make install

[comment]: # ({/new-5def516d})

[comment]: # ({new-04b79926})
##### Build 32bit PCRE

1\. Run:

    mingw32-make clean

2\. Delete *CMakeCache.txt*:

    del CMakeCache.txt
    rmdir /q /s CMakeFiles

3\. Run cmake:

    cmake -G "MinGW Makefiles" -DCMAKE_C_COMPILER=gcc -DCMAKE_C_FLAGS="-m32 -O2 -g" -DCMAKE_CXX_FLAGS="-m32 -O2 -g" -DCMAKE_EXE_LINKER_FLAGS="-Wl,-mi386pe" -DCMAKE_INSTALL_PREFIX=c:\dev\pcre32

4\. Next, run:

    mingw32-make install

[comment]: # ({/new-04b79926})

[comment]: # ({new-80e7f73a})
#### Installing OpenSSL development libraries

1\. Download 32 and 64 bit builds from <https://curl.se/windows/>\
2. Extract files into *c:\\dev\\openssl32* and *c:\\dev\\openssl*
directories accordingly.\
3. After that remove extracted *\*.dll.a* (dll call wrapper libraries)
as MinGW prioritizes them before static libraries.

[comment]: # ({/new-80e7f73a})

[comment]: # ({new-f4af7b47})
#### Compiling Zabbix agent 2

[comment]: # ({/new-f4af7b47})

[comment]: # ({new-5f096f18})
##### 32 bit

Open MinGW environment (Windows command prompt) and navigate to
*build/mingw* directory in the Zabbix source tree.

Run:

    mingw32-make clean
    mingw32-make ARCH=x86 PCRE=c:\dev\pcre32 OPENSSL=c:\dev\openssl32

[comment]: # ({/new-5f096f18})

[comment]: # ({new-f7876f1d})
##### 64 bit

Open MinGW environment (Windows command prompt) and navigate to
*build/mingw* directory in the Zabbix source tree.

Run:

    mingw32-make clean
    mingw32-make PCRE=c:\dev\pcre OPENSSL=c:\dev\openssl

::: noteclassic
 Both 32- and 64- bit versions can be built on a 64-bit
platform, but only a 32-bit version can be built on a 32-bit platform.
When working on the 32-bit platform, follow the same steps as for 64-bit
version on 64-bit platform. 
:::

[comment]: # ({/new-f7876f1d})
