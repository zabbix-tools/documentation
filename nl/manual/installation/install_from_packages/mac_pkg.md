[comment]: # translation:outdated

[comment]: # ({new-bfa3f768})
# 6 Mac OS agent installation from PKG

[comment]: # ({/new-bfa3f768})

[comment]: # ({new-5eec6c30})
#### Overview

Zabbix Mac OS agent can be installed from PKG installer packages
available for [download](https://www.zabbix.com/download_agents#tab:44).
Versions with or without encryption are available.

[comment]: # ({/new-5eec6c30})

[comment]: # ({new-3688af62})
#### Installing agent

The agent can be installed using the graphical user interface or from
the command line, for example:

    sudo installer -pkg zabbix_agent-5.4.0-macos-amd64-openssl.pkg -target /

Make sure to use the correct Zabbix package version in the command. It
must match the name of the downloaded package.

[comment]: # ({/new-3688af62})

[comment]: # ({new-052e551c})
#### Running agent

The agent will start automatically after installation or restart.

You may edit the configuration file at
`/usr/local/etc/zabbix/zabbix_agentd.conf` if necessary.

To start the agent manually, you may run:

    sudo launchctl start com.zabbix.zabbix_agentd

To stop the agent manually:

    sudo launchctl stop com.zabbix.zabbix_agentd

During upgrade, the existing configuration file is not overwritten.
Instead a new `zabbix_agentd.conf.NEW` file is created to be used for
reviewing and updating the existing configuration file, if necessary.
Remember to restart the agent after manual changes to the configuration
file.

[comment]: # ({/new-052e551c})

[comment]: # ({new-3150eaf4})
#### Troubleshooting and removing agent

This section lists some useful commands that can be used for
troubleshooting and removing Zabbix agent installation.

See if Zabbix agent is running:

    ps aux | grep zabbix_agentd

See if Zabbix agent has been installed from packages:

    $ pkgutil --pkgs | grep zabbix 
    com.zabbix.pkg.ZabbixAgent

See the files that were installed from the installer package (note that
the initial `/` is not displayed in this view):

    $ pkgutil --only-files --files com.zabbix.pkg.ZabbixAgent
    Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist                                                                                                                                                                                                                           
    usr/local/bin/zabbix_get                                                                                                                                                                                                                                                       
    usr/local/bin/zabbix_sender                                                                                                                                                                                                                                                    
    usr/local/etc/zabbix/zabbix_agentd/userparameter_examples.conf.NEW                                                                                                                                                                                                             
    usr/local/etc/zabbix/zabbix_agentd/userparameter_mysql.conf.NEW                                                                                                                                                                                                                
    usr/local/etc/zabbix/zabbix_agentd.conf.NEW                                                                                                                                                                                                                                    
    usr/local/sbin/zabbix_agentd

Stop Zabbix agent if it was launched with `launchctl`:

    sudo launchctl unload /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist

Remove files (including configuration and logs) that were installed with
installer package:

    sudo rm -f /Library/LaunchDaemons/com.zabbix.zabbix_agentd.plist
    sudo rm -f /usr/local/sbin/zabbix_agentd
    sudo rm -f /usr/local/bin/zabbix_get
    sudo rm -f /usr/local/bin/zabbix_sender
    sudo rm -rf /usr/local/etc/zabbix
    sudo rm -rf /var/log/zabbix

Forget that Zabbix agent has been installed:

    sudo pkgutil --forget com.zabbix.pkg.ZabbixAgent

[comment]: # ({/new-3150eaf4})
