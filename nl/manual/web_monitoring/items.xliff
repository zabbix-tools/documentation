<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/web_monitoring/items.md">
    <body>
      <trans-unit id="a3537eda" xml:space="preserve">
        <source># 1 Web monitoring items</source>
      </trans-unit>
      <trans-unit id="7f39438f" xml:space="preserve">
        <source>#### Overview

Some new items are automatically added for monitoring when web scenarios
are created.

All items inherit tags from the web scenario.</source>
      </trans-unit>
      <trans-unit id="6bf9e7b2" xml:space="preserve">
        <source>#### Scenario items

As soon as a scenario is created, Zabbix automatically adds the
following items for monitoring.

|Item|Description|
|----|-----------|
|*Download speed for scenario &lt;Scenario&gt;*|This item will collect information about the download speed (bytes per second) of the whole scenario, i.e. average for all steps.&lt;br&gt;Item key: web.test.in\[Scenario,,bps\]&lt;br&gt;Type: *Numeric(float)*|
|*Failed step of scenario &lt;Scenario&gt;*|This item will display the number of the step that failed on the scenario. If all steps are executed successfully, 0 is returned.&lt;br&gt;Item key: web.test.fail\[Scenario\]&lt;br&gt;Type: *Numeric(unsigned)*|
|*Last error message of scenario &lt;Scenario&gt;*|This item returns the last error message text of the scenario. A new value is stored only if the scenario has a failed step. If all steps are ok, no new value is collected.&lt;br&gt;Item key: web.test.error\[Scenario\]&lt;br&gt;Type: *Character*|

The actual scenario name will be used instead of "Scenario".

::: noteclassic
If the scenario name contains [user macros](/manual/config/macros/user_macros), these macros will be left unresolved in web monitoring item names.
&lt;br&gt;&lt;br&gt;
If the scenario name starts with a doublequote or contains a comma or a square bracket, it will be properly quoted in item keys.
In other cases no additional quoting will be performed.
:::

::: noteclassic
Web monitoring items are added with a 30 day history and a 90 day trend retention period.
:::

These items can be used to create triggers and define notification conditions.</source>
      </trans-unit>
      <trans-unit id="38a780ad" xml:space="preserve">
        <source>##### Example 1

To create a "Web scenario failed" trigger, you can define a trigger
expression:

    last(/host/web.test.fail[Scenario])&lt;&gt;0

Make sure to replace 'Scenario' with the real name of your scenario.</source>
      </trans-unit>
      <trans-unit id="1f198e97" xml:space="preserve">
        <source>##### Example 2

To create a "Web scenario failed" trigger with a useful problem
description in the trigger name, you can define a trigger with name:

    Web scenario "Scenario" failed: {ITEM.VALUE}

and trigger expression:

    length(last(/host/web.test.error[Scenario]))&gt;0 and last(/host/web.test.fail[Scenario])&gt;0

Make sure to replace 'Scenario' with the real name of your scenario.</source>
      </trans-unit>
      <trans-unit id="abd173df" xml:space="preserve">
        <source>##### Example 3

To create a "Web application is slow" trigger, you can define a trigger
expression:

    last(/host/web.test.in[Scenario,,bps])&lt;10000

Make sure to replace 'Scenario' with the real name of your scenario.</source>
      </trans-unit>
      <trans-unit id="fc2e76a2" xml:space="preserve">
        <source>#### Scenario step items

As soon as a step is created, Zabbix automatically adds the following
items for monitoring.

|Item|Description|
|----|-----------|
|*Download speed for step &lt;Step&gt; of scenario &lt;Scenario&gt;*|This item will collect information about the download speed (bytes per second) of the step.&lt;br&gt;Item key: web.test.in\[Scenario,Step,bps\]&lt;br&gt;Type: *Numeric(float)*|
|*Response time for step &lt;Step&gt; of scenario &lt;Scenario&gt;*|This item will collect information about the response time of the step in seconds. Response time is counted from the beginning of the request until all information has been transferred.&lt;br&gt;Item key: web.test.time\[Scenario,Step,resp\]&lt;br&gt;Type: *Numeric(float)*|
|*Response code for step &lt;Step&gt; of scenario &lt;Scenario&gt;*|This item will collect response codes of the step.&lt;br&gt;Item key: web.test.rspcode\[Scenario,Step\]&lt;br&gt;Type: *Numeric(unsigned)*|

Actual scenario and step names will be used instead of "Scenario" and
"Step" respectively.

::: noteclassic
Web monitoring items are added with a 30 day history and a
90 day trend retention period.
:::

::: noteclassic
If scenario name starts with a doublequote or contains comma
or square bracket, it will be properly quoted in item keys. In other
cases no additional quoting will be performed.
:::

These items can be used to create triggers and define notification
conditions. For example, to create a "Zabbix GUI login is too slow"
trigger, you can define a trigger expression:

    last(/zabbix/web.test.time[ZABBIX GUI,Login,resp])&gt;3</source>
      </trans-unit>
    </body>
  </file>
</xliff>
