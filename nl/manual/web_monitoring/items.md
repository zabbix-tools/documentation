[comment]: # translation:outdated

[comment]: # ({new-a3537eda})
# 1 Web monitoring items

[comment]: # ({/new-a3537eda})

[comment]: # ({new-7f39438f})
#### Overview

Some new items are automatically added for monitoring when web scenarios
are created.

All items inherit tags from the web scenario.

[comment]: # ({/new-7f39438f})

[comment]: # ({new-6bf9e7b2})
#### Scenario items

As soon as a scenario is created, Zabbix automatically adds the
following items for monitoring.

|Item|Description|
|----|-----------|
|*Download speed for scenario <Scenario>*|This item will collect information about the download speed (bytes per second) of the whole scenario, i.e. average for all steps.<br>Item key: web.test.in\[Scenario,,bps\]<br>Type: *Numeric(float)*|
|*Failed step of scenario <Scenario>*|This item will display the number of the step that failed on the scenario. If all steps are executed successfully, 0 is returned.<br>Item key: web.test.fail\[Scenario\]<br>Type: *Numeric(unsigned)*|
|*Last error message of scenario <Scenario>*|This item returns the last error message text of the scenario. A new value is stored only if the scenario has a failed step. If all steps are ok, no new value is collected.<br>Item key: web.test.error\[Scenario\]<br>Type: *Character*|

The actual scenario name will be used instead of "Scenario".

::: noteclassic
Web monitoring items are added with a 30 day history and a
90 day trend retention period.
:::

::: noteclassic
If scenario name starts with a doublequote or contains comma
or square bracket, it will be properly quoted in item keys. In other
cases no additional quoting will be performed.
:::

These items can be used to create triggers and define notification
conditions.

[comment]: # ({/new-6bf9e7b2})

[comment]: # ({new-38a780ad})
##### Example 1

To create a "Web scenario failed" trigger, you can define a trigger
expression:

    last(/host/web.test.fail[Scenario])<>0

Make sure to replace 'Scenario' with the real name of your scenario.

[comment]: # ({/new-38a780ad})

[comment]: # ({new-1f198e97})
##### Example 2

To create a "Web scenario failed" trigger with a useful problem
description in the trigger name, you can define a trigger with name:

    Web scenario "Scenario" failed: {ITEM.VALUE}

and trigger expression:

    length(last(/host/web.test.error[Scenario]))>0 and last(/host/web.test.fail[Scenario])>0

Make sure to replace 'Scenario' with the real name of your scenario.

[comment]: # ({/new-1f198e97})

[comment]: # ({new-abd173df})
##### Example 3

To create a "Web application is slow" trigger, you can define a trigger
expression:

    last(/host/web.test.in[Scenario,,bps])<10000

Make sure to replace 'Scenario' with the real name of your scenario.

[comment]: # ({/new-abd173df})

[comment]: # ({new-fc2e76a2})
#### Scenario step items

As soon as a step is created, Zabbix automatically adds the following
items for monitoring.

|Item|Description|
|----|-----------|
|*Download speed for step <Step> of scenario <Scenario>*|This item will collect information about the download speed (bytes per second) of the step.<br>Item key: web.test.in\[Scenario,Step,bps\]<br>Type: *Numeric(float)*|
|*Response time for step <Step> of scenario <Scenario>*|This item will collect information about the response time of the step in seconds. Response time is counted from the beginning of the request until all information has been transferred.<br>Item key: web.test.time\[Scenario,Step,resp\]<br>Type: *Numeric(float)*|
|*Response code for step <Step> of scenario <Scenario>*|This item will collect response codes of the step.<br>Item key: web.test.rspcode\[Scenario,Step\]<br>Type: *Numeric(unsigned)*|

Actual scenario and step names will be used instead of "Scenario" and
"Step" respectively.

::: noteclassic
Web monitoring items are added with a 30 day history and a
90 day trend retention period.
:::

::: noteclassic
If scenario name starts with a doublequote or contains comma
or square bracket, it will be properly quoted in item keys. In other
cases no additional quoting will be performed.
:::

These items can be used to create triggers and define notification
conditions. For example, to create a "Zabbix GUI login is too slow"
trigger, you can define a trigger expression:

    last(/zabbix/web.test.time[ZABBIX GUI,Login,resp])>3

[comment]: # ({/new-fc2e76a2})
