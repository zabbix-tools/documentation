<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/config/hosts/hostupdate.md">
    <body>
      <trans-unit id="1c6301ba" xml:space="preserve">
        <source># 3 Mass update</source>
      </trans-unit>
      <trans-unit id="7205743f" xml:space="preserve">
        <source>#### Overview

Sometimes you may want to change some attribute for a number of hosts at
once. Instead of opening each individual host for editing, you may use
the mass update function for that.</source>
      </trans-unit>
      <trans-unit id="929367ac" xml:space="preserve">
        <source>#### Using mass update

To mass-update some hosts, do the following:

-   Mark the checkboxes before the hosts you want to update in the [host
    list](/manual/web_interface/frontend_sections/data_collection/hosts)
-   Click on *Mass update* below the list
-   Navigate to the tab with required attributes (*Host*, *IPMI*,
    *Tags*, *Macros*, *Inventory*, *Encryption* or *Value mapping*)
-   Mark the checkboxes of any attribute to update and enter a new value
    for them

![](../../../../assets/en/manual/config/hosts/host_mass.png)

The following options are available when selecting the respective button
for **template** linkage update:

-   *Link* - specify which additional templates to link
-   *Replace* - specify which templates to link while unlinking any
    template that was linked to the hosts before
-   *Unlink* - specify which templates to unlink

To specify the templates to link/unlink start typing the template name
in the auto-complete field until a dropdown appears offering the
matching templates. Just scroll down to select the required template.

The *Clear when unlinking* option will allow to not only unlink any
previously linked templates, but also remove all elements inherited from
them (items, triggers, etc.).

The following options are available when selecting the respective button
for **host group** update:

-   *Add* - allows to specify additional host groups from the existing
    ones or enter completely new host groups for the hosts
-   *Replace* - will remove the host from any existing host groups and
    replace them with the one(s) specified in this field (existing or
    new host groups)
-   *Remove* - will remove specific host groups from hosts

These fields are auto-complete - starting to type in them offers a
dropdown of matching host groups. If the host group is new, it also
appears in the dropdown and it is indicated by *(new)* after the string.
Just scroll down to select.

![](../../../../assets/en/manual/config/hosts/host_mass_c.png)

![](../../../../assets/en/manual/config/hosts/host_mass_d.png)

User macros, {INVENTORY.\*} macros, {HOST.HOST}, {HOST.NAME},
{HOST.CONN}, {HOST.DNS}, {HOST.IP}, {HOST.PORT} and {HOST.ID} macros are
supported in tags. Note, that tags with the same name, but different
values are not considered 'duplicates' and can be added to the same
host.

![](../../../../assets/en/manual/config/hosts/host_mass_e.png)

The following options are available when selecting the respective button
for macros update:

-   *Add* - allows to specify additional user macros for the hosts. If
    *Update existing* checkbox is checked, value, type and description
    for the specified macro name will be updated. If unchecked, if a
    macro with that name already exist on the host(s), it will not be
    updated.
-   *Update* - will replace values, types and descriptions of macros
    specified in this list. If *Add missing* checkbox is checked, macro
    that didn't previously exist on a host will be added as new macro.
    If unchecked, only macros that already exist on a host will be
    updated.
-   *Remove* - will remove specified macros from hosts. If *Except
    selected* box is checked, all macros except specified in the list
    will be removed. If unchecked, only macros specified in the list
    will be removed.
-   *Remove all* - will remove all user macros from hosts. If *I confirm
    to remove all macros* checkbox is not checked, a new popup window
    will open asking to confirm removal of all macros.

![](../../../../assets/en/manual/config/hosts/host_mass_f.png)

To be able to mass update inventory fields, the *Inventory mode* should
be set to 'Manual' or 'Automatic'.

![](../../../../assets/en/manual/config/hosts/host_mass_g.png)

![](../../../../assets/en/manual/config/hosts/host_mass_h.png)

Buttons with the following options are available for value map update:

-   *Add* - add value maps to the hosts. If you mark *Update existing*,
    all properties of the value map with this name will be updated.
    Otherwise, if a value map with that name already exists, it will not
    be updated.
-   *Update* - update existing value maps. If you mark *Add missing*, a
    value map that didn't previously exist on a host will be added as a
    new value map. Otherwise only the value maps that already exist on a
    host will be updated.
-   *Rename* - give new name to an existing value map
-   *Remove* - remove the specified value maps from the hosts. If you
    mark *Except selected*, all value maps will be removed **except**
    the ones that are specified.
-   *Remove all* - remove all value maps from the hosts. If the *I
    confirm to remove all value maps* checkbox is not marked, a new
    popup window will open asking to confirm the removal.

When done with all required changes, click on *Update*. The attributes
will be updated accordingly for all the selected hosts.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
