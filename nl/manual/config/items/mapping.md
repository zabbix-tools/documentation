[comment]: # translation:outdated

[comment]: # ({new-e8992262})
# 9 Value mapping

[comment]: # ({/new-e8992262})

[comment]: # ({new-2d2da55e})
#### Overview

For a more "human" representation of received values, you can use value
maps that contain the mapping between numeric/string values and string
representations.

Value mappings can be used in both the Zabbix frontend and notifications
sent by media types.

For example, an item which has value '0' or '1' can use value mapping to
represent the values in a human-readable form:

-   '0' => 'Not Available'
-   '1' => 'Available'

Or, a backup related value map could be:

-   'F' => 'Full'
-   'D' => 'Differential'
-   'I' => 'Incremental'

In another example, value ranges for voltage may be mapped:

-   '<=209' => 'Low'
-   '210-230' => 'OK'
-   '>=231' => 'High'

Value mappings are defined on template or host level. Once defined they
become available for all items of the respective template or host. There
is no value map inheritance - a template item on a host still uses the
value map from the template; linking a template with value maps to the
host does not make the host inherit the value maps.

When [configuring items](item) you can use a value map to "humanize" the
way an item value will be displayed. To do that, you refer to the name
of a previously defined value map in the *Value mapping* field.

::: noteclassic
Value mapping can be used with items having *Numeric
(unsigned)*, *Numeric (float)* and *Character* type of
information.
:::

Value mappings can be exported/imported with the respective template or
host.

Value mappings can be mass updated. Both
[host](/manual/config/hosts/hostupdate) and
[template](/manual/config/templates/mass) mass update forms have a
*Value mapping* tab for mass updating value maps.

[comment]: # ({/new-2d2da55e})

[comment]: # ({new-18d7ef55})
#### Configuration

To define a value map:

-   Open a host or template configuration form
-   Go to the *Value mapping* tab
-   Click on *Add* to add a new map
-   Click on the name of an existing map to edit it

![](../../../../assets/en/manual/config/items/value_mapping.png)

Parameters of a value map:

|Parameter|<|Description|
|---------|-|-----------|
|*Name*|<|Unique name of a set of value mappings.|
|*Mappings*|<|Individual mapping rules for mapping numeric/string values to string representations.<br>Mapping is applied according to the order of mapping rules. It is possible to reorder mappings by dragging.<br>Only numeric value types are supported for mapping ranges (*is greater than or equals*, *is less than or equals*, *in range* mapping types).|
|<|*Type*|Mapping type:<br>**equals** - equal values will be mapped<br>**is greater than or equals** - equal or greater values will be mapped<br>**is less than or equals** - equal or smaller values will be mapped<br>**in range** - values in range will be mapped; the range is expressed as <number1>-<number2>, or <number>. Multiple ranges are supported (e.g. 1-10,101-110,201)<br>**regexp** - values corresponding to the [regular expression](/manual/regular_expressions) will be mapped (global regular expressions are not supported)<br>**default** - all outstanding values will be mapped, other than those with specific mappings|
|<|*Value*|Incoming value.<br>Depending on the mapping type, may also contain a range or regular expression.|
|<|*Mapped to*|String representation for the incoming value.|

All mandatory input fields are marked with a red asterisk.

When the value map is displayed in the list, only the first three
mappings of it are visible, while three dots indicate that more mappings
exist.

![](../../../../assets/en/manual/config/items/value_map_list.png)

[comment]: # ({/new-18d7ef55})

[comment]: # ({new-6241051d})
#### How this works

For example, one of the predefined agent items 'Zabbix agent ping' uses
a template-level value map called 'Zabbix agent ping status' to display
its values.

![](../../../../assets/en/manual/config/items/value_mapping2.png)

In the item [configuration form](item) you can see a reference to this
value map in the *Value mapping* field:

![](../../../../assets/en/manual/config/items/item_value_mapping.png)

So in *Monitoring → Latest data* the mapping is put to use to display
'Up' (with the raw value in parentheses).

![](../../../../assets/en/manual/config/items/value_display.png){width="600"}

In the *Latest data* section displayed values are shortened to 20
symbols. If value mapping is used, this shortening is not applied to the
mapped value, but only to the raw value separately (displayed in
parentheses).

::: notetip
A value being displayed in a human-readable form is also
easier to understand when receiving notifications.
:::

Without a predefined value map you would only get this:

![](../../../../assets/en/manual/config/items/value_display2.png){width="600"}

So in this case you would either have to guess what the '1' stands for
or do a search of documentation to find out.

[comment]: # ({/new-6241051d})
