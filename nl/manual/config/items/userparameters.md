[comment]: # translation:outdated

[comment]: # ({new-70a6015a})
# 5 User parameters

[comment]: # ({/new-70a6015a})

[comment]: # ({new-9a4b2756})
#### Overview

Sometimes you may want to run an agent check that does not come
predefined with Zabbix. This is where user parameters come to help.

You may write a command that retrieves the data you need and include it
in the user parameter in the [agent configuration
file](/manual/appendix/config/zabbix_agentd) ('UserParameter'
configuration parameter).

A user parameter has the following syntax:

    UserParameter=<key>,<command>

As you can see, a user parameter also contains a key. The key will be
necessary when configuring an item. Enter a key of your choice that will
be easy to reference (it must be unique within a host).

Restart the agent or use the agent [runtime
control](/manual/concepts/agent#runtime_control) option to pick up the
new parameter, e. g.:

    zabbix_agentd -R userparameter_reload

Then, when [configuring an item](item), enter the key to reference the
command from the user parameter you want executed.

User parameters are commands executed by Zabbix agent. Up to 512KB of
data can be returned before item preprocessing steps. Note, however,
that the text value that can be eventually stored in database is limited
to 64KB on MySQL (see info on other databases in the
[table](/manual/config/notifications/action/operation/remote_command#overview)).

**/bin/sh** is used as a command line interpreter under UNIX operating
systems. User parameters obey the agent check timeout; if timeout is
reached the forked user parameter process is terminated.

See also:

-   [Step-by-step
    tutorial](/manual/config/items/userparameters/extending_agent) on
    making use of user parameters
-   [Command execution](/manual/appendix/command_execution)

[comment]: # ({/new-9a4b2756})

[comment]: # ({new-53dfad45})
##### Examples of simple user parameters

A simple command:

    UserParameter=ping,echo 1

The agent will always return '1' for an item with 'ping' key.

A more complex example:

    UserParameter=mysql.ping,mysqladmin -uroot ping | grep -c alive

The agent will return '1', if MySQL server is alive, '0' - otherwise.

[comment]: # ({/new-53dfad45})

[comment]: # ({new-e619711f})
#### Flexible user parameters

Flexible user parameters accept parameters with the key. This way a
flexible user parameter can be the basis for creating several items.

Flexible user parameters have the following syntax:

    UserParameter=key[*],command

|Parameter|Description|
|---------|-----------|
|**Key**|Unique item key. The \[\*\] defines that this key accepts parameters within the brackets.<br>Parameters are given when configuring the item.|
|**Command**|Command to be executed to evaluate value of the key.<br>*For flexible user parameters only*:<br>You may use positional references $1…$9 in the command to refer to the respective parameter in the item key.<br>Zabbix parses the parameters enclosed in \[ \] of the item key and substitutes $1,...,$9 in the command accordingly.<br>$0 will be substituted by the original command (prior to expansion of $0,...,$9) to be run.<br>Positional references are interpreted regardless of whether they are enclosed between double (") or single (') quotes.<br>To use positional references unaltered, specify a double dollar sign - for example, awk '{print $$2}'. In this case `$$2` will actually turn into `$2` when executing the command.|

::: noteimportant
Positional references with the $ sign are searched
for and replaced by Zabbix agent only for flexible user parameters. For
simple user parameters, such reference processing is skipped and,
therefore, any $ sign quoting is not necessary.
:::

::: noteimportant
Certain symbols are not allowed in user parameters
by default. See
[UnsafeUserParameters](/manual/appendix/config/zabbix_agentd)
documentation for a full list.
:::

[comment]: # ({/new-e619711f})

[comment]: # ({new-5c6e8e3c})
##### Example 1

Something very simple:

    UserParameter=ping[*],echo $1

We may define unlimited number of items for monitoring all having format
ping\[something\].

-   ping\[0\] - will always return '0'
-   ping\[aaa\] - will always return 'aaa'

[comment]: # ({/new-5c6e8e3c})

[comment]: # ({new-db2a6e16})
##### Example 2

Let's add more sense!

    UserParameter=mysql.ping[*],mysqladmin -u$1 -p$2 ping | grep -c alive

This parameter can be used for monitoring availability of MySQL
database. We can pass user name and password:

    mysql.ping[zabbix,our_password]

[comment]: # ({/new-db2a6e16})

[comment]: # ({new-dd3ac3fa})
##### Example 3

How many lines matching a regular expression in a file?

    UserParameter=wc[*],grep -c "$2" $1

This parameter can be used to calculate number of lines in a file.

    wc[/etc/passwd,root]
    wc[/etc/services,zabbix]

[comment]: # ({/new-dd3ac3fa})

[comment]: # ({new-a5d86054})
#### Command result

The return value of the command is standard output together with
standard error.

::: noteimportant
A text (character, log or text type of
information) item will not become unsupported in case of standard error
output.
:::

User parameters that return text (character, log, text type of
information) can return whitespace. In case of invalid result the item
will become unsupported.

[comment]: # ({/new-a5d86054})
