[comment]: # translation:outdated

[comment]: # ({new-1a3ec895})
# 1 Usage examples

[comment]: # ({/new-1a3ec895})

[comment]: # ({new-f4923ce3})
#### Overview

This section presents examples of using preprocessing steps to
accomplish some practical tasks.

[comment]: # ({/new-f4923ce3})

[comment]: # ({new-6c9dfb09})
#### Filtering VMware event log records

Using a regular expression preprocessing to filter unnecessary events of
the VMWare event log.

1\. On a working VMWare Hypervisor host check that the event log item
`vmware.eventlog[<url>,<mode>]` is present and working properly. Note
that the event log item could already be present on the hypervisor if
the *Template VM VMWare* template has been linked during the host
creation.

2\. On the VMWare Hypervisor host create a [dependent
item](/manual/config/items/itemtypes/dependent_items) of 'Log' type and
set the event log item as its master.

In the "Preprocessing" tab of the dependent item select the "Matches
regular expression" validation option and fill pattern, for example:

    ".* logged in .*" - filters all logging events in the event log
    "\bUser\s+\K\S+" -  filter only lines with usernames from the event log

::: noteimportant
If the regular expression is not matched then the
dependent item becomes unsupported with a corresponding error message.
To avoid this mark the "Custom on fail" checkbox and select to discard
unmatched value, for example.
:::

Another approach that allows using matching groups and output control is
to select "Regular expression" option in the "Preprocessing" tab and
fill parameters, for example:

    pattern: ".*logged in.*", output: "\0" - filters all logging events in the event log
    pattern "User (.*?)(?=\ )", output: "\1" - filter only usernames from the event log

[comment]: # ({/new-6c9dfb09})
