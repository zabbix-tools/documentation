[comment]: # translation:outdated

[comment]: # ({new-e25ccc09})
# 3 Custom alertscripts

[comment]: # ({/new-e25ccc09})

[comment]: # ({new-bc410af8})
#### Overview

If you are not satisfied with existing media types for sending alerts
there is an alternative way to do that. You can create a script that
will handle the notification your way.

Alert scripts are executed on Zabbix server. These scripts are located
in the directory defined in the server [configuration
file](/manual/appendix/config/zabbix_server) **AlertScriptsPath**
variable.

Here is an example alert script:

``` {.bash}
#!/bin/bash

to=$1
subject=$2
body=$3

cat <<EOF | mail -s "$subject" "$to"
$body
EOF
```

::: noteimportant
Starting from version 3.4 Zabbix checks for the
exit code of the executed commands and scripts. Any exit code which is
different from **0** is considered as a [command
execution](/manual/appendix/command_execution) error. In such case
Zabbix will try to repeat failed execution.
:::

Environment variables are not preserved or created for the script, so
they should be handled explicitly.

[comment]: # ({/new-bc410af8})

[comment]: # ({new-47055351})
#### Configuration

To configure custom alertscripts as the media type:

-   Go to *Administration → Media types*
-   Click on *Create media type*

The **Media type** tab contains general media type attributes:

![](../../../../../assets/en/manual/config/notifications/media/media_script.png)

All mandatory input fields are marked with a red asterisk.

The following parameters are specific for the script media type:

|Parameter|Description|
|---------|-----------|
|*Script name*|Enter the name of the script.|
|*Script parameters*|Add command-line parameters to the script.<br>{ALERT.SENDTO}, {ALERT.SUBJECT} and {ALERT.MESSAGE} [macros](/manual/appendix/macros/supported_by_location) are supported in script parameters.<br>Customizing script parameters is supported since Zabbix 3.0.|

See [common media type
parameters](/manual/config/notifications/media#common_parameters) for
details on how to configure default messages and alert processing
options.

::: notewarning
 Even if an alertscript doesn't use default
messages, message templates for operation types used by this media type
must still be defined, otherwise a notification will not be sent.

:::

::: noteimportant
As parallel processing of media types is
implemented since Zabbix 3.4.0, it is important to note that with more
than one script media type configured, these scripts may be processed in
parallel by alerter processes. The total number of alerter processes is
limited by the StartAlerters
[parameter](/manual/appendix/config/zabbix_server).
:::

[comment]: # ({/new-47055351})

[comment]: # ({new-6a270eac})

#### Media type testing

To test a configured script media type:

-   Locate the relevant script in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Edit the script parameter values, if needed (editing affects the test procedure only, the actual values will not be changed).
-   Click on *Test*.

![](../../../../../assets/en/manual/config/notifications/media/script_test.png){width="600"}


[comment]: # ({/new-6a270eac})

[comment]: # ({new-525cb972})
#### User media

Once the media type is configured, go to the *Administration → Users*
section and edit user profile to assign media of this type to the user.
Steps for setting up user media, being common for all media types, are
described on the [Media
types](/manual/config/notifications/media#user_media) page.

Note, that when defining a user media, a *Send to* field cannot be
empty. If this field will not be used in an alertscript, enter any
combination of supported characters to bypass validation requirements.

[comment]: # ({/new-525cb972})
