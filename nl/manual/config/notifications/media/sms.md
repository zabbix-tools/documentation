[comment]: # translation:outdated

[comment]: # ({new-adbf1c90})
# 2 SMS

[comment]: # ({/new-adbf1c90})

[comment]: # ({new-657f31be})
#### Overview

Zabbix supports the sending of SMS messages using a serial GSM modem
connected to Zabbix server's serial port.

Make sure that:

-   The speed of the serial device (normally /dev/ttyS0 under Linux)
    matches that of the GSM modem. Zabbix does not set the speed of the
    serial link. It uses default settings.
-   The 'zabbix' user has read/write access to the serial device. Run
    the command ls –l /dev/ttyS0 to see current permissions of the
    serial device.
-   The GSM modem has PIN entered and it preserves it after power reset.
    Alternatively you may disable PIN on the SIM card. PIN can be
    entered by issuing command AT+CPIN="NNNN" (NNNN is your PIN number,
    the quotes must be present) in a terminal software, such as Unix
    minicom or Windows HyperTerminal.

Zabbix has been tested with these GSM modems:

-   Siemens MC35
-   Teltonika ModemCOM/G10

To configure SMS as the delivery channel for messages, you also need to
configure SMS as the media type and enter the respective phone numbers
for the users.

[comment]: # ({/new-657f31be})

[comment]: # ({new-e7ed9491})
#### Configuration

To configure SMS as the media type:

-   Go to *Administration → Media types*
-   Click on *Create media type* (or click on *SMS* in the list of
    pre-defined media types).

The following parameters are specific for the SMS media type:

|Parameter|Description|
|---------|-----------|
|*GSM modem*|Set the serial device name of the GSM modem.|

See [common media type
parameters](/manual/config/notifications/media#common_parameters) for
details on how to configure default messages and alert processing
options. Note that parallel processing of sending SMS notifications is
not possible.

[comment]: # ({/new-e7ed9491})

[comment]: # ({new-84a44424})
#### User media

Once the SMS media type is configured, go to the *Administration →
Users* section and edit user profile to assign SMS media to the user.
Steps for setting up user media, being common for all media types, are
described on the [Media
types](/manual/config/notifications/media#user_media) page.

[comment]: # ({/new-84a44424})
