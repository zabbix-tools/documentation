[comment]: # translation:outdated

[comment]: # ({new-be8f2fc2})
# 2 Linking/unlinking

[comment]: # ({/new-be8f2fc2})

[comment]: # ({new-63cf7d66})
#### Overview

Linking is a process whereby templates are applied to hosts, whereas
unlinking removes the association with the template from a host.

::: noteimportant
Templates are linked directly to individual hosts
and not to host groups. Simply adding a template to a host group will
not link it. Host groups are used only for logical grouping of hosts and
templates.
:::

[comment]: # ({/new-63cf7d66})

[comment]: # ({new-af0ee654})
#### Linking a template

To link a template to the host, do the following:

-   Go to *Configuration → Hosts*
-   Click on the required host
-   Start typing the template name in the *Templates* field. A list of
    matching templates will appear; scroll down to select.
-   Alternatively, you may click on *Select* next to the field and
    select one or several templates from the list in a popup window
-   Click on *Add/Update* in the host attributes form

The host will now have all the entities (items, triggers, graphs, etc)
of the template.

::: noteimportant
Linking multiple templates to the same host will
fail if in those templates there are items with the same item key. And,
as triggers and graphs use items, they cannot be linked to a single host
from multiple templates either, if using identical item
keys.
:::

When entities (items, triggers, graphs etc.) are added from the
template:

-   previously existing identical entities on the host are updated as
    entities of the template
-   entities from the template are added
-   any directly linked entities that, prior to template linkage,
    existed only on the host remain untouched

In the lists, all entities from the template now are prefixed by the
template name, indicating that these belong to the particular template.
The template name itself (in gray text) is a link allowing to access the
list of those entities on the template level.

If some entity (item, trigger, graph etc.) is not prefixed by the
template name, it means that it existed on the host before and was not
added by the template.

[comment]: # ({/new-af0ee654})

[comment]: # ({new-b32f387a})
##### Entity uniqueness criteria

When adding entities (items, triggers, graphs etc.) from a template it
is important to know what of those entities already exist on the host
and need to be updated and what entities differ. The uniqueness criteria
for deciding upon the sameness/difference are:

-   for items - the item key
-   for triggers - trigger name and expression
-   for custom graphs - graph name and its items

[comment]: # ({/new-b32f387a})

[comment]: # ({new-368f1425})
##### Linking templates to several hosts

To update template linkage of many hosts, in *Configuration → Hosts*
select some hosts by marking their checkboxes, then click on **[Mass
update](/manual/config/hosts/hostupdate)** below the list and then
select *Link templates*:

![](../../../../assets/en/manual/config/mass_link.png)

To link additional templates, start typing the template name in the
auto-complete field until a dropdown appears offering the matching
templates. Just scroll down to select the template to link.

The *Replace* option will allow to link a new template while unlinking
any template that was linked to the hosts before. The *Unlink* option
will allow to specify which templates to unlink. The *Clear when
unlinking* option will allow to not only unlink any previously linked
templates, but also remove all elements inherited from them (items,
triggers, etc.).

::: notetip
Zabbix offers a sizable set of predefined templates. You
can use these for reference, but beware of using them unchanged in
production as they may contain too many items and poll for data too
often. If you feel like using them, finetune them to fit you real
needs.
:::

[comment]: # ({/new-368f1425})

[comment]: # ({new-4fdf985b})
##### Editing linked entities

If you try to edit an item or trigger that was linked from the template,
you may realize that many key options are disabled for editing. This
makes sense as the idea of templates is that things are edited in
one-touch manner on the template level. However, you still can, for
example, enable/disable an item on the individual host and set the
update interval, history length and some other parameters.

If you want to edit the entity fully, you have to edit it on the
template level (template level shortcut is displayed in the form name),
keeping in mind that these changes will affect all hosts that have this
template linked to them.

[comment]: # ({/new-4fdf985b})

[comment]: # ({new-2d6f7e87})
#### Unlinking a template

To unlink a template from a host, do the following:

-   Go to *Configuration → Hosts*
-   Click on the required host and find the *Templates* field
-   Click on *Unlink* or *Unlink and clear* next to the template to
    unlink
-   Click on *Update* in the host attributes form

Choosing the *Unlink* option will simply remove association with the
template, while leaving all its entities (items, triggers, graphs etc.)
with the host.

Choosing the *Unlink and clear* option will remove both the association
with the template and all its entities (items, triggers, graphs etc.).

[comment]: # ({/new-2d6f7e87})
