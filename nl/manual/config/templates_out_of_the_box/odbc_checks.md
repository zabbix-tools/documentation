[comment]: # translation:outdated

[comment]: # ({new-69d56449})
# ODBC template operation

Steps to ensure correct operation of templates that collect metrics via
[ODBC monitoring](/manual/config/items/itemtypes/odbc_checks):

1\. Make sure that required ODBC driver is installed on Zabbix server or
proxy.\
2. [Link](/manual/config/templates/linking#linking_a_template) the
template to a target host (if the template is not available in your
Zabbix installation, you may need to import the template's .xml file
first - see [Templates
out-of-the-box](/manual/config/templates_out_of_the_box) section for
instructions).\
3. Adjust the values of mandatory macros as needed.\
4. Configure the instance being monitored to allow sharing data with
Zabbix - see instructions in the *Additional steps/comments* column.

::: notetip
 This page contains only a minimum set of macros and
setup steps that are required for proper template operation. A detailed
description of a template, including the full list of macros, items and
triggers, is available in the template's Readme.md file (accessible by
clicking on a template name). 
:::

|Template|Mandatory macros|Additional steps/comments|
|--------|----------------|-------------------------|
|[MSSQL by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mssql_odbc/README.md)|**{$MSSQL.DSN}** - the system data source name (default: <Put your DSN here>)<br>**{$MSSQL.PORT}** - the TCP port of Microsoft SQL Server (default: 1433)<br>**{$MSSQL.USER}, {$MSSQL.PASSWORD}** - Microsoft SQL login credentials (default: not set)|Create a Microsoft SQL user for monitoring and grant the user the following permissions: View Server State; View Any Definition (see Microsoft SQL [documentation](https://docs.microsoft.com/en-us/sql/relational-databases/security/authentication-access/grant-a-permission-to-a-principal?view=sql-server-ver15) for details).<br><br>The "Service's TCP port state" item uses {HOST.CONN} and {$MSSQL.PORT} macros to check the availability of the Microsoft SQL instance.|
|[MySQL by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/mysql_odbc/README.md)|**{$MYSQL.DSN}** - the system data source name (default: <Put your DSN here>)<br>**{$MYSQL.USER}, {$MYSQL.PASSWORD}** - MySQL login credentials; password can be blank (default: not set)|To grant required privileges to MySQL user that will be used for monitoring, run:<br>`GRANT USAGE,REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON %% *.* TO '<username>'@'%';%%`<br><br>See [MYSQL documentation](https://dev.mysql.com/doc/refman/8.0/en/grant.html) for details.|
|[Oracle by ODBC](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/db/oracle_odbc/README.md)|**{$ORACLE.DSN}** - the system data source name (default: <Put your DSN here>)<br>**{$ORACLE.PORT}** - the TCP port of Oracle DB (default: 1521)<br>**{$ORACLE.USER}, {$ORACLE.PASSWORD}** - Oracle login credentials (default: not set)|1\. To create an Oracle user for monitoring, run:<br>`CREATE USER zabbix_mon IDENTIFIED BY <PASSWORD>;`<br>`-- Grant access to the zabbix_mon user.`<br>`GRANT CONNECT, CREATE SESSION TO zabbix_mon;`<br>`GRANT SELECT ON V_$instance TO zabbix_mon;`<br>`GRANT SELECT ON V_$database TO zabbix_mon;`<br>`GRANT SELECT ON v_$sysmetric TO zabbix_mon;`<br>`GRANT SELECT ON v$recovery_file_dest TO zabbix_mon;`<br>`GRANT SELECT ON v$active_session_history TO zabbix_mon;`<br>`GRANT SELECT ON v$osstat TO zabbix_mon;`<br>`GRANT SELECT ON v$restore_point TO zabbix_mon;`<br>`GRANT SELECT ON v$process TO zabbix_mon;`<br>`GRANT SELECT ON v$datafile TO zabbix_mon;`<br>`GRANT SELECT ON v$pgastat TO zabbix_mon;`<br>`GRANT SELECT ON v$sgastat TO zabbix_mon;`<br>`GRANT SELECT ON v$log TO zabbix_mon;`<br>`GRANT SELECT ON v$archive_dest TO zabbix_mon;`<br>`GRANT SELECT ON v$asm_diskgroup TO zabbix_mon;`<br>`GRANT SELECT ON sys.dba_data_files TO zabbix_mon;`<br>`GRANT SELECT ON DBA_TABLESPACES TO zabbix_mon;`<br>`GRANT SELECT ON DBA_TABLESPACE_USAGE_METRICS TO zabbix_mon;`<br>`GRANT SELECT ON DBA_USERS TO zabbix_mon;`<br><br>2. Make sure, that ODBC connects to Oracle with session parameter `NLS_NUMERIC_CHARACTERS= '.,'`<br><br>3. Add a new record to odbc.ini:<br>`[$ORACLE.DSN]`<br>`Driver = Oracle 19 ODBC driver`<br>`Servername = $ORACLE.DSN`<br>`DSN = $ORACLE.DSN`<br><br>4. Check the connection via isql:<br>`isql $TNS_NAME $DB_USER $DB_PASSWORD`<br><br>5. Configure Zabbix server or Zabbix proxy for Oracle ENV Usage. Edit or add a new file: */etc/sysconfig/zabbix-server*, or for the proxy: */etc/sysconfig/zabbix-proxy*. Then add the following lines to the file:<br>`export ORACLE_HOME=/usr/lib/oracle/19.6/client64`<br>`export PATH=$PATH:$ORACLE_HOME/bin`<br>`export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib64:/usr/lib:$ORACLE_HOME/bin`<br>`export TNS_ADMIN=$ORACLE_HOME/network/admin`<br><br>6. Restart Zabbix server or proxy.|

[comment]: # ({/new-69d56449})
