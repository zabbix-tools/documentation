[comment]: # translation:outdated

[comment]: # ({new-b4d6f1c9})
# 15 Data export

[comment]: # ({/new-b4d6f1c9})

[comment]: # ({new-34273682})

#### Overview

Zabbix supports data export in real-time in two ways:

-   [export to files](/manual/config/export/files)
-   [streaming to external systems](/manual/config/export/streaming)

The following entities can be exported:

-   trigger events
-   item values
-   trends (export to files only)

[comment]: # ({/new-34273682})
