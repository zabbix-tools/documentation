[comment]: # translation:outdated

[comment]: # ({new-03ce9d2a})
# 11 Macros

[comment]: # ({/new-03ce9d2a})

[comment]: # ({new-4c81df7a})
#### Overview

Zabbix supports a number of built-in macros which may be used in various
situations. These macros are variables, identified by a specific syntax:

    {MACRO} 

Macros resolve to a specific value depending on the context.

Effective use of macros allows to save time and make Zabbix
configuration more transparent.

In one of typical uses, a macro may be used in a template. Thus a
trigger on a template may be named "Processor load is too high on
{HOST.NAME}". When the template is applied to the host, such as Zabbix
server, the name will resolve to "Processor load is too high on Zabbix
server" when the trigger is displayed in the Monitoring section.

Macros may be used in item key parameters. A macro may be used for only
a part of the parameter, for example
`item.key[server_{HOST.HOST}_local]`. Double-quoting the parameter is
not necessary as Zabbix will take care of any ambiguous special symbols,
if present in the resolved macro.

Besides built-in macros Zabbix also supports user-defined macros,
user-defined macros with context and macros for low-level discovery.

See also:

-   full list of [built-in
    macros](/manual/appendix/macros/supported_by_location)
-   macro [functions](/manual/config/macros/macro_functions)
-   [user macros](/manual/config/macros/user_macros)
-   [user macros with
    context](/manual/config/macros/user_macros_context)
-   [low-level discovery macros](/manual/config/macros/lld_macros)

[comment]: # ({/new-4c81df7a})
