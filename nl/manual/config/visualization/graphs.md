[comment]: # translation:outdated

[comment]: # ({new-0808b0e0})
# 1 Graphs

[comment]: # ({/new-0808b0e0})

[comment]: # ({new-addb9147})
#### Overview

With lots of data flowing into Zabbix, it becomes much easier for the
users if they can look at a visual representation of what is going on
rather than only numbers.

This is where graphs come in. Graphs allow to grasp the data flow at a
glance, correlate problems, discover when something started or make a
presentation of when something might turn into a problem.

Zabbix provides users with:

-   built-in [simple graphs](/manual/config/visualization/graphs/simple)
    of one item data
-   the possibility to create more complex [customized
    graphs](/manual/config/visualization/graphs/custom)
-   access to a comparison of several items quickly in [ad-hoc
    graphs](/manual/config/visualization/graphs/adhoc)
-   modern customizable [vector
    graphs](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#graph)

[comment]: # ({/new-addb9147})
