[comment]: # ({new-ce30c55d})
# 4 Host prototypes

[comment]: # ({/new-ce30c55d})

[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# 4 Host prototypes

Host prototypes can be created with the low-level discovery rule. When
matching entities are discovered, these prototypes become real hosts. 
Discovered hosts belong to an existing host and are prefixed 
with the discovery rule name.

Prototypes, before becoming discovered, cannot have their own items and
triggers, other than those from the linked templates. 

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b2664a94})
## Host prototype configuration

To create a host prototype, press on the *Host prototypes* hyperlink for the required discovery rule, then press *Create host prototype* button in the upper right corner. 

![](../../../../assets/en/manual/vm_monitoring/vm_host_prototypes_if2.png)

In the new window, specify host prototype parameters. Host prototypes have the same parameters as regular [hosts](/manual/config/hosts/host), with the following exceptions:

- *Host name* must contain at least one [low-level discovery macro](/manual/config/macros/lld_macros) 
to ensure that hosts created from the prototype have unique host names.
- *Interfaces* defines whether discovered hosts should inherit the IP 
of a host the discovery rule belongs to (default) or get [custom
interfaces](/manual/discovery/low_level_discovery/host_prototypes#host_interfaces).
- *Group prototypes* allows specifying host group prototypes by using LLD macros.
- *Create enabled* sets the status of discovered hosts, if 
the checkbox is unmarked the hosts will be created, but disabled.
- *Discover* if the checkbox is unmarked, the hosts will not be 
created from the host prototype, unless this setting is [overridden](/manual/discovery/low_level_discovery#override) in the discovery rule.
- *Value maps* are not supported for host prototypes.

LLD macros can be used for host name, visible name, host group prototype, interfaces, tag values, or values 
of host prototype user macros.

[comment]: # ({/new-b2664a94})

[comment]: # ({new-9b6a7dc5})
### Host interfaces

To add custom interfaces, switch the *Interface*
selector from *Inherit* to *Custom* mode, then press
![add\_link.png](../../../../assets/en/manual/config/add_link.png) and select
the required interface type from the menu. 

A host prototype may have any of the supported interface types: Zabbix agent, SNMP, JMX, IPMI. 

Low-level discovery macros and [user macros](/manual/config/macros/user_macros) are supported.

If several custom interfaces are specified, use the *Default* column to
specify the primary interface.

Notes:

-   If *Custom* is selected, but no interfaces have been specified, the
    hosts will be created without interfaces.
-   If *Inherit* is selected for a host prototype that belongs to a
    template, discovered hosts will inherit the interface of a host to
    which the template is linked to.

::: notewarning
 A host will not be created, if the host interface
contains incorrect data. 
:::

[comment]: # ({/new-9b6a7dc5})

[comment]: # ({new-cbeb5931})
## Discovered hosts

In the host list, discovered hosts are prefixed with the name of the 
discovery rule that created them. 

Only the following discovered host parameters are customizable: 
- *Templates* - it is possible to link additional templates to these hosts or unlink manually added templates. Templates inherited from a host prototype cannot be unlinked. 
- *Description*
- *Status* - a host can be manually enabled/disabled
- Host inventory fields

Other parameters are inherited from the host prototype as read-only. 

Discovered hosts can be deleted manually. Hosts that are no longer discovered, will be deleted automatically, 
based on the *Keep lost resources period (in days)* value of the discovery rule.

[comment]: # ({/new-cbeb5931})
