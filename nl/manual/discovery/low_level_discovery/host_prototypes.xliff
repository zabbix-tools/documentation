<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/discovery/low_level_discovery/host_prototypes.md">
    <body>
      <trans-unit id="ce30c55d" xml:space="preserve">
        <source># 4 Host prototypes</source>
      </trans-unit>
      <trans-unit id="f9e8f670" xml:space="preserve">
        <source>Host prototypes are like blueprints for hosts to be created with a
[low-level discovery](/manual/discovery/low_level_discovery)
rule. Prototypes, before becoming discovered, cannot have their own items and triggers, other than those from the linked
templates.</source>
      </trans-unit>
      <trans-unit id="b2664a94" xml:space="preserve">
        <source>## Host prototype configuration

Host prototypes are configured under [discovery rules](/manual/discovery/low_level_discovery#discovery_rule).

To create a host prototype:

1. Navigate to [*Data collection* → *Hosts*](/manual/web_interface/frontend_sections/data_collection/hosts).
2. Click on *Discovery* for the required host to navigate to the list of low-level discovery rules configured for the host.
3. Click on *Host prototypes* for the required discovery rule.
4. Click on the *Create host prototype* button in the upper right corner.

![](../../../../assets/en/manual/discovery/host_prototype.png){width=600}

In the new window, specify host prototype parameters. Host prototypes have the same parameters as regular
[hosts](/manual/config/hosts/host),
with the following exceptions:

- *Host name* must contain at least one
[low-level discovery macro](/manual/config/macros/lld_macros) 
to ensure that hosts created from the prototype have unique host names.
- *Group prototypes* allow specifying host group prototypes by using
[low-level discovery macros](/manual/config/macros/lld_macros).
- *Interfaces* defines whether discovered hosts should inherit the IP 
of a host the discovery rule belongs to (default) or get
[custom interfaces](/manual/discovery/low_level_discovery/host_prototypes#host_interfaces).
- *Create enabled* sets the status of discovered hosts, if 
the checkbox is unmarked the hosts will be created, but disabled.
- *Discover* - if the checkbox is unmarked, the hosts will not be 
created from the host prototype, unless this setting is
[overridden](/manual/discovery/low_level_discovery#override)
in the discovery rule.

::: noteclassic
*Value maps* are not supported for host prototypes.
:::

[Low-level discovery macros](/manual/config/macros/lld_macros)
can be used for host name, visible name, host group prototype, interfaces, tag values, or values 
of host prototype user macros.</source>
      </trans-unit>
      <trans-unit id="9b6a7dc5" xml:space="preserve">
        <source>### Host interfaces

To add custom interfaces, switch the *Interface* selector from *Inherit* to *Custom* mode.
Then press ![add\_link.png](../../../../assets/en/manual/config/add_link.png) and select the required interface type from the menu. 

A host prototype may have any of the supported interface types: Zabbix agent, SNMP, JMX, IPMI. 

[Low-level discovery macros](/manual/config/macros/lld_macros) and [user macros](/manual/config/macros/user_macros) are supported.

If several custom interfaces are specified, use the *Default* column to specify the primary interface.

Notes:

-   If *Custom* is selected, but no interfaces have been specified, the hosts will be created without interfaces.
-   If *Inherit* is selected for a host prototype that belongs to a template, discovered hosts will inherit the interface of a host to which the template is linked to.

::: notewarning
A host will not be created if a host interface contains incorrect data. 
:::</source>
      </trans-unit>
      <trans-unit id="cbeb5931" xml:space="preserve">
        <source>### Discovered hosts

In the host list, discovered hosts are prefixed with the name of the discovery rule that created them. 

The following discovered host parameters are customizable: 

-   *Templates* - it is possible to link additional templates to these hosts or unlink manually added templates.
    Templates inherited from a host prototype cannot be unlinked.
-   *Description*.
-   *Status* - a host can be manually enabled/disabled.
-   *Tags* - host tags can be added manually, alongside the tags inherited from the host prototype.
    Neither manual nor inherited tags can be duplicate, that is, have the same name and value.
    If an inherited tag has the same name and value as a manual tag, it will replace the manual tag during discovery.
-   *Macros* - host macros can be added manually, alongside the macros inherited from the host prototype.
    For inherited macros, it is possible to change macro value and [type](/manual/config/macros/user_macros#configuration) on the host level.
-   Host inventory fields.

Other parameters are inherited from the host prototype as read-only. 

Discovered hosts can be deleted manually.
Hosts that are no longer discovered will be deleted automatically based on the *Keep lost resources period (in days)* value of the discovery rule.

::: noteclassic
Zabbix does not support nested host prototypes, i.e. host prototypes are not supported on hosts that are discovered by low-level discovery rule.
:::</source>
      </trans-unit>
    </body>
  </file>
</xliff>
