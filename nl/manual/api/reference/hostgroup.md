[comment]: # translation:outdated

[comment]: # ({new-8eb68faf})
# Host group

This class is designed to work with host groups.

Object references:\

-   [Host group](/manual/api/reference/hostgroup/object#host_group)

Available methods:\

-   [hostgroup.create](/manual/api/reference/hostgroup/create) -
    creating new host groups
-   [hostgroup.delete](/manual/api/reference/hostgroup/delete) -
    deleting host groups
-   [hostgroup.get](/manual/api/reference/hostgroup/get) - retrieving
    host groups
-   [hostgroup.massadd](/manual/api/reference/hostgroup/massadd) -
    adding related objects to host groups
-   [hostgroup.massremove](/manual/api/reference/hostgroup/massremove) -
    removing related objects from host groups
-   [hostgroup.massupdate](/manual/api/reference/hostgroup/massupdate) -
    replacing or removing related objects from host groups
-   [hostgroup.update](/manual/api/reference/hostgroup/update) -
    updating host groups

[comment]: # ({/new-8eb68faf})
