[comment]: # translation:outdated

[comment]: # ({new-41154bde})
# > Value map object

The following objects are directly related to the `valuemap` API.

[comment]: # ({/new-41154bde})

[comment]: # ({new-65471f47})
### Value map

The value map object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|valuemapid|string|*(readonly)* ID of the value map.|
|**hostid**<br>(required)|id|Value map host ID.|
|**name**<br>(required)|string|Name of the value map.|
|**mappings**<br>(required)|array|Value mappings for current value map. The mapping object is [described in detail below](object#value_mappings).|
|uuid|string|Universal unique identifier, used for linking imported value maps to already existing ones. Used only for value maps on templates. Auto-generated, if not given.<br><br>For update operations this field is *readonly*.|

[comment]: # ({/new-65471f47})

[comment]: # ({new-145a9918})
### Value mappings

The value mappings object defines value mappings of the value map. It
has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|type|integer|Mapping match type. For type equal 0,1,2,3,4 `value` field cannot be empty, for type 5 `value` field should be empty.<br><br>Possible values:<br>0 - (default) exact match ;<br>1 - mapping will be applied if value is greater or equal^1^;<br>2 - mapping will be applied if value is less or equal^1^;<br>3 - mapping will be applied if value is in range (ranges are inclusive), allow to define multiple ranges separated by comma character^1^;<br>4 - mapping will be applied if value match regular expression^2^;<br>5 - default value, mapping will be applied if no other match were found.|
|**value**<br>(required)|string|Original value.<br><br>Is not required for mapping of type "default".|
|**newvalue**<br>(required)|string|Value to which the original value is mapped to.|

^1^ supported only for items having value type "numeric unsigned",
"numeric float".\
^2^ supported only for items having value type "character".\

[comment]: # ({/new-145a9918})
