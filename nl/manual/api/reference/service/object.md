[comment]: # translation:outdated

[comment]: # ({new-b1a89f1b})
# > Service object

The following objects are directly related to the `service` API.

[comment]: # ({/new-b1a89f1b})

[comment]: # ({new-5d7a3d70})
### Service

The service object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|serviceid|string|*(readonly)* ID of the service.|
|**algorithm**<br>(required)|integer|Status calculation rule. Only applicable if child services exist.<br><br>Possible values:<br>0 - set status to OK;<br>1 - most critical if all children have problems;<br>2 - most critical of child services.|
|**name**<br>(required)|string|Name of the service.|
|**showsla**<br>(required)|integer|Whether SLA should be calculated.<br><br>Possible values:<br>0 - do not calculate;<br>1 - calculate.|
|**sortorder**<br>(required)|integer|Position of the service used for sorting.<br><br>Possible values: 0-999.|
|goodsla|float|Minimum acceptable SLA value. If the SLA drops lower, the service is considered to be in problem state.<br><br>Possible values: 0-100.<br><br>Default: 99.9.|
|weight|integer|Service weight.<br><br>Possible values: 0-1000000.<br><br>Default: 0.|
|propagation\_rule|integer|Status propagation rule. Must be set together with `propagation_value`.<br><br>Possible values:<br>0 - *(default)* propagate service status as is - without any changes;<br>1 - increase the propagated status by a given `propagation_value` (by 1 to 5 severities);<br>2 - decrease the propagated status by a given `propagation_value` (by 1 to 5 severities);<br>3 - ignore this service - the status is not propagated to the parent service at all;<br>4 - set fixed service status using a given `propagation_value`.|
|propagation\_value|integer|Status propagation value. Must be set together with `propagation_rule`.<br><br>Possible values for `propagation_rule` with values `0` and `3`: 0.<br><br>Possible values for `propagation_rule` with values `1` and `2`: 1-5.<br><br>Possible values for `propagation_rule` with value `4`:<br>-1 - OK;<br>0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.|
|status|integer|*(readonly)* Whether the service is in OK or problem state.<br><br>If the service is in problem state, `status` is equal either to:<br>- the severity of the most critical problem;<br>- the highest status of a child service in problem state.<br><br>If the service is in OK state, `status` is equal to -1.|
|readonly|boolean|*(readonly)* Access to the service.<br><br>Possible values:<br>0 - Read-write;<br>1 - Read-only.|

[comment]: # ({/new-5d7a3d70})

[comment]: # ({new-c2041847})
### Status rule

The status rule object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|Condition for setting (New status) status.<br><br>Possible values:<br>0 - if at least (N) child services have (Status) status or above;<br>1 - if at least (N%) of child services have (Status) status or above;<br>2 - if less than (N) child services have (Status) status or below;<br>3 - if less than (N%) of child services have (Status) status or below;<br>4 - if weight of child services with (Status) status or above is at least (W);<br>5 - if weight of child services with (Status) status or above is at least (N%);<br>6 - if weight of child services with (Status) status or below is less than (W);<br>7 - if weight of child services with (Status) status or below is less than (N%).<br><br>Where:<br>- N (W) is `limit_value`;<br>- (Status) is `limit_status`;<br>- (New status) is `new_status`.|
|**limit\_value**<br>(required)|integer|Limit value.<br><br>Possible values:<br>- for N and W: 1-100000;<br>- for N%: 1-100.|
|**limit\_status**<br>(required)|integer|Limit status.<br><br>Possible values:<br>-1 - OK;<br>0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.|
|**new\_status**<br>(required)|integer|New status value.<br><br>Possible values:<br>0 - Not classified;<br>1 - Information;<br>2 - Warning;<br>3 - Average;<br>4 - High;<br>5 - Disaster.|

[comment]: # ({/new-c2041847})

[comment]: # ({new-c301cfb2})
### Service tag

The service tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Service tag name.|
|value|string|Service tag value.|

[comment]: # ({/new-c301cfb2})


[comment]: # ({new-e6daa7b4})
### Service alarm

::: noteclassic
Service alarms cannot be directly created, updated or
deleted via the Zabbix API.
:::

The service alarm objects represents an service's state change. It has
the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|clock|timestamp|Time when the service state change has happened.|
|value|integer|Status of the service.<br><br>Refer to the [service status property](object#service) for a list of possible values.|

[comment]: # ({/new-e6daa7b4})

[comment]: # ({new-6b88d3dc})
### Problem tag

Problem tags allow linking services with problem events. The problem tag
object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Problem tag name.|
|operator|integer|Mapping condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>2 - like.|
|value|string|Problem tag value.|

[comment]: # ({/new-6b88d3dc})
