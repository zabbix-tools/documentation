[comment]: # translation:outdated

[comment]: # ({3a19529f-690a015b})
# configuratie.export

[comment]: # ({/3a19529f-690a015b})

[comment]: # ({32550bc2-98d36809})
### Beschrijving

`string configuration.export(objectparameters)`

Met deze methode kunnen configuratie gegevens worden geëxporteerd als een geserialiseerde string.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kan worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/32550bc2-98d36809})

[comment]: # ({649be5e8-ef6ad2c8})
### Parameters

`(object)` Parameters die de te exporteren objecten en het formaat definiëren
gebruiken.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|**format**<br>(verplicht)|string|Formaat waarin de gegevens moeten worden geëxporteerd.<br><br>Mogelijke waarden:<br>`yaml` - YAML;<br>`xml` - XML ;<br>`json` - JSON;<br>`raw` - onverwerkte PHP-array.|
|prettyprint|boolean|Maak de uitvoer leesbaarder door inspringing toe te voegen.<br><br>Mogelijke waarden:<br>`true` - voeg inspringing toe;<br>`false` - *(standaard)* voeg geen inspringing toe .|
|**options**<br>(vereist)|object|Objecten die moeten worden geëxporteerd.<br><br>Het object `options` heeft de volgende parameters:<br>`groups` - `(array)` ID's van hostgroepen om te exporteren;<br>`hosts` - `(array)` ID's van hosts om te exporteren;<br>`images` - `(array)` ID's van te exporteren afbeeldingen;<br>`maps` - `( array)` ID's van te exporteren kaarten;<br>`mediaTypes` - `(array)` ID's van mediatypes om te exporteren;<br>`templates` - `(array)` ID's van te exporteren sjablonen.<br>|

[comment]: # ({/649be5e8-ef6ad2c8})

[comment]: # ({23d88384-0bfd9762})
### Retourwaarden

`(string)` Retourneert een geserialiseerde tekenreeks met de gevraagde
configuratie gegevens.

[comment]: # ({/23d88384-0bfd9762})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({1ee4cbae-5c446795})
#### Een host exporteren

Exporteer de configuratie van een host als een XML-tekenreeks.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.export",
    "params": {
        "options": {
            "hosts": [
                "10161"
            ]
        },
        "format": "xml"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>5.4</version><date>2020-03-13T15:31: 45Z</date><groups><group><uuid>6f6799aa69e844b4b3918f779f2abf08</uuid><name>Zabbix-servers</name></group></groups><hosts><host><host>Host exporteren</host ><name>Host exporteren</name><groups><group><name>Zabbix-servers</name></group></groups><interfaces><interface><interface_ref>if1</interface_ref></interface ></interfaces><items><item><name>Artikel</name><key>item.key</key><delay>30s</delay><tags><tag><tag>Toepassing</tag ><value>CPU</value></tag></tags><valuemap><name>Hoststatus</name></valuemap><interface_ref>if1</interface_ref><request_method>POST</request_method>< /item></items><valuemaps><valuemap><name>Hoststatus</name><mappings><mapping><value>0</value><newvalue>Omhoog</newvalue></mapping><mapping ><value>2</value><newvalue>Onbereikbaar</newvalue></mapping></mappings></valuemap></valuemaps></host></hosts></zabbix_export>\n",
    "id": 1
}
```

[comment]: # ({/1ee4cbae-5c446795})

[comment]: # ({972247c9-a3a5fdbf})
### Bron

CConfiguratie::export() in
*ui/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/972247c9-a3a5fdbf})
