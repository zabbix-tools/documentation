[comment]: # translation:outdated

[comment]: # ({8a80dafe-1359fe15})
# configuratie.import

[comment]: # ({/8a80dafe-1359fe15})

[comment]: # ({f948520d-fe885a4a})
### Beschrijving

`booleaanse configuratie.import(objectparameters)`

Met deze methode kunt u configuratiegegevens importeren van een geserialiseerde string.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kan worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/f948520d-fe885a4a})

[comment]: # ({bc8e5a20-147fd272})
### Parameters

`(object)` Parameters die de te importeren gegevens bevatten en bepalen hoe de
gegevens moeten worden verwerkt.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|**format**<br>(vereist)|string|Formaat van de geserialiseerde tekenreeks.<br><br>Mogelijke waarden:<br>`yaml` - YAML;<br>`xml` - XML;<br >`json` - JSON.|
|**bron**<br>(vereist)|string|Geserialiseerde string die de configuratiegegevens bevat.|
|**rules**<br>(vereist)|object|Regels over hoe nieuwe en bestaande objecten moeten worden geïmporteerd.<br><br>De parameter `rules` wordt in detail beschreven in de onderstaande tabel.|

::: notetip
Als er geen regels worden gegeven, wordt de configuratie niet
bijgewerkt.
:::

Het object `rules` ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|discoveryRules|object|Regels voor het importeren van LLD-regels.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe LLD-regels gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande LLD-regels worden bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden LLD-regels die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|
|graphs|object|Regels voor het importeren van grafieken.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe grafieken gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande grafieken worden bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, zullen grafieken die niet aanwezig zijn in de geïmporteerde gegevens uit de database worden verwijderd; standaard: `false`.|
|groups|object|Regels voor het importeren van hostgroepen.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe hostgroepen aangemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande hostgroepen worden bijgewerkt; standaard: `false`.|
|hosts|object|Regels voor het importeren van hosts.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, zullen nieuwe hosts worden aangemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande hosts worden bijgewerkt; standaard: `false`.|
|httptests|object|Regels voor het importeren van webscenario's.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe webscenario's gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande webscenario's worden bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden webscenario's die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|
|images|object|Regels voor het importeren van afbeeldingen.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe afbeeldingen gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande afbeeldingen worden bijgewerkt; standaard: `false`.|
|items|object|Regels voor het importeren van items.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe items gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande items worden bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden items die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|
|maps|object|Regels voor het importeren van kaarten.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe kaarten gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande kaarten worden bijgewerkt; standaard: `false`.|
|mediaTypes|object|Regels voor het importeren van mediatypen.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe mediatypen gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande mediatypes worden bijgewerkt; standaard: `false`.|
|templateLinkage|object|Regels voor het importeren van sjabloon koppelingen.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe koppelingen tussen sjablonen en host gemaakt ; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden sjabloon koppelingen die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|
|templates|object|Regels voor het importeren van sjablonen.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe sjablonen gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande sjablonen worden bijgewerkt; standaard: `false`.|
|templateDashboards|object|Regels voor het importeren van sjabloon dashboards.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe sjabloon dashboards gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, worden bestaande sjabloon dashboards bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden sjabloon dashboards die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|
|triggers|object|Regels voor het importeren van triggers.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe triggers gemaakt; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande triggers worden bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden triggers die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|
|valueMaps|object|Regels voor het importeren van host- of sjabloon waarde kaarten.<br><br>Ondersteunde parameters:<br>`createMissing` - `(boolean)` indien ingesteld op `true`, worden nieuwe waardekaarten gemaakt ; standaard: `false`;<br>`updateExisting` - `(boolean)` indien ingesteld op `true`, zullen bestaande waardekaarten worden bijgewerkt; standaard: `false`;<br>`deleteMissing` - `(boolean)` indien ingesteld op `true`, worden waardekaarten die niet aanwezig zijn in de geïmporteerde gegevens uit de database verwijderd; standaard: `false`.|

[comment]: # ({/bc8e5a20-147fd272})

[comment]: # ({fc2f8be5-08d02880})
### Retourwaarden

`(boolean)` Retourneert `true` als het importeren is gelukt.

[comment]: # ({/fc2f8be5-08d02880})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({71778716-0801380e})
#### Importing hosts and items

Importeer de host en items in de XML-tekenreeks. Als er items in de
XML ontbreekt, worden ze uit de database verwijderd en alle
anderen blijven ongewijzigd.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.import",
    "params": {
        "format": "xml",
        "rules": {
            "valueMaps": {
                "createMissing": true,
                "updateExisting": false
            },
            "hosts": {
                "createMissing": true,
                "updateExisting": true
            },
            "items": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            }
        },
        "source": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zabbix_export><version>5.4</version><date>2020-03-13T15:31:45Z</date><groups><group><uuid>6f6799aa69e844b4b3918f779f2abf08</uuid><name>Zabbix servers</name></group></groups><hosts><host><host>Export host</host><name>Export host</name><groups><group><name>Zabbix servers</name></group></groups><interfaces><interface><interface_ref>if1</interface_ref></interface></interfaces><items><item><name>Item</name><key>item.key</key><delay>30s</delay><valuemap><name>Host status</name></valuemap><interface_ref>if1</interface_ref><request_method>POST</request_method></item></items><valuemaps><valuemap><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></valuemap></valuemaps></host></hosts></zabbix_export>"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/71778716-0801380e})

[comment]: # ({286e649d-c5744b74})
### Bron

CConfiguratie::import() in
*ui/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/286e649d-c5744b74})
