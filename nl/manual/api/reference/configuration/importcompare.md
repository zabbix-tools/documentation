[comment]: # translation:outdated

[comment]: # ({6f340a71-2956300c})
# configuratie.importvergelijk

[comment]: # ({/6f340a71-2956300c})

[comment]: # ({c9a9a215-069ae59d})
### Beschrijving

`array configuration.importcompare(objectparameters)`

Met deze methode kan het importbestand worden vergeleken met de huidige systeemelementen
en laat zien wat er verandert als dit importbestand wordt geïmporteerd.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/c9a9a215-069ae59d})

[comment]: # ({c96a64d2-0ae26f0f})
### Parameters

`(object)` Parameters met mogelijke data om te importeren en regels hoe de data verwerkt moet worden

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|---------------------------------------------------|-----------|
|**format**<br>(verplicht)|string|Formaat van de geserialiseerde string.<br><br>Mogelijk waardes:<br>`yaml` - YAML;<br>`xml` - XML;<br>`json` - JSON.|
|**source**<br>(verplicht)|string|geserialiseerde string met the configuratie data.|
|**rules**<br>(verplicht)|object|Regels hoe nieuwe en bestaande objecten geimporteerd moeten worden<br><br>de `rules` parameter is in detail beschreven in de tabel hieronder.|

::: notetip
Als er geen regels meegegeven zijn, is er niets om te updaten en zal een leeg resultaat terugkomen.
::: 

::: notetip
Vergelijking wordt alleen gedaan op hostgroepen en sjablonen. Triggers en grafieken worden alleen voor geimporteerde sjabloenen vergeleken. In alle andere gevallen worden ze als nieuw beschouwd.
:::

The `rules` object supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|discoveryRules|object|Rules on how to import LLD rules.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new LLD rules will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing LLD rules will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, LLD rules not present in the imported data will be deleted from the database; default: `false`.|
|graphs|object|Rules on how to import graphs.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new graphs will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing graphs will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, graphs not present in the imported data will be deleted from the database; default: `false`.|
|groups|object|Rules on how to import host groups.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new host groups will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing host groups will be updated; default: `false`.|
|hosts|object|Rules on how to import hosts.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new hosts will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing hosts will be updated; default: `false`.<br><br>This parameter will make no difference to the output. It is allowed only for consistency with `configuration.import`.|
|httptests|object|Rules on how to import web scenarios.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new web scenarios will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing web scenarios will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, web scenarios not present in the imported data will be deleted from the database; default: `false`.|
|images|object|Rules on how to import images.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new images will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing images will be updated; default: `false`.<br><br>This parameter will make no difference to the output. It is allowed only for consistency with `configuration.import`.|
|items|object|Rules on how to import items.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new items will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing items will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, items not present in the imported data will be deleted from the database; default: `false`.|
|maps|object|Rules on how to import maps.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new maps will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing maps will be updated; default: `false`.<br><br>This parameter will make no difference to the output. It is allowed only for consistency with `configuration.import`.|
|mediaTypes|object|Rules on how to import media types.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new media types will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing media types will be updated; default: `false`.<br><br>This parameter will make no difference to the output. It is allowed only for consistency with `configuration.import`.|
|templateLinkage|object|Rules on how to import template links.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new links between templates and host will be created; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, template links not present in the imported data will be deleted from the database; default: `false`.|
|templates|object|Rules on how to import templates.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new templates will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing templates will be updated; default: `false`.|
|templateDashboards|object|Rules on how to import template dashboards.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new template dashboards will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing template dashboards will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, template dashboards not present in the imported data will be deleted from the database; default: `false`.|
|triggers|object|Rules on how to import triggers.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new triggers will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing triggers will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, triggers not present in the imported data will be deleted from the database; default: `false`.|
|valueMaps|object|Rules on how to import host or template value maps.<br><br>Supported parameters:<br>`createMissing` - `(boolean)` if set to `true`, new value maps will be created; default: `false`;<br>`updateExisting` - `(boolean)` if set to `true`, existing value maps will be updated; default: `false`;<br>`deleteMissing` - `(boolean)` if set to `true`, value maps not present in the imported data will be deleted from the database; default: `false`.|

[comment]: # ({/c96a64d2-0ae26f0f})

[comment]: # ({24381a7e-43693d8e})
### Retourwaarden

`(array)` Retourneert een array met wijzigingen in de configuratie, dat zal zijn
gemaakt.

[comment]: # ({/24381a7e-43693d8e})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({b67ca6ca-d979e902})
#### Importeren van hosts en items

Importeer de sjabloon en items in YAML formaat. Als er items in YAML ontbreken
worden ze getoond als verwijderd en al het andere zal ongewijzigd blijven.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "configuration.import",
    "params": {
        "format": "xml",
        "rules": {
            "groups": {
                "createMissing": true,
                "updateExisting": true
            },
            "templates": {
                "createMissing": true,
                "updateExisting": true
            },
            "items": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            },
            "triggers": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            },
            "discoveryRules": {
                "createMissing": true,
                "updateExisting": true,
                "deleteMissing": true
            },
            "valueMaps": {
                "createMissing": true,
                "updateExisting": false
            }
        },
        "source": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><zabbix_export><version>5.4</version><date>2021-05-27T07:12:07Z</date><groups><group><uuid>6f6799aa69e844b4b3918f779f2abf08</uuid><name>Zabbix servers</name></group></groups><templates><template><uuid>e1bde9bf2f0544f5929f45b82502e744</uuid><template>Export template</template><name>Export template</name><groups><group><name>Zabbix servers</name></group></groups><items><item><uuid>3237bc89226e42ed8207574022470e83</uuid><name>Item</name><key>item.key</key><delay>30s</delay><valuemap><name>Host status</name></valuemap><triggers><trigger><uuid>bd1ed0089e4b4f35b762c9d6c599c348</uuid><expression>last(/Export template/item.key)=0</expression><name>Trigger</name></trigger></triggers></item></items><discovery_rules><discovery_rule><uuid>c91616bcf4a44f349539a1b40cb0979d</uuid><name>Discovery rule</name><key>rule.key</key><item_prototypes><item_prototype><uuid>7e164881825744248b3039af3435cf4b</uuid><name>Item prototype</name><key>prototype.key</key></item_prototype></item_prototypes></discovery_rule></discovery_rules><valuemaps><valuemap><uuid>7e666857c5df4af3a338349586f2afc3</uuid><name>Host status</name><mappings><mapping><value>0</value><newvalue>Up</newvalue></mapping><mapping><value>2</value><newvalue>Unreachable</newvalue></mapping></mappings></valuemap></valuemaps></template></templates></zabbix_export>"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
   "jsonrpc":"2.0",
   "result":{
      "templates":{
         "updated":[
            {
               "before":{
                  "uuid":"e1bde9bf2f0544f5929f45b82502e744",
                  "template":"Export template",
                  "name":"Export template"
               },
               "after":{
                  "uuid":"e1bde9bf2f0544f5929f45b82502e744",
                  "template":"Export template",
                  "name":"Export template"
               },
               "items":{
                  "added":[
                     {
                        "after":{
                           "uuid":"3237bc89226e42ed8207574022470e83",
                           "name":"Item",
                           "key":"item.key",
                           "delay":"30s",
                           "valuemap":{
                              "name":"Host status"
                           }
                        },
                        "triggers":{
                           "added":[
                              {
                                 "after":{
                                    "uuid":"bd1ed0089e4b4f35b762c9d6c599c348",
                                    "expression":"last(/Export template/item.key)=0",
                                    "name":"Trigger"
                                 }
                              }
                           ]
                        }
                     }
                  ],
                  "removed":[
                     {
                        "before":{
                           "uuid":"bd3e7b28b3d544d6a83ed01ddaa65ab6",
                           "name":"Old Item",
                           "key":"ite_old.key",
                           "delay":"30s",
                           "valuemap":{
                              "name":"Host status"
                           }
                        }
                     }
                  ]
               },
               "discovery_rules":{
                  "updated":[
                     {
                        "before":{
                           "uuid":"c91616bcf4a44f349539a1b40cb0979d",
                           "name":"Discovery rule",
                           "key":"rule.key"
                        },
                        "after":{
                           "uuid":"c91616bcf4a44f349539a1b40cb0979d",
                           "name":"Discovery rule",
                           "key":"rule.key"
                        },
                        "item_prototypes":{
                           "updated":[
                              {
                                 "before":{
                                    "uuid":"7e164881825744248b3039af3435cf4b",
                                    "name":"Old item prototype",
                                    "key":"prototype_old.key"
                                 },
                                 "after":{
                                    "uuid":"7e164881825744248b3039af3435cf4b",
                                    "name":"Item prototype",
                                    "key":"prototype.key"
                                 }
                              }
                           ]
                        }
                     }
                  ]
               }
            }
         ]
      }
   },
   "id":1
}
```

[comment]: # ({/b67ca6ca-d979e902})

[comment]: # ({c0f75746-c9a0185c})
### Bron

CConfiguratie::importcompare() in
*ui/include/classes/api/services/CConfiguration.php*.

[comment]: # ({/c0f75746-c9a0185c})
