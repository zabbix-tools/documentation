[comment]: # translation:outdated

[comment]: # ({ea99ae0d-9b6159cd})
# Ontdekkingscontrole

Deze klasse is ontworpen om te werken met ontdekkingscontroles.

Objectreferenties:\

- [Ontdekking
    check](/manual/api/reference/dcheck/object#discovery_check)

Beschikbare methoden:\

- [dcheck.get](/manual/api/reference/dcheck/get) - ontdek ontdekking
    cheques

[comment]: # ({/ea99ae0d-9b6159cd})
