[comment]: # translation:outdated

[comment]: # ({new-668456d1})
# auditlog.get

[comment]: # ({/new-668456d1})

[comment]: # ({4e43daf9-002826bb})
### Beschrijving

`integer/array auditlog.get(objectparameters)`

De methode maakt het mogelijk om auditlog records op te halen volgens de gegeven
parameters.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/4e43daf9-002826bb})

[comment]: # ({5ce9f0cf-c9891e0d})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|auditids|string/array|Retourneer alleen auditlog met de opgegeven ID's.|
|userids|string/array|Retourneert alleen het audit logboek dat is gemaakt door de opgegeven gebruikers.|
|time\_from|timestamp|Retourneert alleen controle logboek vermeldingen die na of op het opgegeven tijdstip zijn gemaakt.|
|time\_till|timestamp|Retourneert alleen controle logboek vermeldingen die vóór of op het opgegeven tijdstip zijn gemaakt.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `auditid`, `userid`, `clock`.|
|filter|object|Retourneert alleen resultaten die exact overeenkomen met het opgegeven filter.<br><br>Accepteert een array, waarbij de sleutels eigenschap namen zijn en de waarden een enkele waarde zijn of een array van waarden om mee te vergelijken.< br><br>Ondersteunt bovendien het filteren op details eigenschaps velden: `table_name`, `field_name`.|
|search|object|Hoofdlettergevoelig zoeken in subtekenreeksen in de inhoud van velden: `username`, `ip`, `resourcename`, `details`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch|boolean|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorde|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/5ce9f0cf-c9891e0d})

[comment]: # ({32334ca9-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` is
    gebruikt.

[comment]: # ({/32334ca9-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({9547a68d-66670b26})
#### Auditlogboek ophalen

Haal twee laatste controle logboek records op.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "auditlog.get",
    "params": {
        "output": "extend",
        "sortfield": "clock",
        "sortorder": "DESC",
        "limit": 2
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "auditid": "cksstgfam0001yhdcc41y20q2",
            "userid": "1",
            "username": "Admin",
            "clock": "1629975715",
            "ip": "127.0.0.1",
            "action": "1",
            "resourcetype": "0",
            "resourceid": "0",
            "resourcename": "Jim",
            "recordsetid": "cksstgfal0000yhdcso67ondl",
            "details": "{\"user.name\":[\"update\",\"Jim\",\"\"],\"user.medias[37]\":[\"add\" ],\"user.medias[37].\":[\"add\"],\"user.medias[37].mediatypeid\":[\"add\",\"1\"],\ "user.medias[37].sendto\":[\"add\",\"support123@company.com\"]}"
            
        },
        {
            "auditid": "ckssofl0p0001yhdcqxclsg8r",
            "userid": "1",
            "username": "Admin",
            "clock": "1629967278",
            "ip": "127.0.0.1",
            "action": "0",
            "eventtype": "0",
            "resourceid": "20",
            "resourcename": "Jan",
            "recordsetid": "ckssofl0p0000yhdcpxyo1jgo",
            "details": "{\"user.username\":[\"add\",\"John\"], \"user.userid:\":[\"add\",\"20\"] ,\"user.usrgrps[28]\":[\"add\"],\"user.usrgrps[28].usrgrpid\":[\"add\", \"7\"]}"
        }
    ],
    "id": 1
}
```

[comment]: # ({/9547a68d-66670b26})

[comment]: # ({597ea116-e2b9a859})
### Zie ook

- [Auditlog-object](/manual/api/reference/auditlog/object)

[comment]: # ({/597ea116-e2b9a859})

[comment]: # ({cb8b2446-11b31975})
### Bron

CAuditLog::get() in *ui/include/classes/api/services/CAuditLog.php*.

[comment]: # ({/cb8b2446-11b31975})
