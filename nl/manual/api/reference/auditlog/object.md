[comment]: # translation:outdated

[comment]: # ({f10a44ff-e0b9dd2d})
# > Auditlog-object

De volgende objecten zijn direct gerelateerd aan de `auditlog` API.

[comment]: # ({/f10a44ff-e0b9dd2d})

[comment]: # ({11852f1c-63741e1f})
### Auditlogboek

Het auditlog object bevat informatie over gebruikersacties. Het heeft de
volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|auditid|string|*(alleen-lezen)* ID van invoer in het audit logboek. Gegenereerd met behulp van CUID-algoritme.|
|userid|string|Audit loginvoer auteur userid.|
|username|string|Audit loginvoer gebruikersnaam auteur.|
|clock|timestamp|Tijdstempel voor het maken van auditlog items.|
|ip|string|Auditloginvoer auteur IP-adres.|
|action|integer|Audit logboek invoeractie.<br><br>Mogelijke waarden zijn:<br>0 - Toevoegen;<br>1 - Bijwerken;<br>2 - Verwijderen;<br>4 - Uitloggen;<br >7 - Uitvoeren;<br>8 - Inloggen;<br>9 - Inloggen mislukt;<br>10 - Geschiedenis wissen.|
|resourcetype|integer|Resourcetype audit logboek invoer.<br><br>Mogelijke waarden zijn:<br>0 - Gebruiker;<br>3 - Mediatype;<br>4 - Host;<br>5 - Actie; <br>6 - Grafiek;<br>11 - Gebruikersgroep;<br>13 - Trigger;<br>14 - Hostgroep;<br>15 - Item;<br>16 - Afbeelding;<br>17 - Waarde kaart;<br>18 - Service;<br>19 - Kaart;<br>22 - Webscenario;<br>23 - Ontdekkingsregel;<br>25 - Script;<br>26 - Proxy;<br>27 - Onderhoud;<br>28 - Reguliere expressie;<br>29 - Macro;<br>30 - Sjabloon;<br>31 - Prototype triggeren;<br>32 - Pictogramtoewijzing;<br>33 - Dashboard;<br >34 - Gebeurteniscorrelatie;<br>35 - Grafiek-prototype;<br>36 - Item-prototype;<br>37 - Host-prototype;<br>38 - Automatische registratie;<br>39 - Module;<br>40 - Instellingen ;<br>41 - Huishouden;<br>42 - Verificatie;<br>43 - Sjabloondashboard;<br>44 - Gebruikersrol;<br>45 - Verificatietoken;<br>46 - Gepland rapport.|
|resourceid|string|Resource-ID voor auditlogboek invoer.|
|resourcename|string|Auditlog invoer resource door mensen leesbare naam.|
|recordsetid|string|Recordset-ID voor invoer in het controlelogboek. De auditlogboek records die tijdens dezelfde bewerking zijn gemaakt, hebben dezelfde recordset-ID. Gegenereerd met behulp van CUID-algoritme.|
|details|text|Invoergegevens audit logboek. De details worden opgeslagen als JSON-object waarbij elke eigenschaps naam een pad is naar een eigenschap of genest object waarin de wijziging heeft plaatsgevonden, en elke waarde bevat de gegevens over de wijziging van deze eigenschap in array-indeling.<br><br>Mogelijke waarde-indelingen zijn :<br>\["add"\] - Genest object is toegevoegd;<br>\["add", "<value>"\] - De eigenschap van toegevoegd object bevat <value>;<br>\[ "update"\] - Genest object is bijgewerkt;<br>\["update", "<nieuwe waarde>", "<oude waarde>"\] - De waarde van de eigenschap van het bijgewerkte object is gewijzigd van <oude waarde > naar <nieuwe waarde>;<br>\["delete"\] - Genest object is verwijderd.|

[comment]: # ({/11852f1c-63741e1f})
