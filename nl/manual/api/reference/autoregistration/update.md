[comment]: # translation:outdated

[comment]: # ({9949043f-38f2889f})
# autoregistratie.update

[comment]: # ({/9949043f-38f2889f})

[comment]: # ({99bf4cc4-13c79ed6})
### Beschrijving

`object autoregistratie.update(object autoregistratie)`

Met deze methode kunt u bestaande automatische registratie bijwerken.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/99bf4cc4-13c79ed6})

[comment]: # ({1f3de851-dc051772})
### Parameters

`(object)` Autoregistratie-eigenschappen moeten worden bijgewerkt.

[comment]: # ({/1f3de851-dc051772})

[comment]: # ({fc9bc012-4bb135e8})
### Retourwaarden

`(boolean )` Retourneert boolean true als resultaat bij succesvolle update.

[comment]: # ({/fc9bc012-4bb135e8})

[comment]: # ({291f8950-d84ea91e})
### Voorbeelden

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "autoregistration.update",
    "params": {
        "tls_accept": "3",
        "tls_psk_identity": "PSK 001",
        "tls_psk": "11111595725ac58dd977beef14b97461a7c1045b9a1c923453302c5473193478"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/291f8950-d84ea91e})

[comment]: # ({4e96eabc-0e014313})
### Bron

CAutoregistratie::update() in
*ui/include/classes/api/services/CAutoregistration.php*.

[comment]: # ({/4e96eabc-0e014313})
