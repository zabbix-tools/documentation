[comment]: # translation:outdated

[comment]: # ({c7ef2474-de6776f4})
# > Autoregistratie-object

De volgende objecten zijn direct gerelateerd aan de `autoregistratie`
API.

[comment]: # ({/c7ef2474-de6776f4})

[comment]: # ({8233c019-1e4b66e4})
### Automatische registratie

Het autoregistratie-object heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|tls\_accept|integer|Type toegestane inkomende verbindingen voor automatische registratie.<br><br>Mogelijke waarden:<br>1 - sta onbeveiligde verbindingen toe;<br>2 - sta TLS toe met PSK.<br>3 - sta beide toe onveilig en TLS met PSK-verbindingen.|
|tls\_psk\_identity|string|*(alleen schrijven)* PSK-identiteitsreeks.<br>Plaats geen gevoelige informatie in de PSK-identiteit, deze wordt onversleuteld over het netwerk verzonden om een ontvanger te informeren welke PSK moet worden gebruikt.|
|tls\_psk|string|*(alleen schrijven)* PSK-waardeteken reeks (een even aantal hexadecimale tekens).|

[comment]: # ({/8233c019-1e4b66e4})
