[comment]: # translation:outdated

[comment]: # ({1e55a9ff-8b0c7f00})
# autoregistratie.get

[comment]: # ({/1e55a9ff-8b0c7f00})

[comment]: # ({85c7f464-c6a45cf0})
### Beschrijving

`object autoregistratie.get(objectparameters)`

De methode maakt het mogelijk om autoregistratie-object op te halen volgens de
gegeven parameters.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/85c7f464-c6a45cf0})

[comment]: # ({2bbcb0e4-24b20f8d})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt slechts één parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|output|query|Deze parameter is gebruikelijk voor alle `get`-methoden die worden beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/2bbcb0e4-24b20f8d})

[comment]: # ({e8cfae60-805927e0})
### Retourwaarden

`(object)` Retourneert object voor automatische registratie.

[comment]: # ({/e8cfae60-805927e0})

[comment]: # ({853b1fee-1f25ddf1})
### Voorbeelden

Verzoek:

``` {.java}
{
   "jsonrpc": "2.0",
    "method": "autoregistration.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "tls_accept": "3"
    },
    "id": 1
}
```

[comment]: # ({/853b1fee-1f25ddf1})

[comment]: # ({1a4ae943-c74f8347})
### Bron

CAutoregistratie::get() in
*ui/include/classes/api/services/CAutoregistration.php*.

[comment]: # ({/1a4ae943-c74f8347})
