[comment]: # translation:outdated

[comment]: # ({new-2709706a})
# discoveryrule.create

[comment]: # ({/new-2709706a})

[comment]: # ({33e4e82b-78136d05})
### Beschrijving

`object discoveryrule.create(object/array lldRules)`

Met deze methode kunnen nieuwe LLD-regels worden gemaakt.

::: noteclassic
Deze methode is alleen beschikbaar voor *Admin* en *Super admin*
gebruikers typen. Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de gebruikersrol
instellingen. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/33e4e82b-78136d05})

[comment]: # ({0fd7e13d-1c165cc1})
### Parameters

`(object/array)` LLD-regels om te maken.

Naast de [standaard LLD-regeleigenschappen](object#lld_rule), is de
methode accepteert de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|filter|object|LLD-regel [filter](/manual/api/reference/discoveryrule/object#lld_rule_filter) object voor de LLD-regel.|
|preprocessing|array|LLD-regel [preprocessing](/manual/api/reference/discoveryrule/object#lld_rule_preprocessing) opties.|
|lld\_macro\_paths|array|LLD-regel [lld\_macro\_path](/manual/api/reference/discoveryrule/object#lld_macro_path) opties.|
|overrides|array|LLD-regel [overschrijft](/manual/api/reference/discoveryrule/object#lld_rule_overrides) opties.|

[comment]: # ({/0fd7e13d-1c165cc1})

[comment]: # ({53dfded2-08a72633})
### Retourwaarden

`(object)` Retourneert een object dat de ID's van de gemaakte LLD-regels bevat
onder de eigenschap 'itemids'. De volgorde van de geretourneerde ID's komt overeen met de
volgorde van de doorgegeven LLD-regels.

[comment]: # ({/53dfded2-08a72633})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({52bb7e08-c30fe277})
#### Maken van een LLD regel

Maak een Zabbix agent LLD regel om mounted bestand systemen te ontdekken.
Ontdekte items zullen welke 30 seconden ge-update worden.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Mounted filesystem discovery",
        "key_": "vfs.fs.discovery",
        "hostid": "10197",
        "type": "0",
        "interfaceid": "112",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/52bb7e08-c30fe277})

[comment]: # ({e203c538-01b95865})
#### Gebruik een filter

Maak een LLD-regel met een set voorwaarden om de resultaten te filteren.
De voorwaarden worden gegroepeerd met een logische "en" functie.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Filtered LLD rule",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "filter": {
            "evaltype": 1,
            "conditions": [
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex1"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex2",
                    "operator": "9"
                },
                {
                    "macro": "{#MACRO3}",
                    "value": "",
                    "operator": "12"
                },
                {
                    "macro": "{#MACRO4}",
                    "value": "",
                    "operator": "13"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e203c538-01b95865})

[comment]: # ({e7d48dac-a201a7a1})
#### Maak een LLD regel met macropaden

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "LLD rule with LLD macro paths",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "lld_macro_paths": [
            {
                "lld_macro": "{#MACRO1}",
                "path": "$.path.1"
            },
            {
                "lld_macro": "{#MACRO2}",
                "path": "$.path.2"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e7d48dac-a201a7a1})

[comment]: # ({bc76e14d-32af137e})
#### Een aangepaste expressiefilter gebruiken

Maak een LLD regel met een filter welke een reguliere expressie gebruikt om de voorwaarden te evalueren.
De LLR regel mag alleen objecten ontdekken die de
"{\#MACRO1}" macro gelijk zijn aan 
"regex1" and "regex2", en de waarde van "{\#MACRO2}" matched ofwel
"regex3" ofwel "regex4". De formule IDs "A", "B", "C" and "D" zijn willekeurig gekozen.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Filtered LLD rule",
        "key_": "lld",
        "hostid": "10116",
        "type": "0",
        "interfaceid": "13",
        "delay": "30s",
        "filter": {
            "evaltype": 3,
            "formula": "(A and B) and (C or D)",
            "conditions": [
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex1",
                    "formulaid": "A"
                },
                {
                    "macro": "{#MACRO1}",
                    "value": "@regex2",
                    "formulaid": "B"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex3",
                    "formulaid": "C"
                },
                {
                    "macro": "{#MACRO2}",
                    "value": "@regex4",
                    "formulaid": "D"
                }
            ]
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "27665"
        ]
    },
    "id": 1
}
```

[comment]: # ({/bc76e14d-32af137e})

[comment]: # ({f092cfb8-001d915b})
#### Aangepasten query velden en kopteksten gebruiken

Maak een LLD regel met aangepaste query velden en kopteksten.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "hostid": "10257",
        "interfaceid": "5",
        "type": "19",
        "name": "API HTTP agent",
        "key_": "api_discovery_rule",
        "value_type": "3",
        "delay": "5s",
        "url": "http://127.0.0.1?discoverer.php",
        "query_fields": [
            {
                "mode": "json"
            },
            {
                "elements":"2"
            }
        ],
        "headers": {
            "X-Type": "api",
            "Authorization": "Bearer mF_A.B5f-2.1JcM"
        },
        "allow_traps": "1",
        "trapper_hosts": "127.0.0.1",
        "id": 35,
        "auth": "d678e0b85688ce578ff061bd29a20d3b",
    }
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "28336"
        ]
    },
    "id": 35
}
```

[comment]: # ({/f092cfb8-001d915b})

[comment]: # ({8499a1a6-3e76a4a9})
#### Een LLD regel maken met voorverwerking

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Discovery rule with preprocessing",
        "key_": "lld.with.preprocessing",
        "hostid": "10001",
        "ruleid": "27665",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "interfaceid": "1155",
        "preprocessing": [
            {
                "type": "20",
                "params": "20",
                "error_handler": "0",
                "error_handler_params": ""
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "44211"
        ]
    },
    "id": 1
}
```

[comment]: # ({/8499a1a6-3e76a4a9})

[comment]: # ({c1c714e0-f9343aca})
#### Een LLD regel maken met overschrijvingen

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Discover database host",
        "key_": "lld.with.overrides",
        "hostid": "10001",
        "type": 0,
        "value_type": 3,
        "delay": "60s",
        "interfaceid": "1155",
        "overrides": [
            {
                "name": "Discover MySQL host",
                "step": "1",
                "stop": "1",
                "filter": {
                    "evaltype": "2",
                    "conditions": [
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^mysqld\\.service$"
                        },
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^mariadb\\.service$"
                        }
                    ]
                },
                "operations": [
                    {
                        "operationobject": "3",
                        "operator": "2",
                        "value": "Database host",
                        "opstatus": {
                            "status": "0"
                        },
                        "optemplate": [
                            {
                                "templateid": "10170"
                            }
                        ],
                        "optag": [
                            {
                                "tag": "Database",
                                "value": "MySQL"
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Discover PostgreSQL host",
                "step": "2",
                "stop": "1",
                "filter": {
                    "evaltype": "0",
                    "conditions": [
                        {
                            "macro": "{#UNIT.NAME}",
                            "operator": "8",
                            "value": "^postgresql\\.service$"
                        }
                    ]
                },
                "operations": [
                    {
                        "operationobject": "3",
                        "operator": "2",
                        "value": "Database host",
                        "opstatus": {
                            "status": "0"
                        },
                        "optemplate": [
                            {
                                "templateid": "10263"
                            }
                        ],
                        "optag": [
                            {
                                "tag": "Database",
                                "value": "PostgreSQL"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "30980"
        ]
    },
    "id": 1
}
```

[comment]: # ({/c1c714e0-f9343aca})

[comment]: # ({61537e2a-c75d7d1e})
#### Een script LLD regel maken

Maak een simpele data verzameling met een script type LLD regel.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.create",
    "params": {
        "name": "Script example",
        "key_": "custom.script.lldrule",
        "hostid": "12345",
        "type": 21,
        "value_type": 4,
        "params": "var request = new CurlHttpRequest();\nreturn request.Post(\"https://postman-echo.com/post\", JSON.parse(value));",
        "parameters": [{
            "name": "host",
            "value": "{HOST.CONN}"
        }],
        "timeout": "6s",
        "delay": "30s"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 3
}
```

[comment]: # ({/61537e2a-c75d7d1e})

[comment]: # ({256e4a49-f40c8a6e})
### Zie ook

- [LLD-regelfilter](object#lld_rule_filter)
- [LLD-macropaden](object#lld_macro_path)
- [LLD-regelvoorverwerking](object#lld_rule_preprocessing)

[comment]: # ({/256e4a49-f40c8a6e})

[comment]: # ({db912954-73c4009f})
### Bron

CDiscoveryRule::create() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/db912954-73c4009f})
