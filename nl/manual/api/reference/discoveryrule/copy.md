[comment]: # translation:outdated

[comment]: # ({new-04928fa3})
# discoveryrule.copy

[comment]: # ({/new-04928fa3})

[comment]: # ({12c0d081-8ae93b5b})
### Beschrijving

`object discoveryrule.copy(objectparameters)`

Deze methode maakt het mogelijk om LLD-regels met alle prototypes te kopiëren naar de
gastheren gegeven.

::: noteclassic
Deze methode is alleen beschikbaar voor *Admin* en *Super admin*
gebruikers typen. Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de gebruikersrol
instellingen. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/12c0d081-8ae93b5b})

[comment]: # ({4f6f1dd1-47295f20})
### Parameters

`(object)` Parameters die de LLD-regels definiëren om te kopiëren en het doel
gastheren.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|discoveryids|array|ID's van de LLD-regels die moeten worden gekopieerd.|
|hostids|array|ID's van de hosts waarnaar de LLD-regels moeten worden gekopieerd.|

[comment]: # ({/4f6f1dd1-47295f20})

[comment]: # ({b7d8ce67-b8760eee})
### Retourwaarden

`(boolean)` Retourneert `true` als het kopiëren is gelukt.

[comment]: # ({/b7d8ce67-b8760eee})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({77df9fb6-91be874e})
#### Kopieer een LLD regel naar meerdere hosts

Kopieer een LLD regel naar twee hosts.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.copy",
    "params": {
        "discoveryids": [
            "27426"
        ],
        "hostids": [
            "10196",
            "10197"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/77df9fb6-91be874e})

[comment]: # ({3b12951e-8870f994})
### Bron

CDiscoveryrule::copy() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/3b12951e-8870f994})
