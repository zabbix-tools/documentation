[comment]: # translation:outdated

[comment]: # ({new-293a482d})
# discoveryrule.delete

[comment]: # ({/new-293a482d})

[comment]: # ({e6fa4ff3-1760a96c})
### Beschrijving

`object discoveryrule.delete(array lldRuleIds)`

Met deze methode kunnen LLD-regels worden verwijderd.

::: noteclassic
Deze methode is alleen beschikbaar voor *Admin* en *Super admin*
gebruikers typen. Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de gebruikersrol
instellingen. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/e6fa4ff3-1760a96c})

[comment]: # ({24bacd69-27340eaa})
### Parameters

`(array)` ID's van de LLD-regels om te verwijderen.

[comment]: # ({/24bacd69-27340eaa})

[comment]: # ({d803dc78-ae1ac187})
### Retourwaarden

`(object)` Retourneert een object dat de ID's van de verwijderde LLD-regels bevat
onder de eigenschap 'itemids'.

[comment]: # ({/d803dc78-ae1ac187})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({dd3e43a0-98660baf})
#### Verwijderen van meerdere LLD regels

Verwijder twee LLD regels.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.delete",
    "params": [
        "27665",
        "27668"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "ruleids": [
            "27665",
            "27668"
        ]
    },
    "id": 1
}
```

[comment]: # ({/dd3e43a0-98660baf})

[comment]: # ({b5fc590f-76a51bc0})
### Bron

CDiscoveryRule::delete() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/b5fc590f-76a51bc0})
