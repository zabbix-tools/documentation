[comment]: # translation:outdated

[comment]: # ({new-ddce888e})
# discoveryrule.get

[comment]: # ({/new-ddce888e})

[comment]: # ({3c4bf02f-b87cd1ee})
### Beschrijving

`integer/array discoveryrule.get(objectparameters)`

De methode maakt het mogelijk om LLD-regels op te halen volgens de gegeven
parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kan worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/3c4bf02f-b87cd1ee})

[comment]: # ({3bbdfe7d-57007d3d})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|itemids|string/array|Retourneer alleen LLD-regels met de opgegeven ID's.|
|groupids|string/array|Retourneert alleen LLD-regels die behoren tot de hosts van de gegeven groepen.|
|hostids|string/array|Retourneer alleen LLD-regels die bij de opgegeven hosts horen.|
|inherited|boolean|Indien ingesteld op `true`, worden alleen LLD-regels geretourneerd die zijn overgenomen van een sjabloon.|
|interfaceids|string/array|Return alleen LLD-regels gebruiken de opgegeven host interfaces.|
|monitored|boolean|Indien ingesteld op `true` retourneer alleen ingeschakelde LLD-regels die bij bewaakte hosts horen.|
|template|boolean|Indien ingesteld op `true`, worden alleen LLD-regels geretourneerd die bij sjablonen horen.|
|templateids|string/array|Retourneer alleen LLD-regels die bij de opgegeven sjablonen horen.|
|selectFilter|query|Retourneer een eigenschap [filter](/manual/api/reference/discoveryrule/object#LLD_rule_filter) met gegevens van het filter dat door de LLD-regel wordt gebruikt.|
|selectGraphs|query|Retourneert een eigenschap [graphs](/manual/api/reference/graph/object) met grafiek prototypes die bij de LLD-regel horen.<br><br>Ondersteunt `count`.|
|selectHostPrototypes|query|Retourneer een eigenschap [hostPrototypes](/manual/api/reference/hostprototype/object) met host prototypes die bij de LLD-regel horen.<br><br>Ondersteunt `count`.|
|selectHosts|query|Retourneer een eigenschap [hosts](/manual/api/reference/host/object) met een array van hosts waartoe de LLD-regel behoort.|
|selectItems|query|Retourneer een eigenschap [items](/manual/api/reference/item/object) met item prototypes die bij de LLD-regel horen.<br><br>Ondersteunt `count`.|
|selectTriggers|query|Retourneer een eigenschap [triggers](/manual/api/reference/trigger/object) met trigger-prototypes die bij de LLD-regel horen.<br><br>Ondersteunt `count`.|
|selectLLDMacroPaths|query|Retourneer een eigenschap [lld\_macro\_paths](/manual/api/reference/discoveryrule/object#lld_macro_path) met een lijst van LLD-macro's en paden naar waarden die aan elke corresponderende macro zijn toegewezen.|
|selectPreprocessing|query|Retourneer een eigenschap `preprocessing` met voorverwerkings opties voor LLD-regels.<br><br>Het heeft de volgende eigenschappen:<br>`type` - `(string)` Het preprocessing-optietype:<br>5 - Overeenstemming met reguliere expressies;<br>11 - XML XPath;<br>12 - JSONPath;<br>15 - Komt niet overeen met reguliere expressie;<br>16 - Controleer op fouten in JSON;<br>17 - Controleer op fouten in XML;<br>20 - Ongewijzigd weggooien met hartslag;<br>23 - Prometheus naar JSON;<br>24 - CSV naar JSON;<br>25 - Vervang;<br>27 - XML naar JSON.<br> <br>`params` - `(string)` Extra parameters gebruikt door voorverwerkings optie. Meerdere parameters worden gescheiden door het LF-teken (\\n).<br>`error_handler` - `(string)` Actietype dat wordt gebruikt in het geval van een mislukte voorverwerkings stap:<br>0 - Foutbericht wordt ingesteld door Zabbix-server;<br >1 - Waarde negeren;<br>2 - Aangepaste waarde instellen;<br>3 - Aangepaste foutmelding instellen.<br><br>`error_handler_params` - `(string)` Parameters voor fouthandler.|
|selectOverrides|query|Retourneer een eigenschap [lld\_rule\_overrides](/manual/api/reference/discoveryrule/object#lld_rule_overrides) met een lijst van opheffingsfilters, voorwaarden en bewerkingen die worden uitgevoerd op prototypeobjecten.|
|filter|object|Retourneert alleen die resultaten die exact overeenkomen met het opgegeven filter.<br><br>Accepteert een array, waarbij de sleutels eigenschaps namen zijn en de waarden een enkele waarde zijn of een array van waarden om mee te vergelijken. <br><br>Ondersteunt extra filters:<br>`host` - technische naam van de host waartoe de LLD-regel behoort.|
|limitSelects|integer|Beperkt het aantal records dat wordt geretourneerd door subselecties.<br><br>Van toepassing op de volgende subselecties:<br>`selctItems`;<br>`selectGraphs`;<br>`selectTriggers`.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `itemid`, `name`, `key_`, `delay`, `type` en `status`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden in detail beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortordee|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/3bbdfe7d-57007d3d})

[comment]: # ({ef8f6fed-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` heeft
    gebruikt.

[comment]: # ({/ef8f6fed-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({b852afee-5f8b5429})
#### Ophalen van ontdekkingsregels van een host

Haal alle ontdekkingsregels op van host "10202".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": "extend",
        "hostids": "10202"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "27425",
            "type": "0",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Network interface discovery",
            "key_": "net.if.discovery",
            "delay": "1h",
            "state": "0",
            "status": "0",
            "trapper_hosts": "",
            "error": "",
            "templateid": "22444",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "description": "Discovery of network interfaces as defined in global regular expression \"Network interfaces for discovery\".",
            "lifetime": "30d",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "parameters": []
        },
        {
            "itemid": "27426",
            "type": "0",
            "snmp_oid": "",
            "hostid": "10202",
            "name": "Mounted filesystem discovery",
            "key_": "vfs.fs.discovery",
            "delay": "1h",
            "state": "0",
            "status": "0",
            "trapper_hosts": "",
            "error": "",
            "templateid": "22450",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "interfaceid": "119",
            "description": "Discovery of file systems of different types as defined in global regular expression \"File systems for discovery\".",
            "lifetime": "30d",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "",
            "query_fields": [],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": [],
            "retrieve_mode": "0",
            "request_method": "0",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "parameters": []
        }
    ],
    "id": 1
}
```

[comment]: # ({/b852afee-5f8b5429})

[comment]: # ({7e8be051-4d0af030})
#### Ophalen van filter voorwaarden

Ophalen van LLD regel "24681" en de filter voorwaarden hiervan. Het filter
gebruikt de logische "en" evaluatie, dus de eigenschap "formula" is leeg terwijl
"eval_formula" automatisch gegenereerd is.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": [
            "name"
        ],
        "selectFilter": "extend",
        "itemids": ["24681"]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "24681",
            "name": "Filtered LLD rule",
            "filter": {
                "evaltype": "1",
                "formula": "",
                "conditions": [
                    {
                        "macro": "{#MACRO1}",
                        "value": "@regex1",
                        "operator": "8",
                        "formulaid": "A"
                    },
                    {
                        "macro": "{#MACRO2}",
                        "value": "@regex2",
                        "operator": "9",
                        "formulaid": "B"
                    },
                    {
                        "macro": "{#MACRO3}",
                        "value": "",
                        "operator": "12",
                        "formulaid": "C"
                    },
                    {
                        "macro": "{#MACRO4}",
                        "value": "",
                        "operator": "13",
                        "formulaid": "D"
                    }
                ],
                "eval_formula": "A and B and C and D"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/7e8be051-4d0af030})

[comment]: # ({f1927e12-e3de6fe8})
#### Haal LLD regel op via URL

Haal de LLD regel voor een host op via de URL veld waarde.
Alleen een exacte match voor de URL string zoals in de LLD regel gedefineerd
is, is supported.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "hostids": "10257",
        "filter": {
            "type": "19",
            "url": "http://127.0.0.1/discoverer.php"
        }
    },
    "id": 39,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "itemid": "28336",
            "type": "19",
            "snmp_oid": "",
            "hostid": "10257",
            "name": "API HTTP agent",
            "key_": "api_discovery_rule",
            "delay": "5s",
            "history": "90d",
            "trends": "0",
            "status": "0",
            "value_type": "4",
            "trapper_hosts": "",
            "units": "",
            "error": "",
            "logtimefmt": "",
            "templateid": "0",
            "valuemapid": "0",
            "params": "",
            "ipmi_sensor": "",
            "authtype": "0",
            "username": "",
            "password": "",
            "publickey": "",
            "privatekey": "",
            "flags": "1",
            "interfaceid": "5",
            "description": "",
            "inventory_link": "0",
            "lifetime": "30d",
            "state": "0",
            "jmx_endpoint": "",
            "master_itemid": "0",
            "timeout": "3s",
            "url": "http://127.0.0.1/discoverer.php",
            "query_fields": [
                {
                    "mode": "json"
                },
                {
                    "elements": "2"
                }
            ],
            "posts": "",
            "status_codes": "200",
            "follow_redirects": "1",
            "post_type": "0",
            "http_proxy": "",
            "headers": {
                "X-Type": "api",
                "Authorization": "Bearer mF_A.B5f-2.1JcM"
            },
            "retrieve_mode": "0",
            "request_method": "1",
            "ssl_cert_file": "",
            "ssl_key_file": "",
            "ssl_key_password": "",
            "verify_peer": "0",
            "verify_host": "0",
            "allow_traps": "0",
            "parameters": []
        }
    ],
    "id": 39
}
```

[comment]: # ({/f1927e12-e3de6fe8})

[comment]: # ({15052757-c0ae2d21})
#### Haal een LLD regel met overschrijvingen op.

Haal een LLD regel met meerdere overschrijvings instellingen op.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.get",
    "params": {
        "output": ["name"],
        "itemids": "30980",
        "selectOverrides": ["name", "step", "stop", "filter", "operations"]
    },
    "id": 39,
    "auth": "d678e0b85688ce578ff061bd29a20d3b"
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "name": "Discover database host"
            "overrides": [
                {
                    "name": "Discover MySQL host",
                    "step": "1",
                    "stop": "1",
                    "filter": {
                        "evaltype": "2",
                        "formula": "",
                        "conditions": [
                            {
                                "macro": "{#UNIT.NAME}",
                                "operator": "8",
                                "value": "^mysqld\\.service$"
                                "formulaid": "A"
                            },
                            {
                                "macro": "{#UNIT.NAME}",
                                "operator": "8",
                                "value": "^mariadb\\.service$"
                                "formulaid": "B"
                            }
                        ],
                        "eval_formula": "A or B"
                    },
                    "operations": [
                        {
                            "operationobject": "3",
                            "operator": "2",
                            "value": "Database host",
                            "opstatus": {
                                "status": "0"
                            },
                            "optag": [
                                {
                                    "tag": "Database",
                                    "value": "MySQL"
                                }
                            ],
                            "optemplate": [
                                {
                                    "templateid": "10170"
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "Discover PostgreSQL host",
                    "step": "2",
                    "stop": "1",
                    "filter": {
                        "evaltype": "0",
                        "formula": "",
                        "conditions": [
                            {
                                "macro": "{#UNIT.NAME}",
                                "operator": "8",
                                "value": "^postgresql\\.service$"
                                "formulaid": "A"
                            }
                        ],
                        "eval_formula": "A"
                    },
                    "operations": [
                        {
                            "operationobject": "3",
                            "operator": "2",
                            "value": "Database host",
                            "opstatus": {
                                "status": "0"
                            },
                            "optag": [
                                {
                                    "tag": "Database",
                                    "value": "PostgreSQL"
                                }
                            ],
                            "optemplate": [
                                {
                                    "templateid": "10263"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    "id": 39
}
```

[comment]: # ({/15052757-c0ae2d21})

[comment]: # ({1cd8f848-f25d02bb})
### Zie ook

- [Grafiek
    prototype](/manual/api/reference/graphprototype/object#graph_prototype)
- [Host](/manual/api/reference/host/object#host)
- [Artikel
    prototype](/manual/api/reference/itemprototype/object#item_prototype)
- [LLD-regelfilter](object#lld_rule_filter)
-   [Op gang brengen
    prototype](/manual/api/reference/triggerprototype/object#trigger_prototype)

[comment]: # ({/1cd8f848-f25d02bb})

[comment]: # ({8fb35fd0-e14efed8})
### Bron

CDiscoveryRule::get() in
*ui/include/classes/api/services/CDiscoveryRule.php*.

[comment]: # ({/8fb35fd0-e14efed8})
