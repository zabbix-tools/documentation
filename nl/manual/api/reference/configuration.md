[comment]: # translation:outdated

[comment]: # ({30909541-52ad42a2})
# Configuratie

Deze klasse is ontworpen om Zabbix-configuratiegegevens te exporteren en importeren.

Beschikbare methoden:\

- [configuratie.export](/manual/api/reference/configuration/export) -
    de configuratie exporteren
- [configuratie.import](/manual/api/reference/configuration/import) -
    de configuratie importeren

[comment]: # ({/30909541-52ad42a2})
