[comment]: # translation:outdated

[comment]: # ({new-03fad7e1})
# mediatype.delete

[comment]: # ({/new-03fad7e1})

[comment]: # ({new-3ee75870})
### Description

`object mediatype.delete(array mediaTypeIds)`

This method allows to delete media types.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-3ee75870})

[comment]: # ({new-854e3ea2})
### Parameters

`(array)` IDs of the media types to delete.

[comment]: # ({/new-854e3ea2})

[comment]: # ({new-ade2598d})
### Return values

`(object)` Returns an object containing the IDs of the deleted media
types under the `mediatypeids` property.

[comment]: # ({/new-ade2598d})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-09d9ff4a})
#### Deleting multiple media types

Delete two media types.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "mediatype.delete",
    "params": [
        "3",
        "5"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "mediatypeids": [
            "3",
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-09d9ff4a})

[comment]: # ({new-68bd428e})
### Source

CMediaType::delete() in
*ui/include/classes/api/services/CMediaType.php*.

[comment]: # ({/new-68bd428e})
