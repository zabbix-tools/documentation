[comment]: # translation:outdated

[comment]: # ({82ce8ee4-a9b6b9e5})
# > Discovery-controleobject

De volgende objecten zijn direct gerelateerd aan de `dcheck` API.

[comment]: # ({/82ce8ee4-a9b6b9e5})

[comment]: # ({81366853-662afbc9})
### Ontdekkingscontrole

Het discovery controle object definieert een specifieke controle die wordt uitgevoerd door een
regel voor netwerkdetectie. Het heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|dcheckid|string|*(alleen-lezen)* ID van de detectie controle.|
|druleid|string|*(alleen-lezen)* ID van de ontdekkingsregel waartoe de controle behoort.|
|key\_|string|De waarde van deze eigenschap verschilt afhankelijk van het type controle:<br>- sleutel voor het opvragen van Zabbix agent controles, vereist;<br>- SNMP OID voor SNMPv1-, SNMPv2- en SNMPv3-controles, vereist .|
|ports|string|Een of meerdere poortbereiken om te controleren, gescheiden door komma's. Gebruikt voor alle controles behalve ICMP.<br><br>Standaard: 0.|
|snmp\_community|string|SNMP-community.<br><br>Vereist voor SNMPv1- en SNMPv2 agent controles.|
|snmpv3\_authpassphrase|string|Verificatiewachtwoord gebruikt voor SNMPv3 agent controles met beveiligingsniveau ingesteld op *authNoPriv* of *authPriv*.|
|snmpv3\_authprotocol|integer|Verificatieprotocol gebruikt voor SNMPv3 agent controles met beveiligingsniveau ingesteld op *authNoPriv* of *authPriv*.<br><br>Mogelijke waarden:<br>0 - *(standaard)* MD5;<br >1 - SHA1;<br>2 - SHA224;<br>3 - SHA256;<br>4 - SHA384;<br>5 - SHA512.|
|snmpv3\_contextname|string|SNMPv3 context naam. Alleen gebruikt door SNMPv3-controles.|
|snmpv3\_privpassphrase|string|Privacy passphrase gebruikt voor SNMPv3 agent controles met beveiligingsniveau ingesteld op *authPriv*.|
|snmpv3\_privprotocol|integer|Privacyprotocol gebruikt voor SNMPv3 agent controles met beveiligingsniveau ingesteld op *authPriv*.<br><br>Mogelijke waarden:<br>0 - *(standaard)* DES;<br>1 - AES128 ;<br>2 - AES192;<br>3 - AES256;<br>4 - AES192C;<br>5 - AES256C.|
|snmpv3\_securitylevel|string|Beveiligingsniveau gebruikt voor SNMPv3 agent controles.<br><br>Mogelijke waarden:<br>0 - noAuthNoPriv;<br>1 - authNoPriv;<br>2 - authPriv.|
|snmpv3\_securityname|string|Beveiligingsnaam gebruikt voor SNMPv3 agent controles.|
|**type**<br>(verplicht)|geheel getal|Type controle.<br><br>Mogelijke waarden:<br>0 - SSH;<br>1 - LDAP;<br>2 - SMTP;< br>3 - FTP;<br>4 - HTTP;<br>5 - POP;<br>6 - NNTP;<br>7 - IMAP;<br>8 - TCP;<br>9 - Zabbix agent;< br>10 - SNMPv1-agent;<br>11 - SNMPv2-agent;<br>12 - ICMP-ping;<br>13 - SNMPv3-agent;<br>14 - HTTPS;<br>15 - Telnet.|
|uniq|integer|Of deze controle gebruikt moet worden als uniekheids criterium voor apparaten. Er kan slechts één unieke controle worden geconfigureerd voor een detectieregel. Gebruikt voor Zabbix-agent-, SNMPv1-, SNMPv2- en SNMPv3 agent controles.<br><br>Mogelijke waarden:<br>0 - *(standaard)* gebruik deze controle niet als uniekheids criterium;<br>1 - gebruik deze controle als een uniekheids criterium.|
|host\_source|integer|Bron voor hostnaam.<br><br>Mogelijke waarden:<br>1 - *(standaard)* DNS;<br>2 - IP;<br>3 - ontdekkingswaarde van deze controle .|
|name\_source|integer|Bron voor zichtbare naam.<br><br>Mogelijke waarden:<br>0 - *(standaard)* niet gespecificeerd;<br>1 - DNS;<br>2 - IP;<br >3 - ontdekkingswaarde van deze controle.|

[comment]: # ({/81366853-662afbc9})
