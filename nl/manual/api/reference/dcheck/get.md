[comment]: # translation:outdated

[comment]: # ({new-25145295})
# dcheck.get

[comment]: # ({/new-25145295})

[comment]: # ({032981ee-cf2cd01b})
### Beschrijving

`integer/array dcheck.get(objectparameters)`

De methode maakt het mogelijk om detectie controles op te halen volgens de gegeven
parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/032981ee-cf2cd01b})

[comment]: # ({8b870683-3db1bd90})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|dcheckids|string/array|Retourneer alleen detectie controles met de opgegeven ID's.|
|druleids|string/array|Retourneer alleen discovery controles die bij de opgegeven discovery regels horen.|
|dserviceids|string/array|Retourneer alleen detectie controles die de opgegeven ontdekte services hebben gedetecteerd.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `dcheckid` en `druleid`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden in detail beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/8b870683-3db1bd90})

[comment]: # ({32334ca9-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` is
    gebruikt.

[comment]: # ({/32334ca9-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({6c8bac3f-90f6afd9})
#### Ontdekkingscontroles ophalen voor een ontdekkingsregel

Haal alle detectiecontroles op die worden gebruikt door detectieregel "6".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dcheck.get",
    "params": {
        "output": "extend",
        "dcheckids": "6"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dcheckid": "6",
            "druleid": "4",
            "type": "3",
            "key_": "",
            "snmp_community": "",
            "ports": "21",
            "snmpv3_securityname": "",
            "snmpv3_securitylevel": "0",
            "snmpv3_authpassphrase": "",
            "snmpv3_privpassphrase": "",
            "uniq": "0",
            "snmpv3_authprotocol": "0",
            "snmpv3_privprotocol": "0",
            "host_source": "1",
            "name_source": "0"            
        }
    ],
    "id": 1
}
```

[comment]: # ({/6c8bac3f-90f6afd9})

[comment]: # ({7659f717-d1a6b685})
### Bron

CDCheck::get() in *ui/include/classes/api/services/CDCheck.php*.

[comment]: # ({/7659f717-d1a6b685})
