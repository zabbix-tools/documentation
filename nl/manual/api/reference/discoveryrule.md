[comment]: # translation:outdated

[comment]: # ({36b1b2bb-2593ef3e})
# LLD-regel

Deze klasse is ontworpen om te werken met ontdekkingsregels op laag niveau.

Objectreferenties:\

- [LLD-regel](/manual/api/reference/discoveryrule/object#lld_rule)

Beschikbare methoden:\

- [discoveryrule.copy](/manual/api/reference/discoveryrule/copy) -
    LLD-regels kopiëren
- [discoveryrule.create](/manual/api/reference/discoveryrule/create) -
    nieuwe LLD-regels maken
- [discoveryrule.delete](/manual/api/reference/discoveryrule/delete) -
    LLD-regels verwijderen
- [discoveryrule.get](/manual/api/reference/discoveryrule/get) -
    LLD-regels ophalen
- [discoveryrule.update](/manual/api/reference/discoveryrule/update) -
    LLD-regels bijwerken

[comment]: # ({/36b1b2bb-2593ef3e})
