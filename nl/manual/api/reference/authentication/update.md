[comment]: # translation:outdated

[comment]: # ({dd7f4dc0-35521717})
# authenticatie.update

[comment]: # ({/dd7f4dc0-35521717})

[comment]: # ({71709cfb-4244aae4})
### Beschrijving

`object authenticatie.update(object authenticatie)`

Met deze methode kunnen bestaande authenticatie-instellingen worden bijgewerkt.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Superbeheerder*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/71709cfb-4244aae4})

[comment]: # ({3f134b51-6894664c})
### Parameters

`(object)` Authenticatie-eigenschappen moeten worden bijgewerkt.

[comment]: # ({/3f134b51-6894664c})

[comment]: # ({5102234d-440363c6})
### Retourwaarden

`(array)` Retourneert array met de namen van bijgewerkte parameters.

[comment]: # ({/5102234d-440363c6})

[comment]: # ({29ce50cd-f95d3166})
### Voorbeelden

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "authentication.update",
    "params": {
        "http_auth_enabled": 1,
        "http_case_sensitive": 0,
        "http_login_form": 1
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        "http_auth_enabled",
        "http_case_sensitive",
        "http_login_form"
    ],
    "id": 1
}
```

[comment]: # ({/29ce50cd-f95d3166})

[comment]: # ({ef7066f5-8e7357d5})
### Bron

CAauthenticatie::update() in
*ui/include/classes/api/services/CAuthentication.php*.

[comment]: # ({/ef7066f5-8e7357d5})
