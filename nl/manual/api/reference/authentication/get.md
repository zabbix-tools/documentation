[comment]: # translation:outdated

[comment]: # ({new-558a988d})
# authentication.get

[comment]: # ({/new-558a988d})

[comment]: # ({e29d9c5d-f493fd10})
### Beschrijving

`object authenticatie.get(object parameters)`

De methode maakt het mogelijk om authenticatie-object op te halen volgens de
gegeven parameters.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/e29d9c5d-f493fd10})

[comment]: # ({2bbcb0e4-24b20f8d})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt slechts één parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|output|query|Deze parameter is gebruikelijk voor alle `get`-methoden die worden beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/2bbcb0e4-24b20f8d})

[comment]: # ({96c0afce-6394c247})
### Retourwaarden

`(object)` Retourneert authenticatie-object.

[comment]: # ({/96c0afce-6394c247})

[comment]: # ({3c2e4331-cad6afa3})
### Voorbeelden

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "authentication.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "authentication_type": "0",
        "http_auth_enabled": "0",
        "http_login_form": "0",
        "http_strip_domains": "",
        "http_case_sensitive": "1",
        "ldap_configured": "0",
        "ldap_host": "",
        "ldap_port": "389",
        "ldap_base_dn": "",
        "ldap_search_attribute": "",
        "ldap_bind_dn": "",
        "ldap_case_sensitive": "1",
        "ldap_bind_password": "",
        "saml_auth_enabled": "0",
        "saml_idp_entityid": "",
        "saml_sso_url": "",
        "saml_slo_url": "",
        "saml_username_attribute": "",
        "saml_sp_entityid": "",
        "saml_nameid_format": "",
        "saml_sign_messages": "0",
        "saml_sign_assertions": "0",
        "saml_sign_authn_requests": "0",
        "saml_sign_logout_requests": "0",
        "saml_sign_logout_responses": "0",
        "saml_encrypt_nameid": "0",
        "saml_encrypt_assertions": "0",
        "saml_case_sensitive": "0",
        "passwd_min_length": "8",
        "passwd_check_rules": "8"
    },
    "id": 1
}
```

[comment]: # ({/3c2e4331-cad6afa3})

[comment]: # ({35bf4f57-b8be0c1f})
### Bron

CAauthenticatie::get() in
*ui/include/classes/api/services/CAuthentication.php*.

[comment]: # ({/35bf4f57-b8be0c1f})
