[comment]: # translation:outdated

[comment]: # ({61354f69-e73a60a4})
# > Authenticatie-object

De volgende objecten zijn direct gerelateerd aan de `authenticatie` API.

[comment]: # ({/61354f69-e73a60a4})

[comment]: # ({966f240a-2df9176c})
### Authenticatie

Het authenticatie object heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|authentication\_type|integer|Standaardverificatie.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Intern;<br>1 - LDAP.|
|http\_auth\_enabled|integer|HTTP-verificatie inschakelen.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Uitschakelen;<br>1 - Inschakelen.|
|http\_login\_form|integer|Standaard aanmeldingsformulier.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Zabbix aanmeldingsformulier;<br>1 - HTTP aanmeldingsformulier.|
|http\_strip\_domains|string|Domeinnaam verwijderen.|
|http\_case\_sensitive|integer|HTTP hoofdlettergevoelig inloggen.<br><br>Mogelijke waarden:<br>0 - Uit;<br>1 - *(standaard)* Aan.|
|ldap\_configured|integer|Schakel LDAP-verificatie in.<br><br>Mogelijke waarden:<br>0 - Uitschakelen;<br>1 - *(standaard)* Inschakelen.|
|ldap\_host|string|LDAP-host.|
|ldap\_port|geheel getal|LDAP-poort.|
|ldap\_base\_dn|string|LDAP basis-DN.|
|ldap\_search\_attribute|string|LDAP-zoekkenmerk.|
|ldap\_bind\_dn|string|LDAP bind-DN.|
|ldap\_case\_sensitive|integer|LDAP hoofdlettergevoelig inloggen.<br><br>Mogelijke waarden:<br>0 - Uit;<br>1 - *(standaard)* Aan.|
|ldap\_bind\_password|string|LDAP bindwachtwoord.|
|saml\_auth\_enabled|integer|Saml-verificatie inschakelen.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Uitschakelen;<br>1 - Inschakelen.|
|saml\_idp\_entityid|string|SAML IdP entiteit-ID.|
|saml\_sso\_url|string|SAML SSO-service-URL.|
|saml\_slo\_url|string|SAML SLO-service-URL.|
|saml\_username\_attribute|string|SAML gebruikersnaam kenmerk.|
|saml\_sp\_entityid|string|SAML SP entiteits-ID.|
|saml\_nameid\_format|string|SAML SP naam ID formaat.|
|saml\_sign\_messages|integer|SAML-berichten ondertekenen.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Berichten niet ondertekenen;<br>1 - Berichten ondertekenen.|
|saml\_sign\_assertions|integer|SAML onderteken beweringen.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Onderteken beweringen niet;<br>1 - Onderteken beweringen.|
|saml\_sign\_authn\_requests|integer|SAML onderteken AuthN-verzoeken.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Onderteken geen AuthN-verzoeken;<br>1 - Onderteken AuthN-verzoeken. |
|saml\_sign\_logout\_requests|integer|SAML-onderteken uitlog verzoeken.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Onderteken geen uitlog verzoeken;<br>1 - Onderteken uitlogverzoeken. |
|saml\_sign\_logout\_responses|integer|SAML afmeld reacties ondertekenen.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Afmeld reacties niet ondertekenen;<br>1 - Afmeldings reacties ondertekenen. |
|saml\_encrypt\_nameid|integer|SAML versleutelt naam-ID.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Naam-ID niet versleutelen;<br>1 - Naam-ID versleutelen.|
|saml\_encrypt\_assertions|integer|SAML versleutelt beweringen.<br><br>Mogelijke waarden:<br>0 - *(standaard)* Versleutel geen beweringen;<br>1 - Versleutel beweringen.|
|saml\_case\_sensitive|integer|SAML hoofdlettergevoelig inloggen.<br><br>Mogelijke waarden:<br>0 - Uit;<br>1 - *(standaard)* Aan.|
|passwd\_min\_length|integer|Vereist minimale wachtwoord lengte.<br><br>Mogelijk waarden bereik: 1-70<br>8 - *default*|
|passwd\_check\_rules|integer|Regels voor wachtwoord controle.<br><br>Mogelijke bitmap waarden zijn:<br>0 - controleer wachtwoord lengte;<br>1 - controleer of wachtwoord Latijnse letters en kleine letters gebruikt;<br >2 - controleer of wachtwoord cijfers gebruikt;<br>4 - controleer of wachtwoord speciale tekens gebruikt;<br>8 - *(standaard)* controleer of wachtwoord niet in de lijst met veelgebruikte wachtwoorden staat, geen woordafleidingen bevat "Zabbix" of gebruikersnaam, achternaam of gebruikersnaam.|

[comment]: # ({/966f240a-2df9176c})
