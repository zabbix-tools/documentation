[comment]: # translation:outdated

[comment]: # ({250d2c62-a68f24c8})
# Waarschuwing

Deze klasse is ontworpen om met waarschuwingen te werken.

Objectreferenties:\

- [Alert](/manual/api/reference/alert/object#alert)

Beschikbare methoden:\

- [alert.get](/manual/api/reference/alert/get) - waarschuwingen ophalen

[comment]: # ({/250d2c62-a68f24c8})
