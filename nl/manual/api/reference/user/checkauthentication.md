[comment]: # translation:outdated

[comment]: # ({new-43f8bc43})
# user.checkAuthentication

[comment]: # ({/new-43f8bc43})

[comment]: # ({new-a9b9d697})
### Description

`object user.checkAuthentication`

This method checks and prolongs user session.

::: noteimportant
Calling **user.checkAuthentication** method
prolongs user session by default.
:::

[comment]: # ({/new-a9b9d697})

[comment]: # ({new-49a0b391})
### Parameters

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|extend|boolean|Default value: "true". Setting it's value to "false" allows to check session without extending it's lifetime. Supported since Zabbix 4.0.|
|sessionid|string|User session id.|

[comment]: # ({/new-49a0b391})

[comment]: # ({new-a8544e3e})
### Return values

`(object)` Returns an object containing information about user.

[comment]: # ({/new-a8544e3e})

[comment]: # ({new-e79e2236})
### Examples

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.checkAuthentication",
    "params": {
        "sessionid": "673b8ba11562a35da902c66cf5c23fa2"
    },
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userid": "1",
        "username": "Admin",
        "name": "Zabbix",
        "surname": "Administrator",
        "url": "",
        "autologin": "1",
        "autologout": "0",
        "lang": "ru_RU",
        "refresh": "0",
        "theme": "default",
        "attempt_failed": "0",
        "attempt_ip": "127.0.0.1",
        "attempt_clock": "1355919038",
        "rows_per_page": "50",
        "timezone": "Europe/Riga",
        "roleid": "3",
        "type": 3,
        "sessionid": "673b8ba11562a35da902c66cf5c23fa2"
        "debug_mode": 0,
        "userip": "127.0.0.1",
        "gui_access": 0
    },
    "id": 1
}
```

::: noteclassic
Response is similar to
[User.login](/manual/api/reference/user/login) call response with
"userData" parameter set to true (the difference is that user data is
retrieved by session id and not by username / password).
:::

[comment]: # ({/new-e79e2236})

[comment]: # ({new-97f5ed87})
### Source

CUser::checkAuthentication() in
*ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-97f5ed87})
