<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/api/reference/user/checkauthentication.md">
    <body>
      <trans-unit id="43f8bc43" xml:space="preserve">
        <source># user.checkAuthentication</source>
      </trans-unit>
      <trans-unit id="a9b9d697" xml:space="preserve">
        <source>### Description

`object user.checkAuthentication`

This method checks and prolongs user session.

::: noteimportant
Calling the **user.checkAuthentication** method with the parameter `sessionid`
set prolongs user session by default.
:::</source>
      </trans-unit>
      <trans-unit id="49a0b391" xml:space="preserve">
        <source>### Parameters

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|extend|boolean|Whether to prolong the user session.&lt;br&gt;&lt;br&gt;Default value: "true".&lt;br&gt;Setting the value to "false" allows to check the user session without prolonging it.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *supported* if `sessionid` is set|
|sessionid|string|User session ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `token` is not set|
|secret|string|Random 32 characters string. Is generated on user login.|
|token|string|User [API token](/manual/web_interface/frontend_sections/users/api_tokens).&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `sessionid` is not set|</source>
      </trans-unit>
      <trans-unit id="a8544e3e" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing information about user.</source>
      </trans-unit>
      <trans-unit id="e79e2236" xml:space="preserve">
        <source>### Examples

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "user.checkAuthentication",
    "params": {
        "sessionid": "673b8ba11562a35da902c66cf5c23fa2"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "userid": "1",
        "username": "Admin",
        "name": "Zabbix",
        "surname": "Administrator",
        "url": "",
        "autologin": "1",
        "autologout": "0",
        "lang": "ru_RU",
        "refresh": "0",
        "theme": "default",
        "attempt_failed": "0",
        "attempt_ip": "127.0.0.1",
        "attempt_clock": "1355919038",
        "rows_per_page": "50",
        "timezone": "Europe/Riga",
        "roleid": "3",
        "type": 3,
        "userdirectoryid": "0",
        "ts_provisioned": "0",
        "sessionid": "673b8ba11562a35da902c66cf5c23fa2",
        "debug_mode": 0,
        "secret": "0e329b933e46984e49a5c1051ecd0751",
        "userip": "127.0.0.1",
        "gui_access": 0,
        "deprovisioned": false,
        "auth_type": 0
    },
    "id": 1
}
```

::: noteclassic
Response is similar to
[User.login](/manual/api/reference/user/login) call response with
"userData" parameter set to true (the difference is that user data is
retrieved by session id and not by username / password).
:::</source>
      </trans-unit>
      <trans-unit id="97f5ed87" xml:space="preserve">
        <source>### Source

CUser::checkAuthentication() in
*ui/include/classes/api/services/CUser.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
