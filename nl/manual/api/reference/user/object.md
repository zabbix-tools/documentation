[comment]: # translation:outdated

[comment]: # ({new-0834cf88})
# > User object

The following objects are directly related to the `user` API.

[comment]: # ({/new-0834cf88})

[comment]: # ({new-0ecc45be})
### User

The user object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|userid|string|*(readonly)* ID of the user.|
|**username**<br>*(required)*|string|User name.|
|attempt\_clock|timestamp|*(readonly)* Time of the last unsuccessful login attempt.|
|attempt\_failed|integer|*(readonly)* Recent failed login attempt count.|
|attempt\_ip|string|*(readonly)* IP address from where the last unsuccessful login attempt came from.|
|autologin|integer|Whether to enable auto-login.<br><br>Possible values:<br>0 - *(default)* auto-login disabled;<br>1 - auto-login enabled.|
|autologout|string|User session life time. Accepts seconds and time unit with suffix. If set to 0s, the session will never expire.<br><br>Default: 15m.|
|lang|string|Language code of the user's language, for example, `en_GB`.<br><br>Default: `default` - system default.|
|name|string|Name of the user.|
|refresh|string|Automatic refresh period. Accepts seconds and time unit with suffix.<br><br>Default: 30s.|
|rows\_per\_page|integer|Amount of object rows to show per page.<br><br>Default: 50.|
|surname|string|Surname of the user.|
|theme|string|User's theme.<br><br>Possible values:<br>`default` - *(default)* system default;<br>`blue-theme` - Blue;<br>`dark-theme` - Dark.|
|url|string|URL of the page to redirect the user to after logging in.|
|timezone|string|User's time zone, for example, `Europe/London`, `UTC`.<br><br>Default: `default` - system default.<br><br>For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).|
|**roleid**<br>*(required)*|string|Role ID of the user.|

[comment]: # ({/new-0ecc45be})

[comment]: # ({new-1ad487be})
### Media

The media object has the following properties.

|Property|Type|Description|
|--------|----|-----------|
|**mediatypeid**<br>(required)|string|ID of the media type used by the media.|
|**sendto**<br>(required)|string/array|Address, user name or other identifier of the recipient.<br><br>If type of [Media type](/manual/api/reference/mediatype/object#mediatype) is e-mail, values are represented as array. For other types of [Media types](/manual/api/reference/mediatype/object#mediatype), value is represented as a string.|
|**active**|integer|Whether the media is enabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|**severity**|integer|Trigger severities to send notifications about.<br><br>Severities are stored in binary form with each bit representing the corresponding severity. For example, 12 equals 1100 in binary and means, that notifications will be sent from triggers with severities warning and average.<br><br>Refer to the [trigger object page](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.<br><br>Default: 63|
|**period**|string|Time when the notifications can be sent as a [time period](/manual/appendix/time_period) or user macros separated by a semicolon.<br><br>Default: 1-7,00:00-24:00|

[comment]: # ({/new-1ad487be})
