[comment]: # translation:outdated

[comment]: # ({new-4fff4611})
# Problem

This class is designed to work with problems.

Object references:\

-   [Problem](/manual/api/reference/problem/object#problem)

Available methods:\

-   [problem.get](/manual/api/reference/problem/get) - retrieving
    problems

[comment]: # ({/new-4fff4611})
