[comment]: # translation:outdated

[comment]: # ({new-fde2c9b2})
# report.update

[comment]: # ({/new-fde2c9b2})

[comment]: # ({new-3aeb5e9e})
### Description

`object report.update(object/array reports)`

This method allows to update existing scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user type. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-3aeb5e9e})

[comment]: # ({new-e2e33f61})
### Parameters

`(object/array)` Scheduled report properties to be updated.

The `reportid` property must be defined for each scheduled report, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard scheduled report
properties](object#report) the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|users|object/array of objects|[Users](object#users) to replace the current users assigned to the scheduled report.|
|user\_groups|object/array of objects|[User groups](object#user_groups) to replace the current user groups assigned to the scheduled report.|

[comment]: # ({/new-e2e33f61})

[comment]: # ({new-de75bc91})
### Return values

`(object)` Returns an object containing the IDs of the updated scheduled
reports under the `reportids` property.

[comment]: # ({/new-de75bc91})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3f9e0b32})
#### Disabling scheduled report

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "report.update",
    "params": {
        "reportid": "1",
        "status": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3f9e0b32})

[comment]: # ({new-c52572c8})
### See also

-   [Users](object#users)
-   [User groups](object#user_groups)

[comment]: # ({/new-c52572c8})

[comment]: # ({new-74d81ce5})
### Source

CReport::update() in *ui/include/classes/api/services/CReport.php*.

[comment]: # ({/new-74d81ce5})
