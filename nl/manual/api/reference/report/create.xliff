<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/api/reference/report/create.md">
    <body>
      <trans-unit id="9793e988" xml:space="preserve">
        <source># report.create</source>
      </trans-unit>
      <trans-unit id="a8301e64" xml:space="preserve">
        <source>### Description

`object report.create(object/array reports)`

This method allows to create new scheduled reports.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="869414cf" xml:space="preserve">
        <source>### Parameters

`(object/array)` Scheduled reports to create.

Additionally to the [standard scheduled report
properties](object#report), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|users|object/array of objects|[Users](object#users) to send the report to.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `user_groups` is not set|
|user\_groups|object/array of objects|[User groups](object#user_groups) to send the report to.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if `users` is not set|</source>
      </trans-unit>
      <trans-unit id="05ef7a50" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created scheduled
reports under the `reportids` property. The order of the returned IDs
matches the order of the passed scheduled reports.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="1cfccf99" xml:space="preserve">
        <source>#### Creating a scheduled report

Create a weekly report that will be prepared for the previous week every
Monday-Friday at 12:00 from 2021-04-01 to 2021-08-31.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "report.create",
    "params": {
        "userid": "1",
        "name": "Weekly report",
        "dashboardid": "1",
        "period": "1",
        "cycle": "1",
        "start_time": "43200",
        "weekdays": "31",
        "active_since": "2021-04-01",
        "active_till": "2021-08-31",
        "subject": "Weekly report",
        "message": "Report accompanying text",
        "status": "1",
        "description": "Report description",
        "users": [
            {
                "userid": "1",
                "access_userid": "1",
                "exclude": "0"
            },
            {
                "userid": "2",
                "access_userid": "0",
                "exclude": "1"
            }
        ],
        "user_groups": [
            {
                "usrgrpid": "7",
                "access_userid": "0"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "reportids": [
            "1"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="c52572c8" xml:space="preserve">
        <source>### See also

-   [Users](object#users)
-   [User groups](object#user_groups)</source>
      </trans-unit>
      <trans-unit id="f8046691" xml:space="preserve">
        <source>### Source

CReport::create() in *ui/include/classes/api/services/CReport.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
