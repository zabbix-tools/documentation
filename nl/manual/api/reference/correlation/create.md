[comment]: # translation:outdated

[comment]: # ({0bd1cff6-fbeccd23})
# correlatie.creëren

[comment]: # ({/0bd1cff6-fbeccd23})

[comment]: # ({ba3cf48c-c06fb521})
### Beschrijving

`objectcorrelatie.create(object/array-correlaties)`

Deze methode maakt het mogelijk om nieuwe correlaties te creëren.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/ba3cf48c-c06fb521})

[comment]: # ({2f0bbded-8a55e636})
### Parameters

`(object/array)` Te maken correlaties.

Naast de [standaardcorrelatie
eigenschappen](object#correlatie), accepteert de methode het volgende:
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|**operations**<br>(vereist)|array|Correlatie [bewerkingen](/manual/api/reference/correlation/object#correlation_operation) die moet worden gemaakt voor de correlatie.|
|**filter**<br>(vereist)|object|Correlation [filter](/manual/api/reference/correlation/object#correlation_filter) object voor de correlatie.|

[comment]: # ({/2f0bbded-8a55e636})

[comment]: # ({b3f2d7dc-88660193})
### Retourwaarden

`(object)` Retourneert een object met de ID's van de gemaakte
correlaties onder de eigenschap 'correlationids'. De volgorde van de
geretourneerde ID's komen overeen met de volgorde van de doorgegeven correlaties.

[comment]: # ({/b3f2d7dc-88660193})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({af8bfc96-7325d5f8})
#### Create a new event tag correlation

Maak een correlatie met behulp van evaluatiemethode 'EN/OF' met één voorwaarde
en één operatie. Standaard is de correlatie ingeschakeld.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new event tag correlation",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "type": 1,
                    "tag": "ok"
                }
            ]
        },
        "operations": [
            {
                "type": 0
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/af8bfc96-7325d5f8})

[comment]: # ({96e89cc8-a12df7f6})
#### Een aangepast expressiefilter gebruiken

Maak een correlatie die een aangepaste filtervoorwaarde gebruikt. De
formule-ID's "A" of "B" zijn willekeurig gekozen. Voorwaarde type zal
wees "Hostgroep" met operator "<>".
Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new host group correlation",
        "description": "a custom description",
        "status": 0,
        "filter": {
            "evaltype": 3,
            "formula": "A or B",
            "conditions": [
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "A"
                },
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "B"
                }
            ]
        },
        "operations": [
            {
                "type": 1
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/96e89cc8-a12df7f6})

[comment]: # ({37ca3f7a-e7d15410})
### Zie ook

- [Correlatiefilter](object#correlatiefilter)
- [Correlatiebewerking](object#correlatie_bewerking)

[comment]: # ({/37ca3f7a-e7d15410})

[comment]: # ({2c2064f6-934ba89b})
### Bron

CCorrelatie::create() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/2c2064f6-934ba89b})
