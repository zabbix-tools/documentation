[comment]: # translation:outdated

[comment]: # ({a34d7c69-7416ad19})
# correlatie.get

[comment]: # ({/a34d7c69-7416ad19})

[comment]: # ({863e1638-858936aa})
### Beschrijving

`integer/array correlation.get(object parameters)`

De methode maakt het mogelijk om correlaties op te halen volgens de gegeven
parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/863e1638-858936aa})

[comment]: # ({61572d58-06a650a0})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|correlationids|string/array|Retourneer alleen correlaties met de opgegeven ID's.|
|selectFilter|query|Retourneer een eigenschap [filter](/manual/api/reference/correlation/object#correlation_filter) met de correlatievoorwaarden.|
|selectOperations|query|Retourneer een eigenschap [operations](/manual/api/reference/correlation/object#correlation_operation) met de correlatiebewerkingen.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `correlationid`, `name` en `status`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/61572d58-06a650a0})

[comment]: # ({32334ca9-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` is
    gebruikt.

[comment]: # ({/32334ca9-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({bdf4089a-d14c4f20})
#### Correlaties ophalen

Haal alle geconfigureerde correlaties op samen met correlatie
omstandigheden en operaties. Het filter gebruikt het evaluatietype "en/of",

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectFilter": "extend"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "correlationid": "1",
            "name": "Correlation 1",
            "description": "",
            "status": "0",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "conditions": [
                    {
                        "type": "3",
                        "oldtag": "error",
                        "newtag": "ok",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A"
            },
            "operations": [
                {
                    "type": "0"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/bdf4089a-d14c4f20})

[comment]: # ({dacb2151-e7d15410})
### Zie ook

- [Actiefilter](object#action_filter)
- [Actiebewerking](object#action_operation)

[comment]: # ({/dacb2151-e7d15410})

[comment]: # ({92f752e7-bbab04bd})
### Bron

CCorrelatie::get() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/92f752e7-bbab04bd})
