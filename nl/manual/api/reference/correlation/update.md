[comment]: # translation:outdated

[comment]: # ({48b740de-c71a2e54})
# correlatie.update

[comment]: # ({/48b740de-c71a2e54})

[comment]: # ({4ae25d06-b9562236})
### Beschrijving

`objectcorrelatie.update(object/array-correlaties)`

Met deze methode kunnen bestaande correlaties worden bijgewerkt.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/4ae25d06-b9562236})

[comment]: # ({693f7fbe-326bc80e})
### Parameters

`(object/array)` Correlatie-eigenschappen moeten worden bijgewerkt.

De eigenschap `correlationid` moet voor elke correlatie worden gedefinieerd, alle
andere eigenschappen zijn optioneel. Alleen de doorgegeven eigenschappen worden
bijgewerkt, blijven alle andere ongewijzigd.

Naast de [standaardcorrelatie
eigenschappen](object#correlatie), accepteert de methode het volgende:
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|filter|object|Correlatie [filter](/manual/api/reference/correlation/object#correlation_filter) object om het huidige filter te vervangen.|
|operations|array|Correlatie [bewerkingen](/manual/api/reference/correlation/object#correlation_operation) om bestaande bewerkingen te vervangen.|

[comment]: # ({/693f7fbe-326bc80e})

[comment]: # ({1e4664b6-13e1dc13})
### Retourwaarden

`(object)` Retourneert een object met de ID's van de bijgewerkte
correlaties onder de eigenschap 'correlationids'.

[comment]: # ({/1e4664b6-13e1dc13})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({8e402769-67bb2dfb})
#### Correlatie uitschakelen

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "status": "1"
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/8e402769-67bb2dfb})

[comment]: # ({d0deade3-d6ef1778})
#### Voorwaarden vervangen, maar de evaluatiemethode behouden

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.update",
    "params": {
        "correlationid": "1",
        "filter": {
            "conditions": [
                {
                    "type": 3,
                    "oldtag": "error",
                    "newtag": "ok"
                }
            ]
        }
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/d0deade3-d6ef1778})

[comment]: # ({cbb27b8c-e7d15410})
### Zie ook

- [Correlatiefilter](object#correlatie_filter)
- [Correlatiebewerking](object#correlatie_bewerking)

[comment]: # ({/cbb27b8c-e7d15410})

[comment]: # ({6b0eeb46-40fa665f})
### Bron

CCorrelatie::update() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/6b0eeb46-40fa665f})
