[comment]: # translation:outdated

[comment]: # ({07c95f67-89029829})
# correlatie.delete

[comment]: # ({/07c95f67-89029829})

[comment]: # ({87806ad9-4922e104})
### Beschrijving

`object correlatie.delete(array correlationids)`

Met deze methode kunnen correlaties worden verwijderd.

::: noteclassic
Deze methode is alleen beschikbaar voor het gebruikerstype *Super admin*.
Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie
[Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/87806ad9-4922e104})

[comment]: # ({90347802-b445698c})
### Parameters

`(array)` ID's van de correlaties die moeten worden verwijderd.

[comment]: # ({/90347802-b445698c})

[comment]: # ({dbed8ae0-855af217})
### Retourwaarden

`(object)` Retourneert een object met de ID's van de verwijderde
correlaties onder de eigenschap 'correlationids'.

[comment]: # ({/dbed8ae0-855af217})

[comment]: # ({e5ef042b-c9f65268})
### Voorbeeld

[comment]: # ({/e5ef042b-c9f65268})

[comment]: # ({a20112bc-bfc49991})
#### Meerdere correlaties verwijderen

Verwijder twee correlaties.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlaionids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/a20112bc-bfc49991})

[comment]: # ({476f9d01-b0713c0f})
### Bron

CCorrelatie::delete() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/476f9d01-b0713c0f})
