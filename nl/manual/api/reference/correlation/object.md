[comment]: # translation:outdated

[comment]: # ({6899c633-e205e572})
# > Correlatie-object

De volgende objecten zijn direct gerelateerd aan de `correlatie` API.

[comment]: # ({/6899c633-e205e572})

[comment]: # ({51718e48-935ba6ef})
### Correlatie

Het correlatieobject heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|correlationid|string|*(alleen-lezen)* ID van de correlatie.|
|**name**<br>(verplicht)|string|Naam van de correlatie.|
|description|string|Beschrijving van de correlatie.|
|status|geheel getal|Of de correlatie is ingeschakeld of uitgeschakeld.<br><br>Mogelijke waarden zijn:<br>0 - *(standaard)* ingeschakeld;<br>1 - uitgeschakeld.|

[comment]: # ({/51718e48-935ba6ef})

[comment]: # ({0e1df4cd-d12c2169})
### Correlatiebewerking

Het correlatie bewerkings object definieert een bewerking die
uitgevoerd wanneer een correlatie wordt uitgevoerd. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**type**<br>(vereist)|geheel getal|Type bewerking.<br><br>Mogelijke waarden:<br>0 - oude gebeurtenissen sluiten;<br>1 - nieuwe gebeurtenis sluiten.|

[comment]: # ({/0e1df4cd-d12c2169})

[comment]: # ({5d90f460-d0662dba})
### Correlatiefilter

Het correlatie filter object definieert een set voorwaarden die
voldaan om de geconfigureerde correlatiebewerkingen uit te voeren. Het heeft de
volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**evaltype**<br>(vereist)|geheel getal|Filterconditie-evaluatiemethode.<br><br>Mogelijke waarden:<br>0 - en/of;<br>1 - en;<br>2 - of;<br>3 - aangepaste uitdrukking.|
|**conditions**<br>(vereist)|array|Set van filtervoorwaarden om te gebruiken voor het filteren van resultaten.|
|eval\_formula|string|*(alleen-lezen)* Gegenereerde expressie die zal worden gebruikt voor het evalueren van filtervoorwaarden. De expressie bevat ID's die verwijzen naar specifieke filtervoorwaarden door middel van de `formulaid`. De waarde van `eval_formula` is gelijk aan de waarde van `formula` voor filters met een aangepaste expressie.|
|formula|string|Door de gebruiker gedefinieerde expressie die moet worden gebruikt voor het evalueren van voorwaarden van filters met een aangepaste expressie. De expressie moet ID's bevatten die verwijzen naar specifieke filtervoorwaarden door middel van de `formulaid`. De ID's die in de expressie worden gebruikt, moeten exact overeenkomen met de ID's die zijn gedefinieerd in de filtervoorwaarden: geen enkele voorwaarde mag ongebruikt blijven of worden weggelaten.<br><br>Vereist voor aangepaste expressie filters.|

[comment]: # ({/5d90f460-d0662dba})

[comment]: # ({a5681896-72ee5420})
#### Correlatie filter conditie

Het voorwaarde object van het correlatiefilter definieert een specifieke voorwaarde
die moet worden gecontroleerd voordat de correlatiebewerkingen worden uitgevoerd.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**type**<br>(verplicht)|geheel getal|Type voorwaarde.<br><br>Mogelijke waarden:<br>0 - oude gebeurtenis tag;<br>1 - nieuwe gebeurtenis tag;<br>2 - nieuwe gebeurtenis hostgroep;<br>3 - gebeurtenis tag paar;<br>4 - oude gebeurtenis tag waarde;<br>5 - nieuwe gebeurtenis tag waarde.|
|tag|string|Event-tag (oud of nieuw). Vereist wanneer type voorwaarde is: 0, 1, 4, 5.|
|groupid|string|Hostgroep-ID. Vereist wanneer type voorwaarde is: 2.|
|oldtag|string|Oude gebeurtenis tag. Vereist wanneer type voorwaarde is: 3.|
|newtag|string|Oude gebeurtenis tag. Vereist wanneer type voorwaarde is: 3.|
|value|string|Event tag (oud of nieuw) waarde. Vereist wanneer type conditie is: 4, 5.|
|formulaid|string|Willekeurige unieke ID die wordt gebruikt om te verwijzen naar de voorwaarde vanuit een aangepaste expressie. Mag alleen hoofdletters bevatten. De ID moet door de gebruiker worden gedefinieerd bij het wijzigen van filtervoorwaarden, maar zal opnieuw worden gegenereerd wanneer ze daarna worden aangevraagd.|
|operator|geheel getal|Conditie-operator.<br><br>Vereist wanneer type voorwaarde is: 2, 4, 5.|

::: notetip
Om beter te begrijpen hoe u filters kunt gebruiken met verschillende
soorten uitdrukkingen, zie voorbeelden op de
[correlation.get](get#retrieve_correlations) en
[correlation.create](create#using_a_custom_expression_filter) methode
Pagina's.
:::

De volgende operatoren en waarden worden ondersteund voor elke voorwaarde:
type.

|Voorwaarde|Voorwaarde naam|Ondersteunde operators|Verwachte waarde|
|---------|--------------|-------------------|---- ----------|
|2|Hostgroep|=, <>|Hostgroep-ID.|
|4|Oude gebeurtenistagwaarde|=, <>, leuk, niet leuk|string|
|5|Nieuwe gebeurtenistagwaarde|=, <>, leuk, niet leuk|string|

[comment]: # ({/a5681896-72ee5420})
