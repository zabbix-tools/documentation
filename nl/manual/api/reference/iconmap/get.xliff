<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/api/reference/iconmap/get.md">
    <body>
      <trans-unit id="8e76cf8c" xml:space="preserve">
        <source># iconmap.get</source>
      </trans-unit>
      <trans-unit id="04265e54" xml:space="preserve">
        <source>### Description

`integer/array iconmap.get(object parameters)`

The method allows to retrieve icon maps according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="efcde208" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|iconmapids|string/array|Return only icon maps with the given IDs.|
|sysmapids|string/array|Return only icon maps that are used in the given maps.|
|selectMappings|query|Return a [`mappings`](/manual/api/reference/iconmap/object#icon_mapping) property with the icon mappings used.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `iconmapid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="016ff9fc" xml:space="preserve">
        <source>#### Retrieve an icon map

Retrieve all data about icon map "3".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "iconmap.get",
    "params": {
        "iconmapids": "3",
        "output": "extend",
        "selectMappings": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "mappings": [
                {
                    "iconmappingid": "3",
                    "iconmapid": "3",
                    "iconid": "6",
                    "inventory_link": "1",
                    "expression": "server",
                    "sortorder": "0"
                },
                {
                    "iconmappingid": "4",
                    "iconmapid": "3",
                    "iconid": "10",
                    "inventory_link": "1",
                    "expression": "switch",
                    "sortorder": "1"
                }
            ],
            "iconmapid": "3",
            "name": "Host type icons",
            "default_iconid": "2"
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="8df05cf6" xml:space="preserve">
        <source>### See also

-   [Icon mapping](object#icon_mapping)</source>
      </trans-unit>
      <trans-unit id="2420d0f0" xml:space="preserve">
        <source>### Source

CIconMap::get() in *ui/include/classes/api/services/CIconMap.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
