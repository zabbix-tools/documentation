[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.create

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.create(object/array templateGroups)`

This method allows to create new template groups.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-6102edd8})
### Parameters

`(object/array)` Template groups to create. The method accepts template groups
with the [standard template group properties](object#template_group).

[comment]: # ({/new-6102edd8})

[comment]: # ({new-76d53254})
### Return values

`(object)` Returns an object containing the IDs of the created template
groups under the `groupids` property. The order of the returned IDs
matches the order of the passed template groups.

[comment]: # ({/new-76d53254})

[comment]: # ({new-e8653e4d})
### Examples

[comment]: # ({/new-e8653e4d})

[comment]: # ({new-1aca44e8})
#### Creating a template group

Create a template group called "Templates/Databases".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templategroup.create",
    "params": {
        "name": "Templates/Databases"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "107820"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-1aca44e8})

[comment]: # ({new-225e8cd3})
### Source

CTemplateGroup::create() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/new-225e8cd3})
