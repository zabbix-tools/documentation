[comment]: # translation:outdated

[comment]: # ({907e6be5-e35cf787})
# Ontdekte gastheer

Deze klasse is ontworpen om te werken met ontdekte hosts.

Objectreferenties:\

- [Ontdekt
    host](/manual/api/reference/dhost/object#discovered_host)

Beschikbare methoden:\

- [dhost.get](/manual/api/reference/dhost/get) - ontdek ontdekt
    gastheren

[comment]: # ({/907e6be5-e35cf787})
