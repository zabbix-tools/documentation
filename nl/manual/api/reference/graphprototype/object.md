[comment]: # translation:outdated

[comment]: # ({new-07413af4})
# > Graph prototype object

The following objects are directly related to the `graphprototype` API.

[comment]: # ({/new-07413af4})

[comment]: # ({new-c613d83a})
### Graph prototype

The graph prototype object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|graphid|string|*(readonly)* ID of the graph prototype.|
|**height**<br>(required)|integer|Height of the graph prototype in pixels.|
|**name**<br>(required)|string|Name of the graph prototype.|
|**width**<br>(required)|integer|Width of the graph prototype in pixels.|
|graphtype|integer|Graph prototypes's layout type.<br><br>Possible values:<br>0 - *(default)* normal;<br>1 - stacked;<br>2 - pie;<br>3 - exploded.|
|percent\_left|float|Left percentile.<br><br>Default: 0.|
|percent\_right|float|Right percentile.<br><br>Default: 0.|
|show\_3d|integer|Whether to show discovered pie and exploded graphs in 3D.<br><br>Possible values:<br>0 - *(default)* show in 2D;<br>1 - show in 3D.|
|show\_legend|integer|Whether to show the legend on the discovered graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|show\_work\_period|integer|Whether to show the working time on the discovered graph.<br><br>Possible values:<br>0 - hide;<br>1 - *(default)* show.|
|templateid|string|*(readonly)* ID of the parent template graph prototype.|
|yaxismax|float|The fixed maximum value for the Y axis.|
|yaxismin|float|The fixed minimum value for the Y axis.|
|ymax\_itemid|string|ID of the item that is used as the maximum value for the Y axis.|
|ymax\_type|integer|Maximum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|
|ymin\_itemid|string|ID of the item that is used as the minimum value for the Y axis.|
|ymin\_type|integer|Minimum value calculation method for the Y axis.<br><br>Possible values:<br>0 - *(default)* calculated;<br>1 - fixed;<br>2 - item.|
|discover|integer|Graph prototype discovery status.<br><br>Possible values:<br>0 - *(default)* new graphs will be discovered;<br>1 - new graphs will not be discovered and existing graphs will be marked as lost.|
|uuid|string|Universal unique identifier, used for linking imported graph prototypes to already existing ones. Used only for graph prototypes on templates. Auto-generated, if not given.<br><br>For update operations this field is *readonly*.|

[comment]: # ({/new-c613d83a})
