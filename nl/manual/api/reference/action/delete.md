[comment]: # translation:outdated

[comment]: # ({73b31b7c-3d0c523c})
# actie.verwijderen

[comment]: # ({/73b31b7c-3d0c523c})

[comment]: # ({8cf57417-c056f978})
### Beschrijving

`object action.delete(array actionIds)`

Met deze methode kunnen acties worden verwijderd.

::: noteclassic
Deze methode is alleen beschikbaar voor *Admin* en *Super admin*
gebruikers. Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de gebruikersrol
instellingen. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/8cf57417-c056f978})

[comment]: # ({32503e46-f66aff82})
### Parameters

`(array)` ID's van de acties die moeten worden verwijderd.

[comment]: # ({/32503e46-f66aff82})

[comment]: # ({0f72bfc8-04c6deae})
### Retourwaarden

`(object)` Retourneert een object dat de ID's van de verwijderde acties bevat
onder de eigenschap `actionids`.

[comment]: # ({/0f72bfc8-04c6deae})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({0aa8dc89-36abb590})
#### Meerdere acties verwijderen

Twee acties verwijderen.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.delete",
    "params": [
        "17",
        "18"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17",
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/0aa8dc89-36abb590})

[comment]: # ({a969ace5-70602a31})
### Bron

CAction::delete() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/a969ace5-70602a31})
