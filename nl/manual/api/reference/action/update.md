[comment]: # translation:outdated

[comment]: # ({263dbead-e4b5aac4})
# actie.update

[comment]: # ({/263dbead-e4b5aac4})

[comment]: # ({0473109c-b1673b49})
### Beschrijving

`object action.update(object/array acties)`

Met deze methode kunnen bestaande acties worden bijgewerkt.

::: noteclassic
Deze methode is alleen beschikbaar voor *Admin* en *Super admin*
gebruikers. Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de gebruikersrol
instellingen. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/0473109c-b1673b49})

[comment]: # ({6e5957b9-703e85ce})
### Parameters

`(object/array)` Actie-eigenschappen die moeten worden bijgewerkt.

De eigenschap `actionid` moet worden gedefinieerd voor elke actie, alle andere
eigenschappen zijn optioneel. Alleen de doorgegeven eigenschappen worden bijgewerkt, allemaal
andere blijven ongewijzigd.

Naast de [standaard actie-eigenschappen](object#actie), is de
methode accepteert de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|filter|object|Actie [filter](/manual/api/reference/action/object#action_filter) object om het huidige filter te vervangen.|
|operations|array|Actie [bewerkingen](/manual/api/reference/action/object#action_operation) om bestaande bewerkingen te vervangen.|
|recovery\_operations|array|Actie [herstelbewerkingen](/manual/api/reference/action/object#action_recovery_operation) om bestaande herstelbewerkingen te vervangen.|
|update\_operations|array|Actie [update-bewerkingen](/manual/api/reference/action/object#action_update_operation) om bestaande update-bewerkingen te vervangen.|

[comment]: # ({/6e5957b9-703e85ce})

[comment]: # ({565b0df0-8cbaa103})
### Retourwaarden

`(object)` Retourneert een object dat de ID's van de bijgewerkte acties bevat
onder de eigenschap `actionids`.

[comment]: # ({/565b0df0-8cbaa103})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({4fcdeddd-77b78ec5})
#### Disable action

Schakel actie uit, dat wil zeggen, stel de status in op "1".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": "2",
        "status": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4fcdeddd-77b78ec5})

[comment]: # ({dacb2151-755496f7})
### Zie ook

- [Actiefilter](object#action_filter)
- [Actiebewerking](object#action_operation)

[comment]: # ({/dacb2151-755496f7})

[comment]: # ({ff686386-df9f1d35})
### Bron

CAction::update() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/ff686386-df9f1d35})
