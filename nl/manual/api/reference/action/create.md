[comment]: # translation:outdated

[comment]: # ({new-c7f202d4})
# action.create

[comment]: # ({/new-c7f202d4})

[comment]: # ({6c4697ae-71c93aa0})
### Beschrijving

`object action.create(object/array acties)`

Deze methode maakt het mogelijk om nieuwe acties te creëren.

::: noteclassic
Deze methode is alleen beschikbaar voor *Admin* en *Super admin*
gebruikers. Machtigingen om de methode aan te roepen kunnen worden ingetrokken in de gebruikersrol
instellingen. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/6c4697ae-71c93aa0})

[comment]: # ({453ce8c2-5a9be3c9})
### Parameters

`(object/array)` Te maken acties.

Naast de [standaard actie-eigenschappen](object#actie), accepteert de
methode de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|filter|object|Actie [filter](/manual/api/reference/action/object#action_filter) object voor de actie.|
|operations|array|Actie [bewerkingen](/manual/api/reference/action/object#action_operation) om voor de actie aan te maken.|
|recovery\_operations|array|Actie [herstelbewerkingen](/manual/api/reference/action/object#action_recovery_operation) om voor de actie te maken.|
|update\_operations|array|Actie [update-bewerkingen](/manual/api/reference/action/object#action_update_operation) die moet worden gemaakt voor de actie.|

[comment]: # ({/453ce8c2-5a9be3c9})

[comment]: # ({26685b01-2830affd})
### Retourwaarden

`(object)` Retourneert een object dat de ID's van de gemaakte acties bevat
onder de eigenschap `actionids`. De volgorde van de geretourneerde ID's komt overeen
de volgorde van de uitgevoerde acties.

[comment]: # ({/26685b01-2830affd})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({new-afc96865})
#### Maak een triggeractie

Maak een actie die wordt uitgevoerd wanneer een trigger van host "10084" met het woord "geheugen" in zijn naam, in de probleemtoestand gaat. De actie
moet eerst een bericht sturen naar alle gebruikers in gebruikersgroep "7". Als het evenement
niet binnen 4 minuten is opgelost, wordt script "3" uitgevoerd op alle hosts in
groep "2". Bij trigger herstel zal het alle gebruikers op de hoogte stellen die een bericht hebben ontvangen over het probleem eerder. Bij trigger-update, word een bericht met
aangepast onderwerp en tekst naar iedereen gestuurd die een bevestiging heeft achtergelaten en
opmerkingen via alle mediatypes.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Activeer actie",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084"
                },
                {
                    "conditiontype": 3,
                    "operator": 2,
                    "value": "geheugen"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            },
            {
                "bewerkingstype": 1,
                "esc_step_from": 3,
                "esc_step_to": 4,
                "evaltype": 0,
                "opconditions": [
                    {
                        "conditiontype": 14,
                        "operator": 0,
                        "value": "0"
                    }
                ],
                "opcommand_grp": [
                    {
                        "groupid": "2"
                    }
                ],
                "opcommand": {
                    "scriptid": "3"
                }
            }
        ],
        "recovery_operations": [
            {
                "operationtype": "11",
                "opmessage": {
                    "default_msg": 1
                }
            }
        ],
        "update_operations": [
            {
                "operationtype": "12",
                "opmessage": {
                    "default_msg": 0,
                    "message": "Berichttekst voor aangepaste updatebewerking",
                    "subject": "Onderwerp van bericht over aangepaste updatebewerking"
                }
            }
        ],
        "pause_suppressed": "0",
        "notify_if_canceled": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-afc96865})

[comment]: # ({new-53bdbb4b})
#### Maak een ontdekkingsactie

Maak een actie die ontdekte hosts koppelt aan sjabloon "10091".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Ontdekkingsactie",
        "eventsource": 1,
        "status": 0,
        "esc_period": "0s",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 21,
                    "value": "1"
                },
                {
                    "conditiontype": 10,
                    "value": "2"
                }
            ]
        },
        "operations": [
            {
                "esc_step_from": 1,
                "esc_period": "0s",
                "optemplate": [
                    {
                        "templateid": "10091"
                    }
                ],
                "operationtype": 6,
                "esc_step_to": 1
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-53bdbb4b})

[comment]: # ({new-de84bbc4})
#### Een aangepast expressie filter gebruiken

Maak een trigger actie die een aangepaste filtervoorwaarde gebruikt. De
actie moet een bericht verzenden voor elke trigger met een hogere ernst of
gelijk aan "Waarschuwing" voor hosts "10084" en "10106". De formule-ID's "A",
"B" en "C" zijn willekeurig gekozen.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Activeer actie",
        "eventsource": 0,
        "status": 0,
        "esc_periode": "2m",
        "filter": {
            "evaltype": 3,
            "formula": "A and (B or C)",
            "conditions": [
                {
                    "conditiontype": 4,
                    "operator": 5,
                    "value": "2",
                    "formulaid": "A"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084",
                    "formulaid": "B"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10106",
                    "formulaid": "C"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            }
        ],
        "pause_suppressed": "0",
        "notify_if_canceled": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-de84bbc4})

[comment]: # ({new-b81a306b})
#### Regel voor automatische agent registratie maken

Voeg een host toe aan de hostgroep "Linux-servers" wanneer de hostnaam "SRV" bevat
of metadata bevat "CentOS".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Registreer Linux-servers",
        "eventsource": "2",
        "status": "0",
        "filter": {
            "evaltype": "2",
            "formula": "A or B",
            "condition": [
                {
                    "conditiontype": "22",
                    "operator": "2",
                    "value": "SRV",
                    "value2": "",
                    "formulaid": "B"
                },
                {
                    "conditiontype": "24",
                    "operator": "2",
                    "value": "CentOS",
                    "value2": "",
                    "formulaid": "A"
                }
            ]
        },
        "operations": [
            {
                "actionid": "9",
                "operationtype": "4",
                "esc_period": "0",
                "esc_step_from": "1",
                "esc_step_to": "1",
                "evaltype": "0",
                "opgroup": [
                    {
                        "operationid": "16",
                        "groepid": "2"
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            19
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b81a306b})

[comment]: # ({dacb2151-755496f7})
### Zie ook

- [Actiefilter](object#action_filter)
- [Actiebewerking](object#action_operation)

[comment]: # ({/dacb2151-755496f7})

[comment]: # ({259b911d-32335876})
### Bron

CAction::create() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/259b911d-32335876})
