[comment]: # translation:outdated

[comment]: # ({new-2293b018})
# action.get

[comment]: # ({/new-2293b018})

[comment]: # ({cbc12493-42834e1b})
### Beschrijving

`integer/array action.get(object parameters)`

De methode maakt het mogelijk om acties op te halen volgens de gegeven parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/cbc12493-42834e1b})

[comment]: # ({3255ca5d-f6c19c90})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|actionids|string/array|Retourneer alleen acties met de opgegeven ID's.|
|groupids|string/array|Retourneer alleen acties die de opgegeven hostgroepen gebruiken in actievoorwaarden.|
|hostids|string/array|Retourneer alleen acties die de opgegeven hosts gebruiken in actievoorwaarden.|
|triggerids|string/array|Retourneer alleen acties die de gegeven triggers in actievoorwaarden gebruiken.|
|mediatypeids|string/array|Retourneer alleen acties die de opgegeven mediatypen gebruiken om berichten te verzenden.|
|usrgrpids|string/array|Retourneer alleen acties die zijn geconfigureerd om berichten naar de opgegeven gebruikersgroepen te verzenden.|
|userids|string/array|Retourneer alleen acties die zijn geconfigureerd om berichten naar de opgegeven gebruikers te verzenden.|
|scriptids|string/array|Retourneer alleen acties die zijn geconfigureerd om de opgegeven scripts uit te voeren.|
|selectFilter|query|Retourneer een eigenschap [filter](/manual/api/reference/action/object#action_filter) met het actievoorwaardefilter.|
|selectOperations|query|Retourneer een eigenschap [operations](/manual/api/reference/action/object#action_operation) met actiebewerkingen.|
|selectRecoveryOperations|query|Retourneer een eigenschap [recovery\_operations](/manual/api/reference/action/object#action_recovery_operation) met actieherstelbewerkingen.|
|selectUpdateOperations|query|Retourneer een eigenschap [update\_operations](/manual/api/reference/action/object#action_update_operation) met actie-updatebewerkingen.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `actionid`, `name` en `status`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorde|string/array|^|
|startZoeken|booleaans|^|

[comment]: # ({/3255ca5d-f6c19c90})

[comment]: # ({ef8f6fed-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` heeft
    gebruikt.

[comment]: # ({/ef8f6fed-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({e48068b9-f7d2e806})
#### Triggeracties ophalen

Alle geconfigureerde triggeracties ophalen samen met actievoorwaarden
en operaties.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend",
        "selectRecoveryOperations": "extend",
        "selectUpdateOperations": "extend",
        "selectFilter": "extend",
        "filter": {
            "eventsource": 0
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "actionid": "3",
            "name": "Meld problemen aan Zabbix-beheerders",
            "eventsource": "0",
            "status": "1",
            "esc_period": "1u",
            "pause_suppressed": "1",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "condition": [],
                "eval_formula": ""
            },
            "operations": [
                {
                    "operationid": "3",
                    "actionid": "3",
                    "operationtype": "0",
                    "esc_periode": "0",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "opmessage": [
                        {
                            "default_msg": "1",
                            "subject": "",
                            "message": "",
                            "mediatypeid" => "0"
                        }
                    ],
                    "opmessage_grp": [
                        {
                            "usrgrpid": "7"
                        }
                    ]
                }
            ],
            "recovery_operations": [
                {
                    "operationid": "7",
                    "actionid": "3",
                    "operationtype": "11",
                    "evaltype": "0",
                    "opcondition": [],
                    "opmessage": {
                        "default_msg": "0",
                        "subject": "{TRIGGER.STATUS}: {TRIGGER.NAME}",
                        "message": "Trigger: {TRIGGER.NAME}\r\nTriggerstatus: {TRIGGER.STATUS}\r\nTrigger-ernst: {TRIGGER.SEVERITY}\r\nTrigger-URL: {TRIGGER.URL}\r\n\ r\nItemwaarden:\r\n\r\n1. {ITEM.NAME1} ({HOST.NAME1}:{ITEM.KEY1}): {ITEM.VALUE1}\r\n2. {ITEM.NAME2} ({ HOST.NAME2}:{ITEM.KEY2}): {ITEM.VALUE2}\r\n3 {ITEM.NAME3} ({HOST.NAME3}:{ITEM.KEY3}): {ITEM.VALUE3}\r\n \r\nOriginele gebeurtenis-ID: {EVENT.ID}",
                        "mediatypeid": "0"
                    }
                }
            ],
            "update_operations": [
                {
                    "operationid": "31",
                    "operationtype": "12",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "1",
                        "subject": "",
                        "message": "",
                        "mediatypeid": "0"
                    }
                },
                {
                    "operationid": "32",
                    "operationtype": "0",
                    "evaltype": "0",
                    "opmessage": {
                        "default_msg": "0",
                        "subject": "Bijgewerkt: {TRIGGER.NAME}",
                        "message": "{USER.FULLNAME} heeft het probleem bijgewerkt op {EVENT.UPDATE.DATE} {EVENT.UPDATE.TIME} met het volgende bericht:\r\n{EVENT.UPDATE.MESSAGE}\r\n\r\ nHuidige probleemstatus is {EVENT.STATUS}",
                        "mediatypeid": "1"
                    },
                    "opmessage_grp": [
                        {
                            "usrgrpid": "7"
                        }
                    ],
                    "opmessage_usr": []
                },
                {
                    "operationid": "33",
                    "operationtype": "1",
                    "evaltype": "0",
                    "opcommand": {
                        "scriptid": "3"
                    },
                    "opcommand_hst": [
                        {
                            "hostid": "10084"
                        }
                    ],
                    "opcommand_grp": []
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/e48068b9-f7d2e806})

[comment]: # ({77d205ca-d96da9cf})
#### Ontdekkingsacties ophalen

Haal alle geconfigureerde ontdekkingsacties op samen met actie
omstandigheden en operaties. Het filter gebruikt het evaluatietype "and", dus
de eigenschap `formula` is leeg en `eval_formula` is gegenereerd
automatisch.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "action.get",
    "params": {
        "output": "extend",
        "selectOperations": "extend"
        "selectFilter": "extend",
        "filter": {
            "eventsource": 1
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "actionid": "2",
            "name": "Automatische detectie. Linux-servers.",
            "eventsource": "1",
            "status": "1",
            "esc_period": "0s",
            "pause_suppressed": "1",
            "filter": {
                "evaltype": "0",
                "formula": "",
                "condition": [
                    {
                        "conditiontype": "10",
                        "operator": "0",
                        "value": "0",
                        "value2": "",
                        "formulaid": "B"
                    },
                    {
                        "conditiontype": "8",
                        "operator": "0",
                        "value": "9",
                        "value2": "",
                        "formulaid": "C"
                    },
                    {
                        "conditiontype": "12",
                        "operator": "2",
                        "value": "Linux",
                        "value2": "",
                        "formulaid": "A"
                    }
                ],
                "eval_formula": "A and B and C"
            },
            "operations": [
                {
                    "operationid": "1",
                    "actionid": "2",
                    "operationtype": "6",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "optemplate": [
                        {
                            "templateid": "10001"
                        }
                    ]
                },
                {
                    "operationid": "2",
                    "actionid": "2",
                    "operationtype": "4",
                    "esc_period": "0s",
                    "esc_step_from": "1",
                    "esc_step_to": "1",
                    "evaltype": "0",
                    "opconditions": [],
                    "opgroup": [
                        {
                            "groupid": "2"
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/77d205ca-d96da9cf})

[comment]: # ({dacb2151-755496f7})
### Zie ook

- [Actiefilter](object#action_filter)
- [Actiebewerking](object#action_operation)

[comment]: # ({/dacb2151-755496f7})

[comment]: # ({dc8f2315-025b824e})
### Bron

CAction::get() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/dc8f2315-025b824e})
