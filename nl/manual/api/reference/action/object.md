[comment]: # translation:outdated

[comment]: # ({4c660401-502c65c3})
# > Actie-object

De volgende objecten zijn direct gerelateerd aan de `action` API.

[comment]: # ({/4c660401-502c65c3})

[comment]: # ({new-73117797})
### Actie

Het actieobject heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|actionid|string|*(alleen-lezen)* ID van de actie.|
|**esc\_period**<br>(vereist)|string|Standaard bewerkings stap duur. Moet minimaal 60 seconden zijn. Accepteert seconden, tijdseenheid met achtervoegsel en gebruikersmacro.<br><br>Houd er rekening mee dat escalaties alleen worden ondersteund voor trigger- en interne acties. Bij trigger acties worden escalaties niet ondersteund bij probleemherstel- en update bewerkingen.|
|**eventsource**<br>(vereist)|geheel getal|*(constant)* Type gebeurtenissen dat de actie zal verwerken.<br><br>Raadpleeg de [event "source" eigenschap](/manual/api /reference/event/object#event) voor een lijst met ondersteunde gebeurtenis typen.|
|**name**<br>(verplicht)|string|Naam van de actie.|
|status|geheel getal|Of de actie is ingeschakeld of uitgeschakeld.<br><br>Mogelijke waarden:<br>0 - *(standaard)* ingeschakeld;<br>1 - uitgeschakeld.|
|pause\_suppressed|integer|Of de escalatie moet worden onderbroken tijdens onderhoudsperioden of niet.<br><br>Mogelijke waarden:<br>0 - Escalatie niet pauzeren;<br>1 - *(standaard)* Escalatie pauzeren. <br><br>Houd er rekening mee dat deze parameter alleen geldig is voor trigger acties.|
|notify\_if\_canceled|integer|Al dan niet melden wanneer escalatie wordt geannuleerd.<br><br>Mogelijke waarden:<br>0 - Niet melden wanneer escalatie wordt geannuleerd;<br>1 - *(standaard)* Waarschuwen wanneer escalatie is geannuleerd.<br><br>Houd er rekening mee dat deze parameter alleen geldig is voor trigger acties.|

[comment]: # ({/new-73117797})

[comment]: # ({new-8757165b})
### Actie bewerking

Het actie bewerkingsobject definieert een bewerking die wordt uitgevoerd
wanneer een actie wordt uitgevoerd. Het heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|operationid|string|*(alleen-lezen)* ID van de actiebewerking.|
|**operationtype**<br>(vereist)|geheel getal|Type bewerking.<br><br>Mogelijke waarden:<br>0 - bericht verzenden;<br>1 - globaal script;<br>2 - toevoegen host;<br>3 - verwijder host;<br>4 - voeg toe aan hostgroep;<br>5 - verwijder uit hostgroep;<br>6 - link naar sjabloon;<br>7 - ontkoppel van sjabloon;<br >8 - host inschakelen;<br>9 - host uitschakelen;<br>10 - hostinventarismodus instellen.<br><br>Houd er rekening mee dat alleen typen '0' en '1' worden ondersteund voor triggeracties, alleen '0 ' wordt ondersteund voor interne acties. Alle typen worden ondersteund voor detectie- en automatische registratieacties.|
|actionid|string|ID van de actie waartoe de bewerking behoort.|
|esc\_period|string|Duur van een escalatie stap in seconden. Moet langer zijn dan 60 seconden. Accepteert seconden, tijdseenheid met achtervoegsel en gebruikersmacro. Indien ingesteld op 0 of 0s, wordt de standaard actie-escalatie periode gebruikt.<br><br>Standaard: 0s.<br><br>Houd er rekening mee dat escalaties alleen worden ondersteund voor trigger- en interne acties. Bij trigger acties worden escalaties niet ondersteund bij probleemherstel- en update bewerkingen.|
|esc\_step\_from|integer|Stap om escalatie te starten.<br><br>Standaard: 1.<br><br>Houd er rekening mee dat escalaties alleen worden ondersteund voor trigger- en interne acties. Bij trigger acties worden escalaties niet ondersteund bij probleemherstel- en update bewerkingen.|
|esc\_step\_to|integer|Stap om escalatie te beëindigen op.<br><br>Standaard: 1.<br><br>Houd er rekening mee dat escalaties alleen worden ondersteund voor trigger- en interne acties. Bij trigger acties worden escalaties niet ondersteund bij probleemherstel- en update bewerkingen.|
|evaltype|geheel getal|Bewerkingsvoorwaarde evaluatiemethode.<br><br>Mogelijke waarden:<br>0 - *(standaard)* AND / OR;<br>1 - AND;<br>2 - OR.|
|opcommand|object|Object dat gegevens bevat over een globaal script dat door de bewerking wordt uitgevoerd.<br><br>Elk object heeft één volgende eigenschap: `scriptid` - *(string)* ID van het script.<br><br>Vereist voor globale script bewerkingen.|
|opcommand\_grp|array|Hostgroepen om globale scripts op uit te voeren.<br><br>Elk object heeft de volgende eigenschappen:<br>`opcommand_grpid` - *(string, alleen-lezen)* ID van het object;<br> `operationid` - *(string)* ID van de bewerking;<br>`groupid` - *(string)* ID van de hostgroep.<br><br>Vereist voor algemene script bewerkingen als `opcommand_hst` niet is ingesteld .|
|opcommand\_hst|array|Host om globale scripts op uit te voeren.<br><br>Elk object heeft de volgende eigenschappen:<br>`opcommand_hstid` - *(string, alleen-lezen)* ID van het object;<br>` operationsid` - *(string)* ID van de bewerking;<br>`hostid` - *(string)* ID van de host; indien ingesteld op 0 wordt de opdracht uitgevoerd op de huidige host.<br><br>Vereist voor algemene script bewerkingen als `opcommand_grp` niet is ingesteld.|
|opconditions|array|Bewerkingsvoorwaarden gebruikt voor trigger acties.<br><br>Het bewerkingsvoorwaarde-object wordt [hieronder in detail beschreven](/manual/api/reference/action/object#action_operation_condition).|
|opgroup|array|Hostgroepen om hosts aan toe te voegen.<br><br>Elk object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de bewerking;<br>`groupid` - * (string)* ID van de hostgroep.<br><br>Vereist voor bewerkingen "toevoegen aan hostgroep" en "verwijderen uit hostgroep".|
|opmessage|object|Object dat de gegevens bevat over het bericht dat door de bewerking is verzonden.<br><br>Het bewerkings bericht object wordt [hieronder in detail beschreven](/manual/api/reference/action/object#action_operation_message).< br><br>Vereist voor berichtbewerkingen.|
|opmessage\_grp|array|Gebruikersgroepen om berichten naar te verzenden.<br><br>Elk object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de bewerking;<br>`usrgrpid` - *(string)* ID van de gebruikersgroep.<br><br>Vereist voor berichtbewerkingen als `opmessage_usr` niet is ingesteld.|
|opmessage\_usr|array|Gebruikers om berichten naar te verzenden.<br><br>Elk object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de operatie;<br>`userid` - *(string)* ID van de gebruiker.<br><br>Vereist voor bericht bewerkingen als `opmessage_grp` niet is ingesteld.|
|optemplate|array|Sjablonen om de hosts aan te koppelen.<br><br>Elk object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de operatie;<br>`templateid` - *(string)* ID van de sjabloon.<br><br>Vereist voor bewerkingen "link naar sjabloon" en "ontkoppelen van sjabloon".|
|opinventory|object|Voorraadmodus zet host op.<br><br>Object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de bewerking;<br>`inventory_mode` - *(string )* Voorraadmodus.<br><br>Vereist voor bewerkingen voor 'Host inventaris modus instellen'.|

[comment]: # ({/new-8757165b})

[comment]: # ({d8dbdbe7-4aeccf9f})
#### Actie operatie bericht

Het bewerkings bericht object bevat gegevens over het bericht dat
worden verzonden door de operatie.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|default\_msg|integer|Of de standaardtekst en het onderwerp van het actiebericht moeten worden gebruikt.<br><br>Mogelijke waarden:<br>0 - gebruik de gegevens van de bewerking;<br>1 - *(standaard)* gebruik de gegevens van het mediatype.|
|mediatypeid|string|ID van het mediatype dat zal worden gebruikt om het bericht te verzenden.|
|message|string|Bewerkings bericht tekst.|
|subject|string|Bewerking bericht onderwerp.|

[comment]: # ({/d8dbdbe7-4aeccf9f})

[comment]: # ({a78ef5ad-6762fd66})
#### Actie operatie voorwaarde

Het actie bewerkingsvoorwaarde object definieert een voorwaarde die moet zijn
voldaan om de huidige bewerking uit te voeren. Het heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|opconditionid|string|*(alleen-lezen)* ID van de actievoorwaarde|
|**conditiontype**<br>(vereist)|geheel getal|Type voorwaarde.<br><br>Mogelijke waarden:<br>14 - gebeurtenis bevestigd.|
|**waarde**<br>(verplicht)|string|Waarde om mee te vergelijken.|
|operationid|string|*(alleen-lezen)* ID van de bewerking.|
|operator|geheel getal|Conditie-operator.<br><br>Mogelijke waarden:<br>0 - *(standaard)* =.|

De volgende operatoren en waarden worden ondersteund voor elke bewerking:
soort toestand.

|Voorwaarde|Voorwaarde naam|Ondersteunde operators|Verwachte waarde|
|---------|--------------|-------------------|---- ----------|
|14|Gebeurtenis bevestigd|=|Of de gebeurtenis is bevestigd.<br><br>Mogelijke waarden:<br>0 - niet bevestigd;<br>1 - bevestigd.|

[comment]: # ({/a78ef5ad-6762fd66})

[comment]: # ({new-d1e6762b})
### Actie herstel bewerking

Het actie herstel bewerkings object definieert een bewerking die wordt
uitgevoerd wanneer een probleem is opgelost. Herstelbewerkingen zijn mogelijk
voor trigger acties en interne acties. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|operationid|string|*(alleen-lezen)* ID van de actiebewerking.|
|**operationtype**<br>(vereist)|geheel getal|Type bewerking.<br><br>Mogelijke waarden voor trigger acties:<br>0 - bericht verzenden;<br>1 - algemeen script;<br> 11 - breng alle betrokkenen op de hoogte.<br><br>Mogelijke waarden voor interne acties:<br>0 - stuur bericht;<br>11 - breng alle betrokkenen op de hoogte.|
|actionid|string|ID van de actie waartoe de herstelbewerking behoort.|
|opcommand|object|Object bevat gegevens over een globaal actie type script dat door de bewerking wordt uitgevoerd.<br><br>Elk object heeft één volgende eigenschap: `scriptid` - *(string)* ID van het actie type script.<br> <br>Vereist voor algemene scriptbewerkingen.|
|opcommand\_grp|array|Hostgroepen om globale scripts op uit te voeren.<br><br>Elk object heeft de volgende eigenschappen:<br>`opcommand_grpid` - *(string, alleen-lezen)* ID van het object;<br> `operationid` - *(string)* ID van de bewerking;<br>`groupid` - *(string)* ID van de hostgroep.<br><br>Vereist voor algemene script bewerkingen als `opcommand_hst` niet is ingesteld .|
|opcommand\_hst|array|Host om globale scripts op uit te voeren.<br><br>Elk object heeft de volgende eigenschappen:<br>`opcommand_hstid` - *(string, alleen-lezen)* ID van het object;<br>` operationsid` - *(string)* ID van de bewerking;<br>`hostid` - *(string)* ID van de host; indien ingesteld op 0 wordt de opdracht uitgevoerd op de huidige host.<br><br>Vereist voor algemene scriptbewerkingen als `opcommand_grp` niet is ingesteld.|
|opmessage|object|Object dat de gegevens bevat over het bericht dat door de herstelbewerking is verzonden.<br><br>Het bewerkings bericht object is [hierboven in detail beschreven](/manual/api/reference/action/object#action_operation_message). <br><br>Vereist voor bericht bewerkingen.|
|opmessage\_grp|array|Gebruikersgroepen om berichten naar te verzenden.<br><br>Elk object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de bewerking;<br>`usrgrpid` - *(string)* ID van de gebruikersgroep.<br><br>Vereist voor bericht bewerkingen als `opmessage_usr` niet is ingesteld.|
|opmessage\_usr|array|Gebruikers om berichten naar te verzenden.<br><br>Elk object heeft de volgende eigenschappen:<br>`operationid` - *(string)* ID van de operatie;<br>`userid` - *(string)* ID van de gebruiker.<br><br>Vereist voor bericht bewerkingen als `opmessage_grp` niet is ingesteld.|

[comment]: # ({/new-d1e6762b})

[comment]: # ({new-f41f59ea})
### Actie update bewerking

Het actie update bewerkings object definieert een bewerking die wordt
uitgevoerd wanneer een probleem wordt bijgewerkt (becommentarieerd, erkend,
ernst gewijzigd of handmatig gesloten). Update bewerkingen zijn mogelijk
voor trigger acties. Het heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|operationid|string|*(alleen-lezen)* ID van de actiebewerking.|
|**operationtype**<br>(vereist)|geheel getal|Type bewerking.<br><br>Mogelijke waarden voor trigger acties:<br>0 - bericht verzenden;<br>1 - algemeen script;<br> 12 - breng alle betrokkenen op de hoogte.|
|opcommand|object|Object dat gegevens bevat over een globaal actie type script dat door de bewerking wordt uitgevoerd.<br><br>Elk object heeft één volgende eigenschap: `scriptid` - *(string)* ID van het actie type script.<br> <br>Vereist voor algemene script bewerkingen.|
|opcommand\_grp|array|Hostgroepen om globale scripts op uit te voeren.<br><br>Elk object heeft de volgende eigenschappen:<br>`groupid` - *(string)* ID van de hostgroep.<br><br> br>Vereist voor globale script bewerkingen als `opcommand_hst` niet is ingesteld.|
|opcommand\_hst|array|Host om globale scripts op uit te voeren.<br><br>Elk object heeft de volgende eigenschappen:<br>`hostid` - *(string)* ID van de host; indien ingesteld op 0 wordt de opdracht uitgevoerd op de huidige host.<br><br>Vereist voor algemene script bewerkingen als `opcommand_grp` niet is ingesteld.|
|opmessage|object|Object dat de gegevens bevat over het bericht dat is verzonden door de update bewerking.<br><br>Het bewerkings bericht object is [hierboven in detail beschreven](/manual/api/reference/action/object#action_operation_message). |
|opmessage\_grp|array|Gebruikersgroepen om berichten naar te verzenden.<br><br>Elk object heeft de volgende eigenschappen:<br>`usrgrpid` - *(string)* ID van de gebruikersgroep.<br><br >Alleen vereist voor bewerkingen 'bericht verzenden' als 'opmessage_usr' niet is ingesteld.<br>Wordt genegeerd voor bewerkingen 'update bericht verzenden'.|
|opmessage\_usr|array|Gebruikers om berichten naar te verzenden.<br><br>Elk object heeft de volgende eigenschappen:<br>`userid` - *(string)* ID van de gebruiker.<br><br>Vereist alleen voor bewerkingen voor 'bericht verzenden' als 'opmessage_grp' niet is ingesteld.<br>Wordt genegeerd voor bewerkingen voor 'update-bericht verzenden'.|

[comment]: # ({/new-f41f59ea})

[comment]: # ({6e3e3dcc-0d8b3bb1})
### Actiefilter

Het actiefilter object definieert een reeks voorwaarden waaraan moet worden voldaan om:
de geconfigureerde actiebewerkingen uitvoeren. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**voorwaarden**<br>(vereist)|array|Set van filtervoorwaarden om te gebruiken voor het filteren van resultaten.|
|**evaltype**<br>(vereist)|geheel getal|Filterconditie-evaluatiemethode.<br><br>Mogelijke waarden:<br>0 - en/of;<br>1 - en;<br>2 - of;<br>3 - aangepaste uitdrukking.|
|eval\_formula|string|*(alleen-lezen)* Gegenereerde expressie die zal worden gebruikt voor het evalueren van filtervoorwaarden. De expressie bevat ID's die verwijzen naar specifieke filtervoorwaarden door middel van de `formulaid`. De waarde van `eval_formula` is gelijk aan de waarde van `formula` voor filters met een aangepaste expressie.|
|formula|string|Door de gebruiker gedefinieerde expressie die moet worden gebruikt voor het evalueren van voorwaarden van filters met een aangepaste expressie. De expressie moet ID's bevatten die verwijzen naar specifieke filtervoorwaarden door middel van de `formulaid`. De ID's die in de expressie worden gebruikt, moeten exact overeenkomen met de ID's die zijn gedefinieerd in de filtervoorwaarden: geen enkele voorwaarde mag ongebruikt blijven of worden weggelaten.<br><br>Vereist voor aangepaste expressie filters.|

[comment]: # ({/6e3e3dcc-0d8b3bb1})

[comment]: # ({new-dd860074})
#### Actiefilter voorwaarde

Het actiefilter voorwaarde object definieert een specifieke voorwaarde die:
moet worden gecontroleerd voordat de actiebewerkingen worden uitgevoerd.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|conditionid|string|*(alleen-lezen)* ID van de actievoorwaarde.|
|**conditiontype**<br>(vereist)|geheel getal|Type voorwaarde.<br><br>Mogelijke waarden voor trigger acties:<br>0 - host groep;<br>1 - host;<br>2 - trigger;<br>3 - trigger naam;<br>4 - ernst van trigger;<br>6 - tijdsperiode;<br>13 - host sjabloon;<br>16 - probleem is onderdrukt;<br>25 - gebeurtenis tag;<br>26 - gebeurtenis tag waarde.<br><br>Mogelijke waarden voor ontdekkingsacties:<br>7 - host-IP;<br>8 - ontdekt servicetype;<br>9 - ontdekte servicepoort;< br>10 - ontdekkingsstatus;<br>11 - uptime of duur van downtime;<br>12 - ontvangen waarde;<br>18 - ontdekkingsregel;<br>19 - ontdekkingscontrole;<br>20 - proxy;<br >21 - discovery-object.<br><br>Mogelijke waarden voor automatische registratieacties:<br>20 - proxy;<br>22 - host naam;<br>24 - host metadata.<br><br>Mogelijke waarden voor interne acties:<br>0 - host groep;<br>1 - host;<br>13 - host sjabloon;<br>23 - gebeurtenis type;<br>25 - gebeurtenis tag;<br>26 - waarde van gebeurtenis tag .<br><br>Mogelijke waarden voor serviceacties:<br>25 - gebeurtenis tag;<br>26 - gebeurtenis tag waarde;<br>27 - service;<br >28 - servicenaam.|
|**waarde**<br>(verplicht)|string|Waarde om mee te vergelijken.|
|value2<br>|string|Secundaire waarde om mee te vergelijken. Vereist voor trigger acties wanneer het conditietype *26* is.|
|actionid|string|*(alleen-lezen)* ID van de actie waartoe de voorwaarde behoort.|
|formulaid|string|Willekeurige unieke ID die wordt gebruikt om te verwijzen naar de voorwaarde vanuit een aangepaste expressie. Mag alleen hoofdletters bevatten. De ID moet door de gebruiker worden gedefinieerd bij het wijzigen van filtervoorwaarden, maar wordt opnieuw gegenereerd wanneer ze daarna worden aangevraagd.|
|operator|geheel getal|Condition-operator.<br><br>Mogelijke waarden:<br>0 - *(standaard)* is gelijk aan;<br>1 - is niet gelijk aan;<br>2 - bevat;<br>3 - bevat niet;<br>4 - in;<br>5 - is groter dan of gelijk aan;<br>6 - is kleiner dan of gelijk aan;<br>7 - niet in;<br>8 - komt overeen;<br >9 - komt niet overeen;<br>10 - Ja;<br>11 - Nee.|

::: notetip
Om beter te begrijpen hoe u filters kunt gebruiken met verschillende
soorten uitdrukkingen, zie voorbeelden op de
[action.get](get#retrieve_discovery_actions) en
[action.create](create#using_a_custom_expression_filter) methode
Pagina's.
:::

De volgende operatoren en waarden worden ondersteund voor elke voorwaarde:
type.

|Voorwaarde|Voorwaarde naam|Ondersteunde operators|Verwachte waarde|
|---------|--------------|-------------------|---- ----------|
|0|Hostgroep|is gelijk aan,<br>is niet gelijk aan|Hostgroep ID.|
|1|Host|is gelijk aan,<br>is niet gelijk aan|Host-ID.|
|2|Trigger|is gelijk aan,<br>is niet gelijk aan|Trigger ID.|
|3|Triggernaam|bevat,<br>bevat niet|Trigger naam.|
|4|De ernst van de trigger|is gelijk aan,<br>is niet gelijk aan,<br>is groter dan of gelijk aan,<br>is kleiner dan of gelijk aan|De ernst van de trigger. Raadpleeg de [trigger "severity" eigenschap](/manual/api/reference/trigger/object#trigger) voor een lijst met ondersteunde trigger ernsten.|
|5|Triggerwaarde|is gelijk aan|Triggerwaarde. Raadpleeg de eigenschap [trigger "value"](/manual/api/reference/trigger/object#trigger) voor een lijst met ondersteunde triggerwaarden.|
|6|Tijdsperiode|in, niet in|Tijd waarop de gebeurtenis werd geactiveerd als een [tijdsperiode](/manual/appendix/time_period).|
|7|Host IP|gelijk aan,<br>is niet gelijk aan|Een of meerdere IP-bereiken om te controleren, gescheiden door komma's. Raadpleeg de sectie [configuratie van netwerkdetectie](/manual/discovery/network_discovery/rule) voor meer informatie over ondersteunde indelingen van IP-bereiken.|
|8|Ontdekt servicetype|gelijk aan,<br>is niet gelijk aan|Type ontdekte service. Het type service komt overeen met het type detectie controle dat wordt gebruikt om de service te detecteren. Raadpleeg de [discovery check "type" eigenschap](/manual/api/reference/dcheck/object#discovery_check) voor een lijst met ondersteunde typen.|
|9|Ontdekte servicepoort|is gelijk aan,<br>is niet gelijk aan|Een of meerdere poortbereiken gescheiden door komma's.|
|10|Ontdekkingsstatus|gelijk aan|Status van een ontdekt object.<br><br>Mogelijke waarden:<br>0 - host of service up;<br>1 - host of service down;<br>2 - host of service ontdekt;<br>3 - host of service verloren.|
|11|Duur van uptime of downtime|is groter dan of gelijk aan,<br>is kleiner dan of gelijk aan|Tijd die aangeeft hoe lang het ontdekte object in de huidige status is in seconden.|
|12|Ontvangen waarden|gelijk aan,<br>is niet gelijk aan,<br>is groter dan of gelijk aan,<br>is kleiner dan of gelijk aan,<br>bevat,<br>bevat niet|Waarde die wordt geretourneerd bij het uitvoeren van een Zabbix-agent, SNMPv1-, SNMPv2- of SNMPv3-detectie controle.|
|13|Hostsjabloon|is gelijk aan,<br>is niet gelijk aan|Gekoppelde sjabloon-ID.|
|16|Probleem is onderdrukt|Ja, Nee|Geen waarde vereist: het gebruik van de "Ja"-operator betekent dat het probleem moet worden onderdrukt, "Nee" - niet onderdrukt.|
|18|Ontdekkingsregel|is gelijk aan,<br>is niet gelijk aan|ID van de ontdekkingsregel.|
|19|Ontdekkingscontrole|is gelijk aan,<br>is niet gelijk aan|ID van de ontdekkingscontrole.|
|20|Proxy|is gelijk aan,<br>is niet gelijk aan|ID van de proxy.|
|21|Ontdekkingsobject|gelijk aan|Type object dat de ontdekkingsgebeurtenis heeft geactiveerd.<br><br>Mogelijke waarden:<br>1 - ontdekte host;<br>2 - ontdekte service.|
|22|Hostnaam|bevat,<br>bevat niet,<br>komt overeen,<br>komt niet overeen|Hostnaam.<br>Het gebruik van een reguliere expressie wordt ondersteund voor operators *komt overeen* en *komt niet overeen* in autoregistratie voorwaarden.|
|23|Gebeurtenistype|gelijk aan|Specifieke interne gebeurtenis.<br><br>Mogelijke waarden:<br>0 - item in "niet ondersteund" status;<br>1 - item in "normale" status;<br>2 - LLD-regel in "niet ondersteund" status;<br>3 - LLD-regel in "normale" status;<br>4 - trigger in "onbekende" status;<br>5 - trigger in "normale" status.|
|24|Metadata host|bevat,<br>bevat niet,<br>komt overeen,<br>komt niet overeen|Metadata van de automatisch geregistreerde host.<br>Het gebruik van een reguliere expressie wordt ondersteund voor operators *komt overeen* en *komt niet overeen*.|
|25|Tag|is gelijk aan,<br>is niet gelijk aan,<br>bevat,<br>bevat niet|Gebeurtenis-tag.|
|26|Tagwaarde|is gelijk aan,<br>is niet gelijk aan,<br>bevat,<br>bevat niet|Gebeurtenis tag waarde.|
|27|Service|gelijk aan,<br>is niet gelijk aan|Service-ID.|
|28|Servicenaam|is gelijk aan,<br>is niet gelijk aan|Servicenaam.|

[comment]: # ({/new-dd860074})
