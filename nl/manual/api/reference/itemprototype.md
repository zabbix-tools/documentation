[comment]: # translation:outdated

[comment]: # ({new-7e2bad53})
# Item prototype

This class is designed to work with item prototypes.

Object references:\

-   [Item
    prototype](/manual/api/reference/itemprototype/object#item_prototype)

Available methods:\

-   [itemprototype.create](/manual/api/reference/itemprototype/create) -
    creating new item prototypes
-   [itemprototype.delete](/manual/api/reference/itemprototype/delete) -
    deleting item prototypes
-   [itemprototype.get](/manual/api/reference/itemprototype/get) -
    retrieving item prototypes
-   [itemprototype.update](/manual/api/reference/itemprototype/update) -
    updating item prototypes

[comment]: # ({/new-7e2bad53})
