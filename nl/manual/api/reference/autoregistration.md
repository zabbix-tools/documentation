[comment]: # translation:outdated

[comment]: # ({2cdda25d-17119dd8})
# Automatische registratie

Deze klasse is ontworpen om te werken met automatische registratie.

Objectreferenties:\

- [Autoregistratie](/manual/api/reference/autoregistration/object#autoregistration)

Beschikbare methoden:\

- [autoregistration.get](/manual/api/reference/autoregistration/get) -
    autoregistratie ophalen
- [autoregistration.update](/manual/api/reference/autoregistration/update) -
    automatische registratie bijwerken

[comment]: # ({/2cdda25d-17119dd8})
