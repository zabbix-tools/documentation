[comment]: # translation:outdated

[comment]: # ({bd3e5895-c179cb27})
# dashboard.creëren

[comment]: # ({/bd3e5895-c179cb27})

[comment]: # ({5870411f-00970602})
### Beschrijving

`object dashboard.create(object/array dashboards)`

Deze methode maakt het mogelijk om nieuwe dashboards te maken.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/5870411f-00970602})

[comment]: # ({b2c1539a-df2301b4})
### Parameters

`(object/array)` Dashboards om te maken.

Naast de [standaard dashboardeigenschappen](object#dashboard),
de methode accepteert de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|**pages**<br>(vereist)|array|Dashboard [pages](object#dashboard_page) dat moet worden gemaakt voor het dashboard. Dashboard pagina's worden in dezelfde volgorde geordend als opgegeven. Er is ten minste één dashboardpagina-object vereist voor de eigenschap `pages`.|
|users|array|Dashboard [gebruiker](object#dashboard_user) aandelen die op het dashboard moeten worden gemaakt.|
|userGroups|array|Dashboard [gebruikersgroep](object#dashboard_user_group) aandelen die op het dashboard moeten worden gemaakt.|

[comment]: # ({/b2c1539a-df2301b4})

[comment]: # ({11b67e26-f460a18e})
### Retourwaarden

`(object)` Retourneert een object met de ID's van de gemaakte
dashboards onder de eigenschap `dashboardids`. De volgorde van de geretourneerde
ID's komen overeen met de volgorde van de doorgegeven dashboards.

[comment]: # ({/11b67e26-f460a18e})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({4cf2228e-b33dc2ee})
#### Een dashboard maken

Maak een dashboard met de naam "Mijn dashboard" met één Problems widget met
tags en het gebruik van twee soorten delen (gebruikersgroep en gebruiker) op een enkele
dashboardpagina.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problems",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "service"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "zabbix_server"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": "7",
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": "4",
                "permission": 3
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/4cf2228e-b33dc2ee})

[comment]: # ({17773fc2-299ae089})
### Zie ook

- [Dashboardpagina](object#dashboard_page)
- [Dashboard-widget](object#dashboard_widget)
- [Dashboard-widgetveld](object#dashboard_widget_field)
- [Dashboardgebruiker](object#dashboard_gebruiker)
- [Dashboard gebruikersgroep](object#dashboard_user_group)

[comment]: # ({/17773fc2-299ae089})

[comment]: # ({893d697b-7d488f32})
### Bron

CDashboard::create() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/893d697b-7d488f32})
