[comment]: # translation:outdated

[comment]: # ({4ca463d5-faff6037})
# dashboard.verwijderen

[comment]: # ({/4ca463d5-faff6037})

[comment]: # ({f45e768d-6f29484b})
### Beschrijving

`object dashboard.delete(array dashboardids)`

Met deze methode kunnen dashboards worden verwijderd.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kan worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/f45e768d-6f29484b})

[comment]: # ({3eed0890-4fd471a4})
### Parameters

`(array)` ID's van de dashboards die moeten worden verwijderd.

[comment]: # ({/3eed0890-4fd471a4})

[comment]: # ({feb8548f-c04394d8})
### Retourwaarden

`(object)` Retourneert een object met de ID's van de verwijderde
dashboards onder de eigenschap `dashboardids`.

[comment]: # ({/feb8548f-c04394d8})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({44446d8b-4d5d4a5d})
#### Meerdere dashboards verwijderen

Verwijder twee dashboards

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.delete",
    "params": [
        "2",
        "3"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2",
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/44446d8b-4d5d4a5d})

[comment]: # ({e36fb44c-2bc6b04b})
### Bron

CDashboard::delete() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/e36fb44c-2bc6b04b})
