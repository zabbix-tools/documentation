[comment]: # translation:outdated

[comment]: # ({new-f7884974})
# dashboard.get

[comment]: # ({/new-f7884974})

[comment]: # ({b4613e25-8d36c9b5})
### Beschrijving

`integer/array dashboard.get(objectparameters)`

De methode maakt het mogelijk om dashboards op te halen volgens de gegeven
parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/b4613e25-8d36c9b5})

[comment]: # ({ac32175f-342bbd26})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|dashboardids|string/array|Retourneer alleen dashboards met de opgegeven ID's.|
|selectPages|query|Retourneer een [pages](/manual/api/reference/dashboard/object#dashboard_page) eigenschap met dashboardpagina's, correct geordend.|
|selectUsers|query|Retourneer een eigenschap [users](/manual/api/reference/dashboard/object#dashboard_user) met gebruikers met wie het dashboard is gedeeld.|
|selectUserGroups|query|Retourneer een eigenschap [userGroups](/manual/api/reference/dashboard/object#dashboard_user_group) met gebruikersgroepen waarmee het dashboard wordt gedeeld.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarde is: `dashboardid`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden in detail beschreven op de pagina [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/ac32175f-342bbd26})

[comment]: # ({32334ca9-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` is
    gebruikt.

[comment]: # ({/32334ca9-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({c1f24827-84a10cd6})
#### Ophalen van dashboards via ID

Haal alle data van dashboard "1" en "2" op.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.get",
    "params": {
        "output": "extend",
        "selectPages": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "dashboardids": [
            "1",
            "2"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dashboardid": "1",
            "name": "Dashboard",
            "userid": "1",
            "private": "0",
            "display_period": "30",
            "auto_start": "1",
            "users": [],
            "userGroups": [],
            "pages": [
                {
                    "dashboard_pageid": "1",
                    "name": "",
                    "display_period": "0",
                    "widgets": [
                        {
                            "widgetid": "9",
                            "type": "systeminfo",
                            "name": "",
                            "x": "12",
                            "y": "8",
                            "width": "12",
                            "height": "5",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "8",
                            "type": "problemsbysv",
                            "name": "",
                            "x": "12",
                            "y": "4",
                            "width": "12",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "7",
                            "type": "problemhosts",
                            "name": "",
                            "x": "12",
                            "y": "0",
                            "width": "12",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "6",
                            "type": "discovery",
                            "name": "",
                            "x": "6",
                            "y": "9",
                            "width": "6",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "5",
                            "type": "web",
                            "name": "",
                            "x": "0",
                            "y": "9",
                            "width": "6",
                            "height": "4",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "4",
                            "type": "problems",
                            "name": "",
                            "x": "0",
                            "y": "3",
                            "width": "12",
                            "height": "6",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "3",
                            "type": "favmaps",
                            "name": "",
                            "x": "8",
                            "y": "0",
                            "width": "4",
                            "height": "3",
                            "view_mode": "0",
                            "fields": []
                        },
                        {
                            "widgetid": "1",
                            "type": "favgraphs",
                            "name": "",
                            "x": "0",
                            "y": "0",
                            "width": "4",
                            "height": "3",
                            "view_mode": "0",
                            "fields": []
                        }
                    ]
                },
                {
                    "dashboard_pageid": "2",
                    "name": "",
                    "display_period": "0",
                    "widgets": []
                },
                {
                    "dashboard_pageid": "3",
                    "name": "Custom page name",
                    "display_period": "60",
                    "widgets": []
                }
            ]
        },
        {
            "dashboardid": "2",
            "name": "My dashboard",
            "userid": "1",
            "private": "1",
            "display_period": "60",
            "auto_start": "1",
            "users": [
                {
                    "userid": "4",
                    "permission": "3"
                }
            ],
            "userGroups": [
                {
                    "usrgrpid": "7",
                    "permission": "2"
                }
            ],
            "pages": [
                {
                    "dashboard_pageid": "4",
                    "name": "",
                    "display_period": "0",
                    "widgets": [
                        {
                            "widgetid": "10",
                            "type": "problems",
                            "name": "",
                            "x": "0",
                            "y": "0",
                            "width": "12",
                            "height": "5",
                            "view_mode": "0",
                            "fields": [
                                {
                                    "type": "2",
                                    "name": "groupids",
                                    "value": "4"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/c1f24827-84a10cd6})

[comment]: # ({17773fc2-299ae089})
### Zie ook

- [Dashboardpagina](object#dashboard_page)
- [Dashboard-widget](object#dashboard_widget)
- [Dashboard-widgetveld](object#dashboard_widget_field)
- [Dashboardgebruiker](object#dashboard_gebruiker)
- [Dashboard gebruikersgroep](object#dashboard_user_group)

[comment]: # ({/17773fc2-299ae089})

[comment]: # ({195ec9dd-75a9042e})
### Bron

CDashboard::get() in *ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/195ec9dd-75a9042e})
