[comment]: # translation:outdated

[comment]: # ({new-66fb9690})
# dashboard.update

[comment]: # ({/new-66fb9690})

[comment]: # ({6b5c5f8e-f235159f})
### Beschrijving

`object dashboard.update(object/array dashboards)`

Met deze methode kunnen bestaande dashboards worden bijgewerkt.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/6b5c5f8e-f235159f})

[comment]: # ({f78bc86a-62947aa0})
### Parameters

`(object/array)` Dashboard-eigenschappen moeten worden bijgewerkt.

De eigenschap `dashboardid` moet worden opgegeven voor elk dashboard, alles
andere eigenschappen zijn optioneel. Alleen de opgegeven eigenschappen worden
bijgewerkt.

Naast de [standaard dashboard eigenschappen](object#dashboard),
de methode accepteert de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|pages|array|Dashboard [pages](object#dashboard_page) om de bestaande dashboard pagina's te vervangen.<br><br>Dashboard pagina's worden bijgewerkt door de eigenschap `dashboard_pageid`. Er worden nieuwe dashboard pagina's gemaakt voor objecten zonder de eigenschap 'dashboard_pageid' en de bestaande dashboard pagina's worden verwijderd als ze niet opnieuw worden gebruikt. Dashboard pagina's worden in dezelfde volgorde geordend als opgegeven. Alleen de opgegeven eigenschappen van de dashboard pagina's worden bijgewerkt. Er is ten minste één dashboard pagina object vereist voor de eigenschap `pages`.|
|users|array|Dashboard [gebruiker](object#dashboard_user) deelt om de bestaande elementen te vervangen.|
|userGroups|array|Dashboard [gebruikersgroep](object#dashboard_user_group) deelt om de bestaande elementen te vervangen.|

[comment]: # ({/f78bc86a-62947aa0})

[comment]: # ({c1dc3457-28e89b38})
### Retourwaarden

`(object)` Retourneert een object met de ID's van de bijgewerkte
dashboards onder de eigenschap `dashboardids`.

[comment]: # ({/c1dc3457-28e89b38})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({e403bc06-7fcf7de4})
#### Een dashboard hernoemen

Hernoem een dashboard naar "SQL server status".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "name": "SQL server status"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/e403bc06-7fcf7de4})

[comment]: # ({a09833b6-b8348b8a})
#### Update dashboard pagina's

Hernoem de eerste dashboard pagina, vervang widgets op het tweede dashboard
pagina en voeg een nieuwe pagina toe als de derde. Alle andere dashboard pagina's verwijderen.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "pages": [
            {
                "dashboard_pageid": 1,
                "name": 'Renamed Page'
            },
            {
                "dashboard_pageid": 2,
                "widgets": [
                    {
                        "type": "clock",
                        "x": 0,
                        "y": 0,
                        "width": 4,
                        "height": 3
                    }
                ]
            },
            {
                "display_period": 60
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 2
}
```

[comment]: # ({/a09833b6-b8348b8a})

[comment]: # ({3147bae2-e4a57dd3})
#### Verander dashboard eigenaar

Alleen beschikbaar voor Admins en Super admins.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dashboard.update",
    "params": {
        "dashboardid": "2",
        "userid": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 2
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 2
}
```

[comment]: # ({/3147bae2-e4a57dd3})

[comment]: # ({17773fc2-299ae089})
### Zie ook

- [Dashboardpagina](object#dashboard_page)
- [Dashboard-widget](object#dashboard_widget)
- [Dashboard-widgetveld](object#dashboard_widget_field)
- [Dashboardgebruiker](object#dashboard_gebruiker)
- [Dashboard gebruikersgroep](object#dashboard_user_group)

[comment]: # ({/17773fc2-299ae089})

[comment]: # ({1764520c-456ac32b})
### Bron

CDashboard::update() in
*ui/include/classes/api/services/CDashboard.php*.

[comment]: # ({/1764520c-456ac32b})
