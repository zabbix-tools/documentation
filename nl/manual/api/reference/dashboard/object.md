[comment]: # translation:outdated

[comment]: # ({de3cc06a-a79e0e24})
# > Dashboard-object

De volgende objecten zijn direct gerelateerd aan de `dashboard` API.

[comment]: # ({/de3cc06a-a79e0e24})

[comment]: # ({39f1ab00-63e4049e})
### Dashboard

Het dashboardobject heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|dashboardid|string|*(alleen-lezen)* ID van het dashboard.|
|**name**<br>(verplicht)|string|Naam van het dashboard.|
|userid|string|Gebruikers-ID dashboard eigenaar.|
|private|integer|Type dashboard delen.<br><br>Mogelijke waarden:<br>0 - openbaar dashboard;<br>1 - *(standaard)* privé dashboard.|
|display\_period|integer|Standaard pagina weergave periode (in seconden).<br><br>Mogelijke waarden: 10, 30, 60, 120, 600, 1800, 3600.<br><br>Standaard: 30.|
|auto\_start|integer|Slideshow automatisch starten.<br><br>Mogelijke waarden:<br>0 - diavoorstelling niet automatisch starten;<br>1 - *(standaard)* diavoorstelling automatisch starten.|

[comment]: # ({/39f1ab00-63e4049e})

[comment]: # ({62b6b809-b88f0a6a})
### Dashboard-pagina

Het dashboardpagina-object heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|dashboard\_pageid|string|*(alleen-lezen)* ID van de dashboard pagina.|
|name|string|Dashboard pagina naam.<br><br>Standaard: lege string.|
|display\_period|integer|Dashboard pagina weergave periode (in seconden).<br><br>Mogelijke waarden: 0, 10, 30, 60, 120, 600, 1800, 3600.<br><br>Standaard: 0 (gebruikt de standaard pagina weergave periode).|
|widgets|array|Array van de [dashboard widget](object#dashboard_widget) objecten.|

[comment]: # ({/62b6b809-b88f0a6a})

[comment]: # ({new-2938b685})
### Dashboard-widget

Het dashboard widget object heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|widgetid|string|*(alleen-lezen)* ID van de dashboard widget.|
|**type**<br>(vereist)|string|Type van de dashboard widget.<br><br>Mogelijke waarden:<br>actionlog - Action log;<br>clock - Clock;<br>dataover - Gegevensoverzicht;<br>discovery - Discovery-status;<br>favgraphs - Favorite grafieken;<br>favmaps - Favorite maps;<br>graph - Graph (klassiek);<br>graphprototype - Graph prototype;<br>hostavail - Hostbeschikbaarheid;<br>item - Itemwaarde;<br>kaart - Kaart;<br>navtree - Kaartnavigatiestructuur;<br>platte tekst - Platte tekst;<br>probleemhosts - Probleemhosts;<br>problemen - Problemen; <br>problemsbysv - Problemen per ernst;<br>svggraph - Graph;<br>systeminfo - Systeeminformatie;<br>trigover - Triggeroverzicht;<br>url - URL;<br>web - Webmonitoring.|
|name|string|Aangepaste widget naam.|
|x|geheel getal|Een horizontale positie vanaf de linkerkant van het dashboard.<br><br>Geldige waarden variëren van 0 tot 23.|
|y|geheel getal|Een verticale positie vanaf de bovenkant van het dashboard.<br><br>Geldige waarden variëren van 0 tot 62.|
|width|integer|De widget breedte.<br><br>Geldige waarden variëren van 1 tot 24.|
|height|integer|De hoogte van de widget.<br><br>Geldige waarden variëren van 2 tot 32.|
|view\_mode|integer|De widget weergave modus.<br><br>Mogelijke waarden:<br>0 - (standaard) standaard widget weergave;<br>1 - met verborgen koptekst;|
|fields|array|Array van de [dashboard widget field](object#dashboard_widget_field) objecten.|

[comment]: # ({/new-2938b685})

[comment]: # ({a8160431-5b20bc04})
### Dashboard widget veld

Het veldobject van de dashboard widget heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**type**<br>(verplicht)|integer|Type van het widget veld.<br><br>Mogelijke waarden:<br>0 - Integer;<br>1 - String;<br>2 - Host groep;<br>3 - Host;<br>4 - Item;<br>6 - Grafiek;<br>8 - Kaart;|
|name|string|Widget veld naam.|
|**values**<br>(verplicht)|mixed|Widget veld waarde afhankelijk van type.|

[comment]: # ({/a8160431-5b20bc04})

[comment]: # ({204e5435-5c8b3411})
### Dashboard gebruikersgroep

Lijst met dashboard machtigingen op basis van gebruikersgroepen. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**usrgrpid**<br>(vereist)|string|Gebruikersgroep-ID.|
|**machtiging**<br>(vereist)|geheel getal|Type machtigings niveau.<br><br>Mogelijke waarden:<br>2 - alleen lezen;<br>3 - lezen-schrijven;|

[comment]: # ({/204e5435-5c8b3411})

[comment]: # ({cc272a58-d34afa0c})
### Dashboard-gebruiker

Lijst met dashboard machtigingen op basis van gebruikers. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**use-ID**<br>(vereist)|string|Gebruikers-ID.|
|**permission**<br>(vereist)|geheel getal|Type machtigings niveau.<br><br>Mogelijke waarden:<br>2 - alleen lezen;<br>3 - lezen-schrijven;|

[comment]: # ({/cc272a58-d34afa0c})
