<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/api/reference/dashboard/object.md">
    <body>
      <trans-unit id="a79e0e24" xml:space="preserve">
        <source># &gt; Dashboard object

The following objects are directly related to the `dashboard` API.</source>
        <target state="needs-translation"># &gt; Dashboard-object

De volgende objecten zijn direct gerelateerd aan de `dashboard` API.</target>
      </trans-unit>
      <trans-unit id="63e4049e" xml:space="preserve">
        <source>### Dashboard

The dashboard object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dashboardid|string|ID of the dashboard.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the dashboard.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|userid|string|Dashboard owner user ID.|
|private|integer|Type of dashboard sharing.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - public dashboard;&lt;br&gt;1 - *(default)* private dashboard.|
|display\_period|integer|Default page display period (in seconds).&lt;br&gt;&lt;br&gt;Possible values: 10, 30, 60, 120, 600, 1800, 3600.&lt;br&gt;&lt;br&gt;Default: 30.|
|auto\_start|integer|Auto start slideshow.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - do not auto start slideshow;&lt;br&gt;1 - *(default)* auto start slideshow.|</source>
        <target state="needs-translation">### Dashboard

Het dashboardobject heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|dashboardid|string|*(alleen-lezen)* ID van het dashboard.|
|**name**&lt;br&gt;(verplicht)|string|Naam van het dashboard.|
|userid|string|Gebruikers-ID dashboard eigenaar.|
|private|integer|Type dashboard delen.&lt;br&gt;&lt;br&gt;Mogelijke waarden:&lt;br&gt;0 - openbaar dashboard;&lt;br&gt;1 - *(standaard)* privé dashboard.|
|display\_period|integer|Standaard pagina weergave periode (in seconden).&lt;br&gt;&lt;br&gt;Mogelijke waarden: 10, 30, 60, 120, 600, 1800, 3600.&lt;br&gt;&lt;br&gt;Standaard: 30.|
|auto\_start|integer|Slideshow automatisch starten.&lt;br&gt;&lt;br&gt;Mogelijke waarden:&lt;br&gt;0 - diavoorstelling niet automatisch starten;&lt;br&gt;1 - *(standaard)* diavoorstelling automatisch starten.|</target>
      </trans-unit>
      <trans-unit id="b88f0a6a" xml:space="preserve">
        <source>### Dashboard page

The dashboard page object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|dashboard\_pageid|string|ID of the dashboard page.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|name|string|Dashboard page name.&lt;br&gt;&lt;br&gt;Default: empty string.|
|display\_period|integer|Dashboard page display period (in seconds).&lt;br&gt;&lt;br&gt;Possible values: 0, 10, 30, 60, 120, 600, 1800, 3600.&lt;br&gt;&lt;br&gt;Default: 0 (will use the default page display period).|
|widgets|array|Array of the [dashboard widget](object#dashboard-widget) objects.|</source>
        <target state="needs-translation">### Dashboard-pagina

Het dashboardpagina-object heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|dashboard\_pageid|string|*(alleen-lezen)* ID van de dashboard pagina.|
|name|string|Dashboard pagina naam.&lt;br&gt;&lt;br&gt;Standaard: lege string.|
|display\_period|integer|Dashboard pagina weergave periode (in seconden).&lt;br&gt;&lt;br&gt;Mogelijke waarden: 0, 10, 30, 60, 120, 600, 1800, 3600.&lt;br&gt;&lt;br&gt;Standaard: 0 (gebruikt de standaard pagina weergave periode).|
|widgets|array|Array van de [dashboard widget](object#dashboard_widget) objecten.|</target>
      </trans-unit>
      <trans-unit id="2938b685" xml:space="preserve">
        <source>### Dashboard widget

The dashboard widget object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|widgetid|string|ID of the dashboard widget.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|type|string|Type of the dashboard widget.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;actionlog - Action log;&lt;br&gt;clock - Clock;&lt;br&gt;*(deprecated)* dataover - Data overview;&lt;br&gt;discovery - Discovery status;&lt;br&gt;favgraphs - Favorite graphs;&lt;br&gt;favmaps - Favorite maps;&lt;br&gt;graph - Graph (classic);&lt;br&gt;graphprototype - Graph prototype;&lt;br&gt;hostavail - Host availability;&lt;br&gt;item - Item value;&lt;br&gt;map - Map;&lt;br&gt;navtree - Map Navigation Tree;&lt;br&gt;plaintext - Plain text;&lt;br&gt;problemhosts - Problem hosts;&lt;br&gt;problems - Problems;&lt;br&gt;problemsbysv - Problems by severity;&lt;br&gt;slareport - SLA report;&lt;br&gt;svggraph - Graph;&lt;br&gt;systeminfo - System information;&lt;br&gt;tophosts - Top hosts;&lt;br&gt;trigover - Trigger overview;&lt;br&gt;url - URL;&lt;br&gt;web - Web monitoring.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|name|string|Custom widget name.|
|x|integer|A horizontal position from the left side of the dashboard.&lt;br&gt;&lt;br&gt;Valid values range from 0 to 23.|
|y|integer|A vertical position from the top of the dashboard.&lt;br&gt;&lt;br&gt;Valid values range from 0 to 62.|
|width|integer|The widget width.&lt;br&gt;&lt;br&gt;Valid values range from 1 to 24.|
|height|integer|The widget height.&lt;br&gt;&lt;br&gt;Valid values range from 2 to 32.|
|view\_mode|integer|The widget view mode.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* default widget view;&lt;br&gt;1 - with hidden header;|
|fields|array|Array of the [dashboard widget field](object#dashboard-widget-field) objects.|</source>
      </trans-unit>
      <trans-unit id="5b20bc04" xml:space="preserve">
        <source>### Dashboard widget field

The dashboard widget field object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|Type of the widget field.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Integer;&lt;br&gt;1 - String;&lt;br&gt;2 - Host group;&lt;br&gt;3 - Host;&lt;br&gt;4 - Item;&lt;br&gt;5 - Item prototype;&lt;br&gt;6 - Graph;&lt;br&gt;7 - Graph prototype;&lt;br&gt;8 - Map;&lt;br&gt;9 - Service;&lt;br&gt;10 - SLA;&lt;br&gt;11 - User;&lt;br&gt;12 - Action;&lt;br&gt;13 - Media type.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|name|string|Widget field name.&lt;br&gt;&lt;br&gt;Possible values: see [Dashboard widget fields](/manual/api/reference/dashboard/widget_fields).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|mixed|Widget field value depending on the type.&lt;br&gt;&lt;br&gt;Possible values: see [Dashboard widget fields](/manual/api/reference/dashboard/widget_fields).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
        <target state="needs-translation">### Dashboard widget veld

Het veldobject van de dashboard widget heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**type**&lt;br&gt;(verplicht)|integer|Type van het widget veld.&lt;br&gt;&lt;br&gt;Mogelijke waarden:&lt;br&gt;0 - Integer;&lt;br&gt;1 - String;&lt;br&gt;2 - Host groep;&lt;br&gt;3 - Host;&lt;br&gt;4 - Item;&lt;br&gt;6 - Grafiek;&lt;br&gt;8 - Kaart;|
|name|string|Widget veld naam.|
|**values**&lt;br&gt;(verplicht)|mixed|Widget veld waarde afhankelijk van type.|</target>
      </trans-unit>
      <trans-unit id="5c8b3411" xml:space="preserve">
        <source>### Dashboard user group

List of dashboard permissions based on user groups. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|usrgrpid|string|User group ID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|permission|integer|Type of permission level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;2 - read only;&lt;br&gt;3 - read-write.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
        <target state="needs-translation">### Dashboard gebruikersgroep

Lijst met dashboard machtigingen op basis van gebruikersgroepen. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**usrgrpid**&lt;br&gt;(vereist)|string|Gebruikersgroep-ID.|
|**machtiging**&lt;br&gt;(vereist)|geheel getal|Type machtigings niveau.&lt;br&gt;&lt;br&gt;Mogelijke waarden:&lt;br&gt;2 - alleen lezen;&lt;br&gt;3 - lezen-schrijven;|</target>
      </trans-unit>
      <trans-unit id="d34afa0c" xml:space="preserve">
        <source>### Dashboard user

List of dashboard permissions based on users. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|userid|string|User ID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|permission|integer|Type of permission level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;2 - read only;&lt;br&gt;3 - read-write.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
        <target state="needs-translation">### Dashboard-gebruiker

Lijst met dashboard machtigingen op basis van gebruikers. Het heeft het volgende:
eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|**use-ID**&lt;br&gt;(vereist)|string|Gebruikers-ID.|
|**permission**&lt;br&gt;(vereist)|geheel getal|Type machtigings niveau.&lt;br&gt;&lt;br&gt;Mogelijke waarden:&lt;br&gt;2 - alleen lezen;&lt;br&gt;3 - lezen-schrijven;|</target>
      </trans-unit>
    </body>
  </file>
</xliff>
