[comment]: # translation:outdated

[comment]: # ({new-f125871b})
# drule.delete

[comment]: # ({/new-f125871b})

[comment]: # ({new-2430e072})
### Description

`object drule.delete(array discoveryRuleIds)`

This method allows to delete discovery rules.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-2430e072})

[comment]: # ({new-5b654054})
### Parameters

`(array)` IDs of the discovery rules to delete.

[comment]: # ({/new-5b654054})

[comment]: # ({new-35b8a89c})
### Return values

`(object)` Returns an object containing the IDs of the deleted discovery
rules under the `druleids` property.

[comment]: # ({/new-35b8a89c})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4df0ae86})
#### Delete multiple discovery rules

Delete two discovery rules.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "drule.delete",
    "params": [
        "4",
        "6"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "druleids": [
            "4",
            "6"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4df0ae86})

[comment]: # ({new-89517129})
### Source

CDRule::delete() in *ui/include/classes/api/services/CDRule.php*.

[comment]: # ({/new-89517129})
