[comment]: # translation:outdated

[comment]: # ({f68457d6-34f25b55})
# apiinfo.versie

[comment]: # ({/f68457d6-34f25b55})

[comment]: # ({0c7e6661-bc32199a})
### Beschrijving

`string apiinfo.version(array)`

Met deze methode kan de versie van de Zabbix API worden opgehaald.

::: noteimportant
Deze methode is alleen beschikbaar voor niet-geverifieerde
gebruikers en moet worden aangeroepen zonder de parameter `auth` in de JSON-RPC
verzoek.
:::

[comment]: # ({/0c7e6661-bc32199a})

[comment]: # ({fb60fba2-4fa8a419})
### Parameters

`(array)` De methode accepteert een lege array.

[comment]: # ({/fb60fba2-4fa8a419})

[comment]: # ({2c51a53d-53521d11})
### Retourwaarden

`(string)` Retourneert de versie van de Zabbix API.

::: notetip
Vanaf Zabbix 2.0.4 komt de versie van de API overeen met de versie van Zabbix.
:::

[comment]: # ({/2c51a53d-53521d11})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({ccd112a5-6ba4bdd0})
#### De versie van de API ophalen

Haal de versie van de Zabbix API op.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "apiinfo.version",
    "params": [],
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "4.0.0",
    "id": 1
}
```

[comment]: # ({/ccd112a5-6ba4bdd0})

[comment]: # ({a064f59f-25499b5e})
### Bron

CAPIInfo::version() in *ui/include/classes/api/services/CAPIInfo.php*.

[comment]: # ({/a064f59f-25499b5e})
