[comment]: # translation:outdated

[comment]: # ({84067286-4a04064b})
# Auditlogboek

Deze klasse is ontworpen om te werken met auditlogboek.

Objectreferenties:\

- [Auditlog-object](/manual/api/reference/auditlog/object)

Beschikbare methoden:\

- [auditlog.get](/manual/api/reference/auditlog/get) - audit ophalen
    logboekrecords

[comment]: # ({/84067286-4a04064b})
