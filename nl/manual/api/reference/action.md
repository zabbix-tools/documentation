[comment]: # translation:outdated

[comment]: # ({feb35a31-06845577})
# Actie

Deze klasse is ontworpen om met acties te werken.

Objectreferenties:\

- [action](/manual/api/reference/action/object#action)
- [Action
    condition](/manual/api/reference/action/object#action_condition)
- [Action
    operatie](/manual/api/reference/action/object#action_operation)

Beschikbare methoden:\

- [action.create](/manual/api/reference/action/create) - maak nieuwe
    acties
- [action.delete](/manual/api/reference/action/delete) - delete
    acties
- [action.get](/manual/api/reference/action/get) - acties ophalen
- [action.update](/manual/api/reference/action/update) - update
    acties

[comment]: # ({/feb35a31-06845577})
