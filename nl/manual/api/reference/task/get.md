[comment]: # translation:outdated

[comment]: # ({new-d981e093})
# task.get

[comment]: # ({/new-d981e093})

[comment]: # ({new-79cf46cd})
### Description

`integer/array task.get(object parameters)`

The method allows to retrieve tasks according to the given parameters.
Method returns details only about 'diagnostic information' tasks.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-79cf46cd})

[comment]: # ({new-cd6e3f53})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|taskids|string/array|Return only tasks with the given IDs.|
|output|query|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|preservekeys|boolean|^|

[comment]: # ({/new-cd6e3f53})

[comment]: # ({new-e1126edc})
### Return values

`(integer/array)` Returns an array of objects.

[comment]: # ({/new-e1126edc})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-8d9c84cb})
#### Retrieve task by ID

Retrieve all data about task “1”.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "task.get",
    "params": {
        "output": "extend",
        "taskids": "1"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "taskid": "1",
            "type": "7",
            "status": "3",
            "clock": "1601039076",
            "ttl": "3600",
            "proxy_hostid": null,
            "request": {
                "alerting": {
                    "stats": [
                        "alerts"
                    ],
                    "top": {
                        "media.alerts": 10
                    }
                },
                "lld": {
                    "stats": "extend",
                    "top": {
                        "values": 5
                    }
                }
            },
            "result": {
                "data": {
                    "alerting": {
                        "alerts": 0,
                        "top": {
                            "media.alerts": []
                        },
                        "time": 0.000663
                    },
                    "lld": {
                        "rules": 0,
                        "values": 0,
                        "top": {
                            "values": []
                        },
                        "time": 0.000442
                    }
                },
                "status": "0"
            }
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8d9c84cb})

[comment]: # ({new-e1d2d11a})
### See also

-   [Task](/manual/api/reference/task/object)
-   [Statistic result
    object](/manual/api/reference/task/object#statistic_result_object)

[comment]: # ({/new-e1d2d11a})

[comment]: # ({new-85e7fa97})
### Source

CTask::get() in *ui/include/classes/api/services/CTask.php*.

[comment]: # ({/new-85e7fa97})
