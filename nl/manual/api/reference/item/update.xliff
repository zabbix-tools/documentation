<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/api/reference/item/update.md">
    <body>
      <trans-unit id="0ec30c22" xml:space="preserve">
        <source># item.update</source>
      </trans-unit>
      <trans-unit id="38961105" xml:space="preserve">
        <source>### Description

`object item.update(object/array items)`

This method allows to update existing items.

::: noteclassic
Web items cannot be updated via the Zabbix API.
:::

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="4e151fd6" xml:space="preserve">
        <source>### Parameters

`(object/array)` Item properties to be updated.

The `itemid` property must be defined for each item, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard item properties](object#item), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|preprocessing|array|[Item preprocessing](/manual/api/reference/item/object#item_preprocessing) options to replace the current preprocessing options.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|tags|array|Item [tags](/manual/api/reference/item/object#item_tag).&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *read-only* for discovered objects|</source>
      </trans-unit>
      <trans-unit id="aefff750" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated items
under the `itemids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="5388578b" xml:space="preserve">
        <source>#### Enabling an item

Enable an item, that is, set its status to "0".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="0bf89382" xml:space="preserve">
        <source>#### Update dependent item

Update Dependent item name and Master item ID. Only dependencies on same
host are allowed, therefore Master and Dependent item should have same
hostid.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "name": "Dependent item updated name",
        "master_itemid": "25562",
        "itemid": "189019"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "189019"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="8f365363" xml:space="preserve">
        <source>#### Update HTTP agent item

Enable item value trapping.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "allow_traps": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="44f323b8" xml:space="preserve">
        <source>#### Updating an item with preprocessing

Update an item with item preprocessing rule "In range".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23856",
        "preprocessing": [
            {
                "type": 13,
                "params": "\n100",
                "error_handler": 1,
                "error_handler_params": ""
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23856"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="dcfb98a0" xml:space="preserve">
        <source>#### Updating a script item

Update a script item with a different script and remove unnecessary parameters that were used by previous script.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "23865",
        "parameters": [],
        "script": "Zabbix.log(3, 'Log test');\nreturn 1;"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "23865"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="646fe32a" xml:space="preserve">
        <source>### Source

CItem::update() in *ui/include/classes/api/services/CItem.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
