<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/api/reference/item/object.md">
    <body>
      <trans-unit id="6bb9f60f" xml:space="preserve">
        <source># &gt; Item object

The following objects are directly related to the `item` API.</source>
      </trans-unit>
      <trans-unit id="385ac8b5" xml:space="preserve">
        <source>### Item

::: noteclassic
Web items cannot be directly created, updated or deleted via the Zabbix API.
:::

The item object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|itemid|string|ID of the item.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|delay|string|Update interval of the item.&lt;br&gt;Accepts seconds or a time unit with suffix (30s,1m,2h,1d).&lt;br&gt;Optionally one or more [custom intervals](/manual/config/items/item/custom_intervals) can be specified either as flexible intervals or scheduling.&lt;br&gt;Multiple intervals are separated by a semicolon.&lt;br&gt;User macros may be used. A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.&lt;br&gt;Flexible intervals may be written as two macros separated by a forward slash (e.g. `{$FLEX_INTERVAL}/{$FLEX_PERIOD}`).&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Zabbix agent" (0), "Simple check" (3), "Zabbix internal" (5), "External check" (10), "Database monitor" (11), "IPMI agent" (12), "SSH agent" (13), "TELNET agent" (14), "Calculated" (15), "JMX agent" (16), "HTTP agent" (19), "SNMP agent" (20), "Script" (21), or if `type` is set to "Zabbix agent (active)" (7) and `key_` does not contain "mqtt.get"|
|hostid|string|ID of the host or template that the item belongs to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *constant*&lt;br&gt;- *required* for create operations|
|interfaceid|string|ID of the item's host interface.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if item belongs to host and `type` is set to "Zabbix agent", "IPMI agent", "JMX agent", "SNMP trap", or "SNMP agent"&lt;br&gt;- *supported* if item belongs to host and `type` is set to "Simple check", "External check", "SSH agent", "TELNET agent", or "HTTP agent"&lt;br&gt;- *read-only* for discovered objects|
|key\_|string|Item key.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|name|string|Name of the item.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|type|integer|Type of the item.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Zabbix agent;&lt;br&gt;2 - Zabbix trapper;&lt;br&gt;3 - Simple check;&lt;br&gt;5 - Zabbix internal;&lt;br&gt;7 - Zabbix agent (active);&lt;br&gt;9 - Web item;&lt;br&gt;10 - External check;&lt;br&gt;11 - Database monitor;&lt;br&gt;12 - IPMI agent;&lt;br&gt;13 - SSH agent;&lt;br&gt;14 - TELNET agent;&lt;br&gt;15 - Calculated;&lt;br&gt;16 - JMX agent;&lt;br&gt;17 - SNMP trap;&lt;br&gt;18 - Dependent item;&lt;br&gt;19 - HTTP agent;&lt;br&gt;20 - SNMP agent;&lt;br&gt;21 - Script.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|url|string|URL string.&lt;br&gt;Supports user macros, {HOST.IP}, {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|value\_type|integer|Type of information of the item.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - numeric float;&lt;br&gt;1 - character;&lt;br&gt;2 - log;&lt;br&gt;3 - numeric unsigned;&lt;br&gt;4 - text.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|allow\_traps|integer|Allow to populate value similarly to the trapper item.&lt;br&gt;&lt;br&gt;0 - *(default)* Do not allow to accept incoming data;&lt;br&gt;1 - Allow to accept incoming data.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for discovered objects|
|authtype|integer|Authentication method.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "SSH agent":&lt;br&gt;0 - *(default)* password;&lt;br&gt;1 - public key.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "HTTP agent":&lt;br&gt;0 - *(default)* none;&lt;br&gt;1 - basic;&lt;br&gt;2 - NTLM;&lt;br&gt;3 - Kerberos.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "SSH agent" or "HTTP agent"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "HTTP agent") or discovered objects|
|description|string|Description of the item.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only* for discovered objects|
|error|string|Error text if there are problems updating the item.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|flags|integer|Origin of the item.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - a plain item;&lt;br&gt;4 - a discovered item.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|follow\_redirects|integer|Follow response redirects while pooling data.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not follow redirects;&lt;br&gt;1 - *(default)* Follow redirects.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|headers|object|Object with HTTP(S) request headers, where header name is used as key and header value as value.&lt;br&gt;&lt;br&gt;Example: { "User-Agent": "Zabbix" }&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|history|string|A time unit of how long the history data should be stored.&lt;br&gt;Also accepts user macro.&lt;br&gt;&lt;br&gt;Default: 90d.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only* for discovered objects|
|http\_proxy|string|HTTP(S) proxy connection string.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|inventory\_link|integer|ID of the host inventory field that is populated by the item.&lt;br&gt;&lt;br&gt;Refer to the [host inventory page](/manual/api/reference/host/object#host_inventory) for a list of supported host inventory fields and their IDs.&lt;br&gt;&lt;br&gt;Default: 0.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `value_type` is set to "numeric float", "character", "numeric unsigned", or "text"&lt;br&gt;- *read-only* for discovered objects|
|ipmi\_sensor|string|IPMI sensor.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "IPMI agent" and `key_` is not set to "ipmi.get"&lt;br&gt;- *supported* if `type` is set to "IPMI agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|jmx\_endpoint|string|JMX agent custom connection string.&lt;br&gt;&lt;br&gt;Default value: service:jmx:rmi:///jndi/rmi://{HOST.CONN}:{HOST.PORT}/jmxrmi&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "JMX agent"&lt;br&gt;- *read-only* for discovered objects|
|lastclock|timestamp|Time when the item was last updated.&lt;br&gt;&lt;br&gt;By default, only values that fall within the last 24 hours are displayed. You can extend this time period by changing the value of *Max history display period* parameter in the *[Administration → General](/manual/web_interface/frontend_sections/administration/general#gui)* menu section.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|lastns|integer|Nanoseconds when the item was last updated.&lt;br&gt;&lt;br&gt;By default, only values that fall within the last 24 hours are displayed. You can extend this time period by changing the value of *Max history display period* parameter in the *[Administration → General](/manual/web_interface/frontend_sections/administration/general#gui)* menu section.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|lastvalue|string|Last value of the item.&lt;br&gt;&lt;br&gt;By default, only values that fall within the last 24 hours are displayed. You can extend this time period by changing the value of *Max history display period* parameter in the *[Administration → General](/manual/web_interface/frontend_sections/administration/general#gui)* menu section.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|logtimefmt|string|Format of the time in log entries.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `value_type` is set to "log"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|master\_itemid|integer|Master item ID.&lt;br&gt;Recursion up to 3 dependent items and maximum count of dependent items equal to 29999 are allowed.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Dependent item"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|output\_format|integer|Should the response be converted to JSON.&lt;br&gt;&lt;br&gt;0 - *(default)* Store raw;&lt;br&gt;1 - Convert to JSON.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|params|string|Additional parameters depending on the type of the item:&lt;br&gt;- executed script for SSH agent and TELNET agent items;&lt;br&gt;- SQL query for database monitor items;&lt;br&gt;- formula for calculated items;&lt;br&gt;- the script for script item.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Database monitor", "SSH agent", "TELNET agent", "Calculated", or "Script"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "Script") or discovered objects|
|parameters|array|Additional parameters if `type` is set to "Script". Array of objects with `name` and `value` properties, where `name` must be unique.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "Script"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|password|string|Password for authentication.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "JMX agent" and `username` is set&lt;br&gt;- *supported* if `type` is set to "Simple check", "SSH agent", "TELNET agent", "Database monitor", or "HTTP agent"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "HTTP agent") or discovered objects|
|post\_type|integer|Type of post data body stored in `posts` property.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Raw data;&lt;br&gt;2 - JSON data;&lt;br&gt;3 - XML data.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|posts|string|HTTP(S) request body data.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "HTTP agent" and `post_type` is set to "JSON data" or "XML data"&lt;br&gt;- *supported* if `type` is set to "HTTP agent" and `post_type` is set to "Raw data"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|prevvalue|string|Previous value of the item.&lt;br&gt;&lt;br&gt;By default, only values that fall within the last 24 hours are displayed. You can extend this time period by changing the value of *Max history display period* parameter in the *[Administration → General](/manual/web_interface/frontend_sections/administration/general#gui)* menu section.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|privatekey|string|Name of the private key file.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"&lt;br&gt;- *read-only* for discovered objects|
|publickey|string|Name of the public key file.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SSH agent" and `authtype` is set to "public key"&lt;br&gt;- *read-only* for discovered objects|
|query\_fields|array|Query parameters. Array of objects with `key`:`value` pairs, where `value` can be an empty string.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|request\_method|integer|Type of request method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* GET;&lt;br&gt;1 - POST;&lt;br&gt;2 - PUT;&lt;br&gt;3 - HEAD.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|retrieve\_mode|integer|What part of response should be stored.&lt;br&gt;&lt;br&gt;Possible values if `request_method` is set to "GET", "POST", or "PUT":&lt;br&gt;0 - *(default)* Body;&lt;br&gt;1 - Headers;&lt;br&gt;2 - Both body and headers will be stored.&lt;br&gt;&lt;br&gt;Possible values if `request_method` is set to "HEAD":&lt;br&gt;1 - Headers.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|snmp\_oid|string|SNMP OID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SNMP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|ssl\_cert\_file|string|Public SSL Key file path.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|ssl\_key\_file|string|Private SSL Key file path.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|ssl\_key\_password|string|Password for SSL Key file.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|state|integer|State of the item.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* normal;&lt;br&gt;1 - not supported.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|status|integer|Status of the item.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* enabled item;&lt;br&gt;1 - disabled item.|
|status\_codes|string|Ranges of required HTTP status codes, separated by commas.&lt;br&gt;Also supports user macros as part of comma separated list.&lt;br&gt;&lt;br&gt;Example: 200,200-{$M},{$M},200-400&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|templateid|string|ID of the parent template item.&lt;br&gt;&lt;br&gt;*Hint*: Use the `hostid` property to specify the template that the item belongs to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|timeout|string|Item data polling request timeout.&lt;br&gt;Supports user macros.&lt;br&gt;&lt;br&gt;Default: `3s`.&lt;br&gt;Maximum value: `60s`.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent" or "Script"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|trapper\_hosts|string|Allowed hosts.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *readonly* for discovered objects&lt;br&gt;- *supported* if `type` is set to "Zabbix trapper", or if `type` is set to "HTTP agent" and `allow_traps` is set to "Allow to accept incoming data"|
|trends|string|A time unit of how long the trends data should be stored.&lt;br&gt;Also accepts user macro.&lt;br&gt;&lt;br&gt;Default: 365d.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `value_type` is set to "numeric float" or "numeric unsigned"&lt;br&gt;- *read-only* for discovered objects|
|units|string|Value units.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `value_type` is set to "numeric float" or "numeric unsigned"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|username|string|Username for authentication.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "SSH agent", "TELNET agent", or if `type` is set to "JMX agent" and `password` is set&lt;br&gt;- *supported* if `type` is set to "Simple check", "Database monitor", or "HTTP agent"&lt;br&gt;- *read-only* for inherited objects (if `type` is set to "HTTP agent") or discovered objects|
|uuid|string|Universal unique identifier, used for linking imported item to already existing ones. Auto-generated, if not given.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if the item belongs to a template|
|valuemapid|string|ID of the associated value map.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `value_type` is set to "numeric float", "character", or "numeric unsigned"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|verify\_host|integer|Validate host name in URL is in *Common Name* field or a *Subject Alternate Name* field of host certificate.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not validate;&lt;br&gt;1 - Validate.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|
|verify\_peer|integer|Validate is host certificate authentic.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Do not validate;&lt;br&gt;1 - Validate.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `type` is set to "HTTP agent"&lt;br&gt;- *read-only* for inherited objects or discovered objects|</source>
      </trans-unit>
      <trans-unit id="c1b98afa" xml:space="preserve">
        <source>### Item tag

The item tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Item tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|value|string|Item tag value.|</source>
      </trans-unit>
      <trans-unit id="2169bb78" xml:space="preserve">
        <source>### Item preprocessing

The item preprocessing object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|type|integer|The preprocessing option type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;1 - Custom multiplier;&lt;br&gt;2 - Right trim;&lt;br&gt;3 - Left trim;&lt;br&gt;4 - Trim;&lt;br&gt;5 - Regular expression matching;&lt;br&gt;6 - Boolean to decimal;&lt;br&gt;7 - Octal to decimal;&lt;br&gt;8 - Hexadecimal to decimal;&lt;br&gt;9 - Simple change;&lt;br&gt;10 - Change per second;&lt;br&gt;11 - XML XPath;&lt;br&gt;12 - JSONPath;&lt;br&gt;13 - In range;&lt;br&gt;14 - Matches regular expression;&lt;br&gt;15 - Does not match regular expression;&lt;br&gt;16 - Check for error in JSON;&lt;br&gt;17 - Check for error in XML;&lt;br&gt;18 - Check for error using regular expression;&lt;br&gt;19 - Discard unchanged;&lt;br&gt;20 - Discard unchanged with heartbeat;&lt;br&gt;21 - JavaScript;&lt;br&gt;22 - Prometheus pattern;&lt;br&gt;23 - Prometheus to JSON;&lt;br&gt;24 - CSV to JSON;&lt;br&gt;25 - Replace;&lt;br&gt;26 - Check unsupported;&lt;br&gt;27 - XML to JSON;&lt;br&gt;28 - SNMP walk value;&lt;br&gt;29 - SNMP walk to JSON.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|params|string|Additional parameters used by preprocessing option.&lt;br&gt;Multiple parameters are separated by the newline (\\n) character.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Custom multiplier" (1), "Right trim" (2), "Left trim" (3), "Trim" (4), "Regular expression matching" (5), "XML XPath" (11), "JSONPath" (12), "In range" (13), "Matches regular expression" (14), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Check for error using regular expression" (18), "Discard unchanged with heartbeat" (20), "JavaScript" (21), "Prometheus pattern" (22), "Prometheus to JSON" (23), "CSV to JSON" (24), "Replace" (25), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler|integer|Action type used in case of preprocessing step failure.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Error message is set by Zabbix server;&lt;br&gt;1 - Discard value;&lt;br&gt;2 - Set custom value;&lt;br&gt;3 - Set custom error message.&lt;br&gt;&lt;br&gt;Possible values if `type` is set to "Check unsupported":&lt;br&gt;1 - Discard value;&lt;br&gt;2 - Set custom value;&lt;br&gt;3 - Set custom error message.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `type` is set to "Custom multiplier" (1), "Regular expression matching" (5), "Boolean to decimal" (6), "Octal to decimal" (7), "Hexadecimal to decimal" (8), "Simple change" (9), "Change per second" (10), "XML XPath" (11), "JSONPath" (12), "In range" (13), "Matches regular expression" (14), "Does not match regular expression" (15), "Check for error in JSON" (16), "Check for error in XML" (17), "Check for error using regular expression" (18), "Prometheus pattern" (22), "Prometheus to JSON" (23), "CSV to JSON" (24), "Check unsupported" (26), "XML to JSON" (27), "SNMP walk value" (28), or "SNMP walk to JSON" (29)|
|error\_handler\_params|string|Error handler parameters.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `error_handler` is set to "Set custom value" or "Set custom error message"|

The following parameters and error handlers are supported for each preprocessing type.

|Preprocessing type|Name|Parameter 1|Parameter 2|Parameter 3|Supported error handlers|
|------------------|----|-----------|-----------|-----------|------------------------|
|1|Custom multiplier|number^1,\ 6^|&lt;|&lt;|0, 1, 2, 3|
|2|Right trim|list of characters^2^|&lt;|&lt;|&lt;|
|3|Left trim|list of characters^2^|&lt;|&lt;|&lt;|
|4|Trim|list of characters^2^|&lt;|&lt;|&lt;|
|5|Regular expression|pattern^3^|output^2^|&lt;|0, 1, 2, 3|
|6|Boolean to decimal|&lt;|&lt;|&lt;|0, 1, 2, 3|
|7|Octal to decimal|&lt;|&lt;|&lt;|0, 1, 2, 3|
|8|Hexadecimal to decimal|&lt;|&lt;|&lt;|0, 1, 2, 3|
|9|Simple change|&lt;|&lt;|&lt;|0, 1, 2, 3|
|10|Change per second|&lt;|&lt;|&lt;|0, 1, 2, 3|
|11|XML XPath|path^4^|&lt;|&lt;|0, 1, 2, 3|
|12|JSONPath|path^4^|&lt;|&lt;|0, 1, 2, 3|
|13|In range|min^1,\ 6^|max^1,\ 6^|&lt;|0, 1, 2, 3|
|14|Matches regular expression|pattern^3^|&lt;|&lt;|0, 1, 2, 3|
|15|Does not match regular expression|pattern^3^|&lt;|&lt;|0, 1, 2, 3|
|16|Check for error in JSON|path^4^|&lt;|&lt;|0, 1, 2, 3|
|17|Check for error in XML|path^4^|&lt;|&lt;|0, 1, 2, 3|
|18|Check for error using regular expression|pattern^3^|output^2^|&lt;|0, 1, 2, 3|
|19|Discard unchanged|&lt;|&lt;|&lt;|&lt;|
|20|Discard unchanged with heartbeat|seconds^5,\ 6^|&lt;|&lt;|&lt;|
|21|JavaScript|script^2^|&lt;|&lt;|&lt;|
|22|Prometheus pattern|pattern^6,\ 7^|`value`, `label`, `function`|output^8,\ 9^|0, 1, 2, 3|
|23|Prometheus to JSON|pattern^6,\ 7^|&lt;|&lt;|0, 1, 2, 3|
|24|CSV to JSON|character^2^|character^2^|0,1|0, 1, 2, 3|
|25|Replace|search string^2^|replacement^2^|&lt;|&lt;|
|26|Check unsupported|&lt;|&lt;|&lt;|1, 2, 3|
|27|XML to JSON|&lt;|&lt;|&lt;|0, 1, 2, 3|
|28|SNMP walk value|OID^2^|Format:&lt;br&gt;0 - Unchanged&lt;br&gt;1 - UTF-8 from Hex-STRING&lt;br&gt;2 - MAC from Hex-STRING&lt;br&gt;3 - Integer from BITS|&lt;|0, 1, 2, 3|
|29|SNMP walk to JSON^10^|Field name^2^|OID prefix^2^|Format:&lt;br&gt;0 - Unchanged&lt;br&gt;1 - UTF-8 from Hex-STRING&lt;br&gt;2 - MAC from Hex-STRING&lt;br&gt;3 - Integer from BITS|0, 1, 2, 3|

^1^ integer or floating-point number\
^2^ string\
^3^ regular expression\
^4^ JSONPath or XML XPath\
^5^ positive integer (with support of time suffixes, e.g. 30s, 1m, 2h,
1d)\
^6^ user macro\
^7^ Prometheus pattern following the syntax:
`&lt;metric name&gt;{&lt;label name&gt;="&lt;label value&gt;", ...} == &lt;value&gt;`. Each
Prometheus pattern component (metric, label name, label value and metric
value) can be user macro.\
^8^ Prometheus output following the syntax: `&lt;label name&gt;` (can be a user macro) if `label` is selected as the second parameter.\
^9^ One of the aggregation functions: `sum`, `min`, `max`, `avg`, `count` if `function` is selected as the second parameter.\
^10^ Supports multiple "Field name,OID prefix,Format records" records delimited by a new line character.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
