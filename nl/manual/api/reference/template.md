[comment]: # translation:outdated

[comment]: # ({new-4f0fdd87})
# Template

This class is designed to work with templates.

Object references:\

-   [Template](/manual/api/reference/template/object#template)

Available methods:\

-   [template.create](/manual/api/reference/template/create) - creating
    new templates
-   [template.delete](/manual/api/reference/template/delete) - deleting
    templates
-   [template.get](/manual/api/reference/template/get) - retrieving
    templates
-   [template.massadd](/manual/api/reference/template/massadd) - adding
    related objects to templates
-   [template.massremove](/manual/api/reference/template/massremove) -
    removing related objects from templates
-   [template.massupdate](/manual/api/reference/template/massupdate) -
    replacing or removing related objects from templates
-   [template.update](/manual/api/reference/template/update) - updating
    templates

[comment]: # ({/new-4f0fdd87})
