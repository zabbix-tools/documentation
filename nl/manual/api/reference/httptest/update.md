[comment]: # translation:outdated

[comment]: # ({new-c289fd9d})
# httptest.update

[comment]: # ({/new-c289fd9d})

[comment]: # ({new-b09ad465})
### Description

`object httptest.update(object/array webScenarios)`

This method allows to update existing web scenarios.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b09ad465})

[comment]: # ({new-83b60d28})
### Parameters

`(object/array)` Web scenario properties to be updated.

The `httptestid` property must be defined for each web scenario, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard web scenario
properties](object#web_scenario), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|steps|array|Scenario [steps](/manual/api/reference/httptest/object#scenario_step) to replace existing steps.|
|tags|array|Web scenario [tags.](/manual/api/reference/httptest/object#web_scenario_tag)|

[comment]: # ({/new-83b60d28})

[comment]: # ({new-02282fdb})
### Return values

`(object)` Returns an object containing the IDs of the updated web
scenarios under the `httptestid` property.

[comment]: # ({/new-02282fdb})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4c3bf00e})
#### Enabling a web scenario

Enable a web scenario, that is, set its status to "0".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "httptest.update",
    "params": {
        "httptestid": "5",
        "status": 0
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "httptestids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4c3bf00e})

[comment]: # ({new-e8ca015f})
### See also

-   [Scenario step](object#scenario_step)

[comment]: # ({/new-e8ca015f})

[comment]: # ({new-628fc1ff})
### Source

CHttpTest::update() in *ui/include/classes/api/services/CHttpTest.php*.

[comment]: # ({/new-628fc1ff})
