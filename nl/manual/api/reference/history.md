[comment]: # translation:outdated

[comment]: # ({new-6525f8c0})
# History

This class is designed to work with history data.

Object references:\

-   [History](/manual/api/reference/history/object#history)

Available methods:\

-   [history.get](/manual/api/reference/history/get) - retrieving
    history data.

[comment]: # ({/new-6525f8c0})
