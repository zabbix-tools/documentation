[comment]: # translation:outdated

[comment]: # ({new-9230837a})
# hostinterface.massremove

[comment]: # ({/new-9230837a})

[comment]: # ({new-4559aff6})
### Description

`object hostinterface.massremove(object parameters)`

This method allows to remove host interfaces from the given hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-4559aff6})

[comment]: # ({new-8970fdbb})
### Parameters

`(object)` Parameters containing the IDs of the hosts to be updated and
the interfaces to be removed.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**hostids**<br>(required)|string/array|IDs of the hosts to be updated.|
|**interfaces**<br>(required)|object/array|Host interfaces to remove from the given hosts.<br><br>The host interface object must have the ip, dns and port properties defined|

[comment]: # ({/new-8970fdbb})

[comment]: # ({new-61a8b976})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
interfaces under the `interfaceids` property.

[comment]: # ({/new-61a8b976})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-cceffaf2})
#### Removing interfaces

Remove the "127.0.0.1" SNMP interface from two hosts.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostinterface.massremove",
    "params": {
        "hostids": [
            "30050",
            "30052"
        ],
        "interfaces": {
            "dns": "",
            "ip": "127.0.0.1",
            "port": "161"
        }
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "interfaceids": [
            "30069",
            "30070"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-cceffaf2})

[comment]: # ({new-a3245281})
### See also

-   [hostinterface.delete](delete)
-   [host.massremove](/manual/api/reference/host/massremove)

[comment]: # ({/new-a3245281})

[comment]: # ({new-6853cf7a})
### Source

CHostInterface::massRemove() in
*ui/include/classes/api/services/CHostInterface.php*.

[comment]: # ({/new-6853cf7a})
