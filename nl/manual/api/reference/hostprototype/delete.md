[comment]: # translation:outdated

[comment]: # ({new-8f15992f})
# hostprototype.delete

[comment]: # ({/new-8f15992f})

[comment]: # ({new-f7c792a6})
### Description

`object hostprototype.delete(array hostPrototypeIds)`

This method allows to delete host prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-f7c792a6})

[comment]: # ({new-65284f99})
### Parameters

`(array)` IDs of the host prototypes to delete.

[comment]: # ({/new-65284f99})

[comment]: # ({new-78e6f07c})
### Return values

`(object)` Returns an object containing the IDs of the deleted host
prototypes under the `hostids` property.

[comment]: # ({/new-78e6f07c})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-48be53a2})
#### Deleting multiple host prototypes

Delete two host prototypes.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.delete",
    "params": [
        "10103",
        "10105"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "10103",
            "10105"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-48be53a2})

[comment]: # ({new-dd993f84})
### Source

CHostPrototype::delete() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-dd993f84})
