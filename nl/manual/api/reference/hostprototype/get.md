[comment]: # translation:outdated

[comment]: # ({new-31d5554a})
# hostprototype.get

[comment]: # ({/new-31d5554a})

[comment]: # ({new-d1534f8a})
### Description

`integer/array hostprototype.get(object parameters)`

The method allows to retrieve host prototypes according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-d1534f8a})

[comment]: # ({new-0d6ab1f2})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|hostids|string/array|Return only host prototypes with the given IDs.|
|discoveryids|string/array|Return only host prototype that belong to the given LLD rules.|
|inherited|boolean|If set to `true` return only items inherited from a template.|
|selectDiscoveryRule|query|Return a [discoveryRule](/manual/api/reference/discoveryrule/object#lld_rule) property with the LLD rule that the host prototype belongs to.|
|selectInterfaces|query|Return an [interfaces](/manual/api/reference/hostprototype/object#custom_interface) property with host prototype custom interfaces.|
|selectGroupLinks|query|Return a [groupLinks](/manual/api/reference/hostprototype/object#group_link) property with the group links of the host prototype.|
|selectGroupPrototypes|query|Return a [groupPrototypes](/manual/api/reference/hostprototype/object#group_prototype) property with the group prototypes of the host prototype.|
|selectMacros|query|Return a [macros](/manual/api/reference/usermacro/object) property with host prototype macros.|
|selectParentHost|query|Return a [parentHost](/manual/api/reference/host/object) property with the host that the host prototype belongs to.|
|selectTags|query|Return a [tags](/manual/api/reference/hostprototype/object#host_prototype_tag) property with host prototype tags.|
|selectTemplates|query|Return a [templates](/manual/api/reference/template/object) property with the templates linked to the host prototype.<br><br>Supports `count`.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `hostid`, `host`, `name` and `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail on the [Generic Zabbix API information](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-0d6ab1f2})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-a91c8b17})
#### Retrieving host prototypes from an LLD rule

Retrieve all host prototypes, their group links, group prototypes and
tags from an LLD rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "hostprototype.get",
    "params": {
        "output": "extend",
        "selectInterfaces": "extend",
        "selectGroupLinks": "extend",
        "selectGroupPrototypes": "extend",
        "selectTags": "extend",
        "discoveryids": "23554"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10092",
            "host": "{#HV.UUID}",
            "name": "{#HV.UUID}",
            "status": "0",
            "templateid": "0",
            "discover": "0",
            "custom_interfaces": "1",
            "inventory_mode": "-1",
            "groupLinks": [
                {
                    "group_prototypeid": "4",
                    "hostid": "10092",
                    "groupid": "7",
                    "templateid": "0"
                }
            ],
            "groupPrototypes": [
                {
                    "group_prototypeid": "7",
                    "hostid": "10092",
                    "name": "{#CLUSTER.NAME}",
                    "templateid": "0"
                }
            ],
            "tags": [
                {
                    "tag": "Datacenter",
                    "value": "{#DATACENTER.NAME}"
                },
                {
                    "tag": "Instance type",
                    "value": "{#INSTANCE_TYPE}"
                }
            ],
            "interfaces": [
                {
                    "main": "1",
                    "type": "2",
                    "useip": "1",
                    "ip": "127.0.0.1",
                    "dns": "",
                    "port": "161",
                    "details": {
                        "version": "2",
                        "bulk": "1",
                        "community": "{$SNMP_COMMUNITY}"
                    }
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-a91c8b17})

[comment]: # ({new-749ec170})
### See also

-   [Group link](object#group_link)
-   [Group prototype](object#group_prototype)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-749ec170})

[comment]: # ({new-437ab274})
### Source

CHostPrototype::get() in
*ui/include/classes/api/services/CHostPrototype.php*.

[comment]: # ({/new-437ab274})
