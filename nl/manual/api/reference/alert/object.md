[comment]: # translation:outdated

[comment]: # ({cbdd7909-05585755})
# > Waarschuwingsobject

De volgende objecten zijn direct gerelateerd aan de `alert` API.

[comment]: # ({/cbdd7909-05585755})

[comment]: # ({39597527-933fed24})
### Waarschuwing

::: noteclassic
Waarschuwingen worden gemaakt door de Zabbix-server en kunnen niet worden
gewijzigd via de API.
:::

Het waarschuwingsobject bevat informatie over of bepaalde actie
operaties succesvol zijn uitgevoerd. Het heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|alertid|string|ID van de waarschuwing.|
|actionid|string|ID van de actie die de waarschuwing heeft gegenereerd.|
|alerttype|integer|Alert type.<br><br>Mogelijke waarden:<br>0 - bericht;<br>1 - extern commando.|
|clock|timestamp|Tijd waarop de waarschuwing is gegenereerd.|
|error|string|Fouttekst als er problemen zijn met het verzenden van een bericht of het uitvoeren van een commando.|
|esc\_step|integer|Actie escalatie stap waarin de waarschuwing is gegenereerd.|
|eventid|string|ID van de gebeurtenis die de actie heeft geactiveerd.|
|mediatypeid|string|ID van het mediatype dat is gebruikt om het bericht te verzenden.|
|message|tekst|Bericht tekst. Gebruikt voor bericht waarschuwingen.|
|retries|integer|Aantal keren dat Zabbix heeft geprobeerd het bericht te verzenden.|
|sendto|string|Adres, gebruikersnaam of andere identificatie van de ontvanger. Gebruikt voor bericht waarschuwingen.|
|status|integer|Status die aangeeft of de actiebewerking succesvol is uitgevoerd.<br><br>Mogelijke waarden voor bericht waarschuwingen:<br>0 - bericht niet verzonden.<br>1 - bericht verzonden.<br>2 - mislukt na een aantal nieuwe pogingen.<br>3 - nieuwe waarschuwing is nog niet verwerkt door waarschuwingsmanager.<br><br>Mogelijke waarden voor opdracht waarschuwingen:<br>0 - opdracht niet uitgevoerd.<br>1 - opdracht uitgevoerd .<br>2 - probeerde het commando op de Zabbix-agent uit te voeren, maar het was niet beschikbaar.|
|subject|string|Bericht onderwerp. Gebruikt voor bericht waarschuwingen.|
|userid|string|ID van de gebruiker naar wie het bericht is verzonden.|
|p\_eventid|string|ID van de probleemgebeurtenis, die de waarschuwing heeft gegenereerd.|
|acknowledgeid|string|ID van bevestiging, die de waarschuwing heeft gegenereerd.|

[comment]: # ({/39597527-933fed24})
