[comment]: # translation:outdated

[comment]: # ({new-4c82c7ad})
# alert.get

[comment]: # ({/new-4c82c7ad})

[comment]: # ({542b58ad-ed55724c})
### Beschrijving

`integer/array alert.get(object parameters)`

De methode maakt het mogelijk om waarschuwingen op te halen volgens de gegeven parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/542b58ad-ed55724c})

[comment]: # ({67ecd2fd-218fa8cf})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|alertids|string/array|Retourneer alleen waarschuwingen met de opgegeven ID's.|
|actionids|string/array|Retourneer alleen waarschuwingen die zijn gegenereerd door de opgegeven acties.|
|eventids|string/array|Retourneer alleen waarschuwingen die zijn gegenereerd door de gegeven gebeurtenissen.|
|groupids|string/array|Retourneer alleen waarschuwingen die zijn gegenereerd door objecten van de opgegeven host groepen.|
|hostids|string/array|Retourneer alleen waarschuwingen die zijn gegenereerd door objecten van de opgegeven hosts.|
|mediatypeids|string/array|Retourneer alleen bericht waarschuwingen die de opgegeven mediatypen hebben gebruikt.|
|objectids|string/array|Retourneer alleen waarschuwingen die zijn gegenereerd door de opgegeven objecten|
|userids|string/array|Retourneer alleen bericht waarschuwingen die naar de opgegeven gebruikers zijn verzonden.|
|eventobject|integer|Retourneer alleen waarschuwingen die zijn gegenereerd door gebeurtenissen die verband houden met objecten van het opgegeven type.<br><br>Zie gebeurtenis ["object"](/manual/api/reference/event/object#event) voor een lijst met ondersteunde objecttypen.<br><br>Standaard: 0 - trigger.|
|eventsource|integer|Retourneer alleen waarschuwingen die zijn gegenereerd door gebeurtenissen van het opgegeven type.<br><br>Zie gebeurtenis ["source"](/manual/api/reference/event/object#event) voor een lijst met ondersteunde gebeurtenis typen .<br><br>Standaard: 0 - trigger gebeurtenissen.|
|time\_from|timestamp|Retourneer alleen waarschuwingen die na de opgegeven tijd zijn gegenereerd.|
|time\_till|timestamp|Retourneer alleen waarschuwingen die vóór de opgegeven tijd zijn gegenereerd.|
|selectHosts|query|Retourneer een eigenschap [hosts](/manual/api/reference/host/object) met gegevens van hosts die de actie hebben geactiveerd.|
|selectMediatypes|query|Retourneer een eigenschap [mediatypes](/manual/api/reference/mediatype/object) met een array van de mediatypen die zijn gebruikt voor de bericht waarschuwing.|
|selectUsers|query|Retourneer een eigenschap [users](/manual/api/reference/user/object) met een array van de gebruikers aan wie het bericht was geadresseerd.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `alertid`, `clock`, `eventid`, `mediatypeid`, `sendto` en `status`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/67ecd2fd-218fa8cf})

[comment]: # ({32334ca9-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` is
    gebruikt.

[comment]: # ({/32334ca9-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({745623b9-8d56d57c})
#### Meldingen ophalen op actie-ID

Haal alle waarschuwingen op die zijn gegenereerd door actie "3".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "alert.get",
    "params": {
        "output": "verlengen",
        "actionids": "3"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "alertid": "1",
            "actionid": "3",
            "eventid": "21243",
            "userid": "1",
            "clock": "1362128008",
            "mediatypeid": "1",
            "sendto": "support@company.com",
            "subject": "PROBLEEM: Zabbix-agent op Linux-server is 5 minuten onbereikbaar: ",
            "message": "Trigger: Zabbix-agent op Linux-server is 5 minuten niet bereikbaar: \nTriggerstatus: PROBLEEM\nSterkte trigger: niet geclassificeerd",
            "status": "0",
            "retries": "3",
            "error": "",
            "esc_step": "1",
            "alerttype": "0",
            "p_eventid": "0",
            "acknowledgeid": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/745623b9-8d56d57c})

[comment]: # ({de1f86d7-5371aed2})
### Zie ook

- [Host](/manual/api/reference/host/object#host)
- [Mediatype](/manual/api/reference/mediatype/object#media_type)
- [Gebruiker](/manual/api/reference/user/object#user)

[comment]: # ({/de1f86d7-5371aed2})

[comment]: # ({a35e83bd-14a85885})
### Bron

CAlert::get() in *ui/include/classes/api/services/CAlert.php*.

[comment]: # ({/a35e83bd-14a85885})
