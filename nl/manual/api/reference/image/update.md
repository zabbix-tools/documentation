[comment]: # translation:outdated

[comment]: # ({new-d229f871})
# image.update

[comment]: # ({/new-d229f871})

[comment]: # ({new-71e9b7b3})
### Description

`object image.update(object/array images)`

This method allows to update existing images.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-71e9b7b3})

[comment]: # ({new-b1c7183d})
### Parameters

`(object/array)` Image properties to be updated.

The `imageid` property must be defined for each image, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard image properties](object#image), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|image|string|Base64 encoded image. The maximum size of the encoded image is 1 MB. Maximum size can be adjusted by changing ZBX\_MAX\_IMAGE\_SIZE constant value.<br>Supported image formats are: PNG, JPEG, GIF.|

[comment]: # ({/new-b1c7183d})

[comment]: # ({new-a4a768f8})
### Return values

`(object)` Returns an object containing the IDs of the updated images
under the `imageids` property.

[comment]: # ({/new-a4a768f8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4aaa0fb1})
#### Rename image

Rename image to "Cloud icon".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "image.update",
    "params": {
        "imageid": "2",
        "name": "Cloud icon"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "imageids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4aaa0fb1})

[comment]: # ({new-5590c086})
### Source

CImage::update() in *ui/include/classes/api/services/CImage.php*.

[comment]: # ({/new-5590c086})
