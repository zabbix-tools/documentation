[comment]: # translation:outdated

[comment]: # ({87019d21-b1a4e412})
# Correlatie

Deze klasse is ontworpen om met correlaties te werken.

Objectreferenties:\

- [Correlatie](/manual/api/referentie/correlatie/object#correlatie)

Beschikbare methoden:\

- [correlatie.create](/manual/api/reference/correlation/create) -
    nieuwe correlaties creëren
- [correlation.delete](/manual/api/reference/correlation/delete) -
    correlaties verwijderen
- [correlation.get](/manual/api/reference/correlation/get) -
    correlaties ophalen
- [correlatie.update](/manual/api/reference/correlation/update) -
    correlaties bijwerken

[comment]: # ({/87019d21-b1a4e412})
