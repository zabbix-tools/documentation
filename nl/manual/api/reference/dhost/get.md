[comment]: # translation:outdated

[comment]: # ({new-37c3aad7})
# dhost.get

[comment]: # ({/new-37c3aad7})

[comment]: # ({304216ab-1309d6bb})
### Beschrijving

`integer/array dhost.get(objectparameters)`

De methode maakt het mogelijk om ontdekte hosts op te halen volgens de gegeven
parameters.

::: noteclassic
Deze methode is beschikbaar voor gebruikers van elk type. Rechten
om de methode aan te roepen, kunnen worden ingetrokken in de instellingen van de gebruikersrol. Zie [Gebruiker
rollen](/manual/web_interface/frontend_sections/administration/user_roles)
voor meer informatie.
:::

[comment]: # ({/304216ab-1309d6bb})

[comment]: # ({68fe51fb-061d7cf0})
### Parameters

`(object)` Parameters die de gewenste uitvoer definiëren.

De methode ondersteunt de volgende parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|---------|--------------------------------------- ------------|-----------|
|dhostids|string/array|Retourneer alleen ontdekte hosts met de opgegeven ID's.|
|druleids|string/array|Retourneer alleen ontdekte hosts die zijn gemaakt door de gegeven ontdekkingsregels.|
|dserviceids|string/array|Retourneer alleen ontdekte hosts die de opgegeven services uitvoeren.|
|selectDRules|query|Retourneer een eigenschap [drules](/manual/api/reference/drule/object) met een array van de detectieregels die de host hebben gedetecteerd.|
|selectDServices|query|Retourneer een eigenschap [dservices](/manual/api/reference/dservice/object) waarbij de gedetecteerde services op de host worden uitgevoerd.<br><br>Ondersteunt `count`.|
|limitSelects|integer|Beperkt het aantal records dat wordt geretourneerd door subselecties.<br><br>Van toepassing op de volgende subselecties:<br>`selectDServices` - resultaten worden gesorteerd op `dserviceid`.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen.<br><br>Mogelijke waarden zijn: `dhostid` en `druleid`.|
|countOutput|boolean|Deze parameters gelden voor alle `get`-methoden en worden in detail beschreven in de [referentiecommentaar](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|booleaans|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|geheel getal|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|booleaans|^|

[comment]: # ({/68fe51fb-061d7cf0})

[comment]: # ({32334ca9-7223bab1})
### Retourwaarden

`(integer/array)` Retourneert ofwel:

- een reeks objecten;
- het aantal opgehaalde objecten, als de parameter `countOutput` is
    gebruikt.

[comment]: # ({/32334ca9-7223bab1})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({1ed7f2f4-4b9c1e99})
#### Ontdekte hosts ophalen volgens ontdekkingsregel

Haal alle hosts en ontdekte services op die ontdekt zijn door ontdekkingsregel "4".

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "dhost.get",
    "params": {
        "output": "extend",
        "selectDServices": "extend",
        "druleids": "4"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "dservices": [
                {
                    "dserviceid": "1",
                    "dhostid": "1",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697227",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.1",
                    "dns": "station.company.lan"
                }
            ],
            "dhostid": "1",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697227",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "2",
                    "dhostid": "2",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.4",
                    "dns": "john.company.lan"
                }
            ],
            "dhostid": "2",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "3",
                    "dhostid": "3",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.26",
                    "dns": "printer.company.lan"
                }
            ],
            "dhostid": "3",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        },
        {
            "dservices": [
                {
                    "dserviceid": "4",
                    "dhostid": "4",
                    "type": "4",
                    "key_": "",
                    "value": "",
                    "port": "80",
                    "status": "0",
                    "lastup": "1337697234",
                    "lastdown": "0",
                    "dcheckid": "5",
                    "ip": "192.168.1.7",
                    "dns": "mail.company.lan"
                }
            ],
            "dhostid": "4",
            "druleid": "4",
            "status": "0",
            "lastup": "1337697234",
            "lastdown": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/1ed7f2f4-4b9c1e99})

[comment]: # ({39a1694b-e6b09247})
### Zie ook

- [Ontdekt
    service](/manual/api/reference/dservice/object#discovered_service)
- [Ontdekkingsregel](/manual/api/reference/drule/object#discovery_rule)

[comment]: # ({/39a1694b-e6b09247})

[comment]: # ({ae5abf34-8dc0c01a})
### Bron

CDHost::get() in *ui/include/classes/api/services/CDHost.php*.

[comment]: # ({/ae5abf34-8dc0c01a})
