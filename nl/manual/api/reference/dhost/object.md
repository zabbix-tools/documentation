[comment]: # translation:outdated

[comment]: # ({3e1015c8-854803de})
# > Host-object ontdekt

De volgende objecten zijn direct gerelateerd aan de `dhost` API.

[comment]: # ({/3e1015c8-854803de})

[comment]: # ({587d6d8c-236628fe})
### Host ontdekt

::: noteclassic
De ontdekte host wordt gemaakt door de Zabbix-server en kunnen niet
worden gewijzigd via de API.
:::

Het ontdekte host object bevat informatie over een ontdekte host
door een netwerkdetectie regel. Het heeft de volgende eigenschappen.

|Property|[Type](/manual/api/reference_commentary#data_types)|Beschrijving|
|--------|---------------------------------------- -----------|-----------|
|dhostid|string|ID van de gevonden host.|
|druleid|string|ID van de ontdekkingsregel die de host heeft gedetecteerd.|
|lastdown|timestamp|Tijd waarop de ontdekte host voor het laatst is uitgeschakeld.|
|lastup|timestamp|Tijd waarop de ontdekte host voor het laatst is geactiveerd.|
|status|integer|Of de ontdekte host omhoog of omlaag is. Een host is actief als deze ten minste één actieve gedetecteerde service heeft.<br><br>Mogelijke waarden:<br>0 - host up;<br>1 - host down.|

[comment]: # ({/587d6d8c-236628fe})
