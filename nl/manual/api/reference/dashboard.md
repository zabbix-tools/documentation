[comment]: # translation:outdated

[comment]: # ({2b17a3b3-03e77d6d})
# Dashboard

Deze klasse is ontworpen om met dashboards te werken.

Objectreferenties:\

- [Dashboard](/manual/api/referentie/dashboard/object#dashboard)
- [Dashboard
    pagina](/manual/api/reference/dashboard/object#dashboard_page)
- [Dashboard
    widget](/manual/api/reference/dashboard/object#dashboard_widget)
- [Dashboard-widget
    field](/manual/api/reference/dashboard/object#dashboard_widget_field)
- [Dashboard
    gebruiker](/manual/api/reference/dashboard/object#dashboard_user)
- [Dashboard-gebruiker
    group](/manual/api/reference/dashboard/object#dashboard_user_group)

Beschikbare methoden:\

- [dashboard.create](/manual/api/reference/dashboard/create) -
    nieuwe dashboards maken
- [dashboard.delete](/manual/api/reference/dashboard/delete) -
    dashboards verwijderen
- [dashboard.get](/manual/api/reference/dashboard/get) - ophalen
    dashboards
- [dashboard.update](/manual/api/reference/dashboard/update) -
    dashboards bijwerken

[comment]: # ({/2b17a3b3-03e77d6d})
