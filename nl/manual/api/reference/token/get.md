[comment]: # translation:outdated

[comment]: # ({new-d632e1b8})
# token.get

[comment]: # ({/new-d632e1b8})

[comment]: # ({new-980bb7e6})
### Description

`integer/array token.get(object parameters)`

The method allows to retrieve tokens according to the given parameters.

::: noteclassic
Only *Super admin* user type is allowed to view tokens for
other users.
:::

[comment]: # ({/new-980bb7e6})

[comment]: # ({new-cc094e72})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|tokenids|string/array|Return only tokens with the given IDs.|
|userids|string/array|Return only tokens created for the given users.|
|token|string|Return only tokens created for the given *Auth token*.|
|valid\_at|timestamp|Return only tokens which are valid (not expired) at the given date and time.|
|expired\_at|timestamp|Return only tokens which are expired (not valid) at the given date and time.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `tokenid`, `name`, `lastaccess`, `status`, `expires_at` and `created_at`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-cc094e72})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-66760f5f})
#### Retrieve an token

Retrieve all data for token with ID "2".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "token.get",
    "params": {
        "output": "extend",
        "tokenids": "2"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "tokenid": "1",
            "name": "The Token",
            "description": "",
            "userid": "1",
            "lastaccess": "0",
            "status": "0",
            "expires_at": "1609406220",
            "created_at": "1611239454",
            "creator_userid": "1"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-66760f5f})

[comment]: # ({new-5cf2ab45})
### Source

CToken::get() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/new-5cf2ab45})
