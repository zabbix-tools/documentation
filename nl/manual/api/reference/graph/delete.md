[comment]: # translation:outdated

[comment]: # ({new-cf547db2})
# graph.delete

[comment]: # ({/new-cf547db2})

[comment]: # ({new-c740de99})
### Description

`object graph.delete(array graphIds)`

This method allows to delete graphs.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c740de99})

[comment]: # ({new-54514063})
### Parameters

`(array)` IDs of the graphs to delete.

[comment]: # ({/new-54514063})

[comment]: # ({new-eb5c8bd8})
### Return values

`(object)` Returns an object containing the IDs of the deleted graphs
under the `graphids` property.

[comment]: # ({/new-eb5c8bd8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3a78f2ec})
#### Deleting multiple graphs

Delete two graphs.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "graph.delete",
    "params": [
        "652",
        "653"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "graphids": [
            "652",
            "653"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3a78f2ec})

[comment]: # ({new-e06af123})
### Source

CGraph::delete() in *ui/include/classes/api/services/CGraph.php*.

[comment]: # ({/new-e06af123})
