[comment]: # translation:outdated

[comment]: # ({4800f9f0-7fceb632})
# API-info

Deze klasse is ontworpen om meta-informatie over de API op te halen.

Beschikbare methoden:\

- [apiinfo.version](/manual/api/reference/apiinfo/version) -
    de versie van de Zabbix API ophalen

[comment]: # ({/4800f9f0-7fceb632})
