[comment]: # translation:outdated

[comment]: # ({c5d2b3ac-11ad57db})
# Authenticatie

Deze klasse is ontworpen om te werken met authenticatie-instellingen.

Objectreferenties:\

- [Authenticatie](/manual/api/reference/authentication/object#authentication)

Beschikbare methoden:\

- [authenticatie.get](/manual/api/reference/authentication/get) -
    authenticatie ophalen
- [authenticatie.update](/manual/api/reference/authentication/update) -
    authenticatie bijwerken

[comment]: # ({/c5d2b3ac-11ad57db})
