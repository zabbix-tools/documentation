[comment]: # translation:outdated

[comment]: # ({new-e42e8a0a})
# template.massupdate

[comment]: # ({/new-e42e8a0a})

[comment]: # ({new-cf27d8f8})
### Description

`object template.massupdate(object parameters)`

This method allows to simultaneously replace or remove related objects
and update properties on multiple templates.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-cf27d8f8})

[comment]: # ({new-1f7d5759})
### Parameters

`(object)` Parameters containing the IDs of the templates to update and
the properties that should be updated.

Additionally to the [standard template properties](object#template), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**templates**<br>(required)|object/array|Templates to be updated.<br><br>The templates must have the `templateid` property defined.|
|groups|object/array|Host groups to replace the current host groups the templates belong to.<br><br>The host groups must have the `groupid` property defined.|
|hosts|object/array|Hosts and templates to replace the ones the templates are currently linked to.<br><br>Both hosts and templates must use the `hostid` property to pass an ID.|
|macros|object/array|User macros to replace the current user macros on the given templates.|
|templates\_clear|object/array|Templates to unlink and clear from the given templates.<br><br>The templates must have the `templateid` property defined.|
|templates\_link|object/array|Templates to replace the currently linked templates.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-1f7d5759})

[comment]: # ({new-dcba01c8})
### Return values

`(object)` Returns an object containing the IDs of the updated templates
under the `templateids` property.

[comment]: # ({/new-dcba01c8})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-d18b091c})
#### Unlinking a template

Unlink and clear template "10091" from the given templates.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-d18b091c})

[comment]: # ({new-0ecefb75})
#### Replacing host groups

Unlink and clear template "10091" from the given templates.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "template.massupdate",
    "params": {
        "templates": [
            {
                "templateid": "10085"
            },
            {
                "templateid": "10086"
            }
        ],
        "templates_clear": [
            {
                "templateid": "10091"
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "templateids": [
            "10085",
            "10086"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-0ecefb75})

[comment]: # ({new-5d283e35})
### See also

-   [template.update](update)
-   [template.massadd](massadd)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)

[comment]: # ({/new-5d283e35})

[comment]: # ({new-b5f8dcd9})
### Source

CTemplate::massUpdate() in
*ui/include/classes/api/services/CTemplate.php*.

[comment]: # ({/new-b5f8dcd9})
