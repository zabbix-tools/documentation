[comment]: # translation:outdated

[comment]: # ({new-6a1da71f})
# Host

This class is designed to work with hosts.

Object references:\

-   [Host](/manual/api/reference/host/object#host)
-   [Host inventory](/manual/api/reference/host/object#host_inventory)

Available methods:\

-   [host.create](/manual/api/reference/host/create) - creating new
    hosts
-   [host.delete](/manual/api/reference/host/delete) - deleting hosts
-   [host.get](/manual/api/reference/host/get) - retrieving hosts
-   [host.massadd](/manual/api/reference/host/massadd) - adding related
    objects to hosts
-   [host.massremove](/manual/api/reference/host/massremove) - removing
    related objects from hosts
-   [host.massupdate](/manual/api/reference/host/massupdate) - replacing
    or removing related objects from hosts
-   [host.update](/manual/api/reference/host/update) - updating hosts

[comment]: # ({/new-6a1da71f})
