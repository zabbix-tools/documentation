[comment]: # translation:outdated

[comment]: # ({new-d8014f4f})
# > Settings object

The following object are directly related to the `settings` API.

[comment]: # ({/new-d8014f4f})

[comment]: # ({new-a08f9118})
### Settings

The settings object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|default\_lang|string|System language by default.<br><br>Default: `en_GB`.|
|default\_timezone|string|System time zone by default.<br><br>Default: `system` - system default.<br><br>For the full list of supported time zones please refer to [PHP documentation](https://www.php.net/manual/en/timezones.php).|
|default\_theme|string|Default theme.<br><br>Possible values:<br>`blue-theme` - *(default)* Blue;<br>`dark-theme` - Dark;<br>`hc-light` - High-contrast light;<br>`hc-dark` - High-contrast dark.|
|search\_limit|integer|Limit for search and filter results.<br><br>Default: 1000.|
|max\_overview\_table\_size|integer|Max number of columns and rows in Data overview and Trigger overview dashboard widgets.<br><br>Default: 50.|
|max\_in\_table|integer|Max count of elements to show inside table cell.<br><br>Default: 50.|
|server\_check\_interval|integer|Show warning if Zabbix server is down.<br><br>Possible values:<br>0 - Do not show warning;<br>10 - *(default)* Show warning.|
|work\_period|string|Working time.<br><br>Default: 1-5,09:00-18:00.|
|show\_technical\_errors|integer|Show technical errors (PHP/SQL) to non-Super admin users and to users that are not part of user groups with debug mode enabled.<br><br>Possible values:<br>0 - *(default)* Do not technical errors;<br>1 - Show technical errors.|
|history\_period|string|Max period to display history data in Latest data, Web, and Data overview dashboard widgets. Accepts seconds and time unit with suffix.<br><br>Default: 24h.|
|period\_default|string|Time filter default period. Accepts seconds and time unit with suffix with month and year support (30s,1m,2h,1d,1M,1y).<br><br>Default: 1h.|
|max\_period|string|Max period for time filter. Accepts seconds and time unit with suffix with month and year support (30s,1m,2h,1d,1M,1y).<br><br>Default: 2y.|
|severity\_color\_0|string|Color for "Not classified" severity as a hexadecimal color code.<br><br>Default: 97AAB3.|
|severity\_color\_1|string|Color for "Information" severity as a hexadecimal color code.<br><br>Default: 7499FF.|
|severity\_color\_2|string|Color for "Warning" severity as a hexadecimal color code.<br><br>Default: FFC859.|
|severity\_color\_3|string|Color for "Average" severity as a hexadecimal color code.<br><br>Default: FFA059.|
|severity\_color\_4|string|Color for "High" severity as a hexadecimal color code.<br><br>Default: E97659.|
|severity\_color\_5|string|Color for "Disaster" severity as a hexadecimal color code.<br><br>Default: E45959.|
|severity\_name\_0|string|Name for "Not classified" severity.<br><br>Default: Not classified.|
|severity\_name\_1|string|Name for "Information" severity.<br><br>Default: Information.|
|severity\_name\_2|string|Name for "Warning" severity.<br><br>Default: Warning.|
|severity\_name\_3|string|Name for "Average" severity.<br><br>Default: Average.|
|severity\_name\_4|string|Name for "High" severity.<br><br>Default: High.|
|severity\_name\_5|string|Name for "Disaster" severity.<br><br>Default: Disaster.|
|custom\_color|integer|Use custom event status colors.<br><br>Possible values:<br>0 - *(default)* Do not use custom event status colors;<br>1 - Use custom event status colors.|
|ok\_period|string|Display OK triggers period. Accepts seconds and time unit with suffix.<br><br>Default: 5m.|
|blink\_period|string|On status change triggers blink period. Accepts seconds and time unit with suffix.<br><br>Default: 2m.|
|problem\_unack\_color|string|Color for unacknowledged PROBLEM events as a hexadecimal color code.<br><br>Default: CC0000.|
|problem\_ack\_color|string|Color for acknowledged PROBLEM events as a hexadecimal color code.<br><br>Default: CC0000.|
|ok\_unack\_color|string|Color for unacknowledged RESOLVED events as a hexadecimal color code.<br><br>Default: 009900.|
|ok\_ack\_color|string|Color for acknowledged RESOLVED events as a hexadecimal color code.<br><br>Default: 009900.|
|problem\_unack\_style|integer|Blinking for unacknowledged PROBLEM events.<br><br>Possible values:<br>0 - Do not show blinking;<br>1 - *(default)* Show blinking.|
|problem\_ack\_style|integer|Blinking for acknowledged PROBLEM events.<br><br>Possible values:<br>0 - Do not show blinking;<br>1 - *(default)* Show blinking.|
|ok\_unack\_style|integer|Blinking for unacknowledged RESOLVED events.<br><br>Possible values:<br>0 - Do not show blinking;<br>1 - *(default)* Show blinking.|
|ok\_ack\_style|integer|Blinking for acknowledged RESOLVED events.<br><br>Possible values:<br>0 - Do not show blinking;<br>1 - *(default)* Show blinking.|
|url|string|Frontend URL.|
|discovery\_groupid|integer|ID of the host group to which will be automatically placed discovered hosts.|
|default\_inventory\_mode|integer|Default host inventory mode.<br><br>Possible values:<br>-1 - *(default)* Disabled;<br>0 - Manual;<br>1 - Automatic.|
|alert\_usrgrpid|integer|ID of the user group to which will be sending database down alarm message. If set to empty, the alarm message will not sent.|
|snmptrap\_logging|integer|Log unmatched SNMP traps.<br><br>Possible values:<br>0 - Do not log unmatched SNMP traps;<br>1 - *(default)* Log unmatched SNMP traps.|
|login\_attempts|integer|Number of failed login attempts after which login form will be blocked.<br><br>Default: 5.|
|login\_block|string|Time interval during which login form will be blocked if number of failed login attempts exceeds defined in login\_attempts field. Accepts seconds and time unit with suffix.<br><br>Default: 30s.|
|validate\_uri\_schemes|integer|Validate URI schemes.<br><br>Possible values:<br>0 - Do not validate;<br>1 - *(default)* Validate.|
|uri\_valid\_schemes|string|Valid URI schemes.<br><br>Default: http,https,ftp,file,mailto,tel,ssh.|
|x\_frame\_options|string|X-Frame-Options HTTP header.<br><br>Default: SAMEORIGIN.|
|iframe\_sandboxing\_enabled|integer|Use iframe sandboxing.<br><br>Possible values:<br>0 - Do not use;<br>1 - *(default)* Use.|
|iframe\_sandboxing\_exceptions|string|Iframe sandboxing exceptions.|
|connect\_timeout|string|Connection timeout with Zabbix server.<br><br>Default: 3s.|
|socket\_timeout|string|Network default timeout.<br><br>Default: 3s.|
|media\_type\_test\_timeout|string|Network timeout for media type test.<br><br>Default: 65s.|
|item\_test\_timeout|string|Network timeout for item tests.<br><br>Default: 60s.|
|script\_timeout|string|Network timeout for script execution.<br><br>Default: 60s.|
|report\_test\_timeout|string|Network timeout for scheduled report test.<br><br>Default: 60s.|
|auditlog\_enabled|integer|Enable audit logging.<br><br>Possible values:<br>0 - Disable;<br>1 - *(default)* Enable.|
|geomaps\_tile\_provider|string|Geomap tile provider.<br><br>Possible values:<br>`OpenStreetMap.Mapnik` - *(default)* OpenStreetMap Mapnik;<br>`OpenTopoMap` - OpenTopoMap;<br>`Stamen.TonerLite` - Stamen Toner Lite;<br>`Stamen.Terrain` - Stamen Terrain;<br>`USGS.USTopo` - USGS US Topo;<br>`USGS.USImagery` - USGS US Imagery.<br><br>Supports empty string to specify custom values of `geomaps_tile_url`, `geomaps_max_zoom` and `geomaps_attribution`.|
|geomaps\_tile\_url|string|Geomap tile URL if `geomaps_tile_provider` is set to empty string.|
|geomaps\_max\_zoom|integer|Geomap max zoom level if `geomaps_tile_provider` is set to empty string. Max zoom must be in the range between 0 and 30.|
|geomaps\_attribution|string|Geomap attribution text if `geomaps_tile_provider` is set to empty string.|

[comment]: # ({/new-a08f9118})
