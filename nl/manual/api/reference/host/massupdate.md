[comment]: # translation:outdated

[comment]: # ({new-4a6b3375})
# host.massupdate

[comment]: # ({/new-4a6b3375})

[comment]: # ({new-10cbc072})
### Description

`object host.massupdate(object parameters)`

This method allows to simultaneously replace or remove related objects
and update properties on multiple hosts.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-10cbc072})

[comment]: # ({new-6d664071})
### Parameters

`(object)` Parameters containing the IDs of the hosts to update and the
properties that should be updated.

Additionally to the [standard host properties](object#host), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**hosts**<br>(required)|object/array|[Hosts](/manual/api/reference/host/object) to be updated.<br><br>The hosts must have the `hostid` property defined.|
|groups|object/array|Host [groups](/ manual/api/reference/hostgroup/object) to replace the current host groups the hosts belong to.<br><br>The host groups must have the `groupid` property defined.|
|interfaces|object/array|Host [interfaces](/manual/api/reference/hostinterface/object) to replace the current host interfaces on the given hosts.|
|inventory|object|Host [inventory](/manual/api/reference/host/object#host_inventory) properties.<br><br>Host inventory mode cannot be updated using the `inventory` parameter, use `inventory_mode` instead.|
|macros|object/array|[User macros](/manual/api/reference/usermacro/object) to replace the current user macros on the given hosts.|
|templates|object/array|[Templates](/ manual/api/reference/template/object) to replace the currently linked templates on the given hosts.<br><br>The templates must have the `templateid` property defined.|
|templates\_clear|object/array|[Templates](/ manual/api/reference/template/object) to unlink and clear from the given hosts.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-6d664071})

[comment]: # ({new-d166b99b})
### Return values

`(object)` Returns an object containing the IDs of the updated hosts
under the `hostids` property.

[comment]: # ({/new-d166b99b})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-4a8d8b94})
#### Enabling multiple hosts

Enable monitoring of two hosts, i.e., set their status to 0.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.massupdate",
    "params": {
        "hosts": [
            {
                "hostid": "69665"
            },
            {
                "hostid": "69666"
            }
        ],
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "hostids": [
            "69665",
            "69666"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-4a8d8b94})

[comment]: # ({new-4b09fb40})
### See also

-   [host.update](update)
-   [host.massadd](massadd)
-   [host.massremove](massremove)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)
-   [Template](/manual/api/reference/template/object#host_group)
-   [User
    macro](/manual/api/reference/usermacro/object#hosttemplate_level_macro)
-   [Host
    interface](/manual/api/reference/hostinterface/object#host_interface)

[comment]: # ({/new-4b09fb40})

[comment]: # ({new-ad74e98b})
### Source

CHost::massUpdate() in *ui/include/classes/api/services/CHost.php*.

[comment]: # ({/new-ad74e98b})
