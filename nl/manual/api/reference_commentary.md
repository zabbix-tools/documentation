[comment]: # translation:outdated

[comment]: # ({52bde805-6d056871})
# Bijlage 1. Referentie commentaar

[comment]: # ({/52bde805-6d056871})

[comment]: # ({f552a5ff-6594bdc4})
### Notatie

[comment]: # ({/f552a5ff-6594bdc4})

[comment]: # ({6007f535-2df08058})
#### Gegevenstypen

De Zabbix API ondersteunt de volgende gegevenstypen als invoer:

|Type|Beschrijving|
|----|-----------|
|boolean|Een booleaanse waarde, accepteert `true` of `false`.|
|flag|De waarde wordt als `true` beschouwd als deze wordt doorgegeven en anders niet gelijk aan `null` en `false`.|
|integer|Een geheel getal.|
|float|Een getal met drijvende komma.|
|string|Een tekststring.|
|text|Een langere tekenreeks.|
|timestamp|Een Unix-tijdstempel.|
|array|Een geordende reeks waarden, dat wil zeggen een gewone array.|
|object|Een associatieve array.|
|query|Een waarde die bepaalt welke gegevens moeten worden geretourneerd.<br><br>Kan worden gedefinieerd als een array van eigenschaps namen om alleen specifieke eigenschappen te retourneren, of als een van de vooraf gedefinieerde waarden:<br>`extend` - geeft alle objecteigenschappen terug;<br>`count` - retourneert het aantal opgehaalde records, alleen ondersteund door bepaalde sub selects.|

::: noteimportant
Zabbix API retourneert altijd waarden als tekenreeksen of
alleen arrays.
:::

[comment]: # ({/6007f535-2df08058})

[comment]: # ({0e471ef6-a2c16a18})
#### Eigenschapslabels

Sommige eigenschappen van objecten zijn gemarkeerd met korte labels om te beschrijven
hun gedrag. De volgende labels worden gebruikt:

- *readonly* - de waarde van de eigenschap wordt automatisch ingesteld en
    kan niet worden gedefinieerd of gewijzigd door de client;
- *constant* - de waarde van de eigenschap kan worden ingesteld bij het maken van een
    object, maar kan daarna niet worden gewijzigd.

[comment]: # ({/0e471ef6-a2c16a18})

[comment]: # ({new-f255da9d})
#### Parameter behavior

Some of the operation parameters are marked with short labels to describe their behavior for the operation.
The following labels are used:

-   *read-only* - the value of the parameter is set automatically and cannot be defined or changed by the user, even in some specific conditions
    (e.g., *read-only* for inherited objects or discovered objects);
-   *write-only* - the value of the parameter can be set, but cannot be accessed after;
-   *supported* - the value of the parameter is not required to be set, but is allowed to be set in some specific conditions
    (e.g., *supported* if `status` of Proxy object is set to "passive proxy");
-   *required* - the value of the parameter is required to be set.

Parameters that are not marked with labels are optional.

[comment]: # ({/new-f255da9d})

[comment]: # ({b42123a6-5e99748d})
### Gereserveerde ID-waarde "0"

Gereserveerde ID-waarde "0" kan worden gebruikt om elementen te filteren en om
objecten waarnaar wordt verwezen te verwijderen. Om bijvoorbeeld een proxy waarnaar wordt verwezen te verwijderen uit een
host, moet proxy\_hostid worden ingesteld op 0 ("proxy\_hostid": "0") of op
hosts te filteren die door de server bewaakt worden, moet de optie proxy-id's ingesteld worden op 0
("proxyids": "0").

[comment]: # ({/b42123a6-5e99748d})

[comment]: # ({34f6c837-b4e0f5ab})
### Algemene "get" methode parameters

De volgende parameters worden ondersteund door alle `get`-methoden:

|Parameter|[Type](#data_types)|Beschrijving|
|---------|-------------------|-----------|
|countOutput|boolean|Retourneert het aantal records in het resultaat in plaats van de werkelijke gegevens.|
|editable|boolean|Indien ingesteld op `true`, worden alleen objecten geretourneerd waarvoor de gebruiker schrijfrechten heeft.<br><br>Standaard: `false`.|
|excludeSearch|boolean|Retourneert resultaten die niet overeenkomen met de criteria die zijn opgegeven in de `search`-parameter.|
|filter|object|Retourneert alleen die resultaten die exact overeenkomen met het opgegeven filter.<br><br>Accepteert een array, waarbij de sleutels eigenschaps namen zijn en de waarden een enkele waarde zijn of een array van waarden om mee te vergelijken. <br><br>Werkt niet voor `tekst`-velden.|
|limit|integer|Beperk het aantal geretourneerde records.|
|output|query|Te retourneren objecteigenschappen.<br><br>Standaard: `extend`.|
|preservekeys|boolean|Gebruik ID's als sleutels in de resulterende array.|
|search|object|Retourneert resultaten die overeenkomen met de opgegeven zoekopdracht met joker tekens (niet hoofdlettergevoelig).<br><br>Accepteert een array, waarbij de sleutels eigenschapnamen zijn en de waarden tekenreeksen zijn waarnaar moet worden gezocht. Als er geen extra opties worden gegeven, zal dit een `LIKE "%…%"` zoekactie uitvoeren.<br><br>Werkt alleen voor `string` en `text` velden.|
|searchByAny|boolean|Indien ingesteld op `true`, worden resultaten geretourneerd die overeenkomen met een van de criteria die zijn opgegeven in de parameter `filter` of `search` in plaats van allemaal.<br><br>Standaard: `false`.|
|searchWildcardsEnabled|boolean|Indien ingesteld op `true`, wordt het gebruik van "\*" als joker teken in de parameter `search` ingeschakeld.<br><br>Standaard: `false`.|
|sortfield|string/array|Sorteer het resultaat op de gegeven eigenschappen. Raadpleeg een specifieke API get-methode beschrijving voor een lijst met eigenschappen die kunnen worden gebruikt om te sorteren. Macro's worden niet uitgevouwen voordat ze worden gesorteerd.<br><br>Als er geen waarde is opgegeven, worden de gegevens ongesorteerd geretourneerd.|
|sortorder|string/array|Orde van sorteren. Als een array wordt doorgegeven, wordt elke waarde gekoppeld aan de overeenkomstige eigenschap die is opgegeven in de parameter `sortfield`.<br><br>Mogelijke waarden zijn:<br>`ASC` - *(standaard)* oplopend;<br> `DESC` - aflopend.|
|startSearch|boolean|De `search` parameter vergelijkt het begin van velden, dat wil zeggen, voer in plaats daarvan een `LIKE "…%"` zoekopdracht uit.<br><br>Negeert als `searchWildcardsEnabled` is ingesteld op `true`. |

[comment]: # ({/34f6c837-b4e0f5ab})

[comment]: # ({aa9a5ad3-b41637d2})
### Voorbeelden

[comment]: # ({/aa9a5ad3-b41637d2})

[comment]: # ({3991bfe1-7a121fac})
#### Controle gebruikersrechten

Heeft de gebruiker rechten om te schrijven naar host waarvan de namen beginnen met 
"MySQL" of "Linux" ?

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": ["MySQL", "Linux"]
        },
        "editable": true,
        "startSearch": true,
        "searchByAny": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0",
    "id": 1
}
```

::: noteclassic
Geen resultaat betekend dat er geen hosts met lezen/schrijven rechten zijn
:::

[comment]: # ({/3991bfe1-7a121fac})

[comment]: # ({735c4700-ea3cccd8})
#### Tellen van niet-matches

Tellen van het aantal hosts waarin het woord "ubuntu" niet in voor komt.

VCerzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "countOutput": true,
        "search": {
            "host": "ubuntu"
        },
        "excludeSearch": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "44",
    "id": 1
}
```

[comment]: # ({/735c4700-ea3cccd8})

[comment]: # ({4047b982-e911c08b})
#### Zoeken van hosts met jokertekens

Vind hosts waarin "server" in de naam voor komt en interface poorten "10050" of "10071" hebben. Sorteer het resultaat per hostname in aflopende volgorde en beperk de resultaten tot 5 hosts.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/4047b982-e911c08b})

[comment]: # ({c22e59af-8ce6f554})
#### Zoeken naar hosts met jokertekens met "preservekeys"

Als u de parameter "preservekeys" toevoegt aan het vorige verzoek, wordt de
resultaat wordt geretourneerd als een associatieve array, waarbij de sleutels de id zijn van
de objecten.

Verzoek:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["hostid", "host"],
        "selectInterfaces": ["port"],
        "filter": {
            "port": ["10050", "10071"]
        },
        "search": {
            "host": "*server*"
        },
        "searchWildcardsEnabled": true,
        "searchByAny": true,
        "sortfield": "host",
        "sortorder": "DESC",
        "limit": 5,
        "preservekeys": true
    },
    "auth": "766b71ee543230a1182ca5c44d353e36",
    "id": 1
}
```

Antwoord:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "50003": {
            "hostid": "50003",
            "host": "WebServer-Tomcat02",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "50005": {
            "hostid": "50005",
            "host": "WebServer-Tomcat01",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },        
        "50004": {
            "hostid": "50004",
            "host": "WebServer-Nginx",
            "interfaces": [
                {
                    "port": "10071"
                }
            ]
        },
        "99032": {
            "hostid": "99032",
            "host": "MySQL server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        },
        "99061": {
            "hostid": "99061",
            "host": "Linux server 01",
            "interfaces": [
                {
                    "port": "10050"
                }
            ]
        }
    },
    "id": 1
}
```

[comment]: # ({/c22e59af-8ce6f554})
