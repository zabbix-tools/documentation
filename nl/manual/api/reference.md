[comment]: # translation:outdated

[comment]: # ({e9def6ff-b0d207c5})
# Methode referentie

Dit gedeelte geeft een overzicht van de functies van de
Zabbix API en zal je helpen je weg te vinden in de beschikbare klassen
en methoden.

[comment]: # ({/e9def6ff-b0d207c5})

[comment]: # ({124c1957-53edf72e})
### Toezicht houden

Met de Zabbix API hebt u toegang tot de geschiedenis en andere verzamelde gegevens
tijdens het monitoren.

[comment]: # ({/124c1957-53edf72e})

[comment]: # ({0eaebf2f-ead86ed5})
#### Cluster met hoge beschikbaarheid

Een lijst met server nodes en hun status ophalen.

[Hoge beschikbaarheid cluster-API](/manual/api/reference/hanode)

[comment]: # ({/0eaebf2f-ead86ed5})

[comment]: # ({b0a6fc1d-46aecf9d})
#### Geschiedenis

Haal historische waarden op die zijn verzameld door Zabbix-bewakingsprocessen voor:
presentatie of verdere verwerking.

[Geschiedenis-API](/manual/api/referentie/geschiedenis)

[comment]: # ({/b0a6fc1d-46aecf9d})

[comment]: # ({fa9b757f-ce08707b})
#### Trends

Haal trendwaarden op die zijn berekend door de Zabbix-server voor presentatie of
verdere verwerking.

[Trend-API](/manual/api/referentie/trend)

[comment]: # ({/fa9b757f-ce08707b})

[comment]: # ({31266a70-4e280d13})
#### Evenementen

Gebeurtenissen ophalen die zijn gegenereerd door triggers, netwerkdetectie en andere
Zabbix-systemen voor flexibeler situatiebeheer of derden
tool integratie.

[Event API](/manual/api/referentie/event)

[comment]: # ({/31266a70-4e280d13})

[comment]: # ({9ce1adbf-087f1047})
#### Problemen

Ophalen van problemen volgens de gegeven parameters.

[Probleem-API](/manual/api/referentie/probleem)

[comment]: # ({/9ce1adbf-087f1047})


[comment]: # ({new-2b35beaa})
#### Service Level Agreement

Define Service Level Objectives (SLO), retrieve detailed Service Level Indicators (SLI)
information about service performance.

[SLA API](/manual/api/reference/sla)

[comment]: # ({/new-2b35beaa})

[comment]: # ({d405ac7f-ea863fe1})
#### Taken

Interactie met Zabbix-server taakbeheer, taken maken en ophalen
van antwoord.

[Taak-API](/manual/api/referentie/taak)

[comment]: # ({/d405ac7f-ea863fe1})

[comment]: # ({new-services})
## Services

The Zabbix API allows you to access data gathered
during service monitoring.

[comment]: # ({/new-services})

[comment]: # ({d25d774e-467838af})
### Configuratie

Met de Zabbix API kunt u de configuratie van uw monitoring systeem beheren.

[comment]: # ({/d25d774e-467838af})

[comment]: # ({aa41ad64-4fe0de77})
#### Hosts en Host groepen

Beheer host groepen, hosts en alles wat daarmee verband houdt, inclusief host
interfaces, hostmacro's en onderhoudsperiodes.

[Host-API](/manual/api/reference/host) | [Gastgroep
API](/manual/api/referentie/hostgroep) | [Host-interface
API](/manual/api/reference/hostinterface) | [Gebruikersmacro
API](/manual/api/reference/usermacro) | [Waardekaart
API](/manual/api/reference/valuemap) | [Onderhoud
API](/manual/api/referentie/onderhoud)

[comment]: # ({/aa41ad64-4fe0de77})

[comment]: # ({23a714a7-29ae0d97})
#### Artikelen

Definieer items om te controleren.

[Item-API](/manual/api/referentie/item)

[comment]: # ({/23a714a7-29ae0d97})

[comment]: # ({f4dcb6ce-de0198b3})
#### Triggers

Configureer triggers om u op de hoogte te stellen van problemen in uw systeem. Beheren van 
afhankelijkheden.

[Trigger-API](/manual/api/referentie/trigger)

[comment]: # ({/f4dcb6ce-de0198b3})

[comment]: # ({ccee699f-f61b98cf})
#### Grafieken

Bewerk grafieken of scheid grafiek items voor een betere presentatie van de
verzamelde gegevens.

[Graph API](/manual/api/reference/graph) | [Grafiekitem
API](/manual/api/referentie/graphitem)

[comment]: # ({/ccee699f-f61b98cf})

[comment]: # ({2a996c5f-7a170077})
#### Sjablonen

Beheer sjablonen en koppel ze aan hosts of andere sjablonen.

[Template API](/manual/api/reference/template) | [Waardekaart
API](/manual/api/referentie/valuemap)

[comment]: # ({/2a996c5f-7a170077})

[comment]: # ({c61b5017-56f63ece})
#### Export en import

Exporteer en importeer Zabbix-configuratiegegevens voor configuratie back-ups,
migratie of grootschalige configuratie-updates.

[Configuratie-API](/manual/api/referentie/configuratie)

[comment]: # ({/c61b5017-56f63ece})

[comment]: # ({7d4a2007-61993c62})
#### Ontdekking op laag niveau

Configureer detectie regels op laag niveau, evenals item, trigger en grafiek
prototypes om dynamische entiteiten te monitoren.

[LLD-regel-API](/manual/api/reference/discoveryrule) | [Artikel prototype
API](/manual/api/referentie/itemprototype) | [Trigger-prototype
API](/manual/api/referentie/triggerprototype) | [Grafiek prototype
API](/manual/api/reference/graphprototype) | [Host-prototype
API](/manual/api/referentie/hostprototype)

[comment]: # ({/7d4a2007-61993c62})

[comment]: # ({f777211a-63c25731})
#### Evenement correlatie

Maak aangepaste correlatieregels voor gebeurtenissen.

[Correlatie-API](/manual/api/referentie/correlatie)

[comment]: # ({/f777211a-63c25731})

[comment]: # ({27fa8195-3ea1b33b})
#### Acties en waarschuwingen

Definieer acties en bewerkingen om gebruikers op de hoogte te stellen van bepaalde gebeurtenissen of
automatisch opdrachten op afstand uitvoeren. Toegang krijgen tot informatie over
gegenereerde waarschuwingen en hun ontvangers.

[Action API](/manual/api/reference/action) | [Waarschuwing
API](/manual/api/referentie/waarschuwing)

[comment]: # ({/27fa8195-3ea1b33b})

[comment]: # ({new-media})
#### Media types

Configure media types and multiple ways users will receive alerts.

[Media type API](/manual/api/reference/mediatype)

[comment]: # ({/new-media})

[comment]: # ({aff24718-891cbcad})
#### Diensten

Services beheren voor monitoring op serviceniveau en gedetailleerde SLA ophalen
informatie over elke dienst.

[Service-API](/manual/api/referentie/service)

[comment]: # ({/aff24718-891cbcad})

[comment]: # ({5ff24843-1a3dcbcf})
#### Dashboards

Beheer dashboards en maak op basis daarvan geplande rapporten.

[Dashboard API](/manual/api/referentie/dashboard) | [Sjabloondashboard
API](/manual/api/reference/templateashboard) | [Verslag doen van
API](/manual/api/referentie/rapport)

[comment]: # ({/5ff24843-1a3dcbcf})

[comment]: # ({e7a8376e-29b6c9a4})
#### Kaarten

Configureer kaarten om gedetailleerde dynamische weergaven van uw IT infrastructuur te maken.

[Map API](/manual/api/referentie/kaart)

[comment]: # ({/e7a8376e-29b6c9a4})

[comment]: # ({cf18887a-e083e0e0})
#### Web bewaking

Configureer web scenario's om uw web toepassingen en -services te bewaken.

[Webscenario-API](/manual/api/reference/httptest)

[comment]: # ({/cf18887a-e083e0e0})

[comment]: # ({new-alerts})
## Alerts

The Zabbix API allows you to manage the actions and alerts of your monitoring system.

[comment]: # ({/new-alerts})

[comment]: # ({15cf3c52-91788676})
#### Netwerk ontdekking

Beheer detectie regels op netwerkniveau om automatisch te zoeken en te controleren
nieuwe gastheren. Krijg volledige toegang tot informatie over ontdekte services en
gastheren.

[Discovery rule API](/manual/api/reference/drule) | [Ontdekkingscontrole
API](/manual/api/reference/dcheck) | [Ontdekte gastheer
API](/manual/api/reference/dhost) | [Ontdekte dienst
API](/manual/api/referentie/dservice)

[comment]: # ({/15cf3c52-91788676})

[comment]: # ({a73d45cf-ad3cb7b9})
### Administratie

Met de Zabbix API kunt u de beheer instellingen van uw controlesysteem.

[comment]: # ({/a73d45cf-ad3cb7b9})

[comment]: # ({bbe9bb5d-c2d096e3})
#### Gebruikers

Voeg gebruikers toe die toegang hebben tot Zabbix, wijs ze toe aan gebruikersgroepen
en machtigingen verlenen. Maak rollen voor gedetailleerd gebruikersbeheer
rechten. Volg configuratie wijzigingen die elke gebruiker heeft aangebracht. Media configureren
typen en meerdere manieren waarop gebruikers waarschuwingen ontvangen.

[Gebruikers-API](/manual/api/reference/user) | [Gebruikersgroep
API](/manual/api/referentie/gebruikersgroep) | [Gebruikersrol
API](/manual/api/referentie/rol) | [Mediatype
API](/manual/api/referentie/mediatype) | [Auditlogboek
API](/manual/api/referentie/auditlog)

[comment]: # ({/bbe9bb5d-c2d096e3})

[comment]: # ({5f691fd3-925840a1})
#### Algemeen

Wijzig bepaalde algemene configuratie-opties.

[API voor automatische registratie](/manual/api/referentie/autoregistratie) | [Icoon
map API](/manual/api/reference/iconmap) | [Afbeelding
API](/manual/api/referentie/afbeelding) | [Gebruikersmacro
API](/manual/api/reference/usermacro) | [Instellingen
API](/manual/api/referentie/instellingen) | [Huishouden
API](/manual/api/referentie/huishouding)

[comment]: # ({/5f691fd3-925840a1})

[comment]: # ({new-audit})
#### Audit log

Track configuration changes each user has done.

[Audit log API](/manual/api/reference/auditlog)

[comment]: # ({/new-audit})

[comment]: # ({new-housekeeping})
#### Housekeeping

Configure housekeeping.

[Housekeeping API](/manual/api/reference/housekeeping)

[comment]: # ({/new-housekeeping})


[comment]: # ({51713d65-feb914d9})
#### Proxy's

Beheer de proxy's die worden gebruikt in uw gedistribueerde bewakingsconfiguratie.

[Proxy-API](/manual/api/referentie/proxy)

[comment]: # ({/51713d65-feb914d9})

[comment]: # ({new-macros})
#### Macros

Manage macros.

[User macro API](/manual/api/reference/usermacro)

[comment]: # ({/new-macros})

[comment]: # ({be86d593-a1c36c8e})
#### Authenticatie

Configuratie-opties voor authenticatie wijzigen.

[Authenticatie-API](/manual/api/referentie/authenticatie)

[comment]: # ({/be86d593-a1c36c8e})

[comment]: # ({45f1f65b-4050de56})
#### API-tokens

Beheer autorisatie tokens.

[Token API](/manual/api/referentie/token)

[comment]: # ({/45f1f65b-4050de56})

[comment]: # ({af09110b-4e5421dd})
#### Scripts

Configureer en voer scripts uit om u te helpen bij uw dagelijkse taken.

[Script-API](/manual/api/referentie/script)

[comment]: # ({/af09110b-4e5421dd})

[comment]: # ({new-users})
## Users

The Zabbix API allows you to manage users of your monitoring system.

[comment]: # ({/new-users})

[comment]: # ({94de4d69-3b3700b0})
### API-informatie

Haal de versie van de Zabbix API op zodat uw toepassing versie-specifieke functies kan gebruiken.

[API info API](/manual/api/reference/apiinfo)

[comment]: # ({/94de4d69-3b3700b0})
