[comment]: # translation:outdated

[comment]: # ({new-88a3dfb2})
# 11 Smart plugin

[comment]: # ({/new-88a3dfb2})

[comment]: # ({new-dc0ed23f})
#### Overview

This section lists parameters supported in the Smart Zabbix agent 2
plugin configuration file (smart.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-dc0ed23f})

[comment]: # ({new-4577d3c5})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Smart.Path|no|<|smartctl|Path to the smartctl executable.|
|Plugins.Smart.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-4577d3c5})
