<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/appendix/items/perf_counters.md">
    <body>
      <trans-unit id="e03ef403" xml:space="preserve">
        <source># 16 Creating custom performance counter names for VMware</source>
      </trans-unit>
      <trans-unit id="eaab320d" xml:space="preserve">
        <source>### Overview

The VMware performance counter path has the
`group/counter[rollup]` format where:

-   `group` - the performance counter group, for example *cpu*
-   `counter` - the performance counter name, for example *usagemhz*
-   `rollup` - the performance counter rollup type, for example
    *average*

So the above example would give the following counter path:
`cpu/usagemhz[average]`

The performance counter group descriptions, counter names and rollup
types can be found in [VMware
documentation](https://developer.vmware.com/apis/968).

It is possible to obtain internal names and create custom performance counter names by using script item in Zabbix.</source>
      </trans-unit>
      <trans-unit id="2d867bd9" xml:space="preserve">
        <source>
### Configuration

1. Create disabled Script item on the main VMware host (where the **eventlog[]** item is present) with the following parameters:

![](../../../../assets/en/manual/appendix/items/perf_counter_item.png)

- *Name*:  VMware metrics
- *Type*: Script
- *Key*: vmware.metrics
- *Type of information*: Text
- *Script*: copy and paste the [script](#script) provided below
- *Timeout*: 10
- *History storage period*: Do not keep history
- *Enabled*: unmarked


#### Script

    try {
        Zabbix.log(4, 'vmware metrics script');

        var result, resp,
        req = new HttpRequest();
        req.addHeader('Content-Type: application/xml');
        req.addHeader('SOAPAction: "urn:vim25/6.0"');

        login = '&lt;soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:vim25"&gt;\
        &lt;soapenv:Header/&gt;\
        &lt;soapenv:Body&gt;\
            &lt;urn:Login&gt;\
                &lt;urn:_this type="SessionManager"&gt;SessionManager&lt;/urn:_this&gt;\
                &lt;urn:userName&gt;{$VMWARE.USERNAME}&lt;/urn:userName&gt;\
                &lt;urn:password&gt;{$VMWARE.PASSWORD}&lt;/urn:password&gt;\
            &lt;/urn:Login&gt;\
        &lt;/soapenv:Body&gt;\
    &lt;/soapenv:Envelope&gt;'
        resp = req.post("{$VMWARE.URL}", login);
        if (req.getStatus() != 200) {
            throw 'Response code: '+req.getStatus();
        }

        query = '&lt;soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:vim25"&gt;\
    &lt;soapenv:Header/&gt;\
        &lt;soapenv:Body&gt;\
            &lt;urn:RetrieveProperties&gt;\
                &lt;urn:_this type="PropertyCollector"&gt;propertyCollector&lt;/urn:_this&gt;\
                &lt;urn:specSet&gt;\
                    &lt;urn:propSet&gt;\
                       &lt;urn:type&gt;PerformanceManager&lt;/urn:type&gt;\
                       &lt;urn:pathSet&gt;perfCounter&lt;/urn:pathSet&gt;\
                    &lt;/urn:propSet&gt;\
                    &lt;urn:objectSet&gt;\
                       &lt;urn:obj type="PerformanceManager"&gt;PerfMgr&lt;/urn:obj&gt;\
                    &lt;/urn:objectSet&gt;\
                &lt;/urn:specSet&gt;\
            &lt;/urn:RetrieveProperties&gt;\
        &lt;/soapenv:Body&gt;\
    &lt;/soapenv:Envelope&gt;'
        resp = req.post("{$VMWARE.URL}", query);
        if (req.getStatus() != 200) {
            throw 'Response code: '+req.getStatus();
        }
        Zabbix.log(4, 'vmware metrics=' + resp);
        result = resp;

        logout = '&lt;soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:vim25"&gt;\
        &lt;soapenv:Header/&gt;\
        &lt;soapenv:Body&gt;\
            &lt;urn:Logout&gt;\
                &lt;urn:_this type="SessionManager"&gt;SessionManager&lt;/urn:_this&gt;\
            &lt;/urn:Logout&gt;\
        &lt;/soapenv:Body&gt;\
    &lt;/soapenv:Envelope&gt;'

        resp = req.post("{$VMWARE.URL}",logout);         
        if (req.getStatus() != 200) {
            throw 'Response code: '+req.getStatus();
        }

    } catch (error) {
        Zabbix.log(4, 'vmware call failed : '+error);
        result = {};
    }

    return result;


Once the item is configured, press *Test* button, then press *Get value*. 

![](../../../../assets/en/manual/appendix/items/perf_counter_item1.png){width=600}

Copy received XML to any XML formatter and find the desired metric.

An example of XML for one metric: 

    &lt;PerfCounterInfo xsi:type="PerfCounterInfo"&gt;
        &lt;key&gt;6&lt;/key&gt;
        &lt;nameInfo&gt;
            &lt;label&gt;Usage in MHz&lt;/label&gt;
            &lt;summary&gt;CPU usage in megahertz during the interval&lt;/summary&gt;
            &lt;key&gt;usagemhz&lt;/key&gt;
        &lt;/nameInfo&gt;
        &lt;groupInfo&gt;
            &lt;label&gt;CPU&lt;/label&gt;
            &lt;summary&gt;CPU&lt;/summary&gt;
            &lt;key&gt;cpu&lt;/key&gt;
        &lt;/groupInfo&gt;
        &lt;unitInfo&gt;
            &lt;label&gt;MHz&lt;/label&gt;
            &lt;summary&gt;Megahertz&lt;/summary&gt;
            &lt;key&gt;megaHertz&lt;/key&gt;
        &lt;/unitInfo&gt;
        &lt;rollupType&gt;average&lt;/rollupType&gt;
        &lt;statsType&gt;rate&lt;/statsType&gt;
        &lt;level&gt;1&lt;/level&gt;
        &lt;perDeviceLevel&gt;3&lt;/perDeviceLevel&gt;
    &lt;/PerfCounterInfo&gt;

Use XPath to extract the counter path from received XML. For the example above, the XPath will be:

|field |xPath | value |
|--|--|--|
|group | //groupInfo[../key=6]/key | cpu |
|counter |//nameInfo[../key=6]/key |usagemhz |
|rollup |//rollupType[../key=6] |average |

Resulting performance counter path in this case is: `cpu/usagemhz[average]`</source>
      </trans-unit>
    </body>
  </file>
</xliff>
