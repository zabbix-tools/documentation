[comment]: # translation:outdated

[comment]: # ({new-930fae3b})
# 14 Configuring Kerberos with Zabbix

[comment]: # ({/new-930fae3b})

[comment]: # ({new-5be9f534})
#### Overview

Kerberos authentication can be used in web monitoring and HTTP items in
Zabbix since version 4.4.0.

This section describes an example of configuring Kerberos with Zabbix
server to perform web monitoring of `www.example.com` with user
'zabbix'.

[comment]: # ({/new-5be9f534})

[comment]: # ({new-edb96e72})
#### Steps

[comment]: # ({/new-edb96e72})

[comment]: # ({new-33108ef2})
##### Step 1

Install Kerberos package.

For Debian/Ubuntu:

    apt install krb5-user

For RHEL/CentOS:

    yum install krb5-workstation

[comment]: # ({/new-33108ef2})

[comment]: # ({new-b841f368})
##### Step 2

Configure Kerberos configuration file (see MIT documentation for
details)

``` {.java}
cat /etc/krb5.conf 
[libdefaults]
    default_realm = EXAMPLE.COM

[comment]: # ({/new-b841f368})

[comment]: # ({new-90a52eb3})
# The following krb5.conf variables are only for MIT Kerberos.
    kdc_timesync = 1
    ccache_type = 4
    forwardable = true
    proxiable = true

[realms]
    EXAMPLE.COM = {
    }

[domain_realm]
    .example.com=EXAMPLE.COM
    example.com=EXAMPLE.COM

```

[comment]: # ({/new-90a52eb3})

[comment]: # ({new-ffbcf49a})
##### Step 3

Create a Kerberos ticket for user *zabbix*. Run the following command as
user *zabbix*:

    kinit zabbix

::: noteimportant
It is important to run the above command as user
*zabbix*. If you run it as *root* the authentication will not
work.
:::

[comment]: # ({/new-ffbcf49a})

[comment]: # ({new-01fcf461})
##### Step 4

Create a web scenario or HTTP agent item with Kerberos authentication
type.

Optionally can be tested with the following curl command:

    curl -v --negotiate -u : http://example.com

Note that for lengthy web monitoring it is necessary to take care of
renewing the Kerberos ticket. Default time of ticket expiration is 10h.

[comment]: # ({/new-01fcf461})
