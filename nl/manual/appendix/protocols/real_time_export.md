[comment]: # translation:outdated

[comment]: # ({new-cb9df6d6})
# 5 Real-time export protocol

This section presents details of the [real-time
export](/manual/appendix/install/real_time_export) protocol in a
newline-delimited JSON format for:

-   [trigger events](#trigger_events)
-   [item values](#item_values)
-   [trends](#trends)

All files have a .ndjson extension. Each line of the export file is a
JSON object.

[comment]: # ({/new-cb9df6d6})

[comment]: # ({new-bc637674})
#### Trigger events

The following information is exported for a problem event:

|Field|<|<|Type|Description|
|-----|-|-|----|-----------|
|*clock*|<|<|number|Number of seconds since Epoch to the moment when problem was detected (integer part).|
|*ns*|<|<|number|Number of nanoseconds to be added to `clock` to get a precise problem detection time.|
|*value*|<|<|number|1 (always).|
|*eventid*|<|<|number|Problem event ID.|
|*name*|<|<|string|Problem event name.|
|*severity*|<|<|number|Problem event severity (0 - Not classified, 1 - Information, 2 - Warning, 3 - Average, 4 - High, 5 - Disaster).|
|*hosts*|<|<|array|List of hosts involved in the trigger expression; there should be at least one element in array.|
|<|\-|<|object|<|
|<|<|*host*|string|Host name.|
|<|<|*name*|string|Visible host name.|
|*groups*|<|<|array|list of host groups of all hosts involved in the trigger expression; there should be at least one element in array.|
|<|\-|<|string|Host group name.|
|*tags*|<|<|array|List of problem tags (can be empty).|
|<|\-|<|object|<|
|<|<|*tag*|string|Tag name.|
|<|<|*value*|string|Tag value (can be empty).|

The following information is exported for a recovery event:

|Field|Type|Description|
|-----|----|-----------|
|*clock*|number|Number of seconds since Epoch to the moment when problem was resolved (integer part).|
|*ns*|number|Number of nanoseconds to be added to `clock` to get a precise problem resolution time.|
|*value*|number|0 (always).|
|*eventid*|number|Recovery event ID.|
|*p\_eventid*|number|Problem event ID.|

[comment]: # ({/new-bc637674})

[comment]: # ({new-9eccc413})
##### Examples

Problem:

    {"clock":1519304285,"ns":123456789,"value":1,"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","severity":3,"eventid":42, "hosts":[{"host":"Host B", "name":"Host B visible"},{"host":"Zabbix Server","name":"Zabbix Server visible"}],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}]}

Recovery:

    {"clock":1519304345,"ns":987654321,"value":0,"eventid":43,"p_eventid":42}

Problem (multiple problem event generation):

    {"clock":1519304286,"ns":123456789,"value":1,"eventid":43,"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","severity":3,"hosts":[{"host":"Host B", "name":"Host B visible"},{"host":"Zabbix Server","name":"Zabbix Server visible"}],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}]}

    {"clock":1519304286,"ns":123456789,"value":1,"eventid":43,"name":"Either Zabbix agent is unreachable on Host B or pollers are too busy on Zabbix Server","severity":3,"hosts":[{"host":"Host B", "name":"Host B visible"},{"host":"Zabbix Server","name":"Zabbix Server visible"}],"groups":["Group X","Group Y","Group Z","Zabbix servers"],"tags":[{"tag":"availability","value":""},{"tag":"data center","value":"Riga"}]}

Recovery:

    {"clock":1519304346,"ns":987654321,"value":0,"eventid":44,"p_eventid":43}

    {"clock":1519304346,"ns":987654321,"value":0,"eventid":44,"p_eventid":42}

[comment]: # ({/new-9eccc413})

[comment]: # ({new-9faee484})
#### Item values

The following information is exported for a collected item value:

|Field|<|Type|Description|
|-----|-|----|-----------|
|*host*|<|object|Host name of the item host.|
|<|host|string|Host name.|
|<|name|string|Visible host name.|
|*groups*|<|array|List of host groups of the item host; there should be at least one element in array.|
|<|\-|string|Host group name.|
|*itemid*|<|number|Item ID.|
|*name*|<|string|Visible item name.|
|*clock*|<|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*ns*|<|number|Number of nanoseconds to be added to `clock` to get a precise value collection time.|
|*timestamp*<br>(*Log* only)|<|number|0 if not available.|
|*source*<br>(*Log* only)|<|string|Empty string if not available.|
|*severity*<br>(*Log* only)|<|number|0 if not available.|
|*eventid*<br>(*Log* only)|<|number|0 if not available.|
|*value*|<|number (for numeric items) or<br>string (for text items)|Collected item value.|
|*type*|<|number|Collected value type:<br>0 - numeric float, 1 - character, 2 - log, 3 - numeric unsigned, 4 - text|

[comment]: # ({/new-9faee484})

[comment]: # ({new-683a131b})
##### Examples

Numeric (unsigned) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"itemid":3,"name":"Agent availability","clock":1519304285,"ns":123456789,"value":1,"type":3}

Numeric (float) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"itemid":4,"name":"CPU Load","clock":1519304285,"ns":123456789,"value":0.1,"type":0}

Character, text value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"itemid":2,"name":"Agent version","clock":1519304285,"ns":123456789,"value":"3.4.4","type":4}

Log value:

    {"host":{"host":"Host A","name":"Host A visible"},"groups":["Group X","Group Y","Group Z"],"itemid":1,"name":"Messages in log file","clock":1519304285,"ns":123456789,"timestamp":1519304285,"source":"","severity":0,"eventid":0,"value":"log file message","type":2}

[comment]: # ({/new-683a131b})

[comment]: # ({new-f534dda9})
#### Trends

The following information is exported for a calculated trend value:

|Field|<|Type|Description|
|-----|-|----|-----------|
|*host*|<|object|Host name of the item host.|
|<|host|string|Host name.|
|<|name|string|Visible host name.|
|*groups*|<|array|List of host groups of the item host; there should be at least one element in array.|
|<|\-|string|Host group name.|
|*itemid*|<|number|Item ID.|
|*name*|<|string|Visible item name.|
|*clock*|<|number|Number of seconds since Epoch to the moment when value was collected (integer part).|
|*count*|<|number|Number of values collected for a given hour.|
|*min*|<|number|Minimum item value for a given hour.|
|*avg*|<|number|Average item value for a given hour.|
|*max*|<|number|Maximum item value for a given hour.|
|*type*|<|number|Value type:<br>0 - numeric float, 3 - numeric unsigned|

[comment]: # ({/new-f534dda9})

[comment]: # ({new-8606813d})
##### Examples

Numeric (unsigned) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"itemid":3,"name":"Agent availability","clock":1519311600,"count":60,"min":1,"avg":1,"max":1,"type":3}

Numeric (float) value:

    {"host":{"host":"Host B","name":"Host B visible"},"groups":["Group X","Group Y","Group Z"],"itemid":4,"name":"CPU Load","clock":1519311600,"count":60,"min":0.01,"avg":0.15,"max":1.5,"type":0}

[comment]: # ({/new-8606813d})
