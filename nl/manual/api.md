[comment]: # translation:outdated

[comment]: # ({new-92f30379})
# 19. API

[comment]: # ({/new-92f30379})

[comment]: # ({fd4a0187-13405b49})
### Overzicht

Met de Zabbix API kunt u programmatisch het ophalen en wijzigen van de Zabbix 
configuratie en biedt toegang tot historische gegevens. Het is
veel gebruikt om:

- Nieuwe applicaties maken om met Zabbix te werken;
- Integreren van Zabbix met software van derden;
- Automatiseren van routinetaken.

De Zabbix API is een webgebaseerde API en wordt geleverd als onderdeel van het web
server. Het maakt gebruik van het JSON-RPC 2.0-protocol, wat twee dingen betekent:

- De API bestaat uit een reeks afzonderlijke methoden;
- Verzoeken en antwoorden tussen de clients en de API zijn gecodeerd
    met behulp van het JSON-formaat.

Meer info over het protocol en JSON is te vinden in de [JSON-RPC 2.0
specificatie](http://www.jsonrpc.org/specification) en de [JSON
format homepage](http://json.org/).

[comment]: # ({/fd4a0187-13405b49})

[comment]: # ({fe68c94a-9cb56d09})
### Structuur

De API bestaat uit een aantal methoden die nominaal zijn gegroepeerd in afzonderlijke API's. Elk van de methoden voert een specifieke taak uit. Voor
de methode `host.create` hoort bijvoorbeeld bij de *host* API en wordt gebruikt
om nieuwe hosts te maken. Historisch gezien worden API's soms aangeduid als:
"klassen".

::: notetip
De meeste API's bevatten ten minste vier methoden: 'get',
`create`, `update` en `delete` voor het ophalen, creëren, bijwerken en
respectievelijk het verwijderen van gegevens, maar sommige van de API's kunnen een volledig
verschillende set methoden ondersteunen.
:::

[comment]: # ({/fe68c94a-9cb56d09})

[comment]: # ({0532b170-9cb2f5ca})
### Verzoeken uitvoeren

Nadat u de frontend heeft ingesteld, kunt u externe HTTP-verzoeken gebruiken om de API aan te roepen. Om dat te doen, moet u HTTP POST-verzoeken naar het
`api_jsonrpc.php` bestand in de frontend directory. Bijvoorbeeld,
als uw Zabbix-frontend is geïnstalleerd onder *http://company.com/zabbix*,
ken het HTTP-verzoek om de `apiinfo.version`-methode aan te roepen er als volgt uitzien:

``` {.http}
POST http://company.com/zabbix/api_jsonrpc.php HTTP/1.1
Content-Type: application/json-rpc

{"jsonrpc":"2.0","method":"apiinfo.version","id":1,"auth":null,"params":{}}
```

Het verzoek moet de kop 'Content-Type' hebben ingesteld op een van deze
waarden: `application/json-rpc`, `application/json` of
`application/jsonrequest`.

::: notetip
U kunt elke HTTP-client of een JSON-RPC-test tool gebruiken
om API-verzoeken handmatig uit te voeren, maar voor het ontwikkelen van applicaties stellen we voor dat u een van de [door de gemeenschap onderhouden
bibliotheken gebruikt](http://zabbix.org/wiki/Docs/api/libraries).
:::

[comment]: # ({/0532b170-9cb2f5ca})

[comment]: # ({27fa849b-ec5a9f60})
### Voorbeeld workflow

In het volgende gedeelte wordt u door enkele gebruiksvoorbeelden geleid in meer
detail.

[comment]: # ({/27fa849b-ec5a9f60})

[comment]: # ({e4b49b96-ef3f5841})
#### Authenticatie

Voordat je toegang hebt tot gegevens in Zabbix, moet je inloggen
en verkrijg een authenticatie token. Dit kan met behulp van de
[user.login](/manual/api/reference/user/login) methode. Laten we veronderstellen
dat u als standaard Admin-gebruiker wilt inloggen. Uw JSON-verzoek
zal er dan als volgt uitzien:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "zabbix"
    },
    "id": 1,
    "auth": null
}
```

Laten we het aanvraag object eens nader bekijken. Het heeft de volgende eigenschappen:

- `jsonrpc` - de versie van het JSON-RPC-protocol dat door de API wordt gebruikt;
    de Zabbix API implementeert JSON-RPC versie 2.0;
- `method` - de API-methode die wordt aangeroepen;
- `params` - parameters die worden doorgegeven aan de API-methode;
- `id` - een willekeurige identifier van het verzoek;
- `auth` - een token voor gebruikers authenticatie; aangezien we er nog geen hebben,
    het is ingesteld op 'null'.

Als u de inloggegevens correct hebt opgegeven, bevat het antwoord van de
API call het token voor gebruikersverificatie:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "0424bd59b807674191e7d77572075f33",
    "id": 1
}
```

Het respons object bevat op zijn beurt de volgende eigenschappen:

- `jsonrpc` - nogmaals, de versie van het JSON-RPC-protocol;
- `result` - de gegevens die door de methode worden geretourneerd;
- `id` - identifier van het corresponderende verzoek.

[comment]: # ({/e4b49b96-ef3f5841})

[comment]: # ({new-37011a71})
#### Authorization methods

[comment]: # ({/new-37011a71})

[comment]: # ({new-130188f5})
##### By "Authorization" header

All API requests require an authentication or an API token.
You can provide the credentials by using the "Authorization" request header:

```bash
curl --request POST \
  --url 'https://company.com/zabbix/ui/api_jsonrpc.php' \
  --header 'Authorization: Bearer 0424bd59b807674191e7d77572075f33'
```

[comment]: # ({/new-130188f5})

[comment]: # ({new-38cc7fd6})
##### By "auth" property

An API request can be authorized by the "auth" property.

::: noteimportant
Note that the "auth" property is deprecated. It will be removed in the future releases.
:::

```bash
curl --request POST \
  --url 'https://company.com/zabbix/ui/api_jsonrpc.php' \
  --header 'Content-Type: application/json-rpc' \
  --data '{"jsonrpc":"2.0","method":"host.get","params":{"output":["hostid"]},"auth":"0424bd59b807674191e7d77572075f33","id":1}'
```

[comment]: # ({/new-38cc7fd6})

[comment]: # ({new-8950965e})
##### By Zabbix cookie

A *"zbx_session"* cookie is used to authorize an API request from Zabbix UI performed using JavaScript (from a module or
a custom widget).

[comment]: # ({/new-8950965e})

[comment]: # ({b3b07525-f572ecc2})
#### Hosts ophalen

We hebben nu een geldig token voor gebruikersverificatie die kan worden gebruikt om toegang te krijgen
de gegevens in Zabbix. Laten we bijvoorbeeld de [host.get](/manual/api/reference/host/get) methode gebruiken om de ID's, hostnamen en interfaces van alle geconfigureerde
[hosts](/manual/api/referentie/host/object) op te halen:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": [
            "hostid",
            "host"
        ],
        "selectInterfaces": [
            "interfaceid",
            "ip"
        ]
    },
    "id": 2,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

::: noteimportant Merk op dat de eigenschap `auth` nu is ingesteld op de
authenticatie token dat we hebben verkregen door de 
`user.login` methode.
:::

Het respons object zal de gevraagde gegevens over de hosts bevatten:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "hostid": "10084",
            "host": "Zabbix server",
            "interfaces": [
                {
                    "interfaceid": "1",
                    "ip": "127.0.0.1"
                }
            ]
        }
    ],
    "id": 2
}
```

::: notetip
Om prestatieredenen raden we aan om altijd om alleen objecteigenschappen die u wilt op te halen. Vermijd het ophalen
alles.
:::

[comment]: # ({/b3b07525-f572ecc2})

[comment]: # ({98fb3833-ee2c324f})
#### Een nieuw item maken

Laten we een nieuw [item](/manual/api/reference/item/object) maken op "Zabbix
server" met behulp van de gegevens die we hebben verkregen van het vorige `host.get`
verzoek. Dit kan met behulp van de
[item.create](/manual/api/reference/item/create) methode:

``` {.java}
{
    "jsonrpc": "2.0",
    "methode": "item.create",
    "params": {
        "name": "Vrij schijfruimte op /home/joe/",
        "key_": "vfs.fs.size[/home/joe/,free]",
        "hostid": "10084",
        "type": 0,
        "value_type": 3,
        "interfaceid": "1",
        "delay": 30
    },
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 3
}
```

Een succesvol antwoord bevat de ID van het nieuw gemaakte item,
die kan worden gebruikt om naar het item te verwijzen in de volgende verzoeken:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "24759"
        ]
    },
    "id": 3
}
```

::: notetip
De `item.create`-methode en andere aanmaakmethoden
kunnen ook arrays van objecten accepteren en meerdere items maken met één API
call.
:::

[comment]: # ({/98fb3833-ee2c324f})

[comment]: # ({75e401bd-5ed44978})
#### Meerdere triggers maken

Dus als maakmethoden arrays accepteren, kunnen we meerdere
[triggers](/manual/api/reference/trigger/object) als volgt toevoegen:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.create",
    "params": [
        {
            "description": "De processorbelasting is te hoog op {HOST.NAME}",
            "expression": "last(/Linux server/system.cpu.load[percpu,avg1])>5",
        },
        {
            "description": "Te veel processen op {HOST.NAME}",
            "expression": "avg(/Linux-server/proc.num[],5m)>300",
        }
    ],
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 4
}
```

Een succesvol antwoord bevat de ID's van de nieuw aangemaakte
triggers:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "17369",
            "17370"
        ]
    },
    "id": 4
}
```

[comment]: # ({/75e401bd-5ed44978})

[comment]: # ({e182f0b6-aa174634})
#### Een item bijwerken

Schakel een item in, dat wil zeggen, stel de status in op "0":

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
        "itemid": "10092",
        "status": 0
    },
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 5
}
```

Een succesvolle reactie bevat de ID van het bijgewerkte item:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "10092"
        ]
    },
    "id": 5
}
```

::: notetip
De `item.update`-methode en andere update-methoden
kunnen ook arrays van objecten accepteren en meerdere items bijwerken met één API
call.
:::

[comment]: # ({/e182f0b6-aa174634})

[comment]: # ({5543eae4-e217b4a2})
#### Meerdere triggers bijwerken

Schakel meerdere triggers in, dat wil zeggen, stel hun status in op 0:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.update",
    "params": [
        {
            "triggerid": "13938",
            "status": 0
        },
        {
            "triggerid": "13939",
            "status": 0
        }
    ],
    "auth": "0424bd59b807674191e7d77572075f33",
    "id": 6
}
```

Een succesvolle reactie bevat de ID's van de bijgewerkte triggers:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "13938",
            "13939"
        ]
    },
    "id": 6
}
```

::: notetip
Dit is de voorkeursmethode voor bijwerken. Sommige API
methoden zoals `host.massupdate` maken het mogelijk om meer eenvoudige code te schrijven, maar het is
niet aanbevolen om deze methoden te gebruiken, omdat ze worden verwijderd in de
toekomstige releases.
:::

[comment]: # ({/5543eae4-e217b4a2})

[comment]: # ({713b4c2a-7665e280})
#### Fout afhandeling

Tot nu toe heeft alles wat we hebben geprobeerd goed gewerkt. Maar wat
gebeurt er als we een onjuiste call naar de API proberen te doen? Laten we proberen om
maak een andere host door
[host.create](/manual/api/reference/host/create) aan te roepen maar de
verplichte parameter `groepen` weg te laten.

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "Linux server",
        "interfaces": [
            {
                "type 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.3.1",
                "dns": "",
                "port": "10050"
            }
        ]
    },
    "ID": 7,
    "auth": "0424bd59b807674191e7d77572075f33"
}
```

Het antwoord zal dan een foutmelding bevatten:

``` {.java}
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32602,
        "message": "Ongeldige parameters.",
        "data": "Geen groepen voor host \"Linux server\"."
    },
    "id": 7
}
```

Als er een fout is opgetreden, wordt in plaats van de  `result` eigenschap een `error` eigenschap in het antwoord meegegeven met de volgende gegevens:

- `code` - een foutcode;
- `bericht` - een korte samenvatting van de fouten;
- `data` - een meer gedetailleerde foutmelding.

Fouten kunnen in verschillende gevallen voorkomen, bijvoorbeeld bij het gebruik van onjuiste invoer
waarden, een sessie time-out of het proberen toegang te krijgen tot niet-bestaande objecten. Je
applicatie zou dit soort fouten netjes moeten kunnen afhandelen.

[comment]: # ({/713b4c2a-7665e280})

[comment]: # ({16fbc227-c1074d48})
### API-versies

Om API-versiebeheer te vereenvoudigen, komt sinds Zabbix 2.0.4, de versie van de API overeen met de versie van Zabbix zelf. U kunt de
[apiinfo.version](/manual/api/reference/apiinfo/version) methode gebruiken om de versie van de API waarmee u werkt te vinden. Dit kan handig zijn voor uw toepassing voor aanpassen van versie specifieke functies.

We garanderen achterwaartse compatibiliteit van functies binnen een hoofdversie.
Bij het maken van achterwaarts onverenigbare wijzigingen tussen grote releases,
laten we meestal de oude functies in de volgende release, en
verwijderen deze pas in de release daarna. Af en toe verwijderen we functies tussen grote releases zonder enige achteruit
compatibiliteit te garanderen. Het is belangrijk dat u nooit vertrouwt op verouderde
functies en migreer zo snel mogelijk naar nieuwere alternatieven.

::: notetip
U kunt alle wijzigingen in de API volgen in de
[API changelog](/manual/api/changes_3.4_-_4.0).
:::

[comment]: # ({/16fbc227-c1074d48})

[comment]: # ({b62f9088-dfd7315f})
### Verder lezen

Je weet nu genoeg om met de Zabbix API aan de slag te gaan, maar stop niet
hier. Voor meer informatie raden we u aan een kijkje te nemen in de [lijst van
beschikbare API's](/manual/api/referentie).

[comment]: # ({/b62f9088-dfd7315f})
