[comment]: # translation:outdated

[comment]: # ({new-bade5a47})
# 4 Zabbix overview

[comment]: # ({/new-bade5a47})

[comment]: # ({new-b4d6d531})
#### Architecture

Zabbix consists of several major software components. Their
responsibilities are outlined below.

[comment]: # ({/new-b4d6d531})

[comment]: # ({new-5957a1ba})
##### Server

[Zabbix server](/manual/concepts/server) is the central component to
which agents report availability and integrity information and
statistics. The server is the central repository in which all
configuration, statistical and operational data are stored.

[comment]: # ({/new-5957a1ba})

[comment]: # ({new-494fb840})
##### Database storage

All configuration information as well as the data gathered by Zabbix is
stored in a database.

[comment]: # ({/new-494fb840})

[comment]: # ({new-bd637ffa})
##### Web interface

For an easy access to Zabbix from anywhere and from any platform, the
web-based interface is provided. The interface is part of Zabbix server,
and usually (but not necessarily) runs on the same physical machine as
the one running the server.

[comment]: # ({/new-bd637ffa})

[comment]: # ({new-bfd730af})
##### Proxy

[Zabbix proxy](/manual/concepts/proxy) can collect performance and
availability data on behalf of Zabbix server. A proxy is an optional
part of Zabbix deployment; however, it may be very beneficial to
distribute the load of a single Zabbix server.

[comment]: # ({/new-bfd730af})

[comment]: # ({new-b9991630})
##### Agent

Zabbix agents are deployed on monitoring targets to actively monitor
local resources and applications and report the gathered data to Zabbix
server. Since Zabbix 4.4, there are two types of agents available: the
[Zabbix agent](/manual/concepts/agent) (lightweight, supported on many
platforms, written in C) and the [Zabbix agent
2](/manual/concepts/agent) (extra-flexible, easily extendable with
plugins, written in Go).

[comment]: # ({/new-b9991630})

[comment]: # ({new-9ec23332})
#### Data flow

In addition it is important to take a step back and have a look at the
overall data flow within Zabbix. In order to create an item that gathers
data you must first create a host. Moving to the other end of the Zabbix
spectrum you must first have an item to create a trigger. You must have
a trigger to create an action. Thus if you want to receive an alert that
your CPU load is too high on *Server X* you must first create a host
entry for *Server X* followed by an item for monitoring its CPU, then a
trigger which activates if the CPU is too high, followed by an action
which sends you an email. While that may seem like a lot of steps, with
the use of templating it really isn't. However, due to this design it is
possible to create a very flexible setup.

[comment]: # ({/new-9ec23332})
