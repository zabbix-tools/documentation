<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/web_interface/frontend_sections/users/authentication/ldap.md">
    <body>
      <trans-unit id="64e71448" xml:space="preserve">
        <source># 2 LDAP</source>
      </trans-unit>
      <trans-unit id="28a1e9c5" xml:space="preserve">
        <source>#### Overview

External LDAP [authentication](/manual/web_interface/frontend_sections/users/authentication) 
can be used to check user names and passwords. 

Zabbix LDAP authentication works at least with Microsoft Active
Directory and OpenLDAP.

If only LDAP sign-in is configured, then the user must also exist in Zabbix, however, 
its Zabbix password will not be used. If authentication is successful, then Zabbix will match 
a local username with the username attribute returned by LDAP.</source>
      </trans-unit>
      <trans-unit id="3654e16b" xml:space="preserve">
        <source>
#### User provisioning

It is possible to configure JIT (just-in-time) **user provisioning** for LDAP users. In this case, 
it is not required that a user already exists in Zabbix. The user account can be created when 
the user logs into Zabbix for the first time.

When an LDAP user enters their LDAP login and password, Zabbix checks the *default* LDAP server if this user exists. If the user exists and does not have an account in Zabbix yet, a new user is created in Zabbix and the user is able to log in.

:::noteimportant
If JIT provisioning is enabled, a user group for deprovisioned users must be specified in the *Authentication* tab.
:::

JIT provisioning also allows to update provisioned user accounts based on changes in LDAP.
For example, if a user is moved from one LDAP group to another, the user will also be moved from one group to another in Zabbix;
if a user is removed from an LDAP group, the user will also be removed from the group in Zabbix and, if not belonging to any other group, added to the user group for deprovisioned users.
Note that provisioned user accounts are updated based on the configured [provisioning period](#configuration) or when the user logs into Zabbix.

LDAP JIT provisioning is available only when LDAP is configured to use "anonymous" or "special user" for binding. For direct user binding, provisioning will be made only for user login action, because logging in user password is used for such type of binding.</source>
      </trans-unit>
      <trans-unit id="c7234b3e" xml:space="preserve">
        <source>
#### Multiple servers

Several LDAP servers can be defined, if necessary. For example, a different 
server can be used to authenticate a different user group. Once LDAP servers 
are configured, in [user group](/manual/config/users_and_usergroups/usergroup#configuration) 
configuration it becomes possible to select the required LDAP server for the 
respective user group.

If a user is in multiple user groups and multiple LDAP servers, the first server in the 
list of LDAP servers sorted by name in ascending order will be used for authentication.</source>
      </trans-unit>
      <trans-unit id="e085db50" xml:space="preserve">
        <source>
#### Configuration

![](../../../../../../assets/en/manual/web_interface/frontend_sections/users/auth_ldap.png)

Configuration parameters:

|Parameter|Description|
|--|--------|
|*Enable LDAP authentication*|Mark the checkbox to enable LDAP authentication.|
|*Enable JIT provisioning*|Mark the checkbox to enable JIT provisioning.|
|*Servers*|Click on *Add* to configure an LDAP server (see [LDAP server configuration](#ldap-server-configuration) below).|
|*Case-sensitive login*|Unmark the checkbox to disable case-sensitive login (enabled by default) for usernames.&lt;br&gt;E.g. disable case-sensitive login and log in with, for example, 'ADMIN' user even if the Zabbix user is 'Admin'.&lt;br&gt;*Note* that with case-sensitive login disabled the login will be denied if multiple users exist in Zabbix database with similar usernames (e.g. Admin, admin).|
|*Provisioning period*|Set the provisioning period, i.e. how often user provisioning is performed.|</source>
      </trans-unit>
      <trans-unit id="9c2f07ee" xml:space="preserve">
        <source>
#### LDAP server configuration

![](../../../../../../assets/en/manual/web_interface/frontend_sections/users/auth_ldap_server.png)

LDAP server configuration parameters:

|Parameter|Description|
|--|--------|
|*Name*|Name of the LDAP server in Zabbix configuration.|
|*Host*|Host of the LDAP server. For example: ldap://ldap.example.com&lt;br&gt;For secure LDAP server use *ldaps* protocol.&lt;br&gt;ldaps://ldap.example.com&lt;br&gt;With OpenLDAP 2.x.x and later, a full LDAP URI of the form ldap://hostname:port or ldaps://hostname:port may be used.|
|*Port*|Port of the LDAP server. Default is 389.&lt;br&gt;For secure LDAP connection port number is normally 636.&lt;br&gt;Not used when using full LDAP URIs.|
|*Base DN*|Base path to user accounts in LDAP server:&lt;br&gt;ou=Users,ou=system (for OpenLDAP),&lt;br&gt;DC=company,DC=com (for Microsoft Active Directory)&lt;br&gt; uid=%{user},dc=example,dc=com (for direct user binding, see a note below)|
|*Search attribute*|LDAP account attribute used for search:&lt;br&gt;uid (for OpenLDAP),&lt;br&gt;sAMAccountName (for Microsoft Active Directory)|
|*Bind DN*|LDAP account for binding and searching over the LDAP server, examples:&lt;br&gt;uid=ldap\_search,ou=system (for OpenLDAP),&lt;br&gt;CN=ldap\_search,OU=user\_group,DC=company,DC=com (for Microsoft Active Directory)&lt;br&gt;Anonymous binding is also supported. Note that anonymous binding potentially opens up domain configuration to unauthorized users (information about users, computers, servers, groups, services, etc.). For security reasons, disable anonymous binds on LDAP hosts and use authenticated access instead.|
|*Bind password*|LDAP password of the account for binding and searching over the LDAP server.|
|*Description*|Description of the LDAP server.|
|*Configure JIT provisioning*|Mark this checkbox to show options related to JIT provisioning.|
|*Group configuration*|Select the group configuration method:&lt;br&gt;**memberOf** - by searching users and their group membership attribute&lt;br&gt;**groupOfNames** - by searching groups through the member attribute&lt;br&gt;Note that memberOf is preferable as it is faster; use groupOfNames if your LDAP server does not support `memberOf` or group filtering is required.|
|*Group name attribute*|Specify the attribute to get the group name from all objects in the `memberOf` attribute (see the *User group membership attribute* field)&lt;br&gt;The group name is necessary for user group mapping.|
|*User group membership attribute*|Specify the attribute that contains information about the groups that the user belongs to (e.g. `memberOf`).&lt;br&gt;For example, the memberOf attribute may hold information like this: `memberOf=cn=zabbix-admin,ou=Groups,dc=example,dc=com`&lt;br&gt;This field is available only for the memberOf method.|
|*User name attribute*|Specify the attribute that contains the user's first name.|
|*User last name attribute*|Specify the attribute that contains the user's last name.|
|*User group mapping*|Map an LDAP user group pattern to Zabbix user group and user role.&lt;br&gt;This is required to determine what user group/role the provisioned user will get in Zabbix.&lt;br&gt;Click on *Add* to add a mapping.&lt;br&gt;The *LDAP group pattern* field supports wildcards. The group name must match an existing group.&lt;br&gt;If an LDAP user matches several Zabbix user groups, the user becomes a member of all of them.&lt;br&gt;If a user matches several Zabbix user roles, the user will get the one with the highest permission level among them.|
|*Media type mapping*|Map the user's LDAP media attributes (e.g. email) to Zabbix user media for sending notifications.|
|*Advanced configuration*|Click on the *Advanced configuration* label to display advanced configuration options (see below).|
|*StartTLS*|Mark the checkbox to use the StartTLS operation when connecting to LDAP server. The connection will fall if the server doesn't support StartTLS.&lt;br&gt;StartTLS cannot be used with servers that use the *ldaps* protocol.|
|*Search filter*|Define a custom string when authenticating user in LDAP. The following placeholders are supported:&lt;br&gt;`%{attr}` - search attribute name (uid, sAMAccountName)&lt;br&gt;`%{user}` - user username value to authenticate.&lt;br&gt;If omitted then LDAP will use the default filter: `(%{attr}=%{user})`.|

::: noteclassic
To configure an LDAP server for **direct user binding**, append an attribute uid=%{user} to the *Base DN* parameter (for example,*uid=%{user},dc=example,dc=com*) and leave *BindDN* and *Bind password* parameters empty. When authenticating, a placeholder %{user} will be replaced by the username entered during login.
:::

The following fields are specific to "groupOfNames" as the *Group configuration* method:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/users/auth_ldap_server2.png)

|Parameter|Description|
|--|--------|
|*Group base DN*|Base path to the groups in LDAP server.|
|*Group name attribute*|Specify the attribute to get the group name in the specified base path to groups.&lt;br&gt;The group name is necessary for user group mapping.|
|*Group member attribute*|Specify the attribute that contains information about the members of the group in LDAP (e.g. `member`).|
|*Reference attribute*|Specify the reference attribute for the group filter (see the *Group filter* field).&lt;br&gt;Then use `%{ref}` in the group filter to get values for the attribute specified here.|
|*Group filter*|Specify the filter to retrieve the group that the user is member of.&lt;br&gt;For example, `(member=uid=%{ref},ou=Users,dc=example,dc=com)` will match "User1" if the member attribute of the group is `uid=User1,ou=Users,dc=example,dc=com` and will return the group that "User1" is a member of.|

::: notewarning
In case of trouble with certificates, to make a secure LDAP connection (ldaps) work 
you may need to add a `TLS_REQCERT allow` line to the /etc/openldap/ldap.conf configuration
file. It may decrease the security of connection to the LDAP catalog.
:::

::: notetip
It is recommended to create a separate LDAP account (*Bind DN*) to perform binding and 
searching over the LDAP server with minimal privileges in the LDAP instead of using real 
user accounts (used for logging in the Zabbix frontend).\
Such an approach provides more security and does not require changing the *Bind password* 
when the user changes his own password in the LDAP server.\
In the table above it's the *ldap\_search* account name.
:::</source>
      </trans-unit>
      <trans-unit id="5c3e92b7" xml:space="preserve">
        <source>
##### Testing access

The *Test* button allows to test user access:

|Parameter|Description|
|--|--------|
|*Login*|LDAP user name to test (prefilled with the current user name from Zabbix frontend). This user name must exist in the LDAP server.&lt;br&gt;Zabbix will not activate LDAP authentication if it is unable to authenticate the test user.|
|*User password*|LDAP user password to test.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
