[comment]: # translation:outdated

[comment]: # ({new-c6632d7f})
# 6 Data collection

[comment]: # ({/new-c6632d7f})

[comment]: # ({new-917a67b4})
#### Overview

This menu features sections that are related to configuring 
data collection.

[comment]: # ({/new-917a67b4})
