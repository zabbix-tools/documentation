<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/web_interface/frontend_sections/monitoring/hosts.md">
    <body>
      <trans-unit id="ae6d2ebd" xml:space="preserve">
        <source># 2 Hosts</source>
      </trans-unit>
      <trans-unit id="26f6fa20" xml:space="preserve">
        <source>#### Overview

The *Monitoring → Hosts* section displays a full list of monitored hosts
with detailed information about host interface, availability, tags,
current problems, status (enabled/disabled), and links to easily
navigate to the host's latest data, problem history, graphs, dashboards
and web scenarios.

![](../../../../../assets/en/manual/web_interface/monitoring_hosts.png){width="600"}

|Column|Description|
|--|--------|
|*Name*|The visible host name. Clicking on the name brings up the [host menu](/manual/web_interface/menu/host_menu).&lt;br&gt;An orange wrench icon ![](../../../../../assets/en/manual/web_interface/frontend_sections/configuration/maintenance_wrench_icon.png) after the name indicates that this host is in maintenance.&lt;br&gt;Click on the column header to sort hosts by name in ascending or descending order.|
|*Interface*|The main interface of the host is displayed.|
|*Availability*|Host [availability](/manual/web_interface/frontend_sections/data_collection/hosts#reading_host_availability) per configured interface is displayed.&lt;br&gt;Icons represent only those interface types (Zabbix agent, SNMP, IPMI, JMX) that are configured. If you position the mouse on the icon, a popup list appears listing all interfaces of this type with details, status and errors (for the agent interface, availability of active checks is also listed).&lt;br&gt;The column is empty for hosts with no interfaces.&lt;br&gt;The current status of all interfaces of one type is displayed by the respective icon color:&lt;br&gt;**Green** - all interfaces available&lt;br&gt;**Yellow** - at least one interface available and at least one unavailable; others can have any value including 'unknown'&lt;br&gt;**Red** - no interfaces available&lt;br&gt;**Gray** - at least one interface unknown (none unavailable)&lt;br&gt;&lt;br&gt;**Active check availability**&lt;br&gt;Since Zabbix 6.2 active checks also affect host availability, if there is at least one enabled active check on the host. To determine active check availability heartbeat messages are sent in the agent active check thread. The frequency of the heartbeat messages is set by the `HeartbeatFrequency` parameter in Zabbix [agent](/manual/appendix/config/zabbix_agentd) and [agent 2](/manual/appendix/config/zabbix_agent2) configurations (60 seconds by default, 0-3600 range). Active checks are considered unavailable when the active check heartbeat is older than 2 x HeartbeatFrequency seconds.&lt;br&gt;*Note* that if Zabbix agents older than 6.2.x are used, they are not sending any active check heartbeats, so the availability of their hosts will remain unknown.&lt;br&gt;Active agent availability is counted towards the total Zabbix agent availability in the same way as a passive interface is (for example, if a passive interface is available, while the active checks are unknown, the total agent availability is set to gray(unknown)).|
|*Tags*|[Tags](/manual/config/tagging) of the host and all linked templates, with macros unresolved.|
|*Status*|Host status - *Enabled* or *Disabled*.&lt;br&gt;Click on the column header to sort hosts by status in ascending or descending order.|
|*Latest data*|Clicking on the link will open the *Monitoring - Latest data* page with all the latest data collected from the host.&lt;br&gt; The number of items with latest data is displayed in gray.|
|*Problems*| The number of open host problems sorted by severity. The color of the square indicates problem severity. The number on the square means the number of problems for the given severity.&lt;br&gt;Clicking on the icon will open *Monitoring - Problems* page for the current host. &lt;br&gt;If a host has no problems, a link to the Problems section for this host is displayed as text. &lt;br&gt;Use the filter to select whether suppressed problems should be included (not included by default).|
|*Graphs*|Clicking on the link will display graphs configured for the host. The number of graphs is displayed in gray.&lt;br&gt;If a host has no graphs, the link is disabled (gray text) and no number is displayed.|
|*Dashboards*|Clicking on the link will display dashboards configured for the host. The number of dashboards is displayed in gray.&lt;br&gt;If a host has no dashboards, the link is disabled (gray text) and no number is displayed.|
|*Web*|Clicking on the link will display web scenarios configured for the host. The number of web scenarios is displayed in gray.&lt;br&gt;If a host has no web scenarios, the link is disabled (gray text) and no number is displayed.|</source>
      </trans-unit>
      <trans-unit id="bd285dea" xml:space="preserve">
        <source>##### Buttons

*Create host* allows to create a [new host](/manual/config/hosts/host).
This button is available for Admin and Super Admin users only.

View mode buttons being common for all sections are described on the
[Monitoring](/manual/web_interface/frontend_sections/monitoring#view_mode_buttons)
page.</source>
      </trans-unit>
      <trans-unit id="159cadac" xml:space="preserve">
        <source>#### Using filter

You can use the filter to display only the hosts you are interested in.
For better search performance, data is searched with macros unresolved.

The filter is located above the table. It is possible to filter hosts by
name, host group, IP or DNS, interface port, tags, problem severity,
status (enabled/disabled/any); you can also select whether to display
suppressed problems and hosts that are currently in maintenance.

![](../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/monitoring_hosts_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Name*|Filter by visible host name.|
|*Host groups*|Filter by one or more host groups.&lt;br&gt;Specifying a parent host group implicitly selects all nested host groups.|
|*IP*|Filter by IP address.|
|*DNS*|Filter by DNS name.|
|*Port*|Filter by port number.|
|*Severity*|Filter by problem severity. By default problems of all severities are displayed. Problems are displayed if not suppressed.|
|*Status*|Filter by host status.|
|*Tags*|Filter by host tag name and value. Hosts can be filtered by host-level tags as well as tags from all linked templates, including nested templates.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive)&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;**Does not exist** - exclude the specified tag names&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive)&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition&lt;br&gt;**Or** - enough if one condition is met|
|*Show hosts in maintenance*|Mark the checkbox to display hosts that are in maintenance (displayed by default).|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance or single [problem suppression](/manual/acknowledgment/suppression).|

##### Saving filter

Favorite filter settings can be saved as tabs and then quickly accessed by 
clicking on the respective tab above the filter.

See more details about [saving filters](/manual/web_interface/frontend_sections/monitoring/problems#tabs_for_favorite_filters).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
