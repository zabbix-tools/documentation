<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="nl" datatype="plaintext" original="manual/web_interface/frontend_sections/alerts/actions.md">
    <body>
      <trans-unit id="1b8c35fe" xml:space="preserve">
        <source># 1 Actions</source>
      </trans-unit>
      <trans-unit id="a3e9781e" xml:space="preserve">
        <source>#### Overview

In the *Alerts → Actions* section users can configure and
maintain actions.

The actions displayed are actions assigned to the selected event source
(trigger, services, discovery, autoregistration, internal actions).

Actions are grouped into subsections by event source (trigger, service,
discovery, autoregistration, internal actions). The list of available
subsections appears upon clicking on *Actions* in the *Alerts*
menu section. It is also possible to switch between subsections by using
the title dropdown in the top left corner.

After selecting a subsection, a list of existing actions
with their details will be displayed.

![](../../../../../assets/en/manual/web_interface/actions.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the action. Clicking on the action name opens the action [configuration form](/manual/config/notifications/action#configuring_an_action).|
|*Conditions*|Action conditions are displayed.|
|*[Operations](/manual/config/notifications/action/operation#configuring_an_operation)*|Action operations are displayed.&lt;br&gt;Since Zabbix 2.2, the operation list also displays the media type (email, SMS or script) used for notification as well as the name and surname (in parentheses after the username) of a notification recipient.&lt;br&gt;Action operation can both be a [notification](/manual/config/notifications/action/operation/message) or a [remote command](/manual/config/notifications/action/operation/remote_command) depending on the selected type of operation.|
|*Status*|Action status is displayed - *Enabled* or *Disabled*.&lt;br&gt;By clicking on the status you can change it.&lt;br&gt;See the [Escalations](/manual/config/notifications/action/escalations) section for more details as to what happens if an action is disabled during an escalation in progress.|

To configure a new action, click on the *Create action* button in the
top right-hand corner.

For users without Super admin rights actions are displayed according to
the permission settings. That means in some cases a user without Super admin
rights isn’t able to view the complete action list because of certain
permission restrictions. An action is displayed to the user without
Super admin rights if the following conditions are fulfilled:

-   The user has read-write access to host groups, hosts, templates, and
    triggers in action conditions
-   The user has read-write access to host groups, hosts, and templates
    in action operations, recovery operations, and update operations
-   The user has read access to user groups and users in action
    operations, recovery operations, and update operations</source>
      </trans-unit>
      <trans-unit id="c252a236" xml:space="preserve">
        <source>##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Enable* - change the action status to *Enabled*
-   *Disable* - change the action status to *Disabled*
-   *Delete* - delete the actions

To use these options, mark the checkboxes before the respective actions,
then click on the required button.</source>
      </trans-unit>
      <trans-unit id="1e414677" xml:space="preserve">
        <source>##### Using filter

You can use the filter to display only the actions you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* link is available above the list of actions. If you click
on it, a filter becomes available where you can filter actions by name
and status.

![](../../../../../assets/en/manual/web_interface/action_filter.png){width="600"}</source>
      </trans-unit>
    </body>
  </file>
</xliff>
