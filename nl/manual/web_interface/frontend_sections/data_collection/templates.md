[comment]: # translation:outdated

[comment]: # ({new-e8fbaf7c})
# 3 Templates

[comment]: # ({/new-e8fbaf7c})

[comment]: # ({new-2fff982e})
#### Overview

In the *Data collection* → *Templates* section users can configure and
maintain templates.

A listing of existing templates with their details is displayed.

![](../../../../../assets/en/manual/web_interface/templates.png){width="600"}

Displayed data:

|Column|Description|
|--|--------|
|*Name*|Name of the template. Clicking on the template name opens the template [configuration form](/manual/config/templates/template#creating_a_template).|
|*Hosts*|Number of editable hosts to which the template is linked; read-only hosts are not included.<br>Clicking on *Hosts* will open the host list with only those hosts filtered that are linked to the template.|
|*Entities (Items, Triggers, Graphs, Dashboards, Discovery, Web)*|Number of the respective entities in the template (displayed in gray). Clicking on the entity name will, in the whole listing of that entity, filter out those that belong to the template.|
|*Linked templates*|Templates that are linked to the template, in a nested setup where the template will inherit all entities of the linked templates.|
|*Linked to templates*|The templates that the template is linked to ("children" templates that inherit all entities from this template).<br>Since Zabbix 5.0.3, this column no longer includes hosts.|
|*Tags*|[Tags](/manual/config/tagging) of the template, with macros unresolved.|

To configure a new template, click on the *Create template* button in
the top right-hand corner. To import a template from a YAML, XML, or
JSON file, click on the *Import* button in the top right-hand corner.

[comment]: # ({/new-2fff982e})

[comment]: # ({new-f09409e4})
##### Using filter

You can use the filter to display only the templates you are interested
in. For better search performance, data is searched with macros
unresolved.

The *Filter* link is available below *Create template* and *Import*
buttons. If you click on it, a filter becomes available where you can
filter templates by template group, linked templates, name and tags.

![](../../../../../assets/en/manual/web_interface/template_filter.png){width="600"}

|Parameter|Description|
|--|--------|
|*Template groups*|Filter by one or more template groups.<br>Specifying a parent template group implicitly selects all nested groups.|
|*Linked templates*|Filter by directly linked templates.|
|*Name*|Filter by template name.|
|*Tags*|Filter by template tag name and value.<br>Filtering is possible only by template-level tags (not inherited ones). It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.<br>There are several operators available for each condition:<br>**Exists** - include the specified tag names<br>**Equals** - include the specified tag names and values (case-sensitive)<br>**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>**Does not exist** - exclude the specified tag names<br>**Does not equal** - exclude the specified tag names and values (case-sensitive)<br>**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive)<br>There are two calculation types for conditions:<br>**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the Or condition<br>**Or** - enough if one condition is met|

[comment]: # ({/new-f09409e4})

[comment]: # ({new-1ff75389})
##### Mass editing options

Buttons below the list offer some mass-editing options:

-   *Export* - export the template to a YAML, XML or JSON file
-   *Mass update* - [update several
    properties](/manual/config/templates/mass) for a number of templates
    at once
-   *Delete* - delete the template while leaving its linked entities
    (items, triggers etc.) with the hosts
-   *Delete and clear* - delete the template and its linked entities
    from the hosts

To use these options, mark the checkboxes before the respective
templates, then click on the required button.

[comment]: # ({/new-1ff75389})
