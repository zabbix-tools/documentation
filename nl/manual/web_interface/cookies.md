[comment]: # translation:outdated

[comment]: # ({new-faf2deb0})
# 10 Cookies used by Zabbix

[comment]: # ({/new-faf2deb0})

[comment]: # ({new-50a6addc})
#### Overview

This page provides a list of cookies used by Zabbix.

|Name|Description|Values|Expires/Max-Age|HttpOnly[^1]|Secure[^2]|
|----|-----------|------|---------------|------------|----------|
|ZBX\_SESSION\_NAME|Zabbix frontend session data, stored as JSON encoded by base64|<|Session (expires when the browsing session ends)|\+|\+ (only if HTTPS is enabled on a web server)|
|tab|Active tab number; this cookie is only used on pages with multiple tabs (e.g. *Host*, *Trigger* or *Action* configuration page) and is created, when a user navigates from a primary tab to another tab (such as *Tags* or *Dependencies* tab).<br><br>0 is used for the primary tab.|Example: 1|Session (expires when the browsing session ends)|\-|\-|
|browserwarning\_ignore|Whether a warning about using an outdated browser should be ignored.|yes|Session (expires when the browsing session ends)|\-|\-|
|system-message-ok|A message to show as soon as page is reloaded.|Plain text message|Session (expires when the browsing session ends) or as soon as page is reloaded|\+|\-|
|system-message-error|An error message to show as soon as page is reloaded.|Plain text message|Session (expires when the browsing session ends) or as soon as page is reloaded|\+|\-|

::: noteclassic
 Forcing 'HttpOnly' flag on Zabbix cookies by a webserver
directive is not supported. 
:::

[^1]: When `HttpOnly` is 'true' the cookie will be made accessible only
    through the HTTP protocol. This means that the cookie won't be
    accessible by scripting languages, such as JavaScript. This setting
    can effectively help to reduce identity theft through XSS attacks
    (although it is not supported by all browsers).

[^2]: `Secure` indicates that the cookie should only be transmitted over
    a secure HTTPS connection from the client. When set to 'true', the
    cookie will only be set if a secure connection exists.

[comment]: # ({/new-50a6addc})
