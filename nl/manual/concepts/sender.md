[comment]: # translation:outdated

[comment]: # ({new-43ef458c})
# 6 Sender

[comment]: # ({/new-43ef458c})

[comment]: # ({new-836aa47c})
#### Overview

Zabbix sender is a command line utility that may be used to send
performance data to Zabbix server for processing.

The utility is usually used in long running user scripts for periodical
sending of availability and performance data.

For sending results directly to Zabbix server or proxy, a [trapper
item](/manual/config/items/itemtypes/trapper) type must be configured.

[comment]: # ({/new-836aa47c})

[comment]: # ({new-fce06a66})
#### Running Zabbix sender

An example of running Zabbix UNIX sender:

    shell> cd bin
    shell> ./zabbix_sender -z zabbix -s "Linux DB3" -k db.connections -o 43

where:

-   z - Zabbix server host (IP address can be used as well)
-   s - technical name of monitored host (as registered in Zabbix
    frontend)
-   k - item key
-   o - value to send

::: noteimportant
Options that contain whitespaces, must be quoted
using double quotes.
:::

Zabbix sender can be used to send multiple values from an input file.
See the [Zabbix sender manpage](/manpages/zabbix_sender) for more
information.

If a configuration file is specified, Zabbix sender uses all addresses
defined in the agent ServerActive configuration parameter for sending
data. If sending to one address fails, the sender tries sending to the
other addresses. If sending of batch data fails to one address, the
following batches are not sent to this address.

Zabbix sender accepts strings in UTF-8 encoding (for both UNIX-like
systems and Windows) without byte order mark (BOM) first in the file.

Zabbix sender on Windows can be run similarly:

    zabbix_sender.exe [options]

Since Zabbix 1.8.4, zabbix\_sender realtime sending scenarios have been
improved to gather multiple values passed to it in close succession and
send them to the server in a single connection. A value that is not
further apart from the previous value than 0.2 seconds can be put in the
same stack, but maximum pooling time still is 1 second.

::: noteclassic
Zabbix sender will terminate if invalid (not following
*parameter=value* notation) parameter entry is present in the specified
configuration file.
:::

[comment]: # ({/new-fce06a66})
