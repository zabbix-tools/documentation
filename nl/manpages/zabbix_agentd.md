[comment]: # translation:outdated

[comment]: # ({6c2284d1-2c290a27})
# zabbix\_agentd

Sectie: Onderhoudsopdrachten (8)\
Bijgewerkt: 2019-01-29\
[Index](#index) [Terug naar hoofdinhoud](/documentation/5.0/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/6c2284d1-2c290a27})

[comment]: # ({7aaf9764-75de926e})
## NAAM

zabbix\_agentd - Zabbix agent-daemon [ ]{#lbAC}

[comment]: # ({/7aaf9764-75de926e})

[comment]: # ({d05d2812-b8c27067})
## KORTE INHOUD

**zabbix\_agentd** \[**-c** *configuratiebestand*\]\
**zabbix\_agentd** \[**-c** *config-file*\] **-p**\
**zabbix\_agentd** \[**-c** *config-file*\] **-t** *item-key*\
**zabbix\_agentd** \[**-c** *configuratiebestand*\] **-R** *runtime-optie*\
**zabbix\_agentd -h**\
**zabbix\_agentd -V** [ ]{#lbAD}

[comment]: # ({/d05d2812-b8c27067})

[comment]: # ({8aa7b864-ecdda52c})
## BESCHRIJVING

**zabbix\_agentd** is een daemon voor het bewaken van verschillende serverparameters.
[ ]{#lbAE}

[comment]: # ({/8aa7b864-ecdda52c})

[comment]: # ({889cbfe8-63567f56})
## OPTIES

**-c**, **--config** *config-bestand*
Gebruik het alternatieve *config-bestand* in plaats van het standaardbestand.

**-f**, **--voorgrond**
Zet Zabbix-agent op de voorgrond.

**-R**, **--runtime-control** *runtime-optie*
Voer administratieve functies uit volgens *runtime-optie*.

[ ]{#lbAF}

[comment]: # ({/889cbfe8-63567f56})

[comment]: # ({d5f44c25-5333e0f4})
###

  
Runtime-besturingsopties

  
**gebruikersparameter\_reload**\[=*doel*\]
Herlaad gebruikersparameters uit het configuratiebestand

  
**log\_level\_increase**\[=*target*\]
Logboekniveau verhogen, heeft invloed op alle processen als het doel niet is opgegeven

  
**log\_level\_decrease**\[=*target*\]
Logboekniveau verlagen, heeft invloed op alle processen als het doel niet is opgegeven

[ ]{#lbAG}

[comment]: # ({/d5f44c25-5333e0f4})

[comment]: # ({1697efdc-199def5e})
###

  
Controledoelen op logniveau

  
*procestype*
Alle processen van het opgegeven type (actieve controles, verzamelaar, luisteraar)

  
*procestype,N*
Procestype en -nummer (bijv. luisteraar, 3)

  
*pid*
Proces-ID, tot 65535. Voor grotere waarden specificeer doel als
"procestype,N"

```{=html}
<!-- -->
```
**-p**, **--afdruk**
Druk bekende items af en sluit af. Voor elk item zijn ofwel generieke standaardwaarden:
gebruikt, of specifieke standaardwaarden voor testen worden geleverd. Deze standaardinstellingen zijn:
vermeld tussen vierkante haken als itemsleutelparameters. Geretourneerde waarden zijn
tussen vierkante haken en voorafgegaan door het type van de geretourneerde
waarde, gescheiden door een pijpteken. Voor gebruikersparameters is het type altijd
**t**, omdat de agent niet alle mogelijke retourwaarden kan bepalen. Artikelen,
weergegeven als werkend, werken niet gegarandeerd vanaf de Zabbix-server
of zabbix\_get bij het opvragen van een actieve agent-daemon als machtigingen of
omgeving kan anders zijn. Typen met geretourneerde waarden zijn:

  
D
Getal met een decimaal deel.

  
m
Niet ondersteund. Dit kan worden veroorzaakt door het opvragen van een item dat alleen werkt
in de actieve modus, zoals een logboekbewakingsitem of een item waarvoor:
meerdere verzamelde waarden. Toestemmingsproblemen of onjuiste gebruiker
parameters kunnen ook resulteren in de niet-ondersteunde status.

  
s
Tekst. Maximale lengte niet beperkt.

  
t
Tekst. Hetzelfde als **s**.

  
jij
Niet-ondertekend geheel getal.

**-t**, **--test** *item-sleutel*
Test een enkel item en sluit af. Zie **--print** voor uitvoerbeschrijving.

**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAH}

[comment]: # ({/1697efdc-199def5e})

[comment]: # ({0bc964ad-fcfd4796})
## BESTANDEN

*/usr/local/etc/zabbix\_agentd.conf*
Standaardlocatie van Zabbix-agent configuratie bestand (indien niet gewijzigd
tijdens het compileren).

[ ]{#lbAI}

[comment]: # ({/0bc964ad-fcfd4796})

[comment]: # ({1239c374-539868a7})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAJ}

[comment]: # ({/1239c374-539868a7})

[comment]: # ({e0f4ad0c-e55ed07e})
## AUTEUR

Alexei Vladishev <<alex@zabbix.com>>

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/e0f4ad0c-e55ed07e})

[comment]: # ({0ac3f305-6ea4e50b})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[](#lbAF)

[](#lbAG)

  

[BESTANDEN](#lbAH)

[ZIE OOK](#lbAI)

[AUTEUR](#lbAJ)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/documentation/5.0/manpages),
met behulp van de handleidingen.\
Tijd: 20:50:13 GMT, 22 november 2021

[comment]: # ({/0ac3f305-6ea4e50b})
