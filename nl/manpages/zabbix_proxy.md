[comment]: # translation:outdated

[comment]: # ({7e069eea-e0cabf53})
# zabbix\_proxy

Sectie: Onderhoudsopdrachten (8)\
Bijgewerkt: 2020-09-04\
[Index](#index) [Terug naar hoofdinhoud](/documentation/5.2/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/7e069eea-e0cabf53})

[comment]: # ({df8c2649-c7ba44c5})
## NAAM

zabbix\_proxy - Zabbix proxy-daemon [ ]{#lbAC}

[comment]: # ({/df8c2649-c7ba44c5})

[comment]: # ({new-6590818d})
## SYNOPSIS

**zabbix\_proxy** \[**-c** *config-file*\]\
**zabbix\_proxy** \[**-c** *config-file*\] **-R** *runtime-option*\
**zabbix\_proxy -h**\
**zabbix\_proxy -V** [ ]{#lbAD}

[comment]: # ({/new-6590818d})

[comment]: # ({e17bace3-5c6b9587})
## BESCHRIJVING

**zabbix\_proxy** is een daemon die monitoring gegevens van apparaten verzamelt
en stuurt het naar de Zabbix-server. [ ]{#lbAE}

[comment]: # ({/e17bace3-5c6b9587})

[comment]: # ({1bcb27d8-daee5c55})
## OPTIES

**-c**, **--config** *config-bestand*
Gebruik het alternatieve *config-bestand* in plaats van het standaardbestand.

**-f**, **--voorgrond**
Voer Zabbix-proxy op de voorgrond uit.

**-R**, **--runtime-control** *runtime-optie*
Voer administratieve functies uit volgens *runtime-optie*.

[ ]{#lbAF}

[comment]: # ({/1bcb27d8-daee5c55})

[comment]: # ({cac58159-045894c6})
###

  
Runtime-besturingsopties

  
**config\_cache\_reload**
Laad de configuratie cache opnieuw. Genegeerd als de cache momenteel wordt geladen.
Actieve Zabbix-proxy maakt verbinding met de Zabbix-server en vraagt
configuratie gegevens. Standaard configuratiebestand (tenzij de optie **-c** is
gespecificeerd) wordt gebruikt om het PID-bestand te vinden en het signaal wordt verzonden naar
proces, vermeld in PID-bestand.

  
**snmp\_cache\_reload**
Laad de SNMP-cache opnieuw.

  
**huishoudster\_execute**
Executeer de huishoudster. Genegeerd als huishoudster momenteel wordt
uitgevoerd.

  
**diaginfo**\[=*sectie*\]
Log interne diagnostische informatie van de opgegeven sectie. Sectie
kan *history cache*, *preprocessing* zijn. Standaard diagnose
informatie van alle secties wordt gelogd.

  
**log\_level\_increase**\[=*target*\]
Logboek niveau verhogen, heeft invloed op alle processen als het doel niet is opgegeven.

  
**log\_level\_decrease**\[=*target*\]
Logboek niveau verlagen, heeft invloed op alle processen als het doel niet is opgegeven.

[ ]{#lbAG}

[comment]: # ({/cac58159-045894c6})

[comment]: # ({b5a5b27a-b863520e})
###

  
Controledoelen op logniveau

  
*procestype*
Alle processen van het gespecificeerde type (configuratie synchronisatie, gegevensverzender,
ontdekker, hartslagzender, geschiedenissynchronisatie, huishoudster, http-poller,
icmp pinger, ipmi manager, ipmi poller, java poller, poller,
zelfcontrole, snmp trapper, taakbeheerder, trapper,
onbereikbare poller, vmware-collector)

  
*procestype,N*
Procestype en -nummer (bijv. poller,3)

  
*pid*
Proces-ID, tot 65535. Voor grotere waarden specificeer doel als
"procestype,N"

```{=html}
<!-- -->
```
**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAH}

[comment]: # ({/b5a5b27a-b863520e})

[comment]: # ({06b64bdd-583a1725})
## BESTANDEN

*/usr/local/etc/zabbix\_proxy.conf*
Standaardlocatie van het Zabbix-proxy configuratie bestand (indien niet gewijzigd)
tijdens het compileren).

[ ]{#lbAI}

[comment]: # ({/06b64bdd-583a1725})

[comment]: # ({da157037-41feb9bb})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAJ}

[comment]: # ({/da157037-41feb9bb})

[comment]: # ({e0f4ad0c-e55ed07e})
## AUTEUR

Alexei Vladishev <<alex@zabbix.com>>

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/e0f4ad0c-e55ed07e})

[comment]: # ({bb8288d1-9f3a9d2a})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[](#lbAF)

[](#lbAG)

  

[BESTANDEN](#lbAH)

[ZIE OOK](#lbAI)

[AUTEUR](#lbAJ)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/documentation/5.2/manpages),
met behulp van de handleidingen.\
Tijd: 16:12:22 GMT, 4 september 2020

[comment]: # ({/bb8288d1-9f3a9d2a})
