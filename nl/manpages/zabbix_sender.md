[comment]: # translation:outdated

[comment]: # ({new-4dfa5936})
# zabbix\_sender

Sectie: Gebruikerscommando's (1)\
Bijgewerkt: 2021-06-01\
[Index](#index) [Terug naar hoofdinhoud](/documentation/6.0/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/new-4dfa5936})

[comment]: # ({ad4a2c69-1fb3a028})
## NAAM

zabbix\_sender - Zabbix-afzender hulpprogramma [ ]{#lbAC}

[comment]: # ({/ad4a2c69-1fb3a028})

[comment]: # ({25135969-3f811652})
## KORTE INHOUD

**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *poort*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] **-s** *host* **-k** *key* **-o**
*waarde*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *poort*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] \[**-s** *host*\] \[**-T**\]
\[**-N**\] \[**-r**\] **-i** *invoerbestand*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *poort*\] \[**-I** *IP-adres*\] \[**-t** *time-out*\] \[**-s**
*host*\] **-k** *key* **-o** *value*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *poort*\] \[**-I** *IP-adres*\] \[**-t** *time-out*\] \[**-s**
*host*\] \[**-T**\] \[**-N**\] \[**-r**\] **-i** *invoerbestand*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *poort*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] **-s** *host* **--tls-connect**
**cert** **--tls-ca-bestand** *CA-bestand* \[**--tls-crl-bestand** *CRL-bestand*\]
\[**--tls-server-cert-issuer** *cert-issuer*\]
\[**--tls-server-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-bestand* **--tls-sleutelbestand** *sleutelbestand* \[**--tls-cipher13**
*cijferreeks*\] \[**--tls-cijfer** *cijferreeks*\] **-k** *sleutel*
**-o** *waarde*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *poort*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] \[**-s** *host*\] **--tls-connect**
**cert** **--tls-ca-bestand** *CA-bestand* \[**--tls-crl-bestand** *CRL-bestand*\]
\[**--tls-server-cert-issuer** *cert-issuer*\]
\[**--tls-server-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-bestand* **--tls-sleutelbestand** *sleutelbestand* \[**--tls-cipher13**
*cijferreeks*\] \[**--tls-cijfer** *cijferreeks*\] \[**-T**\]
\[**-N**\] \[**-r**\] **-i** *invoerbestand*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *poort*\] \[**-I** *IP-adres*\] \[**-t** *time-out*\] \[**-s**
*host*\] **--tls-connect** **cert** **--tls-ca-bestand** *CA-bestand*
\[**--tls-crl-bestand** *CRL-bestand*\] \[**--tls-server-cert-issuer**
*cert-issuer*\] \[**--tls-server-cert-subject** *cert-subject*\]
**--tls-cert-bestand** *cert-bestand* **--tls-sleutelbestand** *sleutelbestand*
\[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cijferreeks*\] **-k** *sleutel* **-o** *waarde*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *poort*\] \[**-I** *IP-adres*\] \[**-t** *time-out*\] \[**-s**
*host*\] **--tls-connect** **cert** **--tls-ca-bestand** *CA-bestand*
\[**--tls-crl-bestand** *CRL-bestand*\] \[**--tls-server-cert-issuer**
*cert-issuer*\] \[**--tls-server-cert-subject** *cert-subject*\]
**--tls-cert-bestand** *cert-bestand* **--tls-sleutelbestand** *sleutelbestand*
\[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cijferreeks*\] \[**-T**\] \[**-N**\] \[**-r**\] **-i** *invoerbestand*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *poort*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] **-s** *host* **--tls-connect**
**psk** **--tls-psk-identiteit** *PSK-identiteit* **--tls-psk-bestand**
*PSK-bestand* \[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cijferreeks*\] **-k** *sleutel* **-o** *waarde*\
**zabbix\_sender** \[**-v**\] **-z** *server* \[**-p** *poort*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] \[**-s** *host*\] **--tls-connect**
**psk** **--tls-psk-identiteit** *PSK-identiteit* **--tls-psk-bestand**
*PSK-bestand* \[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cijferreeks*\] \[**-T**\] \[**-N**\] \[**-r**\] **-i** *invoerbestand*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *poort*\] \[**-I** *IP-adres*\] \[**-t** *time-out*\] \[**-s**
*host*\] **--tls-connect** **psk** **--tls-psk-identity** *PSK-identity*
**--tls-psk-bestand** *PSK-bestand* \[**--tls-cipher13** *cipher-string*\]
\[**--tls-cijfer** *cijferreeks*\] **-k** *sleutel* **-o** *waarde*\
**zabbix\_sender** \[**-v**\] **-c** *config-file* \[**-z** *server*\]
\[**-p** *poort*\] \[**-I** *IP-adres*\] \[**-t** *time-out*\] \[**-s**
*host*\] **--tls-connect** **psk** **--tls-psk-identity** *PSK-identity*
**--tls-psk-bestand** *PSK-bestand* \[**--tls-cipher13** *cipher-string*\]
\[**--tls-cipher** *cipher-string*\] \[**-T**\] \[**-N**\] \[**-r**\]
**-i** *invoerbestand*\
**zabbix\_sender -h**\
**zabbix\_sender -V** [ ]{#lbAD}

[comment]: # ({/25135969-3f811652})

[comment]: # ({40871518-14895cca})
## BESCHRIJVING

**zabbix\_sender** is een opdrachtregel programma voor het verzenden van bewakingsgegevens
naar Zabbix-server of proxy. Op de Zabbix-server een item van het type **Zabbix
trapper** moet worden aangemaakt met de bijbehorende sleutel. Merk op dat inkomende
waarden worden alleen geaccepteerd van hosts gespecificeerd in **Toegestane hosts**
veld voor dit item. [ ]{#lbAE}

[comment]: # ({/40871518-14895cca})

[comment]: # ({14987927-cd7de714})
## OPTIES

**-c**, **--config** *config-bestand*
Gebruik *config-bestand*. **Zabbix-afzender** leest serverdetails van de
agentd configuratiebestand. Standaard leest **Zabbix-afzender** niet
elk configuratiebestand. Alleen parameters **Hostnaam**, **ServerActive**,
**BronIP**, **TLSConnect**, **TLSCAFile**, **TLSCRLFile**,
**TLSServerCertIssuer**, **TLSServerCertSubject**, **TLSCertFile**,
**TLSKeyFile**, **TLSPSKIdentity** en **TLSPSKFile** worden ondersteund. Allemaal
adressen gedefinieerd in de agent **ServerActive** configuratie parameter
worden gebruikt voor het verzenden van gegevens. Als het verzenden van batch gegevens niet naar één
adres, worden de volgende batches niet naar dit adres verzonden.

**-z**, **--zabbix-server** *server*
Hostnaam of IP-adres van Zabbix-server. Als een host wordt gecontroleerd door een
proxy, proxy-hostnaam of IP-adres moeten in plaats daarvan worden gebruikt. Wanneer gebruikt
samen met **--config** overschrijft de vermeldingen van **ServerActive**
parameter gespecificeerd in agentd configuratiebestand.

**-p**, **--poort** *poort*
Geef het poortnummer op van de Zabbix-server trapper die op de server wordt uitgevoerd.
Standaard is 10051. Bij gebruik in combinatie met **--config**, overschrijft de
poortvermeldingen van de parameter **ServerActive** gespecificeerd in agentd
configuratiebestand.

**-I**, **--bronadres** *IP-adres*
Geef het bron-IP-adres op. Bij gebruik samen met **--config**,
overschrijft de parameter **SourceIP** die is opgegeven in het configuratiebestand van de agent.

**-t**, **--time-out** *seconden*
Geef een time-out op. Geldig bereik: 1-300 seconden (standaard: 60)

**-s**, **--host** *host*
Geef de hostnaam op waartoe het item behoort (zoals geregistreerd in Zabbix
voorkant). Host IP-adres en DNS-naam werken niet. Wanneer gebruikt
samen met **--config** overschrijft de opgegeven parameter **Hostnaam**
in het agentd-configuratie bestand.

**-k**, **--toets** *toets*
Geef de itemsleutel op waarnaar de waarde moet worden verzonden.

**-o**, **--waarde** *waarde*
Geef de artikelwaarde op.

**-i**, **--invoerbestand** *invoerbestand*
Laad waarden uit het invoerbestand. Specificeer **-** als **<input-file>** to
lees waarden van standaard invoer. Elke regel van het bestand bevat witruimte
gescheiden: **<hostnaam> <sleutel> <waarde>**. Elke waarde
moet op zijn eigen regel worden opgegeven. Elke regel moet 3 spaties bevatten
scheidingstekens: **<hostnaam> <sleutel> <waarde>**, waarbij
"hostnaam" is de naam van de bewaakte host zoals geregistreerd in Zabbix
frontend, "sleutel" is de doel itemsleutel en "waarde" - de waarde die moet worden verzonden.
Specificeer **-** als **<hostnaam>** om hostnaam van agent te gebruiken
configuratiebestand of van het argument **--host**.

Een voorbeeld van een regel van een invoerbestand:

**"Linux DB3" db.verbindingen 43**

Het waardetype moet correct zijn ingesteld in de item configuratie van Zabbix
voorkant. Zabbix-zender verzendt tot 250 waarden in één verbinding.
De inhoud van het invoerbestand moet de UTF-8-codering hebben. Alle waarden
uit het invoerbestand worden in een sequentiële volgorde top-down verzonden. Inzendingen
moet worden opgemaakt volgens de volgende regels:

  
  
•
Geciteerde en niet-geciteerde inzendingen worden ondersteund.

•
Dubbel aanhalingsteken is het aanhalingsteken.

•
Vermeldingen met witruimte moeten worden geciteerd.

•
Dubbele aanhalingstekens en backslash-tekens binnen de invoer tussen aanhalingstekens moeten:
ontsnapte met een backslash.

•
Ontsnappen wordt niet ondersteund in niet-geciteerde items.

•
Escape-reeksen voor regelinvoer (\\n) worden ondersteund tussen tekenreeksen tussen aanhalingstekens.

•
Escape-reeksen voor regelinvoer worden bijgesneden vanaf het einde van een item.

**-T**, **--met-tijdstempels**
Deze optie kan alleen worden gebruikt met de optie **--input-file**.

Elke regel van het invoerbestand moet 4 door spaties gescheiden vermeldingen bevatten:
**<hostnaam> <sleutel> <tijdstempel> <waarde>**.
Tijdstempel moet worden opgegeven in Unix-tijdstempel formaat. Als doelitem
heeft triggers die ernaar verwijzen, alle tijdstempels moeten oplopend zijn
bestelling, anders zal de gebeurtenis berekening niet correct zijn.

Een voorbeeld van een regel van het invoerbestand:

**"Linux DB3" db.verbindingen 1429533600 43**

Zie optie **--input-file** voor meer details.

Als een tijdstempel waarde wordt verzonden voor een host die zich in een "geen gegevens" bevindt
onderhoudstype dan komt deze waarde te vervallen; hoe het ook is
mogelijk om een tijdstempel in te sturen voor een verlopen onderhoud
periode en het wordt geaccepteerd.

**-N**, **--met-ns**
Deze optie kan alleen worden gebruikt met de optie **--met-tijdstempels**.

Elke regel van het invoerbestand moet 5 door spaties gescheiden vermeldingen bevatten:
**<hostnaam> <sleutel> <tijdstempel> <ns>
<waarde>**.

Een voorbeeld van een regel van het invoerbestand:

**"Linux DB3" db.verbindingen 1429533600 7402561 43**

Zie optie **--input-file** voor meer details.

**-r**, **--realtime**
Verzend waarden één voor één zodra ze zijn ontvangen. Dit kan worden gebruikt
bij het lezen van standaardinvoer.

**--tls-connect** *waarde*
Verbinding maken met server of proxy. Waarden:

[ ]{#lbAF}

[comment]: # ({/14987927-cd7de714})

[comment]: # ({2c739157-4029bd0a})
###

  
**niet versleuteld**
verbinden zonder encryptie (standaard)

```{=html}
<!-- -->
```
  
**ps**
verbinding maken met TLS en een vooraf gedeelde sleutel

```{=html}
<!-- -->
```
  
**certificaat**
verbinding maken met TLS en een certificaat

```{=html}
<!-- -->
```
**--tls-ca-bestand** *CA-bestand*
Volledige pad naam van een bestand dat de CA('s)-certificaten op het hoogste niveau bevat voor:
peer-certificaat verificatie.

**--tls-crl-bestand** *CRL-bestand*
Volledige pad naam van een bestand met ingetrokken certificaten.

**--tls-server-cert-issuer** *cert-issuer*
Toegestane uitgever van server certificaten.

**--tls-server-cert-subject** *cert-subject*
Toegestaan onderwerp van server certificaat.

**--tls-cert-bestand** *cert-bestand*
Volledige pad naam van een bestand dat het certificaat of de certificaat keten bevat.

**--tls-sleutelbestand** *sleutelbestand*
Volledige pad naam van een bestand dat de persoonlijke sleutel bevat.

**--tls-psk-identiteit** *PSK-identiteit*
PSK-identiteitsreeks.

**--tls-psk-bestand** *PSK-bestand*
Volledige pad naam van een bestand dat de vooraf gedeelde sleutel bevat.

**--tls-cipher13** *cijferreeks*
Cipher string voor OpenSSL 1.1.1 of nieuwer voor TLS 1.3. Overschrijf de
standaard ciphersuite selectiecriteria. Deze optie is niet beschikbaar als:
OpenSSL-versie is minder dan 1.1.1.

**--tls-cijfer** *cijferreeks*
GnuTLS-prioriteits reeks (voor TLS 1.2 en hoger) of OpenSSL-coderings reeks
(alleen voor TLS 1.2). Overschrijf de standaard selectiecriteria voor ciphersuite.

**-v**, **--uitgebreid**
Uitgebreide modus, **-vv** voor meer details.

**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAG}

[comment]: # ({/2c739157-4029bd0a})

[comment]: # ({e87e5037-965bb13b})
## VERLATEN STATUS

De exit-status is 0 als de waarden zijn verzonden en ze allemaal waren
succesvol verwerkt door de server. Als gegevens zijn verzonden, maar verwerking van at
ten minste één van de waarden is mislukt, is de afsluitstatus 2. Als gegevens verzenden
mislukt, is de uitgangsstatus 1.

[ ]{#lbAH}

[comment]: # ({/e87e5037-965bb13b})

[comment]: # ({d8557083-9b276166})
## VOORBEELDEN

**zabbix\_sender -c /etc/zabbix/zabbix\_agentd.conf -k mysql.queries -o
342,45**\

  
Stuur **342.45** als de waarde voor **mysql.queries** item van gemonitord
gastheer. Gebruik bewaakte host en Zabbix-server gedefinieerd in agent
configuratiebestand.

**zabbix\_sender -c /etc/zabbix/zabbix\_agentd.conf -s "Bewaakte host"
-k mysql.queries -o 342.45**\

  
Verzend **342.45** als de waarde voor **mysql.queries** item van **Bewaakt
Host** host met Zabbix-server gedefinieerd in agentconfiguratiebestand.

\
**zabbix\_sender -z 192.168.1.113 -i data\_values.txt**

  
\
Stuur waarden van bestand **data\_values.txt** naar Zabbix-server met IP
**192.168.1.113**. Hostnamen en sleutels worden gedefinieerd in het bestand.

\
**echo "- hw.serienummer 1287872261 SQ4321ASDF" | zabbix\_sender -c
/usr/local/etc/zabbix\_agentd.conf -T -i -**\

  
Stuur een tijdstempelwaarde van de opdrachtregel naar de Zabbix-server,
gespecificeerd in het agentconfiguratiebestand. Streepjes in de invoergegevens
geeft aan dat hostnaam ook moet worden gebruikt vanuit dezelfde configuratie
het dossier.

\
**echo '"Zabbix-server" trapper.item ""' | zabbix\_sender -z
192.168.1.113 -p 10000 -i -**\

  
Stuur lege waarde van een item naar de Zabbix-server met IP-adres
**192.168.1.113** op poort **10000** vanaf de opdrachtregel. Lege waarden
moet worden aangegeven met lege dubbele aanhalingstekens.

**zabbix\_sender -z 192.168.1.113 -s "Bewaakte host" -k mysql.queries
-o 342.45 --tls-connect cert --tls-ca-file /home/zabbix/zabbix\_ca\_file
--tls-cert-bestand /home/zabbix/zabbix\_agentd.crt --tls-key-bestand
/home/zabbix/zabbix\_agentd.key**\

  
Verzend **342.45** als de waarde voor **mysql.queries** item in **Bewaakt
Host** host naar server met IP **192.168.1.113** met TLS met
certificaat.

**zabbix\_sender -z 192.168.1.113 -s "Bewaakte host" -k mysql.queries
-o 342.45 --tls-connect psk --tls-psk-identity "PSK ID Zabbix agentd"
--tls-psk-bestand /home/zabbix/zabbix\_agentd.psk**\

  
Verzend **342.45** als de waarde voor **mysql.queries** item in **Bewaakt
Host** host naar server met IP **192.168.1.113** met TLS met
vooraf gedeelde sleutel (PSK).

[ ]{#lbAI}

[comment]: # ({/d8557083-9b276166})

[comment]: # ({d7129711-554afaaf})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_web\_service](zabbix_web_service)**(8) [ ]{#lbAJ}

[comment]: # ({/d7129711-554afaaf})

[comment]: # ({f0309493-c84769dd})
## AUTEUR

Alexei Vladishev <[[\[email protected\]]{.__cf_email__
cfemail="0d6c6168754d776c6f6f6475236e6260"}](/cdn-cgi/l/email-protection#e48588819ca49e8586868d9cca878b89)>

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/f0309493-c84769dd})

[comment]: # ({3e051b05-0c5a6f3e})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[](#lbAF)

  

[STATUS VERLATEN](#lbAG)

[VOORBEELDEN](#lbAH)

[ZIE OOK](#lbAI)

[AUTEUR](#lbAJ)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/cgi-bin/man/man2html), met behulp van
de handleidingen.\
Tijd: 08:42:39 GMT, 11 juni 2021

[comment]: # ({/3e051b05-0c5a6f3e})
