[comment]: # translation:outdated

[comment]: # ({new-dabb0bd5})
# zabbix\_get

Sectie: Gebruikerscommando's (1)\
Bijgewerkt: 2021-06-01\
[Index](#index) [Terug naar hoofdinhoud](/documentation/6.0/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/new-dabb0bd5})

[comment]: # ({a61c6ba7-0ba1ce53})
## NAAM

zabbix\_get - Zabbix get - hulpprogramma [ ]{#lbAC}

[comment]: # ({/a61c6ba7-0ba1ce53})

[comment]: # ({5b2bea52-60e7e93b})
## KORTE INHOUD

**zabbix\_get -s** *hostnaam-of-IP* \[**-p** *poortnummer*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] **-k** *item-key*\
**zabbix\_get -s** *hostnaam-of-IP* \[**-p** *poortnummer*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] **--tls-connect** **cert**
**--tls-ca-bestand** *CA-bestand* \[**--tls-crl-bestand** *CRL-bestand*\]
\[**--tls-agent-cert-issuer** *cert-issuer*\]
\[**--tls-agent-cert-subject** *cert-subject*\] **--tls-cert-file**
*cert-bestand* **--tls-sleutelbestand** *sleutelbestand* \[**--tls-cipher13**
*cijferreeks*\] \[**--tls-cijfer** *cijferreeks*\] **-k**
*item-sleutel*\
**zabbix\_get -s** *hostnaam-of-IP* \[**-p** *poortnummer*\] \[**-I**
*IP-adres*\] \[**-t** *time-out*\] **--tls-connect** **psk**
**--tls-psk-identiteit** *PSK-identiteit* **--tls-psk-bestand** *PSK-bestand*
\[**--tls-cipher13** *cipher-string*\] \[**--tls-cipher**
*cijferreeks*\] **-k** *item-sleutel*\
**zabbix\_get -h**\
**zabbix\_get -V** [ ]{#lbAD}

[comment]: # ({/5b2bea52-60e7e93b})

[comment]: # ({76107f32-da08bc1a})
## BESCHRIJVING

**zabbix\_get** is een opdracht regelprogramma voor het ophalen van gegevens van de Zabbix
agent. [ ]{#lbAE}

[comment]: # ({/76107f32-da08bc1a})

[comment]: # ({6fec277b-4ab1a759})
## OPTIES

**-s**, **--host** *hostnaam-of-IP*
Geef de hostnaam of het IP-adres van een host op.

**-p**, **--poort** *poortnummer*
Geef het poortnummer op van de agent die op de host wordt uitgevoerd. Standaard is 10050.

**-I**, **--bron-adres** *IP-adres*
Geef het bron-IP-adres op.

**-t**, **--time-out** *seconden*
Geef een time-out op. Geldig bereik: 1-30 seconden (standaard: 30)

**-k**, **--sleutel** *item-sleutel*
Geef de sleutel op van het item waarvoor de waarde moet worden opgehaald.

**--tls-connect** *waarde*
Hoe verbinding te maken met de agent. Waarden:

[ ]{#lbAF}

[comment]: # ({/6fec277b-4ab1a759})

[comment]: # ({7417eb89-465bc4c8})
###

  
**niet versleuteld**
verbinden zonder encryptie (standaard)

```{=html}
<!-- -->
```
  
**ps**
verbinding maken met TLS en een vooraf gedeelde sleutel

```{=html}
<!-- -->
```
  
**certificaat**
verbinding maken met TLS en een certificaat

```{=html}
<!-- -->
```
**--tls-ca-bestand** *CA-bestand*
Volledige pad naam van een bestand dat de CA('s)-certificaten op het hoogste niveau bevat voor:
peer-certificaat verificatie.

**--tls-crl-bestand** *CRL-bestand*
Volledige pad naam van een bestand met ingetrokken certificaten.

**--tls-agent-cert-issuer** *cert-issuer*
Toegestane uitgever van agent certificaten.

**--tls-agent-cert-subject** *cert-subject*
Toegestaan certificaat onderwerp voor agent.

**--tls-cert-bestand** *cert-bestand*
Volledige pad naam van een bestand dat het certificaat of de certificaat keten bevat.

**--tls-sleutelbestand** *sleutelbestand*
Volledige pad naam van een bestand dat de persoonlijke sleutel bevat.

**--tls-psk-identiteit** *PSK-identiteit*
PSK-identiteitsreeks.

**--tls-psk-bestand** *PSK-bestand*
Volledige pad naam van een bestand dat de vooraf gedeelde sleutel bevat.

**--tls-cipher13** *cijferreeks*
Cipher string voor OpenSSL 1.1.1 of nieuwer voor TLS 1.3. Overschrijf de
standaard ciphersuite selectiecriteria. Deze optie is niet beschikbaar als:
OpenSSL-versie is minder dan 1.1.1.

**--tls-cijfer** *cijferreeks*
GnuTLS-prioriteits reeks (voor TLS 1.2 en hoger) of OpenSSL-coderings reeks
(alleen voor TLS 1.2). Overschrijf de standaard selectiecriteria voor ciphersuite.

**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAG}

[comment]: # ({/7417eb89-465bc4c8})

[comment]: # ({9c132e21-c5e4d00b})
## VOORBEELDEN

**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"**\
**zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect cert --tls-ca-file /home/zabbix/zabbix\_ca\_file
--tls-agent-cert-issuer "CN=CA ondertekenen,OU=IT-bewerkingen,O=Voorbeeld
Corp,DC=voorbeeld,DC=com" --tls-agent-cert-subject "CN=server1,OU=IT
operaties,O=Voorbeeld Corp,DC=voorbeeld,DC=com" --tls-cert-bestand
/home/zabbix/zabbix\_get.crt --tls-key-file
/home/zabbix/zabbix\_get.key\
zabbix\_get -s 127.0.0.1 -p 10050 -k "system.cpu.load\[all,avg1\]"
--tls-connect psk --tls-psk-identity "PSK ID Zabbix agentd"
--tls-psk-bestand /home/zabbix/zabbix\_agentd.psk** [ ]{#lbAH}

[comment]: # ({/9c132e21-c5e4d00b})

[comment]: # ({df0311d5-0ed7cca7})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_web\_service](zabbix_web_service)**(8) [ ]{#lbAI}

[comment]: # ({/df0311d5-0ed7cca7})

[comment]: # ({8ed4e63d-aaed5529})
## AUTEUR

Alexei Vladishev <[[\[email protected\]]{.__cf_email__
cfemail="254449405d655f4447474c5d0b464a48"}](/cdn-cgi/l/email-protection#eb8a878e93ab918a89898293c5888486)>

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/8ed4e63d-aaed5529})

[comment]: # ({26593404-bdb17437})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[](#lbAF)

  

[VOORBEELDEN](#lbAG)

[ZIE OOK](#lbAH)

[AUTEUR](#lbAI)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/cgi-bin/man/man2html), met behulp van
de handleidingen.\
Tijd: 08:42:29 GMT, 11 juni 2021

[comment]: # ({/26593404-bdb17437})
