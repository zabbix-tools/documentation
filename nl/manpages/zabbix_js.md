[comment]: # translation:outdated

[comment]: # ({dde4bf66-50528bfc})
# zabbix\_js

Sectie: Gebruikerscommando's (1)\
Bijgewerkt: 2019-01-29\
[Index](#index) [Terug naar hoofdinhoud](/documentation/5.0/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/dde4bf66-50528bfc})

[comment]: # ({35a1a6e6-42249a0d})
## NAAM

zabbix\_js - Zabbix JS-hulpprogramma [ ]{#lbAC}

[comment]: # ({/35a1a6e6-42249a0d})

[comment]: # ({4a5f2ee5-8c5d99cc})
## KORTE INHOUD

**zabbix\_js -s** *script-bestand* **-p** *input-param* \[**-l**
*logniveau*\] \[**-t** *time-out*\]\
**zabbix\_js -s** *scriptbestand* **-i** *invoerbestand* \[**-l**
*logniveau*\] \[**-t** *time-out*\]\
**zabbix\_js -h**\
**zabbix\_js -V** [ ]{#lbAD}

[comment]: # ({/4a5f2ee5-8c5d99cc})

[comment]: # ({055de244-b7046c11})
## BESCHRIJVING

**zabbix\_js** is een opdrachtregel programma dat kan worden gebruikt voor embedded
script testen. [ ]{#lbAE}

[comment]: # ({/055de244-b7046c11})

[comment]: # ({7778d170-02064894})
## OPTIES

**-s**, **--script** *script-bestand*
Geef de bestandsnaam op van het uit te voeren script. Als '-' is opgegeven als
bestandsnaam, wordt het script gelezen vanuit stdin.

**-p**, **--param** *input-param*
Geef de invoerparameter op.

**-i**, **--invoer** *invoerbestand*
Geef de bestandsnaam van de invoerparameter op. Als '-' is opgegeven als
bestandsnaam, wordt de invoer gelezen uit stdin.

**-l**, **--logniveau** *logniveau*
Geef het logniveau op.

**-t**, **--time-out** *time-out*
Geef de time-out in seconden op.

**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAF}

[comment]: # ({/7778d170-02064894})

[comment]: # ({4d9b8325-74bea5ed})
## VOORBEELDEN

**zabbix\_js -s script-file.js -p voorbeeld** [ ]{#lbAG}

[comment]: # ({/4d9b8325-74bea5ed})

[comment]: # ({06d1dee3-f909573a})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agent2](zabbix_agent2)**(8),
**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8)

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/06d1dee3-f909573a})

[comment]: # ({1f6392a4-af76d2f6})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[VOORBEELDEN](#lbAF)

[ZIE OOK](#lbAG)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/cgi-bin/man/man2html), met behulp van
de handleidingen.\
Tijd: 21:23:35 GMT, 18 maart 2020

[comment]: # ({/1f6392a4-af76d2f6})
