[comment]: # translation:outdated

[comment]: # ({new-d1b7141f})
# zabbix\_web\_service

Sectie: Onderhoudsopdrachten (8)\
Bijgewerkt: 2019-01-29\
[Index](#index) [Terug naar hoofdinhoud](/documentation/6.0/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/new-d1b7141f})

[comment]: # ({e6f999e4-6319fb62})
## NAAM

zabbix\_web\_service - Zabbix-web service [ ]{#lbAC}

[comment]: # ({/e6f999e4-6319fb62})

[comment]: # ({78ae4aa6-93a3222a})
## KORTE INHOUD

**zabbix\_web\_service** \[**-c** *config-file*\]\
**zabbix\_web\_service -h**\
**zabbix\_web\_service -V** [ ]{#lbAD}

[comment]: # ({/78ae4aa6-93a3222a})

[comment]: # ({6016f1bd-c1ed92af})
## BESCHRIJVING

**zabbix\_web\_service** is een applicatie voor het leveren van webservices aan:
Zabbix-componenten. [ ]{#lbAE}

[comment]: # ({/6016f1bd-c1ed92af})

[comment]: # ({e294931a-98c2799b})
## OPTIES

**-c**, **--config** *config-bestand*
Gebruik het alternatieve *config-bestand* in plaats van het standaardbestand.

**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAF}

[comment]: # ({/e294931a-98c2799b})

[comment]: # ({a0aef37d-440875a7})
## BESTANDEN

*/usr/local/etc/zabbix\_web\_service.conf*
Standaardlocatie van het configuratiebestand van de Zabbix-web service (indien niet
gewijzigd tijdens het compileren).

[ ]{#lbAG}

[comment]: # ({/a0aef37d-440875a7})

[comment]: # ({281dce92-4f67430a})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1),
**[zabbix\_server](zabbix_server)**(8), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAH}

[comment]: # ({/281dce92-4f67430a})

[comment]: # ({new-96162c7c})
## AUTHOR

Zabbix LLC

------------------------------------------------------------------------

[ ]{#index}

[comment]: # ({/new-96162c7c})

[comment]: # ({new-ea1c0f43})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[BESTANDEN](#lbAF)

[ZIE OOK](#lbAG)

[AUTEUR](#lbAH)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/documentation/6.0/manpages),
met behulp van de handleidingen.\
Tijd: 12:58:30 GMT, 11 juni 2021

[comment]: # ({/new-ea1c0f43})
