[comment]: # translation:outdated

[comment]: # ({5f8c4a3b-9beb7cfa})
# zabbix\_agent2

Sectie: Onderhoudsopdrachten (8)\
Bijgewerkt: 2019-01-29\
[Index](#index) [Terug naar hoofdinhoud](/documentation/5.0/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/5f8c4a3b-9beb7cfa})

[comment]: # ({c4953051-942e0d82})
## NAAM

zabbix\_agent2 - Zabbix agent 2\
\
[ ]{#lbAC}

[comment]: # ({/c4953051-942e0d82})

[comment]: # ({47cf682b-5535a811})
## KORTE INHOUD

**zabbix\_agent2** \[**-c** *config-bestand*\]\
**zabbix\_agent2** \[**-c** *config-bestand*\] **-p**\
**zabbix\_agent2** \[**-c** *config-bestand*\] **-t** *item-key*\
**zabbix\_agent2** \[**-c** *configuratiebestand*\] **-R** *runtime-optie*\
**zabbix\_agent2 -h**\
**zabbix\_agent2 -V**\
\
[ ]{#lbAD}

[comment]: # ({/47cf682b-5535a811})

[comment]: # ({1a2d114b-5b789849})
## BESCHRIJVING

**zabbix\_agent2** is een applicatie voor het bewaken van parameters van
verschillende diensten.\
\
[ ]{#lbAE}

[comment]: # ({/1a2d114b-5b789849})

[comment]: # ({118b33fd-7197fda6})
## OPTIES

**-c**, **--config** *config-bestand*
Gebruik het alternatieve *config-bestand* in plaats van het standaardbestand.

**-R**, **--runtime-control** *runtime-optie*
Voer administratieve functies uit volgens *runtime-optie*.

[comment]: # ({/118b33fd-7197fda6})

[comment]: # ({20ba7281-c754f1e2})
###

  

[comment]: # ({/20ba7281-c754f1e2})

[comment]: # ({1fb01940-c730d427})
#### Runtime-besturingsopties:

  
**gebruikersparameter herladen**
Herlaad gebruikersparameters uit het configuratiebestand

  
**logniveau verhogen**
Logboekniveau verhogen

  
**loglevel afname**
Logboekniveau verlagen

  
**helpen**
Beschikbare runtime-besturingsopties weergeven

  
**statistieken**
Beschikbare statistieken weergeven

  
**versie**
Weergaveversie

```{=html}
<!-- -->
```
**-p**, **--print**
Druk bekende items af en sluit af. Voor elk item zijn ofwel generieke standaardwaarden:
gebruikt, of specifieke standaardwaarden voor testen worden geleverd. Deze standaardinstellingen zijn:
vermeld tussen vierkante haken als itemkey parameters. Geretourneerde waarden zijn
tussen vierkante haken en voorafgegaan door het type van de geretourneerde
waarde, gescheiden door een pijpteken. Voor gebruikersparameters is het type altijd
**t**, omdat de agent niet alle mogelijke retourwaarden kan bepalen. Artikelen,
weergegeven als werkend, werken niet gegarandeerd vanaf de Zabbix-server
of zabbix\_get bij het opvragen van een actieve agent-daemon als machtigingen of
omgeving kan anders zijn. Typen met geretourneerde waarden zijn:

  
D
Getal met een decimaal deel.

  
m
Niet ondersteund. Dit kan worden veroorzaakt door het opvragen van een item dat alleen werkt
in de actieve modus, zoals een logboekbewakingsitem of een item waarvoor:
meerdere verzamelde waarden. Toestemmingsproblemen of onjuiste gebruiker
parameters kunnen ook resulteren in de niet-ondersteunde status.

  
s
Tekst. Maximale lengte niet beperkt.

  
t
Tekst. Hetzelfde als **s**.

  
u
Niet-ondertekend geheel getal.


**-t**, **--test** *item-sleutel*
Test een enkel item en sluit af. Zie **--print** voor uitvoerbeschrijving.

**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAG}

[comment]: # ({/1fb01940-c730d427})

[comment]: # ({81fa1594-37997f11})
## BESTANDEN

*/usr/local/etc/zabbix\_agent2.conf*
Standaardlocatie van Zabbix agent 2-configuratiebestand (indien niet gewijzigd
tijdens het compileren).

[ ]{#lbAH}

[comment]: # ({/81fa1594-37997f11})

[comment]: # ({fd3a242e-ad1bba15})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(8), **[zabbix\_js](zabbix_js)**(8),
**[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(8),
**[zabbix\_server](zabbix_server)**(8) [ ]{#lbAI}

[comment]: # ({/fd3a242e-ad1bba15})

[comment]: # ({a18b48b1-96162c7c})
## AUTEUR

Zabbix LLC

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/a18b48b1-96162c7c})

[comment]: # ({abffff03-8d9d9852})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[](#lbAF)

  

[BESTANDEN](#lbAG)

[ZIE OOK](#lbAH)

[AUTEUR](#lbAI)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/man/man2html), met behulp van de handleiding
Pagina's.\
Tijd: 14:07:57 GMT, 22 november 2021

[comment]: # ({/abffff03-8d9d9852})
