[comment]: # translation:outdated

[comment]: # ({7cc3d6b1-fdbd84bb})
# zabbix\_server

Sectie: Onderhoudsopdrachten (8)\
Bijgewerkt: 2020-09-04\
[Index](#index) [Terug naar hoofdinhoud](/documentation/5.2/manpages)

-------------------------------------------------- ----------------------

[ ]{#lbAB}

[comment]: # ({/7cc3d6b1-fdbd84bb})

[comment]: # ({b9251f58-1090a00e})
## NAAM

zabbix\_server - Zabbix-server daemon [ ]{#lbAC}

[comment]: # ({/b9251f58-1090a00e})

[comment]: # ({e8996a1c-2a5507db})
## KORTE INHOUD

**zabbix\_server** \[**-c** *configuratiebestand*\]\
**zabbix\_server** \[**-c** *configuratiebestand*\] **-R** *runtime-optie*\
**zabbix\_server -h**\
**zabbix\_server -V** [ ]{#lbAD}

[comment]: # ({/e8996a1c-2a5507db})

[comment]: # ({079e6167-f39aa640})
## BESCHRIJVING

**zabbix\_server** is de kern daemon van Zabbix-software. [ ]{#lbAE}

[comment]: # ({/079e6167-f39aa640})

[comment]: # ({0ee82fc2-455125db})
## OPTIES

**-c**, **--config** *config-bestand*
Gebruik het alternatieve *config-bestand* in plaats van het standaardbestand.

**-f**, **--voorgrond**
Voer de Zabbix-server op de voorgrond uit.

**-R**, **--runtime-control** *runtime-optie*
Voer administratieve functies uit volgens *runtime-optie*.

[ ]{#lbAF}

[comment]: # ({/0ee82fc2-455125db})

[comment]: # ({df3fa0ba-d7d7f728})
###

  
Runtime-besturingsopties

  
**config\_cache\_reload**
Laad de configuratie cache opnieuw. Wordt genegeerd als de cache momenteel wordt geladen.
Actieve Zabbix-proxy maakt verbinding met de Zabbix-server en vraagt
configuratie gegevens. Standaard configuratiebestand (tenzij de optie **-c** is
gespecificeerd) wordt gebruikt om het PID-bestand te vinden en het signaal wordt verzonden naar
proces, vermeld in PID-bestand.

  
**snmp\_cache\_reload**
Laad de SNMP-cache opnieuw.

  
**housekeeper\_execute**
Executeer de housekeeper. Genegeerd als housekeeper momenteel wordt
uitgevoerd.

  
**diaginfo**\[=*sectie*\]
Log interne diagnostische informatie van de opgegeven sectie. Sectie
kan *history cache*, *preprocessing* zijn. Standaard diagnose
informatie van alle secties wordt gelogd.

  
**log\_level\_increase**\[=*target*\]
Logboek niveau verhogen, heeft invloed op alle processen als het doel niet is opgegeven.

  
**log\_level\_decrease**\[=*target*\]
Logboek niveau verlagen, heeft invloed op alle processen als het doel niet is opgegeven.

[ ]{#lbAG}

[comment]: # ({/df3fa0ba-d7d7f728})

[comment]: # ({new-800a1525})

**ha\_remove\_node**\[=*target*\]  
Remove the high availability (HA) node specified by its name or ID.
Note that active/standby nodes cannot be removed.

**ha\_set\_failover\_delay**\[=*delay*\]  
Set high availability (HA) failover delay.
Time suffixes are supported, e.g. 10s, 1m.

**secrets\_reload**  
Reload secrets from Vault.

**service\_cache\_reload**  
Reload the service manager cache.

**snmp\_cache\_reload**  
Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.

**prof\_enable**\[=*target*\]  
Enable profiling.
Affects all processes if target is not specified.
Enabled profiling provides details of all rwlocks/mutexes by function name.
Supported since Zabbix 6.0.13.

**prof\_disable**\[=*target*\]  
Disable profiling.
Affects all processes if target is not specified.
Supported since Zabbix 6.0.13.
  
**log\_level\_increase**\[=*target*\]  
Increase log level, affects all processes if target is not specified

  
**log\_level\_decrease**\[=*target*\]  
Decrease log level, affects all processes if target is not specified

[ ]{#lbAG}

[comment]: # ({/new-800a1525})

[comment]: # ({3566609c-858c64d1})
###

  
Controledoelen op logniveau

  
*procestype*
Alle processen van het gespecificeerde type (waarschuwing, waarschuwingsmanager,
configuratie synchronisatie, ontdekker, roltrap, geschiedenissynchronisatie,
huishoudster, http poller, icmp pinger, ipmi manager, ipmi poller,
java poller, lld manager, lld worker, poller, preprocessing manager,
preprocessing werknemer, proxy poller, zelfcontrole, snmp trapper,
taakbeheer, timer, trapper, onbereikbare poller, vmware-collector)

  
*procestype,N*
Procestype en -nummer (bijv. poller,3)

  
*pid*
Proces-ID, tot 65535. Voor grotere waarden specificeer doel als
"procestype,N"

```{=html}
<!-- -->
```
**-h**, **--help**
Geef deze hulp weer en sluit af.

**-V**, **--versie**
Voer versie-informatie uit en sluit af.

[ ]{#lbAH}

[comment]: # ({/3566609c-858c64d1})

[comment]: # ({7901a325-00ee0d1e})
## BESTANDEN

*/usr/local/etc/zabbix\_server.conf*
Standaardlocatie van Zabbix-server configuratiebestand (indien niet gewijzigd
tijdens het compileren).

[ ]{#lbAI}

[comment]: # ({/7901a325-00ee0d1e})

[comment]: # ({12faddf0-d095490c})
## ZIE OOK

Documentatie <https://www.zabbix.com/manuals>

**[zabbix\_agentd](zabbix_agentd)**(8),
**[zabbix\_get](zabbix_get)**(1), **[zabbix\_proxy](zabbix_proxy)**(8),
**[zabbix\_sender](zabbix_sender)**(1), **[zabbix\_js](zabbix_js)**(1),
**[zabbix\_agent2](zabbix_agent2)**(8) [ ]{#lbAJ}

[comment]: # ({/12faddf0-d095490c})

[comment]: # ({a18b48b1-e55ed07e})
## AUTEUR

Zabbix LLC

-------------------------------------------------- ----------------------

[ ]{#inhoudsopgave}

[comment]: # ({/a18b48b1-e55ed07e})

[comment]: # ({699180ab-b105a0f1})
## Inhoudsopgave

[NAAM](#lbAB)

[SYNOPSIS](#lbAC)

[BESCHRIJVING](#lbAD)

[OPTIES](#lbAE)

[](#lbAF)

[](#lbAG)

  

[BESTANDEN](#lbAH)

[ZIE OOK](#lbAI)

[AUTEUR](#lbAJ)

-------------------------------------------------- ----------------------

Dit document is gemaakt door [man2html](/documentation/5.2/manpages),
met behulp van de handleidingen.\
Tijd: 16:12:14 GMT, 04 september 2020

[comment]: # ({/699180ab-b105a0f1})
