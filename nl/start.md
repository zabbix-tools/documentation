[comment]: # translation:outdated

[comment]: # ({3ce73236-63820343})
# Zabbix-documentatie

Deze pagina's bevatten officiële Zabbix-documentatie.

Gebruik de navigatie in de zijbalk om door documentatiepagina's te bladeren.

Om pagina's te kunnen bekijken, logt u in met uw [Zabbix
forums](http://www.zabbix.com/forum/) gebruikersnaam en wachtwoord.

[comment]: # ({/3ce73236-63820343})
