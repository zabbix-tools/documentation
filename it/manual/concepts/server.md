[comment]: # translation:outdated

[comment]: # ({new-cbb57ea2})
# 1 Server

[comment]: # ({/new-cbb57ea2})

[comment]: # ({new-0e549571})
#### Overview

Zabbix server is the central process of Zabbix software.

The server performs the polling and trapping of data, it calculates
triggers, sends notifications to users. It is the central component to
which Zabbix agents and proxies report data on availability and
integrity of systems. The server can itself remotely check networked
services (such as web servers and mail servers) using simple service
checks.

The server is the central repository in which all configuration,
statistical and operational data is stored, and it is the entity in
Zabbix that will actively alert administrators when problems arise in
any of the monitored systems.

The functioning of a basic Zabbix server is broken into three distinct
components; they are: Zabbix server, web frontend and database storage.

All of the configuration information for Zabbix is stored in the
database, which both the server and the web frontend interact with. For
example, when you create a new item using the web frontend (or API) it
is added to the items table in the database. Then, about once a minute
Zabbix server will query the items table for a list of the items which
are active that is then stored in a cache within the Zabbix server. This
is why it can take up to two minutes for any changes made in Zabbix
frontend to show up in the latest data section.

[comment]: # ({/new-0e549571})

[comment]: # ({new-c20247df})
#### Running server

[comment]: # ({/new-c20247df})

[comment]: # ({new-1314fd6f})
##### If installed as package

Zabbix server runs as a daemon process. The server can be started by
executing:

    shell> service zabbix-server start

This will work on most of GNU/Linux systems. On other systems you may
need to run:

    shell> /etc/init.d/zabbix-server start

Similarly, for stopping/restarting/viewing status, use the following
commands:

    shell> service zabbix-server stop
    shell> service zabbix-server restart
    shell> service zabbix-server status

[comment]: # ({/new-1314fd6f})


[comment]: # ({new-a339702b})
##### Runtime control

Runtime control options:

|Option|Description|Target|
|------|-----------|------|
|config\_cache\_reload|Reload configuration cache. Ignored if cache is being currently loaded.| |
|diaginfo\[=<**target**>\]|Gather diagnostic information in the server log file.|**historycache** - history cache statistics<br>**valuecache** - value cache statistics<br>**preprocessing** - preprocessing manager statistics<br>**alerting** - alert manager statistics<br>**lld** - LLD manager statistics<br>**locks** - list of mutexes (is empty on **BSD* systems)|
|ha\_status|Log high availability (HA) cluster status.| |
|ha\_remove\_node=target|Remove the high availability (HA) node specified by its listed number.<br>Note that active/standby nodes cannot be removed.|**target** - number of the node in the list (can be obtained by running ha\_status)|
|ha\_set\_failover\_delay=delay|Set high availability (HA) failover delay.<br>Time suffixes are supported, e.g. 10s, 1m.| |
|secrets\_reload|Reload secrets from Vault.| |
|service\_cache\_reload|Reload the service manager cache.| |
|snmp\_cache\_reload|Reload SNMP cache, clear the SNMP properties (engine time, engine boots, engine id, credentials) for all hosts.| |
|housekeeper\_execute|Start the housekeeping procedure. Ignored if the housekeeping procedure is currently in progress.| |
|trigger\_housekeeper\_execute|Start the trigger housekeeping procedure. Ignored if the trigger housekeeping procedure is currently in progress.| |
|log\_level\_increase\[=<**target**>\]|Increase log level, affects all processes if target is not specified.<br>Not supported on **BSD* systems.|**process type** - All processes of specified type (e.g., poller)<br>See all [server process types](#server_process_types).<br>**process type,N** - Process type and number (e.g., poller,3)<br>**pid** - Process identifier (1 to 65535). For larger values specify target as 'process type,N'.|
|log\_level\_decrease\[=<**target**>\]|Decrease log level, affects all processes if target is not specified.<br>Not supported on **BSD* systems.|^|

[comment]: # ({/new-a339702b})

[comment]: # ({new-1cb7f51c})
Example of using runtime control to reload the server configuration
cache:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R config_cache_reload

Examples of using runtime control to gather diagnostic information:

    Gather all available diagnostic information in the server log file:
    shell> zabbix_server -R diaginfo

    Gather history cache statistics in the server log file:
    shell> zabbix_server -R diaginfo=historycache

Example of using runtime control to reload the SNMP cache:

    shell> zabbix_server -R snmp_cache_reload  

Example of using runtime control to trigger execution of housekeeper:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R housekeeper_execute

Examples of using runtime control to change log level:

    Increase log level of all processes:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase

    Increase log level of second poller process:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=poller,2

    Increase log level of process with PID 1234:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_increase=1234

    Decrease log level of all http poller processes:
    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf -R log_level_decrease="http poller"

Example of setting the HA failover delay to the minimum of 10 seconds:

    shell> zabbix_server -R ha_set_failover_delay=10s

[comment]: # ({/new-1cb7f51c})

[comment]: # ({new-b4f10179})
##### Process user

Zabbix server is designed to run as a non-root user. It will run as
whatever non-root user it is started as. So you can run server as any
non-root user without any issues.

If you will try to run it as 'root', it will switch to a hardcoded
'zabbix' user, which must be [present](/manual/installation/install) on
your system. You can only run server as 'root' if you modify the
'AllowRoot' parameter in the server configuration file accordingly.

If Zabbix server and [agent](agent) are run on the same machine it is
recommended to use a different user for running the server than for
running the agent. Otherwise, if both are run as the same user, the
agent can access the server configuration file and any Admin level user
in Zabbix can quite easily retrieve, for example, the database password.

[comment]: # ({/new-b4f10179})

[comment]: # ({new-0a81f475})
##### Configuration file

See the [configuration file](/manual/appendix/config/zabbix_server)
options for details on configuring zabbix\_server.

[comment]: # ({/new-0a81f475})

[comment]: # ({new-49247ffc})
##### Start-up scripts

The scripts are used to automatically start/stop Zabbix processes during
system's start-up/shutdown. The scripts are located under directory
misc/init.d.

[comment]: # ({/new-49247ffc})

[comment]: # ({new-9f05badb})
#### Server process types

-   `alert manager` - alert queue manager
-   `alert syncer` - alert DB writer
-   `alerter` - process for sending notifications
-   `availability manager` - process for host availability updates
-   `configuration syncer` - process for managing in-memory cache of
    configuration data
-   `discoverer` - process for discovery of devices
-   `escalator` - process for escalation of actions
-   `history poller` - process for handling calculated and
    internal checks requiring a database connection
-   `history syncer` - history DB writer
-   `housekeeper` - process for removal of old historical data
-   `http poller` - web monitoring poller
-   `icmp pinger` - poller for icmpping checks
-   `ipmi manager` - IPMI poller manager
-   `ipmi poller` - poller for IPMI checks
-   `java poller` - poller for Java checks
-   `lld manager` - manager process of low-level discovery tasks
-   `lld worker` - worker process of low-level discovery tasks
-   `odbc poller` - poller for ODBC checks
-   `poller` - normal poller for passive checks
-   `preprocessing manager` - manager of preprocessing tasks
-   `preprocessing worker` - process for data preprocessing
-   `problem housekeeper` - process for removing problems of deleted
    triggers
-   `proxy poller` - poller for passive proxies
-   `report manager`- manager of scheduled report generation tasks
-   `report writer` - process for generating scheduled reports
-   `self-monitoring` - process for collecting internal server
    statistics
-   `snmp trapper` - trapper for SNMP traps
-   `task manager` - process for remote execution of tasks requested by
    other components (e.g. close problem, acknowledge problem, check
    item value now, remote command functionality)
-   `timer` - timer for processing maintenances
-   `trapper` - trapper for active checks, traps, proxy communication
-   `unreachable poller` - poller for unreachable devices
-   `vmware collector` - VMware data collector responsible for data
    gathering from VMware services

The server log file can be used to observe these process types.

Various types of Zabbix server processes can be monitored using the
**zabbix\[process,<type>,<mode>,<state>\]** internal
[item](/manual/config/items/itemtypes/internal).

[comment]: # ({/new-9f05badb})

[comment]: # ({new-cdd68340})
#### Supported platforms

Due to the security requirements and mission-critical nature of server
operation, UNIX is the only operating system that can consistently
deliver the necessary performance, fault tolerance and resilience.
Zabbix operates on market leading versions.

Zabbix server is tested on the following platforms:

-   Linux
-   Solaris
-   AIX
-   HP-UX
-   Mac OS X
-   FreeBSD
-   OpenBSD
-   NetBSD
-   SCO Open Server
-   Tru64/OSF1

::: noteclassic
Zabbix may work on other Unix-like operating systems as
well.
:::

[comment]: # ({/new-cdd68340})

[comment]: # ({new-982e2546})
#### Locale

Note that the server requires a UTF-8 locale so that some textual items
can be interpreted correctly. Most modern Unix-like systems have a UTF-8
locale as default, however, there are some systems where that may need
to be set specifically.

[comment]: # ({/new-982e2546})

[comment]: # ({new-bbb4c5d8})
##### Start up manually

If the above does not work you have to start it manually. Find the path
to the zabbix\_server binary and execute:

    shell> zabbix_server

You can use the following command line parameters with Zabbix server:

    -c --config <file>              path to the configuration file (default is /usr/local/etc/zabbix_server.conf)
    -f --foreground                 run Zabbix server in foreground
    -R --runtime-control <option>   perform administrative functions
    -h --help                       give this help
    -V --version                    display version number

Examples of running Zabbix server with command line parameters:

    shell> zabbix_server -c /usr/local/etc/zabbix_server.conf
    shell> zabbix_server --help
    shell> zabbix_server -V

[comment]: # ({/new-bbb4c5d8})
