<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/concepts/agent2.md">
    <body>
      <trans-unit id="d60be363" xml:space="preserve">
        <source># 3 Agent 2</source>
      </trans-unit>
      <trans-unit id="734dc3ed" xml:space="preserve">
        <source>#### Overview

Zabbix agent 2 is a new generation of Zabbix agent and may be used in
place of Zabbix agent. Zabbix agent 2 has been developed to:

-   reduce the number of TCP connections
-   provide improved concurrency of checks
-   be easily extendible with plugins. A plugin should be able to:
    -   provide trivial checks consisting of only a few simple lines of
        code
    -   provide complex checks consisting of long-running scripts and
        standalone data gathering with periodic sending back of the data
-   be a drop-in replacement for Zabbix agent (in that it supports all
    the previous functionality)

Agent 2 is written in Go programming language (with some C code of Zabbix agent reused). A configured Go environment with a currently supported [Go
version](https://golang.org/doc/devel/release#policy) is required for
building Zabbix agent 2.

Agent 2 does not have built-in daemonization support on Linux; it can be
run as a [Windows service](/manual/appendix/install/windows_agent).</source>
      </trans-unit>
      <trans-unit id="eaa0f2bb" xml:space="preserve">
        <source>##### Passive and active checks

Passive checks work similarly to Zabbix agent. Active checks support
scheduled/flexible intervals and check concurrency within one active
server. 

::: noteclassic
  By default, after a restart, Zabbix agent 2 will schedule the first data collection for active checks at a conditionally random time within the item's update interval to prevent spikes in resource usage. To perform active checks that do not have *Scheduling* [update interval](/manual/config/items/item/custom_intervals#scheduling-intervals) immediately after the agent restart, set `ForceActiveChecksOnStart` parameter (global-level) or `Plugins.&lt;Plugin name&gt;.System.ForceActiveChecksOnStart` (affects only specific plugin checks) in the [configuration file](/manual/appendix/config/zabbix_agent2). Plugin-level parameter, if set, will override the global parameter. Forcing active checks on start is supported since Zabbix 6.0.2. 
:::</source>
      </trans-unit>
      <trans-unit id="05ea1336" xml:space="preserve">
        <source>##### Check concurrency

Checks from different plugins can be executed concurrently. The number
of concurrent checks within one plugin is limited by the plugin capacity
setting. Each plugin may have a hardcoded capacity setting (100 being
default) that can be lowered using the `Plugins.&lt;PluginName&gt;.System.Capacity=N` setting in the *Plugins*
configuration [parameter](#configuration_file). Former name of this parameter `Plugins.&lt;PluginName&gt;.Capacity` is still supported, but has been deprecated in Zabbix 6.0.</source>
      </trans-unit>
      <trans-unit id="a2064519" xml:space="preserve">
        <source>#### Supported platforms

Zabbix agent 2 is supported on the following platforms:

-   Windows (all desktop and server versions since XP; also available as a [pre-compiled binary](https://www.zabbix.com/download_agents?version=6.4&amp;release=6.4.0&amp;os=Windows&amp;os_version=Any&amp;hardware=amd64&amp;encryption=OpenSSL&amp;packaging=MSI&amp;show_legacy=0))
-   Linux (also available in [distribution packages](https://www.zabbix.com/download?zabbix=6.4&amp;os_distribution=alma_linux&amp;os_version=9&amp;components=agent_2&amp;db=&amp;ws=))</source>
      </trans-unit>
      <trans-unit id="cabc0f5f" xml:space="preserve">
        <source>#### Installation

Zabbix agent 2 is available in pre-compiled Zabbix packages.
To compile Zabbix agent 2 from [sources](https://www.zabbix.com/download_sources) you have to specify the `--enable-agent2` configure option.</source>
      </trans-unit>
      <trans-unit id="45c02f12" xml:space="preserve">
        <source>#### Options

The following command line parameters can be used with Zabbix agent 2:

|Parameter|Description|
|--|--------|
|-c --config &lt;config-file&gt;|Path to the configuration file.&lt;br&gt;You may use this option to specify a configuration file that is not the default one.&lt;br&gt;On UNIX, default is /usr/local/etc/zabbix\_agent2.conf or as set by [compile-time](/manual/installation/install#installing_zabbix_daemons) variables *--sysconfdir* or *--prefix*|
|-f --foreground|Run Zabbix agent in foreground (default: true).|
|-p --print|Print known items and exit.&lt;br&gt;*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-t --test &lt;item key&gt;|Test specified item and exit.&lt;br&gt;*Note*: To return [user parameter](/manual/config/items/userparameters) results as well, you must specify the configuration file (if it is not in the default location).|
|-h --help|Print help information and exit.|
|-v --verbose|Print debugging information. Use this option with -p and -t options.|
|-V --version|Print agent version and license information.|
|-R --runtime-control &lt;option&gt;|Perform administrative functions. See [runtime control](/manual/concepts/agent2#runtime_control).|

Specific **examples** of using command line parameters:

-   print all built-in agent items with values
-   test a user parameter with "mysql.ping" key defined in the specified configuration file

    zabbix_agent2 --print
    zabbix_agent2 -t "mysql.ping" -c /etc/zabbix/zabbix_agentd.conf</source>
      </trans-unit>
      <trans-unit id="11aa0de0" xml:space="preserve">
        <source>##### Runtime control

Runtime control provides some options for remote control.

|Option|Description|
|--|--------|
|log\_level\_increase|Increase log level.|
|log\_level\_decrease|Decrease log level.|
|metrics|List available metrics.|
|version|Display agent version.|
|userparameter\_reload|Reload values of the *UserParameter* and *Include* options from the current configuration file.|
|help|Display help information on runtime control.|

Examples:

-   increasing log level for agent 2
-   print runtime control options

```{=html}
&lt;!-- --&gt;
```
    zabbix_agent2 -R log_level_increase
    zabbix_agent2 -R help</source>
      </trans-unit>
      <trans-unit id="77e9b590" xml:space="preserve">
        <source>#### Configuration file

The configuration parameters of agent 2 are mostly compatible with
Zabbix agent with some exceptions.

|New parameters|Description|
|--|--------|
|*ControlSocket*|The runtime control socket path. Agent 2 uses a control socket for [runtime commands](#runtime_control).|
|*EnablePersistentBuffer*,&lt;br&gt;*PersistentBufferFile*,&lt;br&gt;*PersistentBufferPeriod*|These parameters are used to configure persistent storage on agent 2 for active items.|
|*ForceActiveChecksOnStart*|Determines whether the agent should perform active checks immediately after restart or spread evenly over time.|
|*Plugins*|Plugins may have their own parameters, in the format `Plugins.&lt;Plugin name&gt;.&lt;Parameter&gt;=&lt;value&gt;`. A common plugin parameter is *System.Capacity*, setting the limit of checks that can be executed at the same time.|
|*StatusPort*|The port agent 2 will be listening on for HTTP status request and display of a list of configured plugins and some internal parameters|
|**Dropped parameters**|**Description**|
|*AllowRoot*, *User*|Not supported because daemonization is not supported.|
|*LoadModule*, *LoadModulePath*|Loadable modules are not supported.|
|*StartAgents*|This parameter was used in Zabbix agent to increase passive check concurrency or disable them. In Agent 2, the concurrency is configured at a plugin level and can be limited by a capacity setting. Whereas disabling passive checks is not currently supported.|
|*HostInterface*, *HostInterfaceItem*|Not yet supported.|

For more details see the configuration file options for
[zabbix\_agent2](/manual/appendix/config/zabbix_agent2).</source>
      </trans-unit>
      <trans-unit id="472820ac" xml:space="preserve">
        <source>#### Exit codes

Starting from version 4.4.8 Zabbix agent 2 can also be compiled with
older OpenSSL versions (1.0.1, 1.0.2).

In this case Zabbix provides mutexes for locking in OpenSSL. If a mutex
lock or unlock fails then an error message is printed to the standard
error stream (STDERR) and Agent 2 exits with return code 2 or 3,
respectively.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
