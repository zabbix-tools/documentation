[comment]: # translation:outdated

[comment]: # ({new-fb8f0d5a})
# 1 High availability cluster

[comment]: # ({/new-fb8f0d5a})

[comment]: # ({new-ea8bc3db})
#### Overview

The high availability mode offers protection against software/hardware
failures for Zabbix server and allows to minimize downtime during
software/hardware maintenance.

The high availability (HA) cluster is an opt-in solution and it is
supported for Zabbix server. The native HA solution is designed to be
simple in use, it will work across sites and does not have specific
requirements for the databases that Zabbix recognizes. Users are free to
use the native Zabbix HA solution, or a third party HA solution,
depending on what best suits the high availability requirements in their
environment.

The solution consists of multiple zabbix\_server instances or nodes.
Every node:

-   is configured separately (configuration file, scripts, encryption,
    data export)
-   uses the same database
-   has several modes: active, standby, unavailable, stopped

Only one node can be active (working) at a time. The standby nodes do no
data collection, processing or other regular server activities; they do
not listen on ports; they have minimum database connections.

Both active and standby nodes update their last access time every 5
seconds. Each standby node monitors the last access time of the active
node. If the last access time of the active node is over 'failover
delay' seconds, the standby node switches itself to be the active node
and assigns 'unavailable' status to the previously active node.

The active node monitors its own database connectivity - if it is lost
for more than `failover delay-5` seconds, it must stop all processing
and switch to standby mode. The active node also monitors the status of
the standby nodes - if the last access time of a standby node is over
'failover delay' seconds, the standby node is assigned the 'unavailable'
status.

The failover delay is configurable, with the minimum being 10 seconds.

The nodes are designed to be compatible across minor Zabbix versions.

[comment]: # ({/new-ea8bc3db})

[comment]: # ({new-567d3671})
#### Enabling HA cluster

[comment]: # ({/new-567d3671})

[comment]: # ({new-23e81771})
##### Server configuration

To turn any Zabbix server from a standalone server into an HA cluster
node, specify the HANodeName parameter in the server
[configuration](/manual/appendix/config/zabbix_server).

The NodeAddress parameter (address:port), if set, must be used by the
frontend for the active node, overriding the value in zabbix.conf.php.

[comment]: # ({/new-23e81771})

[comment]: # ({new-3e9035e5})
##### Preparing frontend

Make sure that Zabbix server address:port is **not defined** in the 
frontend configuration.

Zabbix frontend will autodetect the active node by reading settings 
from the nodes table in Zabbix database. Node address of the active node 
will be used as the Zabbix server address.

[comment]: # ({/new-3e9035e5})

[comment]: # ({new-d47b6ef3})
##### Proxy configuration

To enable connections to multiple servers in a high availability setup,
list addresses of the HA nodes in the Server
[parameter](/manual/appendix/config/zabbix_proxy) of the proxy,
separated by a semicolon.

[comment]: # ({/new-d47b6ef3})

[comment]: # ({new-f2fc0e77})
##### Agent configuration

To enable connections to multiple servers in a high availability setup,
list addresses of the HA nodes in the ServerActive
[parameter](/manual/appendix/config/zabbix_agentd) of the agent,
separated by a semicolon.

[comment]: # ({/new-f2fc0e77})

[comment]: # ({new-311341fc})
### Failover to standby node

Zabbix will fail over to another node automatically if the active node stops. There 
must be at least one node in standby status for the failover to happen.

How fast will the failover be? All nodes update their last access time (and status, if 
it is changed) every 5 seconds. So: 

-   If the active node shuts down and manages to report its status 
as "shut down", another node will take over within **5 seconds**.

-   If the active node shuts down/becomes unavailable without being able to update 
its status, standby nodes will wait for the **failover delay** + 5 seconds to take over

The failover delay is configurable, with the supported range between 10 seconds and 15 
minutes (one minute by default). To change the failover delay, you may run:

```
zabbix_server -R ha_set_failover_delay=5m
```

[comment]: # ({/new-311341fc})

[comment]: # ({new-593144b8})
#### Managing HA cluster

The current status of the HA cluster can be managed using the dedicated
[runtime control](/manual/concepts/server#runtime_control) options:

-   ha\_status - log HA cluster status in the Zabbix server log;
-   ha\_remove\_node=target - remove an HA node identified by its
    <target> - number of the node in the list (the number can be
    obtained from the output of running ha\_status). Note that
    active/standby nodes cannot be removed.
-   ha\_set\_failover\_delay=delay - set HA failover delay (time
    suffixes are supported, e.g. 10s, 1m)

Node status can be monitored:

-   in *Reports* → *[System
    information](/manual/web_interface/frontend_sections/reports/status_of_zabbix#status_of_ha_cluster_nodes)*
-   in the *System information* dashboard widget
-   using the `ha_status` runtime control option of the server (see
    above).

The `zabbix[cluster,discovery,nodes]` internal item can be used for node
discovery, as it returns a JSON with high availability node information.

[comment]: # ({/new-593144b8})

[comment]: # ({new-82cd7e56})
#### Disabling HA cluster

To disable a high availability cluster:

-   make backup copies of configuration files
-   stop standby nodes
-   remove the HANodeName parameter from the active primary server
-   restart the primary server (it will start in standalone mode)

[comment]: # ({/new-82cd7e56})

[comment]: # ({new-7d944a36})
### Upgrading HA cluster

To perform a major version upgrade for the HA nodes:

-   stop all nodes;
-   create a full database backup;
-   if the database uses replication make sure that all nodes are in sync and have no issues. Do not upgrade if replication is broken.
-   select a single node that will perform database upgrade, change its configuration to standalone mode by commenting out HANodeName and [upgrade](/manual/installation/upgrade) it;
-   make sure that database upgrade is fully completed (*System information* should display that Zabbix server is running);
-   restart the node in HA mode;
-   upgrade and start the rest of nodes (it is not required to change them to standalone mode as the database is already upgraded at this point).

In a minor version upgrade it is sufficient to upgrade the first node, make sure it has upgraded and running, and then start upgrade on the next node.

[comment]: # ({/new-7d944a36})

[comment]: # ({new-f4d3143a})

### Implementation details

The high availability (HA) cluster is an opt-in solution and it is
supported for Zabbix server. The native HA solution is designed to be
simple in use, it will work across sites and does not have specific
requirements for the databases that Zabbix recognizes. Users are free to
use the native Zabbix HA solution, or a third party HA solution,
depending on what best suits the high availability requirements in their
environment.

The solution consists of multiple zabbix\_server instances or nodes.
Every node:

-   is configured separately
-   uses the same database
-   may have several modes: active, standby, unavailable, stopped

Only one node can be active (working) at a time. A standby node runs only one 
process - the HA manager. A standby node does no data collection, 
processing or other regular server activities; they do not listen 
on ports; they have minimum database connections.

Both active and standby nodes update their last access time every 5
seconds. Each standby node monitors the last access time of the active
node. If the last access time of the active node is over 'failover
delay' seconds, the standby node switches itself to be the active node
and assigns 'unavailable' status to the previously active node.

The active node monitors its own database connectivity - if it is lost
for more than `failover delay-5` seconds, it must stop all processing
and switch to standby mode. The active node also monitors the status of
the standby nodes - if the last access time of a standby node is over
'failover delay' seconds, the standby node is assigned the 'unavailable'
status.

The failover delay is configurable, with the minimum being 10 seconds.

The nodes are designed to be compatible across minor Zabbix versions.

[comment]: # ({/new-f4d3143a})
