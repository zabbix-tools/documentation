[comment]: # translation:outdated

[comment]: # ({new-52e806d3})
# 1 Template groups

In the frontend template groups can be [exported](/manual/xml_export_import)
only with template export. When a template is exported
all groups it belongs to are exported with it automatically.

API allows to export template groups independently from templates.

[comment]: # ({/new-52e806d3})

[comment]: # ({new-c9081ccb})
##### Export format

``` {.yaml}
  template_groups:
    -
      uuid: 36bff6c29af64692839d077febfc7079
      name: 'Network devices'
```

[comment]: # ({/new-c9081ccb})

[comment]: # ({new-9deb92e2})
#### Element tags

|Parameter|Type|Description|
|------|---|---------|
|uuid|*string*|Unique identifier for this template group. |
|name|*string*|Group name.|

[comment]: # ({/new-9deb92e2})
