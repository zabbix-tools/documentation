<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/xml_export_import/hosts.md">
    <body>
      <trans-unit id="ae6d2ebd" xml:space="preserve">
        <source># 4 Hosts</source>
      </trans-unit>
      <trans-unit id="907c3faa" xml:space="preserve">
        <source>#### Overview

Hosts are [exported](/manual/xml_export_import) with many related
objects and object relations.

Host export contains:

-   Linked host groups
-   Host data
-   Template linkage
-   Host group linkage
-   Host interfaces
-   Directly linked items
-   Directly linked triggers
-   Directly linked graphs
-   Directly linked discovery rules with all prototypes
-   Directly linked web scenarios
-   Host macros
-   Host inventory data
-   Value maps</source>
      </trans-unit>
      <trans-unit id="55d6da26" xml:space="preserve">
        <source>#### Exporting

To export hosts, do the following:

1. Go to *Data collection* → *Hosts*.
2. Mark the checkboxes of the hosts to export.
3. Click on *Export* below the list.

![](../../../assets/en/manual/xml_export_import/export_hosts.png)

Depending on the selected format, hosts are exported to a local file
with a default name:

-   *zabbix\_export\_hosts.yaml* - in YAML export (default option for export);
-   *zabbix\_export\_hosts.xml* - in XML export;
-   *zabbix\_export\_hosts.json* - in JSON export.</source>
      </trans-unit>
      <trans-unit id="158cdcbf" xml:space="preserve">
        <source>#### Importing

To import hosts, do the following:

1. Go to *Data collection* → *Hosts*.
2. Click on *Import* to the right.
3. Select the import file.
4. Click on *Import*.

![](../../../assets/en/manual/xml_export_import/import_hosts.png){width="600"}

If you mark the *Advanced options* checkbox, a detailed list of all importable elements will be displayed - mark or unmark each import rule as required.

If you click the checkbox in the *All* row, all elements below it become marked/unmarked.

Import rules:

|Rule|Description|
|----|-----------|
|*Update existing*|Existing elements will be updated with data taken from the import file. Otherwise they will not be updated.|
|*Create new*|The import will add new elements using data from the import file. Otherwise it will not add them.|
|*Delete missing*|The import will remove existing elements not present in the import file. Otherwise it will not remove them.&lt;br&gt;If *Delete missing* is marked for template linkage, existing template linkage not present in the import file will be unlinked. Entities (items, triggers, graphs, etc.) inherited from the unlinked templates will not be removed (unless the *Delete missing* option is selected for each entity as well).|

A success or failure message of the import will be displayed in the
frontend.</source>
      </trans-unit>
      <trans-unit id="93059e09" xml:space="preserve">
        <source>#### Export format

Export format in YAML:

```yaml
zabbix_export:
  version: '7.0'
  host_groups:
    - uuid: f2481361f99448eea617b7b1d4765566
      name: 'Discovered hosts'
    - uuid: 6f6799aa69e844b4b3918f779f2abf08
      name: 'Zabbix servers'
  hosts:
    - host: 'Zabbix server 1'
      name: 'Main Zabbix server'
      templates:
        - name: 'Linux by Zabbix agent'
        - name: 'Zabbix server health'
      groups:
        - name: 'Discovered hosts'
        - name: 'Zabbix servers'
      interfaces:
        - ip: 192.168.1.1
          interface_ref: if1
      items:
        - name: 'Zabbix trap'
          type: TRAP
          key: trap
          delay: '0'
          history: 1w
          preprocessing:
            - type: MULTIPLIER
              parameters:
                - '8'
          tags:
            - tag: Application
              value: 'Zabbix server'
          triggers:
            - expression: 'last(/Zabbix server 1/trap)=0'
              name: 'Last value is zero'
              priority: WARNING
              tags:
                - tag: Process
                  value: 'Internal test'
      tags:
        - tag: Process
          value: Zabbix
      macros:
        - macro: '{$HOST.MACRO}'
          value: '123'
        - macro: '{$PASSWORD1}'
          type: SECRET_TEXT
      inventory:
        type: 'Zabbix server'
        name: yyyyyy-HP-Pro-3010-Small-Form-Factor-PC
        os: 'Linux yyyyyy-HP-Pro-3010-Small-Form-Factor-PC 4.4.0-165-generic #193-Ubuntu SMP Tue Sep 17 17:42:52 UTC 2019 x86_64'
      inventory_mode: AUTOMATIC
  graphs:
    - name: 'CPU utilization server'
      show_work_period: 'NO'
      show_triggers: 'NO'
      graph_items:
        - drawtype: FILLED_REGION
          color: FF5555
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,steal]'
        - sortorder: '1'
          drawtype: FILLED_REGION
          color: 55FF55
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,softirq]'
        - sortorder: '2'
          drawtype: FILLED_REGION
          color: '009999'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,interrupt]'
        - sortorder: '3'
          drawtype: FILLED_REGION
          color: '990099'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,nice]'
        - sortorder: '4'
          drawtype: FILLED_REGION
          color: '999900'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,iowait]'
        - sortorder: '5'
          drawtype: FILLED_REGION
          color: '990000'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,system]'
        - sortorder: '6'
          drawtype: FILLED_REGION
          color: '000099'
          calc_fnc: MIN
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,user]'
        - sortorder: '7'
          drawtype: FILLED_REGION
          color: '009900'
          item:
            host: 'Zabbix server 1'
            key: 'system.cpu.util[,idle]'

```</source>
      </trans-unit>
      <trans-unit id="2c61d3f8" xml:space="preserve">
        <source>#### Element tags

Element tag values are explained in the table below.</source>
      </trans-unit>
      <trans-unit id="eedfe2b9" xml:space="preserve">
        <source>##### Host tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|--|--|-|--|----|--------|
|host_groups| |x| | |Root element for host groups.|
| |uuid|x|`string`| |Unique identifier for this host group.|
| |name|x|`string`| |Host group name.|
|hosts| |\-| | |Root element for hosts.|
| |host|x|`string`| |Unique host name.|
| |name|\-|`string`| |Visible host name.|
| |description|\-|`text`| |Host description.|
| |status|\-|`string`|0 - ENABLED (default)&lt;br&gt;1 - DISABLED|Host status.|
| |ipmi\_authtype|\-|`string`|-1 - DEFAULT (default)&lt;br&gt;0 - NONE&lt;br&gt;1 - MD2&lt;br&gt;2 - MD5&lt;br&gt;4 - STRAIGHT&lt;br&gt;5 - OEM&lt;br&gt;6 - RMCP\_PLUS|IPMI session authentication type.|
| |ipmi\_privilege|\-|`string`|1 - CALLBACK&lt;br&gt;2 - USER (default)&lt;br&gt;3 - OPERATOR&lt;br&gt;4 - ADMIN&lt;br&gt;5 - OEM|IPMI session privilege level.|
| |ipmi\_username|\-|`string`| |Username for IPMI checks.|
| |ipmi\_password|\-|`string`| |Password for IPMI checks.|
|proxy| |\-| | |Proxy.|
| |name|x|`string`| |Name of the proxy (if any) that monitors the host.|
|templates| |\-| | |Root element for linked templates.|
| |name|x|`string`| |Template name.|
|interfaces| |\-| | |Root element for host interfaces.|
| |default|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Whether this is the primary host interface.&lt;br&gt;There can be only one primary interface of one type on a host.|
| |type|\-|`string`|1 - ZABBIX (default)&lt;br&gt;2 - SNMP&lt;br&gt;3 - IPMI&lt;br&gt;4 - JMX|Interface type.|
| |useip|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Whether to use IP as the interface for connecting to the host (if not, DNS will be used).|
| |ip|\-|`string`| |IP address, can be either IPv4 or IPv6.&lt;br&gt;&lt;br&gt;Required if the connection is made via IP.|
| |dns|\-|`string`| |DNS name.&lt;br&gt;&lt;br&gt;Required if the connection is made via DNS.|
| |port|\-|`string`| |Port number. Supports user macros.|
| |interface\_ref|x|`string`|Format: `if&lt;N&gt;`|Interface reference name to be used in items.|
|details| |\-| | |Root element for interface details.|
| |version|\-|`string`|1 - SNMPV1&lt;br&gt;2 - SNMP\_V2C (default)&lt;br&gt;3 - SNMP\_V3|Use this SNMP version.|
| |community|\-|`string`| |SNMP community.&lt;br&gt;&lt;br&gt;Required by SNMPv1 and SNMPv2 items.|
| |contextname|\-|`string`| |SNMPv3 context name.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |securityname|\-|`string`| |SNMPv3 security name.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |securitylevel|\-|`string`|0 - NOAUTHNOPRIV (default)&lt;br&gt;1 - AUTHNOPRIV&lt;br&gt;2 - AUTHPRIV|SNMPv3 security level.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |authprotocol|\-|`string`|0 - MD5 (default)&lt;br&gt;1 - SHA1&lt;br&gt;2 - SHA224&lt;br&gt;3 - SHA256&lt;br&gt;4 - SHA384&lt;br&gt;5 - SHA512|SNMPv3 authentication protocol.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |authpassphrase|\-|`string`| |SNMPv3 authentication passphrase.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |privprotocol|\-|`string`|0 - DES (default)&lt;br&gt;1 - AES128&lt;br&gt;2 - AES192&lt;br&gt;3 - AES256&lt;br&gt;4 - AES192C&lt;br&gt;5 - AES256C|SNMPv3 privacy protocol.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |privpassphrase|\-|`string`| |SNMPv3 privacy passphrase.&lt;br&gt;&lt;br&gt;Used only by SNMPv3 items.|
| |bulk|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Use bulk requests for SNMP.|
|items| |\-| | |Root element for items.|
| |*For item element tag values, see host [item](/manual/xml_export_import/hosts#host_item_tags) tags.*| | | | |
|tags| |\-| | |Root element for host tags.|
| |tag|x|`string`| |Tag name.|
| |value|\-|`string`| |Tag value.|
|macros| |\-| | |Root element for macros.|
| |macro|x| | |User macro name.|
| |type|\-|`string`|0 - TEXT (default)&lt;br&gt;1 - SECRET\_TEXT&lt;br&gt;2 - VAULT|Type of the macro.|
| |value|\-|`string`| |User macro value.|
| |description|\-|`string`| |User macro description.|
|inventory| |\-| | |Root element for host inventory.|
| |&lt;inventory\_property&gt;|\-| | |Individual inventory property.&lt;br&gt;&lt;br&gt;All available inventory properties are listed under the respective tags, e.g. &lt;type&gt;, &lt;name&gt;, &lt;os&gt; (see example above).|
|inventory\_mode| |\-|`string`|-1 - DISABLED&lt;br&gt;0 - MANUAL (default)&lt;br&gt;1 - AUTOMATIC|Inventory mode.|
|valuemaps| |\-| | |Root element for host value maps.|
| |name|x|`string`| |Value map name.|
| |mapping|\-| | |Root element for mappings.|
| |value|x|`string`| |Value of a mapping.|
| |newvalue|x|`string`| |New value of a mapping.|</source>
      </trans-unit>
      <trans-unit id="8a2e4e2f" xml:space="preserve">
        <source>##### Host item tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|--|--|-|--|----|--------|
|items| |\-| | |Root element for items.|
| |name|x|`string`| |Item name.|
| |type|\-|`string`|0 - ZABBIX\_PASSIVE (default)&lt;br&gt;2 - TRAP&lt;br&gt;3 - SIMPLE&lt;br&gt;5 - INTERNAL&lt;br&gt;7 - ZABBIX\_ACTIVE&lt;br&gt;10 - EXTERNAL&lt;br&gt;11 - ODBC&lt;br&gt;12 - IPMI&lt;br&gt;13 - SSH&lt;br&gt;14 - TELNET&lt;br&gt;15 - CALCULATED&lt;br&gt;16 - JMX&lt;br&gt;17 - SNMP\_TRAP&lt;br&gt;18 - DEPENDENT&lt;br&gt;19 - HTTP\_AGENT&lt;br&gt;20 - SNMP\_AGENT&lt;br&gt;21 - ITEM\_TYPE\_SCRIPT|Item type.|
| |snmp\_oid|\-|`string`| |SNMP object ID.&lt;br&gt;&lt;br&gt;Required by SNMP items.|
| |key|x|`string`| |Item key.|
| |delay|\-|`string`|Default: 1m|Update interval of the item.&lt;br&gt;&lt;br&gt;Note that `delay` will be always '0' for trapper items.&lt;br&gt;&lt;br&gt;Accepts seconds or a time unit with suffix (30s, 1m, 2h, 1d).&lt;br&gt;Optionally one or more [custom intervals](/manual/config/items/item/custom_intervals) can be specified either as flexible intervals or scheduling.&lt;br&gt;Multiple intervals are separated by a semicolon.&lt;br&gt;User macros may be used. A single macro has to fill the whole field. Multiple macros in a field or macros mixed with text are not supported.&lt;br&gt;Flexible intervals may be written as two macros separated by a forward slash (e.g. `{$FLEX_INTERVAL}/{$FLEX_PERIOD}`).|
| |history|\-|`string`|Default: 90d|A time unit of how long the history data should be stored. Time unit with suffix, user macro or LLD macro.|
| |trends|\-|`string`|Default: 365d|A time unit of how long the trends data should be stored. Time unit with suffix, user macro or LLD macro.|
| |status|\-|`string`|0 - ENABLED (default)&lt;br&gt;1 - DISABLED|Item status.|
| |value\_type|\-|`string`|0 - FLOAT&lt;br&gt;1 - CHAR&lt;br&gt;2 - LOG&lt;br&gt;3 - UNSIGNED (default)&lt;br&gt;4 - TEXT&lt;br&gt;5 - BINARY|Received value type.|
| |allowed\_hosts|\-|`string`| |List of IP addresses (comma delimited) of hosts allowed sending data for the item.&lt;br&gt;&lt;br&gt;Used by trapper and HTTP agent items.|
| |units|\-|`string`| |Units of returned values (bps, B, etc).|
| |params|\-|`text`| |Additional parameters depending on the type of the item:&lt;br&gt;- executed script for Script, SSH and Telnet items;&lt;br&gt;- SQL query for database monitor items;&lt;br&gt;- formula for calculated items.|
| |ipmi\_sensor|\-|`string`| |IPMI sensor.&lt;br&gt;&lt;br&gt;Used only by IPMI items.|
| |authtype|\-|`string`|Authentication type for SSH agent items:&lt;br&gt;0 - PASSWORD (default)&lt;br&gt;1 - PUBLIC\_KEY&lt;br&gt;&lt;br&gt;Authentication type for HTTP agent items:&lt;br&gt;0 - NONE (default)&lt;br&gt;1 - BASIC&lt;br&gt;2 - NTLM|Authentication type.&lt;br&gt;&lt;br&gt;Used only by SSH and HTTP agent items.|
| |username|\-|`string`| |Username for authentication.&lt;br&gt;Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.&lt;br&gt;&lt;br&gt;Required by SSH and Telnet items.&lt;br&gt;When used by JMX agent, password should also be specified together with the username or both properties should be left blank.|
| |password|\-|`string`| |Password for authentication.&lt;br&gt;Used by simple check, SSH, Telnet, database monitor, JMX and HTTP agent items.&lt;br&gt;&lt;br&gt;When used by JMX agent, username should also be specified together with the password or both properties should be left blank.|
| |publickey|\-|`string`| |Name of the public key file.&lt;br&gt;&lt;br&gt;Required for SSH agent items.|
| |privatekey|\-|`string`| |Name of the private key file.&lt;br&gt;&lt;br&gt;Required for SSH agent items.|
| |description|\-|`text`| |Item description.|
| |inventory\_link|\-|`string`|0 - NONE&lt;br&gt;&lt;br&gt;Capitalized host inventory field name. For example:&lt;br&gt;4 - ALIAS&lt;br&gt;6 - OS\_FULL&lt;br&gt;14 - HARDWARE&lt;br&gt;etc.|Host inventory field that is populated by the item.&lt;br&gt;&lt;br&gt;Refer to the [host inventory page](/manual/api/reference/host/object#host_inventory) for a list of supported host inventory fields and their IDs.|
| |logtimefmt|\-|`string`| |Format of the time in log entries.&lt;br&gt;Used only by log items.|
| |interface\_ref|\-|`string`|Format: `if&lt;N&gt;`|Reference to the host interface.|
| |jmx\_endpoint|\-|`string`| |JMX endpoint.&lt;br&gt;&lt;br&gt;Used only by JMX agent items.|
| |url|\-|`string`| |URL string.&lt;br&gt;&lt;br&gt;Required only for HTTP agent items.|
| |allow\_traps|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Allow to populate value as in a trapper item.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |follow\_redirects|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Follow HTTP response redirects while pooling data.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
|headers| |\-| | |Root element for HTTP(S) request headers, where header name is used as key and header value as value.&lt;br&gt;Used only by HTTP agent items.|
| |name|x|`string`| |Header name.|
| |value|x|`string`| |Header value.|
| |http\_proxy|\-|`string`| |HTTP(S) proxy connection string.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |output\_format|\-|`string`|0 - RAW (default)&lt;br&gt;1 - JSON|How to process response.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |post\_type|\-|`string`|0 - RAW (default)&lt;br&gt;2 - JSON&lt;br&gt;3 - XML|Type of post data body.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |posts|\-|`string`| |HTTP(S) request body data.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
|query\_fields| |\-| | |Root element for query parameters.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |name|x|`string`| |Parameter name.|
| |value|\-|`string`| |Parameter value.|
| |request\_method|\-|`string`|0 - GET (default)&lt;br&gt;1 - POST&lt;br&gt;2 - PUT&lt;br&gt;3 - HEAD|Request method.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |retrieve\_mode|\-|`string`|0 - BODY (default)&lt;br&gt;1 - HEADERS&lt;br&gt;2 - BOTH|What part of response should be stored.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |ssl\_cert\_file|\-|`string`| |Public SSL Key file path.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |ssl\_key\_file|\-|`string`| |Private SSL Key file path.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |ssl\_key\_password|\-|`string`| |Password for SSL Key file.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |status\_codes|\-|`string`| |Ranges of required HTTP status codes separated by commas. Supports user macros.&lt;br&gt;Example: 200,200-{$M},{$M},200-400&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |timeout|\-|`string`| |Item data polling request timeout. Supports user macros.&lt;br&gt;&lt;br&gt;Used by HTTP agent and Script items.|
| |verify\_host|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Validate if host name in URL is in Common Name field or a Subject Alternate Name field of host certificate.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
| |verify\_peer|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Validate if host certificate is authentic.&lt;br&gt;&lt;br&gt;Used only by HTTP agent items.|
|parameters| |\-| | |Root element for user-defined parameters.&lt;br&gt;&lt;br&gt;Used only by Script items.|
| |name|x|`string`| |Parameter name.&lt;br&gt;&lt;br&gt;Used only by Script items.|
| |value|\-|`string`| |Parameter value.&lt;br&gt;&lt;br&gt;Used only by Script items.|
|value map| |\-| | |Value map.|
| |name|x|`string`| |Name of the value map to use for the item.|
|preprocessing| |\-| | |Root element for item value preprocessing.|
|step| |\-| | |Individual item value preprocessing step.|
| |type|x|`string`|1 - MULTIPLIER&lt;br&gt;2 - RTRIM&lt;br&gt;3 - LTRIM&lt;br&gt;4 - TRIM&lt;br&gt;5 - REGEX&lt;br&gt;6 - BOOL\_TO\_DECIMAL&lt;br&gt;7 - OCTAL\_TO\_DECIMAL&lt;br&gt;8 - HEX\_TO\_DECIMAL&lt;br&gt;9 - SIMPLE\_CHANGE (calculated as (received value-previous value))&lt;br&gt;10 - CHANGE\_PER\_SECOND (calculated as (received value-previous value)/(time now-time of last check))&lt;br&gt;11 - XMLPATH&lt;br&gt;12 - JSONPATH&lt;br&gt;13 - IN\_RANGE&lt;br&gt;14 - MATCHES\_REGEX&lt;br&gt;15 - NOT\_MATCHES\_REGEX&lt;br&gt;16 - CHECK\_JSON\_ERROR&lt;br&gt;17 - CHECK\_XML\_ERROR&lt;br&gt;18 - CHECK\_REGEX\_ERROR&lt;br&gt;19 - DISCARD\_UNCHANGED&lt;br&gt;20 - DISCARD\_UNCHANGED\_HEARTBEAT&lt;br&gt;21 - JAVASCRIPT&lt;br&gt;22 - PROMETHEUS\_PATTERN&lt;br&gt;23 - PROMETHEUS\_TO\_JSON&lt;br&gt;24 - CSV\_TO\_JSON&lt;br&gt;25 - STR\_REPLACE&lt;br&gt;26 - CHECK\_NOT\_SUPPORTED&lt;br&gt;27 - XML\_TO\_JSON|Type of the item value preprocessing step.|
| |parameters|\-| | |Root element for parameters of the item value preprocessing step.|
| |parameter|x|`string`| |Individual parameter of the item value preprocessing step.|
| |error\_handler|\-|`string`|0 - ORIGINAL\_ERROR (default)&lt;br&gt;1 - DISCARD\_VALUE&lt;br&gt;2 - CUSTOM\_VALUE&lt;br&gt;3 - CUSTOM\_ERROR|Action type used in case of preprocessing step failure.|
| |error\_handler\_params|\-|`string`| |Error handler parameters used with 'error\_handler'.|
|master\_item| |\-| | |Individual item master item.&lt;br&gt;&lt;br&gt;Required by dependent items.|
| |key|x|`string`| |Dependent item master item key value.&lt;br&gt;&lt;br&gt;Recursion up to 3 dependent items and maximum count of dependent items equal to 29999 are allowed.|
|triggers| |\-| | |Root element for simple triggers.|
| |*For trigger element tag values, see host [trigger tags](/manual/xml_export_import/hosts#host_trigger_tags).*| | | | |
|tags| |\-| | |Root element for item tags.|
| |tag|x|`string`| |Tag name.|
| |value|\-|`string`| |Tag value.|</source>
      </trans-unit>
      <trans-unit id="19e8eae6" xml:space="preserve">
        <source>##### Host low-level discovery rule tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|--|--|-|--|----|--------|
|discovery\_rules| |\-| | |Root element for low-level discovery rules.|
| |*For most of the element tag values, see element tag values for a regular item. Only the tags that are specific to low-level discovery rules, are described below.*| | | | |
| |type|\-|`string`|0 - ZABBIX\_PASSIVE (default)&lt;br&gt;2 - TRAP&lt;br&gt;3 - SIMPLE&lt;br&gt;5 - INTERNAL&lt;br&gt;7 - ZABBIX\_ACTIVE&lt;br&gt;10 - EXTERNAL&lt;br&gt;11 - ODBC&lt;br&gt;12 - IPMI&lt;br&gt;13 - SSH&lt;br&gt;14 - TELNET&lt;br&gt;16 - JMX&lt;br&gt;18 - DEPENDENT&lt;br&gt;19 - HTTP\_AGENT&lt;br&gt;20 - SNMP\_AGENT|Item type.|
| |lifetime|\-|`string`|Default: 30d|Time period after which items that are no longer discovered will be deleted. Seconds, time unit with suffix or user macro.|
|filter| | | | |Individual filter.|
| |evaltype|\-|`string`|0 - AND\_OR (default)&lt;br&gt;1 - AND&lt;br&gt;2 - OR&lt;br&gt;3 - FORMULA|Logic to use for checking low-level discovery rule filter conditions.|
| |formula|\-|`string`| |Custom calculation formula for filter conditions.|
|conditions| |\-| | |Root element for filter conditions.|
| |macro|x|`string`| |Low-level discovery macro name.|
| |value|\-|`string`| |Filter value: regular expression or global regular expression.|
| |operator|\-|`string`|8 - MATCHES\_REGEX (default)&lt;br&gt;9 - NOT\_MATCHES\_REGEX|Condition operator.|
| |formulaid|x|`character`| |Arbitrary unique ID that is used to reference a condition from the custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|lld\_macro\_paths| |\-| | |Root element for LLD macro paths.|
| |lld\_macro|x|`string`| |Low-level discovery macro name.|
| |path|x|`string`| |Selector for value which will be assigned to the corresponding macro.|
|preprocessing| |\-| | |LLD rule value preprocessing.|
|step| |\-| | |Individual LLD rule value preprocessing step.|
| |*For most of the element tag values, see element tag values for a host item value preprocessing. Only the tags that are specific to low-level discovery value preprocessing, are described below.*| | | | |
| |type|x|`string`|5 - REGEX&lt;br&gt;11 - XMLPATH&lt;br&gt;12 - JSONPATH&lt;br&gt;15 - NOT\_MATCHES\_REGEX&lt;br&gt;16 - CHECK\_JSON\_ERROR&lt;br&gt;17 - CHECK\_XML\_ERROR&lt;br&gt;20 - DISCARD\_UNCHANGED\_HEARTBEAT&lt;br&gt;21 - JAVASCRIPT&lt;br&gt;23 - PROMETHEUS\_TO\_JSON&lt;br&gt;24 - CSV\_TO\_JSON&lt;br&gt;25 - STR\_REPLACE&lt;br&gt;27 - XML\_TO\_JSON|Type of the item value preprocessing step.|
|trigger\_prototypes| |\-| | |Root element for trigger prototypes.|
| |*For trigger prototype element tag values, see regular [host trigger](/manual/xml_export_import/hosts#host_trigger_tags) tags.*| | | | |
|graph\_prototypes| |\-| | |Root element for graph prototypes.|
| |*For graph prototype element tag values, see regular [host graph](/manual/xml_export_import/hosts#host_graph_tags) tags.*| | | | |
|host\_prototypes| |\-| | |Root element for host prototypes.|
| |*For host prototype element tag values, see regular [host](/manual/xml_export_import/hosts#host_tags) tags.*| | | | |
|item\_prototypes| |\-| | |Root element for item prototypes.|
| |*For item prototype element tag values, see regular [host item](/manual/xml_export_import/hosts#host_item_tags) tags.*| | | | |
|master\_item| |\-| | |Individual item prototype master item/item prototype data.|
| |key|x|`string`| |Dependent item prototype master item/item prototype key value.&lt;br&gt;&lt;br&gt;Required for a dependent item.|</source>
      </trans-unit>
      <trans-unit id="5ff5f79a" xml:space="preserve">
        <source>##### Host trigger tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|--|--|-|--|----|--------|
|triggers| |\-| | |Root element for triggers.|
| |expression|x|`string`| |Trigger expression.|
| |recovery\_mode|\-|`string`|0 - EXPRESSION (default)&lt;br&gt;1 - RECOVERY\_EXPRESSION&lt;br&gt;2 - NONE|Basis for generating OK events.|
| |recovery\_expression|\-|`string`| |Trigger recovery expression.|
| |correlation\_mode|\-|`string`|0 - DISABLED (default)&lt;br&gt;1 - TAG\_VALUE|Correlation mode (no event correlation or event correlation by tag).|
| |correlation\_tag|\-|`string`| |The tag name to be used for event correlation.|
| |name|x|`string`| |Trigger name.|
| |event_name|\-|`string`| |Event name.|
| |opdata|\-|`string`| |Operational data.|
| |url_name|\-|`string`| |Label for the URL associated with the trigger.|
| |url|\-|`string`| |URL associated with the trigger.|
| |status|\-|`string`|0 - ENABLED (default)&lt;br&gt;1 - DISABLED|Trigger status.|
| |priority|\-|`string`|0 - NOT\_CLASSIFIED (default)&lt;br&gt;1 - INFO&lt;br&gt;2 - WARNING&lt;br&gt;3 - AVERAGE&lt;br&gt;4 - HIGH&lt;br&gt;5 - DISASTER|Trigger severity.|
| |description|\-|`text`| |Trigger description.|
| |type|\-|`string`|0 - SINGLE (default)&lt;br&gt;1 - MULTIPLE|Event generation type (single problem event or multiple problem events).|
| |manual\_close|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Manual closing of problem events.|
|dependencies| |\-| | |Root element for dependencies.|
| |name|x|`string`| |Dependency trigger name.|
| |expression|x|`string`| |Dependency trigger expression.|
| |recovery\_expression|\-|`string`| |Dependency trigger recovery expression.|
|tags| |\-| | |Root element for event tags.|
| |tag|x|`string`| |Tag name.|
| |value|\-|`string`| |Tag value.|</source>
      </trans-unit>
      <trans-unit id="0a8a430a" xml:space="preserve">
        <source>##### Host graph tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|--|--|-|--|----|--------|
|graphs| |\-| | |Root element for graphs.|
| |name|x|`string`| |Graph name.|
| |width|\-|`integer`|20-65535 (default: 900)|Graph width, in pixels. Used for preview and for pie/exploded graphs.|
| |height|\-|`integer`|20-65535 (default: 200)|Graph height, in pixels. Used for preview and for pie/exploded graphs.|
| |yaxismin|\-|`double`|Default: 0|Value of Y axis minimum.&lt;br&gt;&lt;br&gt;Used if 'ymin\_type\_1' is FIXED.|
| |yaxismax|\-|`double`|Default: 0|Value of Y axis maximum.&lt;br&gt;&lt;br&gt;Used if 'ymax\_type\_1' is FIXED.|
| |show\_work\_period|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Highlight non-working hours.&lt;br&gt;&lt;br&gt;Used by normal and stacked graphs.|
| |show\_triggers|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Display simple trigger values as a line.&lt;br&gt;&lt;br&gt;Used by normal and stacked graphs.|
| |type|\-|`string`|0 - NORMAL (default)&lt;br&gt;1 - STACKED&lt;br&gt;2 - PIE&lt;br&gt;3 - EXPLODED|Graph type.|
| |show\_legend|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Display graph legend.|
| |show\_3d|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Enable 3D style.&lt;br&gt;&lt;br&gt;Used by pie and exploded pie graphs.|
| |percent\_left|\-|`double`|Default:0|Show the percentile line for left axis.&lt;br&gt;&lt;br&gt;Used only for normal graphs.|
| |percent\_right|\-|`double`|Default:0|Show the percentile line for right axis.&lt;br&gt;&lt;br&gt;Used only for normal graphs.|
| |ymin\_type\_1|\-|`string`|0 - CALCULATED (default)&lt;br&gt;1 - FIXED&lt;br&gt;2 - ITEM|Minimum value of Y axis.&lt;br&gt;&lt;br&gt;Used by normal and stacked graphs.|
| |ymax\_type\_1|\-|`string`|0 - CALCULATED (default)&lt;br&gt;1 - FIXED&lt;br&gt;2 - ITEM|Maximum value of Y axis.&lt;br&gt;&lt;br&gt;Used by normal and stacked graphs.|
|ymin\_item\_1| |\-| | |Individual item details.&lt;br&gt;&lt;br&gt;Required if 'ymin\_type\_1' is ITEM.|
| |host|x|`string`| |Item host.|
| |key|x|`string`| |Item key.|
|ymax\_item\_1| |\-| | |Individual item details.&lt;br&gt;&lt;br&gt;Required if 'ymax\_type\_1' is ITEM.|
| |host|x|`string`| |Item host.|
| |key|x|`string`| |Item key.|
|graph\_items| |x| | |Root element for graph items.|
| |sortorder|\-|`integer`| |Draw order. The smaller value is drawn first. Can be used to draw lines or regions behind (or in front of) another.|
| |drawtype|\-|`string`|0 - SINGLE\_LINE (default)&lt;br&gt;1 - FILLED\_REGION&lt;br&gt;2 - BOLD\_LINE&lt;br&gt;3 - DOTTED\_LINE&lt;br&gt;4 - DASHED\_LINE&lt;br&gt;5 - GRADIENT\_LINE|Draw style of the graph item.&lt;br&gt;&lt;br&gt;Used only by normal graphs.|
| |color|\-|`string`| |Element color (6 symbols, hex).|
| |yaxisside|\-|`string`|0 - LEFT (default)&lt;br&gt;1 - RIGHT|Side of the graph where the graph item's Y scale will be drawn.&lt;br&gt;&lt;br&gt;Used by normal and stacked graphs.|
| |calc\_fnc|\-|`string`|1 - MIN&lt;br&gt;2 - AVG (default)&lt;br&gt;4 - MAX&lt;br&gt;7 - ALL (minimum, average and maximum; used only by simple graphs)&lt;br&gt;9 - LAST (used only by pie and exploded pie graphs)|Data to draw if more than one value exists for an item.|
| |type|\-|`string`|0 - SIMPLE (default)&lt;br&gt;2 - GRAPH\_SUM (value of the item represents the whole pie; used only by pie and exploded pie graphs)|Graph item type.|
|item| |x| | |Individual item.|
| |host|x|`string`| |Item host.|
| |key|x|`string`| |Item key.|</source>
      </trans-unit>
      <trans-unit id="cae5eb15" xml:space="preserve">
        <source>##### Host web scenario tags

|Element|Element property|Required|Type|Range^**[1](#footnotes)**^|Description|
|--|--|-|--|----|--------|
|httptests| |\-| | |Root element for web scenarios.|
| |name|x|`string`| |Web scenario name.|
| |delay|\-|`string`|Default: 1m|Frequency of executing the web scenario. Seconds, time unit with suffix or user macro.|
| |attempts|\-|`integer`|1-10 (default: 1)|The number of attempts for executing web scenario steps.|
| |agent|\-|`string`|Default: Zabbix|Client agent. Zabbix will pretend to be the selected browser. This is useful when a website returns different content for different browsers.|
| |http\_proxy|\-|`string`| |Specify an HTTP proxy to use, using the format: `http://[username[:password]@]proxy.example.com[:port]`|
|variables| |\-| | |Root element for scenario-level variables (macros) that may be used in scenario steps.|
| |name|x|`text`| |Variable name.|
| |value|x|`text`| |Variable value.|
|headers| |\-| | |Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
| |name|x|`text`| |Header name.|
| |value|x|`text`| |Header value.|
| |status|\-|`string`|0 - ENABLED (default)&lt;br&gt;1 - DISABLED|Web scenario status.|
| |authentication|\-|`string`|0 - NONE (default)&lt;br&gt;1 - BASIC&lt;br&gt;2 - NTLM|Authentication method.|
| |http\_user|\-|`string`| |User name used for basic, HTTP or NTLM authentication.|
| |http\_password|\-|`string`| |Password used for basic, HTTP or NTLM authentication.|
| |verify\_peer|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Verify the SSL certificate of the web server.|
| |verify\_host|\-|`string`|0 - NO (default)&lt;br&gt;1 - YES|Verify that the Common Name field or the Subject Alternate Name field of the web server certificate matches.|
| |ssl\_cert\_file|\-|`string`| |Name of the SSL certificate file used for client authentication (must be in PEM format).|
| |ssl\_key\_file|\-|`string`| |Name of the SSL private key file used for client authentication (must be in PEM format).|
| |ssl\_key\_password|\-|`string`| |SSL private key file password.|
|steps| |x| | |Root element for web scenario steps.|
| |name|x|`string`| |Web scenario step name.|
| |url|x|`string`| |URL for monitoring.|
|query\_fields| |\-| | |Root element for query fields - an array of HTTP fields that will be added to the URL when performing a request.|
| |name|x|`string`| |Query field name.|
| |value|\-|`string`| |Query field value.|
|posts| |\-| | |HTTP POST variables as a string (raw post data) or as an array of HTTP fields (form field data).|
| |name|x|`string`| |Post field name.|
| |value|x|`string`| |Post field value.|
|variables| |\-| | |Root element of step-level variables (macros) that should be applied after this step.&lt;br&gt;&lt;br&gt;If the variable value has a 'regex:' prefix, then its value is extracted from the data returned by this step according to the regular expression pattern following the 'regex:' prefix|
| |name|x|`string`| |Variable name.|
| |value|x|`string`| |Variable value.|
|headers| |\-| | |Root element for HTTP headers that will be sent when performing a request. Headers should be listed using the same syntax as they would appear in the HTTP protocol.|
| |name|x|`string`| |Header name.|
| |value|x|`string`| |Header value.|
| |follow\_redirects|\-|`string`|0 - NO&lt;br&gt;1 - YES (default)|Follow HTTP redirects.|
| |retrieve\_mode|\-|`string`|0 - BODY (default)&lt;br&gt;1 - HEADERS&lt;br&gt;2 - BOTH|HTTP response retrieve mode.|
| |timeout|\-|`string`|Default: 15s|Timeout of step execution. Seconds, time unit with suffix or user macro.|
| |required|\-|`string`| |Text that must be present in the response. Ignored if empty.|
| |status\_codes|\-|`string`| |A comma delimited list of accepted HTTP status codes. Ignored if empty. For example: 200-201,210-299|
|tags| |\-| | |Root element for web scenario tags.|
| |tag|x|`string`| |Tag name.|
| |value|\-|`string`| |Tag value.|</source>
      </trans-unit>
      <trans-unit id="869bd76e" xml:space="preserve">
        <source>##### Footnotes

^**1**^ For string values, only the string will be exported (e.g.
"ZABBIX\_ACTIVE") without the numbering used in this table. The numbers
for range values (corresponding to the API values) in this table is used
for ordering only.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
