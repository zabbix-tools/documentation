[comment]: # translation:outdated

[comment]: # ({new-8a71c363})
# 2 Host groups

In the frontend host groups can be [exported](/manual/xml_export_import)
only with host export. When a host is exported
all groups it belongs to are exported with it automatically.

API allows to export host groups independently from hosts.

[comment]: # ({/new-8a71c363})

[comment]: # ({new-72d8feac})
##### Export format

``` {.yaml}
   host_groups:
    -
      uuid: 6f6799aa69e844b4b3918f779f2abf08
      name: 'Zabbix servers'
```

[comment]: # ({/new-72d8feac})

[comment]: # ({new-8b16d3d3})
#### Element tags

|Parameter|Type|Description|
|------|---|---------|
|uuid|*string*|Unique identifier for this host group. |
|name|*string*|Group name.|

[comment]: # ({/new-8b16d3d3})
