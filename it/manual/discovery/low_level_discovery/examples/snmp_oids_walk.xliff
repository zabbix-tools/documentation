<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/discovery/low_level_discovery/examples/snmp_oids_walk.md">
    <body>
      <trans-unit id="5508ac64" xml:space="preserve">
        <source># 4 Discovery of SNMP OIDs</source>
      </trans-unit>
      <trans-unit id="3680a9fa" xml:space="preserve">
        <source>#### Overview

In this section we will perform an SNMP
[discovery](/manual/discovery/low_level_discovery) on a switch.

This discovery method of SNMP OIDs has been supported since Zabbix server/proxy 6.4.</source>
      </trans-unit>
      <trans-unit id="afd80cd0" xml:space="preserve">
        <source>
#### Item key

Create an SNMP item, using the following item key in the *SNMP OID* field:

    walk[1.3.6.1.2.1.2.2.1.2,1.3.6.1.2.1.2.2.1.3]

![](../../../../../assets/en/manual/discovery/low_level_discovery/snmp_walk_item.png)

This item will perform an snmpwalk for the OIDs specified in the parameters (1.3.6.1.2.1.2.2.1.2, 1.3.6.1.2.1.2.2.1.3), returning a concatenated list of values, e.g.:

    .1.3.6.1.2.1.2.2.1.2.1 = STRING: "lo"
    .1.3.6.1.2.1.2.2.1.2.2 = STRING: "ens33"
    .1.3.6.1.2.1.2.2.1.2.3 = STRING: "ens37"
    .1.3.6.1.2.1.2.2.1.3.1 = INTEGER: 24
    .1.3.6.1.2.1.2.2.1.3.2 = INTEGER: 6
    .1.3.6.1.2.1.2.2.1.3.3 = INTEGER: 6</source>
      </trans-unit>
      <trans-unit id="3ca02a27" xml:space="preserve">
        <source>#### Dependent discovery rule

Go to the discovery rules of your template/host. Click on *Create discovery rule* in the upper right corner of the screen. 

Fill in the required details in the **Discovery rule** tab:

-   Select *Dependent item* as item type
-   Select the previously created SNMP walk item as the master item
-   Fill the name and key with meaningful values

![](../../../../../assets/en/manual/discovery/low_level_discovery/snmp_discovery_dep_item.png)

In the **Preprocessing** tab, select the *SNMP walk to JSON* preproccesing step.

![](../../../../../assets/en/manual/discovery/low_level_discovery/snmp_discovery_preprocessing.png){width="600"}

In the field name specify a valid LLD macro name. Select the corresponding OID path to discover values from.

This rule will discover entities and set:

*   {\#IFDESCR} macros to `lo`, `ens33`, and `ens37`;
*   {\#IFTYPE} macros set to `24`, `6`, and `6`.

A built-in macro *{\#SNMPINDEX}* containing the index of the
discovered OIDs is applied to discovered entities. The discovered
entities are grouped by *{\#SNMPINDEX}* macro value: **1**, **2** and
**3**:

``` {.java}
[
    {
        "{#SNMPINDEX}": "1",
        "{#IFDESCR}": "lo",
        "{#IFTYPE}": "24"
    },
    {
        "{#SNMPINDEX}": "2",
        "{#IFDESCR}": "ens33",
        "{#IFTYPE}": "6"
    },
    {
        "{#SNMPINDEX}": "3",
        "{#IFDESCR}": "ens37",
        "{#IFTYPE}": "6"
    }
]
```

If an entity does not have the specified OID, then the corresponding
macro will be omitted for this entity.</source>
      </trans-unit>
      <trans-unit id="bff9418f" xml:space="preserve">
        <source>#### Item, trigger, graph prototypes

Item prototypes must be created as **dependent** item prototypes, using macros from the discovery rule.

Dependent items will obtain their values from the `walk[]` master item. Thus it will not be necessary for each discovered item to query 
the SNMP device independently.

Trigger and graph prototypes may also be created also by using macros from the discovery rule.</source>
      </trans-unit>
      <trans-unit id="2be94533" xml:space="preserve">
        <source>#### Discovered entities

When server runs, it will create real dependent items, triggers and graphs based
on the values the SNMP discovery rule returns.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
