[comment]: # translation:outdated

[comment]: # ({new-7ef4b2b9})
# Best practices for secure Zabbix setup

[comment]: # ({/new-7ef4b2b9})

[comment]: # ({new-b9719f19})
#### Overview

This section contains best practices that should be observed in order to
set up Zabbix in a secure way.

The practices contained here are not required for the functioning of
Zabbix. They are recommended for better security of the system.

[comment]: # ({/new-b9719f19})

[comment]: # ({new-cebf8306})
### Access control

[comment]: # ({/new-cebf8306})

[comment]: # ({new-dc572a02})
#### Principle of least privilege

The principle of least privilege should be used at all times for Zabbix.
This principle means that user accounts (in Zabbix frontend) or process
user (for Zabbix server/proxy or agent) have only those privileges that
are essential to perform intended functions. In other words, user
accounts at all times should run with as few privileges as possible.

::: noteimportant
Giving extra permissions to 'zabbix' user will
allow it to access configuration files and execute operations that can
compromise the overall security of the infrastructure.
:::

When implementing the least privilege principle for user accounts,
Zabbix [frontend user
types](/manual/config/users_and_usergroups/permissions) should be taken
into account. It is important to understand that while a "Admin" user
type has less privileges than "Super Admin" user type, it has
administrative permissions that allow managing configuration and execute
custom scripts.

::: noteclassic
Some information is available even for non-privileged users.
For example, while *Administration* → *Scripts* is not available for
non-Super Admins, scripts themselves are available for retrieval by
using Zabbix API. Limiting script permissions and not adding sensitive
information (like access credentials, etc) should be used to avoid
exposure of sensitive information available in global
scripts.
:::

[comment]: # ({/new-dc572a02})

[comment]: # ({new-1631be73})
#### Secure user for Zabbix agent

In the default configuration, Zabbix server and Zabbix agent processes
share one 'zabbix' user. If you wish to make sure that the agent cannot
access sensitive details in server configuration (e.g. database login
information), the agent should be run as a different user:

1.  Create a secure user
2.  Specify this user in the agent [configuration
    file](/manual/appendix/config/zabbix_agentd) ('User' parameter)
3.  Restart the agent with administrator privileges. Privileges will be
    dropped to the specified user.

[comment]: # ({/new-1631be73})

[comment]: # ({new-40e70f9e})
#### UTF-8 encoding

UTF-8 is the only encoding supported by Zabbix. It is known to work
without any security flaws. Users should be aware that there are known
security issues if using some of the other encodings.

[comment]: # ({/new-40e70f9e})

[comment]: # ({new-62eeb2ec})
### Windows installer paths
When using Windows installers, it is recommended to use default paths provided by the installer as using custom paths without proper permissions could compromise the security of the installation.

[comment]: # ({/new-62eeb2ec})

[comment]: # ({new-f8f5054e})
#### Setting up SSL for Zabbix frontend

On RHEL/Centos, install mod\_ssl package:

    yum install mod_ssl

Create directory for SSL keys:

    mkdir -p /etc/httpd/ssl/private
    chmod 700 /etc/httpd/ssl/private

Create SSL certificate:

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/private/apache-selfsigned.key -out /etc/httpd/ssl/apache-selfsigned.crt

Fill out the prompts appropriately. The most important line is the one
that requests the Common Name. You need to enter the domain name that
you want to be associated with your server. You can enter the public IP
address instead if you do not have a domain name. We will use
*example.com* in this article.

    Country Name (2 letter code) [XX]:
    State or Province Name (full name) []:
    Locality Name (eg, city) [Default City]:
    Organization Name (eg, company) [Default Company Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (eg, your name or your server's hostname) []:example.com
    Email Address []:

Edit Apache SSL configuration:

    /etc/httpd/conf.d/ssl.conf

    DocumentRoot "/usr/share/zabbix"
    ServerName example.com:443
    SSLCertificateFile /etc/httpd/ssl/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/httpd/ssl/private/apache-selfsigned.key

Restart the Apache service to apply the changes:

    systemctl restart httpd.service

[comment]: # ({/new-f8f5054e})

[comment]: # ({new-40cfdec2})
### Web server hardening

[comment]: # ({/new-40cfdec2})

[comment]: # ({new-bb3706f4})
#### Enabling Zabbix on root directory of URL

Add a virtual host to Apache configuration and set permanent redirect
for document root to Zabbix SSL URL. Do not forget to replace
*example.com* with the actual name of the server.

    /etc/httpd/conf/httpd.conf

    #Add lines

    <VirtualHost *:*>
        ServerName example.com
        Redirect permanent / https://example.com
    </VirtualHost>

Restart the Apache service to apply the changes:

    systemctl restart httpd.service

[comment]: # ({/new-bb3706f4})

[comment]: # ({new-c7ee0bb2})
#### Enabling HTTP Strict Transport Security (HSTS) on the web server

To protect Zabbix frontend against protocol downgrade attacks, we
recommend to enable
[HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security)
policy on the web server.

For example, to enable HSTS policy for your Zabbix frontend in Apache
configuration:

    /etc/httpd/conf/httpd.conf

add the following directive to your virtual host's configuration:

    <VirtualHost *:443>
       Header set Strict-Transport-Security "max-age=31536000"
    </VirtualHost>

Restart the Apache service to apply the changes:

    systemctl restart httpd.service

[comment]: # ({/new-c7ee0bb2})

[comment]: # ({new-cd09dcd1})
#### Disabling web server information exposure

It is recommended to disable all web server signatures as part of the
web server hardening process. The web server is exposing software
signature by default:

![](../../../../assets/en/manual/installation/requirements/software_signature.png)

The signature can be disabled by adding two lines to the Apache (used as
an example) configuration file:

    ServerSignature Off
    ServerTokens Prod

PHP signature (X-Powered-By HTTP header) can be disabled by changing the
php.ini configuration file (signature is disabled by default):

    expose_php = Off

Web server restart is required for configuration file changes to be
applied.

Additional security level can be achieved by using the mod\_security
(package libapache2-mod-security2) with Apache. mod\_security allows to
remove server signature instead of only removing version from server
signature. Signature can be altered to any value by changing
"SecServerSignature" to any desired value after installing
mod\_security.

Please refer to documentation of your web server to find help on how to
remove/change software signatures.

[comment]: # ({/new-cd09dcd1})

[comment]: # ({new-720052da})
#### Disabling default web server error pages

It is recommended to disable default error pages to avoid information
exposure. Web server is using built-in error pages by default:

![](../../../../assets/en/manual/installation/requirements/error_page_text.png)

Default error pages should be replaced/removed as part of the web server
hardening process. The "ErrorDocument" directive can be used to define a
custom error page/text for Apache web server (used as an example).

Please refer to documentation of your web server to find help on how to
replace/remove default error pages.

[comment]: # ({/new-720052da})

[comment]: # ({new-ba1547c0})
#### Removing web server test page

It is recommended to remove the web server test page to avoid
information exposure. By default, web server webroot contains a test
page called index.html (Apache2 on Ubuntu is used as an example):

![](../../../../assets/en/manual/installation/requirements/test_page.png)

The test page should be removed or should be made unavailable as part of
the web server hardening process.

[comment]: # ({/new-ba1547c0})

[comment]: # ({new-7e4e4a45})
#### Zabbix settings

By default, Zabbix is configured with *X-Frame-Options HTTP response
header* set to `SAMEORIGIN`, meaning that content can only be loaded in
a frame that has the same origin as the page itself.

Zabbix frontend elements that pull content from external URLs (namely,
the URL [dashboard
widget](/manual/web_interface/frontend_sections/monitoring/dashboard/widgets#url))
display retrieved content in a sandbox with all sandboxing restrictions
enabled.

These settings enhance the security of the Zabbix frontend and provide
protection against XSS and clickjacking attacks. Super Admins can
[modify](/manual/web_interface/frontend_sections/administration/general#security)
*iframe sandboxing* and *X-Frame-Options HTTP response header*
parameters as needed. Please carefully weigh the risks and benefits
before changing default settings. Turning sandboxing or X-Frame-Options
off completely is not recommended.

[comment]: # ({/new-7e4e4a45})

[comment]: # ({new-c02c850f})
#### Zabbix Windows agent with OpenSSL

Zabbix Windows agent compiled with OpenSSL will try to reach the SSL
configuration file in c:\\openssl-64bit. The "openssl-64bit" directory
on disk C: can be created by non-privileged users.

So for security hardening, it is required to create this directory
manually and revoke write access from non-admin users.

Please note that the directory names will be different on 32-bit and
64-bit versions of Windows.

[comment]: # ({/new-c02c850f})

[comment]: # ({new-6cfe5a3b})
### Cryptography

[comment]: # ({/new-6cfe5a3b})


[comment]: # ({new-3f76ae70})
#### Hiding the file with list of common passwords

To increase the complexity of password brute force attacks, it is
suggested to limit access to the file `ui/data/top_passwords.txt` by
modifying web server configuration. This file contains a list of the
most common and context-specific passwords, and is used to prevent users
from setting such passwords if *Avoid easy-to-guess passwords* parameter
is enabled in the [password
policy](/manual/web_interface/frontend_sections/administration/authentication#internal_authentication).

For example, on NGINX file access can be limited by using the `location`
directive:

    location = /data/top_passwords.txt {​​​​​​​
        deny all;
        return 404;
    }​​​​​​​

On Apache - by using `.htacess` file:

    <Files "top_passwords.txt">  
      Order Allow,Deny
      Deny from all
    </Files>



[comment]: # ({/new-3f76ae70})

[comment]: # ({new-ba0d4036})

#### Security vulnerabilities

##### CVE-2021-42550

In Zabbix Java gateway with logback version 1.2.7 and prior versions, an attacker with the required 
privileges to edit configuration files could craft a malicious configuration allowing to execute 
arbitrary code loaded from LDAP servers.

Vulnerability to [CVE-2021-42550](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42550) 
has been fixed since Zabbix 5.4.9. However, as an additional security measure it is recommended 
to check permissions to the `/etc/zabbix/zabbix_java_gateway_logback.xml` file and set it read-only, 
if write permissions are available for the "zabbix" user. 

[comment]: # ({/new-ba0d4036})
