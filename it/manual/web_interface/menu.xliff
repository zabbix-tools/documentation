<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/web_interface/menu.md">
    <body>
      <trans-unit id="764560a7" xml:space="preserve">
        <source># 1 Menu</source>
      </trans-unit>
      <trans-unit id="4edaa9a7" xml:space="preserve">
        <source>### Overview

A vertical menu in a sidebar provides access to various Zabbix frontend
sections.

The menu is dark blue in the default theme.

![](../../../assets/en/manual/web_interface/vertical_menu.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="6ddbf7a4" xml:space="preserve">
        <source>### Working with the menu

A [global search](/manual/web_interface/global_search) box is located
below the Zabbix logo.

The menu can be collapsed or hidden completely:

-   To collapse, click on
    ![](../../../assets/en/manual/web_interface/button_collapse.png)
    next to Zabbix logo. In the collapsed menu only the icons are visible.

![](../../../assets/en/manual/web_interface/collapsed_menu.png)

-   To hide, click on
    ![](../../../assets/en/manual/web_interface/button_hide.png) next to
    Zabbix logo. In the hidden menu everything is hidden.

![](../../../assets/en/manual/web_interface/hidden_menu.png)</source>
      </trans-unit>
      <trans-unit id="3086e8b2" xml:space="preserve">
        <source>#### Collapsed menu

When the menu is collapsed to icons only, a full menu reappears as soon
as the mouse cursor is placed upon it. Note that it reappears over page
content; to move page content to the right you have to click on the
expand button. If the mouse cursor again is placed outside the full
menu, the menu will collapse again after two seconds.

You can also make a collapsed menu reappear fully by hitting the Tab
key. Hitting the Tab key repeatedly will allow to focus on the next menu
element.</source>
      </trans-unit>
      <trans-unit id="7dd192ee" xml:space="preserve">
        <source>#### Hidden menu

Even when the menu is hidden completely, a full menu is just one mouse
click away, by clicking on the burger icon. Note that it reappears over
page content; to move page content to the right you have to unhide the
menu by clicking on the show sidebar button.</source>
      </trans-unit>
      <trans-unit id="0d46829f" xml:space="preserve">
        <source>
#### Menu levels

There are up to three levels in the menu.

![](../../../assets/en/manual/web_interface/menu_levels.png)</source>
      </trans-unit>
      <trans-unit id="5ec4ea88" xml:space="preserve">
        <source>### Context menu

In addition to the main menu, Zabbix provides [host context menu](/manual/web_interface/menu/host_menu),
[item context menu](/manual/web_interface/menu/item_menu), and 
[event context menu](/manual/web_interface/menu/event_menu) for a quick access to and configuration of frequently used
entities, such as latest values, simple graphs, configuration form, related scripts or external links.
The context menus are accessible by clicking on the host, item or
problem/trigger name in supported locations (follow the links given above).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
