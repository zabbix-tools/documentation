[comment]: # translation:outdated

[comment]: # ({new-7b41e2d5})
# 1 Monitoring

[comment]: # ({/new-7b41e2d5})

[comment]: # ({new-c99a8958})
#### Overview

The Monitoring menu is all about displaying data. Whatever information
Zabbix is configured to gather, visualize and act upon, it will be
displayed in the various sections of the Monitoring menu.

[comment]: # ({/new-c99a8958})

[comment]: # ({new-e7927a8f})
#### View mode buttons

The following buttons located in the top right corner are common for
every section:

|   |   |
|---|---|
|![](../../../../assets/en/manual/web_interface/button_kiosk.png)|Display page in kiosk mode. In this mode only page content is displayed.<br>To exit kiosk mode, move the mouse cursor until the ![](../../../../assets/en/manual/web_interface/button_kiosk_leave.png) exit button appears and click on it. You will be taken back to normal mode.|

[comment]: # ({/new-e7927a8f})
