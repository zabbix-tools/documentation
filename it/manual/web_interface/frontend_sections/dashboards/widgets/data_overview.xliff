<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/web_interface/frontend_sections/dashboards/widgets/data_overview.md">
    <body>
      <trans-unit id="093ad1f3" xml:space="preserve">
        <source># 3 Data overview</source>
      </trans-unit>
      <trans-unit id="3c27cf5e" xml:space="preserve">
        <source>:::noteimportant
This widget is deprecated and will be removed in the upcoming major release.
:::</source>
      </trans-unit>
      <trans-unit id="fb44b8b3" xml:space="preserve">
        <source>#### Overview

In the data overview widget, you can display the latest data for a group
of hosts.

The color of problem items is based on the problem severity color, which
can be adjusted in the [problem
update](/manual/acknowledgment#updating_problems) screen.

By default, only values that fall within the last 24 hours are
displayed. This limit has been introduced with the aim of improving
initial loading times for large pages of latest data. This limit is
configurable in *Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max history display period* option.

Clicking on a piece of data offers links to some predefined graphs or
latest values.

Note that 50 records are displayed by default (configurable in
*Administration* → *General* →
*[GUI](/manual/web_interface/frontend_sections/administration/general#gui)*,
using the *Max number of columns and rows in overview tables* option).
If more records exist than are configured to display, a message is
displayed at the bottom of the table, asking to provide more specific
filtering criteria. There is no pagination. Note that this limit is applied 
first, before any further filtering of data, for example, by tags. </source>
      </trans-unit>
      <trans-unit id="f4866132" xml:space="preserve">
        <source>#### Configuration

To configure, select *Data overview* as type:

![](../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/data_overview.png){width="600"}

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*Host groups*|Select host groups.&lt;br&gt;This field is auto-complete so starting to type the name of a group will offer a dropdown of matching groups.&lt;br&gt;Scroll down to select. Click on 'x' to remove the selected.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Hosts*|Select hosts.&lt;br&gt;This field is auto-complete so starting to type the name of a host will offer a dropdown of matching hosts.&lt;br&gt;Scroll down to select. Click on 'x' to remove the selected.&lt;br&gt;This parameter is not available when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|
|*Item tags*|Specify tags to limit the number of item data displayed in the widget.&lt;br&gt;It is possible to include as well as exclude specific tags and tag values. Several conditions can be set. Tag name matching is always case-sensitive.&lt;br&gt;&lt;br&gt;There are several operators available for each condition:&lt;br&gt;**Exists** - include the specified tag names;&lt;br&gt;**Equals** - include the specified tag names and values (case-sensitive);&lt;br&gt;**Contains** - include the specified tag names where the tag values contain the entered string (substring match, case-insensitive);&lt;br&gt;**Does not exist** - exclude the specified tag names;&lt;br&gt;**Does not equal** - exclude the specified tag names and values (case-sensitive);&lt;br&gt;**Does not contain** - exclude the specified tag names where the tag values contain the entered string (substring match, case-insensitive).&lt;br&gt;&lt;br&gt;There are two calculation types for conditions:&lt;br&gt;**And/Or** - all conditions must be met, conditions having the same tag name will be grouped by the *Or* condition;&lt;br&gt;**Or** - enough if one condition is met.|
|*Show suppressed problems*|Mark the checkbox to display problems that would otherwise be suppressed (not shown) because of host maintenance.|
|*Hosts location*|Select hosts location - left or top.&lt;br&gt;This parameter is labeled *Host location* when configuring the widget on a [template dashboard](/manual/config/templates/template#adding-dashboards).|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
