[comment]: # translation:outdated

[comment]: # ({new-485672e6})
# 19 SLA report

[comment]: # ({/new-485672e6})

[comment]: # ({new-1a225286})
#### Overview

This widget is useful for displaying [SLA reports](/manual/web_interface/frontend_sections/services/sla_report). 
Functionally it is similar to the *Services* -> *SLA report* section.

[comment]: # ({/new-1a225286})

[comment]: # ({new-61be8294})
#### Configuration

To configure, select *SLA report* as type:

![](../../../../../../../assets/en/manual/web_interface/frontend_sections/monitoring/sla_report.png)

In addition to the parameters that are [common](/manual/web_interface/frontend_sections/dashboards/widgets#common-parameters) 
for all widgets, you may set the following specific options:

|   |   |
|--|--------|
|*SLA*|Select the SLA for the report.|
|*Service*|Select the service for the report.|
|*Show periods*|Set how many periods will be displayed in the widget (20 by default, 100 maximum).|
|*From*|Select the beginning date for the report.<br>Relative dates are supported: `now`, `now/d`, `now/w-1w` etc; supported date modifiers: d, w, M, y.|
|*To*|Select the end date for the report.<br>Relative dates are supported: `now`, `now/d`, `now/w-1w` etc; supported date modifiers: d, w, M, y.|

[comment]: # ({/new-61be8294})
