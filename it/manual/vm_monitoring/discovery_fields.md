[comment]: # aside: 2

[comment]: # ({new-e6df368c})
# 1 Virtual machine discovery key fields

The following table lists fields returned by virtual machine related
discovery keys.

|**Item key**|<|<|
|------------|-|-|
|**Description**|**Field**|**Retrieved content**|
|vmware.cluster.discovery|<|<|
|Performs cluster discovery.|{\#CLUSTER.ID}|Cluster identifier.|
|^|{\#CLUSTER.NAME}|Cluster name.|
|vmware.datastore.discovery|<|<|
|Performs datastore discovery.|{\#DATASTORE}|Datastore name.|
|vmware.dc.discovery|<|<|
|Performs datacenter discovery.|{\#DATACENTER}|Datacenter name.|
|^|{\#DATACENTERID}|Datacenter ID.|
|vmware.hv.discovery|<|<|
|Performs hypervisor discovery.|{\#HV.UUID}|Unique hypervisor identifier.|
|^|{\#HV.ID}|Hypervisor identifier (HostSystem managed object name).|
|^|{\#HV.NAME}|Hypervisor name.|
|^|{\#HV.NETNAME}|Hypervisor network host name.|
|^|{\#HV.IP}|Hypervisor IP address, might be empty.<br>In case of an HA configuration with multiple net interfaces, the following selection priority for interface is observed:<br>- prefer the IP which shares the IP-subnet with the vCenter IP<br>- prefer the IP from IP-subnet with default gateway<br>- prefer the IP from interface with the lowest ID<br>This field is supported since Zabbix 5.2.2.|
|^|{\#CLUSTER.NAME}|Cluster name, might be empty.|
|^|{\#DATACENTER.NAME}|Datacenter name.|
|^|{\#PARENT.NAME}|Name of container that stores the hypervisor.<br>Supported since Zabbix 4.0.3.|
|^|{\#PARENT.TYPE}|Type of container in which the hypervisor is stored. The values could be `Datacenter`, `Folder`, `ClusterComputeResource`, `VMware`, where 'VMware' stands for unknown container type.<br>Supported since Zabbix 4.0.3.|
|vmware.hv.datastore.discovery|<|<|
|Performs hypervisor datastore discovery. Note that multiple hypervisors can use the same datastore.|{\#DATASTORE}|Datastore name.|
|vmware.vm.discovery|<|<|
|Performs virtual machine discovery.|{\#VM.UUID}|Unique virtual machine identifier.|
|^|{\#VM.ID}|Virtual machine identifier (VirtualMachine managed object name).|
|^|{\#VM.NAME}|Virtual machine name.|
|^|{\#HV.NAME}|Hypervisor name.|
|^|{\#DATACENTER.NAME}|Datacenter name.|
|^|{\#CLUSTER.NAME}|Cluster name, might be empty.|
|^|{\#VM.IP}|Virtual machine IP address, might be empty.<br>Supported since Zabbix 5.2.2.|
|^|{\#VM.DNS}|Virtual machine DNS name, might be empty.<br>Supported since Zabbix 5.2.2.|
|^|{\#VM.GUESTFAMILY}|Guest virtual machine OS family, might be empty.<br>Supported since Zabbix 5.2.2.|
|^|{\#VM.GUESTFULLNAME}|Full guest virtual machine OS name, might be empty.<br>Supported since Zabbix 5.2.2.|
|^|{\#VM.FOLDER}|The chain of virtual machine parent folders, can be used as value for nested groups; folder names are combined with "/". Might be empty.<br>Supported since Zabbix 5.4.2.|
|vmware.vm.net.if.discovery|<|<|
|Performs virtual machine network interface discovery.|{\#IFNAME}|Network interface name.|
|vmware.vm.vfs.dev.discovery|<|<|
|Performs virtual machine disk device discovery.|{\#DISKNAME}|Disk device name.|
|vmware.vm.vfs.fs.discovery|<|<|
|Performs virtual machine file system discovery.|{\#FSNAME}|File system name.|

[comment]: # ({/new-e6df368c})

[comment]: # translation:outdated

