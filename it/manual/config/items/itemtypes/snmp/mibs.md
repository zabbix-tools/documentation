[comment]: # translation:outdated

[comment]: # ({new-8afcd82f})
# 3 MIB files

[comment]: # ({/new-8afcd82f})

[comment]: # ({new-f224bb08})
#### Introduction

MIB stands for a Management Information Base. MIB files allow you to use
textual representation of the OID (Object Identifier).

For example,

    ifHCOutOctets

is textual representation of OID

    1.3.6.1.2.1.31.1.1.1.10

You can use either, when monitoring SNMP devices with Zabbix, but if you
feel more comfortable when using textual representation you have to
install MIB files.

[comment]: # ({/new-f224bb08})

[comment]: # ({new-5aed9e1b})
#### Installing MIB files

On Debian-based systems:

    # apt install snmp-mibs-downloader
    # download-mibs

On RedHat-based systems:

    # yum install net-snmp-libs

[comment]: # ({/new-5aed9e1b})

[comment]: # ({new-5346f669})
#### Enabling MIB files

On RedHat-based systems the mib files should be enabled by default. On
Debian-based systems you have to edit file `/etc/snmp/snmp.conf` and
comment out the line that says `mibs :`

    # As the snmp packages come without MIB files due to license reasons, loading
    # of MIBs is disabled by default. If you added the MIBs you can re-enable
    # loading them by commenting out the following line.
    #mibs :

[comment]: # ({/new-5346f669})

[comment]: # ({new-bb4c3c1f})
#### Testing MIB files

Testing snmp MIBs can be done using `snmpwalk` utility. If you don't
have it installed, use the following instructions.

On Debian-based systems:

    # apt install snmp

On RedHat-based systems:

    # yum install net-snmp-utils

After that, the following command must not give error when you query a
network device:

    $ snmpwalk -v 2c -c public <NETWORK DEVICE IP> ifInOctets
    IF-MIB::ifInOctets.1 = Counter32: 176137634
    IF-MIB::ifInOctets.2 = Counter32: 0
    IF-MIB::ifInOctets.3 = Counter32: 240375057
    IF-MIB::ifInOctets.4 = Counter32: 220893420
    [...]

[comment]: # ({/new-bb4c3c1f})

[comment]: # ({new-87dbd7d4})
#### Using MIBs in Zabbix

The most important to keep in mind is that Zabbix processes do not get
informed of the changes made to MIB files. So after every change you
must restart Zabbix server or proxy, e. g.:

    # service zabbix-server restart

After that, the changes made to MIB files are in effect.

[comment]: # ({/new-87dbd7d4})

[comment]: # ({new-efd836b8})
#### Using custom MIB files

There are standard MIB files coming with every GNU/Linux distribution.
But some device vendors provide their own.

Let's say, you would like to use
[CISCO-SMI](ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my) MIB file. The
following instructions will download and install it:

    # wget ftp://ftp.cisco.com/pub/mibs/v2/CISCO-SMI.my -P /tmp
    # mkdir -p /usr/local/share/snmp/mibs
    # grep -q '^mibdirs +/usr/local/share/snmp/mibs' /etc/snmp/snmp.conf 2>/dev/null || echo "mibdirs +/usr/local/share/snmp/mibs" >> /etc/snmp/snmp.conf
    # cp /tmp/CISCO-SMI.my /usr/local/share/snmp/mibs

Now you should be able to use it. Try to translate the name of the
object *ciscoProducts* from the MIB file to OID:

    # snmptranslate -IR -On CISCO-SMI::ciscoProducts
    .1.3.6.1.4.1.9.1

If you receive errors instead of the OID, ensure all the previous
commands did not return any errors.

The object name translation worked, you are ready to use custom MIB
file. Note the MIB name prefix (*CISCO-SMI::*) used in the query. You
will need this when using command-line tools as well as Zabbix.

Don't forget to restart Zabbix server/proxy before using this MIB file
in Zabbix.

::: noteimportant
Keep in mind that MIB files can have dependencies.
That is, one MIB may require another. In order to satisfy these
dependencies you have to install all the affected MIB
files.
:::

[comment]: # ({/new-efd836b8})
