[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-22d75789})
# 1 Zabbix agent

[comment]: # ({/new-22d75789})

[comment]: # ({new-d63c67a4})
#### Overview

These checks use the communication with Zabbix agent for data gathering.

There are [passive and active](/manual/appendix/items/activepassive)
agent checks. When configuring an item, you can select the required
type:

-   *Zabbix agent* - for passive checks
-   *Zabbix agent (active)* - for active checks

[comment]: # ({/new-d63c67a4})

[comment]: # ({new-cea3fc45})

### Supported platforms

Except where specified differently in the item comments, the agent items (and all parameters) are supported on: 

-   **Linux**
-   **FreeBSD**
-   **Solaris**
-   **HP-UX**
-   **AIX**
-   **Tru64**
-   **MacOS X**
-   **OpenBSD**
-   **NetBSD**

Many agent items are also supported on **Windows**. See the [Windows agent item](/manual/config/items/itemtypes/zabbix_agent/win_keys) 
page for details.

[comment]: # ({/new-cea3fc45})

[comment]: # ({new-3077d649})
### Item key details

Parameters without angle brackets are mandatory. Parameters marked with angle brackets **<** **>** are optional.

[comment]: # ({/new-3077d649})

[comment]: # ({new-20fc641c})
##### kernel.maxfiles

<br>
The maximum number of opened files supported by OS.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-20fc641c})

[comment]: # ({new-6dbdc04a})
##### kernel.maxproc

<br>
The maximum number of processes supported by OS.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux 2.6 and later, FreeBSD, Solaris, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-6dbdc04a})

[comment]: # ({new-9f31067e})
##### kernel.openfiles

<br>
The number of currently open file descriptors.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux (the item may work on other UNIX-like platforms).

[comment]: # ({/new-9f31067e})

[comment]: # ({new-f0ef2c24})

##### log[file,<regexp>,<encoding>,<maxlines>,<mode>,<output>,<maxdelay>,<options>,<persistent_dir>]

<br>
The monitoring of a log file.<br>
Return value: *Log*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path and name of a log file;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxlines** - the maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd);<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items);<br>**output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - additional options:<br>*mtime-noreread* - non-unique records, reread only if the file size changes (ignore modification time change). (This parameter is deprecated since 5.0.2, because now mtime is ignored.)<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   If the file is missing or permissions do not allow access, the item turns unsupported;
-   If `output` is left empty - the whole line containing the matched text is returned. Note that all global regular expression types except 'Result is TRUE' always return the whole matched line and the `output` parameter is ignored.
-   Content extraction using the `output` parameter takes place on the agent.

Examples:

    log[/var/log/syslog]
    log[/var/log/syslog,error]
    log[/home/zabbix/logs/logfile,,,100]
    
Example of using the `output` parameter for extracting a number from log record:

    log[/app1/app.log,"task run [0-9.]+ sec, processed ([0-9]+) records, [0-9]+ errors",,,,\1] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send only '6080' to server. Because a numeric value is being sent, the "Type of information" for this item can be set to "Numeric (unsigned)" and the value can be used in graphs, triggers etc.

Example of using the `output` parameter for rewriting a log record before sending to server:

    log[/app1/app.log,"([0-9 :-]+) task run ([0-9.]+) sec, processed ([0-9]+) records, ([0-9]+) errors",,,,"\1 RECORDS: \3, ERRORS: \4, DURATION: \2"] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send a modified record "2015-11-13 10:08:26 RECORDS: 6080, ERRORS: 0, DURATION: 6.08" to the server.

[comment]: # ({/new-f0ef2c24})

[comment]: # ({new-0cc87dbe})

##### log.count[file,<regexp>,<encoding>,<maxproclines>,<mode>,<maxdelay>,<options>,<persistent_dir>]

<br>
The count of matched lines in a monitored log file.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path and name of log file;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxproclines** - the maximum number of new lines per second the agent will analyze (cannot exceed 10000). The default value is 10\*'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - additional options:<br>*mtime-noreread* - non-unique records, reread only if the file size changes (ignore modification time change). (This parameter is deprecated since 5.0.2, because now mtime is ignored.)<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   Matching lines are counted in the new lines since the last log check by the agent, and thus depend on the item update interval;
-   If the file is missing or permissions do not allow access, the item turns unsupported.

[comment]: # ({/new-0cc87dbe})

[comment]: # ({new-95734be0})
##### logrt[file_regexp,<regexp>,<encoding>,<maxlines>,<mode>,<output>,<maxdelay>,<options>,<persistent_dir>]

<br>
The monitoring of a log file that is rotated.<br>
Return value: *Log*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file\_regexp** - the absolute path to file and the file name described by a regular [expression](/manual/regular_expressions#overview). *Note* that only the file name is a regular expression.<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required content pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxlines** - the maximum number of new lines per second the agent will send to Zabbix server or proxy. This parameter overrides the value of 'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items).<br>
-   **output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - the type of log file rotation and other options. Possible values:<br>*rotate* (default),<br>*copytruncate* - note that *copytruncate* cannot be used together with *maxdelay*. In this case *maxdelay* must be 0 or not specified; see [copytruncate](log_items#notes_on_handling_copytruncate_log_file_rotation) notes,<br>*mtime-reread* - non-unique records, reread if modification time or size changes (default),<br>*mtime-noreread* - non-unique records, reread only if the size changes (ignore modification time change).<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   Log rotation is based on the last modification time of files;
-   Note that logrt is designed to work with one currently active log file, with several other matching inactive files rotated. If, for example, a directory has many active log files, a separate logrt item should be created for each one. Otherwise if one logrt item picks up too many files it may lead to exhausted memory and a crash of monitoring.
-   If `output` is left empty - the whole line containing the matched text is returned. Note that all global regular expression types except 'Result is TRUE' always return the whole matched line and the `output` parameter is ignored.
-   Content extraction using the `output` parameter takes place on the agent.

Examples:

    logrt["/home/zabbix/logs/^logfile[0-9]{1,3}$",,,100] #this item will match a file like "logfile1" (will not match ".logfile1")
    logrt["/home/user/^logfile_.*_[0-9]{1,3}$","pattern_to_match","UTF-8",100] #this item will collect data from files such "logfile_abc_1" or "logfile__001"
    
Example of using the `output` parameter for extracting a number from log record:

    logrt[/app1/^test.*log$,"task run [0-9.]+ sec, processed ([0-9]+) records, [0-9]+ errors",,,,\1] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send only '6080' to server. Because a numeric value is being sent, the "Type of information" for this item can be set to "Numeric (unsigned)" and the value can be used in graphs, triggers etc.
    
Example of using the `output` parameter for rewriting a log record before sending to server:

    logrt[/app1/^test.*log$,"([0-9 :-]+) task run ([0-9.]+) sec, processed ([0-9]+) records, ([0-9]+) errors",,,,"\1 RECORDS: \3, ERRORS: \4, DURATION: \2"] #this item will match a log record "2015-11-13 10:08:26 task run 6.08 sec, processed 6080 records, 0 errors" and send a modified record "2015-11-13 10:08:26 RECORDS: 6080, ERRORS: 0, DURATION: 6.08" to server. |

[comment]: # ({/new-95734be0})

[comment]: # ({new-8ad8a2af})

##### logrt.count[file_regexp,<regexp>,<encoding>,<maxproclines>,<mode>,<maxdelay>,<options>,<persistent_dir>]

<br>
The count of matched lines in a monitored log file that is rotated.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file_regexp** - the absolute path to file and regular [expression](/manual/regular_expressions#overview) describing the file name pattern;<br>
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;<br>
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);<br>
-   **maxproclines** - the maximum number of new lines per second the agent will analyze (cannot exceed 10000). The default value is 10\*'MaxLinesPerSecond' in [zabbix\_agentd.conf](/manual/appendix/config/zabbix_agentd).<br>
-   **mode** - possible values: *all* (default) or *skip* - skip processing of older data (affects only newly created items).<br>
-   **maxdelay** - the maximum delay in seconds. Type: float. Values: 0 - (default) never ignore log file lines; > 0.0 - ignore older lines in order to get the most recent lines analyzed within "maxdelay" seconds. Read the [maxdelay](log_items#using_maxdelay_parameter) notes before using it!<br>
-   **options** - the type of log file rotation and other options. Possible values:<br>*rotate* (default),<br>*copytruncate* - note that *copytruncate* cannot be used together with *maxdelay*. In this case *maxdelay* must be 0 or not specified; see [copytruncate](log_items#notes_on_handling_copytruncate_log_file_rotation) notes,<br>*mtime-reread* - non-unique records, reread if modification time or size changes (default),<br>*mtime-noreread* - non-unique records, reread only if the size changes (ignore modification time change).<br>
-   **persistent\_dir** (only in zabbix\_agentd on Unix systems; not supported in Zabbix agent 2) - the absolute pathname of directory where to store persistent files. See also additional notes on [persistent files](log_items#notes-on-persistent-files-for-log-items).

Comments:

-   The item must be configured as an [active check](/manual/appendix/items/activepassive#active_checks);
-   Matching lines are counted in the new lines since the last log check by the agent, and thus depend on the item update interval;
-   Log rotation is based on the last modification time of files..

[comment]: # ({/new-8ad8a2af})

[comment]: # ({new-518a2dd0})

##### modbus.get[endpoint,<slave id>,<function>,<address>,<count>,<type>,<endianness>,<offset>]

<br>
Reads Modbus data.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux. 

Parameters:

-   **endpoint** - the endpoint defined as `protocol://connection_string`;<br>
-   **slave id** - the slave ID;<br>
-   **function** - the Modbus function;<br>
-   **address** - the address of first registry, coil or input;<br>
-   **count** - the number of records to read;<br>
-   **type** - the type of data;<br>
-   **endianness** - the endianness configuration;<br>
-   **offset** - the number of registers, starting from 'address', the results of which will be discarded.

See a [detailed description](/manual/appendix/items/modbus) of parameters.

[comment]: # ({/new-518a2dd0})

[comment]: # ({new-ba7da5e1})

##### net.dns[<ip>,name,<type>,<timeout>,<count>,<protocol>]

<br>
Checks if the DNS service is up.<br>
Return values: 0 - DNS is down (server did not respond or DNS resolution failed); 1 - DNS is up.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP address of DNS server (leave empty for the default DNS server, ignored on Windows unless using Zabbix agent 2);
-   **name** - the DNS name to query;
-   **type** - the record type to be queried (default is *SOA*);
-   **timeout** (ignored on Windows unless using Zabbix agent 2) - the timeout for the request in seconds (default is 1 second);
-   **count** (ignored on Windows unless using Zabbix agent 2) - the number of tries for the request (default is 2);
-   **protocol** - the protocol used to perform DNS queries: *udp* (default) or *tcp*.

Comments:

-   The possible values for `type` are: *ANY*, *A*, *NS*, *CNAME*, *MB*, *MG*, *MR*, *PTR*, *MD*, *MF*, *MX*, *SOA*, *NULL*, *WKS* (not supported for Zabbix agent on Windows, Zabbix agent 2 on all OS), *HINFO*, *MINFO*, *TXT*, *SRV*
-   Internationalized domain names are not supported, please use IDNA encoded names instead.

Example:

    net.dns[198.51.100.1,example.com,MX,2,1]

[comment]: # ({/new-ba7da5e1})

[comment]: # ({new-d5dc268a})

##### net.dns.record[<ip>,name,<type>,<timeout>,<count>,<protocol>]

<br>
Performs a DNS query.<br>
Return value: a character string with the required type of information.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP address of DNS server (leave empty for the default DNS server, ignored on Windows unless using Zabbix agent 2);
-   **name** - the DNS name to query;
-   **type** - the record type to be queried (default is *SOA*);
-   **timeout** (ignored on Windows unless using Zabbix agent 2) - the timeout for the request in seconds (default is 1 second);
-   **count** (ignored on Windows unless using Zabbix agent 2) - the number of tries for the request (default is 2);
-   **protocol** - the protocol used to perform DNS queries: *udp* (default) or *tcp*.

Comments:

-   The possible values for `type` are:<br>*ANY*, *A*, *NS*, *CNAME*, *MB*, *MG*, *MR*, *PTR*, *MD*, *MF*, *MX*, *SOA*, *NULL*, *WKS* (not supported for Zabbix agent on Windows, Zabbix agent 2 on all OS), *HINFO*, *MINFO*, *TXT*, *SRV*
-   Internationalized domain names are not supported, please use IDNA encoded names instead.

Example:

    net.dns.record[198.51.100.1,example.com,MX,2,1]

[comment]: # ({/new-d5dc268a})

[comment]: # ({new-1e07380f})

##### net.if.collisions[if]

<br>
The number of out-of-window collisions.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameter:

-   **if** - network interface name

[comment]: # ({/new-1e07380f})

[comment]: # ({new-8911d171})

##### net.if.discovery

<br>
The list of network interfaces. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, OpenBSD, NetBSD.

[comment]: # ({/new-8911d171})

[comment]: # ({new-e53ce465})

##### net.if.in[if,<mode>]

<br>
The incoming traffic statistics on a network interface.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[5](#footnotes)**^, HP-UX, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameters:

-   **if** - network interface name (Unix); network interface full description or IPv4 address; or, if in braces, network interface GUID (Windows);
-   **mode** - possible values:<br>*bytes* - number of bytes (default)<br>*packets* - number of packets<br>*errors* - number of errors<br>*dropped* - number of dropped packets<br>*overruns (fifo)* - the number of FIFO buffer errors<br>*frame* - the number of packet framing errors<br>*compressed* - the number of compressed packets transmitted or received by the device driver<br>*multicast* - the number of multicast frames received by the device driver

Comments:

-   You may use this key with the *Change per second* preprocessing step in order to get the bytes-per-second statistics;
-   The *dropped* mode is supported only on Linux, FreeBSD, HP-UX, MacOS X, OpenBSD, NetBSD;
-   The *overruns*, *frame*, *compressed*, *multicast* modes are supported only on Linux;
-   On HP-UX this item does not provide details on loopback interfaces (e.g. lo0).

Examples:

    net.if.in[eth0]
    net.if.in[eth0,errors]

[comment]: # ({/new-e53ce465})

[comment]: # ({new-ab67e043})

##### net.if.out[if,<mode>]

<br>
The outgoing traffic statistics on a network interface.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[5](#footnotes)**^, HP-UX, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameters:

-   **if** - network interface name (Unix); network interface full description or IPv4 address; or, if in braces, network interface GUID (Windows);
-   **mode** - possible values:<br>*bytes* - number of bytes (default)<br>*packets* - number of packets<br>*errors* - number of errors<br>*dropped* - number of dropped packets<br>*overruns (fifo)* - the number of FIFO buffer errors<br>*collisions (colls)* - the number of collisions detected on the interface<br>*carrier* - the number of carrier losses detected by the device driver<br>*compressed* - the number of compressed packets transmitted by the device driver

Comments:

-   You may use this key with the *Change per second* preprocessing step in order to get the bytes-per-second statistics;
-   The *dropped* mode is supported only on Linux, HP-UX;
-   The *overruns*, *collision*, *carrier*, *compressed* modes are supported only on Linux;
-   On HP-UX this item does not provide details on loopback interfaces (e.g. lo0).

Examples:

    net.if.out[eth0]
    net.if.out[eth0,errors]

[comment]: # ({/new-ab67e043})

[comment]: # ({new-3c96d291})

##### net.if.total[if,<mode>]

<br>
The sum of incoming and outgoing traffic statistics on a network interface.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[5](#footnotes)**^, HP-UX, AIX, MacOS X, OpenBSD, NetBSD. Root privileges are required on NetBSD.

Parameters:

-   **if** - network interface name (Unix); network interface full description or IPv4 address; or, if in braces, network interface GUID (Windows);
-   **mode** - possible values:<br>*bytes* - number of bytes (default)<br>*packets* - number of packets<br>*errors* - number of errors<br>*dropped* - number of dropped packets<br>*overruns (fifo)* - the number of FIFO buffer errors<br>*collisions (colls)* - the number of collisions detected on the interface<br>*compressed* - the number of compressed packets transmitted or received by the device driver

Comments:

-   You may use this key with the *Change per second* preprocessing step in order to get the bytes-per-second statistics;
-   The *dropped* mode is supported only on Linux, HP-UX. Dropped packets are supported only if both `net.if.in` and `net.if.out` work for dropped packets on your platform.
-   The *overruns*, *collision*, *compressed* modes are supported only on Linux;
-   On HP-UX this item does not provide details on loopback interfaces (e.g. lo0).

Examples:

    net.if.total[eth0]
    net.if.total[eth0,errors]

[comment]: # ({/new-3c96d291})

[comment]: # ({new-a72f9721})

##### net.tcp.listen[port]

<br>
Checks if this TCP port is in LISTEN state.<br>
Return values: 0 - it is not in LISTEN state; 1 - it is in LISTEN state.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, MacOS X.

Parameter:

-   **port** - TCP port number

On Linux kernels 2.6.14 and above, the information about listening TCP sockets is obtained from the kernel's NETLINK interface, if possible. Otherwise, the information is retrieved from /proc/net/tcp and /roc/net/tcp6 files.

Example:

    net.tcp.listen[80]

[comment]: # ({/new-a72f9721})

[comment]: # ({new-000f516d})

##### net.tcp.port[<ip>,port]

<br>
Checks if it is possible to make a TCP connection to the specified port.<br>
Return values: 0 - cannot connect; 1 - can connect.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP address or DNS name (default is 127.0.0.1);
-   **port** - the port number.

Comments:

-   For simple TCP performance testing use `net.tcp.service.perf[tcp,<ip>,<port>]`;
-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually).

Example:

    net.tcp.port[,80] #this item can be used to test the web server availability running on port 80

[comment]: # ({/new-000f516d})

[comment]: # ({new-11ca536e})

##### net.tcp.service[service,<ip>,<port>]

<br>
Checks if a service is running and accepting TCP connections.<br>
Return values: 0 - service is down; 1 - service is running.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, or *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   These checks may result in additional messages in system daemon logfiles (SMTP and SSH sessions being logged usually);
-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.port[]` for checks like these.
-   Checking of LDAP and HTTPS on Windows is only supported by Zabbix agent 2;
-   The telnet check looks for a login prompt (':' at the end);
-   See also [known issues](/manual/installation/known_issues#https_checks) of checking the HTTPS service.

Example:

    net.tcp.service[ftp,,45] #this item can be used to test the availability of FTP server on TCP port 45

[comment]: # ({/new-11ca536e})

[comment]: # ({new-49a8f9ac})

##### net.tcp.service.perf[service,<ip>,<port>]

<br>
Checks the performance of a TCP service.<br>
Return values: 0 - service is down; seconds - the number of seconds spent while connecting to the service.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ssh*, *ldap*, *smtp*, *ftp*, *http*, *pop*, *nntp*, *imap*, *tcp*, *https*, or *telnet* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Comments:

-   Checking of encrypted protocols (like IMAP on port 993 or POP on port 995) is currently not supported. As a workaround, please use `net.tcp.service.perf[tcp,<ip>,<port>]` for checks like these.
-   The telnet check looks for a login prompt (':' at the end);
-   See also [known issues](/manual/installation/known_issues#https_checks) of checking the HTTPS service.

Example:

    net.tcp.service.perf[ssh] #this item can be used to test the speed of initial response from the SSH server

[comment]: # ({/new-49a8f9ac})

[comment]: # ({new-29fba160})

##### net.tcp.socket.count[<laddr>,<lport>,<raddr>,<rport>,<state>]

<br>
Returns the number of TCP sockets that match parameters.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **laddr** - the local IPv4/6 address or CIDR subnet;
-   **lport** - the local port number or service name;
-   **raddr** - the remote IPv4/6 address or CIDR subnet;
-   **rport** - the remote port number or service name;
-   **state** - the connection state (*established*, *syn\_sent*, *syn\_recv*, *fin\_wait1*, *fin\_wait2*, *time\_wait*, *close*, *close\_wait*, *last\_ack*, *listen*, *closing*).

Example:

    net.tcp.socket.count[,80,,,established] #check if the local TCP port 80 is in "established" state

[comment]: # ({/new-29fba160})

[comment]: # ({new-b9f198f7})

##### net.udp.listen[port]

<br>
Checks if this UDP port is in LISTEN state.<br>
Return values: 0 - it is not in LISTEN state; 1 - it is in LISTEN state.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, MacOS X.

Parameter:

-   **port** - UDP port number

Example:

    net.udp.listen[68]

[comment]: # ({/new-b9f198f7})

[comment]: # ({new-7c60892f})

##### net.udp.service[service,<ip>,<port>]

<br>
Checks if a service is running and responding to UDP requests.<br>
Return values: 0 - service is down; 1 - service is running.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service[ntp,,45] #this item can be used to test the availability of NTP service on UDP port 45

[comment]: # ({/new-7c60892f})

[comment]: # ({new-8697ef9c})

##### net.udp.service.perf[service,<ip>,<port>]

<br>
Checks the performance of a UDP service.<br>
Return values: 0 - service is down; seconds - the number of seconds spent waiting for response from the service.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **service** - *ntp* (see [details](/manual/appendix/items/service_check_details));
-   **ip** - the IP address (default is 127.0.0.1);
-   **port** - the port number (by default the standard service port number is used).

Example:

    net.udp.service.perf[ntp] #this item can be used to test response time from NTP service

[comment]: # ({/new-8697ef9c})

[comment]: # ({new-25a2206c})

##### net.udp.socket.count[<laddr>,<lport>,<raddr>,<rport>,<state>]

<br>
Returns the number of UDP sockets that match parameters.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **laddr** - the local IPv4/6 address or CIDR subnet;
-   **lport** - the local port number or service name;
-   **raddr** - the remote IPv4/6 address or CIDR subnet;
-   **rport** - the remote port number or service name;
-   **state** - the connection state (*established*, *unconn*).

Example:

    net.udp.socket.count[,,,,listening] → check if any UDP socket is in "listening" state

[comment]: # ({/new-25a2206c})

[comment]: # ({new-4c3ddb55})

##### proc.cpu.util[<name>,<user>,<type>,<cmdline>,<mode>,<zone>]

<br>
The process CPU utilization percentage.<br>
Return value: *Float*.<br>
[Supported platforms](#supported-platforms): Linux, Solaris^**[6](#footnotes)**^.

Parameters:

-   **name** - the process name (default is *all processes*);
-   **user** - the user name (default is *all users*);
-   **type** - the CPU utilization type: *total* (default), *user*, or *system*;
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview));
-   **mode** - the data gathering mode: *avg1* (default), *avg5*, or *avg15*;
-   **zone** - the target zone: *current* (default) or *all*. This parameter is supported on Solaris only.

Comments:

-   The returned value is based on a single CPU core utilization percentage. For example, the CPU utilization of a process fully using two cores is 200%.
-   The process CPU utilization data is gathered by a collector which supports the maximum of 1024 unique (by name, user and command line) queries. Queries not accessed during the last 24 hours are removed from the collector.
-   When setting the `zone` parameter to *current* (or default) in case the agent has been compiled on a Solaris without zone support, but running on a newer Solaris where zones are supported, then the agent will return NOTSUPPORTED (the agent cannot limit results to only the current zone). However, *all* is supported in this case.

Examples:

    proc.cpu.util[,root] #CPU utilization of all processes running under the "root" user
    proc.cpu.util[zabbix_server,zabbix] #CPU utilization of all zabbix_server processes running under the zabbix user

[comment]: # ({/new-4c3ddb55})

[comment]: # ({new-02480d9a})

##### proc.get[<name>,<user>,<cmdline>,<mode>]

<br>
The list of OS processes and their parameters. Can be used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Windows, OpenBSD, NetBSD.

Parameters:

-   **name** - the process name (default *all processes*);
-   **user** - the user name (default *all users*);
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview)). This parameter is not supported for Windows; on other platforms it is not supported if mode is set to 'summary'.
-   **mode** - possible values:<br>*process* (default), *thread* (not supported for NetBSD), *summary*. See a list of [process parameters](/manual/appendix/items/proc_get) returned for each mode and OS.

Comments:

-   If a value cannot be retrieved, for example, because of an error (process already died, lack of permissions, system call failure), `-1` will be returned;
-   See [notes](/manual/appendix/items/proc_mem_num_notes) on selecting processes with `name` and `cmdline` parameters (Linux-specific).

Examples:

    proc.get[zabbix,,,process] #list of all Zabbix processes, returns one entry per PID
    proc.get[java,,,thread] #list of all Java processes, returns one entry per thread
    proc.get[zabbix,,,summary] #combined data for Zabbix processes of each type, returns one entry per process name

[comment]: # ({/new-02480d9a})

[comment]: # ({new-d3e42b1a})

##### proc.mem[<name>,<user>,<mode>,<cmdline>,<memtype>]

<br>
The memory used by the process in bytes.<br>
Return value: *Integer* - with `mode` as *max*, *min*, *sum*; *Float* - with `mode` as *avg*<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, Tru64, OpenBSD, NetBSD.

Parameters:

-   **name** - the process name (default is *all processes*);
-   **user** - the user name (default is *all users*);
-   **mode** - possible values: *avg*, *max*, *min*, or *sum* (default);
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview));
-   **memtype** - the [type of memory](/manual/appendix/items/proc_mem_notes) used by process

Comments:

-   The `memtype` parameter is supported only on Linux, FreeBSD, Solaris^**[6](#footnotes)**^, AIX;
-   When several processes use shared memory, the sum of memory used by processes may result in large, unrealistic values.<br><br>See [notes](/manual/appendix/items/proc_mem_num_notes) on selecting processes with `name` and `cmdline` parameters (Linux-specific).<br><br>When this item is invoked from the command line and contains a command line parameter (e.g. using the agent test mode: `zabbix_agentd -t proc.mem[,,,apache2]`), one extra process will be counted, as the agent will count itself.

Examples:

    proc.mem[,root] #the memory used by all processes running under the "root" user
    proc.mem[zabbix_server,zabbix] #the memory used by all zabbix_server processes running under the zabbix user
    proc.mem[,oracle,max,oracleZABBIX] #the memory used by the most memory-hungry process running under Oracle having oracleZABBIX in its command line

[comment]: # ({/new-d3e42b1a})

[comment]: # ({new-8e6bf30e})

##### proc.num[<name>,<user>,<state>,<cmdline>,<zone>]

<br>
The number of processes.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris^**[6](#footnotes)**^, HP-UX, AIX, Tru64, OpenBSD, NetBSD.

Parameters:

-   **name** - the process name (default is *all processes*);
-   **user** - the user name (default is *all users*);
-   **state** - possible values:<br>*all* (default),<br>*disk* - uninterruptible sleep,<br>*run* - running,<br>*sleep* - interruptible sleep,<br>*trace* - stopped,<br>*zomb* - zombie;
-   **cmdline** - filter by command line (it is a regular [expression](/manual/regular_expressions#overview));
-   **zone** - the target zone: *current* (default), or *all*. This parameter is supported on Solaris only.

Comments:

-   The *disk* and *trace* state parameters are supported only on Linux, FreeBSD, OpenBSD, NetBSD;
-   When this item is invoked from the command line and contains a command line parameter (e.g. using the agent test mode: `zabbix_agentd -t proc.num[,,,apache2]`), one extra process will be counted, as the agent will count itself;
-   When setting the `zone` parameter to *current* (or default) in case the agent has been compiled on a Solaris without zone support, but running on a newer Solaris where zones are supported, then the agent will return NOTSUPPORTED (the agent cannot limit results to only the current zone). However, *all* is supported in this case.
-   See [notes](/manual/appendix/items/proc_mem_num_notes) on selecting processes with `name` and `cmdline` parameters (Linux-specific).

Examples:

    proc.num[,mysql] #the number of processes running under the mysql user
    proc.num[apache2,www-data] #the number of apache2 processes running under the www-data user
    proc.num[,oracle,sleep,oracleZABBIX] #the number of processes in sleep state running under Oracle having oracleZABBIX in its command line

[comment]: # ({/new-8e6bf30e})

[comment]: # ({new-7cdd8214})

##### sensor[device,sensor,<mode>]

<br>
Hardware sensor reading.<br>
Return value: *Float*.<br>
[Supported platforms](#supported-platforms): Linux, OpenBSD.

Parameters:

-   **device** - the device name;
-   **sensor** - the sensor name;
-   **mode** - possible values: *avg*, *max*, or *min* (if this parameter is omitted, device and sensor are treated verbatim).

Comments:

-   Reads /proc/sys/dev/sensors on Linux 2.4;
-   Reads /sys/class/hwmon on Linux 2.6+. See a more detailed description of [sensor](/manual/appendix/items/sensor) item on Linux.
-   Reads the *hw.sensors* MIB on OpenBSD.

Example:

    sensor[w83781d-i2c-0-2d,temp1]
    sensor[cpu0,temp0] #the temperature of one CPU
    sensor["cpu[0-2]$",temp,avg] #the average temperature of the first three CPUs

[comment]: # ({/new-7cdd8214})

[comment]: # ({new-f46449fd})

##### system.boottime

<br>
The system boot time.<br>
Return value: *Integer (Unix timestamp)*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-f46449fd})

[comment]: # ({new-b01f71c5})

##### system.cpu.discovery

<br>
The list of detected CPUs/CPU cores. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

[comment]: # ({/new-b01f71c5})

[comment]: # ({new-956c4ef9})

##### system.cpu.intr

<br>
The device interrupts.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD, NetBSD.

[comment]: # ({/new-956c4ef9})

[comment]: # ({new-c8c74d92})

##### system.cpu.load[<cpu>,<mode>]

<br>
The [CPU load](http://en.wikipedia.org/wiki/Load_(computing)).<br>
Return value: *Float*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **cpu** - possible values: *all* (default) or *percpu* (the total load divided by online CPU count);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*.

The *percpu* parameter is not supported on Tru64.

Example:

    system.cpu.load[,avg5]

[comment]: # ({/new-c8c74d92})

[comment]: # ({new-e110ddf9})

##### system.cpu.num[<type>]

<br>
The number of CPUs.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, MacOS X, OpenBSD, NetBSD.

Parameter:

-   **type** - possible values: *online* (default) or *max*

The *max* type parameter is supported only on Linux, FreeBSD, Solaris, MacOS X.

Example: 
    
    system.cpu.num

[comment]: # ({/new-e110ddf9})

[comment]: # ({new-addfe1ba})

##### system.cpu.switches

<br>
The count of context switches.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD, NetBSD.

[comment]: # ({/new-addfe1ba})

[comment]: # ({new-d1abd90a})

##### system.cpu.util[<cpu>,<type>,<mode>,<logical_or_physical>]

<br>
The CPU utilization percentage.<br>
Return value: *Float*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, Tru64, OpenBSD, NetBSD.

Parameters:

-   **cpu** - *<CPU number>* or *all* (default);
-   **type** - possible values: *user* (default), *idle*, *nice*, *system*, *iowait*, *interrupt*, *softirq*, *steal*, *guest* (on Linux kernels 2.6.24 and above), or *guest\_nice* (on Linux kernels 2.6.33 and above);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*;
-   **logical\_or\_physical** - possible values: *logical* (default) or *physical*. This parameter is supported on AIX only.

Comments:

-   The *nice* type parameter is supported only on Linux, FreeBSD, HP-UX, Tru64, OpenBSD, NetBSD.
-   The *iowait* type parameter is supported only on Linux 2.6 and later, Solaris, AIX.
-   The *interrupt* type parameter is supported only on Linux 2.6 and later, FreeBSD, OpenBSD.
-   The *softirq*, *steal*, *guest*, *guest_nice* type parameters are supported only on Linux 2.6 and later.
-   The *avg5* and *avg15* mode parameters are supported on Linux, FreeBSD, Solaris, HP-UX, AIX, OpenBSD, NetBSD.

Example:

    system.cpu.util[0,user,avg5]

[comment]: # ({/new-d1abd90a})

[comment]: # ({new-05f3200c})

##### system.hostname[<type>,<transform>]

<br>
The system host name.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **type** - possible values: *netbios* (default on Windows), *host* (default on Linux) or *shorthost* (since version 5.4.7; returns part of the hostname before the first dot, a full string for names without dots);
-   **transform** - possible values: *none* (default) or *lower* (convert to lowercase).

The value is acquired by taking `nodename` from the uname() system API output.

Examples of returned values:

    system.hostname → linux-w7x1
    system.hostname → example.com
    system.hostname[shorthost] → example

[comment]: # ({/new-05f3200c})

[comment]: # ({new-d14d8400})

##### system.hw.chassis[<info>]

<br>
The chassis information.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameter:

-   **info** - possible values: *full* (default), *model*, *serial*, *type*, or *vendor*

Comments:

-   This item key depends on the availability of the [SMBIOS](http://en.wikipedia.org/wiki/System_Management_BIOS) table;
-   It will try to read the DMI table from sysfs, if sysfs access fails then try reading directly from memory;
-   **Root permissions** are required because the value is acquired by reading from sysfs or memory.

Example: 

    system.hw.chassis[full] → Hewlett-Packard HP Pro 3010 Small Form Factor PC CZXXXXXXXX Desktop

[comment]: # ({/new-d14d8400})

[comment]: # ({new-33f13e22})

##### system.hw.cpu[<cpu>,<info>]

<br>
The CPU information.<br>
Return value: *String* or *Integer*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **cpu** - *<CPU number>* or *all* (default);
-   **info** - possible values: *full* (default), *curfreq*, *maxfreq*, *model* or *vendor*.

Comments:

-   Gathers info from `/proc/cpuinfo` and `/sys/devices/system/cpu/[cpunum]/cpufreq/cpuinfo_max_freq`;
-   If a CPU number and *curfreq* or *maxfreq* is specified, a numeric value is returned (Hz).

Example:

    system.hw.cpu[0,vendor] → AuthenticAMD

[comment]: # ({/new-33f13e22})

[comment]: # ({new-87688787})

##### system.hw.devices[<type>]

<br>
The listing of PCI or USB devices.<br>
Return value: *Text*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameter: 

-   **type** - *pci* (default) or *usb*

Returns the output of either the lspci or lsusb utility (executed without any parameters).

Example:

    system.hw.devices → 00:00.0 Host bridge: Advanced Micro Devices [AMD] RS780 Host Bridge

[comment]: # ({/new-87688787})

[comment]: # ({new-ee01b8b4})

##### system.hw.macaddr[<interface>,<format>]

<br>
The listing of MAC addresses.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **interface** - *all* (default) or a regular [expression](/manual/regular_expressions#overview);
-   **format** - *full* (default) or *short*

Comments:

-   Lists MAC addresses of the interfaces whose name matches the given `interface` regular [expression](/manual/regular_expressions#overview) (*all* lists for all interfaces);
-   If `format` is specified as *short*, interface names and identical MAC addresses are not listed.

Example:

    system.hw.macaddr["eth0$",full] → [eth0] 00:11:22:33:44:55

[comment]: # ({/new-ee01b8b4})

[comment]: # ({new-1729b15d})

##### system.localtime[<type>]

<br>
The system time.<br>
Return value: *Integer* - with `type` as *utc*; *String* - with `type` as *local*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **type** - possible values: *utc* - (default) the time since the Epoch (00:00:00 UTC, January 1, 1970), measured in seconds or *local* - the time in the 'yyyy-mm-dd,hh:mm:ss.nnn,+hh:mm' format

Must be used as a [passive check](/manual/appendix/items/activepassive#passive_checks) only.

Example:

    system.localtime[local] #create an item using this key and then use it to display the host time in the *Clock* dashboard widget.

[comment]: # ({/new-1729b15d})

[comment]: # ({new-806250ba})

##### system.run[command,<mode>]

<br>
Run the specified command on the host.<br>
Return value: *Text* result of the command or 1 - with `mode` as *nowait* (regardless of the command result).<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **command** - command for execution;<br>
-   **mode** - possible values: *wait* - wait end of execution (default) or *nowait* - do not wait.

Comments: 

-   This item is disabled by default. Learn how to [enable them](/manual/config/items/restrict_checks);
-   Up to 512KB of data can be returned, including the trailing whitespace that is truncated;
-   To be processed correctly, the output of the command must be text;
-   The return value of the item is standard output together with standard error produced by command. The exit code is not checked. Empty result is allowed.
-   See also: [Command execution](/manual/appendix/command_execution).

Example:

    system.run[ls -l /] #return a detailed file list of the root directory

[comment]: # ({/new-806250ba})

[comment]: # ({new-ed8aa2a2})

##### system.stat[resource,<type>]

<br>
The system statistics.<br>
Return value: *Integer* or *float*.<br>
[Supported platforms](#supported-platforms): AIX.

Parameters:

-   **ent** - the number of processor units this partition is entitled to receive (float);
-   **kthr,<type>** - information about kernel thread states:<br>*r* - average number of runnable kernel threads (float)<br>*b* - average number of kernel threads placed in the Virtual Memory Manager wait queue (float)
-   **memory,<type>** - information about the usage of virtual and real memory:<br>*avm* - active virtual pages (integer)<br>*fre* - size of the free list (integer)
-   **page,<type>** - information about page faults and paging activity:<br>*fi* - file page-ins per second (float)<br>*fo* - file page-outs per second (float)<br>*pi* - pages paged in from paging space (float)<br>*po* - pages paged out to paging space (float)<br>*fr* - pages freed (page replacement) (float)<br>*sr* - pages scanned by page-replacement algorithm (float)
-   **faults,<type>** - trap and interrupt rate:<br>*in* - device interrupts (float)<br>*sy* - system calls (float)<br>*cs* - kernel thread context switches (float)
-   **cpu,<type>** - breakdown of percentage usage of processor time:<br>*us* - user time (float)<br>*sy* - system time (float)<br>*id* - idle time (float)<br>*wa* - idle time during which the system had outstanding disk/NFS I/O request(s) (float)<br>*pc* - number of physical processors consumed (float)<br>*ec* - the percentage of entitled capacity consumed (float)<br>*lbusy* - indicates the percentage of logical processor(s) utilization that occurred while executing at the user and system level (float)<br>*app* - indicates the available physical processors in the shared pool (float)
-   **disk,<type>** - disk statistics:<br>*bps* - indicates the amount of data transferred (read or written) to the drive in bytes per second (integer)<br>*tps* - indicates the number of transfers per second that were issued to the physical disk/tape (float)|

Comments:

-   Take note of the following limitations in these items:<br>
    `system.stat[cpu,app]` - supported only on AIX LPAR of type "Shared"<br>
    `system.stat[cpu,ec]` - supported on AIX LPAR of type "Shared" and "Dedicated" ("Dedicated" always returns 100 (percent))<br>
    `system.stat[cpu,lbusy]` - supported only on AIX LPAR of type "Shared"<br>
    `system.stat[cpu,pc]` - supported on AIX LPAR of type "Shared" and "Dedicated"<br>
    `system.stat[ent]` - supported on AIX LPAR of type "Shared" and "Dedicated"

[comment]: # ({/new-ed8aa2a2})

[comment]: # ({new-087e6420})

##### system.sw.arch

<br>
The software architecture information.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

The info is acquired from the `uname()` function.

Example:

    system.sw.arch → i686

[comment]: # ({/new-087e6420})

[comment]: # ({new-ed818157})

##### system.sw.os[<info>]

<br>
The operating system information.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux, Windows. Supported on Windows since Zabbix 6.4.

Parameter:

-   **info** - possible values: *full* (default), *short*, or *name*

The info is acquired from (note that not all files and options are present in all distributions):

-   `/proc/version` (*full*) on Linux;
-   `/proc/version_signature` (*short*) on Linux;
-   the PRETTY_NAME parameter from `/etc/os-release` on Linux-systems supporting it or `/etc/issue.net` (*name*);
-   the `HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion` registry key on Windows.

Examples:

    system.sw.os[short] → Ubuntu 2.6.35-28.50-generic 2.6.35.11
    system.sw.os[full] → [s|Windows 10 Enterprise 22621.1.amd64fre.ni_release.220506-1250 Build 22621.963]

[comment]: # ({/new-ed818157})

[comment]: # ({new-38e87471})

##### system.sw.os.get

<br>
Detailed information about the operating system (version, type, distribution name, minor and major version, etc).<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, Windows. Supported since Zabbix 6.4.

[comment]: # ({/new-38e87471})

[comment]: # ({new-bc2ee313})

##### system.sw.packages[<regexp>,<manager>,<format>]

<br>
The listing of installed packages.<br>
Return value: *Text*.<br>
[Supported platforms](#supported-platforms): Linux.

Parameters:

-   **regexp** - *all* (default) or a regular [expression](/manual/regular_expressions#overview);
-   **manager** - *all* (default) or a package manager;
-   **format** - *full* (default) or *short*.

Comments:

-   Lists (alphabetically) installed packages whose name matches the given regular [expression](/manual/regular_expressions#overview) (*all* lists them all);
-   Supported package managers (executed command):<br>dpkg (dpkg --get-selections)<br>pkgtool (ls /var/log/packages)<br>rpm (rpm -qa)<br>pacman (pacman -Q)
-   If `format` is specified as *full*, packages are grouped by package managers (each manager on a separate line beginning with its name in square brackets);
-   If `format` is specified as *short*, packages are not grouped and are listed on a single line.

Example:

    system.sw.packages[mini,dpkg,short] → python-minimal, python2.6-minimal, ubuntu-minimal

[comment]: # ({/new-bc2ee313})

[comment]: # ({new-43f5d666})

##### system.sw.packages.get[<regexp>,<manager>]

<br>
A detailed listing of installed packages.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux. Supported since Zabbix 6.4.

Parameters:

-   **regexp** - *all* (default) or a regular [expression](/manual/regular_expressions#overview);
-   **manager** - *all* (default) or a package manager (possible values: *rpm*, *dpkg*, *pkgtool*, or *pacman*).

Comments:

-   Returns unformatted JSON with the installed packages whose name matches the given regular expression;
-   The output is an array of objects each containing the following keys: name, manager, version, size, architecture, buildtime and installtime (see [more details](/manual/appendix/items/return_values#system.sw.packages.get)).

[comment]: # ({/new-43f5d666})

[comment]: # ({new-b0c4a5e8})

##### system.swap.in[<device>,<type>]

<br>
The swap-in (from device into memory) statistics.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, OpenBSD.

Parameters:

-   **device** - specify the device used for swapping (Linux only) or *all* (default);
-   **type** - possible values: *count* (number of swapins, default on non-Linux platforms), *sectors* (sectors swapped in), or *pages* (pages swapped in, default on Linux).

Comments:

-   The source of this information is:<br>/proc/swaps, /proc/partitions, /proc/stat (Linux 2.4)<br>/proc/swaps, /proc/diskstats, /proc/vmstat (Linux 2.6)
-   Note that *pages* will only work if device was not specified;
-   The *sectors* type parameter is supported only on Linux.

Example:

    system.swap.in[,pages]

[comment]: # ({/new-b0c4a5e8})

[comment]: # ({new-6cc86983})

##### system.swap.out[<device>,<type>]

<br>
The swap-out (from memory onto device) statistics.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, OpenBSD.

Parameters:

-   **device** - specify the device used for swapping (Linux only) or *all* (default);
-   **type** - possible values: *count* (number of swapouts, default on non-Linux platforms), *sectors* (sectors swapped out), or *pages* (pages swapped out, default on Linux).

Comments:

-   The source of this information is:<br>`/proc/swaps`, `/proc/partitions`, `/proc/stat` (Linux 2.4)<br>`/proc/swaps`, `/proc/diskstats`, `/proc/vmstat` (Linux 2.6)
-   Note that *pages* will only work if device was not specified;
-   The *sectors* type parameter is supported only on Linux.

Example:

    system.swap.out[,pages]

[comment]: # ({/new-6cc86983})

[comment]: # ({new-5815bd72})

##### system.swap.size[<device>,<type>]

<br>
The swap space size in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, Tru64, OpenBSD.

Parameters:

-   **device** - specify the device used for swapping (FreeBSD only) or *all* (default);
-   **type** - possible values: *free* (free swap space, default), *pfree* (free swap space, in percent), *pused* (used swap space, in percent), *total* (total swap space), or *used* (used swap space).

Comments:

-   Note that *pfree*, *pused* are not supported on Windows if swap size is 0;
-   If device is not specified Zabbix agent will only take into account swap devices (files), the physical memory will be ignored. For example, on Solaris systems the `swap -s` command includes a portion of physical memory and swap devices (unlike `swap -l`).

Example:

    system.swap.size[,pfree] → free swap space percentage
    

[comment]: # ({/new-5815bd72})

[comment]: # ({new-6ceb79d2})

##### system.uname

<br>
Identification of the system.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Comments:

-   On UNIX the value for this item is obtained with the uname() system call;
-   On Windows the item returns the OS architecture, whereas on UNIX it returns the CPU architecture.|


Example (UNIX):

     system.uname → FreeBSD localhost 4.2-RELEASE FreeBSD 4.2-RELEASE #0: Mon Nov i386
     

[comment]: # ({/new-6ceb79d2})

[comment]: # ({new-545f6370})

##### system.uptime

<br>
The system uptime in seconds.<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, MacOS X, OpenBSD, NetBSD. The support on Tru64 is unknown.

In [item configuration](/manual/config/items/item#configuration), use **s** or **uptime** units to get readable values.|

[comment]: # ({/new-545f6370})

[comment]: # ({new-c03c81d9})

##### system.users.num

<br>
The number of users logged in.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

The **who** command is used on the agent side to obtain the value.

[comment]: # ({/new-c03c81d9})

[comment]: # ({new-48d7b263})

##### vfs.dev.discovery

<br>
The list of block devices and their type. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux.

[comment]: # ({/new-48d7b263})

[comment]: # ({new-bde9774e})

##### vfs.dev.read[<device>,<type>,<mode>]

<br>
The disk read statistics.<br>
Return value: *Integer* - with `type` in *sectors*, *operations*, *bytes*; *Float* - with `type` in *sps*, *ops*, *bps*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD.

Parameters:

-   **device** - disk device (default is *all* ^**[3](#footnotes)**^);
-   **type** - possible values: *sectors*, *operations*, *bytes*, *sps*, *ops*, or *bps* (*sps*, *ops*, *bps* stand for: sectors, operations, bytes per second, respectively);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*. This parameter is supported only with `type` in: sps, ops, bps.

Comments:

-   If using an update interval of three hours or more^**[2](#footnotes)**^, this item will always return '0';
-   The *sectors* and *sps* type parameters are supported only on Linux;
-   The *ops* type parameter is supported only on Linux and FreeBSD;
-   The *bps* type parameter is supported only on FreeBSD;
-   The *bytes* type parameter is supported only on FreeBSD, Solaris, AIX, OpenBSD;
-   The `mode` parameter is supported only on Linux, FreeBSD;
-   You may use relative device names (for example, `sda`) as well as an optional /dev/ prefix (for example, `/dev/sda`);
-   LVM logical volumes are supported;
-   The default values of 'type' parameter for different OSes:<br>*AIX* - operations<br>*FreeBSD* - bps<br>*Linux* - sps<br>*OpenBSD* - operations<br>*Solaris* - bytes
-   *sps*, *ops* and *bps* on supported platforms is limited to 1024 devices (1023 individual and one for *all*).

Example:

    vfs.dev.read[,operations]

[comment]: # ({/new-bde9774e})

[comment]: # ({new-beef0883})

##### vfs.dev.write[<device>,<type>,<mode>]

<br>
The disk write statistics.<br>
Return value: *Integer* - with `type` in *sectors*, *operations*, *bytes*; *Float* - with `type` in *sps*, *ops*, *bps*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, AIX, OpenBSD.

Parameters:

-   **device** - disk device (default is *all* ^**[3](#footnotes)**^);
-   **type** - possible values: *sectors*, *operations*, *bytes*, *sps*, *ops*, or *bps* (*sps*, *ops*, *bps* stand for: sectors, operations, bytes per second, respectively);
-   **mode** - possible values: *avg1* (one-minute average, default), *avg5*, or *avg15*. This parameter is supported only with `type` in: sps, ops, bps.

Comments:

-   If using an update interval of three hours or more^**[2](#footnotes)**^, this item will always return '0';
-   The *sectors* and *sps* type parameters are supported only on Linux;
-   The *ops* type parameter is supported only on Linux and FreeBSD;
-   The *bps* type parameter is supported only on FreeBSD;
-   The *bytes* type parameter is supported only on FreeBSD, Solaris, AIX, OpenBSD;
-   The `mode` parameter is supported only on Linux, FreeBSD;
-   You may use relative device names (for example, `sda`) as well as an optional /dev/ prefix (for example, `/dev/sda`);
-   LVM logical volumes are supported;
-   The default values of 'type' parameter for different OSes:<br>*AIX* - operations<br>*FreeBSD* - bps<br>*Linux* - sps<br>*OpenBSD* - operations<br>*Solaris* - bytes
-   *sps*, *ops* and *bps* on supported platforms is limited to 1024 devices (1023 individual and one for *all*).

Example:

    vfs.dev.write[,operations]

[comment]: # ({/new-beef0883})

[comment]: # ({new-a28e06f9})

##### vfs.dir.count[dir,<regex_incl>,<regex_excl>,<types_incl>,<types_excl>,<max_depth>,<min_size>,<max_size>,<min_age>,<max_age>,<regex_excl_dir>]

<br>
The directory entry count.<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **dir** - the absolute path to directory;
-   **regex_incl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to include; include all if empty (default value);
-   **regex_excl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to exclude; don't exclude any if empty (default value);
-   **types_incl** - directory entry types to count, possible values: *file* - regular file, *dir* - subdirectory, *sym* - symbolic link, *sock* - socket, *bdev* - block device, *cdev* - character device, *fifo* - FIFO, *dev* - synonymous with "bdev,cdev", *all* - all types (default), i.e. "file,dir,sym,sock,bdev,cdev,fifo". Multiple types must be separated with comma and quoted.
-   **types_excl** - directory entry types (see <types_incl>) to NOT count. If some entry type is in both <types_incl> and <types_excl>, directory entries of this type are NOT counted.
-   **max_depth** - the maximum depth of subdirectories to traverse:<br>**-1** (default) - unlimited,<br>**0** - no descending into subdirectories.
-   **min_size** - the minimum size (in bytes) for file to be counted. Smaller files will not be counted. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **max_size** - the maximum size (in bytes) for file to be counted. Larger files will not be counted. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **min_age** - the minimum age (in seconds) of directory entry to be counted. More recent entries will not be counted. [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **max_age** - the maximum age (in seconds) of directory entry to be counted. Entries so old and older will not be counted (modification time). [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **regex_excl_dir** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the directory to exclude. All content of the directory will be excluded (in contrast to regex\_excl)

Comments:

-   Environment variables, e.g. %APP_HOME%, $HOME and %TEMP% are not supported;
-   Pseudo-directories "." and ".." are never counted;
-   Symbolic links are never followed for directory traversal;
-   Both `regex_incl` and `regex_excl` are being applied to files and directories when calculating the entry size, but are ignored when picking subdirectories to traverse (if regex_incl is “(?i)\^.+\\.zip$” and max_depth is not set, then all subdirectories will be traversed, but only the files of type zip will be counted).
-   The execution time is limited by the default timeout value in agent [configuration](/manual/appendix/config/zabbix_agentd) (3 sec). Since large directory traversal may take longer than that, no data will be returned and the item will turn unsupported. Partial count will not be returned.
-   When filtering by size, only regular files have meaningful sizes. Under Linux and BSD, directories also have non-zero sizes (a few Kb typically). Devices have zero sizes, e.g. the size of **/dev/sda1** does not reflect the respective partition size. Therefore, when using `<min_size>` and `<max_size>`, it is advisable to specify `<types_incl>` as "*file*", to avoid surprises.

Examples:

    vfs.dir.count[/dev] #monitors the number of devices in /dev (Linux)
    

[comment]: # ({/new-a28e06f9})

[comment]: # ({new-4a354b20})

##### vfs.dir.get[dir,<regex_incl>,<regex_excl>,<types_incl>,<types_excl>,<max_depth>,<min_size>,<max_size>,<min_age>,<max_age>,<regex_excl_dir>]

<br>
The directory entry list.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **dir** - the absolute path to directory;
-   **regex_incl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to include; include all if empty (default value);
-   **regex_excl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to exclude; don't exclude any if empty (default value);
-   **types_incl** - directory entry types to list, possible values: *file* - regular file, *dir* - subdirectory, *sym* - symbolic link, *sock* - socket, *bdev* - block device, *cdev* - character device, *fifo* - FIFO, *dev* - synonymous with "bdev,cdev", *all* - all types (default), i.e. "file,dir,sym,sock,bdev,cdev,fifo". Multiple types must be separated with comma and quoted.
-   **types_excl** - directory entry types (see <types_incl>) to NOT list. If some entry type is in both <types_incl> and <types_excl>, directory entries of this type are NOT listed.
-   **max_depth** - the maximum depth of subdirectories to traverse:<br>**-1** (default) - unlimited,<br>**0** - no descending into subdirectories.
-   **min_size** - the minimum size (in bytes) for file to be listed. Smaller files will not be listed. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **max_size** - the maximum size (in bytes) for file to be listed. Larger files will not be listed. [Memory suffixes](/manual/appendix/suffixes#memory_suffixes) can be used.
-   **min_age** - the minimum age (in seconds) of directory entry to be listed. More recent entries will not be listed. [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **max_age** - the maximum age (in seconds) of directory entry to be listed. Entries so old and older will not be listed (modification time). [Time suffixes](/manual/appendix/suffixes#time_suffixes) can be used.
-   **regex_excl_dir** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the directory to exclude. All content of the directory will be excluded (in contrast to regex\_excl)

Comments:

-   Environment variables, e.g. %APP_HOME%, $HOME and %TEMP% are not supported;
-   Pseudo-directories "." and ".." are never listed;
-   Symbolic links are never followed for directory traversal;
-   Both `regex_incl` and `regex_excl` are being applied to files and directories when calculating the entry size, but are ignored when picking subdirectories to traverse (if regex_incl is “(?i)\^.+\\.zip$” and max_depth is not set, then all subdirectories will be traversed, but only the files of type zip will be counted).
-   The execution time is limited by the default timeout value in agent [configuration](/manual/appendix/config/zabbix_agentd) (3 sec). Since large directory traversal may take longer than that, no data will be returned and the item will turn unsupported. Partial list will not be returned.
-   When filtering by size, only regular files have meaningful sizes. Under Linux and BSD, directories also have non-zero sizes (a few Kb typically). Devices have zero sizes, e.g. the size of **/dev/sda1** does not reflect the respective partition size. Therefore, when using `<min_size>` and `<max_size>`, it is advisable to specify `<types_incl>` as "*file*", to avoid surprises.

Examples:

    vfs.dir.get[/dev] #retrieves the device list in /dev (Linux)
    

[comment]: # ({/new-4a354b20})

[comment]: # ({new-c1571da2})

##### vfs.dir.size[dir,<regex_incl>,<regex_excl>,<mode>,<max_depth>,<regex_excl_dir>]

<br>
The directory size (in bytes).<br>
Return value: *Integer*.<br>
[Supported platforms](#supported-platforms): Linux. The item may work on other UNIX-like platforms.

Parameters:

-   **dir** - the absolute path to directory;
-   **regex_incl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to include; include all if empty (default value);
-   **regex_excl** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the entity (file, directory, symbolic link) to exclude; don't exclude any if empty (default value);
-   **mode** - possible values: *apparent* (default) - gets apparent file sizes rather than disk usage (acts as `du -sb dir`), *disk* - gets disk usage (acts as `du -s -B1 dir`). Unlike the `du` command, the vfs.dir.size item takes hidden files in account when calculating the directory size (acts as `du -sb .[^.]* *` within dir).
-   **max_depth** - the maximum depth of subdirectories to traverse: **-1** (default) - unlimited, **0** - no descending into subdirectories.
-   **regex_excl_dir** - a regular [expression](/manual/regular_expressions#overview) describing the name pattern of the directory to exclude. All content of the directory will be excluded (in contrast to regex_excl)

Comments:

-   Only directories with at least the read permission for *zabbix* user are calculated. For directories with read permission only, the size of the directory itself is calculated. Directories with read & execute permissions are calculated including contents.
-   With large directories or slow drives this item may time out due to the Timeout setting in [agent](/manual/appendix/config/zabbix_agentd) and [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) configuration files. Increase the timeout values as necessary.
-   The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Examples:

    vfs.dir.size[/tmp,log] #calculates the size of all files in /tmp which contain 'log'
    vfs.dir.size[/tmp,log,^.+\.old$] #calculates the size of all files in /tmp which contain 'log', excluding files containing '.old'

[comment]: # ({/new-c1571da2})

[comment]: # ({new-44cb41f6})

##### vfs.file.cksum[file,<mode>]

<br>
The file checksum, calculated by the UNIX cksum algorithm.<br>
Return value: *Integer* - with `mode` as *crc32*, *String* - with `mode` as *md5*, *sha256*.<br>
See [#supported platforms](supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **mode** - *crc32* (default), *md5*, or *sha256*.

The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.cksum[/etc/passwd]
    
Example of returned values (crc32/md5/sha256 respectively):

    675436101
    9845acf68b73991eb7fd7ee0ded23c44
    ae67546e4aac995e5c921042d0cf0f1f7147703aa42bfbfb65404b30f238f2dc

[comment]: # ({/new-44cb41f6})

[comment]: # ({new-0f4f51a1})

##### vfs.file.contents[file,<encoding>]

<br>
Retrieving the contents of a file.<br>
Return value: *Text*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings).

Comments:

-   This item is limited to files no larger than 64KB;
-   An empty string is returned if the file is empty or contains LF/CR characters only;
-   The byte order mark (BOM) is excluded from the output.

Example:

    vfs.file.contents[/etc/passwd]

[comment]: # ({/new-0f4f51a1})

[comment]: # ({new-cb77c4fb})

##### vfs.file.exists[file,<types_incl>,<types_excl>]

<br>
Checks if the file exists.<br>
Return value: 0 - not found; 1 - file of the specified type exists.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **types_incl** - the list of file types to include, possible values: *file* (regular file, default (if types\_excl is not set)), *dir* (directory), *sym* (symbolic link), *sock* (socket), *bdev* (block device), *cdev* (character device), *fifo* (FIFO), *dev* (synonymous with "bdev,cdev"), *all* (all mentioned types, default if types\_excl is set).
-   **types_excl** - the list of file types to exclude, see types_incl for possible values (by default no types are excluded)

Comments:

-   Multiple types must be separated with a comma and the entire set enclosed in quotes "";
-   If the same type is in both <types_incl> and <types_excl>, files of this type are excluded;
-   The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Examples:

    vfs.file.exists[/tmp/application.pid]
    vfs.file.exists[/tmp/application.pid,"file,dir,sym"]
    vfs.file.exists[/tmp/application_dir,dir]

[comment]: # ({/new-cb77c4fb})

[comment]: # ({new-12c8307e})

##### vfs.file.get[file]

<br>
Returns information about a file.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Parameter:

-   **file** - the full path to file

Supported file types on UNIX-like systems: regular file, directory, symbolic link, socket, block device, character device, FIFO.

Example:

    vfs.file.get[/etc/passwd] #return a JSON with information about the /etc/passwd file (type, user, permissions, SID, uid etc)

[comment]: # ({/new-12c8307e})

[comment]: # ({new-731f3af2})

##### vfs.file.md5sum[file]

<br>
The MD5 checksum of file.<br>
Return value: Character string (MD5 hash of the file).<br>
See [supported platforms](#supported-platforms).

Parameter:

-   **file** - the full path to file

The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.md5sum[/usr/local/etc/zabbix_agentd.conf]

Example of returned value:

    b5052decb577e0fffd622d6ddc017e82

[comment]: # ({/new-731f3af2})

[comment]: # ({new-c191e68e})

##### vfs.file.owner[file,<ownertype>,<resulttype>]

<br>
Retrieves the owner of a file.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **ownertype** - *user* (default) or *group* (Unix only);
-   **resulttype** - *name* (default) or *id*; for id - return uid/gid on Unix, SID on Windows.

Example:

    vfs.file.owner[/tmp/zabbix_server.log] #return the file owner of /tmp/zabbix_server.log
    vfs.file.owner[/tmp/zabbix_server.log,,id] #return the file owner ID of /tmp/zabbix_server.log

[comment]: # ({/new-c191e68e})

[comment]: # ({new-781f8e91})

##### vfs.file.permissions[file]

<br>
Return a 4-digit string containing the octal number with UNIX permissions.<br>
Return value: *String*.<br>
[Supported platforms](#supported-platforms): Linux. The item may work on other UNIX-like platforms.

Parameters:

-   **file** - the full path to file

Example:

    vfs.file.permissions[/etc/passwd] #return permissions of /etc/passwd, for example, '0644'
    

[comment]: # ({/new-781f8e91})

[comment]: # ({new-679ce640})

##### vfs.file.regexp[file,regexp,<encoding>,<start line>,<end line>,<output>]

<br>
Retrieve a string in the file.<br>
Return value: The line containing the matched string, or as specified by the optional `output` parameter.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);
-   **start line** - the number of the first line to search (first line of file by default);
-   **end line** - the number of the last line to search (last line of file by default);
-   **output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).

Comments:

-   Only the first matching line is returned;
-   An empty string is returned if no line matched the expression;
-   The byte order mark (BOM) is excluded from the output;
-   Content extraction using the `output` parameter takes place on the agent.

Examples:

    vfs.file.regexp[/etc/passwd,zabbix]
    vfs.file.regexp[/path/to/some/file,"([0-9]+)$",,3,5,\1]
    vfs.file.regexp[/etc/passwd,"^zabbix:.:([0-9]+)",,,,\1] → getting the ID of user *zabbix*
    

[comment]: # ({/new-679ce640})

[comment]: # ({new-5216f821})

##### vfs.file.regmatch[file,regexp,<encoding>,<start line>,<end line>]

<br>
Find a string in the file.<br>
Return values: 0 - match not found; 1 - found.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;
-   **encoding** - the code page [identifier](/manual/config/items/itemtypes/zabbix_agent#encoding_settings);
-   **start line** - the number of the first line to search (first line of file by default);
-   **end line** - the number of the last line to search (last line of file by default).

Comments:

-   The byte order mark (BOM) is ignored.

Example:

    vfs.file.regmatch[/var/log/app.log,error]

[comment]: # ({/new-5216f821})

[comment]: # ({new-705b33c8})

##### vfs.file.size[file,<mode>]

<br>
The file size (in bytes).<br>
Return value: *Integer*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **mode** - possible values: *bytes* (default) or *lines* (empty lines are counted, too).

Comments:

-   The file must have read permissions for user *zabbix*;
-   The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.size[/var/log/syslog]

[comment]: # ({/new-705b33c8})

[comment]: # ({new-d590c33f})

##### vfs.file.time[file,<mode>]

<br>
The file time information.<br>
Return value: *Integer* (Unix timestamp).<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **file** - the full path to file;
-   **mode** - possible values:<br>*modify* (default) - the last time of modifying file content,<br>*access* - the last time of reading file,<br>*change* - the last time of changing file properties

The file size limit depends on [large file support](/manual/appendix/items/large_file_support).

Example:

    vfs.file.time[/etc/passwd,modify]

[comment]: # ({/new-d590c33f})

[comment]: # ({new-5c6f1f9c})

##### vfs.fs.discovery

<br>
The list of mounted filesystems with their type and mount options. Used for low-level discovery.<br>
Return value: *JSON object*.<br>
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, MacOS X, OpenBSD, NetBSD.

[comment]: # ({/new-5c6f1f9c})

[comment]: # ({new-dee74854})

##### vfs.fs.get

<br>
The list of mounted filesystems with their type, available disk space, inode statistics and mount options. Can be used for low-level discovery.<br>
Return value: *JSON object*.<br> 
[Supported platforms](#supported-platforms): Linux, FreeBSD, Solaris, HP-UX, AIX, MacOS X, OpenBSD, NetBSD.

Comments:

-   File systems with the inode count equal to zero, which can be the case for file systems with dynamic inodes (e.g. btrfs), are also reported;
-   See also: [Discovery of mounted filesystems](/manual/discovery/low_level_discovery/examples/mounted_filesystems).

[comment]: # ({/new-dee74854})

[comment]: # ({new-c1ad7314})

##### vfs.fs.inode[fs,<mode>]

<br>
The number or percentage of inodes.<br>
Return value: *Integer* - for number; *Float* - for percentage.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **fs** - the filesystem;
-   **mode** - possible values: *total* (default), *free*, *used*, *pfree* (free, percentage), or *pused* (used, percentage).

If the inode count equals zero, which can be the case for file systems with dynamic inodes (e.g. btrfs), the pfree/pused values will be reported as "100" and "0" respectively.

Example:

    vfs.fs.inode[/,pfree]

[comment]: # ({/new-c1ad7314})

[comment]: # ({new-2aa216f1})

##### vfs.fs.size[fs,<mode>]

<br>
The disk space in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **fs** - the filesystem;
-   **mode** - possible values: *total* (default), *free*, *used*, *pfree* (free, percentage), or *pused* (used, percentage).

Comments:

-   In case of a mounted volume, the disk space for local file system is returned;
-   The reserved space of a file system is taken into account and not included when using the *free* mode.

Example:

    vfs.fs.size[/tmp,free]

[comment]: # ({/new-2aa216f1})

[comment]: # ({new-07413e69})

##### vm.memory.size[<mode>]

<br>
The memory size in bytes or in percentage from total.<br>
Return value: *Integer* - for bytes; *Float* - for percentage.<br>
See [supported platforms](#supported-platforms).

Parameter:

-   **mode** - possible values: *total* (default), *active*, *anon*, *buffers*, *cached*, *exec*, *file*, *free*, *inactive*, *pinned*, *shared*, *slab*, *wired*, *used*, *pused* (used, percentage), *available*, or *pavailable* (available, percentage).

Comments:

-   This item accepts three categories of parameters:<br>1) *total* - total amount of memory<br>2) platform-specific memory types: *active*, *anon*, *buffers*, *cached*, *exec*, *file*, *free*, *inactive*, *pinned*, *shared*, *slab*, *wired*<br>3) user-level estimates on how much memory is used and available: *used*, *pused*, *available*, *pavailable*
-   The *active* mode parameter is supported only on FreeBSD, HP-UX, MacOS X, OpenBSD, NetBSD;
-   The *anon*, *exec*, *file* mode parameters are supported only on NetBSD;
-   The *buffers* mode parameter is supported only on Linux, FreeBSD, OpenBSD, NetBSD;
-   The *cached* mode parameter is supported only on Linux, FreeBSD, AIX, OpenBSD, NetBSD;
-   The *inactive*, *wired* mode parameters are supported only on FreeBSD, MacOS X, OpenBSD, NetBSD;
-   The *pinned* mode parameter is supported only on AIX;
-   The *shared* mode parameter is supported only on Linux 2.4, FreeBSD, OpenBSD, NetBSD;
-   See also [additional details](/manual/appendix/items/vm.memory.size_params) for this item.

Example:

    vm.memory.size[pavailable]

[comment]: # ({/new-07413e69})

[comment]: # ({new-cd3defd2})

##### web.page.get[host,<path>,<port>]

<br>
Get the content of a web page.<br>
Return value: Web page source as text (including headers).<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **host** - the hostname or URL (as `scheme://host:port/path`, where only *host* is mandatory). Allowed URL schemes: *http*, *https*^**[4](#footnotes)**^. A missing scheme will be treated as *http*. If a URL is specified `path` and `port` must be empty. Specifying user name/password when connecting to servers that require authentication, for example: `http://user:password@www.example.com` is only possible with cURL support ^**[4](#footnotes)**^. Punycode is supported in hostnames.
-   **path** - the path to an HTML document (default is /);
-   **port** - the port number (default is 80 for HTTP)

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable;
-   `host` can be a hostname, domain name, IPv4 or IPv6 address. But for IPv6 address Zabbix agent must be compiled with IPv6 support enabled.

Example:

    web.page.get[www.example.com,index.php,80]
    web.page.get[https://www.example.com]
    web.page.get[https://blog.example.com/?s=zabbix]
    web.page.get[localhost:80]
    web.page.get["[::1]/server-status"]

[comment]: # ({/new-cd3defd2})

[comment]: # ({new-ef902e6e})

##### web.page.perf[host,<path>,<port>]

<br>
The loading time of a full web page (in seconds).<br>
Return value: *Float*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **host** - the hostname or URL (as `scheme://host:port/path`, where only *host* is mandatory). Allowed URL schemes: *http*, *https*^**[4](#footnotes)**^. A missing scheme will be treated as *http*. If a URL is specified `path` and `port` must be empty. Specifying user name/password when connecting to servers that require authentication, for example: `http://user:password@www.example.com` is only possible with cURL support ^**[4](#footnotes)**^. Punycode is supported in hostnames.
-   **path** - the path to an HTML document (default is /);
-   **port** - the port number (default is 80 for HTTP)

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable;
-   `host` can be a hostname, domain name, IPv4 or IPv6 address. But for IPv6 address Zabbix agent must be compiled with IPv6 support enabled.

Example:

    web.page.perf[www.example.com,index.php,80]
    web.page.perf[https://www.example.com]

[comment]: # ({/new-ef902e6e})

[comment]: # ({new-a78ae078})

##### web.page.regexp[host,<path>,<port>,regexp,<length>,<output>]

<br>
Find a string on the web page.<br>
Return value: The matched string, or as specified by the optional `output` parameter.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **host** - the hostname or URL (as `scheme://host:port/path`, where only *host* is mandatory). Allowed URL schemes: *http*, *https*^**[4](#footnotes)**^. A missing scheme will be treated as *http*. If a URL is specified `path` and `port` must be empty. Specifying user name/password when connecting to servers that require authentication, for example: `http://user:password@www.example.com` is only possible with cURL support ^**[4](#footnotes)**^. Punycode is supported in hostnames.
-   **path** - the path to an HTML document (default is /);
-   **port** - the port number (default is 80 for HTTP)
-   **regexp** - a regular [expression](/manual/regular_expressions#overview) describing the required pattern;
-   **length** - the maximum number of characters to return;
-   **output** - an optional output formatting template. The **\\0** escape sequence is replaced with the matched part of text (from the first character where match begins until the character where match ends) while an **\\N** (where N=1...9) escape sequence is replaced with Nth matched group (or an empty string if the N exceeds the number of captured groups).

Comments:

-   This item turns unsupported if the resource specified in `host` does not exist or is unavailable;
-   `host` can be a hostname, domain name, IPv4 or IPv6 address. But for IPv6 address Zabbix agent must be compiled with IPv6 support enabled.
-   Content extraction using the `output` parameter takes place on the agent.

Example:

    web.page.regexp[www.example.com,index.php,80,OK,2]
    web.page.regexp[https://www.example.com,,,OK,2]|

[comment]: # ({/new-a78ae078})

[comment]: # ({new-3030d112})

##### agent.hostmetadata

<br>
The agent host metadata.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Returns the value of HostMetadata or HostMetadataItem parameters, or empty string if none are defined.

[comment]: # ({/new-3030d112})

[comment]: # ({new-e20fbf57})

##### agent.hostname

<br>
The agent host name.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Returns:

-   As passive check - the name of the first host listed in the Hostname parameter of the agent configuration file;
-   As active check - the name of the current hostname.

[comment]: # ({/new-e20fbf57})

[comment]: # ({new-3a10c7e2})

##### agent.ping

<br>
The agent availability check.<br>
Return value: Nothing - unavailable; 1 - available.<br>
See [supported platforms](#supported-platforms).

Use the **nodata()** trigger function to check for host unavailability.

[comment]: # ({/new-3a10c7e2})

[comment]: # ({new-d276be7d})

##### agent.variant

<br>
The variant of Zabbix agent (Zabbix agent or Zabbix agent 2).<br>
Return value: 1 - Zabbix agent; 2 - Zabbix agent 2.<br>
See [supported platforms](#supported-platforms).

[comment]: # ({/new-d276be7d})

[comment]: # ({new-f391e3aa})

##### agent.version

<br>
The version of Zabbix agent.<br>
Return value: *String*.<br>
See [supported platforms](#supported-platforms).

Example of returned value: 

    6.0.3

[comment]: # ({/new-f391e3aa})

[comment]: # ({new-31497288})

##### zabbix.stats[<ip>,<port>]

<br>
Returns a set of Zabbix server or proxy internal metrics remotely.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Parameters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051)

Comments:

-   A selected set of internal metrics is returned by this item. For details, see [Remote monitoring of Zabbix stats](/manual/appendix/items/remote_stats#exposed_metrics);
-   Note that the stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance.

[comment]: # ({/new-31497288})

[comment]: # ({new-ea79fb35})

##### zabbix.stats[<ip>,<port>,queue,<from>,<to>]

<br>
Returns the number of monitored items in the queue which are delayed on Zabbix server or proxy remotely.<br>
Return value: *JSON object*.<br>
See [supported platforms](#supported-platforms).

Paramaters:

-   **ip** - the IP/DNS/network mask list of servers/proxies to be remotely queried (default is 127.0.0.1);
-   **port** - the port of server/proxy to be remotely queried (default is 10051)
-   **queue** - constant (to be used as is)
-   **from** - delayed by at least (default is 6 seconds)
-   **to** - delayed by at most (default is infinity)

Note that the stats request will only be accepted from the addresses listed in the 'StatsAllowedIP' [server](/manual/appendix/config/zabbix_server)/[proxy](/manual/appendix/config/zabbix_proxy) parameter on the target instance.

[comment]: # ({/new-ea79fb35})

[comment]: # ({new-cefe2e1b})
#### Supported item keys

The table provides details on the item keys that you can use with Zabbix
agent items grouped by the item family.

See also:

-   [Items supported by
    platform](/manual/appendix/items/supported_by_platform)
-   [Item keys supported by Zabbix agent
    2](/manual/config/items/itemtypes/zabbix_agent/zabbix_agent2)
-   [Item keys specific for Windows
    agent](/manual/config/items/itemtypes/zabbix_agent/win_keys)
-   [Minimum permission level for Windows agent
    items](/manual/appendix/items/win_permissions)

[comment]: # ({/new-cefe2e1b})

















[comment]: # ({new-a2a6198d})
##### Footnotes

^**1**^A Linux-specific note. Zabbix agent must have read-only access to
filesystem */proc*. Kernel patches from www.grsecurity.org limit access
rights of non-privileged users.

^**2**^ `vfs.dev.read[]`, `vfs.dev.write[]`: Zabbix agent will terminate
"stale" device connections if the item values are not accessed for more
than 3 hours. This may happen if a system has devices with dynamically
changing paths or if a device gets manually removed. Note also that
these items, if using an update interval of 3 hours or more, will always
return '0'.

^**3**^ `vfs.dev.read[]`, `vfs.dev.write[]`: If default *all* is used
for the first parameter then the key will return summary statistics,
including all block devices like sda, sbd and their partitions (sda1,
sda2, sdb3...) and multiple devices (MD raid) based on those block
devices/partitions and logical volumes (LVM) based on those block
devices/partitions. In such cases returned values should be considered
only as relative value (dynamic in time) but not as absolute values.

^**4**^ SSL (HTTPS) is supported only if agent is compiled with cURL
support. Otherwise the item will turn unsupported.

[comment]: # ({/new-a2a6198d})

[comment]: # ({new-2e3a2e64})

### Usage with command-line utilities**

Note that when testing or using item keys with zabbix_agentd or zabbix_get
from the command line you should consider shell syntax too.

For example, if a certain parameter of the key has to be enclosed in double quotes
you have to explicitly escape double quotes, otherwise they will be trimmed by the shell
as special characters and will not be passed to the Zabbix utility.

Examples:

    $ zabbix_agentd -t 'vfs.dir.count[/var/log,,,"file,dir",,0]'

    $ zabbix_agentd -t vfs.dir.count[/var/log,,,\"file,dir\",,0]

[comment]: # ({/new-2e3a2e64})

[comment]: # ({new-b7e5172f})
#### Encoding settings

To make sure that the acquired data are not corrupted you may specify
the correct encoding for processing the check (e.g. 'vfs.file.contents')
in the `encoding` parameter. The list of supported encodings (code page
identifiers) may be found in documentation for
[libiconv](http://www.gnu.org/software/libiconv/) (GNU Project) or in
Microsoft Windows SDK documentation for "Code Page Identifiers".

If no encoding is specified in the `encoding` parameter the following
resolution strategies are applied:

-   Standard resolution - UTF-8 is used in Unix/Linux (default in newer
    distributions); ANSI with a system-specific extension is used in
    Windows;
-   BOM analysis - applicable for items 'vfs.file.contents',
    'vfs.file.regexp', 'vfs.file.regmatch'. An attempt is made to
    determine the correct encoding by using the byte order mark (BOM) at
    the beginning of the file. If BOM is not present - standard
    resolution (see above) is applied instead.

[comment]: # ({/new-b7e5172f})

[comment]: # ({new-40450e88})
#### Troubleshooting agent items

-   If used with the passive agent, *Timeout* value in server
    configuration may need to be higher than *Timeout* in the agent
    configuration file. Otherwise the item may not get any value because
    the server request to agent timed out first.

[comment]: # ({/new-40450e88})











