[comment]: # translation:outdated

[comment]: # ({new-1a1e2756})
# 18 Script items

[comment]: # ({/new-1a1e2756})

[comment]: # ({new-cb89bc38})
#### Overview

Script items can be used to collect data by executing a user-defined
JavaScript code with the ability to retrieve data over HTTP/HTTPS. In
addition to the script, an optional list of parameters (pairs of name
and value) and timeout can be specified.

This item type may be useful in data collection scenarios that require
multiple steps or complex logic. As an example, a Script item can be
configured to make an HTTP call, then process the data received in the
first step in some way, and pass transformed value to the second HTTP
call.

Script items are processed by Zabbix server or proxy pollers.

[comment]: # ({/new-cb89bc38})

[comment]: # ({new-7ed55e87})
#### Configuration

In the *Type* field of [item configuration
form](/manual/config/items/item) select Script then fill out required
fields.

![script\_item.png](../../../../../assets/en/manual/config/items/itemtypes/script_item.png)

All mandatory input fields are marked with a red asterisk.

The fields that require specific information for Script items are:

|Field|Description|
|-----|-----------|
|Key|Enter a unique key that will be used to identify the item.|
|Parameters|Specify the variables to be passed to the script as the attribute and value pairs.<br>[Built-in macros](/manual/config/macros) {HOST.CONN}, {HOST.DNS}, {HOST.HOST}, {HOST.IP}, {HOST.NAME}, {ITEM.ID}, {ITEM.KEY}, {ITEM.KEY.ORIG} and [user macros](/manual/config/macros/user_macros) are supported.|
|Script|Enter JavaScript code in the block that appears when clicking in the parameter field (or on the view/edit button next to it). This code must provide the logic for returning the metric value.<br>The code has access to all parameters, it may perform HTTP GET, POST, PUT and DELETE requests and has control over HTTP headers and request body.<br>See also: [Additional JavaScript objects](/manual/config/items/preprocessing/javascript/javascript_objects), [JavaScript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide).|
|Timeout|JavaScript execution timeout (1-60s, default 3s); exceeding it will return error.<br>Time suffixes are supported, e.g. 30s, 1m.<br>Depending on the script it might take longer for the timeout to trigger. |

[comment]: # ({/new-7ed55e87})

[comment]: # ({new-4c860844})
#### Examples

[comment]: # ({/new-4c860844})

[comment]: # ({new-9c6c78ee})
##### Simple data collection

Collect the content of *https://www.example.com/release\_notes*:

-   Create an item with type "Script".\
-   In the *Script* field, enter the following code:

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com/release_notes");
```

[comment]: # ({/new-9c6c78ee})

[comment]: # ({new-7115c5ef})
##### Dat collection with parameters

Get the content of a specific page and make use of parameters: 

- Create an item with type Script and two parameters:
  - **url : {$DOMAIN}** (the user macro {$DOMAIN} should be defined, preferably on a host level)
  - **subpage : /release_notes**

![](../../../../../assets/en/manual/config/items/itemtypes/script_example1.png)

- In the script field, enter: 

   var obj = JSON.parse(value);
   var url = obj.url;
   var subpage = obj.subpage;
   var request = new HttpRequest();
   return request.get(url + subpage);

[comment]: # ({/new-7115c5ef})


[comment]: # ({new-18a089f4})
##### Multiple HTTP requests

Collect the content of both *https://www.example.com* and
*https://www.example.com/release\_notes*:

-   Create an item with type "Script".\
-   In the *Script* field, enter the following code:

``` {.java}
var request = new HttpRequest();
return request.get("https://www.example.com") + request.get("https://www.example.com/release_notes");
```

[comment]: # ({/new-18a089f4})

[comment]: # ({new-d837bcdd})
##### Logging

Add the "Log test" entry to the Zabbix server log and receive the item
value "1" in return:

-   Create an item with type "Script".\
-   In the *Script* field, enter the following code:

``` {.java}
Zabbix.log(3, 'Log test');
return 1;
```

[comment]: # ({/new-d837bcdd})
