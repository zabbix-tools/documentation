[comment]: # translation:outdated

[comment]: # ({new-d40475f4})
# 1 E-mail

[comment]: # ({/new-d40475f4})

[comment]: # ({new-9d59e93d})
#### Overview

To configure e-mail as the delivery channel for messages, you need to
configure e-mail as the media type and assign specific addresses to
users.

::: noteclassic
 Multiple notifications for single event will
be grouped together on the same email thread. 
:::

[comment]: # ({/new-9d59e93d})

[comment]: # ({new-3711e99d})
#### Configuration

To configure e-mail as the media type:

-   Go to *Administration → Media types*
-   Click on *Create media type* (or click on *E-mail* in the list of
    pre-defined media types).

The **Media type** tab contains general media type attributes:

![](../../../../../assets/en/manual/config/notifications/media/media_email.png)

All mandatory input fields are marked with a red asterisk.

The following parameters are specific for the e-mail media type:

|Parameter|Description|
|---------|-----------|
|*SMTP server*|Set an SMTP server to handle outgoing messages.|
|*SMTP server port*|Set the SMTP server port to handle outgoing messages.<br>This option is supported *starting with Zabbix 3.0*.|
|*SMTP helo*|Set a correct SMTP helo value, normally a domain name.|
|*SMTP email*|The address entered here will be used as the **From** address for the messages sent.<br>Adding a sender display name (like "Zabbix\_info" in *Zabbix\_info <zabbix\@company.com>* in the screenshot above) with the actual e-mail address is supported since Zabbix 2.2 version.<br>There are some restrictions on display names in Zabbix emails in comparison to what is allowed by RFC 5322, as illustrated by examples:<br>Valid examples:<br>*zabbix\@company.com* (only email address, no need to use angle brackets)<br>*Zabbix\_info <zabbix\@company.com>* (display name and email address in angle brackets)<br>*∑Ω-monitoring <zabbix\@company.com>* (UTF-8 characters in display name)<br>Invalid examples:<br>*Zabbix HQ zabbix\@company.com* (display name present but no angle brackets around email address)<br>*"Zabbix\\@\\<H(comment)Q\\>" <zabbix\@company.com>* (although valid by RFC 5322, quoted pairs and comments are not supported in Zabbix emails)|
|*Connection security*|Select the level of connection security:<br>**None** - do not use the [CURLOPT\_USE\_SSL](http://curl.haxx.se/libcurl/c/CURLOPT_USE_SSL.html) option<br>**STARTTLS** - use the CURLOPT\_USE\_SSL option with CURLUSESSL\_ALL value<br>**SSL/TLS** - use of CURLOPT\_USE\_SSL is optional<br>This option is supported *starting with Zabbix 3.0*.|
|*SSL verify peer*|Mark the checkbox to verify the SSL certificate of the SMTP server.<br>The value of "SSLCALocation" server configuration directive should be put into [CURLOPT\_CAPATH](http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html) for certificate validation.<br>This sets cURL option [CURLOPT\_SSL\_VERIFYPEER](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYPEER.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*SSL verify host*|Mark the checkbox to verify that the *Common Name* field or the *Subject Alternate Name* field of the SMTP server certificate matches.<br>This sets cURL option [CURLOPT\_SSL\_VERIFYHOST](http://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*Authentication*|Select the level of authentication:<br>**None** - no cURL options are set<br>(since 3.4.2) **Username and password** - implies "AUTH=\*" leaving the choice of authentication mechanism to cURL<br>(until 3.4.2) **Normal password** - [CURLOPT\_LOGIN\_OPTIONS](http://curl.haxx.se/libcurl/c/CURLOPT_LOGIN_OPTIONS.html) is set to "AUTH=PLAIN"<br>This option is supported *starting with Zabbix 3.0*.|
|*Username*|User name to use in authentication.<br>This sets the value of [CURLOPT\_USERNAME](http://curl.haxx.se/libcurl/c/CURLOPT_USERNAME.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*Password*|Password to use in authentication.<br>This sets the value of [CURLOPT\_PASSWORD](http://curl.haxx.se/libcurl/c/CURLOPT_PASSWORD.html).<br>This option is supported *starting with Zabbix 3.0*.|
|*Message format*|Select message format:<br>**HTML** - send as HTML<br>**Plain text** - send as plain text|

::: noteimportant
To make SMTP authentication options available,
Zabbix server should be compiled with the --with-libcurl
[compilation](/manual/installation/install#configure_the_sources) option
with cURL 7.20.0 or higher. 
:::

See also [common media type
parameters](/manual/config/notifications/media#common_parameters) for
details on how to configure default messages and alert processing
options.

[comment]: # ({/new-3711e99d})

[comment]: # ({new-98f29ec0})

#### Media type testing

To test whether a configured e-mail media type works correctly:

-   Locate the relevant e-mail in the [list](/manual/config/notifications/media#overview) of media types.
-   Click on *Test* in the last column of the list (a testing window
    will open).
-   Enter a *Send to* recipient address, message body and, optionally,
    subject.
-   Click on *Test* to send a test message.

Test success or failure message will be displayed in the same window:

![](../../../../../assets/en/manual/config/notifications/media/test_email0.png){width="600"}

[comment]: # ({/new-98f29ec0})

[comment]: # ({new-5e74f274})
#### User media

Once the e-mail media type is configured, go to the *Administration →
Users* section and edit user profile to assign e-mail media to the user.
Steps for setting up user media, being common for all media types, are
described on the [Media
types](/manual/config/notifications/media#user_media) page.

[comment]: # ({/new-5e74f274})
