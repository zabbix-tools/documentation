[comment]: # translation:outdated

[comment]: # ({new-46adf407})
# 3 User groups

[comment]: # ({/new-46adf407})

[comment]: # ({new-f58db4c9})
#### Overview

User groups allow to group users both for organizational purposes and
for assigning permissions to data. Permissions to monitoring data of
host groups are assigned to user groups, not individual users.

It may often make sense to separate what information is available for
one group of users and what - for another. This can be accomplished by
grouping users and then assigning varied permissions to host groups.

A user can belong to any number of groups.

[comment]: # ({/new-f58db4c9})

[comment]: # ({new-fe6bb4a1})
#### Configuration

To configure a user group:

-   Go to *Administration → User groups*
-   Click on *Create user group* (or on the group name to edit an
    existing group)
-   Edit group attributes in the form

The **User group** tab contains general group attributes:

![](../../../../assets/en/manual/config/user_group.png)

All mandatory input fields are marked with a red asterisk.

|Parameter|Description|
|---------|-----------|
|*Group name*|Unique group name.|
|*Users*|To add users to the group start typing the name of an existing user. When the dropdown with matching user names appears, scroll down to select.<br>Alternatively you may click the *Select* button to select users in a popup.|
|*Frontend access*|How the users of the group are authenticated.<br>**System default** - use default authentication method (set [globally](/manual/web_interface/frontend_sections/administration/authentication))<br>**Internal** - use Zabbix internal authentication (even if LDAP authentication is used globally).<br>Ignored if HTTP authentication is the global default.<br>**LDAP** - use LDAP authentication (even if internal authentication is used globally).<br>Ignored if HTTP authentication is the global default.<br>**Disabled** - access to Zabbix frontend is forbidden for this group|
|*Enabled*|Status of user group and group members.<br>*Checked* - user group and users are enabled<br>*Unchecked* - user group and users are disabled|
|*Debug mode*|Mark this checkbox to activate [debug mode](/manual/web_interface/debug_mode) for the users.|

The **Permissions** tab allows you to specify user group access to host
group (and thereby host) data:

![](../../../../assets/en/manual/config/user_group_permissions.png){width="600"}

Current permissions to host groups are displayed in the *Permissions*
block.

If current permissions of the host group are inherited by all nested
host groups, that is indicated by the *including subgroups* text in the
parenthesis after the host group name.

You may change the level of access to a host group:

-   **Read-write** - read-write access to a host group;
-   **Read** - read-only access to a host group;
-   **Deny** - access to a host group denied;
-   **None** - no permissions are set.

Use the selection field below to select host groups and the level of
access to them (note that selecting *None* will remove host group from
the list if the group is already in the list). If you wish to include
nested host groups, mark the *Include subgroups* checkbox. This field is
auto-complete so starting to type the name of a host group will offer a
dropdown of matching groups. If you wish to see all host groups, click
on *Select*.

Note that it is possible for Super Admin users in host group
[configuration](/manual/config/hosts/host#creating_a_host_group) to
enforce the same level of permissions to the nested host groups as the
parent host group.

The **Tag filter** tab allows you to set tag-based permissions for user
groups to see problems filtered by tag name and its value:

![](../../../../assets/en/manual/config/user_group_tags.png)

To select a host group to apply a tag filter for, click *Select* to get
the complete list of existing host groups or start to type the name of a
host group to get a dropdown of matching groups. If you want to apply
tag filters to nested host groups, mark the *Include subgroups*
checkbox.

Tag filter allows to separate the access to host group from the
possibility to see problems.

For example, if a database administrator needs to see only "MySQL"
database problems, it is required to create a user group for database
administrators first, than specify "Service" tag name and "MySQL" value.

![user\_group\_tag\_filter\_2.png](../../../../assets/en/manual/config/user_group_tag_filter_2.png)

If "Service" tag name is specified and value field is left blank,
corresponding user group will see all problems for selected host group
with tag name "Service". If both tag name and value fields are left
blank but host group selected, corresponding user group will see all
problems for selected host group. Make sure a tag name and tag value are
correctly specified otherwise a corresponding user group will not see
any problems.

Let's review an example when a user is a member of several user groups
selected. Filtering in this case will use OR condition for tags.

|   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|
|**User group A**|<|<|**User group B**|<|<|**Visible result for a user (member) of both groups**|
|*Tag filter*|<|<|<|<|<|<|
|*Host group*|*Tag name*|*Tag value*|*Host group*|*Tag name*|*Tag value*|<|
|Templates/Databases|Service|MySQL|Templates/Databases|Service|Oracle|Service: MySQL or Oracle problems visible|
|Templates/Databases|*blank*|*blank*|Templates/Databases|Service|Oracle|All problems visible|
|*not selected*|*blank*|*blank*|Templates/Databases|Service|Oracle|<Service:Oracle> problems visible|

::: noteimportant
 Adding a filter (for example, all tags in a
certain host group "Templates/Databases") results in not being able to
see the problems of other host groups.
:::

[comment]: # ({/new-fe6bb4a1})

[comment]: # ({new-e74e2150})
#### Host access from several user groups

A user may belong to any number of user groups. These groups may have
different access permissions to hosts.

Therefore, it is important to know what hosts an unprivileged user will
be able to access as a result. For example, let us consider how access
to host **X** (in Hostgroup 1) will be affected in various situations
for a user who is in user groups A and B.

-   If Group A has only *Read* access to Hostgroup 1, but Group B
    *Read-write* access to Hostgroup 1, the user will get **Read-write**
    access to 'X'.

::: noteimportant
“Read-write” permissions have precedence over
“Read” permissions starting with Zabbix 2.2.
:::

-   In the same scenario as above, if 'X' is simultaneously also in
    Hostgroup 2 that is **denied** to Group A or B, access to 'X' will
    be **unavailable**, despite a *Read-write* access to Hostgroup 1.
-   If Group A has no permissions defined and Group B has a *Read-write*
    access to Hostgroup 1, the user will get **Read-write** access to
    'X'.
-   If Group A has *Deny* access to Hostgroup 1 and Group B has a
    *Read-write* access to Hostgroup 1, the user will get access to 'X'
    **denied**.

[comment]: # ({/new-e74e2150})

[comment]: # ({new-931529bc})
#### Other details

-   An Admin level user with *Read-write* access to a host will not be
    able to link/unlink templates, if he has no access to the
    *Templates* group. With *Read* access to *Templates* group he will
    be able to link/unlink templates to the host, however, will not see
    any templates in the template list and will not be able to operate
    with templates in other places.
-   An Admin level user with *Read* access to a host will not see the
    host in the configuration section host list; however, the host
    triggers will be accessible in IT service configuration.
-   Any non-Super Admin user (including 'guest') can see network maps as
    long as the map is empty or has only images. When hosts, host groups
    or triggers are added to the map, permissions are respected.
-   Zabbix server will not send notifications to users defined as action
    operation recipients if access to the concerned host is explicitly
    "denied".

[comment]: # ({/new-931529bc})
