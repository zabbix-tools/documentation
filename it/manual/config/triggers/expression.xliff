<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/config/triggers/expression.md">
    <body>
      <trans-unit id="510ac9cc" xml:space="preserve">
        <source># 2 Trigger expression</source>
      </trans-unit>
      <trans-unit id="473bcd5e" xml:space="preserve">
        <source>#### Overview

The expressions used in [triggers](/manual/config/triggers) are very
flexible. You can use them to create complex logical tests regarding
monitored statistics.

A simple expression uses a **function** that is applied to the item with
some parameters. The function returns a result that is compared to the
threshold, using an operator and a constant.

The syntax of a simple useful expression is
`function(/host/key,parameter)&lt;operator&gt;&lt;constant&gt;`.

For example:

      min(/Zabbix server/net.if.in[eth0,bytes],5m)&gt;100K

will trigger if the number of received bytes during the last five
minutes was always over 100 kilobytes.

While the syntax is exactly the same, from the functional point of view
there are two types of trigger expressions:

-   problem expression - defines the conditions of the problem
-   recovery expression (optional) - defines additional conditions of
    the problem resolution

When defining a problem expression alone, this expression will be used
both as the problem threshold and the problem recovery threshold. As
soon as the problem expression evaluates to TRUE, there is a problem. As
soon as the problem expression evaluates to FALSE, the problem is
resolved.

When defining both problem expression and the supplemental recovery
expression, problem resolution becomes more complex: not only the
problem expression has to be FALSE, but also the recovery expression has
to be TRUE. This is useful to create [hysteresis](#hysteresis) and avoid
trigger flapping.</source>
      </trans-unit>
      <trans-unit id="d4e5ef03" xml:space="preserve">
        <source>#### Functions

Functions allow to calculate the collected values (average, minimum,
maximum, sum), find strings, reference current time and other factors.

A complete list of [supported functions](/manual/appendix/functions) is
available.

Typically functions return numeric values for comparison. When returning
strings, comparison is possible with the **=** and **&lt;&gt;**
operators (see [example](#example_14)).</source>
      </trans-unit>
      <trans-unit id="b255de16" xml:space="preserve">
        <source>#### Function parameters

Function parameters allow to specify:

-   host and item key (functions referencing the host item history only)
-   function-specific parameters
-   other expressions (not available for functions referencing the host
    item history, see [other expressions](#other_expressions) for
    examples)

The host and item key can be specified as `/host/key`. The referenced
item must be in a supported state (except for **nodata()** function,
which is calculated for unsupported items as well).

While other trigger expressions as function parameters are limited to
non-history functions in triggers, this limitation does not apply in
[calculated items](/manual/config/items/itemtypes/calculated).</source>
      </trans-unit>
      <trans-unit id="c5d3ec54" xml:space="preserve">
        <source>##### Other expressions

Function parameters may contain other expressions, as in the following
syntax:

    min(min(/host/key,1h),min(/host2/key2,1h)*10)

Note that other expressions may not be used, if the function references
item history. For example, the following syntax is not allowed:

~~`min(/host/key,#5*10)`~~</source>
      </trans-unit>
      <trans-unit id="10bc91a2" xml:space="preserve">
        <source>#### Operators

The following operators are supported for triggers **(in descending
priority of execution)**:

|Priority|Operator|Definition|**Notes for [unknown values](/manual/config/triggers/expression#expressions_with_unknown-operands)**|Force cast operand to float ^**1**^|
|-|-|----|----------------|--|
|**1**|**-**|Unary minus|**-**Unknown → Unknown|Yes|
|**2**|**not**|Logical NOT|**not** Unknown → Unknown|Yes|
|**3**|**\***|Multiplication|0 **\*** Unknown → Unknown&lt;br&gt;(yes, Unknown, not 0 - to not lose&lt;br&gt;Unknown in arithmetic operations)&lt;br&gt;1.2 **\*** Unknown → Unknown|Yes|
|&lt;|**/**|Division|Unknown **/** 0 → error&lt;br&gt;Unknown **/** 1.2 → Unknown&lt;br&gt;0.0 **/** Unknown → Unknown|Yes|
|**4**|**+**|Arithmetical plus|1.2 **+** Unknown → Unknown|Yes|
|&lt;|**-**|Arithmetical minus|1.2 **-** Unknown → Unknown|Yes|
|**5**|**\&lt;**|Less than. The operator is defined as:&lt;br&gt;&lt;br&gt;A&lt;B ⇔ (A&lt;B-0.000001)|1.2 **&lt;** Unknown → Unknown|Yes|
|&lt;|**&lt;=**|Less than or equal to. The operator is defined as:&lt;br&gt;&lt;br&gt;A&lt;=B ⇔ (A≤B+0.000001)|Unknown **&lt;=** Unknown → Unknown|Yes|
|&lt;|**&gt;**|More than. The operator is defined as:&lt;br&gt;&lt;br&gt;A&gt;B ⇔ (A&gt;B+0.000001)| |Yes|
|&lt;|**&gt;=**|More than or equal to. The operator is defined as:&lt;br&gt;&lt;br&gt;A&gt;=B ⇔ (A≥B-0.000001)| |Yes|
|**6**|**=**|Is equal. The operator is defined as:&lt;br&gt;&lt;br&gt;A=B ⇔ (A≥B-0.000001) and (A≤B+0.000001)| |No ^**1**^|
|&lt;|**&lt;&gt;**|Not equal. The operator is defined as:&lt;br&gt;&lt;br&gt;A&lt;&gt;B ⇔ (A&lt;B-0.000001) or (A&gt;B+0.000001)| |No ^**1**^|
|**7**|**and**|Logical AND|0 **and** Unknown → 0&lt;br&gt;1 **and** Unknown → Unknown&lt;br&gt;Unknown **and** Unknown → Unknown|Yes|
|**8**|**or**|Logical OR|1 **or** Unknown → 1&lt;br&gt;0 **or** Unknown → Unknown&lt;br&gt;Unknown **or** Unknown → Unknown|Yes|

^**1**^ String operand is still cast to numeric if:

-   another operand is numeric
-   operator other than **=** or **&lt;&gt;** is used on an operand

(If the cast fails - numeric operand is cast to a string operand and
both operands get compared as strings.)

**not**, **and** and **or** operators are case-sensitive and must be in
lowercase. They also must be surrounded by spaces or parentheses.

All operators, except unary **-** and **not**, have left-to-right
associativity. Unary **-** and **not** are non-associative (meaning
**-(-1)** and **not (not 1)** should be used instead of **--1** and
**not not 1**).

Evaluation result:

-   **&lt;**, **&lt;=**, **&gt;**, **&gt;=**, **=**, **&lt;&gt;**
    operators shall yield '1' in the trigger expression if the specified
    relation is true and '0' if it is false. If at least one operand is
    Unknown the result is Unknown;
-   **and** for known operands shall yield '1' if both of its operands
    compare unequal to '0'; otherwise, it yields '0'; for unknown
    operands **and** yields '0' only if one operand compares equal to
    '0'; otherwise, it yields 'Unknown';
-   **or** for known operands shall yield '1' if either of its operands
    compare unequal to '0'; otherwise, it yields '0'; for unknown
    operands **or** yields '1' only if one operand compares unequal to
    '0'; otherwise, it yields 'Unknown';
-   The result of the logical negation operator **not** for a known
    operand is '0' if the value of its operand compares unequal to '0';
    '1' if the value of its operand compares equal to '0'. For unknown
    operand **not** yields 'Unknown'.</source>
      </trans-unit>
      <trans-unit id="195c3f4f" xml:space="preserve">
        <source>#### Value caching

Values required for trigger evaluation are cached by Zabbix server.
Because of this trigger evaluation causes a higher database load for
some time after the server restarts. The value cache is not cleared when
item history values are removed (either manually or by housekeeper), so
the server will use the cached values until they are older than the time
periods defined in trigger functions or server is restarted.</source>
      </trans-unit>
      <trans-unit id="abac99e3" xml:space="preserve">
        <source>#### Examples of triggers</source>
      </trans-unit>
      <trans-unit id="17b607dc" xml:space="preserve">
        <source>##### Example 1

The processor load is too high on Zabbix server.

    last(/Zabbix server/system.cpu.load[all,avg1])&gt;5

By using the function 'last()', we are referencing the most recent
value. `/Zabbix server/system.cpu.load[all,avg1]` gives a short name of
the monitored parameter. It specifies that the host is 'Zabbix server'
and the key being monitored is 'system.cpu.load\[all,avg1\]'. Finally,
`&gt;5` means that the trigger is in the PROBLEM state whenever the most
recent processor load measurement from Zabbix server is greater than 5.</source>
      </trans-unit>
      <trans-unit id="9a129732" xml:space="preserve">
        <source>##### Example 2

www.example.com is overloaded.

    last(/www.example.com/system.cpu.load[all,avg1])&gt;5 or min(/www.example.com/system.cpu.load[all,avg1],10m)&gt;2 

The expression is true when either the current processor load is more
than 5 or the processor load was more than 2 during last 10 minutes.</source>
      </trans-unit>
      <trans-unit id="49592749" xml:space="preserve">
        <source>##### Example 3

/etc/passwd has been changed.

    last(/www.example.com/vfs.file.cksum[/etc/passwd],#1)&lt;&gt;last(/www.example.com/vfs.file.cksum[/etc/passwd],#2)

The expression is true when the previous value of /etc/passwd checksum
differs from the most recent one.

Similar expressions could be useful to monitor changes in important
files, such as /etc/passwd, /etc/inetd.conf, /kernel, etc.</source>
      </trans-unit>
      <trans-unit id="46e72843" xml:space="preserve">
        <source>##### Example 4

Someone is downloading a large file from the Internet.

Use of function min:

    min(/www.example.com/net.if.in[eth0,bytes],5m)&gt;100K

The expression is true when number of received bytes on eth0 is more
than 100 KB within last 5 minutes.</source>
      </trans-unit>
      <trans-unit id="7956e04e" xml:space="preserve">
        <source>##### Example 5

Both nodes of clustered SMTP server are down.

Note use of two different hosts in one expression:

    last(/smtp1.example.com/net.tcp.service[smtp])=0 and last(/smtp2.example.com/net.tcp.service[smtp])=0

The expression is true when both SMTP servers are down on both
smtp1.example.com and smtp2.example.com.</source>
      </trans-unit>
      <trans-unit id="62cfb014" xml:space="preserve">
        <source>##### Example 6

Zabbix agent needs to be upgraded.

Use of function find():

    find(/example.example.com/agent.version,,"like","beta8")=1

The expression is true if Zabbix agent has version beta8.</source>
      </trans-unit>
      <trans-unit id="567b88a9" xml:space="preserve">
        <source>##### Example 7

Server is unreachable.

    count(/example.example.com/icmpping,30m,,"0")&gt;5

The expression is true if host "example.example.com" is unreachable more
than 5 times in the last 30 minutes.</source>
      </trans-unit>
      <trans-unit id="2f13cb92" xml:space="preserve">
        <source>##### Example 8

No heartbeats within last 3 minutes.

Use of function nodata():

    nodata(/example.example.com/tick,3m)=1

To make use of this trigger, 'tick' must be defined as a Zabbix
[trapper](/manual/config/items/itemtypes/trapper) item. The host should
periodically send data for this item using zabbix\_sender. If no data is
received within 180 seconds, the trigger value becomes PROBLEM.

*Note* that 'nodata' can be used for any item type.</source>
      </trans-unit>
      <trans-unit id="2c48905a" xml:space="preserve">
        <source>##### Example 9

CPU activity at night time.

Use of function time():

    min(/Zabbix server/system.cpu.load[all,avg1],5m)&gt;2 and time()&lt;060000

The trigger may change its state to problem only at night time (00:00 - 06:00).</source>
      </trans-unit>
      <trans-unit id="7e0ba13a" xml:space="preserve">
        <source>##### Example 10

CPU activity at any time with exception.

Use of function time() and **not** operator:

    min(/zabbix/system.cpu.load[all,avg1],5m)&gt;2
    and not (dayofweek()=7 and time()&gt;230000)
    and not (dayofweek()=1 and time()&lt;010000)

The trigger may change its state to problem at any time,
except for 2 hours on a week change (Sunday, 23:00 - Monday, 01:00).</source>
      </trans-unit>
      <trans-unit id="0e6bfc51" xml:space="preserve">
        <source>##### Example 11

Check if client local time is in sync with Zabbix server time.

Use of function fuzzytime():

    fuzzytime(/MySQL_DB/system.localtime,10s)=0

The trigger will change to the problem state in case when local time on
server MySQL\_DB and Zabbix server differs by more than 10 seconds. Note
that 'system.localtime' must be configured as a [passive
check](/manual/appendix/items/activepassive#passive_checks).</source>
      </trans-unit>
      <trans-unit id="47b2e2fd" xml:space="preserve">
        <source>##### Example 12

Comparing average load today with average load of the same time
yesterday (using time shift as `now-1d`).

    avg(/server/system.cpu.load,1h)/avg(/server/system.cpu.load,1h:now-1d)&gt;2

The trigger will fire if the average load of the last hour tops the
average load of the same hour yesterday more than two times.</source>
      </trans-unit>
      <trans-unit id="bb0f5278" xml:space="preserve">
        <source>##### Example 13

Using the value of another item to get a trigger threshold:

    last(/Template PfSense/hrStorageFree[{#SNMPVALUE}])&lt;last(/Template PfSense/hrStorageSize[{#SNMPVALUE}])*0.1

The trigger will fire if the free storage drops below 10 percent.</source>
      </trans-unit>
      <trans-unit id="0cc34ec7" xml:space="preserve">
        <source>##### Example 14

Using [evaluation result](#operators) to get the number of triggers over
a threshold:

    (last(/server1/system.cpu.load[all,avg1])&gt;5) + (last(/server2/system.cpu.load[all,avg1])&gt;5) + (last(/server3/system.cpu.load[all,avg1])&gt;5)&gt;=2

The trigger will fire if at least two of the triggers in the expression
are in a problem state.</source>
      </trans-unit>
      <trans-unit id="adfc6b9d" xml:space="preserve">
        <source>##### Example 15

Comparing string values of two items - operands here are functions that
return strings.

Problem: create an alert if Ubuntu version is different on different
hosts

    last(/NY Zabbix server/vfs.file.contents[/etc/os-release])&lt;&gt;last(/LA Zabbix server/vfs.file.contents[/etc/os-release])</source>
      </trans-unit>
      <trans-unit id="c2b4949f" xml:space="preserve">
        <source>##### Example 16

Comparing two string values - operands are:

-   a function that returns a string
-   a combination of macros and strings

Problem: detect changes in the DNS query

The item key is:

    net.dns.record[8.8.8.8,{$WEBSITE_NAME},{$DNS_RESOURCE_RECORD_TYPE},2,1]

with macros defined as

    {$WEBSITE_NAME} = example.com
    {$DNS_RESOURCE_RECORD_TYPE} = MX

and normally returns:

    example.com           MX       0 mail.example.com

So our trigger expression to detect if the DNS query result deviated
from the expected result is:

    last(/Zabbix server/net.dns.record[8.8.8.8,{$WEBSITE_NAME},{$DNS_RESOURCE_RECORD_TYPE},2,1])&lt;&gt;"{$WEBSITE_NAME}           {$DNS_RESOURCE_RECORD_TYPE}       0 mail.{$WEBSITE_NAME}"

Notice the quotes around the second operand.</source>
      </trans-unit>
      <trans-unit id="60fad4fc" xml:space="preserve">
        <source>##### Example 17

Comparing two string values - operands are:

-   a function that returns a string
-   a string constant with special characters \\ and "

Problem: detect if the `/tmp/hello` file content is equal to:

    \" //hello ?\"

Option 1) write the string directly

    last(/Zabbix server/vfs.file.contents[/tmp/hello])="\\\" //hello ?\\\""

Notice how \\ and " characters are escaped when the string gets compared
directly.

Option 2) use a macro

    {$HELLO_MACRO} = \" //hello ?\"

in the expression:

    last(/Zabbix server/vfs.file.contents[/tmp/hello])={$HELLO_MACRO}</source>
      </trans-unit>
      <trans-unit id="6906462b" xml:space="preserve">
        <source>##### Example 18

Comparing long-term periods.

Problem: Load of Exchange server increased by more than 10% last month

    trendavg(/Exchange/system.cpu.load,1M:now/M)&gt;1.1*trendavg(/Exchange/system.cpu.load,1M:now/M-1M)

You may also use the [Event
name](/manual/config/triggers/trigger#configuration) field in trigger
configuration to build a meaningful alert message, for example to
receive something like

`"Load of Exchange server increased by 24% in July (0.69) comparing to June (0.56)"`

the event name must be defined as:

    Load of {HOST.HOST} server increased by {{?100*trendavg(//system.cpu.load,1M:now/M)/trendavg(//system.cpu.load,1M:now/M-1M)}.fmtnum(0)}% in {{TIME}.fmttime(%B,-1M)} ({{?trendavg(//system.cpu.load,1M:now/M)}.fmtnum(2)}) comparing to {{TIME}.fmttime(%B,-2M)} ({{?trendavg(//system.cpu.load,1M:now/M-1M)}.fmtnum(2)})

It is also useful to allow manual closing in trigger configuration for
this kind of problem.</source>
      </trans-unit>
      <trans-unit id="3f1b1c81" xml:space="preserve">
        <source>#### Hysteresis

Sometimes an interval is needed between problem and recovery states,
rather than a simple threshold. For example, if we want to define a
trigger that reports a problem when server room temperature goes above
20°C and we want it to stay in the problem state until the temperature
drops below 15°C, a simple trigger threshold at 20°C will not be enough.

Instead, we need to define a trigger expression for the problem event
first (temperature above 20°C). Then we need to define an additional
recovery condition (temperature below 15°C). This is done by defining an
additional *Recovery expression* parameter when
[defining](/manual/config/triggers/trigger) a trigger.

In this case, problem recovery will take place in two steps:

-   First, the problem expression (temperature above 20°C) will have to
    evaluate to FALSE
-   Second, the recovery expression (temperature below 15°C) will have
    to evaluate to TRUE

The recovery expression will be evaluated only when the problem event is
resolved first.

::: notewarning
The recovery expression being TRUE alone does not
resolve a problem if the problem expression is still TRUE!
:::</source>
      </trans-unit>
      <trans-unit id="942a7e0e" xml:space="preserve">
        <source>##### Example 1

Temperature in server room is too high.

Problem expression:

    last(/server/temp)&gt;20

Recovery expression:

    last(/server/temp)&lt;=15</source>
      </trans-unit>
      <trans-unit id="bddf0717" xml:space="preserve">
        <source>##### Example 2

Free disk space is too low.

Problem expression: it is less than 10GB for last 5 minutes

    max(/server/vfs.fs.size[/,free],5m)&lt;10G

Recovery expression: it is more than 40GB for last 10 minutes

    min(/server/vfs.fs.size[/,free],10m)&gt;40G</source>
      </trans-unit>
      <trans-unit id="6fc2eb1c" xml:space="preserve">
        <source>#### Expressions with unknown operands

Generally an unknown operand (such as an unsupported item) in the expression will immediately render the trigger value to `Unknown`.

However, in some cases unknown operands (unsupported items, function errors) are admitted into expression evaluation:

-   The `nodata()` function is evaluated regardless of whether the referenced item is supported or not.
-   Logical expressions with OR and AND can be evaluated to known values
    in two cases regardless of unknown operands:
    -   **Case 1**: "`1 or some_function(unsupported_item1) or some_function(unsupported_item2) or ...`" can be evaluated to known result ('1' or "Problem"),
    -   **Case 2**: "`0 and some_function(unsupported_item1) and some_function(unsupported_item2) and ...`" can be evaluated to known result ('0' or "OK").\
        Zabbix tries to evaluate such logical expressions by taking unsupported
        items as unknown operands. In the two cases above a known value will be produced ("Problem" or "OK", respectively); in **all other** cases the trigger will evaluate to `Unknown`.
-   If the function evaluation for a supported item results in error, the
    function value becomes `Unknown` and it takes part as unknown operand 
    in further expression evaluation.

Note that unknown operands may "disappear" only in logical expressions as
described above. In arithmetic expressions unknown operands always lead to
the result `Unknown` (except division by 0).

::: noteimportant
An expression that results in `Unknown` does not change the trigger state ("Problem/OK"). 
So, if it was "Problem" (see Case 1), it stays in the same problem state even if the known 
part is resolved ('1' becomes '0'), because the expression is now evaluated to `Unknown` 
and that does not change the trigger state.
:::

If a trigger expression with several unsupported items evaluates to
`Unknown` the error message in the frontend refers to the last
unsupported item evaluated.</source>
      </trans-unit>
      <trans-unit id="contribute" xml:space="preserve">
        <source>
::: note-contribute
Have a trigger expressions example that might be useful to others? Use the [Example suggestion form](#report-example) to send it to Zabbix developers.
:::</source>
      </trans-unit>
      <trans-unit id="cf9e09bb" xml:space="preserve">
        <source>##### Function-specific parameters

Function-specific parameters are placed after the item key and are
separated from the item key by a comma. See the [supported
functions](/manual/appendix/functions) for a complete list of these
parameters.

Most of numeric functions accept time as a parameter. You may use
seconds or [time suffixes](/manual/appendix/suffixes) to indicate time.
Preceded by a hashtag, the parameter has a different meaning:

|Expression|Description|
|----------|-----------|
|**sum**(/host/key,**10m)**|Sum of values in the last 10 minutes.|
|**sum**(/host/key,**\#10)**|Sum of the last ten values.|

Parameters with a hashtag have a different meaning with the function
**last** - they denote the Nth previous value, so given the values 3, 7,
2, 6, 5 (from the most recent to the least recent):

-   `last(/host/key,#2)` would return '7'
-   `last(/host/key,#5)` would return '5'</source>
      </trans-unit>
      <trans-unit id="6699e631" xml:space="preserve">
        <source>##### Time shift

An optional time shift is supported with time or value count as the
function parameter. This parameter allows to reference data from a
period of time in the past.

Time shift starts with `now` - specifying the current time, and is
followed by `+N&lt;time unit&gt;` or `-N&lt;time unit&gt;` - to add or subtract N
time units.

For example, `avg(/host/key,1h:now-1d)` will return the average value
for an hour one day ago.

::: noteimportant
Time shift specified in months (M) and years (y) is only supported for [trend functions](/manual/appendix/functions/trends). Other functions support seconds (s), minutes (m), hours (h), days (d), and weeks (w).
:::

**Time shift with absolute time periods**

Absolute time periods are supported in the time shift parameter, for example, midnight
to midnight for a day, Monday-Sunday for a week, first day-last day of
the month for a month.

Time shift for absolute time periods starts with `now` - specifying the
current time, and is followed by any number of time operations:
`/&lt;time unit&gt;` - defines the beginning and end of the time unit, for
example, midnight to midnight for a day, `+N&lt;time unit&gt;` or
`-N&lt;time unit&gt;` - to add or subtract N time units.

Please note that the value of time shift can be greater or equal to 0,
while the time period minimum value is 1.

|Parameter|Description|
|--|--------|
|1d:now/d|Yesterday|
|1d:now/d+1d|Today|
|2d:now/d+1d|Last 2 days|
|1w:now/w|Last week|
|1w:now/w+1w|This week|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
