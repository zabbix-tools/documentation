[comment]: # translation:outdated

[comment]: # ({new-ad0fbcd3})
# 3 Manual closing of problems

[comment]: # ({/new-ad0fbcd3})

[comment]: # ({new-0eef1445})
#### Overview

While generally problem events are resolved automatically when trigger
status goes from 'Problem' to 'OK', there may be cases when it is
difficult to determine if a problem has been resolved by means of a
trigger expression. In such cases, the problem needs to be resolved
manually.

For example, *syslog* may report that some kernel parameters need to be
tuned for optimal performance. In this case the issue is reported to
Linux administrators, they fix it and then close the problem manually.

Problems can be closed manually only for triggers with the *Allow manual
close* option enabled.

When a problem is "manually closed", Zabbix generates a new internal
task for Zabbix server. Then the *task manager* process executes this
task and generates an OK event, therefore closing problem event.

A manually closed problem does not mean that the underlying trigger will
never go into a 'Problem' state again. The trigger expression is
re-evaluated and may result in a problem:

-   When new data arrive for any item included in the trigger expression
    (note that the values discarded by a throttling preprocessing step
    are not considered as received and will not cause trigger expression
    to be re-evaluated);
-   When time-based functions are used in the expression. Complete
    time-based function list can be found on [Triggers
    page](/manual/config/triggers).

[comment]: # ({/new-0eef1445})

[comment]: # ({new-ece5ae52})
#### Configuration

Two steps are required to close a problem manually.

[comment]: # ({/new-ece5ae52})

[comment]: # ({new-8e977ac6})
##### Trigger configuration

In trigger configuration, enable the *Allow manual close* option.

![](../../../../assets/en/manual/config/manual_close_conf1.png)

[comment]: # ({/new-8e977ac6})

[comment]: # ({new-229474e2})
##### Problem update window

If a problem arises for a trigger with the *Manual close* flag, you can
open the [problem update](/manual/acknowledges#updating_problems) popup
window of that problem and close the problem manually.

To close the problem, check the *Close problem* option in the form and
click on *Update*.

![](../../../../assets/en/manual/config/close_problem1.png)

All mandatory input fields are marked with a red asterisk.

The request is processed by Zabbix server. Normally it will take a few
seconds to close the problem. During that process *CLOSING* is displayed
in *Monitoring* → *Problems* as the status of the problem.

[comment]: # ({/new-229474e2})

[comment]: # ({new-a2933506})
#### Verification

It can be verified that a problem has been closed manually:

-   in event details, available through *Monitoring* → *Problems*;
-   by using the {EVENT.UPDATE.HISTORY} macro in notification messages
    that will provide this information.

[comment]: # ({/new-a2933506})
