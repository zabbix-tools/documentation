[comment]: # translation:outdated

[comment]: # ({new-31b6b7d8})
# 1 Hosts and host groups

[comment]: # ({/new-31b6b7d8})

[comment]: # ({new-61c2f2bf})
#### What is a "host"?

Typical Zabbix hosts are the devices you wish to monitor (servers,
workstations, switches, etc).

Creating hosts is one of the first monitoring tasks in Zabbix. For
example, if you want to monitor some parameters on a server "x", you
must first create a host called, say, "Server X" and then you can look
to add monitoring items to it.

Hosts are organized into host groups.

Proceed to [creating and configuring a host](/manual/config/hosts/host).

[comment]: # ({/new-61c2f2bf})
