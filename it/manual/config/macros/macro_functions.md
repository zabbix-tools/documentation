[comment]: # translation:outdated

[comment]: # ({new-0458cee0})
# 1 Macro functions

[comment]: # ({/new-0458cee0})

[comment]: # ({new-c1632a9e})
#### Overview

Macro functions offer the ability to customize
[macro](/manual/config/macros) values.

Sometimes a macro may resolve to a value that is not necessarily easy to
work with. It may be long or contain a specific substring of interest
that you would like to extract. This is where macro functions can be
useful.

The syntax of a macro function is:

    {<macro>.<func>(<params>)}

where:

-   <macro> - the macro to customize (for example {ITEM.VALUE} or
    {\#LLDMACRO})
-   <func> - the function to apply
-   <params> - a comma-delimited list of function parameters.
    Parameters must be quoted if they start with '' '' (space), `"` or
    contain `)`, `,`.

For example:

    {{TIME}.fmttime(format,time_shift)}
    {{ITEM.VALUE}.regsub(pattern, output)}
    {{#LLDMACRO}.regsub(pattern, output)}

[comment]: # ({/new-c1632a9e})

[comment]: # ({new-a0ad7754})
#### Supported macro functions

|FUNCTION|<|<|<|<|
|--------|-|-|-|-|
|<|**Description**|**Parameters**|**Supported for**|<|
|**fmtnum** (<digits>)|<|<|<|<|
|<|Number formatting to control the number of digits printed after the decimal point.|**digits** - the number of digits after decimal point. No trailing zeros will be produced.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}<br>Expression macros|<|
|**fmttime** (<format>,<time\_shift>)|<|<|<|<|
|<|Time formatting.|**format** - mandatory format string, compatible with strftime function formatting<br>**time\_shift** - the time shift applied to the time before formatting; should start with<br>`-<N><time_unit>` or `+<N><time_unit>`, where<br>*N* - the number of time units to add or subtract;<br>*time\_unit* - h (hour), d (day), w (week), M (month) or y (year).<br>Since Zabbix 5.4, time\_shift parameter supports multi-step time operations and may include `/<time_unit>` for shifting to the beginning of the time unit (`/d` - midnight, `/w` - 1st day of the week (Monday), `/M` - 1st day of the month, etc.). Examples:<br>`-1w` - exactly 7 days back;<br>`-1w/w` - Monday of the previous week;<br>`-1w/w+1d` - Tuesday of the previous week.<br>Note, that time operations are calculated from left to right without priorities. For example, `-1M/d+1h/w` will be parsed as `((-1M/d)+1h)/w`.|{TIME}|<|
|**iregsub** (<pattern>,<output>)|<|<|<|<|
|<|Substring extraction by a regular expression match (case insensitive).|**pattern** - the regular expression to match<br>**output** - the output options. **\\1 - \\9** placeholders are supported to capture groups. **\\0** returns the matched text.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}<br>[Low-level discovery macros](/manual/config/macros/lld_macros) (except in low-level discovery rule filter)|<|
|**regsub** (<pattern>,<output>)|<|<|<|<|
|<|Substring extraction by a regular expression match (case sensitive).|**pattern** - the regular expression to match<br>**output** - the output options. **\\1 - \\9** placeholders are supported to capture groups. **\\0** returns the matched text.|{ITEM.VALUE}<br>{ITEM.LASTVALUE}<br>[Low-level discovery macros](/manual/config/macros/lld_macros) (except in low-level discovery rule filter)|<|

If a function is used in a [supported
location](/manual/appendix/macros/supported_by_location), but applied to
a macro not supporting macro functions, then the macro evaluates to
'UNKNOWN'.

If pattern is not a correct regular expression then the macro evaluates
to 'UNKNOWN' (excluding low-level discovery macros where the function
will be ignored in that case and macro will remain unexpanded)

If a macro function is applied to the macro in locations not supporting
macro functions then the function is ignored.

[comment]: # ({/new-a0ad7754})

[comment]: # ({new-9423d893})
#### Examples

The ways in which macro functions can be used to customize macro values
is illustrated in the following examples on received values:

|Received value|Macro|Output|
|--------------|-----|------|
|`24.3413523`|`{{ITEM.VALUE}.fmtnum(2)}`|`24.34`|
|`24.3413523`|`{{ITEM.VALUE}.fmtnum(0)}`|`24`|
|`12:36:01`|`{{TIME}.fmttime(%B)}`|`October`|
|`12:36:01`|`{{TIME}.fmttime(%d %B,-1M/M)}`|`1 September`|
|`123Log line`|`{{ITEM.VALUE}.regsub(^[0-9]+, Problem)}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", "Problem")}`|`Problem`|
|`123 Log line`|`{{ITEM.VALUE}.regsub("^([0-9]+)", Problem ID: \1)}`|`Problem ID: 123`|
|`Log line`|`{{ITEM.VALUE}.regsub(".*", "Problem ID: \1")}`|''Problem ID: ''|
|`MySQL crashed errno 123`|`{{ITEM.VALUE}.regsub("^(\w+).*?([0-9]+)", " Problem ID: \1_\2 ")}`|'' Problem ID: MySQL\_123 ''|
|`123 Log line`|`{{ITEM.VALUE}.regsub("([1-9]+", "Problem ID: \1")}`|`*UNKNOWN*` (invalid regular expression)|
|`customername_1`|`{{#IFALIAS}.regsub("(.*)_([0-9]+)", \1)}`|`customername`|
|`customername_1`|`{{#IFALIAS}.regsub("(.*)_([0-9]+)", \2)}`|`1`|
|`customername_1`|`{{#IFALIAS}.regsub("(.*)_([0-9]+", \1)}`|`{{#IFALIAS}.regsub("(.*)_([0-9]+", \1)}` (invalid regular expression)|
|`customername_1`|`{$MACRO:"{{#IFALIAS}.regsub(\"(.*)_([0-9]+)\", \1)}"}`|`{$MACRO:"customername"}`|
|`customername_1`|`{$MACRO:"{{#IFALIAS}.regsub(\"(.*)_([0-9]+)\", \2)}"}`|`{$MACRO:"1"}`|
|`customername_1`|`{$MACRO:"{{#IFALIAS}.regsub(\"(.*)_([0-9]+\", \1)}"}`|`{$MACRO:"{{#M}.regsub(\"(.*)_([0-9]+\", \1)}"}` (invalid regular expression)|
|`customername_1`|`"{$MACRO:\"{{#IFALIAS}.regsub(\\"(.*)_([0-9]+)\\", \1)}\"}"`|`"{$MACRO:\"customername\"}"`|
|`customername_1`|`"{$MACRO:\"{{#IFALIAS}.regsub(\\"(.*)_([0-9]+)\\", \2)}\"}"`|`"{$MACRO:\"1\"}")`|
|`customername_1`|`"{$MACRO:\"{{#IFALIAS}.regsub(\\"(.*)_([0-9]+\\", \1)}\"}"`|`"{$MACRO:\"{{#IFALIAS}.regsub(\\"(.*)_([0-9]+\\", \1)}\"}")` (invalid regular expression)|

[comment]: # ({/new-9423d893})
