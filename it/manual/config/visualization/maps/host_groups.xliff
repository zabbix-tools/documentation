<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/config/visualization/maps/host_groups.md">
    <body>
      <trans-unit id="5954ce5d" xml:space="preserve">
        <source># 2 Host group elements</source>
      </trans-unit>
      <trans-unit id="134d1a9e" xml:space="preserve">
        <source>#### Overview

This section explains how to add a “Host group” type element when
configuring a [network map](/manual/config/visualization/maps/map).</source>
      </trans-unit>
      <trans-unit id="cc7d692a" xml:space="preserve">
        <source>#### Configuration

![](../../../../../assets/en/manual/config/visualization/maps/map_host_group.png){width="600"}

All mandatory input fields are marked with a red asterisk.

This table consists of parameters typical for *Host group* element type:

|Parameter|Description|
|--|--------|
|*Type*|Select Type of the element:&lt;br&gt;**Host group** - icon representing the status of all triggers of all hosts belonging to the selected group|
|*Show*|Show options:&lt;br&gt;**Host group** - selecting this option will result as one single icon displaying corresponding information about the certain host group&lt;br&gt;**Host group elements** - selecting this option will result as multiple icons displaying corresponding information about every single element (host) of the certain host group|
|*Area type*|This setting is available if the “Host group elements” parameter is selected:&lt;br&gt;**Fit to map** - all host group elements are equally placed within the map&lt;br&gt;**Custom size** - a manual setting of the map area for all the host group elements to be displayed|
|*Area size*|This setting is available if “Host group elements” parameter and “Area type” parameter are selected:&lt;br&gt;**Width** - numeric value to be entered to specify map area width&lt;br&gt;**Height** - numeric value to be entered to specify map area height|
|*Placing algorithm*|**Grid** – only available option of displaying all the host group elements|
|*Label*|Icon label, any string.&lt;br&gt;[Macros](/manual/config/macros) and multiline strings can be used in labels.&lt;br&gt;If the type of the map element is “Host group” specifying certain macros has an impact on the map view displaying corresponding information about every single host. For example, if {HOST.IP} macro is used, the edit map view will only display the macro {HOST.IP} itself while map view will include and display each host’s unique IP address|</source>
      </trans-unit>
      <trans-unit id="a843184c" xml:space="preserve">
        <source>#### Viewing host group elements

This option is available if the "Host group elements" show option is
chosen. When selecting "Host group elements" as the *show* option, you
will at first see only one icon for the host group. However, when you
save the map and then go to the map view, you will see that the map
includes all the elements (hosts) of the certain host group:

|Map editing view|Map view|
|----------------|--------|
|![](../../../../../assets/en/manual/config/visualization/maps/map_host_group_conf.png){width="450"}|![](../../../../../assets/en/manual/config/visualization/maps/map_host_group_view.png){width="450"}|

Notice how the {HOST.NAME} macro is used. In map editing, the macro name
is unresolved, while in map view all the unique names of the hosts are
displayed.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
