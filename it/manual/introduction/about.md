[comment]: # translation:outdated

[comment]: # ({new-761c4a6f})
# 2 What is Zabbix

[comment]: # ({/new-761c4a6f})

[comment]: # ({new-319fca18})
#### Overview

Zabbix was created by Alexei Vladishev, and currently is actively
developed and supported by Zabbix SIA.

Zabbix is an enterprise-class open source distributed monitoring
solution.

Zabbix is a software that monitors numerous parameters of a network and
the health and integrity of servers, virtual machines, applications,
services, databases, websites, the cloud and more. Zabbix uses a
flexible notification mechanism that allows users to configure e-mail
based alerts for virtually any event. This allows a fast reaction to
server problems. Zabbix offers excellent reporting and data
visualization features based on the stored data. This makes Zabbix ideal
for capacity planning.

Zabbix supports both polling and trapping. All Zabbix reports and
statistics, as well as configuration parameters, are accessed through a
web-based frontend. A web-based frontend ensures that the status of your
network and the health of your servers can be assessed from any
location. Properly configured, Zabbix can play an important role in
monitoring IT infrastructure. This is equally true for small
organizations with a few servers and for large companies with a
multitude of servers.

Zabbix is free of cost. Zabbix is written and distributed under the GPL
General Public License version 2. It means that its source code is
freely distributed and available for the general public.

[Commercial support](http://www.zabbix.com/support.php) is available and
provided by Zabbix Company and its partners around the world.

Learn more about [Zabbix features](features).

[comment]: # ({/new-319fca18})

[comment]: # ({new-46ee774b})
#### Users of Zabbix

Many organizations of different size around the world rely on Zabbix as
a primary monitoring platform.

[comment]: # ({/new-46ee774b})
