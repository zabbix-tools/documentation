<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/appendix/config/zabbix_web_service.md">
    <body>
      <trans-unit id="66a8ced4" xml:space="preserve">
        <source># 9 Zabbix web service</source>
      </trans-unit>
      <trans-unit id="132b4156" xml:space="preserve">
        <source>### Overview

The Zabbix web service is a process that is used for communication with external web services.

The parameters supported by the Zabbix web service configuration file (zabbix\_web\_service.conf) are listed in this section. 

The parameters are listed without additional information. Click on the parameter to see the full details.

|Parameter|Description|
|--|--------|
|[AllowedIP](#allowedip)|A list of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies.|
|[DebugLevel](#debuglevel)|The debug level.|
|[ListenPort](#listenport)|The agent will listen on this port for connections from the server.|
|[LogFile](#logfile)|The name of the log file.|
|[LogFileSize](#logfilesize)|The maximum size of the log file.|
|[LogType](#logtype)|The type of the log output.|
|[Timeout](#timeout)|Spend no more than Timeout seconds on processing.|
|[TLSAccept](#tlsaccept)|What incoming connections to accept.|
|[TLSCAFile](#tlscafile)|The full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.|
|[TLSCertFile](#tlscertfile)|The full pathname of a file containing the service certificate or certificate chain, used for encrypted communications between Zabbix components.|
|[TLSKeyFile](#tlskeyfile)|The full pathname of a file containing the service private key, used for encrypted communications between Zabbix components.|

All parameters are non-mandatory unless explicitly stated that the parameter is mandatory.

Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.</source>
      </trans-unit>
      <trans-unit id="69d7c913" xml:space="preserve">
        <source>### Parameter details</source>
      </trans-unit>
      <trans-unit id="0f829aa7" xml:space="preserve">
        <source>##### AllowedIP
A list of comma delimited IP addresses, optionally in CIDR notation, or DNS names of Zabbix servers and Zabbix proxies. Incoming connections will be accepted only from the hosts listed here.&lt;br&gt;If IPv6 support is enabled then `127.0.0.1`, `::127.0.0.1`, `::ffff:127.0.0.1` are treated equally and `::/0` will allow any IPv4 or IPv6 address. `0.0.0.0/0` can be used to allow any IPv4 address.

Example: 

    127.0.0.1,192.168.1.0/24,::1,2001:db8::/32,zabbix.example.com

Mandatory: yes</source>
      </trans-unit>
      <trans-unit id="e5288ea7" xml:space="preserve">
        <source>##### DebugLevel

Specify the debug level:&lt;br&gt;*0* - basic information about starting and stopping of Zabbix processes&lt;br&gt;*1* - critical information;&lt;br&gt;*2* - error information;&lt;br&gt;*3* - warnings;&lt;br&gt;*4* - for debugging (produces lots of information);&lt;br&gt;*5* - extended debugging (produces even more information).

Default: `3`&lt;br&gt;
Range: 0-5</source>
      </trans-unit>
      <trans-unit id="e99b72c0" xml:space="preserve">
        <source>##### ListenPort

The service will listen on this port for connections from the server.

Default: `10053`&lt;br&gt;
Range: 1024-32767</source>
      </trans-unit>
      <trans-unit id="bb5252d8" xml:space="preserve">
        <source>##### LogFile

The name of the log file.

Example:

    /tmp/zabbix_web_service.log

Mandatory: Yes, if LogType is set to *file*; otherwise no</source>
      </trans-unit>
      <trans-unit id="778f1edc" xml:space="preserve">
        <source>##### LogFileSize

The maximum size of a log file in MB.&lt;br&gt;0 - disable automatic log rotation.&lt;br&gt;*Note*: If the log file size limit is reached and file rotation fails, for whatever reason, the existing log file is truncated and started anew.

Default: `1`&lt;br&gt;
Range: 0-1024</source>
      </trans-unit>
      <trans-unit id="9d26f327" xml:space="preserve">
        <source>##### LogType

The type of the log output:&lt;br&gt;*file* - write log to the file specified by LogFile parameter;&lt;br&gt;*system* - write log to syslog;&lt;br&gt;*console* - write log to standard output.

Default: `file`</source>
      </trans-unit>
      <trans-unit id="ee64bcdf" xml:space="preserve">
        <source>##### Timeout

Spend no more than Timeout seconds on processing.

Default: `3`&lt;br&gt;
Range: 1-30</source>
      </trans-unit>
      <trans-unit id="849f8496" xml:space="preserve">
        <source>##### TLSAccept

What incoming connections to accept:&lt;br&gt;*unencrypted* - accept connections without encryption (default)&lt;br&gt;*cert* - accept connections with TLS and a certificate

Default: `unencrypted`</source>
      </trans-unit>
      <trans-unit id="336a620d" xml:space="preserve">
        <source>##### TLSCAFile

The full pathname of the file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
      <trans-unit id="185ad3c6" xml:space="preserve">
        <source>##### TLSCertFile

The full pathname of the file containing the service certificate or certificate chain, used for encrypted communications with Zabbix components.</source>
      </trans-unit>
      <trans-unit id="cbcbba0a" xml:space="preserve">
        <source>##### TLSKeyFile

The full pathname of the file containing the service private key, used for encrypted communications between Zabbix components.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
