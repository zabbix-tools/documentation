[comment]: # translation:outdated

[comment]: # ({new-f957ad95})
# 7 MySQL plugin

[comment]: # ({/new-f957ad95})

[comment]: # ({new-7e8aa5c3})
#### Overview

This section lists parameters supported in the MySQL Zabbix agent 2
plugin configuration file (mysql.conf). Note that:

-   The default values reflect process defaults, not the values in the
    shipped configuration files;
-   Zabbix supports configuration files only in UTF-8 encoding without
    [BOM](https://en.wikipedia.org/wiki/Byte_order_mark);
-   Comments starting with "\#" are only supported at the beginning of
    the line.

[comment]: # ({/new-7e8aa5c3})

[comment]: # ({new-b9c98913})
#### Parameters

|Parameter|Mandatory|Range|Default|Description|
|---------|---------|-----|-------|-----------|
|Plugins.Mysql.CallTimeout|no|1-30|global timeout|The maximum amount of time in seconds to wait for a request to be done.|
|Plugins.Mysql.KeepAlive|no|60-900|300|The maximum time of waiting (in seconds) before unused plugin connections are closed.|
|Plugins.Mysql.Sessions.<SessionName>.Password|no|<|<|Named session password.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mysql.Sessions.<SessionName>.TLSCAFile|no|<|<|Full pathname of a file containing the top-level CA(s) certificates for peer certificate verification, used for encrypted communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mysql.Sessions.<SessionName>.TLSCertFile|no|<|<|Full pathname of a file containing the agent certificate or certificate chain, used for encrypted communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mysql.Sessions.<SessionName>.TLSConnect|no|<|<|Encryption type for communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - name of a session for using in item keys.<br><br>Accepted values:<br>*required* - require TLS connection;<br>*verify\_ca* - verify certificates;<br>*verify\_full* - verify certificates and IP address.|
|Plugins.Mysql.Sessions.<SessionName>.TLSKeyFile|no|<|<|Full pathname of a file containing the database private key used for encrypted communications between Zabbix agent 2 and monitored databases.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mysql.Sessions.<SessionName>.Uri|no|<|tcp://localhost:3306|Connection string of a named session.<br>**<SessionName>** - name of a session for using in item keys.<br><br>Should not include embedded credentials (they will be ignored).<br>Must match the URI format.<br>Supported schemes: `tcp`, `unix`; a scheme can be omitted (since version 5.2.3).<br>A port can be omitted (default=3306).<br>Examples: `tcp://localhost:3306`<br>`localhost`<br>`unix:/var/run/mysql.sock`|
|Plugins.Mysql.Sessions.<SessionName>.User|no|<|<|Named session username.<br>**<SessionName>** - name of a session for using in item keys.|
|Plugins.Mysql.Timeout|no|1-30|global timeout|Request execution timeout (how long to wait for a request to complete before shutting it down).|

See also:

-   Description of general Zabbix agent 2 configuration parameters:
    [Zabbix agent 2 (UNIX)](/manual/appendix/config/zabbix_agent2) /
    [Zabbix agent 2
    (Windows)](/manual/appendix/config/zabbix_agent2_win)
-   Instructions for configuring [plugins](/manual/config/items/plugins)

[comment]: # ({/new-b9c98913})
