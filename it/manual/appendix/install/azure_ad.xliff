<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/appendix/install/azure_ad.md">
    <body>
      <trans-unit id="11206317" xml:space="preserve">
        <source># 10 SAML setup with Microsoft Azure AD</source>
      </trans-unit>
      <trans-unit id="d1a8a2fc" xml:space="preserve">
        <source>
#### Overview

This section provides guidelines for configuring single sign-on and user provisioning into Zabbix 
from Microsoft Azure Active Directory using SAML 2.0 authentication.</source>
      </trans-unit>
      <trans-unit id="ed685e3c" xml:space="preserve">
        <source>#### Microsoft Azure configuration</source>
      </trans-unit>
      <trans-unit id="440025da" xml:space="preserve">
        <source>
##### Creating application

1\. Log into your account at [Microsoft Azure](https://portal.azure.com). For testing purposes, you may create a free trial account in Microsoft Azure.

2\. From the main menu (see top left of the screen) select *Azure Active Directory*.

3\. Select *Enterprise applications* -&gt; *Add new application* -&gt; *Create your own application*.

4\. Add the name of your app and select the *Integrate any other application...* option. After that, click on *Create*.

![](../../../../assets/en/manual/appendix/install/azure_create_app.png){width="600"}</source>
      </trans-unit>
      <trans-unit id="82aca309" xml:space="preserve">
        <source>
##### Setting up single sign-on

1\. In your application page, go to *Set up single sign on* and click on *Get started*. Then select *SAML*.

2\. Edit *Basic SAML Configuration*:

-   In *Identifier (Entity ID)* set a unique name to identify your app to Azure Active Directory, for example, `zabbix`;
-   In *Reply URL (Assertion Consumer Service URL)* set the Zabbix single sign-on endpoint: `https://&lt;zabbix-instance-url&gt;/zabbix/index_sso.php?acs`:

![](../../../../assets/en/manual/appendix/install/azure_basic_saml.png){width="600"}

Note that this field requires "https". To make that work with Zabbix, it is necessary to add to `conf/zabbix.conf.php` the following line:

```
$SSO['SETTINGS'] = ['use_proxy_headers' =&gt; true];
```

3\. Edit *Attributes &amp; Claims*. You must add all attributes that you want to pass to Zabbix (user_name, user_lastname, user_email, user_mobile, groups).

The attribute names are arbitrary. Different attribute names may be used, however, it is required that they match the respective field value in Zabbix SAML settings.

-   Click on *Add new claim* to add an attribute:

![](../../../../assets/en/manual/appendix/install/azure_claim.png)

-   Click on *Add a group claim* to add an attribute for passing groups to Zabbix:

![](../../../../assets/en/manual/appendix/install/azure_claim_group.png)

4\. In *SAML Certificates* download the certificate provided by Azure and place it into `conf/certs` of the Zabbix frontend installation. 

Set 644 permissions to it by running:

    chmod 644 azure.cer

Make sure that `conf/zabbix.conf.php` contains the line:

```
$SSO['IDP_CERT'] = 'conf/certs/azure.cer';
```

5\. Use the values from *Set up &lt;your app name&gt;* in Azure to configure Zabbix SAML authentication (see next section):

![](../../../../assets/en/manual/appendix/install/azure_sso_settings.png){width="550"}</source>
      </trans-unit>
      <trans-unit id="b1c1c877" xml:space="preserve">
        <source>#### Zabbix configuration

1\. In Zabbix, go to the [SAML settings](/manual/web_interface/frontend_sections/users/authentication/saml#setting-up-zabbix) 
and fill the configuration options based on the Azure configuration:

![](../../../../assets/en/manual/appendix/install/azure_zabbix_conf.png){width="600"}

|Zabbix field|Setup field in Azure|Sample value|
|----|----|--|
|*IdP entity ID*|Azure AD identifier||
|*SSO service URL*|Login URL||
|*SLO service URL*|Logout URL||
|*Username attribute*|Custom attribute (claim)|`user_email`|
|*Group name attribute*|Custom attribute (claim)|`groups`|
|*User name attribute*|Custom attribute (claim)|`user_name`|
|*User last name attribute*|Custom attribute (claim)|`user_lastname`|

It is also required to configure user group mapping. Media mapping is optional.

Click on *Update* to save these settings.</source>
      </trans-unit>
      <trans-unit id="8c2fcfa4" xml:space="preserve">
        <source>
#### SCIM user provisioning

1\. In your Azure AD application page, from the main menu open the Provisioning page. Click on *Get started* and then select Automatic provisioning mode:

-   In *Tenant URL*, set the following value: `https://&lt;zabbix-instance-url&gt;/zabbix/api_scim.php`
-   In *Secret token*, enter a Zabbix API token with Super admin permissions.
-   Click on *Test connection* to see if the connection is established. 

![](../../../../assets/en/manual/appendix/install/azure_api_connection.png){width="550"}

Save the settings.

2\. Now you can add all the attributes that will be passed with SCIM to Zabbix. To do that, click on *Mappings* and then on *Provision Azure Active Directory Users*.

![](../../../../assets/en/manual/appendix/install/azure_scim_mappings_add.png){width="550"}

At the bottom of the Attribute Mapping list, enable *Show advanced options*, and then click on *Edit attribute list for customappsso*.

At the bottom of the attribute list, add your own attributes with type 'String':

![](../../../../assets/en/manual/appendix/install/azure_scim_attrib.png){width="550"}

Save the list.

3\. Now you can add mappings for the added attributes. At the bottom of the Attribute Mapping list, click on *Add New Mapping* and create mappings as shown below:

![](../../../../assets/en/manual/appendix/install/azure_scim_mapping.png){width="550"}

When all mappings are added, save the list of mappings.

![](../../../../assets/en/manual/appendix/install/azure_scim_mappings.png){width="550"}

4\. As a prerequisite of user provisioning into Zabbix, you must have users and groups configured in Azure. 

To do that, select *Azure Active Directory* from the main Azure menu (see top left of the screen) and then add users/groups in the respective Users and Groups pages.

5\. When users and groups have been created in Azure AD, you can go to the *Users and groups* menu of your application and add them to the app.

6\. Go to the *Provisioning* menu of your app, and click on *Start provisioning* to have users provisioned to Zabbix.

Note that the Users PATCH request in Azure does not support changes in media.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
