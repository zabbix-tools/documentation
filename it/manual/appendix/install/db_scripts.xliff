<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/appendix/install/db_scripts.md">
    <body>
      <trans-unit id="7af1bbaa" xml:space="preserve">
        <source># 1 Database creation</source>
      </trans-unit>
      <trans-unit id="fab90562" xml:space="preserve">
        <source>#### Overview

A Zabbix database must be created during the installation of Zabbix
server or proxy.

This section provides instructions for creating a Zabbix database. A
separate set of instructions is available for each supported database.

UTF-8 is the only encoding supported by Zabbix. It is known to work
without any security flaws. Users should be aware that there are known
security issues if using some of the other encodings. For switching to UTF-8, see [Repairing Zabbix database character set and collation](/manual/appendix/install/db_charset_coll).</source>
      </trans-unit>
      <trans-unit id="fad527fc" xml:space="preserve">
        <source>
::: noteclassic
If installing from [Zabbix Git repository](https://git.zabbix.com/projects/ZBX/repos/zabbix/browse), you need to run the following command prior to proceeding to the next steps:
&lt;br&gt;&lt;br&gt;
`make dbschema`
:::</source>
      </trans-unit>
      <trans-unit id="1e36e539" xml:space="preserve">
        <source>#### MySQL

Character sets utf8 (aka utf8mb3) and utf8mb4 are supported (with utf8_bin and utf8mb4_bin collation respectively) for Zabbix server/proxy to work properly with MySQL database. It is recommended to use utf8mb4 for new installations.

Deterministic triggers need to be created during the import of schema. On MySQL and MariaDB, this requires `GLOBAL log_bin_trust_function_creators = 1` to be set if binary logging is enabled and there is no superuser privileges and log_bin_trust_function_creators = 1 is not set in MySQL configuration file.

If you are installing from Zabbix **packages**, proceed to the [instructions](https://www.zabbix.com/download) for
your platform.

If you are installing Zabbix from sources: 

* Create and configure a database and a user.

```bash
mysql -uroot -p&lt;password&gt;

mysql&gt; create database zabbix character set utf8mb4 collate utf8mb4_bin;
mysql&gt; create user 'zabbix'@'localhost' identified by '&lt;password&gt;';
mysql&gt; grant all privileges on zabbix.* to 'zabbix'@'localhost';
mysql&gt; SET GLOBAL log_bin_trust_function_creators = 1;
mysql&gt; quit;
```

* Import the data into the database and set utf8mb4 character set as default. For a Zabbix proxy database, only `schema.sql` should be imported (no images.sql nor data.sql).

```bash
cd database/mysql
mysql -uzabbix -p&lt;password&gt; zabbix &lt; schema.sql
# stop here if you are creating database for Zabbix proxy
mysql -uzabbix -p&lt;password&gt; zabbix &lt; images.sql
mysql -uzabbix -p&lt;password&gt; --default-character-set=utf8mb4 zabbix &lt; data.sql
```

`log_bin_trust_function_creators` can be disabled after the schema has been successfully imported:

```bash
mysql -uroot -p&lt;password&gt;

mysql&gt; SET GLOBAL log_bin_trust_function_creators = 0;
mysql&gt; quit;
```</source>
      </trans-unit>
      <trans-unit id="61d6043c" xml:space="preserve">
        <source>#### PostgreSQL

You need to have database user with permissions to create database
objects. 

If you are installing from Zabbix **packages**, proceed to the [instructions](https://www.zabbix.com/download) for
your platform.

If you are installing Zabbix from sources:

* Create a database user.

The following shell command will create user `zabbix`. Specify
a password when prompted and repeat the password (note, you may first be asked
for `sudo` password):

```bash
sudo -u postgres createuser --pwprompt zabbix
```

* Create a database.

The following shell command will create the database `zabbix` (last parameter) with the
previously created user as the owner (`-O zabbix`).

```bash
sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix
```

* Import the initial schema and data (assuming you are in the root directory of Zabbix sources). For a Zabbix proxy database, only `schema.sql` should be
imported (no images.sql nor data.sql).

```bash
cd database/postgresql
cat schema.sql | sudo -u zabbix psql zabbix
# stop here if you are creating database for Zabbix proxy
cat images.sql | sudo -u zabbix psql zabbix
cat data.sql | sudo -u zabbix psql zabbix
```

::: noteimportant
The above commands are provided as an example that will work in most of GNU/Linux installations.
You can use different commands depending on how your system/database is configured, for example:
&lt;br&gt;&lt;br&gt;
`psql -U &lt;username&gt;`
&lt;br&gt;&lt;br&gt;
If you have any trouble setting up the database, please consult your Database administrator.
:::</source>
      </trans-unit>
      <trans-unit id="cc68ca58" xml:space="preserve">
        <source>#### TimescaleDB

Instructions for creating and configuring TimescaleDB are provided in a
separate [section](/manual/appendix/install/timescaledb).</source>
      </trans-unit>
      <trans-unit id="7b4d56a7" xml:space="preserve">
        <source>#### Oracle

Instructions for creating and configuring Oracle database are provided
in a separate [section](/manual/appendix/install/oracle).</source>
      </trans-unit>
      <trans-unit id="02d49e4f" xml:space="preserve">
        <source>#### SQLite

Using SQLite is supported for **Zabbix proxy** only!

The database will be
automatically created if it does not exist.

Return to the [installation section](/manual/installation/install).</source>
      </trans-unit>
    </body>
  </file>
</xliff>
