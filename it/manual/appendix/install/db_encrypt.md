[comment]: # translation:outdated

[comment]: # ({new-5407a56a})
# 4 Secure connection to the database

[comment]: # ({/new-5407a56a})

[comment]: # ({new-7cadd09e})
#### Overview

This section provides Zabbix setup steps and configuration examples for
secure TLS connections between:

|Database|Zabbix components|
|--------|-----------------|
|MySQL|Zabbix frontend, Zabbix server, Zabbix proxy|
|PostgreSQL|Zabbix frontend, Zabbix server, Zabbix proxy|

To set up connection encryption within the DBMS, see official vendor
documentation for details:

-   [MySQL](https://dev.mysql.com/doc/refman/8.0/en/replication-solutions-encrypted-connections.html):
    source and replica replication database servers.
-   [MySQL](https://dev.mysql.com/doc/refman/8.0/en/group-replication-security.html):
    group replication, etc. database servers.
-   [PostgreSQL](https://www.postgresql.org/docs/current/encryption-options.html)
    encryption options.

All examples are based on the GA releases of MySQL CE (8.0) and PgSQL
(13) available through official repositories using CentOS 8.

[comment]: # ({/new-7cadd09e})

[comment]: # ({new-5af34b43})
##### Requirements

The following is required to set up encryption:

-   Developer-supported operating system with OpenSSL >=1.1.X or
    alternative.

::: noteclassic
 It is recommended to avoid OS in the end-of-life status,
especially in the case of new installations 
:::

-   Database engine (RDBMS) installed and maintained from the official
    repository provided by developer. Operating systems often shipped
    with outdated  database software versions for which encryption
    support is not implemented, for example RHEL 7 based systems and
    PostgreSQL 9.2, MariaDB 5.5 without encryption support. 

[comment]: # ({/new-5af34b43})

[comment]: # ({new-9801ac5b})
##### Terminology

Setting this option enforces to use TLS connection to database from
Zabbix server/proxy and frontend to database:

-   required - connect using TLS as transport mode without identity
    checks;
-   verify\_ca - connect using TLS and verify certificate;
-   verify\_full - connect using TLS, verify certificate and verify that
    database identity (CN) specified by DBHost matches its certificate;

[comment]: # ({/new-9801ac5b})

[comment]: # ({new-84f30836})
#### Zabbix configuration

[comment]: # ({/new-84f30836})

[comment]: # ({new-b1071a8a})
##### Frontend to the database

A secure connection to the database can be configured during frontend
installation:

-   Mark the *Database TLS encryption* checkbox in the [Configure DB
    connection](/manual/installation/frontend#configure_db_connection)
    step to enable transport encryption.
-   Mark the *Verify database certificate* checkbox that appears when
    *TLS encryption* field is checked to enable encryption with
    certificates.

::: noteclassic
 For MySQL, the *Database TLS encryption* checkbox is
disabled, if *Database host* is set to localhost, because connection
that uses a socket file (on Unix) or shared memory (on Windows) cannot
be encrypted.\
For PostgreSQL, the *TLS encryption* checkbox is disabled, if the value
of the *Database host* field begins with a slash or the field is empty.

:::

The following parameters become available in the TLS encryption in
certificates mode (if both checkboxes are marked):

|Parameter|Description|
|---------|-----------|
|*Database TLS CA file*|Specify the full path to a valid TLS certificate authority (CA) file.|
|*Database TLS key file*|Specify the full path to a valid TLS key file.|
|*Database TLS certificate file*|Specify the full path to a valid TLS certificate file.|
|*Database host verification*|Mark this checkbox to activate host verification.<br>Disabled for MYSQL, because PHP MySQL library does not allow to skip the peer certificate validation step.|
|*Database TLS cipher list*|Specify a custom list of valid ciphers. The format of the cipher list must conform to the OpenSSL standard.<br>Available for MySQL only.|

::: noteimportant
TLS parameters must point to valid files. If they
point to non-existent or invalid files, it will lead to the
authorization error.\
If certificate files are writable, the frontend generates a warning in
the [System
information](/manual/web_interface/frontend_sections/reports/status_of_zabbix)
report that "TLS certificate files must be read-only." (displayed only
if the PHP user is the owner of the certificate).\
\
Certificates protected by passwords are not supported. 
:::

[comment]: # ({/new-b1071a8a})

[comment]: # ({new-d4fd87a2})
##### Use cases

Zabbix frontend uses GUI interface to define possible options: required,
verify\_ca, verify\_full. Specify required options in the installation
wizard step *Configure DB connections*. These options are mapped to the
configuration file (zabbix.conf.php) in the following manner:

|GUI settings|Configuration file|Description|Result|
|------------|------------------|-----------|------|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_transport.png)|...<br>// Used for TLS connection.<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '';<br>$DB\['CERT\_FILE'\] = '';<br>$DB\['CA\_FILE'\] = '';<br>$DB\['VERIFY\_HOST'\] = false;<br>$DB\['CIPHER\_LIST'\] = '';<br>...|Check *Database TLS encryption*<br>Leave *Verify database certificate* unchecked|Enable 'required' mode.|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_verify_ca.png)|...<br>$DB\['ENCRYPTION'\] = true;\\\\ $DB\['KEY\_FILE'\] = '';<br>$DB\['CERT\_FILE'\] = '';<br>$DB\['CA\_FILE'\] = '/etc/ssl/mysql/ca.pem';<br>$DB\['VERIFY\_HOST'\] = false;<br>$DB\['CIPHER\_LIST'\] = '';<br>...|1\. Check *Database TLS encryption* and *Verify database certificate*<br>2. Specify path to *Database TLS CA file*|Enable 'verify\_ca' mode.|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_verify_full1.png)|...<br>// Used for TLS connection with strictly defined Cipher list.<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '<key\_file\_path>';<br>$DB\['CERT\_FILE'\] = '<key\_file\_path>';<br>$DB\['CA\_FILE'\] = '<key\_file\_path>';<br>$DB\['VERIFY\_HOST'\] = true;<br>$DB\['CIPHER\_LIST'\] = '<cipher\_list>';<br>...<br><br>Or:<br><br>...<br>// Used for TLS connection without Cipher list defined - selected by MySQL server<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '<key\_file\_path>';<br>$DB\['CERT\_FILE'\] = '<key\_file\_path>';<br>$DB\['CA\_FILE'\] = '<key\_file\_path>';<br>$DB\['VERIFY\_HOST'\] = true;<br>$DB\['CIPHER\_LIST'\] = '';<br>...|1\. Check *Database TLS encryption* and *Verify database certificate*<br>2. Specify path to *Database TLS key file*<br>3. Specify path to *Database TLS CA file*<br>4. Specify path to *Database TLS certificate file*<br>6. Specify TLS cipher list (optional)|Enable 'verify\_full' mode for MySQL.|
|![](../../../../assets/en/manual/appendix/install/encrypt_db_verify_full2.png)|...<br>$DB\['ENCRYPTION'\] = true;<br>$DB\['KEY\_FILE'\] = '<key\_file\_path>';<br>$DB\['CERT\_FILE'\] = '<key\_file\_path>';<br>$DB\['CA\_FILE'\] = '<key\_file\_path>';<br>$DB\['VERIFY\_HOST'\] = true;<br>$DB\['CIPHER\_LIST'\] = ' ';<br>...<br>|1\. Check *Database TLS encryption* and *Verify database certificate*<br>2. Specify path to *Database TLS key file*<br>3. Specify path to *Database TLS CA file*<br>4. Specify path to *Database TLS certificate file*<br>6. Check *Database host verification*|Enable 'verify\_full' mode for PostgreSQL.|

**See also:** [Encryption configuration examples for
MySQL](/manual/appendix/install/db_encrypt/mysql), [Encryption
configuration examples for
PostgreSQL](/manual/appendix/install/db_encrypt/postgres).

[comment]: # ({/new-d4fd87a2})

[comment]: # ({new-9cfb91d1})
#### Zabbix server/proxy configuration

Secure connections to the database can be configured with the respective
parameters in the Zabbix [server](/manual/appendix/config/zabbix_server)
and/or [proxy](/manual/appendix/config/zabbix_proxy) configuration file.

|Configuration|Result|
|-------------|------|
|None|Connection to the database without encryption.|
|1\. Set DBTLSConnect=required|Server/proxy make a TLS connection to the database. An unencrypted connection is not allowed.|
|1\. Set DBTLSConnect=verify\_ca<br>2. Set DBTLSCAFile - specify the TLS certificate authority file|Server/proxy make a TLS connection to the database after verifying the database certificate.|
|1\. Set DBTLSConnect=verify\_full<br>2. Set DBTLSCAFile - specify TLS certificate authority file|Server/proxy make a TLS connection to the database after verifying the database certificate and the database host identity.|
|1\. Set DBTLSCAFile - specify TLS certificate authority file<br>2. Set DBTLSCertFile - specify the client public key certificate file<br>3. Set DBTLSKeyFile - specify the client private key file|Server/proxy provide a client certificate while connecting to the database.|
|1\. Set DBTLSCipher - the list of encryption ciphers that the client permits for connections using TLS protocols up to TLS 1.2<br><br>or DBTLSCipher13 - the list of encryption ciphers that the client permits for connections using TLS 1.3 protocol|(MySQL) TLS connection is made using a cipher from the provided list.<br>(PostgreSQL) Setting this option will be considered as an error.|

[comment]: # ({/new-9cfb91d1})
