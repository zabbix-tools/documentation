[comment]: # translation:outdated

[comment]: # ({new-0ea9fa0f})
# 3 Zabbix agent 2 protocol

[comment]: # ({/new-0ea9fa0f})

[comment]: # ({new-e87a981b})

### Overview

This section provides information on:

*   Agent2 -> Server : active checks request
*   Server -> Agent2 : active checks response

*   Agent2 -> Server : agent data request
*   Server -> Agent2 : agent data response

[comment]: # ({/new-e87a981b})

[comment]: # ({new-1c996373})

### Active checks request

The active checks request is used to obtain the active checks to be processed by agent. This request is sent by the agent upon start and then with *RefreshActiveChecks* inervals.

| Field | Type | Value |
|-|-|--------|
| request | _string_ | `active checks` |
| host | _string_ | Host name. |
| version | _string_ | The agent version: \<major>.\<minor>. |
| host_metadata | _string_ | The configuration parameter HostMetadata or HostMetadataItem metric value (optional). |
| interface | _string_ | The configuration parameter HostInterface or HostInterfaceItem metric value (optional). |
| ip | _string_ | The configuration parameter ListenIP first IP if set (optional). |
| port | _number_ | The configuration parameter ListenPort value if set and not default agent listening port (optional). |

Example:
```json
{
  "request": "active checks",
  "host": "Zabbix server",
  "version": "6.0",
  "host_metadata": "mysql,nginx",
  "hostinterface": "zabbix.server.lan",
  "ip": "159.168.1.1",
  "port": 12050
}
```

[comment]: # ({/new-1c996373})

[comment]: # ({new-44da8f0a})

### Active checks response

The active checks response is sent by the server back to agent after processing active checks request.

| Field |<| Type | Value |
|-|------|--|-----------------------|
| response |<| _string_ | `success` \| `failed` |
| info |<| _string_ | Error information in the case of failure (optional). |
| data |<| _array of objects_ | Active check items (optional). |
| | key | _string_ | Item key with expanded macros. |
|^| itemid | _number_ | Item identifier. |
|^| delay | _string_ | Item update interval. |
|^| lastlogsize | _number_ | Item lastlogsize. |
|^| mtime | _number_ | Item mtime. |
| refresh_unsupported |<| _number_ | Unsupported item refresh interval (agent version < 5.4). |
| regexp |<| _array of objects_ | Global regular expressions (optional). |
| | name | _string_ | Global regular expression name. |
|^| expression | _string_ | Global regular expression. |
|^| expression_type | _number_ | Global regular expression type. |
|^| exp_delimiter | _string_ | Global regular expression delimiter. |
|^| case_sensitive | _number_ | Global regular expression case sensitiviness setting. |

Example:
```json
{
  "response": "success",
  "data": [
    {
      "key": "log[/home/zabbix/logs/zabbix_agentd.log]",
      "itemid": 1234,
      "delay": "30s",
      "lastlogsize": 0,
      "mtime": 0
    },
    {
      "key": "agent.version",
      "itemid": 5678,
      "delay": "10m",
      "lastlogsize": 0,
      "mtime": 0
    }
  ]
}
```

[comment]: # ({/new-44da8f0a})

[comment]: # ({new-8a564dbc})

### Agent data request

The agent data request contains the gathered item values.

| Field |<| Type | Value |
|-|------|--|-----------------------|
| request |<| _string_ | `agent data` |
| host |<| _string_ | Host name. |
| version |<| _string_ | The agent version: \<major>.\<minor>. |
| session |<| _string_ | Unique session identifier generated each time when agent is started. |
| data |<| _array of objects_ | Item values. |
| | id | _number_ | The value identifier (incremental counter used for checking duplicated values in the case of network problems). |
|^| itemid | _number_ | Item identifier. |
|^| value | _string_ | The item value (optional). |
|^| lastlogsize | _number_ | The item lastlogsize (optional). |
|^| mtime | _number_ | The item mtime (optional). |
|^| state | _number_ | The item state (optional). |
|^| source | _string_ | The value event log source (optional). |
|^| eventid | _number_ | The value event log eventid (optional). |
|^| severity | _number_ | The value event log severity (optional). |
|^| timestamp | _number_ | The value event log timestamp (optional). |
|^| clock | _number_ | The value timestamp (seconds since Epoch). |
|^| ns | _number_ | The value timestamp nanoseconds. |

Example:
```json
{
  "request": "agent data",
  "data": [
    {
      "id": 1,
      "itemid": 5678,
      "value": "2.4.0",
      "clock": 1400675595,
      "ns": 76808644
    },
    {
      "id": 2,
      "itemid": 1234,
      "lastlogsize": 112,
      "value": " 19845:20140621:141708.521 Starting Zabbix Agent [<hostname>]. Zabbix 2.4.0 (revision 50000).",
      "clock": 1400675595,
      "ns": 77053975
    }
  ],
  "host": "Zabbix server",
  "version": "6.0",
  "sessionid": "1234456akdsjhfoui"
}
```

[comment]: # ({/new-8a564dbc})

[comment]: # ({new-b302d014})

### Agent data response

The agent data response is sent by the server back to agent after processing the agent data request.

| Field | Type | Value |
|-|-|--------|
| response | _string_ | `success` \| `failed` |
| info | _string_ | Item processing results. |

Example:
```json
{
  "response": "success",
  "info": "processed: 2; failed: 0; total: 2; seconds spent: 0.003534"
}
```

[comment]: # ({/new-b302d014})

[comment]: # ({new-70d7fe1b})

### Heartbeat message

The heartbeat message is sent by an active agent to Zabbix server/proxy 
every HeartbeatFrequency seconds (configured in the Zabbix agent 2 
[configuration file](/manual/appendix/config/zabbix_agent2)). 

It is used to monitor the availability of active checks.

```json
{
  "request": "active check heartbeat",
  "host": "Zabbix server",
  "heartbeat_freq": 60
}
```

| Field | Type | Mandatory | Value |
|-|-|-|--------|
| request | _string_ | yes | `active check heartbeat` |
| host | _string_ | yes | The host name. |
| heartbeat_freq | _number_ | yes | The agent heartbeat frequency (HeartbeatFrequency configuration parameter). |

[comment]: # ({/new-70d7fe1b})
