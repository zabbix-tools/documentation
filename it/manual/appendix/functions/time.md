[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-493edd72})
# 3 Date and time functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

::: noteimportant
Date and time functions cannot be used in the
expression alone; at least one non-time-based function referencing the
host item must be present in the expression.
:::

|FUNCTION|<|<|<|
|--------|-|-|-|
|<|**Description**|**Function-specific parameters**|**Comments**|
|**date**|<|<|<|
|<|Current date in YYYYMMDD format.|<|Example:<br>=> **date**()<20220101|
|**dayofmonth**|<|<|<|
|<|Day of month in range of 1 to 31.|<|Example:<br>=> **dayofmonth**()=1|
|**dayofweek**|<|<|<|
|<|Day of week in range of 1 to 7 (Mon - 1, Sun - 7).|<|Example:<br>=> **dayofweek**()<6|
|**now**|<|<|<|
|<|Number of seconds since the Epoch (00:00:00 UTC, January 1, 1970).|<|Example:<br>=> **now**()<1640998800|
|**time**|<|<|<|
|<|Current time in HHMMSS format.|<|Example:<br>=> **time**()>000000 and **time**()<060000|

[comment]: # ({/new-493edd72})

[comment]: # ({new-fbd1f745})
### Function details

[comment]: # ({/new-fbd1f745})

[comment]: # ({new-5fe053df})

##### date

The current date in YYYYMMDD format.

Example:

    date()<20220101

[comment]: # ({/new-5fe053df})

[comment]: # ({new-9ba00e3f})

##### dayofmonth

The day of month in range of 1 to 31.

Example:

    dayofmonth()=1

[comment]: # ({/new-9ba00e3f})

[comment]: # ({new-d60d267c})

##### dayofweek

The day of week in range of 1 to 7 (Mon - 1, Sun - 7).

Example:

    dayofweek()<6

[comment]: # ({/new-d60d267c})

[comment]: # ({new-e049cb14})

##### now

The number of seconds since the Epoch (00:00:00 UTC, January 1, 1970).

Example:

    now()<1640998800

[comment]: # ({/new-e049cb14})

[comment]: # ({new-6f5f853e})

##### time

The current time in HHMMSS format.

Example:

    time()>000000 and time()<060000

[comment]: # ({/new-6f5f853e})

[comment]: # ({new-30214ece})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/new-30214ece})
