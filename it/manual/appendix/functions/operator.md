[comment]: # attributes: notoc

[comment]: # translation:outdated

[comment]: # ({new-a5990009})
# 6 Operator functions

All functions listed here are supported in:

-   [Trigger expressions](/manual/config/triggers/expression)
-   [Calculated items](/manual/config/items/itemtypes/calculated)

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters

|FUNCTION|<|<|<|
|--------|-|-|-|
|<|**Description**|**Function-specific parameters**|**Comments**|
|**between** (value,min,max)|<|<|<|
|<|Check if a value belongs to the given range.|**value** - value to check<br>**min** - minimum value<br>**max** - maximum value|Supported value types: integer, float<br><br>Returns:<br>1 - in range<br>0 - otherwise<br><br>Example:<br>=> **between**(last(/host/key),**1**,**10**)=1 - trigger if the value is between 1 and 10.|
|**in** (value,value1,value2,...valueN)|<|<|<|
|<|Check if a value is equal to at least one of the listed values.|**value** - value to check<br>**value1,value2,...valueN** - listed values (string values must be double-quoted)|Supported value types: all<br><br>Returns:<br>1 - if equal<br>0 - otherwise<br><br>The value is compared to the listed values as numbers, if all of these values can be converted to numeric; otherwise compared as strings.<br><br>Example:<br>=> **in**(last(/host/key),**5**,**10**)=1 - trigger if the last value is equal to 5 or 10<br>=> **in**(**"text"**, last(/host/key),last(/host/key,\#2))=1 - trigger if "text" is equal to either of the last 2 values.|

[comment]: # ({/new-a5990009})

[comment]: # ({new-3b2fb872})
### Function details

Some general notes on function parameters:

-   Function parameters are separated by a comma
-   Expressions are accepted as parameters

[comment]: # ({/new-3b2fb872})

[comment]: # ({new-0d7a79bf})

##### between(value,min,max) {#between}

Check if the value belongs to the given range.<br>
Supported value types: *Integer*, *Float*.<br>
Returns: 1 - in range; 0 - otherwise.

Parameter: 

-   **value** - the value to check;<br>
-   **min** - the minimum value;<br>
-   **max** - the maximum value.

Example:

    between(last(/host/key),1,10)=1 #trigger if the value is between 1 and 10

[comment]: # ({/new-0d7a79bf})

[comment]: # ({new-2300beb9})

##### in(value,value1,value2,...valueN) {#in}

Check if the value is equal to at least one of the listed values.<br>
Supported value types: *Integer*, *Float*, *Character*, *Text*, *Log*.<br>
Returns: 1 - if equal; 0 - otherwise.

Parameter: 

-   **value** - the value to check;<br>
-   **valueX** - listed values (string values must be double-quoted).

The value is compared to the listed values as numbers, if all of these values can be converted to numeric; otherwise compared as strings.

Example:

    in(last(/host/key),5,10)=1 #trigger if the last value is equal to 5 or 10
    in("text",last(/host/key),last(/host/key,#2))=1 #trigger if "text" is equal to either of the last 2 values

[comment]: # ({/new-2300beb9})

[comment]: # ({new-690cfdc0})

See [all supported functions](/manual/appendix/functions).

[comment]: # ({/new-690cfdc0})
