[comment]: # translation:outdated

[comment]: # ({new-a93134cd})
# 15 Zabbix sender dynamic link library for Windows

In a Windows environment applications can send data to Zabbix
server/proxy directly by using the Zabbix sender dynamic link library
(zabbix\_sender.dll) instead of having to launch an external process
(zabbix\_sender.exe).

The dynamic link library with the development files is located in
bin\\winXX\\dev folders. To use it, include the zabbix\_sender.h header
file and link with the zabbix\_sender.lib library. An example file with
Zabbix sender API usage can be found in
build\\win32\\examples\\zabbix\_sender folder.

The following functionality is provided by the Zabbix sender dynamic
link library:

|`int zabbix_sender_send_values(const char *address, unsigned short port,const char *source, const zabbix_sender_value_t *values, int count,char **result);`{.c}|<|<|
|---------------------------------------------------------------------------------------------------------------------------------------------------------------|-|-|
|char **result);`{.c}|<|<|

The following data structures are used by the Zabbix sender dynamic link
library:

``` {.c}
typedef struct
{
    /* host name, must match the name of target host in Zabbix */
    char    *host;
    /* the item key */
    char    *key;
    /* the item value */
    char    *value;
}
zabbix_sender_value_t;

typedef struct
{
    /* number of total values processed */
    int total;
    /* number of failed values */
    int failed;
    /* time in seconds the server spent processing the sent values */
    double  time_spent;
}
zabbix_sender_info_t;
```

[comment]: # ({/new-a93134cd})
