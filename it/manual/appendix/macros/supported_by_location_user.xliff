<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/appendix/macros/supported_by_location_user.md">
    <body>
      <trans-unit id="d428738a" xml:space="preserve">
        <source># 2 User macros supported by location</source>
      </trans-unit>
      <trans-unit id="f0afb8e0" xml:space="preserve">
        <source>#### Overview

This section contains a list of locations, where
[user-definable](/manual/config/macros/user_macros) macros are
supported.

::: noteclassic
 Only global-level user macros are supported for *Actions*,
*Network discovery*, *Proxies* and all locations listed under *Other
locations* section of this page. In the mentioned locations, host-level
and template-level macros will not be resolved. 
:::</source>
      </trans-unit>
      <trans-unit id="7cb4c86b" xml:space="preserve">
        <source>#### Actions

In [actions](/manual/config/notifications/action), user macros can be
used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Trigger-based notifications and commands|&lt;|yes|
|Trigger-based internal notifications|&lt;|yes|
|Problem update notifications|&lt;|yes|
|Service-based notifications and commands|&lt;|yes|
|Service update notifications|&lt;|yes|
|Time period condition|&lt;|no|
|*Operations*|&lt;|&lt;|
| |Default operation step duration|no|
|^|Step duration|no|</source>
      </trans-unit>
      <trans-unit id="f30645e0" xml:space="preserve">
        <source>#### Hosts/host prototypes

In a [host](/manual/config/hosts/host) and [host
prototype](/manual/vm_monitoring#host_prototypes) configuration, user
macros can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Interface IP/DNS|&lt;|DNS only|
|Interface port|&lt;|no|
|*SNMP v1, v2*|&lt;|&lt;|
| |SNMP community|yes|
|*SNMP v3*|&lt;|&lt;|
| |Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|*IPMI*|&lt;|&lt;|
| |Username|yes|
|^|Password|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|&lt;|&lt;|
| |Tag names|yes|
|^|Tag values|yes|</source>
      </trans-unit>
      <trans-unit id="d51563d6" xml:space="preserve">
        <source>#### Items / item prototypes

In an [item](/manual/config/items/item) or an [item
prototype](/manual/discovery/low_level_discovery#item_prototypes)
configuration, user macros can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Item key parameters|&lt;|yes|
|Update interval|&lt;|no|
|Custom intervals|&lt;|no|
|History storage period|&lt;|no|
|Trend storage period|&lt;|no|
|Description|&lt;|yes|
|*Calculated item*|&lt;|&lt;|
| |Formula|yes|
|*Database monitor*|&lt;|&lt;|
| |Username|yes|
|^|Password|yes|
|^|SQL query|yes|
|*HTTP agent*|&lt;|&lt;|
| |URL^[3](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP proxy|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|^|SSl certificate file|yes|
|^|SSl key file|yes|
|^|SSl key password|yes|
|^|Allowed hosts|yes|
|*JMX agent*|&lt;|&lt;|
| |JMX endpoint|yes|
|*Script item*|&lt;|&lt;|
| |Parameter names and values|yes|
|*SNMP agent*|&lt;|&lt;|
| |SNMP OID|yes|
|*SSH agent*|&lt;|&lt;|
| |Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|&lt;|&lt;|
| |Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|&lt;|&lt;|
| |Allowed hosts|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|&lt;|&lt;|
| |Tag names|yes|
|^|Tag values|yes|
|*Preprocessing*|&lt;|&lt;|
| |Step parameters (including custom scripts)|yes|</source>
      </trans-unit>
      <trans-unit id="747bd0ca" xml:space="preserve">
        <source>#### Low-level discovery

In a [low-level discovery
rule](/manual/discovery/low_level_discovery#configuring_low-level_discovery),
user macros can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Key parameters|&lt;|yes|
|Update interval|&lt;|no|
|Custom interval|&lt;|no|
|Keep lost resources period|&lt;|no|
|Description|&lt;|yes|
|*SNMP agent*|&lt;|&lt;|
| |SNMP OID|yes|
|*SSH agent*|&lt;|&lt;|
| |Username|yes|
|^|Public key file|yes|
|^|Private key file|yes|
|^|Password|yes|
|^|Script|yes|
|*TELNET agent*|&lt;|&lt;|
| |Username|yes|
|^|Password|yes|
|^|Script|yes|
|*Zabbix trapper*|&lt;|&lt;|
| |Allowed hosts|yes|
|*Database monitor*|&lt;|&lt;|
| |Username|yes|
|^|Password|yes|
|^|SQL query|yes|
|*JMX agent*|&lt;|&lt;|
| |JMX endpoint|yes|
|*HTTP agent*|&lt;|&lt;|
| |URL^[3](supported_by_location_user#footnotes)^|yes|
|^|Query fields|yes|
|^|Timeout|no|
|^|Request body|yes|
|^|Headers (names and values)|yes|
|^|Required status codes|yes|
|^|HTTP authentication username|yes|
|^|HTTP authentication password|yes|
|*Filters*|&lt;|&lt;|
| |Regular expression|yes|
|*Overrides*|&lt;|&lt;|
| |Filters: regular expression|yes|
|^|Operations: update interval (for item prototypes)|no|
|^|Operations: history storage period (for item prototypes)|no|
|^|Operations: trend storage period (for item prototypes)|no|</source>
      </trans-unit>
      <trans-unit id="95f2f372" xml:space="preserve">
        <source>#### Network discovery

In a [network discovery rule](/manual/discovery/network_discovery/rule),
user macros can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Update interval|&lt;|no|
|*SNMP v1, v2*|&lt;|&lt;|
| |SNMP community|yes|
|^|SNMP OID|yes|
|*SNMP v3*|&lt;|&lt;|
| |Context name|yes|
|^|Security name|yes|
|^|Authentication passphrase|yes|
|^|Privacy passphrase|yes|
|^|SNMP OID|yes|</source>
      </trans-unit>
      <trans-unit id="575d88fc" xml:space="preserve">
        <source>#### Proxies

In a [proxy](/manual/distributed_monitoring/proxies#configuration)
configuration, user macros can be used in the following field:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Interface port (for passive proxy)|&lt;|no|</source>
      </trans-unit>
      <trans-unit id="32b6aef1" xml:space="preserve">
        <source>#### Templates

In a [template](/manual/config/templates/template) configuration, user
macros can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|*Tags*^[2](supported_by_location_user#footnotes)^|&lt;|&lt;|
| |Tag names|yes|
|^|Tag values|yes|</source>
      </trans-unit>
      <trans-unit id="c78b04ef" xml:space="preserve">
        <source>#### Triggers

In a [trigger](/manual/config/triggers/trigger) configuration, user macros can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Name|&lt;|yes|
|Operational data|&lt;|yes|
|Expression (only in constants and function parameters; secret macros are not supported)|&lt;|yes|
|Tag for matching|&lt;|yes|
|Menu entry name|&lt;|yes|
|Menu entry URL^[3](supported_by_location_user#footnotes)^|&lt;|yes|
|Description|&lt;|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|&lt;|&lt;|
| |Tag names|yes|
|^|Tag values|yes|</source>
      </trans-unit>
      <trans-unit id="78f1f511" xml:space="preserve">
        <source>#### Web scenario

In a [web scenario](/manual/web_monitoring) configuration, user macros
can be used in the following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Name|&lt;|yes|
|Update interval|&lt;|no|
|Agent|&lt;|yes|
|HTTP proxy|&lt;|yes|
|Variables (values only)|&lt;|yes|
|Headers (names and values)|&lt;|yes|
|*Steps*|&lt;|&lt;|
| |Name|yes|
|^|URL^[3](supported_by_location_user#footnotes)^|yes|
|^|Variables (values only)|yes|
|^|Headers (names and values)|yes|
|^|Timeout|no|
|^|Required string|yes|
|^|Required status codes|no|
|*Authentication*|&lt;|&lt;|
| |User|yes|
|^|Password|yes|
|^|SSL certificate|yes|
|^|SSL key file|yes|
|^|SSL key password|yes|
|*Tags*^[2](supported_by_location_user#footnotes)^|&lt;|&lt;|
| |Tag names|yes|
|^|Tag values|yes|</source>
      </trans-unit>
      <trans-unit id="efe84e1d" xml:space="preserve">
        <source>#### Other locations

In addition to the locations listed here, user macros can be used in the
following fields:

|Location|&lt;|Multiple macros/mix with text^[1](supported_by_location_user#footnotes)^|
|-|------------------------------|----------|
|Global scripts (URL, script, SSH, Telnet, IPMI), including confirmation text|&lt;|yes|
|Webhooks|&lt;|&lt;|
| |JavaScript script|no|
|^|JavaScript script parameter name|no|
|^|JavaScript script parameter value|yes|
|*Dashboards*|&lt;|&lt;|
| |Description field of *Item value* dashboard widget|yes|
|^|URL^[3](supported_by_location_user#footnotes)^ field of *dynamic URL* dashboard widget|yes|
|*Users → Users → Media*|&lt;|&lt;|
| |When active|no|
|*Administration → General → GUI*|&lt;|&lt;|
| |Working time|no|
|*Administration → General → Connectors*|&lt;|&lt;|
| |URL|yes|
|^|Username|yes|
|^|Password|yes|
|^|Bearer token|yes|
|^|Timeout|no|
|^|HTTP proxy|yes|
|^|SSL certificate file|yes|
|^|SSL key file|yes|
|^|SSL key password|yes|
|*Alerts → Media types → Message templates*|&lt;|&lt;|
| |Subject|yes|
|^|Message|yes|
|*Alerts → Media types → Script*|&lt;|&lt;|
| |Script parameters|yes|

For a complete list of all macros supported in Zabbix, see [supported
macros](/manual/appendix/macros/supported_by_location).</source>
      </trans-unit>
      <trans-unit id="5a370e78" xml:space="preserve">
        <source>##### Footnotes

^**1**^ If multiple macros in a field or macros mixed with text are not
supported for the location, a single macro has to fill the whole field.

^**2**^ Macros used in tag names and values are resolved only during event generation process.

^**3**^ URLs that contain a [secret
macro](/manual/config/macros/user_macros#configuration) will not work,
as the macro in them will be resolved as "\*\*\*\*\*\*".</source>
      </trans-unit>
    </body>
  </file>
</xliff>
