<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/proxy/get.md">
    <body>
      <trans-unit id="70dbc2c9" xml:space="preserve">
        <source># proxy.get</source>
      </trans-unit>
      <trans-unit id="b91ae38b" xml:space="preserve">
        <source>### Description

`integer/array proxy.get(object parameters)`

The method allows to retrieve proxies according to the given parameters.

::: noteclassic
This method is available to users of any type. Permissions to call the method can be revoked in user role settings. See
[User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::</source>
      </trans-unit>
      <trans-unit id="74f0aaac" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|proxyids|string/array|Return only proxies with the given IDs.|
|selectHosts|query|Return a [`hosts`](/manual/api/reference/host/object) property with the hosts monitored by the proxy.|
|selectInterface|query|Return an [`interface`](/manual/api/reference/proxy/object#proxy_interface) property with the proxy interface used by a passive proxy.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `hostid`, `host`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="b40fb513" xml:space="preserve">
        <source>#### Retrieve all proxies

Retrieve all configured proxies and their interfaces.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "proxy.get",
    "params": {
        "output": "extend",
        "selectInterface": "extend"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "host": "Active proxy",
            "status": "5",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "proxy_address": "",
            "auto_compress": "0",
            "version": "60400",
            "compatibility": "1",
            "proxyid": "30091",
            "interface": []
        },
        {
            "host": "Passive proxy",
            "status": "6",
            "description": "",
            "tls_connect": "1",
            "tls_accept": "1",
            "tls_issuer": "",
            "tls_subject": "",
            "proxy_address": "",
            "auto_compress": "0",
            "lastaccess": "0",
            "version": "0",
            "compatibility": "0",
            "proxyid": "30092",
            "interface": {
                "interfaceid": "30109",
                "hostid": "30092",
                "main": "1",
                "type": "0",
                "useip": "1",
                "ip": "127.0.0.1",
                "dns": "",
                "port": "10051",
                "available": "0",
                "error": "",
                "errors_from": "0",
                "disable_until": "0",
                "details": []
            }
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="273e0fd8" xml:space="preserve">
        <source>### See also

-   [Host](/manual/api/reference/host/object#host)
-   [Proxy interface](object#proxy_interface)</source>
      </trans-unit>
      <trans-unit id="b9a96f2c" xml:space="preserve">
        <source>### Source

CProxy::get() in *ui/include/classes/api/services/CProxy.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
