[comment]: # translation:outdated

[comment]: # ({new-f5d7e62f})
# sla.create

[comment]: # ({/new-f5d7e62f})

[comment]: # ({new-c44d5a50})
### Description

`object sla.create(object/array SLAs)`

This method allows to create new SLA objects.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c44d5a50})

[comment]: # ({new-69be5e01})
### Parameters

`(object/array)` SLA objects to create.

Additionally to the [standard SLA properties](object#sla), the
method accepts the following parameters.

| Parameter                      | [Type](/manual/api/reference_commentary#data_types) | Description                                                                                                                                 |
|--------------------------------|-----------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| **service_tags**<br>(required) | array                                               | SLA service tags to be created for the SLA.<br>At least one service tag must be specified.                                                  |
| schedule                       | array                                               | SLA schedule to be created for the SLA.<br>Specifying an empty parameter will be interpreted as a 24x7 schedule.<br>Default: 24x7 schedule. |
| excluded_downtimes             | array                                               | SLA excluded downtimes to be created for the SLA.                                                                                           |

[comment]: # ({/new-69be5e01})

[comment]: # ({new-f5eb6cfd})
### Return values

`(object)` Returns an object containing the IDs of the created SLAs
under the `slaids` property. The order of the returned IDs matches
the order of the passed SLAs.

[comment]: # ({/new-f5eb6cfd})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-fc6bed3c})
#### Creating an SLA

Instruct to create an SLA entry for:
* tracking uptime for SQL-engine related services;
* custom schedule of all weekdays excluding last hour on Saturday;
* an effective date of the last day of the year 2022;
* with 1 hour and 15 minutes long planned downtime starting at midnight on the 4th of July;
* SLA weekly report calculation will be on;
* the minimum acceptable SLO will be 99.9995%.

[comment]: # ({/new-fc6bed3c})

[comment]: # ({new-18749288})
Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "sla.create",
    "params": [
        {
            "name": "Database Uptime",
            "slo": "99.9995",
            "period": "1",
            "timezone": "America/Toronto",
            "description": "Provide excellent uptime for main database engines.",
            "effective_date": 1672444800,
            "status": 1,
            "schedule": [
                {
                    "period_from": 0,
                    "period_to": 601200
                }
            ],
            "service_tags": [
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "MySQL"
                },
                {
                    "tag": "Database",
                    "operator": "0",
                    "value": "PostgreSQL"
                }
            ],
            "excluded_downtimes": [
                {
                    "name": "Software version upgrade rollout",
                    "period_from": "1648760400",
                    "period_to": "1648764900"
                }
            ]
        }
    ],
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "slaids": [
            "5"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-18749288})

[comment]: # ({new-fda6729e})
### Source

CSla::create() in *ui/include/classes/api/services/CSla.php*.

[comment]: # ({/new-fda6729e})
