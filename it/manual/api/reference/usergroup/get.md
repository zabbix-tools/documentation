[comment]: # translation:outdated

[comment]: # ({new-ce8d2bd2})
# usergroup.get

[comment]: # ({/new-ce8d2bd2})

[comment]: # ({new-ef79b8a9})
### Description

`integer/array usergroup.get(object parameters)`

The method allows to retrieve user groups according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-ef79b8a9})

[comment]: # ({new-4be9c06e})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|status|integer|Return only user groups with the given status.<br><br>Refer to the [user group page](object#user_group) for a list of supported statuses.|
|userids|string/array|Return only user groups that contain the given users.|
|usrgrpids|string/array|Return only user groups with the given IDs.|
|selectTagFilters|query|Return user group tag based permissions in the [tag\_filters](/manual/api/reference/usergroup/object#tag_based_permission) property.<br><br>It has the following properties:<br>`groupid` - (string) ID of the host group;<br>`tag` - (string) tag name;<br>`value` - (string) tag value.|
|selectUsers|query|Return the users from the user group in the [users](/manual/api/reference/user/object#user) property.|
|selectRights|query|Return user group rights in the [rights](/manual/api/reference/usergroup/object#permission) property.<br><br>It has the following properties:<br>`permission` - (integer) access level to the host group;<br>`id` - (string) ID of the host group.<br><br>Refer to the [user group page](object#permission) for a list of access levels to host groups.|
|limitSelects|integer|Limits the number of records returned by subselects.|
|sortfield|string/array|Sort the result by the given properties.<br><br>Possible values are: `usrgrpid`, `name`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|
|editable|boolean|^|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-4be9c06e})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-62cd5d8e})
#### Retrieving enabled user groups

Retrieve all enabled user groups.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "usergroup.get",
    "params": {
        "output": "extend",
        "status": 0
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "usrgrpid": "7",
            "name": "Zabbix administrators",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "1"
        },
        {
            "usrgrpid": "8",
            "name": "Guests",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "11",
            "name": "Enabled debug mode",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "1"
        },
        {
            "usrgrpid": "12",
            "name": "No access to the frontend",
            "gui_access": "2",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "14",
            "name": "Read only",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        },
        {
            "usrgrpid": "18",
            "name": "Deny",
            "gui_access": "0",
            "users_status": "0",
            "debug_mode": "0"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-62cd5d8e})

[comment]: # ({new-039ccba1})
### See also

-   [User](/manual/api/reference/user/object#user)

[comment]: # ({/new-039ccba1})

[comment]: # ({new-e72b2ac9})
### Source

CUserGroup::get() in *ui/include/classes/api/services/CUserGroup.php*.

[comment]: # ({/new-e72b2ac9})
