[comment]: # translation:outdated

[comment]: # ({new-119c5103})
# > User group object

The following objects are directly related to the `usergroup` API.

[comment]: # ({/new-119c5103})

[comment]: # ({new-3c2622e5})
### User group

The user group object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|usrgrpid|string|*(readonly)* ID of the user group.|
|**name**<br>(required)|string|Name of the user group.|
|debug\_mode|integer|Whether debug mode is enabled or disabled.<br><br>Possible values are:<br>0 - *(default)* disabled;<br>1 - enabled.|
|gui\_access|integer|Frontend authentication method of the users in the group.<br><br>Possible values:<br>0 - *(default)* use the system default authentication method;<br>1 - use internal authentication;<br>2 - use LDAP authentication;<br>3 - disable access to the frontend.|
|users\_status|integer|Whether the user group is enabled or disabled.<br><br>Possible values are:<br>0 - *(default)* enabled;<br>1 - disabled.|

[comment]: # ({/new-3c2622e5})

[comment]: # ({new-ba46c802})
### Permission

The permission object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**id**<br>(required)|string|ID of the host group to add permission to.|
|**permission**<br>(required)|integer|Access level to the host group.<br><br>Possible values:<br>0 - access denied;<br>2 - read-only access;<br>3 - read-write access.|

[comment]: # ({/new-ba46c802})

[comment]: # ({new-33ad932c})
### Tag based permission

The tag based permission object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**groupid**<br>(required)|string|ID of the host group to add permission to.|
|**tag**|string|Tag name.|
|**value**|string|Tag value.|

[comment]: # ({/new-33ad932c})
