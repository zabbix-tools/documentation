[comment]: # translation:outdated

[comment]: # ({new-cd0e20e4})
# item.delete

[comment]: # ({/new-cd0e20e4})

[comment]: # ({new-16951644})
### Description

`object item.delete(array itemIds)`

This method allows to delete items.

::: noteclassic
Web items cannot be deleted via the Zabbix API.
:::

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-16951644})

[comment]: # ({new-3574199c})
### Parameters

`(array)` IDs of the items to delete.

[comment]: # ({/new-3574199c})

[comment]: # ({new-b676662e})
### Return values

`(object)` Returns an object containing the IDs of the deleted items
under the `itemids` property.

[comment]: # ({/new-b676662e})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-92fe5da6})
#### Deleting multiple items

Delete two items.\
Dependent items and item prototypes are removed automatically if master
item is deleted.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "item.delete",
    "params": [
        "22982",
        "22986"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "itemids": [
            "22982",
            "22986"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-92fe5da6})

[comment]: # ({new-4c7d30be})
### Source

CItem::delete() in *ui/include/classes/api/services/CItem.php*.

[comment]: # ({/new-4c7d30be})
