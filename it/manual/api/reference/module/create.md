[comment]: # translation:outdated

[comment]: # ({new-2f868b63})
# module.create

[comment]: # ({/new-2f868b63})

[comment]: # ({new-b100d64b})
### Description

`object module.create(object/array modules)`

This method allows to install new frontend modules.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

::: noteimportant
Module files must be unpacked manually in the correct subdirectories, matching the `relative_path` property of the modules.
:::

[comment]: # ({/new-b100d64b})

[comment]: # ({new-a3899c85})
### Parameters

`(object/array)` Modules to create.

The method accepts modules with the [standard module properties](object#module).

[comment]: # ({/new-a3899c85})

[comment]: # ({new-b4af5197})
### Return values

`(object)` Returns an object containing the IDs of the installed modules under the `moduleids` property.
The order of the returned IDs matches the order of the passed modules.

[comment]: # ({/new-b4af5197})

[comment]: # ({new-d5892b27})
### Examples

[comment]: # ({/new-d5892b27})

[comment]: # ({new-f4bbc4e4})
#### Installing a module

Install a module with the status "Enabled".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "module.create",
    "params": {
        "id": "example_module",
        "relative_path": "modules/example_module",
        "status": 1
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "moduleids": [
            "25"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-f4bbc4e4})

[comment]: # ({new-97287e12})
### See also

-   [Module](object#module)
-   [Frontend modules](/manual/extensions/frontendmodules)

[comment]: # ({/new-97287e12})

[comment]: # ({new-174be480})
### Source

CModule::create() in *ui/include/classes/api/services/CModule.php*.

[comment]: # ({/new-174be480})
