[comment]: # translation:outdated

[comment]: # ({new-05e9a4a1})
# module.update

[comment]: # ({/new-05e9a4a1})

[comment]: # ({new-779d6c90})
### Description

`object module.update(object/array modules)`

This method allows to update existing modules.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings.
See [User roles](/manual/web_interface/frontend_sections/users/user_roles) for more information.
:::

[comment]: # ({/new-779d6c90})

[comment]: # ({new-ad356045})
### Parameters

`(object/array)` Module properties to be updated.

The `moduleid` property must be defined for each module, all other properties are optional.
Only the specified properties will be updated.

The method accepts modules with the [standard module properties](object#module).

[comment]: # ({/new-ad356045})

[comment]: # ({new-cc12ced5})
### Return values

`(object)` Returns an object containing the IDs of the updated modules under the `moduleids` property.

[comment]: # ({/new-cc12ced5})

[comment]: # ({new-161c7484})
### Examples

[comment]: # ({/new-161c7484})

[comment]: # ({new-ee634823})
#### Disabling a module

Disable module "25".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "module.update",
    "params": {
        "moduleid": "25",
        "status": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "moduleids": [
            "25"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ee634823})

[comment]: # ({new-28606e62})
### See also

-   [Module](object#module)
-   [Frontend modules](/manual/extensions/frontendmodules)

[comment]: # ({/new-28606e62})

[comment]: # ({new-9dc6573c})
### Source

CModule::update() in *ui/include/classes/api/services/CModule.php*.

[comment]: # ({/new-9dc6573c})
