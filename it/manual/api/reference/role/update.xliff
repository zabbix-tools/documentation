<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/role/update.md">
    <body>
      <trans-unit id="a0f60ec7" xml:space="preserve">
        <source># role.update</source>
      </trans-unit>
      <trans-unit id="63875138" xml:space="preserve">
        <source>### Description

`object role.update(object/array roles)`

This method allows to update existing roles.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="fa5c3d93" xml:space="preserve">
        <source>### Parameters

`(object/array)` Role properties to be updated.

The `roleid` property must be defined for each role, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard role properties](object#role) the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|rules|array|Access [rules](object#role_rules) to replace the current access rules assigned to the role.|</source>
      </trans-unit>
      <trans-unit id="12d196f6" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the updated roles
under the `roleids` property.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="1ce1fe70" xml:space="preserve">
        <source>#### Disabling ability to execute scripts

Update role with ID "5", disable ability to execute scripts.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "role.update",
    "params": [
        {
            "roleid": "5",
            "rules": {
                "actions": [
                    {
                        "name": "execute_scripts",
                        "status": "0"
                    }
                ]
            }
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "5"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="a6349165" xml:space="preserve">
        <source>#### Limiting access to API

Update role with ID "5", deny to call any "create", "update" or "delete"
methods.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "role.update",
    "params": [
        {
            "roleid": "5",
            "rules": {
                "api.access": "1",
                "api.mode": "0",
                "api": ["*.create", "*.update", "*.delete"]
            }
        }
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "roleids": [
            "5"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="36db1bd2" xml:space="preserve">
        <source>### Source

CRole::update() in *ui/include/classes/api/services/CRole.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
