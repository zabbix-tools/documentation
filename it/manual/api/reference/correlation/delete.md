[comment]: # translation:outdated

[comment]: # ({new-89029829})
# correlation.delete

[comment]: # ({/new-89029829})

[comment]: # ({new-4922e104})
### Description

`object correlation.delete(array correlationids)`

This method allows to delete correlations.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-4922e104})

[comment]: # ({new-b445698c})
### Parameters

`(array)` IDs of the correlations to delete.

[comment]: # ({/new-b445698c})

[comment]: # ({new-855af217})
### Return values

`(object)` Returns an object containing the IDs of the deleted
correlations under the `correlationids` property.

[comment]: # ({/new-855af217})

[comment]: # ({new-c9f65268})
### Example

[comment]: # ({/new-c9f65268})

[comment]: # ({new-bfc49991})
#### Delete multiple correlations

Delete two correlations.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.delete",
    "params": [
        "1",
        "2"
    ],
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlaionids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-bfc49991})

[comment]: # ({new-b0713c0f})
### Source

CCorrelation::delete() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-b0713c0f})
