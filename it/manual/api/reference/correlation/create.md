[comment]: # translation:outdated

[comment]: # ({new-fbeccd23})
# correlation.create

[comment]: # ({/new-fbeccd23})

[comment]: # ({new-c06fb521})
### Description

`object correlation.create(object/array correlations)`

This method allows to create new correlations.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c06fb521})

[comment]: # ({new-8a55e636})
### Parameters

`(object/array)` Correlations to create.

Additionally to the [standard correlation
properties](object#correlation), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**operations**<br>(required)|array|Correlation [operations](/manual/api/reference/correlation/object#correlation_operation) to create for the correlation.|
|**filter**<br>(required)|object|Correlation [filter](/manual/api/reference/correlation/object#correlation_filter) object for the correlation.|

[comment]: # ({/new-8a55e636})

[comment]: # ({new-88660193})
### Return values

`(object)` Returns an object containing the IDs of the created
correlations under the `correlationids` property. The order of the
returned IDs matches the order of the passed correlations.

[comment]: # ({/new-88660193})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7325d5f8})
#### Create a new event tag correlation

Create a correlation using evaluation method `AND/OR` with one condition
and one operation. By default the correlation will be enabled.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new event tag correlation",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "type": 1,
                    "tag": "ok"
                }
            ]
        },
        "operations": [
            {
                "type": 0
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7325d5f8})

[comment]: # ({new-a12df7f6})
#### Using a custom expression filter

Create a correlation that will use a custom filter condition. The
formula IDs "A" or "B" have been chosen arbitrarily. Condition type will
be "Host group" with operator "<>".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "correlation.create",
    "params": {
        "name": "new host group correlation",
        "description": "a custom description",
        "status": 0,
        "filter": {
            "evaltype": 3,
            "formula": "A or B",
            "conditions": [
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "A"
                },
                {
                    "type": 2,
                    "operator": 1,
                    "formulaid": "B"
                }
            ]
        },
        "operations": [
            {
                "type": 1
            }
        ]
    },
    "auth": "343baad4f88b4106b9b5961e77437688",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "correlationids": [
            "2"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-a12df7f6})

[comment]: # ({new-e7d15410})
### See also

-   [Correlation filter](object#correlation_filter)
-   [Correlation operation](object#correlation_operation)

[comment]: # ({/new-e7d15410})

[comment]: # ({new-934ba89b})
### Source

CCorrelation::create() in
*ui/include/classes/api/services/CCorrelation.php*.

[comment]: # ({/new-934ba89b})
