[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# templategroup.massadd

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object templategroup.massadd(object parameters)`

This method allows to simultaneously add multiple related objects to all
the given template groups.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-3ff1a765})
### Parameters

`(object)` Parameters containing the IDs of the template groups to update
and the objects to add to all the template groups.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**groups**<br>(required)|object/array|Template groups to be updated.<br><br>The template groups must have the `groupid` property defined.|
|templates|object/array|Templates to add to all template groups.<br><br>The templates must have the `templateid` property defined.|

[comment]: # ({/new-3ff1a765})

[comment]: # ({new-cc7938a3})
### Return values

`(object)` Returns an object containing the IDs of the updated template
groups under the `groupids` property.

[comment]: # ({/new-cc7938a3})

[comment]: # ({new-518dc5ee})
### Examples

[comment]: # ({/new-518dc5ee})

[comment]: # ({new-3954c00e})
#### Adding templates to template groups

Add two templates to template groups with IDs 12 and 13.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "templategroup.massadd",
    "params": {
        "groups": [
            {
                "groupid": "12"
            },
            {
                "groupid": "13"
            }
        ],
        "templates": [
            {
                "templateid": "10486"
            },
            {
                "templateid": "10487"
            }
        ]
    },
    "auth": "f223adf833b2bf2ff38574a67bba6372",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "groupids": [
            "12",
            "13"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-3954c00e})

[comment]: # ({new-d05fe284})
### See also

-   [Template](/manual/api/reference/template/object#template)

[comment]: # ({/new-d05fe284})

[comment]: # ({new-47263542})
### Source

CTemplateGroup::massAdd() in
*ui/include/classes/api/services/CTemplateGroup.php*.

[comment]: # ({/new-47263542})
