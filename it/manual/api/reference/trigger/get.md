[comment]: # translation:outdated

[comment]: # ({new-fc5b81d2})
# trigger.get

[comment]: # ({/new-fc5b81d2})

[comment]: # ({new-e77e3d1e})
### Description

`integer/array trigger.get(object parameters)`

The method allows to retrieve triggers according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-e77e3d1e})

[comment]: # ({new-44938a82})
### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|triggerids|string/array|Return only triggers with the given IDs.|
|groupids|string/array|Return only triggers that belong to hosts from the given host groups.|
|templateids|string/array|Return only triggers that belong to the given templates.|
|hostids|string/array|Return only triggers that belong to the given hosts.|
|itemids|string/array|Return only triggers that contain the given items.|
|functions|string/array|Return only triggers that use the given functions.<br><br>Refer to the [supported function](/manual/appendix/functions) page for a list of supported functions.|
|group|string|Return only triggers that belong to hosts from the host group with the given name.|
|host|string|Return only triggers that belong to host with the given name.|
|inherited|boolean|If set to `true` return only triggers inherited from a template.|
|templated|boolean|If set to `true` return only triggers that belong to templates.|
|dependent|boolean|If set to `true` return only triggers that have dependencies. If set to `false` return only triggers that do not have dependencies.|
|monitored|flag|Return only enabled triggers that belong to monitored hosts and contain only enabled items.|
|active|flag|Return only enabled triggers that belong to monitored hosts.|
|maintenance|boolean|If set to `true` return only enabled triggers that belong to hosts in maintenance.|
|withUnacknowledgedEvents|flag|Return only triggers that have unacknowledged events.|
|withAcknowledgedEvents|flag|Return only triggers with all events acknowledged.|
|withLastEventUnacknowledged|flag|Return only triggers with the last event unacknowledged.|
|skipDependent|flag|Skip triggers in a problem state that are dependent on other triggers. Note that the other triggers are ignored if disabled, have disabled items or disabled item hosts.|
|lastChangeSince|timestamp|Return only triggers that have changed their state after the given time.|
|lastChangeTill|timestamp|Return only triggers that have changed their state before the given time.|
|only\_true|flag|Return only triggers that have recently been in a problem state.|
|min\_severity|integer|Return only triggers with severity greater or equal than the given severity.|
|evaltype|integer|Rules for tag searching.<br><br>Possible values:<br>0 - (default) And/Or;<br>2 - Or.|
|tags|array of objects|Return only triggers with given tags. Exact match by tag and case-sensitive or case-insensitive search by tag value depending on operator value.<br>Format: `[{"tag": "<tag>", "value": "<value>", "operator": "<operator>"}, ...]`.<br>An empty array returns all triggers.<br><br>Possible operator types:<br>0 - (default) Like;<br>1 - Equal;<br>2 - Not like;<br>3 - Not equal<br>4 - Exists;<br>5 - Not exists.|
|expandComment|flag|Expand macros in the trigger description.|
|expandDescription|flag|Expand macros in the name of the trigger.|
|expandExpression|flag|Expand functions and macros in the trigger expression.|
|selectGroups|query|Return the host groups that the trigger belongs to in the [groups](/manual/api/reference/hostgroup/object) property.|
|selectHosts|query|Return the hosts that the trigger belongs to in the [hosts](/manual/api/reference/host/object) property.|
|selectItems|query|Return items contained by the trigger in the [items](/manual/api/reference/item/object) property.|
|selectFunctions|query|Return functions used in the trigger in the `functions` property.<br><br>The function objects represent the functions used in the trigger expression and has the following properties:<br>`functionid` - *(string)* ID of the function;<br>`itemid` - *(string)* ID of the item used in the function;<br>`function` - *(string)* name of the function;<br>`parameter` - *(string)* parameter passed to the function. Query parameter is replaced by `$` symbol in returned string.|
|selectDependencies|query|Return triggers that the trigger depends on in the `dependencies` property.|
|selectDiscoveryRule|query|Return the [low-level discovery rule](/manual/api/reference/discoveryrule/object#discovery_rule) that created the trigger.|
|selectLastEvent|query|Return the last significant trigger event in the [lastEvent](/manual/api/reference/event/object) property.|
|selectTags|query|Return the trigger tags in [tags](/manual/api/reference/trigger/object#Trigger_tag) property.|
|selectTriggerDiscovery|query|Return the trigger discovery object in the `triggerDiscovery` property. The trigger discovery objects link the trigger to a trigger prototype from which it was created.<br><br>It has the following properties:<br>`parent_triggerid` - `(string)` ID of the trigger prototype from which the trigger has been created.|
|filter|object|Return only those results that exactly match the given filter.<br><br>Accepts an array, where the keys are property names, and the values are either a single value or an array of values to match against.<br><br>Supports additional filters:<br>`host` - technical name of the host that the trigger belongs to;<br>`hostid` - ID of the host that the trigger belongs to.|
|limitSelects|integer|Limits the number of records returned by subselects.<br><br>Applies to the following subselects:<br>`selectHosts` - results will be sorted by `host`.|
|sortfield|string/array|[Sort](/manual/api/reference_commentary#common_get_method_parameters) the result by the given properties.<br><br>Possible values are: `triggerid`, `description`, `status`, `priority`, `lastchange` and `hostname`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|editable|boolean|^|
|excludeSearch|boolean|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|

[comment]: # ({/new-44938a82})

[comment]: # ({new-7223bab1})
### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.

[comment]: # ({/new-7223bab1})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-3d27e76d})
#### Retrieving data by trigger ID

Retrieve all data and the functions used in trigger "14062".

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "triggerids": "14062",
        "output": "extend",
        "selectFunctions": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "14062",
            "expression": "{13513}<10m",
            "description": "{HOST.NAME} has been restarted (uptime < 10m)",
            "url": "",
            "status": "0",
            "value": "0",
            "priority": "2",
            "lastchange": "0",
            "comments": "The host uptime is less than 10 minutes",
            "error": "",
            "templateid": "10016",
            "type": "0",
            "state": "0",
            "flags": "0",
            "recovery_mode": "0",
            "recovery_expression": "",
            "correlation_mode": "0",
            "correlation_tag": "",
            "manual_close": "0",
            "opdata": "",
            "functions": [
                {
                    "functionid": "13513",
                    "itemid": "24350",
                    "triggerid": "14062",
                    "parameter": "$",
                    "function": "last"
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-3d27e76d})

[comment]: # ({new-8a31301f})
#### Retrieving triggers in problem state

Retrieve the ID, name and severity of all triggers in problem state and
sort them by severity in descending order.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "output": [
            "triggerid",
            "description",
            "priority"
        ],
        "filter": {
            "value": 1
        },
        "sortfield": "priority",
        "sortorder": "DESC"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "13907",
            "description": "Zabbix self-monitoring processes < 100% busy",
            "priority": "4"
        },
        {
            "triggerid": "13824",
            "description": "Zabbix discoverer processes more than 75% busy",
            "priority": "3"
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8a31301f})

[comment]: # ({new-8005126d})
#### Retrieving a specific trigger with tags

Retrieve a specific trigger with tags.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "trigger.get",
    "params": {
        "output": [
            "triggerid",
            "description"
        ],
        "selectTags": "extend",
        "triggerids": [
            "17578"
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": [
        {
            "triggerid": "17370",
            "description": "Service status",
            "tags": [
                {
                    "tag": "service",
                    "value": "{{ITEM.VALUE}.regsub(\"Service (.*) has stopped\", \"\\1\")}"
                },
                {
                    "tag": "error",
                    "value": ""
                }
            ]
        }
    ],
    "id": 1
}
```

[comment]: # ({/new-8005126d})

[comment]: # ({new-222d46a0})
### See also

-   [Discovery
    rule](/manual/api/reference/discoveryrule/object#discovery_rule)
-   [Item](/manual/api/reference/item/object#item)
-   [Host](/manual/api/reference/host/object#host)
-   [Host group](/manual/api/reference/hostgroup/object#host_group)

[comment]: # ({/new-222d46a0})

[comment]: # ({new-07515392})
### Source

CTrigger::get() in *ui/include/classes/api/services/CTrigger.php*.

[comment]: # ({/new-07515392})
