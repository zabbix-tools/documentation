[comment]: # translation:outdated

[comment]: # ({new-3816cd64})
# Item

This class is designed to work with items.

Object references:\

-   [Item](/manual/api/reference/item/object#item)

Available methods:\

-   [item.create](/manual/api/reference/item/create) - creating new
    items
-   [item.delete](/manual/api/reference/item/delete) - deleting items
-   [item.get](/manual/api/reference/item/get) - retrieving items
-   [item.update](/manual/api/reference/item/update) - updating items

[comment]: # ({/new-3816cd64})
