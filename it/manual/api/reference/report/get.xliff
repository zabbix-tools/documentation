<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/report/get.md">
    <body>
      <trans-unit id="7925fa72" xml:space="preserve">
        <source># report.get</source>
      </trans-unit>
      <trans-unit id="22ec64b6" xml:space="preserve">
        <source>### Description

`integer/array report.get(object parameters)`

The method allows to retrieve scheduled reports according to the given
parameters.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="2bc4ba55" xml:space="preserve">
        <source>### Parameters

`(object)` Parameters defining the desired output.

The method supports the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|reportids|string/array|Return only scheduled reports with the given report IDs.|
|expired|boolean|If set to `true` returns only expired scheduled reports, if `false` - only active scheduled reports.|
|selectUsers|query|Return a [`users`](/manual/api/reference/user/object) property the report is configured to be sent to.|
|selectUserGroups|query|Return a [`user_groups`](/manual/api/reference/user/object) property the report is configured to be sent to.|
|sortfield|string/array|Sort the result by the given properties.&lt;br&gt;&lt;br&gt;Possible values: `reportid`, `name`, `status`.|
|countOutput|boolean|These parameters being common for all `get` methods are described in detail in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters) page.|
|excludeSearch|boolean|^|
|filter|object|^|
|limit|integer|^|
|output|query|^|
|preservekeys|boolean|^|
|search|object|^|
|searchByAny|boolean|^|
|searchWildcardsEnabled|boolean|^|
|sortorder|string/array|^|
|startSearch|boolean|^|</source>
      </trans-unit>
      <trans-unit id="7223bab1" xml:space="preserve">
        <source>### Return values

`(integer/array)` Returns either:

-   an array of objects;
-   the count of retrieved objects, if the `countOutput` parameter has
    been used.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="b7379080" xml:space="preserve">
        <source>#### Retrieving report data

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "report.get",
    "params": [
        "output": "extend",
        "selectUsers": "extend",
        "selectUserGroups": "extend",
        "reportids": ["1", "2"]
    ],
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": [
        {
            "reportid": "1",
            "userid": "1",
            "name": "Weekly report",
            "dashboardid": "1",
            "period": "1",
            "cycle": "1",
            "start_time": "43200",
            "weekdays": "31",
            "active_since": "2021-04-01",
            "active_till": "2021-08-31",
            "subject": "Weekly report",
            "message": "Report accompanying text",
            "status": "1",
            "description": "Report description",
            "state": "1",
            "lastsent": "1613563219",
            "info": "",
            "users": [
                {
                    "userid": "1",
                    "access_userid": "1",
                    "exclude": "0"
                },
                {
                    "userid": "2",
                    "access_userid": "0",
                    "exclude": "1"
                }
            ],
            "user_groups": [
                {
                    "usrgrpid": "7",
                    "access_userid": "0"
                }
            ]
        },
        {
            "reportid": "2",
            "userid": "1",
            "name": "Monthly report",
            "dashboardid": "2",
            "period": "2",
            "cycle": "2",
            "start_time": "0",
            "weekdays": "0",
            "active_since": "2021-05-01",
            "active_till": "",
            "subject": "Monthly report",
            "message": "Report accompanying text",
            "status": "1",
            "description": "",
            "state": "0",
            "lastsent": "0",
            "info": "",
            "users": [
                {
                    "userid": "1",
                    "access_userid": "1",
                    "exclude": "0"
                }
            ],
            "user_groups": []
        }
    ],
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="7b8422b2" xml:space="preserve">
        <source>### See also

-   [Users](/manual/api/reference/user/object#users)
-   [User groups](/manual/api/reference/user/object#user_groups)</source>
      </trans-unit>
      <trans-unit id="de82795f" xml:space="preserve">
        <source>### Source

CReport::get() in *ui/include/classes/api/services/CReport.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
