<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/dashboard/create.md">
    <body>
      <trans-unit id="c179cb27" xml:space="preserve">
        <source># dashboard.create</source>
      </trans-unit>
      <trans-unit id="00970602" xml:space="preserve">
        <source>### Description

`object dashboard.create(object/array dashboards)`

This method allows to create new dashboards.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="df2301b4" xml:space="preserve">
        <source>### Parameters

`(object/array)` Dashboards to create.

Additionally to the [standard dashboard properties](object#dashboard),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|pages|array|Dashboard [pages](object#dashboard_page) to be created for the dashboard. Dashboard pages will be ordered in the same order as specified.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required*|
|users|array|Dashboard [user](object#dashboard_user) shares to be created on the dashboard.|
|userGroups|array|Dashboard [user group](object#dashboard_user_group) shares to be created on the dashboard.|</source>
      </trans-unit>
      <trans-unit id="f460a18e" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created
dashboards under the `dashboardids` property. The order of the returned
IDs matches the order of the passed dashboards.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="b33dc2ee" xml:space="preserve">
        <source>#### Creating a dashboard

Create a dashboard named "My dashboard" with one Problems widget with
tags and using two types of sharing (user group and user) on a single
dashboard page.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "problems",
                        "x": 0,
                        "y": 0,
                        "width": 12,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "tags.tag.0",
                                "value": "service"
                            },
                            {
                                "type": 0,
                                "name": "tags.operator.0",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "tags.value.0",
                                "value": "zabbix_server"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": "7",
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": "4",
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "2"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="299ae089" xml:space="preserve">
        <source>### See also

-   [Dashboard page](object#dashboard_page)
-   [Dashboard widget](object#dashboard_widget)
-   [Dashboard widget field](object#dashboard_widget_field)
-   [Dashboard user](object#dashboard_user)
-   [Dashboard user group](object#dashboard_user_group)</source>
      </trans-unit>
      <trans-unit id="7d488f32" xml:space="preserve">
        <source>### Source

CDashboard::create() in
*ui/include/classes/api/services/CDashboard.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
