<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/dashboard/widget_fields/map.md">
    <body>
      <trans-unit id="592a0eef" xml:space="preserve">
        <source># 13 Map</source>
      </trans-unit>
      <trans-unit id="abd28bf0" xml:space="preserve">
        <source>### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Map*](/manual/web_interface/frontend_sections/dashboards/widgets/map) widget in `dashboard.create` and `dashboard.update` methods.</source>
      </trans-unit>
      <trans-unit id="4f526f4d" xml:space="preserve">
        <source>### Parameters

The following parameters are supported for the *Map* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;&lt;br&gt;10 - 10 seconds;&lt;br&gt;30 - 30 seconds;&lt;br&gt;60 - 1 minute;&lt;br&gt;120 - 2 minutes;&lt;br&gt;600 - 10 minutes;&lt;br&gt;900 - *(default)* 15 minutes.|
|*Source type*|0|source_type|1 - *(default)* Map;&lt;br&gt;2 - Map navigation tree.|
|*Map*|8|sysmapid|[Map](/manual/api/reference/map/get) ID.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Source type* is set to "Map"|
|*Linked widget reference*|1|filter_widget_reference|Valid [*Map navigation tree*](/manual/api/reference/dashboard/widget_fields/map_tree) widget parameter *Reference* value.&lt;br&gt;&lt;br&gt;[Parameter behavior](/manual/api/reference_commentary#parameter-behavior):&lt;br&gt;- *required* if *Source type* is set to "Map navigation tree"|</source>
      </trans-unit>
      <trans-unit id="f67daee4" xml:space="preserve">
        <source>### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Map* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).</source>
      </trans-unit>
      <trans-unit id="c0fde9a7" xml:space="preserve">
        <source>#### Configuring a *Map* widget

Configure a *Map* widget that displays the map "1".

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "map",
                        "name": "Map",
                        "x": 0,
                        "y": 0,
                        "width": 18,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 8,
                                "name": "sysmapid",
                                "value": 1
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="5ca6f7fa" xml:space="preserve">
        <source>#### Configuring a linked *Map* widget

Configure a *Map* widget that is linked to a [*Map navigation tree*](/manual/api/reference/dashboard/widget_fields/map_tree#configuring-a-map-navigation-tree-widget) widget.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "map",
                        "name": "Map",
                        "x": 0,
                        "y": 5,
                        "width": 18,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 0,
                                "name": "source_type",
                                "value": 2
                            },
                            {
                                "type": 1,
                                "name": "filter_widget_reference",
                                "value": "ABCDE"
                            }
                        ]
                    },
                    {
                        "type": "navtree",
                        "name": "Map navigation tree",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "navtree.name.1",
                                "value": "Element A"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.2",
                                "value": "Element B"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.3",
                                "value": "Element C"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.4",
                                "value": "Element A1"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.5",
                                "value": "Element A2"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.6",
                                "value": "Element B1"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.7",
                                "value": "Element B2"
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.4",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.5",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.6",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.7",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.1",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.2",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.3",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.4",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.5",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.6",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.7",
                                "value": 2
                            },
                            {
                                "type": 8,
                                "name": "navtree.sysmapid.6",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "reference",
                                "value": "ABCDE"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="712fb13b" xml:space="preserve">
        <source>### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)
-   [Map navigation tree](/manual/api/reference/dashboard/widget_fields/map_tree)</source>
      </trans-unit>
    </body>
  </file>
</xliff>
