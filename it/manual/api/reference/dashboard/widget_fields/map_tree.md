[comment]: # translation:outdated

[comment]: # ({new-8569d199})
# 14 Map navigation tree

[comment]: # ({/new-8569d199})

[comment]: # ({new-ec996e0f})
### Description

These parameters and the possible property values for the respective dashboard widget field objects allow to configure
the [*Map navigation tree*](/manual/web_interface/frontend_sections/dashboards/widgets/map_tree) widget in `dashboard.create` and `dashboard.update` methods.

[comment]: # ({/new-ec996e0f})

[comment]: # ({new-42ce0b0b})
### Parameters

The following parameters are supported for the *Map navigation tree* widget.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Refresh interval*|0|rf_rate|0 - No refresh;<br>10 - 10 seconds;<br>30 - 30 seconds;<br>60 - 1 minute;<br>120 - 2 minutes;<br>600 - 10 minutes;<br>900 - *(default)* 15 minutes.|
|*Show unavailable maps*|1|show_unavailable|0 - *(default)* Disabled;<br>1 - Enabled.|
|*Reference*|1|reference|Any string value consisting of 5 characters (e.g. `ABCDE`, `JBPNL`, etc.).<br><br>Parameter *Reference* value is used in the [*Map*](/manual/api/reference/dashboard/widget_fields/map) widget (*Linked widget reference*) for linking with the *Map navigation tree* widget.|

The following parameters are supported for configuring map navigation tree elements.

|Parameter|[type](/manual/api/reference/dashboard/object#dashboard-widget-field)|name|value|
|-----|-|-----|-------------------|
|*Name*|1|navtree.name.1|Any string value.<br><br>Note: The number in the property name sets the element number.|
|*Linked map*|8|navtree.sysmapid.1|[Map](/manual/api/reference/map/get) ID.<br><br>Note: The number in the property name references the element to which the map is linked.|
|*Parameters for creating element hierarchy*|0|navtree.parent.1|Parent element number.<br><br>Note: The number in the property name references the child element. The property value references the parent element.|
|^|0|navtree.order.1|Element position in the map navigation tree.<br><br>Note: The number in the property name references the element number. The property value references the element position in the map navigation tree. Parent element position is determined within the whole map navigation tree. Child element position is determined within the parent element.|

[comment]: # ({/new-42ce0b0b})

[comment]: # ({new-825e97c6})
### Examples

The following examples aim to only describe the configuration of the dashboard widget field objects for the *Map navigation tree* widget.
For more information on configuring a dashboard, see [`dashboard.create`](/manual/api/reference/dashboard/create).

[comment]: # ({/new-825e97c6})

[comment]: # ({new-6aecc153})
#### Configuring a *Map navigation tree* widget

Configure a *Map navigation tree* widget that displays the following map navigation tree:

-   Element A
    - Element A1
    - Element A2
-   Element B
    - Element B1 (contains linked map "1" that can be displayed in a [linked *Map widget*](/manual/api/reference/dashboard/widget_fields/map#configuring-a-linked-map-widget))
    - Element B2
-   Element C

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "dashboard.create",
    "params": {
        "name": "My dashboard",
        "display_period": 30,
        "auto_start": 1,
        "pages": [
            {
                "widgets": [
                    {
                        "type": "navtree",
                        "name": "Map navigation tree",
                        "x": 0,
                        "y": 0,
                        "width": 6,
                        "height": 5,
                        "view_mode": 0,
                        "fields": [
                            {
                                "type": 1,
                                "name": "navtree.name.1",
                                "value": "Element A"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.2",
                                "value": "Element B"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.3",
                                "value": "Element C"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.4",
                                "value": "Element A1"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.5",
                                "value": "Element A2"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.6",
                                "value": "Element B1"
                            },
                            {
                                "type": 1,
                                "name": "navtree.name.7",
                                "value": "Element B2"
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.4",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.5",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.6",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.parent.7",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.1",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.2",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.3",
                                "value": 3
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.4",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.5",
                                "value": 2
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.6",
                                "value": 1
                            },
                            {
                                "type": 0,
                                "name": "navtree.order.7",
                                "value": 2
                            },
                            {
                                "type": 8,
                                "name": "navtree.sysmapid.6",
                                "value": 1
                            },
                            {
                                "type": 1,
                                "name": "reference",
                                "value": "ABCDE"
                            }
                        ]
                    }
                ]
            }
        ],
        "userGroups": [
            {
                "usrgrpid": 7,
                "permission": 2
            }
        ],
        "users": [
            {
                "userid": 1,
                "permission": 3
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "dashboardids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-6aecc153})

[comment]: # ({new-f1112a5e})
### See also

-   [Dashboard widget field](/manual/api/reference/dashboard/object#dashboard-widget-field)
-   [`dashboard.create`](/manual/api/reference/dashboard/create)
-   [`dashboard.update`](/manual/api/reference/dashboard/update)
-   [Map](/manual/api/reference/dashboard/widget_fields/map)

[comment]: # ({/new-f1112a5e})
