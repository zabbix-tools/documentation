[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# User directory

This class is designed to work with user directories.

Object references:\

-   [User directory](/manual/api/reference/userdirectory/object#userdirectory)

Available methods:\

-   [userdirectory.create](/manual/api/reference/userdirectory/create) - create new user directory
-   [userdirectory.delete](/manual/api/reference/userdirectory/delete) - delete user directory
-   [userdirectory.get](/manual/api/reference/userdirectory/get) - retrieve user directory
-   [userdirectory.update](/manual/api/reference/userdirectory/update) - update user directory
-   [userdirectory.test](/manual/api/reference/userdirectory/test) - test user directory connection

[comment]: # ({/new-f9e8f670})
