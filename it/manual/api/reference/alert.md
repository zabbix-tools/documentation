[comment]: # translation:outdated

[comment]: # ({new-a68f24c8})
# Alert

This class is designed to work with alerts.

Object references:\

-   [Alert](/manual/api/reference/alert/object#alert)

Available methods:\

-   [alert.get](/manual/api/reference/alert/get) - retrieve alerts

[comment]: # ({/new-a68f24c8})
