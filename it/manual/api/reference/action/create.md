[comment]: # translation:outdated

[comment]: # ({new-c7f202d4})
# action.create

[comment]: # ({/new-c7f202d4})

[comment]: # ({new-71c93aa0})
### Description

`object action.create(object/array actions)`

This method allows to create new actions.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-71c93aa0})

[comment]: # ({new-5a9be3c9})
### Parameters

`(object/array)` Actions to create.

Additionally to the [standard action properties](object#action), the
method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|filter|object|Action [filter](/manual/api/reference/action/object#action_filter) object for the action.|
|operations|array|Action [operations](/manual/api/reference/action/object#action_operation) to create for the action.|
|recovery\_operations|array|Action [recovery operations](/manual/api/reference/action/object#action_recovery_operation) to create for the action.|
|update\_operations|array|Action [update operations](/manual/api/reference/action/object#action_update_operation) to create for the action.|

[comment]: # ({/new-5a9be3c9})

[comment]: # ({new-2830affd})
### Return values

`(object)` Returns an object containing the IDs of the created actions
under the `actionids` property. The order of the returned IDs matches
the order of the passed actions.

[comment]: # ({/new-2830affd})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})




[comment]: # ({new-b81a306b})
#### Create agent autoregistration rule

Add a host to host group "Linux servers" when host name contains "SRV"
or metadata contains "CentOS".

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Register Linux servers",
        "eventsource": "2",
        "status": "0",
        "filter": {
            "evaltype": "2",
            "conditions": [
                {
                    "conditiontype": "22",
                    "operator": "2",
                    "value": "SRV"
                },
                {
                    "conditiontype": "24",
                    "operator": "2",
                    "value": "CentOS"
                }
            ]
        },
        "operations": [
            {
                "operationtype": "4",
                "opgroup": [
                    {
                        "groupid": "2"
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            19
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-b81a306b})

[comment]: # ({new-755496f7})
### See also

-   [Action filter](object#action_filter)
-   [Action operation](object#action_operation)

[comment]: # ({/new-755496f7})

[comment]: # ({new-32335876})
### Source

CAction::create() in *ui/include/classes/api/services/CAction.php*.

[comment]: # ({/new-32335876})

[comment]: # ({new-afc96865})
#### Create a trigger action

Create an action that will be run when a trigger from host "10084" that
has the word "memory" in its name goes into problem state. The action
must first send a message to all users in user group "7". If the event
is not resolved in 4 minutes, it will run script "3" on all hosts in
group "2". On trigger recovery it will notify all users who received any
messages regarding the problem before. On trigger update, message with
custom subject and body will be sent to all who left acknowledgments and
comments via all media types.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084"
                },
                {
                    "conditiontype": 3,
                    "operator": 2,
                    "value": "memory"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            },
            {
                "operationtype": 1,
                "esc_step_from": 3,
                "esc_step_to": 4,
                "evaltype": 0,
                "opconditions": [
                    {
                        "conditiontype": 14,
                        "operator": 0,
                        "value": "0"
                    }
                ],
                "opcommand_grp": [
                    {
                        "groupid": "2"
                    }
                ],
                "opcommand": {
                    "scriptid": "3"
                }
            }
        ],
        "recovery_operations": [
            {
                "operationtype": "11",
                "opmessage": {
                    "default_msg": 1
                }
            }    
        ],
        "update_operations": [
            {
                "operationtype": "12",
                "opmessage": {
                    "default_msg": 0,
                    "message": "Custom update operation message body",
                    "subject": "Custom update operation message subject"
                }
            }
        ],
        "pause_suppressed": "0",
        "notify_if_canceled": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "17"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-afc96865})

[comment]: # ({new-53bdbb4b})
#### Create a discovery action

Create an action that will link discovered hosts to template "10091".

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Discovery action",
        "eventsource": 1,
        "status": 0,
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": 21,
                    "operator": 0,
                    "value": "1"
                },
                {
                    "conditiontype": 10,
                    "operator": 0,
                    "value": "2"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 6,
                "optemplate": [
                    {
                        "templateid": "10091"
                    }
                ]
            }
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-53bdbb4b})

[comment]: # ({new-de84bbc4})
#### Using a custom expression filter

Create a trigger action that will use a custom filter condition. The
action must send a message for each trigger with severity higher or
equal to "Warning" for hosts "10084" and "10106". The formula IDs "A",
"B" and "C" have been chosen arbitrarily.

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Trigger action",
        "eventsource": 0,
        "status": 0,
        "esc_period": "2m",
        "filter": {
            "evaltype": 3,
            "formula": "A and (B or C)",
            "conditions": [
                {
                    "conditiontype": 4,
                    "operator": 5,
                    "value": "2",
                    "formulaid": "A"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10084",
                    "formulaid": "B"
                },
                {
                    "conditiontype": 1,
                    "operator": 0,
                    "value": "10106",
                    "formulaid": "C"
                }
            ]
        },
        "operations": [
            {
                "operationtype": 0,
                "esc_period": "0s",
                "esc_step_from": 1,
                "esc_step_to": 2,
                "evaltype": 0,
                "opmessage_grp": [
                    {
                        "usrgrpid": "7"
                    }
                ],
                "opmessage": {
                    "default_msg": 1,
                    "mediatypeid": "1"
                }
            }
        ],
        "pause_suppressed": "0",
        "notify_if_canceled": "0"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "actionids": [
            "18"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-de84bbc4})
