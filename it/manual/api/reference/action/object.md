[comment]: # translation:outdated

[comment]: # ({new-502c65c3})
# > Action object

The following objects are directly related to the `action` API.

[comment]: # ({/new-502c65c3})


[comment]: # ({new-8757165b})
### Action operation

The action operation object defines an operation that will be performed
when an action is executed. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|**operationtype**<br>(required)|integer|Type of operation.<br><br>Possible values:<br>0 - send message;<br>1 - global script;<br>2 - add host;<br>3 - remove host;<br>4 - add to host group;<br>5 - remove from host group;<br>6 - link to template;<br>7 - unlink from template;<br>8 - enable host;<br>9 - disable host;<br>10 - set host inventory mode.<br><br>Note that only types '0' and '1' are supported for trigger and service actions, only '0' is supported for internal actions. All types are supported for discovery and autoregistration actions.|
|actionid|string|*(readonly)* ID of the action that the operation belongs to.|
|esc\_period|string|Duration of an escalation step in seconds. Must be greater than 60 seconds. Accepts seconds, time unit with suffix and user macro. If set to 0 or 0s, the default action escalation period will be used.<br><br>Default: 0s.<br><br>Note that escalations are supported only for trigger, internal and service actions, and only in normal operations.|
|esc\_step\_from|integer|Step to start escalation from.<br><br>Default: 1.<br><br>Note that escalations are supported only for for trigger, internal and service actions, and only in normal operations.|
|esc\_step\_to|integer|Step to end escalation at.<br><br>Default: 1.<br><br>Note that escalations are supported only for trigger, internal and service actions, and only in normal operations.|
|evaltype|integer|Operation condition evaluation method.<br><br>Possible values:<br>0 - *(default)* AND / OR;<br>1 - AND;<br>2 - OR.|
|opcommand|object|Object containing data on global script run by the operation.<br><br>Each object has one following property: `scriptid` - *(string)* ID of the script.<br><br>Required for global script operations.|
|opcommand\_grp|array|Host groups to run global scripts on.<br><br>Each object has the following properties:<br>`opcommand_grpid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string, readonly)* ID of the operation;<br>`groupid` - *(string)* ID of the host group.<br><br>Required for global script operations if `opcommand_hst` is not set.|
|opcommand\_hst|array|Host to run global scripts on.<br><br>Each object has the following properties:<br>`opcommand_hstid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string, readonly)* ID of the operation;<br>`hostid` - *(string)* ID of the host; if set to 0 the command will be run on the current host.<br><br>Required for global script operations if `opcommand_grp` is not set.|
|opconditions|array|Operation conditions used for trigger actions.<br><br>The operation condition object is [described in detail below](/manual/api/reference/action/object#action_operation_condition).|
|opgroup|array|Host groups to add hosts to.<br><br>Each object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`groupid` - *(string)* ID of the host group.<br><br>Required for "add to host group" and "remove from host group" operations.|
|opmessage|object|Object containing the data about the message sent by the operation.<br><br>The operation message object is [described in detail below](/manual/api/reference/action/object#action_operation_message).<br><br>Required for message operations.|
|opmessage\_grp|array|User groups to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`usrgrpid` - *(string)* ID of the user group.<br><br>Required for message operations if `opmessage_usr` is not set.|
|opmessage\_usr|array|Users to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`userid` - *(string)* ID of the user.<br><br>Required for message operations if `opmessage_grp` is not set.|
|optemplate|array|Templates to link the hosts to to.<br><br>Each object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`templateid` - *(string)* ID of the template.<br><br>Required for "link to template" and "unlink from template" operations.|
|opinventory|object|Inventory mode set host to.<br><br>Object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`inventory_mode` - *(string)* Inventory mode.<br><br>Required for "Set host inventory mode" operations.|

[comment]: # ({/new-8757165b})

[comment]: # ({new-4aeccf9f})
#### Action operation message

The operation message object contains data about the message that will
be sent by the operation.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|default\_msg|integer|Whether to use the default action message text and subject.<br><br>Possible values:<br>0 - use the data from the operation;<br>1 - *(default)* use the data from the media type.|
|mediatypeid|string|ID of the media type that will be used to send the message.|
|message|string|Operation message text.|
|subject|string|Operation message subject.|

[comment]: # ({/new-4aeccf9f})

[comment]: # ({new-6762fd66})
#### Action operation condition

The action operation condition object defines a condition that must be
met to perform the current operation. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|opconditionid|string|*(readonly)* ID of the action operation condition|
|**conditiontype**<br>(required)|integer|Type of condition.<br><br>Possible values:<br>14 - event acknowledged.|
|**value**<br>(required)|string|Value to compare with.|
|operationid|string|*(readonly)* ID of the operation.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - *(default)* =.|

The following operators and values are supported for each operation
condition type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|14|Event acknowledged|=|Whether the event is acknowledged.<br><br>Possible values:<br>0 - not acknowledged;<br>1 - acknowledged.|

[comment]: # ({/new-6762fd66})


[comment]: # ({new-f41f59ea})
### Action update operation

The action update operation object defines an operation that will be
performed when a problem is updated (commented upon, acknowledged,
severity changed, or manually closed). Update operations are possible
for trigger and service actions. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|**operationtype**<br>(required)|integer|Type of operation.<br><br>Possible values for trigger and service actions:<br>0 - send message;<br>1 - global script;<br>12 - notify all involved.|
|opcommand|object|Object containing data on global action type script run by the operation.<br><br>Each object has one following property: `scriptid` - *(string)* ID of the action type script.<br><br>Required for global script operations.|
|opcommand\_grp|array|Host groups to run global scripts on.<br><br>Each object has the following properties:<br>`groupid` - *(string)* ID of the host group.<br><br>Required for global script operations if `opcommand_hst` is not set.|
|opcommand\_hst|array|Host to run global scripts on.<br><br>Each object has the following properties:<br>`hostid` - *(string)* ID of the host; if set to 0 the command will be run on the current host.<br><br>Required for global script operations if `opcommand_grp` is not set.|
|opmessage|object|Object containing the data about the message sent by the update operation.<br><br>The operation message object is [described in detail above](/manual/api/reference/action/object#action_operation_message).|
|opmessage\_grp|array|User groups to send messages to.<br><br>Each object has the following properties:<br>`usrgrpid` - *(string)* ID of the user group.<br><br>Required only for `send message` operations if `opmessage_usr` is not set.<br>Is ignored for `send update message` operations.|
|opmessage\_usr|array|Users to send messages to.<br><br>Each object has the following properties:<br>`userid` - *(string)* ID of the user.<br><br>Required only for `send message` operations if `opmessage_grp` is not set.<br>Is ignored for `send update message` operations.|

[comment]: # ({/new-f41f59ea})

[comment]: # ({new-0d8b3bb1})
### Action filter

The action filter object defines a set of conditions that must be met to
perform the configured action operations. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**conditions**<br>(required)|array|Set of filter conditions to use for filtering results.|
|**evaltype**<br>(required)|integer|Filter condition evaluation method.<br><br>Possible values:<br>0 - and/or;<br>1 - and;<br>2 - or;<br>3 - custom expression.|
|eval\_formula|string|*(readonly)* Generated expression that will be used for evaluating filter conditions. The expression contains IDs that reference specific filter conditions by its `formulaid`. The value of `eval_formula` is equal to the value of `formula` for filters with a custom expression.|
|formula|string|User-defined expression to be used for evaluating conditions of filters with a custom expression. The expression must contain IDs that reference specific filter conditions by its `formulaid`. The IDs used in the expression must exactly match the ones defined in the filter conditions: no condition can remain unused or omitted.<br><br>Required for custom expression filters.|

[comment]: # ({/new-0d8b3bb1})


[comment]: # ({new-73117797})
### Action

The action object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|actionid|string|*(readonly)* ID of the action.|
|**esc\_period**<br>(required)|string|Default operation step duration. Must be at least 60 seconds. Accepts seconds, time unit with suffix and user macro.<br><br>Note that escalations are supported only for trigger, internal and service actions, and only in normal operations.|
|**eventsource**<br>(required)|integer|*(constant)* Type of events that the action will handle.<br><br>Refer to the [event "source" property](/manual/api/reference/event/object#event) for a list of supported event types.|
|**name**<br>(required)|string|Name of the action.|
|status|integer|Whether the action is enabled or disabled.<br><br>Possible values:<br>0 - *(default)* enabled;<br>1 - disabled.|
|pause\_suppressed|integer|Whether to pause escalation during maintenance periods or not.<br><br>Possible values:<br>0 - Don't pause escalation;<br>1 - *(default)* Pause escalation.<br><br>Note that this parameter is valid for trigger actions only.|
|notify\_if\_canceled|integer|Whether to notify when escalation is canceled.<br><br>Possible values:<br>0 - Don't notify when escalation is canceled;<br>1 - *(default)* Notify when escalation is canceled.<br><br>Note that this parameter is valid for trigger actions only.|

[comment]: # ({/new-73117797})

[comment]: # ({new-d1e6762b})
### Action recovery operation

The action recovery operation object defines an operation that will be
performed when a problem is resolved. Recovery operations are possible
for trigger, internal and service actions. It has the following
properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|operationid|string|*(readonly)* ID of the action operation.|
|**operationtype**<br>(required)|integer|Type of operation.<br><br>Possible values for trigger and service actions:<br>0 - send message;<br>1 - global script;<br>11 - notify all involved.<br><br>Possible values for internal actions:<br>0 - send message;<br>11 - notify all involved.|
|actionid|string|*(readonly)* ID of the action that the recovery operation belongs to.|
|opcommand|object|Object containnig data on global action type script run by the operation.<br><br>Each object has one following property: `scriptid` - *(string)* ID of the action type script.<br><br>Required for global script operations.|
|opcommand\_grp|array|Host groups to run global scripts on.<br><br>Each object has the following properties:<br>`opcommand_grpid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string, readonly)* ID of the operation;<br>`groupid` - *(string)* ID of the host group.<br><br>Required for global script operations if `opcommand_hst` is not set.|
|opcommand\_hst|array|Host to run global scripts on.<br><br>Each object has the following properties:<br>`opcommand_hstid` - *(string, readonly)* ID of the object;<br>`operationid` - *(string, readonly)* ID of the operation;<br>`hostid` - *(string)* ID of the host; if set to 0 the command will be run on the current host.<br><br>Required for global script operations if `opcommand_grp` is not set.|
|opmessage|object|Object containing the data about the message sent by the recovery operation.<br><br>The operation message object is [described in detail above](/manual/api/reference/action/object#action_operation_message).<br><br>Required for message operations.|
|opmessage\_grp|array|User groups to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`usrgrpid` - *(string)* ID of the user group.<br><br>Required for message operations if `opmessage_usr` is not set.|
|opmessage\_usr|array|Users to send messages to.<br><br>Each object has the following properties:<br>`operationid` - *(string, readonly)* ID of the operation;<br>`userid` - *(string)* ID of the user.<br><br>Required for message operations if `opmessage_grp` is not set.|

[comment]: # ({/new-d1e6762b})

[comment]: # ({new-dd860074})
#### Action filter condition

The action filter condition object defines a specific condition that
must be checked before running the action operations.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|conditionid|string|*(readonly)* ID of the action condition.|
|**conditiontype**<br>(required)|integer|Type of condition.<br><br>Possible values for trigger actions:<br>0 - host group;<br>1 - host;<br>2 - trigger;<br>3 - trigger name;<br>4 - trigger severity;<br>6 - time period;<br>13 - host template;<br>16 - problem is suppressed;<br>25 - event tag;<br>26 - event tag value.<br><br>Possible values for discovery actions:<br>7 - host IP;<br>8 - discovered service type;<br>9 - discovered service port;<br>10 - discovery status;<br>11 - uptime or downtime duration;<br>12 - received value;<br>18 - discovery rule;<br>19 - discovery check;<br>20 - proxy;<br>21 - discovery object.<br><br>Possible values for autoregistration actions:<br>20 - proxy;<br>22 - host name;<br>24 - host metadata.<br><br>Possible values for internal actions:<br>0 - host group;<br>1 - host;<br>13 - host template;<br>23 - event type;<br>25 - event tag;<br>26 - event tag value.<br><br>Possible values for service actions:<br>25 - event tag;<br>26 - event tag value;<br>27 - service;<br>28 - service name.|
|**value**<br>(required)|string|Value to compare with.|
|value2<br>|string|Secondary value to compare with. Required for trigger, internal and service actions when condition type is *26*.|
|actionid|string|*(readonly)* ID of the action that the condition belongs to.|
|formulaid|string|Arbitrary unique ID that is used to reference the condition from a custom expression. Can only contain capital-case letters. The ID must be defined by the user when modifying filter conditions, but will be generated anew when requesting them afterward.|
|operator|integer|Condition operator.<br><br>Possible values:<br>0 - *(default)* equals;<br>1 - does not equal;<br>2 - contains;<br>3 - does not contain;<br>4 - in;<br>5 - is greater than or equals;<br>6 - is less than or equals;<br>7 - not in;<br>8 - matches;<br>9 - does not match;<br>10 - Yes;<br>11 - No.|

::: notetip
To better understand how to use filters with various
types of expressions, see examples on the
[action.get](get#retrieve_discovery_actions) and
[action.create](create#using_a_custom_expression_filter) method
pages.
:::

The following operators and values are supported for each condition
type.

|Condition|Condition name|Supported operators|Expected value|
|---------|--------------|-------------------|--------------|
|0|Host group|equals,<br>does not equal|Host group ID.|
|1|Host|equals,<br>does not equal|Host ID.|
|2|Trigger|equals,<br>does not equal|Trigger ID.|
|3|Trigger name|contains,<br>does not contain|Trigger name.|
|4|Trigger severity|equals,<br>does not equal,<br>is greater than or equals,<br>is less than or equals|Trigger severity. Refer to the [trigger "severity" property](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.|
|5|Trigger value|equals|Trigger value. Refer to the [trigger "value" property](/manual/api/reference/trigger/object#trigger) for a list of supported trigger values.|
|6|Time period|in, not in|Time when the event was triggered as a [time period](/manual/appendix/time_period).|
|7|Host IP|equals,<br>does not equal|One or several IP ranges to check separated by commas. Refer to the [network discovery configuration](/manual/discovery/network_discovery/rule) section for more information on supported formats of IP ranges.|
|8|Discovered service type|equals,<br>does not equal|Type of discovered service. The type of service matches the type of the discovery check used to detect the service. Refer to the [discovery check "type" property](/manual/api/reference/dcheck/object#discovery_check) for a list of supported types.|
|9|Discovered service port|equals,<br>does not equal|One or several port ranges separated by commas.|
|10|Discovery status|equals|Status of a discovered object.<br><br>Possible values:<br>0 - host or service up;<br>1 - host or service down;<br>2 - host or service discovered;<br>3 - host or service lost.|
|11|Uptime or downtime duration|is greater than or equals,<br>is less than or equals|Time indicating how long has the discovered object been in the current status in seconds.|
|12|Received values|equals,<br>does not equal,<br>is greater than or equals,<br>is less than or equals,<br>contains,<br>does not contain|Value returned when performing a Zabbix agent, SNMPv1, SNMPv2 or SNMPv3 discovery check.|
|13|Host template|equals,<br>does not equal|Linked template ID.|
|16|Problem is suppressed|Yes, No|No value required: using the "Yes" operator means that problem must be suppressed, "No" - not suppressed.|
|18|Discovery rule|equals,<br>does not equal|ID of the discovery rule.|
|19|Discovery check|equals,<br>does not equal|ID of the discovery check.|
|20|Proxy|equals,<br>does not equal|ID of the proxy.|
|21|Discovery object|equals|Type of object that triggered the discovery event.<br><br>Possible values:<br>1 - discovered host;<br>2 - discovered service.|
|22|Host name|contains,<br>does not contain,<br>matches,<br>does not match|Host name.<br>Using a regular expression is supported for operators *matches* and *does not match* in autoregistration conditions.|
|23|Event type|equals|Specific internal event.<br><br>Possible values:<br>0 - item in "not supported" state;<br>1 - item in "normal" state;<br>2 - LLD rule in "not supported" state;<br>3 - LLD rule in "normal" state;<br>4 - trigger in "unknown" state;<br>5 - trigger in "normal" state.|
|24|Host metadata|contains,<br>does not contain,<br>matches,<br>does not match|Metadata of the auto-registered host.<br>Using a regular expression is supported for operators *matches* and *does not match*.|
|25|Tag|equals,<br>does not equal,<br>contains,<br>does not contain|Event tag.|
|26|Tag value|equals,<br>does not equal,<br>contains,<br>does not contain|Event tag value.|
|27|Service|equals,<br>does not equal|Service ID.|
|28|Service name|equals,<br>does not equal|Service name.|

[comment]: # ({/new-dd860074})
