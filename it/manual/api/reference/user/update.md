[comment]: # translation:outdated

[comment]: # ({new-084d9051})
# user.update

[comment]: # ({/new-084d9051})

[comment]: # ({new-dbc24bad})
### Description

`object user.update(object/array users)`

This method allows to update existing users.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

::: noteclassic
The strength of user password is validated according the
password policy rules defined by Authentication API. See [Authentication
API](/manual/api/reference/authentication) for more
information.
:::

[comment]: # ({/new-dbc24bad})

[comment]: # ({new-3b478f18})
### Parameters

`(object/array)` User properties to be updated.

The `userid` property must be defined for each user, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard user properties](object#user), the method
accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|passwd|string|User's password.<br><br>Can be empty string if user belongs to or is moved only to groups that have LDAP access.|
|usrgrps|array|User [groups](/manual/api/reference/user/create) to replace existing user groups.<br><br>The user groups must have the `usrgrpid` property defined.|
|medias|array|[User media](/manual/api/reference/user/object#media) to replace existing media.|

[comment]: # ({/new-3b478f18})

[comment]: # ({new-4c3fb877})
### Return values

`(object)` Returns an object containing the IDs of the updated users
under the `userids` property.

[comment]: # ({/new-4c3fb877})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-c0483e48})
#### Renaming a user

Rename a user to John Doe.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "name": "John",
        "surname": "Doe"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-c0483e48})

[comment]: # ({new-7ae4c491})
#### Changing user role

Change a role of a user.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "12",
        "roleid": "6"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "userids": [
            "12"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7ae4c491})

[comment]: # ({new-1083dc26})
### See also

-   [Authentication](/manual/api/reference/authentication)

[comment]: # ({/new-1083dc26})

[comment]: # ({new-bbd5a07f})
### Source

CUser::update() in *ui/include/classes/api/services/CUser.php*.

[comment]: # ({/new-bbd5a07f})
