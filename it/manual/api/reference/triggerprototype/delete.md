[comment]: # translation:outdated

[comment]: # ({new-c233abea})
# triggerprototype.delete

[comment]: # ({/new-c233abea})

[comment]: # ({new-4ed080b7})
### Description

`object triggerprototype.delete(array triggerPrototypeIds)`

This method allows to delete trigger prototypes.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-4ed080b7})

[comment]: # ({new-93be9503})
### Parameters

`(array)` IDs of the trigger prototypes to delete.

[comment]: # ({/new-93be9503})

[comment]: # ({new-91b09722})
### Return values

`(object)` Returns an object containing the IDs of the deleted trigger
prototypes under the `triggerids` property.

[comment]: # ({/new-91b09722})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-ed4ab6a8})
#### Deleting multiple trigger prototypes

Delete two trigger prototypes.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "triggerprototype.delete",
    "params": [
        "12002",
        "12003"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "triggerids": [
            "12002",
            "12003"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-ed4ab6a8})

[comment]: # ({new-c4ac1758})
### Source

CTriggerPrototype::delete() in
*ui/include/classes/api/services/CTriggerPrototype.php*.

[comment]: # ({/new-c4ac1758})
