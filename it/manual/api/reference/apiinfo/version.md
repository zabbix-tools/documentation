[comment]: # translation:outdated

[comment]: # ({new-34f25b55})
# apiinfo.version

[comment]: # ({/new-34f25b55})

[comment]: # ({new-bc32199a})
### Description

`string apiinfo.version(array)`

This method allows to retrieve the version of the Zabbix API.

::: noteimportant
This method is only available to unauthenticated
users and must be called without the `auth` parameter in the JSON-RPC
request.
:::

[comment]: # ({/new-bc32199a})

[comment]: # ({new-4fa8a419})
### Parameters

`(array)` The method accepts an empty array.

[comment]: # ({/new-4fa8a419})

[comment]: # ({new-53521d11})
### Return values

`(string)` Returns the version of the Zabbix API.

::: notetip
Starting from Zabbix 2.0.4 the version of the API
matches the version of Zabbix.
:::

[comment]: # ({/new-53521d11})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-6ba4bdd0})
#### Retrieving the version of the API

Retrieve the version of the Zabbix API.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "apiinfo.version",
    "params": [],
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": "4.0.0",
    "id": 1
}
```

[comment]: # ({/new-6ba4bdd0})

[comment]: # ({new-25499b5e})
### Source

CAPIInfo::version() in *ui/include/classes/api/services/CAPIInfo.php*.

[comment]: # ({/new-25499b5e})
