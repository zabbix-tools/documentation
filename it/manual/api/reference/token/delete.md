[comment]: # translation:outdated

[comment]: # ({new-c63958e0})
# token.delete

[comment]: # ({/new-c63958e0})

[comment]: # ({new-e5872416})
### Description

`object token.delete(array tokenids)`

This method allows to delete tokens.

::: noteclassic
Only *Super admin* user type is allowed to manage tokens for
other users.
:::

[comment]: # ({/new-e5872416})

[comment]: # ({new-92c0da91})
### Parameters

`(array)` IDs of the tokens to delete.

[comment]: # ({/new-92c0da91})

[comment]: # ({new-fd9b032e})
### Return values

`(object)` Returns an object containing the IDs of the deleted tokens
under the `tokenids` property.

[comment]: # ({/new-fd9b032e})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-7f001ef1})
#### Delete multiple tokens

Delete two tokens.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "token.delete",
    "params": [
        "188",
        "192"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "tokenids": [
            "188",
            "192"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-7f001ef1})

[comment]: # ({new-9ebff2e2})
### Source

CToken::delete() in *ui/include/classes/api/services/CToken.php*.

[comment]: # ({/new-9ebff2e2})
