[comment]: # translation:outdated

[comment]: # ({new-8c711e69})
# maintenance.update

[comment]: # ({/new-8c711e69})

[comment]: # ({new-292dcffa})
### Description

`object maintenance.update(object/array maintenances)`

This method allows to update existing maintenances.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-292dcffa})

[comment]: # ({new-678c22b3})
### Parameters

`(object/array)` Maintenance properties to be updated.

The `maintenanceid` property must be defined for each maintenance, all
other properties are optional. Only the passed properties will be
updated, all others will remain unchanged.

Additionally to the [standard maintenance
properties](object#maintenance), the method accepts the following
parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|groups|object/array|Host [groups](/manual/api/reference/hostgroup/object) to replace the current groups.<br><br>The host groups must have the `groupid` property defined.|
|hosts|object/array|[Hosts](/manual/api/reference/host/object) to replace the current hosts.<br><br>The hosts must have the `hostid` property defined.|
|timeperiods|object/array|Maintenance [time periods](/manual/api/reference/maintenance/object#time_period) to replace the current periods.|
|tags|object/array|[Problem tags](/manual/api/reference/maintenance/object#problem_tag) to replace the current tags.|

::: noteimportant
At least one host or host group must be defined
for each maintenance.
:::

[comment]: # ({/new-678c22b3})

[comment]: # ({new-b9ffa219})
### Return values

`(object)` Returns an object containing the IDs of the updated
maintenances under the `maintenanceids` property.

[comment]: # ({/new-b9ffa219})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-25215f8d})
#### Assigning different hosts

Replace the hosts currently assigned to maintenance with two different ones.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.update",
    "params": {
        "maintenanceid": "3",
        "hosts": [
            {"hostid": "10085"},
            {"hostid": "10084"}
        ]
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-25215f8d})

[comment]: # ({new-82b3ef0e})
### See also

-   [Time period](object#time_period)

[comment]: # ({/new-82b3ef0e})

[comment]: # ({new-63ad1661})
### Source

CMaintenance::update() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-63ad1661})
