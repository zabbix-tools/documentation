[comment]: # translation:outdated

[comment]: # ({new-0aafdb81})
# maintenance.delete

[comment]: # ({/new-0aafdb81})

[comment]: # ({new-5afbd3a3})
### Description

`object maintenance.delete(array maintenanceIds)`

This method allows to delete maintenances.

::: noteclassic
This method is only available to *Admin* and *Super admin*
user types. Permissions to call the method can be revoked in user role
settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-5afbd3a3})

[comment]: # ({new-9828d580})
### Parameters

`(array)` IDs of the maintenances to delete.

[comment]: # ({/new-9828d580})

[comment]: # ({new-1bb5e665})
### Return values

`(object)` Returns an object containing the IDs of the deleted
maintenances under the `maintenanceids` property.

[comment]: # ({/new-1bb5e665})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-9baa4c26})
#### Deleting multiple maintenances

Delete two maintenanaces.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "maintenance.delete",
    "params": [
        "3",
        "1"
    ],
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "maintenanceids": [
            "3",
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-9baa4c26})

[comment]: # ({new-87f95129})
### Source

CMaintenance::delete() in
*ui/include/classes/api/services/CMaintenance.php*.

[comment]: # ({/new-87f95129})
