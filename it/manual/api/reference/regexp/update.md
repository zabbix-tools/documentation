[comment]: # translation:outdated

[comment]: # ({new-ffe55246})
# regexp.update

[comment]: # ({/new-ffe55246})

[comment]: # ({new-596faacb})
### Description

`object regexp.update(object/array regularExpressions)`

This method allows to update existing global regular expressions.

::: noteclassic
This method is only available to *Super admin* user types.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-596faacb})

[comment]: # ({new-f26d83f8})
### Parameters

`(object/array)` Regular expression properties to be updated.

The `regexpid` property must be defined for each object, all other
properties are optional. Only the passed properties will be updated, all
others will remain unchanged.

Additionally to the [standard properties](object#expressions_object),
the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|expressions|array|[Expressions](/manual/api/reference/regexp/object#expressions_object) options.|

[comment]: # ({/new-f26d83f8})

[comment]: # ({new-7a7252f7})
### Return values

`(object)` Returns an object containing the IDs of the updated regular
expressions under the `regexpids` property.

[comment]: # ({/new-7a7252f7})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-aef504e5})
#### Updating global regular expression for file systems discovery.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "regexp.update",
    "params": {
      "regexpid": "1",
      "name": "File systems for discovery",
      "test_string": "",
      "expressions": [
        {
          "expression": "^(btrfs|ext2|ext3|ext4|reiser|xfs|ffs|ufs|jfs|jfs2|vxfs|hfs|apfs|refs|zfs)$",
          "expression_type": "3",
          "exp_delimiter": ",",
          "case_sensitive": "0"
        },
        {
          "expression": "^(ntfs|fat32|fat16)$",
          "expression_type": "3",
          "exp_delimiter": ",",
          "case_sensitive": "0"
        }
      ]
    },
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "regexpids": [
            "1"
        ]
    },
    "id": 1
}
```

[comment]: # ({/new-aef504e5})

[comment]: # ({new-90c4873e})
### Source

CRegexp::update() in *ui/include/classes/api/services/CRegexp.php*.

[comment]: # ({/new-90c4873e})
