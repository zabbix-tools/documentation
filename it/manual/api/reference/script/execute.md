[comment]: # translation:outdated

[comment]: # ({new-3a2a8d58})
# script.execute

[comment]: # ({/new-3a2a8d58})

[comment]: # ({new-60a9dbe3})
### Description

`object script.execute(object parameters)`

This method allows to run a script on a host or event.

::: noteclassic
This method is available to users of any type. Permissions
to call the method can be revoked in user role settings. See [User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-60a9dbe3})

[comment]: # ({new-e4fa8ac7})
### Parameters

`(object)` Parameters containing the ID of the script to run and either
the ID of the host or the ID of the event.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**scriptid**<br>(required)|string|ID of the script to run.|
|hostid|string|ID of the host to run the script on.|
|eventid|string|ID of the event to run the script on.|

[comment]: # ({/new-e4fa8ac7})

[comment]: # ({new-dde54e60})
### Return values

`(object)` Returns the result of script execution.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|response|string|Whether the script was run successfully.<br><br>Possible values: `success` or `failed`.|
|value|string|Script output.|
|debug|object|Contains a [debug object](/manual/api/reference/script/object#Debug) if a webhook script is executed. For other script types, it contains empty object.|

[comment]: # ({/new-dde54e60})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-03cb1cf3})
#### Run a webhook script

Run a webhook script that sends HTTP request to external service.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.execute",
    "params": {
        "scriptid": "4",
        "hostid": "30079"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "response": "success",
        "value": "{\"status\":\"sent\",\"timestamp\":\"1611235391\"}",
        "debug": {
            "logs": [
                 {
                     "level": 3,
                     "ms": 480,
                     "message": "[Webhook Script] HTTP status: 200."
                 }
             ],
             "ms": 495
        }
    },
    "id": 1
}
```

[comment]: # ({/new-03cb1cf3})

[comment]: # ({new-baebff97})
#### Run a custom script

Run a "ping" script on a host.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "script.execute",
    "params": {
        "scriptid": "1",
        "hostid": "30079"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "response": "success",
        "value": "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n64 bytes from 127.0.0.1: icmp_req=1 ttl=64 time=0.074 ms\n64 bytes from 127.0.0.1: icmp_req=2 ttl=64 time=0.030 ms\n64 bytes from 127.0.0.1: icmp_req=3 ttl=64 time=0.030 ms\n\n--- 127.0.0.1 ping statistics ---\n3 packets transmitted, 3 received, 0% packet loss, time 1998ms\nrtt min/avg/max/mdev = 0.030/0.044/0.074/0.022 ms\n",
        "debug": []
    },
    "id": 1
}
```

[comment]: # ({/new-baebff97})

[comment]: # ({new-e7dc45a5})
### Source

CScript::execute() in *ui/include/classes/api/services/CScript.php*.

[comment]: # ({/new-e7dc45a5})
