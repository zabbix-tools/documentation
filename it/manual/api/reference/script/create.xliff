<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/script/create.md">
    <body>
      <trans-unit id="8961fc33" xml:space="preserve">
        <source># script.create</source>
      </trans-unit>
      <trans-unit id="d7a51ffa" xml:space="preserve">
        <source>### Description

`object script.create(object/array scripts)`

This method allows to create new scripts.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/users/user_roles)
for more information.
:::</source>
      </trans-unit>
      <trans-unit id="bcb9a453" xml:space="preserve">
        <source>### Parameters

`(object/array)` Scripts to create.

The method accepts scripts with the [standard script
properties](/manual/api/reference/script/object#script).</source>
      </trans-unit>
      <trans-unit id="15216b8d" xml:space="preserve">
        <source>### Return values

`(object)` Returns an object containing the IDs of the created scripts
under the `scriptids` property. The order of the returned IDs matches
the order of the passed scripts.</source>
      </trans-unit>
      <trans-unit id="b41637d2" xml:space="preserve">
        <source>### Examples</source>
      </trans-unit>
      <trans-unit id="55d911d7" xml:space="preserve">
        <source>#### Create a webhook script

Create a webhook script that sends HTTP request to external service.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "Webhook script",
        "command": "try {\n var request = new HttpRequest(),\n response,\n data;\n\n request.addHeader('Content-Type: application/json');\n\n response = request.post('https://localhost/post', value);\n\n try {\n response = JSON.parse(response);\n }\n catch (error) {\n response = null;\n }\n\n if (request.getStatus() !== 200 || !('data' in response)) {\n throw 'Unexpected response.';\n }\n\n data = JSON.stringify(response.data);\n\n Zabbix.log(3, '[Webhook Script] response data: ' + data);\n\n return data;\n}\ncatch (error) {\n Zabbix.log(3, '[Webhook Script] script execution failed: ' + error);\n throw 'Execution failed: ' + error + '.';\n}",
        "type": 5,
        "timeout": "40s",
        "parameters": [
            {
                "name": "token",
                "value": "{$WEBHOOK.TOKEN}"
            },
            {
                "name": "host",
                "value": "{HOST.HOST}"
            },
            {
                "name": "v",
                "value": "2.2"
            }
        ]
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "3"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="16a09744" xml:space="preserve">
        <source>#### Create an SSH script

Create an SSH script with public key authentication that can be executed
on a host and has a context menu.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "SSH script",
        "command": "my script command",
        "type": 2,
        "username": "John",
        "publickey": "pub.key",
        "privatekey": "priv.key",
        "password": "secret",
        "port": "12345",
        "scope": 2,
        "menu_path": "All scripts/SSH",
        "usrgrpid": "7",
        "groupid": "4"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "5"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="0e2eb663" xml:space="preserve">
        <source>#### Create a custom script

Create a custom script that will reboot a server. The script will
require write access to the host and will display a configuration
message before running in the frontend.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "Reboot server",
        "command": "reboot server 1",
        "confirmation": "Are you sure you would like to reboot the server?",
        "scope": 2,
        "type": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "4"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="c363dc9b" xml:space="preserve">
        <source>#### Create a URL type script

Create a URL type script that for host scope and remains in same window and has confirmation text.

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "script.create",
    "params": {
        "name": "URL script",
        "type": 6,
        "scope": 2,
        "url": "http://zabbix/ui/zabbix.php?action=host.edit&amp;hostid={HOST.ID}",
        "confirmation": "Edit host {HOST.NAME}?",
        "new_window": 0
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "scriptids": [
            "56"
        ]
    },
    "id": 1
}
```</source>
      </trans-unit>
      <trans-unit id="8c09f129" xml:space="preserve">
        <source>### Source

CScript::create() in *ui/include/classes/api/services/CScript.php*.</source>
      </trans-unit>
    </body>
  </file>
</xliff>
