[comment]: # translation:outdated

[comment]: # ({new-28092341})
# > User macro object

The following objects are directly related to the `usermacro` API.

[comment]: # ({/new-28092341})

[comment]: # ({new-e654cd12})
### Global macro

The global macro object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|globalmacroid|string|*(readonly)* ID of the global macro.|
|**macro**<br>(required)|string|Macro string.|
|**value**<br>(required)|string|Value of the macro.|
|type|integer|Type of macro.<br><br>Possible values:<br>0 - *(default)* Text macro;<br>1 - Secret macro;<br>2 - Vault secret.|
|description|string|Description of the macro.|

[comment]: # ({/new-e654cd12})

[comment]: # ({new-5a3a9f9f})
### Host macro

The host macro object defines a macro available on a host, host
prototype or template. It has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|hostmacroid|string|*(readonly)* ID of the host macro.|
|**hostid**<br>(required)|string|ID of the host that the macro belongs to.|
|**macro**<br>(required)|string|Macro string.|
|**value**<br>(required)|string|Value of the macro.|
|type|integer|Type of macro.<br><br>Possible values:<br>0 - *(default)* Text macro;<br>1 - Secret macro;<br>2 - Vault secret.|
|description|string|Description of the macro.|

[comment]: # ({/new-5a3a9f9f})
