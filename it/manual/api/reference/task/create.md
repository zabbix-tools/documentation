[comment]: # translation:outdated

[comment]: # ({new-c9bc126a})
# task.create

[comment]: # ({/new-c9bc126a})

[comment]: # ({new-0e3179d3})
### Description

`object task.create(object/array tasks)`

This method allows to create a new task (such as collect diagnostic data
or check items or low-level discovery rules without config reload).

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-0e3179d3})

[comment]: # ({new-2574c234})
### Parameters

`(object/array)` A task to create.

The method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|**type**<br>(required)|integer|Task type.<br><br>Possible values:<br>1 - Diagnostic information;<br>6 - Check now.|
|**request**<br>(required)|object|Task request object according to the task type. Correct format of request object is described in [Task object](/manual/api/reference/task/object) section.|
|proxy\_hostid|integer|Proxy about which Diagnostic information task will collect data.<br><br>Ignored for 'Check now' tasks.|

*Note* that 'Check now' tasks can be created only for the following
types of items/discovery rules:

-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent

[comment]: # ({/new-2574c234})

[comment]: # ({new-918de761})
If item or discovery ruls is of type *Dependent item*, then top level master item must be of type:
-   Zabbix agent
-   SNMPv1/v2/v3 agent
-   Simple check
-   Internal check
-   External check
-   Database monitor
-   HTTP agent
-   IPMI agent
-   SSH agent
-   TELNET agent
-   Calculated check
-   JMX agent

[comment]: # ({/new-918de761})

[comment]: # ({new-ab87ce2a})
### Return values

`(object)` Returns an object containing the IDs of the created tasks
under the `taskids` property. One task is created for each item and
low-level discovery rule. The order of the returned IDs matches the
order of the passed `itemids`.

[comment]: # ({/new-ab87ce2a})

[comment]: # ({new-b41637d2})
### Examples

[comment]: # ({/new-b41637d2})

[comment]: # ({new-5c5fa6f1})
#### Creating a task

Create a task `check now` for two items. One is an item, the other is a
low-level discovery rule.

Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": [
        {
            "type": "6",
            "request": {
                "itemid": "10092"
            }
        },
        {
            "type": "6",
            "request": {
                "itemid": "10093"
            }
        }
    ],
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "1",
            "2"
        ]
    },
    "id": 1
}
```

Create a task `diagnostic information` task. Request:

``` {.java}
{
    "jsonrpc": "2.0",
    "method": "task.create",
    "params": [
        {
            "type": "1",
            "request": {
                "alerting": {
                    "stats": [
                        "alerts"
                    ],
                    "top": {
                        "media.alerts": 10
                    }
                },
                "lld": {
                    "stats": "extend",
                    "top": {
                        "values": 5
                    }
                }
            },
            "proxy_hostid": 0
        }
    ],
    "auth": "700ca65537074ec963db7efabda78259",
    "id": 2
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "taskids": [
            "3"
        ]
    },
    "id": 2
}
```

[comment]: # ({/new-5c5fa6f1})

[comment]: # ({new-d87c906b})
### See also

-   [Task](/manual/api/reference/task/object)
-   ['Check now' request object](/manual/api/reference/task/object)
-   ['Diagnostic information' request
    object](/manual/api/reference/task/object)
-   [Statistic request object](/manual/api/reference/task/object)

[comment]: # ({/new-d87c906b})

[comment]: # ({new-be350bd3})
### Source

CTask::create() in *ui/include/classes/api/services/CTask.php*.

[comment]: # ({/new-be350bd3})
