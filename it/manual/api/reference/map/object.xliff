<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/map/object.md">
    <body>
      <trans-unit id="cdacabd4" xml:space="preserve">
        <source># &gt; Map object

The following objects are directly related to the `map` API.</source>
      </trans-unit>
      <trans-unit id="62f73225" xml:space="preserve">
        <source>### Map

The map object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapid|string|ID of the map.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|height|integer|Height of the map in pixels.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|name|string|Name of the map.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|width|integer|Width of the map in pixels.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|backgroundid|string|ID of the image used as the background for the map.|
|expand\_macros|integer|Whether to expand macros in labels when configuring the map.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* do not expand macros;&lt;br&gt;1 - expand macros.|
|expandproblem|integer|Whether the problem trigger will be displayed for elements with a single problem.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - always display the number of problems;&lt;br&gt;1 - *(default)* display the problem trigger if there's only one problem.|
|grid\_align|integer|Whether to enable grid aligning.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - disable grid aligning;&lt;br&gt;1 - *(default)* enable grid aligning.|
|grid\_show|integer|Whether to show the grid on the map.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - do not show the grid;&lt;br&gt;1 - *(default)* show the grid.|
|grid\_size|integer|Size of the map grid in pixels.&lt;br&gt;&lt;br&gt;Supported values: 20, 40, 50, 75 and 100.&lt;br&gt;&lt;br&gt;Default: 50.|
|highlight|integer|Whether icon highlighting is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - highlighting disabled;&lt;br&gt;1 - *(default)* highlighting enabled.|
|iconmapid|string|ID of the icon map used on the map.|
|label\_format|integer|Whether to enable advanced labels.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* disable advanced labels;&lt;br&gt;1 - enable advanced labels.|
|label\_location|integer|Location of the map element label.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* bottom;&lt;br&gt;1 - left;&lt;br&gt;2 - right;&lt;br&gt;3 - top.|
|label\_string\_host|string|Custom label for host elements.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `label_type_host` is set to "custom"|
|label\_string\_hostgroup|string|Custom label for host group elements.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `label_type_hostgroup` is set to "custom"|
|label\_string\_image|string|Custom label for image elements.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `label_type_image` is set to "custom"|
|label\_string\_map|string|Custom label for map elements.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `label_type_map` is set to "custom"|
|label\_string\_trigger|string|Custom label for trigger elements.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `label_type_trigger` is set to "custom"|
|label\_type|integer|Map element label type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - label;&lt;br&gt;1 - IP address;&lt;br&gt;2 - *(default)* element name;&lt;br&gt;3 - status only;&lt;br&gt;4 - nothing.|
|label\_type\_host|integer|Label type for host elements.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - label;&lt;br&gt;1 - IP address;&lt;br&gt;2 - *(default)* element name;&lt;br&gt;3 - status only;&lt;br&gt;4 - nothing;&lt;br&gt;5 - custom.|
|label\_type\_hostgroup|integer|Label type for host group elements.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - label;&lt;br&gt;2 - *(default)* element name;&lt;br&gt;3 - status only;&lt;br&gt;4 - nothing;&lt;br&gt;5 - custom.|
|label\_type\_image|integer|Label type for host group elements.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - label;&lt;br&gt;2 - *(default)* element name;&lt;br&gt;4 - nothing;&lt;br&gt;5 - custom.|
|label\_type\_map|integer|Label type for map elements.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - label;&lt;br&gt;2 - *(default)* element name;&lt;br&gt;3 - status only;&lt;br&gt;4 - nothing;&lt;br&gt;5 - custom.|
|label\_type\_trigger|integer|Label type for trigger elements.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - label;&lt;br&gt;2 - *(default)* element name;&lt;br&gt;3 - status only;&lt;br&gt;4 - nothing;&lt;br&gt;5 - custom.|
|markelements|integer|Whether to highlight map elements that have recently changed their status.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* do not highlight elements;&lt;br&gt;1 - highlight elements.|
|severity\_min|integer|Minimum severity of the triggers that will be displayed on the map.&lt;br&gt;&lt;br&gt;Refer to the [trigger `severity` property](/manual/api/reference/trigger/object#trigger) for a list of supported trigger severities.|
|show\_unack|integer|How problems should be displayed.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* display the count of all problems;&lt;br&gt;1 - display only the count of unacknowledged problems;&lt;br&gt;2 - display the count of acknowledged and unacknowledged problems separately.|
|userid|string|Map owner user ID.|
|private|integer|Type of map sharing.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - public map;&lt;br&gt;1 - *(default)* private map.|
|show\_suppressed|integer|Whether suppressed problems are shown.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* hide suppressed problems;&lt;br&gt;1 - show suppressed problems.|</source>
      </trans-unit>
      <trans-unit id="c7b92dfc" xml:space="preserve">
        <source>### Map element

The map element object defines an object displayed on a map. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|selementid|string|ID of the map element.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|elements|array|Element data object.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `elementtype` is set to "host", "map", "trigger" or "host group"|
|elementtype|integer|Type of map element.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - host;&lt;br&gt;1 - map;&lt;br&gt;2 - trigger;&lt;br&gt;3 - host group;&lt;br&gt;4 - image.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|iconid\_off|string|ID of the image used to display the element in default state.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|areatype|integer|How separate host group hosts should be displayed.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* the host group element will take up the whole map;&lt;br&gt;1 - the host group element will have a fixed size.|
|elementsubtype|integer|How a host group element should be displayed on a map.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* display the host group as a single element;&lt;br&gt;1 - display each host in the group separately.|
|evaltype|integer|Map element tag filtering condition evaluation method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* AND / OR;&lt;br&gt;2 - OR.|
|height|integer|Height of the fixed size host group element in pixels.&lt;br&gt;&lt;br&gt;Default: 200.|
|iconid\_disabled|string|ID of the image used to display disabled map elements.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `elementtype` is set to "host", "map", "trigger", or "host group"|
|iconid\_maintenance|string|ID of the image used to display map elements in maintenance.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `elementtype` is set to "host", "map", "trigger", or "host group"|
|iconid\_on|string|ID of the image used to display map elements with problems.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `elementtype` is set to "host", "map", "trigger", or "host group"|
|label|string|Label of the element.|
|label\_location|integer|Location of the map element label.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;-1 - *(default)* default location;&lt;br&gt;0 - bottom;&lt;br&gt;1 - left;&lt;br&gt;2 - right;&lt;br&gt;3 - top.|
|permission|integer|Type of permission level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;-1 - none;&lt;br&gt;2 - read only;&lt;br&gt;3 - read-write.|
|sysmapid|string|ID of the map that the element belongs to.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|urls|array|Map element URLs.&lt;br&gt;&lt;br&gt;The map element URL object is [described in detail below](object#map_element_url).|
|use\_iconmap|integer|Whether icon mapping must be used for host elements.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - do not use icon mapping;&lt;br&gt;1 - *(default)* use icon mapping.|
|viewtype|integer|Host group element placing algorithm.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* grid.|
|width|integer|Width of the fixed size host group element in pixels.&lt;br&gt;&lt;br&gt;Default: 200.|
|x|integer|X-coordinates of the element in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|y|integer|Y-coordinates of the element in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|</source>
      </trans-unit>
      <trans-unit id="305dd3e0" xml:space="preserve">
        <source>#### Map element Host

The map element Host object defines one host element.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|hostid|string|Host ID|</source>
      </trans-unit>
      <trans-unit id="e1118d67" xml:space="preserve">
        <source>#### Map element Host group

The map element Host group object defines one host group element.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|groupid|string|Host group ID|</source>
      </trans-unit>
      <trans-unit id="2d745dcc" xml:space="preserve">
        <source>#### Map element Map

The map element Map object defines one map element.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapid|string|Map ID|</source>
      </trans-unit>
      <trans-unit id="0283fd46" xml:space="preserve">
        <source>#### Map element Trigger

The map element Trigger object defines one or more trigger elements.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|triggerid|string|Trigger ID|</source>
      </trans-unit>
      <trans-unit id="a7495119" xml:space="preserve">
        <source>#### Map element tag

The map element tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Map element tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|operator|string|Map element tag condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Contains;&lt;br&gt;1 - Equals;&lt;br&gt;2 - Does not contain;&lt;br&gt;3 - Does not equal;&lt;br&gt;4 - Exists;&lt;br&gt;5 - Does not exist.|
|value|string|Map element tag value.|</source>
      </trans-unit>
      <trans-unit id="5a112b10" xml:space="preserve">
        <source>#### Map element URL

The map element URL object defines a clickable link that will be
available for a specific map element. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapelementurlid|string|ID of the map element URL.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|name|string|Link caption.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|url|string|Link URL.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|selementid|string|ID of the map element that the URL belongs to.|</source>
      </trans-unit>
      <trans-unit id="1e44fd2a" xml:space="preserve">
        <source>### Map link

The map link object defines a link between two map elements. It has the
following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|linkid|string|ID of the map link.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|selementid1|string|ID of the first map element linked on one end.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|selementid2|string|ID of the first map element linked on the other end.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|color|string|Line color as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: `000000`.|
|drawtype|integer|Link line draw style.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* line;&lt;br&gt;2 - bold line;&lt;br&gt;3 - dotted line;&lt;br&gt;4 - dashed line.|
|label|string|Link label.|
|linktriggers|array|Map link triggers to use as link status indicators.&lt;br&gt;&lt;br&gt;The map link trigger object is [described in detail below](object#map_link_trigger).|
|permission|integer|Type of permission level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;-1 - none;&lt;br&gt;2 - read only;&lt;br&gt;3 - read-write.|
|sysmapid|string|ID of the map the link belongs to.|</source>
      </trans-unit>
      <trans-unit id="219c2bce" xml:space="preserve">
        <source>#### Map link trigger

The map link trigger object defines a map link status indicator based on
the state of a trigger. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|linktriggerid|string|ID of the map link trigger.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|triggerid|string|ID of the trigger used as a link indicator.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|color|string|Indicator color as a hexadecimal color code.&lt;br&gt;&lt;br&gt;Default: `DD0000`.|
|drawtype|integer|Indicator draw style.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* line;&lt;br&gt;2 - bold line;&lt;br&gt;3 - dotted line;&lt;br&gt;4 - dashed line.|
|linkid|string|ID of the map link that the link trigger belongs to.|</source>
      </trans-unit>
      <trans-unit id="96c2bee0" xml:space="preserve">
        <source>### Map URL

The map URL object defines a clickable link that will be available for
all elements of a specific type on the map. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapurlid|string|ID of the map URL.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|name|string|Link caption.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|url|string|Link URL.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|elementtype|integer|Type of map element for which the URL will be available.&lt;br&gt;&lt;br&gt;Refer to the [map element `type` property](object#map_element) for a list of supported types.&lt;br&gt;&lt;br&gt;Default: 0.|
|sysmapid|string|ID of the map that the URL belongs to.|</source>
      </trans-unit>
      <trans-unit id="942f03ad" xml:space="preserve">
        <source>### Map user

List of map permissions based on users. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapuserid|string|ID of the map user.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|userid|string|User ID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|permission|integer|Type of permission level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;2 - read only;&lt;br&gt;3 - read-write.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="f6e63a8f" xml:space="preserve">
        <source>### Map user group

List of map permissions based on user groups. It has the following
properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmapusrgrpid|string|ID of the map user group.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|usrgrpid|string|User group ID.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|permission|integer|Type of permission level.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;2 - read only;&lt;br&gt;3 - read-write.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|</source>
      </trans-unit>
      <trans-unit id="9d0acde7" xml:space="preserve">
        <source>### Map shapes

The map shape object defines a geometric shape (with or without text)
displayed on a map. It has the following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmap\_shapeid|string|ID of the map shape element.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|type|integer|Type of map shape element.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - rectangle;&lt;br&gt;1 - ellipse.&lt;br&gt;&lt;br&gt;Property is required when new shapes are created.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|x|integer|X-coordinates of the shape in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|y|integer|Y-coordinates of the shape in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|width|integer|Width of the shape in pixels.&lt;br&gt;&lt;br&gt;Default: 200.|
|height|integer|Height of the shape in pixels.&lt;br&gt;&lt;br&gt;Default: 200.|
|text|string|Text of the shape.|
|font|integer|Font of the text within shape.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Georgia, serif&lt;br&gt;1 - “Palatino Linotype”, “Book Antiqua”, Palatino, serif&lt;br&gt;2 - “Times New Roman”, Times, serif&lt;br&gt;3 - Arial, Helvetica, sans-serif&lt;br&gt;4 - “Arial Black”, Gadget, sans-serif&lt;br&gt;5 - “Comic Sans MS”, cursive, sans-serif&lt;br&gt;6 - Impact, Charcoal, sans-serif&lt;br&gt;7 - “Lucida Sans Unicode”, “Lucida Grande”, sans-serif&lt;br&gt;8 - Tahoma, Geneva, sans-serif&lt;br&gt;9 - “Trebuchet MS”, Helvetica, sans-serif&lt;br&gt;10 - Verdana, Geneva, sans-serif&lt;br&gt;11 - “Courier New”, Courier, monospace&lt;br&gt;12 - “Lucida Console”, Monaco, monospace&lt;br&gt;&lt;br&gt;Default: 9.|
|font\_size|integer|Font size in pixels.&lt;br&gt;&lt;br&gt;Default: 11.|
|font\_color|string|Font color.&lt;br&gt;&lt;br&gt;Default: `000000`.|
|text\_halign|integer|Horizontal alignment of text.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - center;&lt;br&gt;1 - left;&lt;br&gt;2 - right.&lt;br&gt;&lt;br&gt;Default: 0.|
|text\_valign|integer|Vertical alignment of text.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - middle;&lt;br&gt;1 - top;&lt;br&gt;2 - bottom.&lt;br&gt;&lt;br&gt;Default: 0.|
|border\_type|integer|Type of the border.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - none;&lt;br&gt;1 - `—————`;&lt;br&gt;2 - `·····`;&lt;br&gt;3 - `- - -`.&lt;br&gt;&lt;br&gt;Default: 0.|
|border\_width|integer|Width of the border in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|border\_color|string|Border color.&lt;br&gt;&lt;br&gt;Default: `000000`.|
|background\_color|string|Background color (fill color).&lt;br&gt;&lt;br&gt;Default: `(empty)`.|
|zindex|integer|Value used to order all shapes and lines (z-index).&lt;br&gt;&lt;br&gt;Default: 0.|</source>
      </trans-unit>
      <trans-unit id="b1536eee" xml:space="preserve">
        <source>### Map lines

The map line object defines a line displayed on a map. It has the
following properties:

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|sysmap\_shapeid|string|ID of the map shape element.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*|
|x1|integer|X-coordinates of the line point 1 in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|y1|integer|Y-coordinates of the line point 1 in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|x2|integer|X-coordinates of the line point 2 in pixels.&lt;br&gt;&lt;br&gt;Default: 200.|
|y2|integer|Y-coordinates of the line point 2 in pixels.&lt;br&gt;&lt;br&gt;Default: 200.|
|line\_type|integer|Type of the lines.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - none;&lt;br&gt;1 - `—————`;&lt;br&gt;2 - `·····`;&lt;br&gt;3 - `- - -`.&lt;br&gt;&lt;br&gt;Default: 0.|
|line\_width|integer|Width of the lines in pixels.&lt;br&gt;&lt;br&gt;Default: 0.|
|line\_color|string|Line color.&lt;br&gt;&lt;br&gt;Default: `000000`.|
|zindex|integer|Value used to order all shapes and lines (z-index).&lt;br&gt;&lt;br&gt;Default: 0.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
