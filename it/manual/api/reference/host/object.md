[comment]: # translation:outdated

[comment]: # ({new-b7ea8d04})
# > Host object

The following objects are directly related to the host API.

[comment]: # ({/new-b7ea8d04})

[comment]: # ({new-2f6c5c76})
### Host

The host object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|hostid|string|*(readonly)* ID of the host.|
|**host**<br>(required)|string|Technical name of the host.|
|description|text|Description of the host.|
|flags|integer|*(readonly)* Origin of the host.<br><br>Possible values:<br>0 - a plain host;<br>4 - a discovered host.|
|inventory\_mode|integer|Host inventory population mode.<br><br>Possible values are:<br>-1 - *(default)* disabled;<br>0 - manual;<br>1 - automatic.|
|ipmi\_authtype|integer|IPMI authentication algorithm.<br><br>Possible values are:<br>-1 - *(default)* default;<br>0 - none;<br>1 - MD2;<br>2 - MD5<br>4 - straight;<br>5 - OEM;<br>6 - RMCP+.|
|ipmi\_password|string|IPMI password.|
|ipmi\_privilege|integer|IPMI privilege level.<br><br>Possible values are:<br>1 - callback;<br>2 - *(default)* user;<br>3 - operator;<br>4 - admin;<br>5 - OEM.|
|ipmi\_username|string|IPMI username.|
|maintenance\_from|timestamp|*(readonly)* Starting time of the effective maintenance.|
|maintenance\_status|integer|*(readonly)* Effective maintenance status.<br><br>Possible values are:<br>0 - *(default)* no maintenance;<br>1 - maintenance in effect.|
|maintenance\_type|integer|*(readonly)* Effective maintenance type.<br><br>Possible values are:<br>0 - *(default)* maintenance with data collection;<br>1 - maintenance without data collection.|
|maintenanceid|string|*(readonly)* ID of the maintenance that is currently in effect on the host.|
|name|string|Visible name of the host.<br><br>Default: `host` property value.|
|proxy\_hostid|string|ID of the proxy that is used to monitor the host.|
|status|integer|Status and function of the host.<br><br>Possible values are:<br>0 - *(default)* monitored host;<br>1 - unmonitored host.|
|tls\_connect|integer|Connections to host.<br><br>Possible values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_accept|integer|Connections from host.<br><br>Possible bitmap values are:<br>1 - *(default)* No encryption;<br>2 - PSK;<br>4 - certificate.|
|tls\_issuer|string|Certificate issuer.|
|tls\_subject|string|Certificate subject.|
|tls\_psk\_identity|string|*(write-only)* PSK identity. Required if either `tls_connect` or `tls_accept` has PSK enabled.<br>Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.|
|tls\_psk|string|*(write-only)* The preshared key, at least 32 hex digits. Required if either `tls_connect` or `tls_accept` has PSK enabled.|

[comment]: # ({/new-2f6c5c76})

[comment]: # ({new-859da78d})
### Host inventory

The host inventory object has the following properties.

::: notetip
Each property has it's own unique ID number, which is
used to associate host inventory fields with items.
:::

|ID|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--------|---------------------------------------------------|-----------|
|4|alias|string|Alias.|
|11|asset\_tag|string|Asset tag.|
|28|chassis|string|Chassis.|
|23|contact|string|Contact person.|
|32|contract\_number|string|Contract number.|
|47|date\_hw\_decomm|string|HW decommissioning date.|
|46|date\_hw\_expiry|string|HW maintenance expiry date.|
|45|date\_hw\_install|string|HW installation date.|
|44|date\_hw\_purchase|string|HW purchase date.|
|34|deployment\_status|string|Deployment status.|
|14|hardware|string|Hardware.|
|15|hardware\_full|string|Detailed hardware.|
|39|host\_netmask|string|Host subnet mask.|
|38|host\_networks|string|Host networks.|
|40|host\_router|string|Host router.|
|30|hw\_arch|string|HW architecture.|
|33|installer\_name|string|Installer name.|
|24|location|string|Location.|
|25|location\_lat|string|Location latitude.|
|26|location\_lon|string|Location longitude.|
|12|macaddress\_a|string|MAC address A.|
|13|macaddress\_b|string|MAC address B.|
|29|model|string|Model.|
|3|name|string|Name.|
|27|notes|string|Notes.|
|41|oob\_ip|string|OOB IP address.|
|42|oob\_netmask|string|OOB host subnet mask.|
|43|oob\_router|string|OOB router.|
|5|os|string|OS name.|
|6|os\_full|string|Detailed OS name.|
|7|os\_short|string|Short OS name.|
|61|poc\_1\_cell|string|Primary POC mobile number.|
|58|poc\_1\_email|string|Primary email.|
|57|poc\_1\_name|string|Primary POC name.|
|63|poc\_1\_notes|string|Primary POC notes.|
|59|poc\_1\_phone\_a|string|Primary POC phone A.|
|60|poc\_1\_phone\_b|string|Primary POC phone B.|
|62|poc\_1\_screen|string|Primary POC screen name.|
|68|poc\_2\_cell|string|Secondary POC mobile number.|
|65|poc\_2\_email|string|Secondary POC email.|
|64|poc\_2\_name|string|Secondary POC name.|
|70|poc\_2\_notes|string|Secondary POC notes.|
|66|poc\_2\_phone\_a|string|Secondary POC phone A.|
|67|poc\_2\_phone\_b|string|Secondary POC phone B.|
|69|poc\_2\_screen|string|Secondary POC screen name.|
|8|serialno\_a|string|Serial number A.|
|9|serialno\_b|string|Serial number B.|
|48|site\_address\_a|string|Site address A.|
|49|site\_address\_b|string|Site address B.|
|50|site\_address\_c|string|Site address C.|
|51|site\_city|string|Site city.|
|53|site\_country|string|Site country.|
|56|site\_notes|string|Site notes.|
|55|site\_rack|string|Site rack location.|
|52|site\_state|string|Site state.|
|54|site\_zip|string|Site ZIP/postal code.|
|16|software|string|Software.|
|18|software\_app\_a|string|Software application A.|
|19|software\_app\_b|string|Software application B.|
|20|software\_app\_c|string|Software application C.|
|21|software\_app\_d|string|Software application D.|
|22|software\_app\_e|string|Software application E.|
|17|software\_full|string|Software details.|
|10|tag|string|Tag.|
|1|type|string|Type.|
|2|type\_full|string|Type details.|
|35|url\_a|string|URL A.|
|36|url\_b|string|URL B.|
|37|url\_c|string|URL C.|
|31|vendor|string|Vendor.|

[comment]: # ({/new-859da78d})

[comment]: # ({new-0ae1807e})
### Host tag

The host tag object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|**tag**<br>(required)|string|Host tag name.|
|value|string|Host tag value.|

[comment]: # ({/new-0ae1807e})
