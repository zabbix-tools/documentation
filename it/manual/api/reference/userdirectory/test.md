[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# userdirectory.test

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-b5ff62ed})
### Description

`object userdirectory.test(array userDirectory)`

This method allows to test user directory connection settings.

::: noteclassic
This method is only available to *Super admin* user type.
:::

[comment]: # ({/new-b5ff62ed})

[comment]: # ({new-d99c84a0})
### Parameters

`(object)` User directory properties.

Since `userdirectory.get` API does not return `bind_password` field, `userdirectoryid` and/or `bind_password` should be supplied.\
Additionally to the [standard user directory properties](object#userdirectory), the method accepts the following parameters.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|test_username |string |Username to test in user directory. |
|test_password |string |Username associated password to test in user directory. |

[comment]: # ({/new-d99c84a0})

[comment]: # ({new-99a75467})
### Return values

`(bool)` Returns true on success.

[comment]: # ({/new-99a75467})

[comment]: # ({new-47b9eb66})
### Examples

[comment]: # ({/new-47b9eb66})

[comment]: # ({new-9f73d089})
#### Test user directory

Test user directory for user "user1".

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "2",
        "host": "127.0.0.1",
        "port": "3389",
        "base_dn": "ou=Users,dc=example,dc=org",
        "search_attribute": "uid",
        "bind_dn": "cn=ldap_search,dc=example,dc=org",
        "bind_password": "password",
        "test_username": "user1",
        "test_password": "password"
    },
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": true,
    "id": 1
}
```

[comment]: # ({/new-9f73d089})

[comment]: # ({new-a8a25347})
#### Test user directory

Test non existing user "user2".

Request:

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "2",
        "host": "127.0.0.1",
        "port": "3389",
        "base_dn": "ou=Users,dc=example,dc=org",
        "search_attribute": "uid",
        "bind_dn": "cn=ldap_search,dc=example,dc=org",
        "test_username": "user2",
        "test_password": "password"
    },
    "auth": "3a57200802b24cda67c4e4010b50c065",
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32500,
        "message": "Application error.",
        "data": "Incorrect user name or password or account is temporarily blocked."
    },
    "id": 1
}
```

[comment]: # ({/new-a8a25347})

[comment]: # ({new-54c18b14})
##### Test user directory for user provisioning

Test userdirectory "3" for what configured data matches the user directory settings for "user3" provisioning
(e.g., what user role, user groups, user medias will be assigned to the user).

[Request](/manual/api#performing-requests):

```json
{
    "jsonrpc": "2.0",
    "method": "userdirectory.test",
    "params": {
        "userdirectoryid": "2",
        "host": "host.example.com",
        "port": "389",
        "base_dn": "DC=zbx,DC=local",
        "search_attribute": "sAMAccountName",
        "bind_dn": "CN=Admin,OU=Users,OU=Zabbix,DC=zbx,DC=local",
        "test_username": "user3",
        "test_password": "password"
    },
    "id": 1
}
```

Response:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "username": "user3",
        "name": "John",
        "surname": "Doe",
        "medias": [],
        "usrgrps": [
            {
                "usrgrpid": "8"
            },
            {
                "usrgrpid": "7"
            }
        ],
        "roleid": "2",
        "userdirectoryid": "2"
    },
    "id": 1
}
```

[comment]: # ({/new-54c18b14})

[comment]: # ({new-0402a0f4})
### Source

CUserDirectory::test() in *ui/include/classes/api/services/CUserDirectory.php*.

[comment]: # ({/new-0402a0f4})
