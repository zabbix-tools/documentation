[comment]: # translation:outdated

[comment]: # ({new-f9e8f670})
# > User directory object

The following objects are directly related to the `userdirectory` API.

[comment]: # ({/new-f9e8f670})

[comment]: # ({new-ad144502})
### User directory {#userdirectory}

The user directory object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|userdirectoryid                   |string |*(readonly)* ID of the user directory.|
|**name**<br>(required)            |string |Unique name of the user directory.|
|**host**<br>(required)            |string |LDAP server host name, IP or URI. URI should contain schema, host and port (optional).|
|**port**<br>(required)            |integer|LDAP server port.|
|**base_dn**<br>(required)         |string |LDAP base distinguished name string.|
|**search_attribute**<br>(required)|string |LDAP attribute name to identify user by username in Zabbix database.|
|bind_dn                           |string |LDAP bind distinguished name string. Can be empty for anonymous binding.|
|bind_password                     |string |LDAP bind password. Can be empty for anonymous binding.<br><br>*Available only for userdirectory.update and userdirectory.create requests.*|
|description                       |string |User directory description.|
|search_filter                     |string |LDAP custom filter string when authenticating user in LDAP.<br><br>Default value:<br>(%{attr}=%{user})|
|start_tls                         |integer|LDAP startTLS option. It cannot be used with `ldaps://` protocol hosts.<br><br>Possible values:<br>0 - *(default)* disabled;<br>1 - enabled.|

[comment]: # ({/new-ad144502})

[comment]: # ({new-c2b51996})

### Media type mappings

The media type mappings object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|----------------|
|**name**<br>(required)|string|Visible name in the list of media type mappings.|
|**mediatypeid**<br>(required)|string|ID of the media type to be created.<br><br>Used as the value for the [`mediatypeid`](/manual/api/reference/user/object#media) field.|
|**attribute**<br>(required)|string|Attribute name.<br><br>Used as the value for the [`sendto`](/manual/api/reference/user/object#media) field.<br><br>If present in data received from IdP and the value is not empty, will trigger media creation for the provisioned user.|

[comment]: # ({/new-c2b51996})

[comment]: # ({new-67bd0ce1})

### Provisioning groups mappings

The provisioning groups mappings has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|----------------|
|**name**<br>(required)|string|IdP group full name.<br><br>Supports the wildcard character "\*". Unique across all provisioning groups mappings.|
|**roleid**<br>(required)|string|User role to assign to the user.<br><br>Note that if multiple provisioning groups mappings are matched, the role of the highest user type will be assigned to the user. If there are multiple roles with the same user type, the first role (sorted in alphabetical order) will be assigned to the user.|
|**user_groups**<br>(required)|array|Array of Zabbix user group ID objects.<br><br>Each object has the following properties:<br>`usrgrpid` - *(integer)* ID of Zabbix user group to assign to the user.<br><br>Note that if multiple provisioning groups mappings are matched, Zabbix user groups of all matched mappings will be assigned to the user.|

[comment]: # ({/new-67bd0ce1})

