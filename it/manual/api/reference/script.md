[comment]: # translation:outdated

[comment]: # ({new-d373938d})
# Script

This class is designed to work with scripts.

Object references:\

-   [Script](/manual/api/reference/script/object#script)
-   [Webhook
    parameters](/manual/api/reference/script/object#webhook_parameters)
-   [Debug](/manual/api/reference/script/object#debug)
-   [Log entry](/manual/api/reference/script/object#log_entry)

Available methods:\

-   [script.create](/manual/api/reference/script/create) - create new
    scripts
-   [script.delete](/manual/api/reference/script/delete) - delete
    scripts
-   [script.execute](/manual/api/reference/script/execute) - run scripts
-   [script.get](/manual/api/reference/script/get) - retrieve scripts
-   [script.getscriptsbyhosts](/manual/api/reference/script/getscriptsbyhosts) -
    retrieve scripts for hosts
-   [script.update](/manual/api/reference/script/update) - update
    scripts

[comment]: # ({/new-d373938d})
