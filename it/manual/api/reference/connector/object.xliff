<?xml version='1.0' encoding='UTF-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="en" target-language="it" datatype="plaintext" original="manual/api/reference/connector/object.md">
    <body>
      <trans-unit id="d96a863c" xml:space="preserve">
        <source># &gt; Connector object

The following objects are directly related to the `connector` API.</source>
      </trans-unit>
      <trans-unit id="bb03100b" xml:space="preserve">
        <source>### Connector

The connector object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|connectorid|string|ID of the connector.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *read-only*&lt;br&gt;- *required* for update operations|
|name|string|Name of the connector.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|url|integer|Endpoint URL, that is, URL of the receiver.&lt;br&gt;User macros are supported.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* for create operations|
|protocol|integer|Communication protocol.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Zabbix Streaming Protocol v1.0.|
|data_type|integer|Data type.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Item values;&lt;br&gt;1 - Events.|
|max\_records|integer|Maximum number of events or items that can be sent within one message.&lt;br&gt;&lt;br&gt;Possible values: 0-2147483647 (max value of 32-bit signed integer).&lt;br&gt;&lt;br&gt;Default: 0 - Unlimited.|
|max\_senders|integer|Number of sender processes to run for this connector.&lt;br&gt;&lt;br&gt;Possible values: 1-100.&lt;br&gt;&lt;br&gt;Default: 1.|
|max\_attempts|integer|Number of attempts.&lt;br&gt;&lt;br&gt;Possible values: 1-5.&lt;br&gt;&lt;br&gt;Default: 1.|
|timeout|string|Timeout.&lt;br&gt;Time suffixes are supported (e.g., 30s, 1m).&lt;br&gt;User macros are supported.&lt;br&gt;&lt;br&gt;Possible values: 1s-60s.&lt;br&gt;&lt;br&gt;Default: 5s.|
|http\_proxy|string|HTTP(S) proxy connection string given as &lt;br&gt;*\[protocol\]://\[username\[:password\]@\]proxy.example.com\[:port\]*.&lt;br&gt;&lt;br&gt;User macros are supported.|
|authtype|integer|HTTP authentication method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* None;&lt;br&gt;1 - Basic;&lt;br&gt;2 - NTLM;&lt;br&gt;3 - Kerberos;&lt;br&gt;4 - Digest;&lt;br&gt;5 - Bearer.|
|username|string|User name.&lt;br&gt;User macros are supported.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `authtype` is set to "Basic", "NTLM", "Kerberos", or "Digest"|
|password|string|Password.&lt;br&gt;User macros are supported.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *supported* if `authtype` is set to "Basic", "NTLM", "Kerberos", or "Digest"|
|token|string|Bearer token.&lt;br&gt;User macros are supported.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required* if `authtype` is set to "Bearer"|
|verify\_peer|integer|Whether to validate the host name matches with the URLs provided in the *Common Name* or *Subject Alternate Name* fields of the host certificate.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not validate;&lt;br&gt;1 - *(default)* Validate.|
|verify\_host|integer|Whether to validate the authenticity of the host certificate.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Do not validate;&lt;br&gt;1 - *(default)* Validate.|
|ssl\_cert\_file|integer|Public SSL Key file path.&lt;br&gt;User macros are supported.|
|ssl\_key\_file|integer|Private SSL Key file path.&lt;br&gt;User macros are supported.|
|ssl\_key\_password|integer|Password for SSL Key file.&lt;br&gt;User macros are supported.|
|description|string|Description of the connector.|
|status|integer|Whether the connector is enabled.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - Disabled;&lt;br&gt;1 - *(default)* Enabled.|
|tags\_evaltype|integer|Tag evaluation method.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* And/Or;&lt;br&gt;2 - Or.|</source>
      </trans-unit>
      <trans-unit id="228356ed" xml:space="preserve">
        <source>### Tag filter

Tag filter allows to export only matching item values or events. If not set then everything will be exported.
The tag filter object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--|--|------|
|tag|string|Tag name.&lt;br&gt;&lt;br&gt;[Property behavior](/manual/api/reference_commentary#property-behavior):&lt;br&gt;- *required*|
|operator|integer|Condition operator.&lt;br&gt;&lt;br&gt;Possible values:&lt;br&gt;0 - *(default)* Equals;&lt;br&gt;1 - Does not equal;&lt;br&gt;2 - Contains;&lt;br&gt;3 - Does not contain;&lt;br&gt;12 - Exists;&lt;br&gt;1 - Does not exist.|
|value|string|Tag value.|</source>
      </trans-unit>
    </body>
  </file>
</xliff>
