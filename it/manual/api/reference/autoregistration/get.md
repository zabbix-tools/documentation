[comment]: # translation:outdated

[comment]: # ({new-8b0c7f00})
# autoregistration.get

[comment]: # ({/new-8b0c7f00})

[comment]: # ({new-c6a45cf0})
### Description

`object autoregistration.get(object parameters)`

The method allows to retrieve autoregistration object according to the
given parameters.

::: noteclassic
This method is only available to *Super admin* user type.
Permissions to call the method can be revoked in user role settings. See
[User
roles](/manual/web_interface/frontend_sections/administration/user_roles)
for more information.
:::

[comment]: # ({/new-c6a45cf0})

[comment]: # ({new-24b20f8d})
### Parameters

`(object)` Parameters defining the desired output.

The method supports only one parameter.

|Parameter|[Type](/manual/api/reference_commentary#data_types)|Description|
|---------|---------------------------------------------------|-----------|
|output|query|This parameter being common for all `get` methods described in the [reference commentary](/manual/api/reference_commentary#common_get_method_parameters).|

[comment]: # ({/new-24b20f8d})

[comment]: # ({new-805927e0})
### Return values

`(object)` Returns autoregistration object.

[comment]: # ({/new-805927e0})

[comment]: # ({new-1f25ddf1})
### Examples

Request:

``` {.java}
{
   "jsonrpc": "2.0",
    "method": "autoregistration.get",
    "params": {
        "output": "extend"
    },
    "auth": "038e1d7b1735c6a5436ee9eae095879e",
    "id": 1
}
```

Response:

``` {.java}
{
    "jsonrpc": "2.0",
    "result": {
        "tls_accept": "3"
    },
    "id": 1
}
```

[comment]: # ({/new-1f25ddf1})

[comment]: # ({new-c74f8347})
### Source

CAutoregistration::get() in
*ui/include/classes/api/services/CAutoregistration.php*.

[comment]: # ({/new-c74f8347})
