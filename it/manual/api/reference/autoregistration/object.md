[comment]: # translation:outdated

[comment]: # ({new-de6776f4})
# > Autoregistration object

The following objects are directly related to the `autoregistration`
API.

[comment]: # ({/new-de6776f4})

[comment]: # ({new-1e4b66e4})
### Autoregistration

The autoregistration object has the following properties.

|Property|[Type](/manual/api/reference_commentary#data_types)|Description|
|--------|---------------------------------------------------|-----------|
|tls\_accept|integer|Type of allowed incoming connections for autoregistration.<br><br>Possible values:<br>1 - allow unsecure connections;<br>2 - allow TLS with PSK.<br>3 - allow both unsecure and TLS with PSK connections.|
|tls\_psk\_identity|string|*(write-only)* PSK identity string.<br>Do not put sensitive information in the PSK identity, it is transmitted unencrypted over the network to inform a receiver which PSK to use.|
|tls\_psk|string|*(write-only)* PSK value string (an even number of hexadecimal characters).|

[comment]: # ({/new-1e4b66e4})
