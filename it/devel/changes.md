[comment]: # translation:outdated

[comment]: # aside:4

[comment]: # ({new-0288c9d9})

# Changes to extension development

This page lists all changes to developing custom Zabbix extensions.

[comment]: # ({/new-0288c9d9})

[comment]: # ({new-ac597f9b})

### Changes from 6.4 to 7.0

[comment]: # ({/new-ac597f9b})

[comment]: # ({new-5b0ca958})

##### Modules

[comment]: # ({/new-5b0ca958})

[comment]: # ({new-b89e6d52})

[ZBXNEXT-8086](https://support.zabbix.com/browse/ZBXNEXT-8086) The object key `template_support` is no longer supported for the `manifest.json` file parameter [`widget`](/devel/modules/file_structure/manifest#widget).
It is no longer required to determine whether a widget should be available on template dashboards, because, since Zabbix 7.0, template dashboards support all widgets.
For more information, see: [*Expanded widget availability on template dashboards*](/manual/introduction/whatsnew700#expanded-widget-availability-on-template-dashboards).

[comment]: # ({/new-b89e6d52})

[comment]: # ({new-dd18f5e7})

### Changes in 7.0

[comment]: # ({/new-dd18f5e7})
